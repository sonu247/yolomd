<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';
class Webservices extends REST_Controller 
{
	public function __construct()
	{
	    parent::__construct();
	    $_POST = $this->post();
	    $this->load->model('user_model');
	}
    
    public function home_content_get(){

    	$banner_section=$this->common_model->get_random_banner();
    	$physician_medical_section=$this->common_model->get_row('cms_physicians_medical',array('id'=>1));
    	$city_section=$this->common_model->get_result('search_job_city',array('status'=>1),array(),array('order_by','ASC'));
    	$section_4=$this->common_model->get_row('cms_physicians_medical',array('id'=>2));
    	$check_social_login_val = '';
    	if($this->session->userdata('check_social_login_val'))
    	{
    		$check_social_login_val = $this->session->userdata('check_social_login_val');
    		$this->session->unset_userdata('check_social_login_val');

    	}
    	$social_login_error = '';
    	if($this->session->userdata('social_login_error'))
    	{
    		$social_login_error = $this->session->userdata('social_login_error');
    		$this->session->unset_userdata('social_login_error');
    	}	
    	$data=array(
			'banner_section'			=>$banner_section,
			'physician_medical_section'	=>$physician_medical_section,
			'city_section'				=>$city_section,
			'section_4'					=>$section_4,
			'check_social_login_val'    => $check_social_login_val,
			'social_login_error'        => $social_login_error
    		);
    	$this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data show successfully.";
		$this->response_array['data'] = $data;	
		$this->api_response();
    }

    public function medical_facility_signup_post()
	{
		$this->form_validation->set_rules('user_name_title', 'Contact Person title', 'required');
        $this->form_validation->set_rules('contact_person_first_name', 'Contact Person first name', 'required');
        $this->form_validation->set_rules('contact_person_last_name', 'Contact Person last name', 'required');
        $this->form_validation->set_rules('contact_person_email_address', 'Contact Person Email', 'trim|required|valid_email|matches[conf_email_address]|callback_email_check[1]');  
        $this->form_validation->set_rules('conf_email_address','Confirm Emails', 'trim|required');
        $this->form_validation->set_rules('password', 'New Password', 'trim|required|matches[confpassword]|min_length[6]');
        $this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required|min_length[6]');
        //$this->form_validation->set_rules('term','Check box', 'required');
  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  	    
	  	    
        if ($this->form_validation->run() == TRUE && $_POST['captcha'] == $this->session->userdata("medical"))  
        {
  	    	$salt = $this->salt();
  	    	$username = '';
		    $user_data=array(
				'user_name_title'	=> $this->input->post('user_name_title'),
				'user_first_name'	=> trim($this->input->post('contact_person_first_name')),
				'user_last_name'	=> trim($this->input->post('contact_person_last_name')),
				'user_email'		=> trim($this->input->post('contact_person_email_address')),
				'password'			=> sha1($salt . sha1($salt . sha1(trim($this->input->post('password'))))),
				'salt'				=> $salt,
				'user_role'			=> 1,
				'user_status'		=> 1,
				'last_login_ip'		=> $this->input->ip_address(),
				'user_created'		=> date('Y-m-d H:i:s A'),
			);
			$username = $this->input->post('contact_person_first_name').' '.$this->input->post('contact_person_last_name');
			$user_id=$this->common_model->insert('users',$user_data);
			if($user_id)
			{
				include(APPPATH.'libraries/phpmailer/class.phpmailer.php');
				$email_template = $this->common_model->get_row('email_templates',array('id'=>1));
				$user_info = '';
				$user_info = get_user_info($user_id);
				$site_url = site_url().'medical-profile';
				$image_url = base_url().'assets/front/images/click-here-7.png';
			    $param=array
			    		(
							'template'  =>  
							array(
						    		'temp'  =>  $email_template->template_body,
					        		'var_name'  =>  
					        					array(
				  										'username'    => $username,
				  										'site_url' => $site_url,
				  										'image_url' => $image_url
														
													),   
							),   
							'email' =>  
						    array(
						            'to'        =>   $this->input->post('contact_person_email_address'), 
						            'from'      =>   GLOBAl_INFO_EMAIL,
						            'from_name' =>   SITE_NAME,
						            'subject'   =>   $email_template->template_subject,
						     ),
						);   
			            $status=$this->chapter247_email->send_mail($param); 
			}
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data inserted successfully.";	
			$this->api_response();
		} 
		else
		{
		 	$this->response_array['response_code'] = 500;
		 	if ($_POST['captcha'] != $this->session->userdata("medical")) {
				$this->response_array['message'] = "Captcha is Invalid, Please try again";	
		 	}
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->api_response();
		} 
	}

	public function physician_signup_post()
	{
		$data = array();
		$this->form_validation->set_rules('user_name_title', 'Physician title', 'required');
        $this->form_validation->set_rules('physician_first_name','Physician first name', 'required');
        $this->form_validation->set_rules('physician_last_name', 'Physician last name', 'required');
        $this->form_validation->set_rules('email_address', 'Contact Person Email', 'trim|required|valid_email|matches[conf_email_address]|callback_email_check[2]');  
        $this->form_validation->set_rules('conf_email_address','Confirm Emails', 'trim|required');
        $this->form_validation->set_rules('password', 'New Password', 'trim|required|matches[confpassword]|min_length[6]');
        $this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required|min_length[6]');

        $this->form_validation->set_rules('physician_gender', 'Physician Gender', 'required');
        $this->form_validation->set_rules('physician_dob_date', 'Physician DOB date', 'required');
        $this->form_validation->set_rules('physician_dob_month', 'Physician DOB month', 'required');
        $this->form_validation->set_rules('physician_dob_year', 'Physician DOB year', 'required');
        $this->form_validation->set_rules('physician_profession', 'Physician profession', 'required');
        $this->form_validation->set_rules('medical_specialty', 'Medical Specialty', 'required');
        $this->form_validation->set_rules('medical_school', 'Medical School', 'required');

        $this->form_validation->set_rules('physician_phone_number', 'Phone Number', 'required');
       
  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  	    $last_id = '';
        if ($this->form_validation->run() == TRUE && $_POST['captcha'] == $this->session->userdata("physician"))  {
  	    	$salt = $this->salt();
		    $user_data=array(
				'user_name_title' => $this->input->post('user_name_title'),
				'user_first_name' => trim($this->input->post('physician_first_name')),
				'user_last_name'  => trim($this->input->post('physician_last_name')),
				'user_email'      => trim($this->input->post('email_address')),
				'password'        => sha1($salt . sha1($salt . sha1(trim($this->input->post('password'))))),
				'salt'            => $salt,
				'user_role'       => 2,
				'user_status'     => 1,
				'last_login_ip'   => $this->input->ip_address(),
				'user_created'    => date('Y-m-d H:i:s A'),
			);
			$user_id = $this->common_model->insert('users',$user_data);
			if(!empty($user_id)){
				$physician_data=array(
					'user_id'                 => $user_id,
					'physician_gender'        => $this->input->post('physician_gender'),
					'physician_dob_date'      => $this->input->post('physician_dob_date'),
					'physician_dob_month'     => $this->input->post('physician_dob_month'),
					'physician_dob_year'      => $this->input->post('physician_dob_year'),
					'physician_profession'    => $this->input->post('physician_profession'),
					'medical_specialty'       => $this->input->post('medical_specialty'),
					'medical_school'          => $this->input->post('medical_school'),
					'traning_year'            => $this->input->post('traning_year'),
					'physician_minc'          => $this->input->post('physician_minc'),
					'physician_country'       => $this->input->post('physician_country'),
					'physician_state'         => $this->input->post('physician_state'),
					'physician_city'          => $this->input->post('physician_city'),
					'physician_address'       => $this->input->post('physician_address'),
					'physician_postal_code'   => $this->input->post('physician_postal_code'),
					'physician_phone_number'  => $this->input->post('physician_phone_number'),
					'physician_default_msg'   => $this->input->post('physician_default_msg'),
					'medical_selectSpecialty' => $this->input->post('medical_selectSpecialty') ? $this->input->post('medical_selectSpecialty') : '',
					'physician_selectState'   => $this->input->post('physician_selectState') ? $this->input->post('physician_selectState') : '',
				);
			 	$last_id = $this->common_model->insert('physicians',$physician_data);

			 	if(!empty($last_id))
				{
					include(APPPATH.'libraries/phpmailer/class.phpmailer.php');
					$user_info = '';
					$user_info = get_user_info($last_id);
					$username = $this->input->post('physician_first_name').' '.$this->input->post('physician_last_name');
					$email_template = $this->common_model->get_row('email_templates',array('id'=>2));
					$site_url = site_url().'physician-profile';
					$image_url = base_url().'assets/front/images/click-here-7.png';
			    	$param=array
			    		(
							'template'  =>  
							array(
						    		'temp'  =>  $email_template->template_body,
					        		'var_name'  =>  
					        					array(
				  										'username'    => $username,
				  										'site_url' => $site_url,
				  										'image_url' => $image_url
														
													),   
							),   
							'email' =>  
						    array(
						            'to'        =>   $this->input->post('email_address'), 
						            'from'      =>   GLOBAl_INFO_EMAIL,
						            'from_name' =>   SITE_NAME,
						            'subject'   =>   $email_template->template_subject,
						     ),
						);   
			        $status=$this->chapter247_email->send_mail($param); 
				}
			  	$this->response_array['response_code'] = 200;				
			  	$this->response_array['message'] = "Data inserted successfully.";	
			  	$this->api_response();
			}
			else{
	  		 	$this->response_array['response_code'] = 500;
				$this->response_array['message'] = "Invalid Form Request";	
				$this->response_array['error'] = $this->form_validation->error_array() ;
				$this->api_response();
			}
		} 
		else{
			if ($_POST['captcha'] != $this->session->userdata("physician")) {
				$data['step'] = 4;	
				$this->response_array['message'] = "Invalid Captcha";	
			}
 		 	$this->response_array['response_code'] = 500;
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->response_array['data'] = $data;	
			$this->api_response();
		}
	}
   function email_check($str='',$role=''){
      	if($this->common_model->get_row('users',array('user_email'=>$str,'user_role' => 1)) && $role!=''):
            $this->form_validation->set_message('email_check', $role==2?"This e-mail login already exists for a Medical Facility user. Please choose a new one.":'This e-mail login already exists. Please choose a new one.');
            return FALSE;
        elseif($this->common_model->get_row('users',array('user_email'=>$str,'user_role' => 2)) && $role!=''):
            $this->form_validation->set_message('email_check', $role==1?"This e-mail login already exists for a Doctor user. Please choose a new one.":'This e-mail login already exists. Please choose a new one.');
            return FALSE;
        elseif($this->common_model->get_row('users',array('user_email'=>$str,'user_role !=' => 3))):
            $this->form_validation->set_message('email_check', 'The %s address already exists.');
            return FALSE;
        else:
           return TRUE;
        endif;
     }

	private function salt()
  	{
    	return substr(md5(uniqid(rand(), true)), 0, 10);
  	}

  	function physician_signup_content_get()
  	{
  		$currently_status = currently_status();
    	$medical_school =$this->common_model->get_result('medical_specialties_annex1',array('status'=>1),'',array('order_by','asc'));
    	$medical_specialty = $this->common_model->get_result('medical_specialties_annex2',array('status'=>1),'',array('order_by','asc'));
    	$canada_province =$this->common_model->get_province('location_complete_data',array(),'','','province_name');
    	$physician_content =$this->common_model->get_result('physician_medical_facility_content',array('status'=>1,'type'=>1),'',array('order_by','asc'));

    	$data=array(
			'currently_status'	=>$currently_status,
			'medical_school'	=>$medical_school,
			'medical_specialty'	=>$medical_specialty,
			'canada_province'	=>$canada_province,
			'physician_content'	=>$physician_content,
    		);
    	$this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data show successfully.";
		$this->response_array['data'] = $data;	
		$this->api_response();
  	}

  	public function user_login_post()
  	{	
  		$data=array();
  		$this->response_array['data'] =$data;
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	    $this->form_validation->set_rules('password', 'Password', 'trim|required');
	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	    if ($this->form_validation->run() == TRUE){
	        $password = $this->input->post('password');
	        $this->load->model('user_model');
	        if($this->user_model->login($this->input->post('email'),$password,'user')){
	        	$link='';
	        	if(medicaluser_logged_in()){
	        		$link='job-listing';
	        	}elseif(physician_logged_in()){
	        		$link='jobs/';
	        	}

	            $this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Login Done successfully.";
				$this->response_array['data'] = $link;
	        }else{
	            $this->response_array['response_code'] = 500;				
				$data['error_message'] = "Invalid Email or Password.";
				$this->response_array['data'] = $data;	
	        }
	    } 
	    else{

		    $this->response_array['response_code'] = 500;				
			$this->response_array['message'] = "Invalid.";
			$this->response_array['error'] = $this->form_validation->error_array() ;

		}
	    $this->api_response();
  	}

  	public function logout_post(){
		$this->session->sess_destroy();
	}

  	public function physician_medical_facility_content_get()
  	{
  		$physician_content =$this->common_model->get_result('physician_medical_facility_content',array('status'=>1,'type'=>1),'',array('order_by','asc'));
    	$medical_facility = $this->common_model->get_result('physician_medical_facility_content',array('status'=>1,'type'=>2),'',array('order_by','asc'));
    	$physician_medical_section=$this->common_model->get_row('cms_physicians_medical',array('id'=>1));
    	$data=array(
			'physician_content'			=>$physician_content,
			'medical_facility'			=>$medical_facility,
			'physician_medical_section'	=>$physician_medical_section,
    		);
    	$this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data show successfully.";
		$this->response_array['data'] = $data;	
		$this->api_response();
  	}

  	public function logout_get()
  	{
	    $this->session->sess_destroy();
	    $this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data show successfully.";
		$this->api_response();
	}

	public function post_job_content_get()
  	{
		
  		$city_data = $this->common_model->get_result('city_price',array('status'=>1),'',array('order_by','asc'));
  		$priority_placement = $this->common_model->get_row('options',array('status'=>1,'id'=>30),'',array());
  		$verified_facility_content = $this->common_model->get_row('options',array('status'=>1,'id'=>31),'',array());
  		$photograph_fee = $this->common_model->get_row('options',array('status'=>1,'id'=>29),'',array());
  			$total_val = '';
  		foreach ($city_data as $city_value)
  		{
  			$total_val = $city_value->price;
  		}
  		
		
		$discount	= $this->common_model->get_row('options',array('status'=>1,'id'=>32));
		
		$discount_cal			= $discount->option_value;
		$time_array				= time_array();
		$time_array_eve			= time_array_eve();
		$transport_typ			= transport_type();
		$facility_typ			= facility_type();
		$hour					= hours();
		$compensatio			= compensation();
		$health_professionals	= health_professionals();
		$nursing				= nursing();
		$verified_facility		= verified_facility();
		$paypal_priority		= paypal_priority();
		$user_information		= get_medicaluser_info();
		$month_name				= month_name();
		$days_name				= days_name();

    	$data=array(
    	    'time_array'			=> $time_array,
    	    'time_array_eve'        => $time_array_eve,
			'transport_type'		=> $transport_typ,
			'facility_type'			=> $facility_typ,
			'hours'					=> $hour,
			'compensation'			=> $compensatio,
			'health_professionals'	=> $health_professionals,
			'nursing'				=> $nursing,
			'verified_facility'		=> $verified_facility,
			'paypal_priority'		=> $paypal_priority,
			'user_information'		=> $user_information,
			'month_name'			=> $month_name,
			'days_name'				=> $days_name,

    		);
    	$data['discount_amount']				= $discount_cal;
		$data['city_data']						= $city_data;
		$data['total_price']					= $total_val;
		$data['priority_placement']				= $priority_placement;
		$data['verified_facility_content']				= $verified_facility_content;
		$data['photograph_fee']					= $photograph_fee;
				
		$this->response_array['response_code']	= 200;				
		$this->response_array['message']		= "Data show successfully.";
		$this->response_array['data']			= $data;	
		$this->api_response();
  	}


    public function CheckLoginredirection_get()
    {
    	if(medicaluser_logged_in()==1){
			$this->response_array['response_code'] = 200;
			$this->response_array['data'] = "job-listing";	
		}
		elseif(physician_logged_in()==1){
			$this->response_array['response_code'] = 200;
			$this->response_array['data'] = "jobs";	
		}
		else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
	}  

	public function check_Medical_user_login_get()
    {
    	if(medicaluser_logged_in()==1){
			$this->response_array['response_code'] = 200;
			$this->response_array['message'] = "User login";	
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
	}

	public function check_physicain_login_get()
	{
		if(physician_logged_in()==1){
			$this->response_array['response_code'] = 200;	
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
	}

	public function post_job_post()
	{
		$check_validation = '';
		if(!$_POST['draft'])
		{
			$this->form_validation->set_rules('contact_name_title', 'Contact Person title', 'required');
	        $this->form_validation->set_rules('contact_first_name', 'Contact Person first name', 'required');
	        $this->form_validation->set_rules('contact_last_name', 'Contact Person last name', 'required');
	        $this->form_validation->set_rules('contact_email_address', 'Contact Person Email', 'trim|required|valid_email'); 
	        $this->form_validation->set_rules('contact_fax', 'Fax Number', 'min_length[8]|max_length[15]');
	        $this->form_validation->set_rules('contact_phone_number', 'Facility Phone Number', 'required|min_length[8]|max_length[12]');  
			//       $this->form_validation->set_rules('director_first_name', 'Director first name', 'required');
			//       $this->form_validation->set_rules('director_last_name', 'Director last name', 'required|alpha_numeric_spaces');
			 //       $this->form_validation->set_rules('director_gender', 'Director Gender', 'trim|required');  
					// $this->form_validation->set_rules('director_dob_date', 'Date', 'required');
			  //       $this->form_validation->set_rules('director_dob_month', 'Month', 'required');
			  //       $this->form_validation->set_rules('director_dob_year', 'Year', 'required');
	        //$this->form_validation->set_rules('director_phone_number', 'Director Phone Number', 'required|min_length[8]|max_length[12]');
	        $this->form_validation->set_rules('medical_specialty_name', 'Medical Specialties', 'trim|required'); 
	        $this->form_validation->set_rules('special_qualification', 'Special Qualification', 'required');
	        $this->form_validation->set_rules('hour', 'Hours', 'required');
	        $this->form_validation->set_rules('position', 'Position', 'trim|required');

	        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');  
	        $this->form_validation->set_rules('start_month', 'Start Month', 'required');
	        $this->form_validation->set_rules('start_year', 'Start Year', 'trim|required');
	         
	     	$this->form_validation->set_rules('call_responsibility', 'Call Responsibility', 'trim|required');


 
	        $this->form_validation->set_rules('facility_name', 'Facility Name', 'required');
	        $this->form_validation->set_rules('facility_state', 'Province', 'required');
	        $this->form_validation->set_rules('facility_city', 'City', 'required');
	        $this->form_validation->set_rules('facility_address', 'Address', 'trim|required');  
	        $this->form_validation->set_rules('facility_postal_code', 'Postal Code', 'required|max_length[7]');
	         
	        $this->form_validation->set_rules('facility_type', 'Facility Type', 'trim|required'); 

	        $this->form_validation->set_rules('parking', 'Parking', 'required');
	        $this->form_validation->set_rules('no_of_fulltime', 'Number of Fulltime', 'trim|required|numeric|max_length[6]');
	        $this->form_validation->set_rules('part_time_doctor', 'Number of part-time doctors', 'trim|required|numeric|max_length[6]');
	        $this->form_validation->set_rules('exam_room', 'Exam room', 'trim|required|numeric|max_length[6]');
	        //$this->form_validation->set_rules('nursing_array', 'Nursing', 'required');
	        //$this->form_validation->set_rules('facility_hours', 'Facility Hours', 'required|alpha_numeric_spaces');
	        
	     	//$this->form_validation->set_rules('health_array', 'Allied Health Professionals', 'trim|required');
	         
	     	$this->form_validation->set_rules('listing_title', 'List Title', 'trim|required');
	     	$this->form_validation->set_rules('listing_description', 'List Description', 'trim|required');
	     	
	  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	  	    $check_validation = $this->form_validation->run();
	  	}
	  	else
	  	{
	  		$check_validation = TRUE;
	  	}
        if($check_validation == TRUE)  
        {
			$_POST['user_id'] = medicaluser_id();
			if($_POST['draft'])
			{
				$_POST['status'] = 4;
			}	
  	    	if(!empty($_POST['uploadImageArray']))
  	    	{
  	    		$full_image = '';
	    		foreach($_POST['uploadImageArray'] as $image_name)
	    		{
		    		
		    		foreach($image_name as $img_name)
		    		{
			    		//exit;
			    		$existing_image_name = $img_name;
					    $title_name = $_POST['listing_title'];
					    $list_name = url_title($title_name);
					    $extension = explode('.', $existing_image_name);
					    $count_exe = count($extension)-1;
					    $new_image_name= $list_name.'_'.strtotime("now").'_'.rand(1000,10000).'.'.$extension[$count_exe];
					    $old_image_path = './assets/uploads/jobs/'.$existing_image_name;
					    $new_image_path = './assets/uploads/jobs/'.$new_image_name;
				    	$thumb_old_image_path = './assets/uploads/jobs/thumbs/'.$existing_image_name;
					    $thumb_new_image_path = './assets/uploads/jobs/thumbs/'.$new_image_name;
				    	if(file_exists($old_image_path))
				    	{
				    		rename($old_image_path , $new_image_path);
				    	}
				    	if(file_exists($thumb_old_image_path))
				    	{
				    		rename($thumb_old_image_path , $thumb_new_image_path);
				    	}
				    	$full_image[] = array('imageName' =>$new_image_name); 
				    }	
			    }
	    		$_POST['uploadImageArray'] =  serialize($full_image);
  	    	}else{
  	    		$_POST['uploadImageArray'] = '';
  	    	}
  	    	if(!empty($_POST['facility_hours']))
  	    	{
  	    		$_POST['facility_hours_array'] =  serialize($_POST['facility_hours']);
  	    	}else{
  	    		$_POST['facility_hours_array'] = '';
  	    	}
  	    	$facility_hours_loop = $_POST['facility_hours'];
  	    	unset($_POST['start_time']);
  	    	unset($_POST['end_time']);
  	    	unset($_POST['facility_hours']);
  	    	unset($_POST['draft']);
  	    	if(empty($_POST['transport_train']))
  	    	{
  	    		$_POST['transport_train_value'] = '';
  	    	}elseif($_POST['transport_train'] == false)
  	    	{
  	    		$_POST['transport_train_value'] = '';
  	    	}
  	    	if(empty($_POST['transport_metro_subway']))
  	    	{
  	    		unset($_POST['transport_m_s_value']);
  	    	}elseif($_POST['transport_metro_subway'] == false)
  	    	{
  	    		$_POST['transport_m_s_value'] = '';
  	    	}
  	    	if(empty($_POST['transport_bus']))
  	    	{
  	    		unset($_POST['transport_bus_value']);
  	    	}elseif($_POST['transport_bus'] == false)
  	    	{
  	    		$_POST['transport_bus_value'] = '';
  	    	}
  	    	if(empty($_POST['transport_other']))
  	    	{
  	    		unset($_POST['transport_other_value']);
  	    	}elseif($_POST['transport_other'] == false)
  	    	{
  	    		$_POST['transport_other_value'] = '';
  	    	}
			$last_id = $this->common_model->insert('post_jobs',$_POST);
			if($last_id && $this->input->post('status')!=4)
			{
				$email_template = $this->common_model->get_row('email_templates',array('id'=>3));
				$sender_info_user = get_user_info(medicaluser_id());
		        $sender_name = $sender_info_user->user_first_name.' '.$sender_info_user->user_last_name;
		        $sender_id = $sender_info_user->user_id;
		        $image_url = base_url().'assets/front/images/click-here-7.png';
			    $param=array
	    		(
					'template'  =>  
					array(
			    		'temp'  =>  $email_template->template_body,
		        		'var_name'  =>  
    					array(
							'job_id'    => $last_id,
							'job_name'  => $_POST['listing_title'],
							'site_url' => site_url(),
							'sender_id' => $sender_id,
							'sender_name' => $sender_name,
							'image_url' => $image_url					
						),   
					),   
					'email' =>  
				    array(
			            'to'        =>   ADMIN_EMAIL, 
			            'from'      =>   GLOBAl_INFO_EMAIL,
			            'from_name' =>   SITE_NAME,
			            'subject'   =>   $email_template->template_subject,
				    ),
				);   
			    $status=$this->chapter247_email->send_mail($param); 
			}
			$data['last_id_key'] = base64_encode($last_id);
			$this->session->set_userdata('check_post',1);
			$this->response_array['data'] = $data;	
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data inserted successfully.";
			$this->api_response();	
		} 
		else
		{
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->api_response();
		}
	}

	function getLnt_post()
	{
		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
	 if($address=$_POST['address']){
        $address=urlencode($address);
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        if($response){
            $json_a = json_decode($response, TRUE); //json decoder
            $data=array();

            if($json_a['status'] != 'ZERO_RESULTS'){
                $data['lat']=$json_a['results'][0]['geometry']['location']['lat']; // get lat for json
                $data['lng']=$json_a['results'][0]['geometry']['location']['lng']; // get ing for json
                $this->response_array['data'] = $data;
				$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Get lat long successfully.";	
             }
        	}
          }
		 $this->api_response();
    }

    public function job_listing_get()
  	{
  		$user_id = medicaluser_id();
  		$status = '';
  		$image_new = '';
  		$result ='';
  		$check_post_value = '';
  		$post_jobs =$this->common_model->get_result('post_jobs',array('user_id'=>$user_id),'',array('id','desc'));
  		if(!empty($post_jobs))
  		{
	  		foreach($post_jobs as $value)
	  		{

	  			$medical_spec_name = get_medical_specialty($value->medical_specialty_name);
	  			if($value->status == 1)
	  			{
	  				$status = 'Active';
	  			}
	  			if($value->status == 2)
	  			{
	  				$status = 'Deactive';
	  			}
	  			if($value->status == 3)
	  			{
	  				$status = 'Banned';
	  			}
	  			if($value->status == 0)
	  			{
	  				$status = 'Pending';
	  			}
	  			if($value->status == 4)
	  			{
	  				$status = 'Draft';
	  			}
	  			$date_posted = '';
	  			//echo $value->posted;
	  			$date_created = date('d M y',strtotime($value->created));
	  			if($value->posted !='0000-00-00 00:00:00')
	  			{
	  				$date_posted = date('d M y',strtotime($value->posted));
	  			}else
	  			{
	  				$date_posted = '';
	  			}
	  			$post_image  = '';
	  			if($value->uploadImageArray)
	  			{
	  				$image_new = unserialize($value->uploadImageArray);
	  				//echo '<pre>'; print_r($image_new);
	  				//echo $value->id;
	  				if(file_exists('assets/uploads/jobs/'.$image_new[0]['imageName']))
				    {
		  				if(!empty($image_new))
			  			{
			  				if(file_exists('assets/uploads/jobs/'.$image_new[0]['imageName']))
					    	{
			  					$post_image = site_url().'assets/uploads/jobs/'.$image_new[0]['imageName'];
			  				}	
			  			}
			  		}	
	  			}

	  			
	  			$medical_spec_logo = get_medical_specialty_logo($value->medical_specialty_name);
		        $logo_image = '';
		        if(!empty($medical_spec_logo))
		        {
			        if(file_exists('assets/uploads/logo/'.$medical_spec_logo))
				    {
			  			$logo_image = site_url().'assets/uploads/logo/'.$medical_spec_logo;
			  			
			  		}else{
		  				$logo_image = site_url().'assets/uploads/logo/medal-new.svg';
		  			}	
			  	}else{
		  			$logo_image = site_url().'assets/uploads/logo/medal-new.svg';
		  		}	
		  		
		  		$result[] = (object)array(
						'medical_spec_name' => $medical_spec_name,
						'id'                => $value->id,
						'listing_title'     => $value->listing_title,
						'facility_city'     => $value->facility_city,
						'status'            => $status,
						'created'           => $date_created,
						'image_path'        => $post_image,
						'job_verify'		=> $value->job_verify,
						'posted'			=> $date_posted,
						'encrpt_id'			=> base64_encode($value->id),
						'medical_spec_logo' => $logo_image
						); 
	  			if($this->session->userdata('check_post'))
				{
					$check_post_value	= $this->session->userdata('check_post');
					$this->session->unset_userdata('check_post');
				}
	  		}
	  	}	
    	$data=array(
			'post_jobs'	=> $result,
			'check_post_value' => $check_post_value
    		);
    	if(!empty($data)){
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
 
  	}
    
    public function get_medical_specialty_name_post()
  	{
  		$this->response_array['response_code'] = 500;	
  		$this->response_array['message'] = "Invalid Form Request";
  		if(!empty($_POST['MedicalSpecialtyValue']))
  		{
  			$medical_spec_name = get_medical_specialty($_POST['MedicalSpecialtyValue']);
	  		$data=array(
				'medical_spec_name'	=> $medical_spec_name,
	    		);
	  		if(!empty($data)){
		  		$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data show successfully.";
				$this->response_array['data'] = $data;	
			}
  		}
		$this->api_response();
  	}

  	public function preview_listing_value_post()
  	{
  		$this->response_array['response_code'] = 500;	
  		$this->response_array['message'] = "Invalid Form Request";
  		$data = array();
  		if(!empty($_POST['MedicalSpecialtyValue']))
  		{
  			$medical_spec_name = get_medical_specialty($_POST['MedicalSpecialtyValue']);
	  		$data['medical_spec_name'] = $medical_spec_name;
	  		
  		}
  		if(!empty($_POST['FacilityType']))
  		{
  			$facility_type = get_facility_type($_POST['FacilityType']);
	  		$data['facility_type'] = $facility_type;
  		}
  	
  		// if(!empty($_POST['NursingArray']))
  		// {
  		// 	$nursing_pro = '';
  		// 	$nursing_array = $_POST['NursingArray'];

  		// 	foreach($nursing_array as $key1 =>$nursing_a)
  		// 	{
  		// 		if($nursing_a)
  		// 		{
  		// 			$nursing_pro .= "<span>".get_nursing($key1)."</span>";	
  		// 		}
  				
  		// 	}
  			
	  	// 	$data['nursing_array'] = $nursing_pro;
  		// }
  		
  		// if(!empty($_POST['HealthProfessional']))
  		// {
  		// 	$health_pro = '';
  		// 	$health_array = $_POST['HealthProfessional'];
  		// 	foreach($health_array as $key => $health_a)
  		// 	{
  		// 		if($health_a)
  		// 		{
  		// 			$health_pro .= "<span>".get_health_professional($key)."</span>";
  		// 		}	
  		// 	}
  			
	  	// 	$data['health_professionals'] = $health_pro;

  		// }
  		$facility_days_name = array();
  		$facility_hours_all = array();
  		if(!empty($_POST['FacilityHours']))
  		{
  			$facility_hour_loop = $_POST['FacilityHours'];
  			if(!empty($facility_hour_loop))
  			{
  				foreach($facility_hour_loop as $facility_loop)
	  			{
	  				foreach($facility_loop['master_days_name'] as $facility_l)
	  				{
	  					$id_name = get_days_name($facility_l['id']);
	  					if (!in_array($id_name, $facility_days_name))
	  					{
	  						$facility_days_name[] = $id_name;
	  						$facility_hours_all[$id_name][] = 
	  						array(
									'start_time' =>$facility_loop['start_time'],
									'end_time'   => $facility_loop['end_time'],
								);
	  					}else
	  					{
	  						
	  						$facility_hours_all[$id_name][] = 
	  						array(					
									'start_time' =>$facility_loop['start_time'],
									'end_time'   => $facility_loop['end_time'],
								);
	  					}
	  				}
	  			}
  			}
  			$data['facility_days_name'] = $facility_days_name;
  			$data['facility_hours_all'] = $facility_hours_all;
  			
  		}
  		if(!empty($_POST['ProvinceName']))
  		{
  			
  			$province_name = get_province_name($_POST['ProvinceName']);
	  		$data['province_name'] = $province_name;
  		}
  		if(!empty($_POST['JobHours']))
  		{
  			
  			$hours = get_hours($_POST['JobHours']);
	  		$data['jobhours'] = $hours;
  		}
  		if(!empty($_POST['Compensation']))
  		{
  			
  			$compensation = get_compensation($_POST['Compensation']);
	  		$data['compensation'] = $compensation;
  		}
  		if(!empty($_POST['StartDate']))
  		{
  			
  			$start_date = date('d M Y',strtotime($_POST['StartDate']));
	  		$data['start_date'] = $start_date;
  		}
  		if(!empty($data)){
	  		$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
		}
		$this->api_response();
  	}

  	public function medical_change_password_post()
  	{

		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|callback_password_check_medical');
		$this->form_validation->set_rules('password', 'New Password', 'trim|required|matches[confirm_password]|min_length[6]');
		$this->form_validation->set_rules('confirm_password','Confirm Password', 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == TRUE){
			$salt = $this->salt();
			$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('password')))));
			$id = medicaluser_id();
			if($this->common_model->update('users',$user_data,array('user_id'=>$id)))
			{

				$this->session->set_flashdata('msg_success','Password updated successfully.');
			}
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Update successfully.";
			$this->api_response();
		} 
		else
		{
		 	
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->api_response();
		}

  	}

  	public function get_medical_profile_detail_get()
  	{
  		$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
  		$id = medicaluser_id();
  		$medical_profile_detail = $this->common_model->get_row('users',array('user_id'=>$id));
  		if(!empty($medical_profile_detail))
  		{
  			$data['user_detail'] = $medical_profile_detail;
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}
  		$this->api_response();
  	}

  	public function medical_profile_update_post()
	{
		$user_id = medicaluser_id();
		$this->form_validation->set_rules('user_name_title', 'Medical User title', 'required');
        $this->form_validation->set_rules('user_first_name', 'Medical User first name', 'required');
        $this->form_validation->set_rules('user_last_name', 'Medical User last name', 'required');
        
        $this->form_validation->set_rules('user_email', 'User Email', "trim|required|unique[users.user_email,users.user_id,$user_id]");

  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if ($this->form_validation->run() == TRUE)  
        {
  	    		$id = medicaluser_id();
			    $con = array('user_id'=> $id);
			    $user_data=array(
					'user_name_title'	=> $this->input->post('user_name_title'),
					'user_first_name'	=> trim($this->input->post('user_first_name')),
					'user_last_name'	=> trim($this->input->post('user_last_name')),
					'user_email'		=> trim($this->input->post('user_email')),
					'user_updated'		=> date('Y-m-d H:i:s A'),
					);
			$user_id=$this->common_model->update('users',$user_data,$con);
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data inserted successfully.";
			$this->response_array['error'] = '';
		} 
		else
		{
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			
		}
		$this->api_response();
	}

	public function physician_change_password_post()
  	{
		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|callback_password_check');
		$this->form_validation->set_rules('password', 'New Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirm_password','Confirm Password', 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == TRUE){
			$salt = $this->salt();
			$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('password')))));
			$id = physicain_id();
			if($this->common_model->update('users',$user_data,array('user_id'=>$id)))
			{

				$this->session->set_flashdata('msg_success','Password updated successfully.');
			}
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Update successfully.";
			$this->response_array['error'] = '';
			$this->api_response();
		} 
		else
		{
		 	
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->api_response();
		}

  	}
  	public function password_check($oldpassword){
		$user_info = $this->user_model->get_row('users',array('user_id'=>physicain_id()));
		$salt = $user_info->salt;
		if($this->common_model->password_check(array('password'=>sha1($salt.sha1($salt.sha1($oldpassword)))),physicain_id())){
		return TRUE;
		}else{
		$this->form_validation->set_message('password_check', 'The %s does not match.');
		return FALSE;
		}
	}
	public function password_check_medical($oldpassword){
		$user_info = $this->user_model->get_row('users',array('user_id'=>medicaluser_id()));
		$salt = $user_info->salt;
		if($this->common_model->password_check(array('password'=>sha1($salt.sha1($salt.sha1($oldpassword)))),medicaluser_id())){
		return TRUE;
		}else{
		$this->form_validation->set_message('password_check_medical', 'The %s does not match.');
		return FALSE;
		}
	}
  	public function priority_placement_content_get($job_id_value)
  	{
	 $this->response_array['response_code'] = 500;
	 $this->response_array['message'] = "Invalid Form Request";	
	 $job_id = base64_decode($job_id_value);
	   if($listing_detail=$this->common_model->get_row('post_jobs',array('id'=>$job_id,'user_id'=>medicaluser_id()),array('listing_title,job_verify'))){
	   		$city_subscription_id='';
	   		if($city_subscription	= $this->common_model->city_subscription($job_id)){
	   			foreach ($city_subscription as $value) {
	   				$city_subscription_id = $city_subscription_id."".$value->city_id.",";
	   			}
	   			$city_subscription_id = rtrim($city_subscription_id, ",");
	   		}
			$city_data			= $this->common_model->get_result_highlight($city_subscription_id);
			$othercity_data		= $this->common_model->get_result_city($city_subscription_id);

			$priority_placement	= $this->common_model->get_row('options',array('status'=>1,'id'=>30),'',array());
			$verified_facility	= $this->common_model->get_row('options',array('status'=>1,'id'=>31),'',array());
			$photograph_fee		= $this->common_model->get_row('options',array('status'=>1,'id'=>29),'',array());
			$total_val = '';
	  		if(!empty($city_data))
	  		{
		  		foreach ($city_data as $city_value)
		  		{
		  			$total_val = $city_value->price;
		  		}
		  	}
			if($this->session->userdata('check_post'))
			{
				$data['check_post_value']	= $this->session->userdata('check_post');
				$this->session->unset_userdata('check_post');
			}
			//$discount					= $this->common_model->get_row('options',array('status'=>1,'id'=>32));
			$listing_title				= $listing_detail->listing_title;
			$job_verify					= $listing_detail->job_verify;
			$discount_cal				= '';
			$data['job_verify']			= $job_verify;
			$data['listing_title']		= $listing_title;
			$data['discount_amount']	= $discount_cal;
			$data['city_data']			= $city_data;
			$data['othercity_data']		= $othercity_data;
			$data['total_price']		= $total_val;
			$data['priority_placement']	= $priority_placement;
			$data['verified_facility']	= $verified_facility;
			$data['photograph_fee']		= $photograph_fee;
			$data['encrypt_id']			= $job_id_value;
			$data['city_subscription']	= $city_subscription;

			$this->response_array['response_code']	= 200;				
			$this->response_array['message']		= "Data Get successfully .";
			$this->response_array['data']			= $data;		
	  		
  		} 
  		$this->api_response();
  	}

  	public function post_data_value_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
		$nursing_unserialize = '';
	    $id = base64_decode($_POST['Id']);	
	    $user_id = medicaluser_id();
  		$post_data_update = $this->common_model->get_row('post_jobs',array('id'=>$id,'user_id'=>$user_id));
		if(!empty($post_data_update))
		{
		//$nursing_unserialize = $post_data_update->nursing_array;
		//$nursing_array = unserialize($nursing_unserialize);
		//$health_array = unserialize($post_data_update->health_array);
		$FacilityHoursArray = unserialize($post_data_update->facility_hours_array);
		//$facility_hours = '';
		$uploadImageArray = '';
		//$post_data_update->nursing_array = $nursing_array;
		//$post_data_update->health_array = $health_array;
		//$post_data_update->uploadImageArray = $uploadImageArray;
		$post_data_update->facility_hours = $FacilityHoursArray;
		$imagesarray=array();
		if($post_data_update->uploadImageArray)
		{
			$uploadImageArray =unserialize($post_data_update->uploadImageArray);
			foreach ($uploadImageArray as $value)
		 	{
				if(file_exists('assets/uploads/jobs/'.$value['imageName']))
				{
					$image=getimagesize('assets/uploads/jobs/'.$value['imageName']);
					if($image){
					$imagesarray[]= array(
								'lastModifiedDate'	=> date('Y-m-d H:i:s'),
								'size'				=> filesize('assets/uploads/jobs/'.$value['imageName']),
								'type'				=> $image['mime'],
								'name'				=> $value['imageName'],
								'url'				=> $value['imageName'],
					         );
					}
				}	
			} 
		}
		$post_data_update->imagesarray 				= $imagesarray;
		$post_data_update->uploadImageArray 		= $uploadImageArray;
		$post_data_update->encrypt_id				= base64_encode($id);
		$post_data_update->facility_postal_code		= get_postalcode_id($post_data_update->facility_postal_code);
	  		
  			$data['post_data_update'] = $post_data_update;
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}
  		$this->api_response();
  	}
  	function post_update_post()
  	{
		$id = $_POST['id'];	
		$this->form_validation->set_rules('contact_name_title', 'Contact Person title', 'required');
        $this->form_validation->set_rules('contact_first_name', 'Contact Person first name', 'required');
        $this->form_validation->set_rules('contact_last_name', 'Contact Person last name', 'required');
        $this->form_validation->set_rules('contact_email_address', 'Contact Person Email', 'trim|required|valid_email'); 
        $this->form_validation->set_rules('contact_fax', 'Fax Number', 'min_length[8]|max_length[15]');
        $this->form_validation->set_rules('contact_phone_number', 'Facility Phone Number', 'required|min_length[8]|max_length[12]');  
        $this->form_validation->set_rules('medical_specialty_name', 'Medical Specialties', 'trim|required'); 
        $this->form_validation->set_rules('special_qualification', 'Special Qualification', 'required');
        $this->form_validation->set_rules('hour', 'Hours', 'required');
        $this->form_validation->set_rules('position', 'Position', 'trim|required');

        $this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');  
        $this->form_validation->set_rules('start_month', 'Start Month', 'required');
        $this->form_validation->set_rules('start_year', 'Start Year', 'trim|required');
         
     	$this->form_validation->set_rules('call_responsibility', 'Call Responsibility', 'trim|required');
        $this->form_validation->set_rules('facility_name', 'Facility Name', 'required');
        $this->form_validation->set_rules('facility_state', 'Province', 'required');
        $this->form_validation->set_rules('facility_city', 'City', 'required');
        $this->form_validation->set_rules('facility_address', 'Address', 'trim|required');  
        $this->form_validation->set_rules('facility_postal_code', 'Postal Code', 'required|max_length[7]');
         
        $this->form_validation->set_rules('facility_type', 'Facility Type', 'trim|required'); 

        $this->form_validation->set_rules('parking', 'Parking', 'required');
        $this->form_validation->set_rules('no_of_fulltime', 'Number of Fulltime', 'trim|required|numeric|max_length[6]');
        $this->form_validation->set_rules('part_time_doctor', 'Number of part-time doctors', 'trim|required|numeric|max_length[6]');
        $this->form_validation->set_rules('exam_room', 'Exam room', 'trim|required|numeric|max_length[6]');
     	$this->form_validation->set_rules('listing_title', 'List Title', 'trim|required');
     	$this->form_validation->set_rules('listing_description', 'List Description', 'trim|required');
     	
  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); 	   	
        if($this->form_validation->run() == TRUE)  
        {
			$data = array();
			$_POST['modified'] = date('Y-m-d H:i:s A');
			unset($_POST['id']);
			unset($_POST['created']);
			unset($_POST['user_id']);
			$encrypt_id = '';
			if($_POST['status'] == 4)
			{
				$_POST['status'] = 0;
				$encrypt_id = base64_encode($id);
			}
			// if(!empty($_POST['health_array']))
	  //   	{
	    		
	  //   		$_POST['health_array'] = serialize($_POST['health_array']);
	  //   	}else{
	  //   		$_POST['health_array']		= '';
	  //   	}
	  //   	if(!empty($_POST['nursing_array']))
	  //   	{
	  //   		$_POST['nursing_array'] = serialize($_POST['nursing_array']);
	  //   	}else{
	  //   		$_POST['nursing_array']		= '';
	  //   	}
	    	
	    	if(!empty($_POST['uploadImageArray']))
	    	{
	    		//echo '<pre>'; print_r($_POST['uploadImageArray']);
	    		$full_image = '';
	    		foreach($_POST['uploadImageArray'] as $image_name)
	    		{
		    		
		    		foreach($image_name as $img_name)
		    		{
		    			//echo $img_name;die;
			    		//exit;
			    		$existing_image_name = $img_name;
					    $title_name = $_POST['listing_title'];
					    $list_name = url_title($title_name);
					    $extension = explode('.', $existing_image_name);
					    $count_exe = count($extension)-1;
					    $new_image_name= $list_name.'_'.strtotime("now").'_'.rand(1000,10000).'.'.$extension[$count_exe];
					    $old_image_path = './assets/uploads/jobs/'.$existing_image_name;
					    $new_image_path = './assets/uploads/jobs/'.$new_image_name;
					    $thumb_old_image_path = './assets/uploads/jobs/thumbs/'.$existing_image_name;
					    $thumb_new_image_path = './assets/uploads/jobs/thumbs/'.$new_image_name;
				    	if(file_exists($old_image_path))
				    	{
				    		rename($old_image_path , $new_image_path);
				    	}
				    	if(file_exists($thumb_old_image_path))
				    	{
				    		rename($thumb_old_image_path , $thumb_new_image_path);
				    	}
				    	$full_image[] = array('imageName' =>$new_image_name); 
				    }	
			    }
	    		$_POST['uploadImageArray'] =  serialize($full_image);
	    	}else{
	    		$_POST['uploadImageArray'] = '';
	    	}

	    	if(!empty($_POST['facility_hours']))
  	    	{
  	    		$_POST['facility_hours_array'] =  serialize($_POST['facility_hours']);
  	    	}else{
  	    		$_POST['facility_hours_array'] = '';
  	    	}
  	    	if(empty($_POST['transport_train']))
  	    	{
  	    		$_POST['transport_train_value'] = '';
  	    	}elseif($_POST['transport_train'] == false)
  	    	{
  	    		$_POST['transport_train_value'] = '';
  	    	}
  	    	if(empty($_POST['transport_metro_subway']))
  	    	{
  	    		unset($_POST['transport_m_s_value']);
  	    	}elseif($_POST['transport_metro_subway'] == false)
  	    	{
  	    		$_POST['transport_m_s_value'] = '';
  	    	}
  	    	if(empty($_POST['transport_bus']))
  	    	{
  	    		unset($_POST['transport_bus_value']);
  	    	}elseif($_POST['transport_bus'] == false)
  	    	{
  	    		$_POST['transport_bus_value'] = '';
  	    	}
  	    	if(empty($_POST['transport_other']))
  	    	{
  	    		unset($_POST['transport_other_value']);
  	    	}elseif($_POST['transport_other'] == false)
  	    	{
  	    		$_POST['transport_other_value'] = '';
  	    	}
  	    	$facility_hours_loop = $_POST['facility_hours'];
  	    	unset($_POST['encrypt_id']);
  	    	unset($_POST['start_time']);
  	    	unset($_POST['end_time']);
  	    	unset($_POST['facility_hours']);
  	    	unset($_POST['imagesarray']);
			$update = $this->common_model->update('post_jobs',$_POST,array('id'=>$id));
			//echo $encrypt_id;
			$data['encrypt_id']  = $encrypt_id;
			//$this->db->last_query();
			if($update)
			{
				$this->response_array['response_code'] = 200;			
				$this->response_array['message'] = "Data Get successfully .";
				if($data['encrypt_id']){
					$this->response_array['data'] = $data;	
				}

			}else{
				$this->response_array['response_code'] = 500;
				$this->response_array['message'] = "Invalid Form Request";	
				$this->response_array['error'] = $this->form_validation->error_array() ;
			}  	
		}else
		{
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			
		}
		$this->api_response();		
	}


    public function calculate_placements_post($jobId_value='')
  	{
		//echo $discount->option_value;
		$jobId									= base64_decode($jobId_value);
		$this->response_array['response_code']	= 500;
		$this->response_array['message']		= "Invalid Form Request";
		$discount_cal							= '';
		//$discount								= $this->common_model->get_row('options',array('status'=>1,'id'=>32));
		$yolomd_verifyfee						= $this->common_model->get_row('options',array('status'=>1,'id'=>29));
		//$discount_cal							= $discount->option_value;
		$city_ids								= $_POST['placement_array'];
		$selected_Tags							= $_POST['selectedTags'];
		$yolomd_verify							= $_POST['yolomd_verify'];
		$placement_city							= array();
		$grand_total							= 0;
		$yolomd_verify_fee						= 0;
		$discount								= 0;
		$gross_total							= 0;
		$city_total								= 0;
		$percentage_disount						= $discount_cal;
		$job_id									= $jobId;
		$listing_detail=$this->common_model->get_row('post_jobs',array('id'=>$job_id,'user_id'=>medicaluser_id()),array('listing_title,job_verify'));
		if($city_ids || $selected_Tags	){
			if($city_ids){
				foreach ($city_ids as $key =>$value)
				 {
					
					if($value)
					{
						
						if($city_data = $this->common_model->get_row('location_data',array('id'=>$key))){
						  $placement_city[] = array(
						                            'city_name'=>$city_data->city_name,
						                            'city_id'=>$city_data->id,
						                            'city_placement_charge'=>$city_data->price
						                            );
						  $city_total = $city_data->price + $city_total;
					  }
				   }
			 	}
			 }
			if($selected_Tags)
			{
					
					foreach ($selected_Tags as $key =>$value)
					{
							if($selected_Tags[$key]['id'] && $city_data = $this->common_model->get_row('location_data',array('id'=>$selected_Tags[$key]['id'])))
							{
							  $placement_city[] = array(
							                            'city_name'=>$city_data->city_name,
							                            'city_id'=>$city_data->id,
							                            'city_placement_charge'=>$city_data->price
							                            );
							  $city_total = $city_data->price + $city_total;
							}
				 	}
			}
		}
  		
  		if($_POST['yolomd_verify']){
  			$yolomd_verify_fee	= $yolomd_verifyfee->option_value;
  			$yolomd_verify_fee= $yolomd_verify_fee;
  		}

  		$grand_total = $city_total + $yolomd_verify_fee;
  		if( $city_total && ($yolomd_verify_fee ||  $listing_detail->job_verify) && $percentage_disount){
  			$discount = ($grand_total/100) * $percentage_disount;
  		}
  		$gross_total = $grand_total - $discount;
  		$data=array(
  		            	'job_id'			=> $job_id,
						'placement_city'	=> $placement_city,
						'yolomd_verify_fee'	=> $yolomd_verify_fee,
						'grand_total'	    => $grand_total,
						'discount'			=> $discount,
						'gross_total'	    => $gross_total,
  		            );
  		$this->session->set_userdata('paydata',$data);
		$this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data Get successfully.";
		$this->response_array['data'] = $data;		
		$this->api_response();
  	}
  	function changestatus_joblisting_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$status_value = $_POST['status_value'];
  		$post_id = $_POST['post_id'];
  		$user_id = medicaluser_id();
  		$update_data = array('status'=>$status_value);
  		if($this->common_model->update('post_jobs',$update_data,array('user_id'=>$user_id,'id'=>$post_id)))
		{
			

			$get_info_user = get_user_info($user_id);
            $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
			$email_template = $this->common_model->get_row('email_templates',array('id'=>4));
			$status = '';
	    	$status = get_job_status($status_value) ;
	    	$image_url = base_url().'assets/front/images/click-here-7.png';
	    	$param=array
	    		(
					
					
					'template'  =>  
					array(
				    		'temp'  =>  $email_template->template_body,
			        		'var_name'  =>  
			        					array(
		  										'username'    => $username,
		  										'site_url' => site_url(),
		  										'job_id'    => $post_id,
                                                'job_name'  => get_job_name($post_id),
												'status' => $status,
												'image_url'=> $image_url
											),   
					),   
					'email' =>  
				    array(
				            'to'        =>   $get_info_user->user_email, 
				            'from'      =>   GLOBAl_INFO_EMAIL,
				            'from_name' =>   SITE_NAME,
				            'subject'   =>   $email_template->template_subject,
				     ),
				);   
	        $status=$this->chapter247_email->send_mail($param); 
			$this->session->set_flashdata('msg_success','Status updated successfully.');
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			
		}	
		$this->api_response();
  	}
  	function delete_joblisting_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$post_id = $_POST['post_id'];
  		$id = medicaluser_id();
  		if($this->common_model->delete('post_jobs',array('user_id'=>$id,'id'=>$post_id)))
		{

			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Delete successfully .";
		}
		$this->api_response();	
  	}

   	public function jobs_post($job_id = '')
  	{
		$canada_province	= trim($_POST['canada_province']);
		$medical_specialty	= trim($_POST['medical_specialty']);

  		$this->response_array['data'] = 
		array(
			'jobs_data'		=> '',
     	);	
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
		$limit				= 1000;
		$offset				= $_POST['offset'];
		$Position			= trim($_POST['Position']);
		$city_postal_code	= trim(ucfirst($_POST['citypostalcode']));

		$sorting			= trim($_POST['sorting']);	
		$city_jobs_id		= '';
		$city_jobs			= array();
		//get all job id by city or postal code
		$city_id_array = array();
		$varible_to_zoom='';
		$latf='';
	  	$lngf='';
	  	$job_id='';

		$lat = '';
		$long = '';
		if(!empty($city_postal_code))
	    {
		    $address = '';
		    if(!empty($city_postal_code))
		    {
		       $address .= $city_postal_code.',';
		    }
	        $address .= 'canada';
        
            $prepAddr = str_replace(' ','+',$address);

            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');

            $output= json_decode($geocode);

            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;        
	    }

        $get_jobs= $this->common_model->get_jobs_id($city_jobs_id, $canada_province, $medical_specialty, $Position, $city_postal_code,$lat,$long);

        $main_job_id_array = array();
		if($get_jobs){
			$main_job_id_array = $get_jobs;
		}

		
		if($main_job_id_array){
			$get_jobs_id = '';
			foreach ($main_job_id_array as $value) {
   				$get_jobs_id=$get_jobs_id."".$value->job_id.",";
   			}
   			$get_jobs_id = rtrim($get_jobs_id, ",");
			//get detail of jobs
  			$jobs =$this->common_model->jobs($get_jobs_id,$limit,$offset);
  			
  			$is_load_more = FALSE;
  			$count_job_id = count(explode(',',$get_jobs_id));
   			$count_view_job = '';
   			$count_view_job = $offset;
   			if($jobs){
   				$count_view_job = $count_view_job + $limit;
   			}
   			if($count_job_id > $count_view_job ){
   				$is_load_more = TRUE;
   			}

  			//echo $this->db->last_query();
  			$jobs_data = array();
  			if(!empty($jobs))
  			{
	  			foreach($jobs as $value)
		  		{
	  				$jobs_image = array();
	  				$post_image  = array();
		  			if($value->uploadImageArray)
		  			{
		  				$jobs_image = unserialize($value->uploadImageArray);
		  				if(!empty($jobs_image))
			  			{
			  				foreach($jobs_image as $image)
			  				{
			  					if(file_exists('assets/uploads/jobs/thumbs/'.$image['imageName']))
			  						$post_image[] = site_url().'assets/uploads/jobs/thumbs/'.$image['imageName'];
			  				}
			  				
			  			}
		  			}
		  			$state = $value->facility_state;
		  			/*if( $stateid){
		  				$state = $this->common_model->get_row('location_complete_data',array('id'=>$stateid),array('province_name'));
	  				    $state = $state->province_name;
	  				}*/
		  			$icon='markerIcon.svg';
		  			$user_data=array(
							'job_id'	=> $value->id,	
							'user_id'   => physicain_id()
					);
					$count = '';
	    			
	    			if( $this->common_model->get_result('favourite_job',$user_data))
	    			{
	    				$count = 1;
	    			}
	  				$jobs_data[] = (object)array(
						'job_id'			=> $value->id,
						'listing_title'		=> $value->listing_title,
						'job_verify'		=> $value->job_verify,
						'jobs_image'		=> $post_image,
						'facility_state'	=> $state,
						'facility_city'		=> $value->facility_city,
						'latitude'			=> $value->latitude,
						'longitude'			=> $value->longitude,
						'icon'				=> $icon,
						'row_favourite'		=> $count,
						'is_load_more'		=> $is_load_more,
						'facility_postal_code' => $value->facility_postal_code,
						'encrpt_id'			=> base64_encode($value->id)
					); 
	  			}
  				$i=0;
  				$k=0;
  				$z=0;
  				foreach($jobs_data as $jobs_det)
  				{
  					if($i==0){
  						$varible_to_zoom=10;
  						$latf=$jobs_det->latitude;
  						$lngf=$jobs_det->longitude;
  						$job_id=$jobs_det->job_id;
  					}
  					if($canada_province != 'All' ||  $city_postal_code){
  						if($canada_province != 'All' &&  $canada_province==$jobs_det->facility_state && !empty($city_postal_code) && ($jobs_det->facility_city == $city_postal_code || $jobs_det->facility_postal_code == $city_postal_code))
	  					{
	  						$varible_to_zoom=13;
	  						$latf=$jobs_det->latitude;
	  						$lngf=$jobs_det->longitude;
	  						$job_id=$jobs_det->job_id;
	  						$z++;
	  						$i++;
	  						break;
	  					}
	  					elseif(!empty($city_postal_code) && ($jobs_det->facility_city == $city_postal_code || $jobs_det->facility_postal_code == $city_postal_code) && $z==0){
	  						$varible_to_zoom=13;
	  						$latf=$jobs_det->latitude;
	  						$lngf=$jobs_det->longitude;
	  						$job_id=$jobs_det->job_id;
	  						$k++;
	  						$i++;
	  						break;
	  					}
	  					elseif($canada_province==$jobs_det->facility_state && $k==0 && $z==0 && $i==0)
	  					{
	  						$varible_to_zoom=12;
	  						$latf=$jobs_det->latitude;
	  						$lngf=$jobs_det->longitude;
	  						$job_id=$jobs_det->job_id;
	  						$i++;
	  					}
  					}
	  				else{
	  					break;
	  				}
  				}
  			}
  			
  			$check_user_login_value = 3;
  			
  			if(medicaluser_logged_in())
  			{
  				$check_user_login_value = 1;
  			}
  			if(physicain_id())
  			{
  				$check_user_login_value = 2;
  			}
  			$count_job = 0;
  			if(!empty($jobs_data))
  			{
  				$count_job = count($jobs_data);
  			}
  			$this->response_array['data'] = 
  			array(
				'is_load_more'			 => $is_load_more,
				'offset'		         => $offset + count($jobs),
				'jobs_data'				 => $jobs_data,
				'check_user_login_value' => $check_user_login_value,
				'varible_to_zoom' 		 => $varible_to_zoom,
				'latf' 					 => $latf,
				'lngf' 					 => $lngf,
				'job_id' 				 => $canada_province.' - '.$job_id,
				'job_count'              => $count_job
				//'jobidarray'			 => $jobidarray

             );	
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data received successfully .";
		}
		$this->api_response();
	}

	public function job_detail_post($job_id='')
  	{
		$job_id_value = '';
		$job_id_value = $this->uri->segment(3);
		$job_id = base64_decode($job_id_value);
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
		$data = array();

  		if($jobs_data = $this->common_model->get_row('post_jobs',array('id'=>$job_id,'status'=>1)))
  		{
			$row_count_payment = '';
			$login_user = '';
			if(physician_logged_in() == TRUE)
			{
				$login_user = physicain_id();
			}
			if(medicaluser_logged_in() == TRUE)
			{
				$login_user = medicaluser_id();
			}
			//if($row_count_payment = $this->common_model->check_payment_status($job_id))
			//{
				
				$row_get_statistics = '';	
				$find_id = $_SERVER['REMOTE_ADDR'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_USER_AGENT'];
				$row_get_statistics = $this->common_model->count_rows('statistics_job', array('job_id'=>$job_id,'ip_address' => md5($find_id)),'','');
				//if($row_get_statistics)
				if($row_get_statistics < 1)
				{
					$this->common_model->insert('statistics_job', array('job_id'=>$job_id, 'ip_address' => md5($find_id),'user_id' => $login_user));

				}
			//}
			
			$facility_hours_array	= '';
			$facility_hours_all		= '';
			$jobs_image				= '';
			$post_images				= '';
			$facility_hours_array	= '';
			$facility_days_name 	= array();
  			/*if($jobs_data->uploadImageArray)
  			{
  				$jobs_image = unserialize($jobs_data->uploadImageArray);
  			}*/
  			if($jobs_data->uploadImageArray)
  			{
  				$jobs_image = unserialize($jobs_data->uploadImageArray);
  				if(!empty($jobs_image))
	  			{
	  				foreach($jobs_image as $image)
	  				{
	  					if(file_exists('assets/uploads/jobs/thumbs/'.$image['imageName']))
	  						$post_images[] = $image;
	  				}
	  				
	  			}
  			}
  			if($jobs_data->facility_hours_array)
  			{
  				$facility_hours_array = unserialize($jobs_data->facility_hours_array);
  			}

  			$facility_days_name = array();
	  		if(!empty($facility_hours_array))
	  		{
	  			$facility_hour_loop = $facility_hours_array;
	  			if(!empty($facility_hour_loop))
	  			{
	  				foreach($facility_hour_loop as $facility_loop)
		  			{
		  				foreach($facility_loop['master_days_name'] as $facility_l)
		  				{
		  					$id_name = get_days_name($facility_l['id']);
		  					if (!in_array($id_name, $facility_days_name))
		  					{
		  						$facility_days_name[] = $id_name;
		  						$facility_hours_all[$id_name][] = 
		  						array(
										'start_time' =>$facility_loop['start_time'],
										'end_time'   => $facility_loop['end_time'],
									);
		  					}else
		  					{
		  						
		  						$facility_hours_all[$id_name][] = 
		  						array(					
										'start_time' =>$facility_loop['start_time'],
										'end_time'   => $facility_loop['end_time'],
									);
		  					}
		  				}
		  			}
	  			}
	  		}
		  	// if(!empty($jobs_data->nursing_array))
	  		// {
	  		// 	$nursing_pro = '';
	  		// 	$nursing_array = unserialize($jobs_data->nursing_array);

	  		// 	foreach($nursing_array as $key1 =>$nursing_a)
	  		// 	{
	  		// 		if($nursing_a)
	  		// 		{
	  		// 			$nursing_pro .= "<span>".get_nursing($key1)."</span>";	
	  		// 		}
	  				
	  		// 	}
	  			
		  	// 	$data['nursing_array'] = $nursing_pro;
	  		// }
	  		
	  		// if(!empty($jobs_data->health_array))
	  		// {
	  		// 	$health_pro = '';
	  		// 	$health_array = unserialize($jobs_data->health_array);
	  		// 	foreach($health_array as $key => $health_a)
	  		// 	{
	  		// 		if($health_a)
	  		// 		{
	  		// 			$health_pro .= "<span>".get_health_professional($key)."</span>";
	  		// 		}	
	  		// 	}
	  			
		  	// 	$data['health_professionals'] = $health_pro;

	  		// }
		    if(!empty($jobs_data->hour))
		    {
		        
		        $hours = get_hours($jobs_data->hour);
		        $data['jobhours'] = $hours;
		    }
		    if(!empty($jobs_data->compensation))
		    {
		        
		        $compensation = get_compensation($jobs_data->compensation);
		        $data['compensation'] = $compensation;
		    }
		    if(!empty($jobs_data->start_date) && !empty($jobs_data->start_month) && !empty($jobs_data->start_year))
		    {
		        //$t_date = ;
		        $mon=['January','February','March','April','May','June','July','August','September','October','November','December'];	        
    			$data['start_date'] = $jobs_data->start_date.' '.$mon[($jobs_data->start_month)-1].' '.$jobs_data->start_year;
		    }

		    if(!empty($jobs_data->medical_specialty_name))
		    {
		        $medical_spec_name = get_medical_specialty($jobs_data->medical_specialty_name);
		        $data['medical_spec_name'] = $medical_spec_name;
		        $medical_spec_logo = get_medical_specialty_logo($jobs_data->medical_specialty_name);
		        $logo_image = '';
		        if(!empty($medical_spec_logo))
		        {
			        if(file_exists('assets/uploads/logo/'.$medical_spec_logo))
				    {
			  			$logo_image = site_url().'assets/uploads/logo/'.$medical_spec_logo;
			  			
			  		}else{
		  				$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
		  			}	
			  	}else{
		  			$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
		  		}	
		  		$data['medical_spec_logo'] = $logo_image;
		        
		    }
		    if(!empty($jobs_data->facility_type))
		    {
		        $facility_type = get_facility_type($jobs_data->facility_type);
		        $data['facility_type'] = $facility_type;
		    }
		     $provience='';
		    if(!empty($jobs_data->facility_state))
		    {
		        $provience=$jobs_data->facility_state;
		        $province_name = get_province_name($provience);
		        $data['province_name'] = $province_name;
		    }
		    $allJobs='';
		    $jobs_lat_lng =$this->common_model->alljobs($jobs_data->latitude,$jobs_data->longitude);
	  		{
	  			foreach($jobs_lat_lng as $value)
		  		{
		  			$active=0;
		  			if($value->id == $job_id){
		  				$active=1;
		  				$icon='active_job.svg';
		  			}else{
		  				$icon='markerIcon.svg';
		  			}
	  				$allJobs[] = (object)array(
							'job_id'			=> $value->id,
							'listing_title'		=> $value->listing_title,
							'facility_postal_code'=> $value->facility_postal_code,
							'facility_city'		=> $value->facility_city,
							'latitude'			=> $value->latitude,
							'longitude'			=> $value->longitude,
							'icon'				=> $icon,
							'active'			=> $active
							); 
	  			}
			}
			
			$user_data=array(
						'job_id'	=> $jobs_data->id,	
						'user_id'   => physicain_id()
			);
			$mark_fav = '';
    		$mark_fav = $this->common_model->get_count('favourite_job',$user_data);
    		$check_user_login_val = 3;
  			
  			if(medicaluser_logged_in())
  			{
  				$check_user_login_val = 1;
  			}
  			if(physicain_id())
  			{
  				$check_user_login_val = 2;
  			}
  			$post_image = '';
  			if($jobs_data->uploadImageArray)
  			{
  				
  				$image_new = unserialize($jobs_data->uploadImageArray);
  				if(file_exists('assets/uploads/jobs/'.$image_new[0]['imageName']))
			    {
	  				if(!empty($image_new))
		  			{
		  				
		  				$post_image = site_url().'assets/uploads/jobs/'.$image_new[0]['imageName'];
		  			}
		  		}	
  			}
  			$site_url = site_url().'jobs/'.$jobs_data->id;
  			
  			if(!empty($post_image))
  			{
  				$this->session->set_userdata('meta_image',$post_image);
  			}
  			if(!empty($site_url))
  			{
  				$this->session->set_userdata('meta_url',$site_url);
  			}
  			if(!empty($jobs_data->listing_description))
  			{
  				$this->session->set_userdata('meta_description',$jobs_data->listing_description);
  			}
  			if(!empty($jobs_data->listing_title))
  			{
  				$this->session->set_userdata('meta_title',$jobs_data->listing_title);
  			}
  			$jobs_data->encrpt_id = base64_encode($jobs_data->id); 
  			$data['check_user_login_val'] 			= $check_user_login_val;
			$data['marked_favourite']				= $mark_fav;
			$data['allJobs']						= $allJobs;	
			$data['jobs_data']						= $jobs_data;
			$data['jobs_image']						= $post_images;
			$data['facility_days_name']				= $facility_days_name;
			$data['facility_hours_all']				= $facility_hours_all;
			$data['physicain_detail_info']			= get_physicain_detail_info();
			$this->response_array['response_code']	= 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;	
		}
		$this->api_response();
	}
	public function receipts_get()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$receipts_information = $this->common_model->receipts_information();
  		if($this->session->userdata('payment_message'))
  		{
  			$data['payment_message'] = 'Payment has been Successfully Completed.';
  			$this->session->unset_userdata('payment_message');
  		}else{
  			$data['payment_message'] = '';
  		}
  		if(!empty($receipts_information))
  		{
  			$data['receipts_information'] = $receipts_information;
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}
  		$this->api_response();
  	}

  	public function searching_content_get(){
  		$medical_specialty = $this->common_model->get_result('medical_specialties_annex2',array('status'=>1),array('id','name'),array('order_by','asc'));
    	$canada_province =$this->common_model->get_province('location_complete_data',array(),array('province_name'),array('province_name','asc'),'province_name');
    	$canada_city =$this->common_model->get_city('post_jobs',array('status'=>1),array('facility_city'),array('facility_city','asc'),'facility_city');
    	$default_msg='';
    	$selectState='';
    	$selectSpecialty='';
    	if(physicain_id()){
    		if($physicain = $this->common_model->get_row('physicians',array('user_id'=>physicain_id())))
    		{
    			$selectSpecialty = $physicain->medical_selectSpecialty;
    			$selectState = $physicain->physician_selectState;
    			$default_msg = $physicain->physician_default_msg;
    		}
    	}

    	$data=array(
			'medical_specialty'	=>$medical_specialty,
			'canada_province'	=>$canada_province,
			'selectSpecialty'	=>$selectSpecialty,
			'selectState'		=>$selectState,
			'default_msg'		=>$default_msg,
			'canada_city'		=>$canada_city,
		);
    	$this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data show successfully.";
		$this->response_array['data'] = $data;	
		$this->api_response();
  	}
  	function receipts_id_post()
  	{

  		$id = $_POST['payment_id'];
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		if(!empty($id))
  		{
  			$pay_unq = '';
  			$jobs_detail =$this->common_model->receipts_user_id($id
  				);
  			//echo $this->db->last_query();
  			if(!empty($jobs_detail))
  			{

	  			$pay_unq = $jobs_detail['unique_id'];
	  			$city_detail =$this->common_model->city_payment($pay_unq
	  				);
	  			if(!empty($city_detail))
	  			{
	  				$data['city_detail'] = $city_detail;
	  			}
	  			if($jobs_detail['job_verify'] == 1)
	  			{
	  				$ver = 'Yes';
	  			}else{
	  				$ver = 'No';
	  			}
	  			$y_amount = 0;
	  			if(!empty($jobs_detail['yolomd_verify']))
	  			{
	  				$y_amount = $jobs_detail['yolomd_verify'];
	  			}
	  			$date_created = date('d M y',strtotime($jobs_detail['date_time']));
	  			$paid_amount  = $jobs_detail['payment_amount'];
	  			$data['jobs_detail'] = array(
						'item'				=> ucfirst($jobs_detail['item']),
						'job_id'			=> $jobs_detail['id'],
						'job_title'			=> $jobs_detail['listing_title'],
						'verify'			=> $ver,
						'payment_amount'	=> '$'.$jobs_detail['payment_amount'],
						'yolomd_verify'		=> '$'.$jobs_detail['yolomd_verify'],
						'date'				=> $date_created,
						'transaction_id'	=> $jobs_detail['txn_id'],
						'paid_amount'		=> '$'.number_format($paid_amount,2),
						'unique_id'			=> $id,
						'encrypt_id'		=> base64_encode($jobs_detail['id']),

				);

	  			if(!empty($data))
	  			{
	  				$this->response_array['response_code'] = 200;			
					$this->response_array['message'] = "Data show successfully.";
					$this->response_array['data'] = $data;	
	  			}
	  		}
  		}
  		$this->api_response();

  	}
  	public function faq_content_get(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$faq = $this->common_model->get_result('faq',array('status'=>1),'',array('order_by','asc'));
    	
    	$data=array(
			'faq'	=>$faq,
			
    		);
    	if(!empty($data))
  		{
	    	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}
		$this->api_response();	
  	}
  	
  	public function aboutus_get(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$content = $this->common_model->get_row('create_page',array('id'=>1));
    	
    	$data=array(
			'content_aboutus'	=>$content,
			
    		);
    	if(!empty($data))
  		{
	    	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}
		$this->api_response();	
  	}
  	
  	public function howwork_get(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$content = $this->common_model->get_row('create_page',array('id'=>2));
    	
    	$data=array(
			'content_howwork'	=>$content,
			
    		);
    	if(!empty($data))
  		{
	    	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}
		$this->api_response();	
  	}
  	public function termcondition_get(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$content = $this->common_model->get_row('create_page',array('id'=>3));
    	
    	$data=array(
			'content_termcondition'	=>$content,
			
    		);
    	if(!empty($data))
  		{
	    	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}
		$this->api_response();	
  	}
  	public function privatepolicy_get(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$content = $this->common_model->get_row('create_page',array('id'=>4));
    	
    	$data=array(
			'content_privatepolicy'	=>$content,
			
    		);
    	if(!empty($data))
  		{
	    	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}
		$this->api_response();	
  	}
  	public function photographypage_get(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
  		$content = $this->common_model->get_row('create_page',array('id'=>5));
    	
    	$data=array(
			'content_photographypage'	=>$content,
			
    		);
    	if(!empty($data))
  		{
	    	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}
		$this->api_response();	
  	}
  	public function mark_favourite_post()
	{
	    $user_data=array(
			'job_id'	=> $this->input->post('job_id'),
			'user_id'	=> physicain_id(),
			
		);
		$data['row_favourite'] = '';
		$data = array();
		$res = $this->common_model->insert('favourite_job',$user_data);
		$mark_fav = $this->common_model->get_count('favourite_job',$user_data);
		
		if(!empty($mark_fav)){
			$data['row_favourite'] = $mark_fav;
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data inserted successfully.";
			$this->response_array['data'] = $data;			
		} 
		else
		{
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
		
		}
		$this->api_response();
	}
	public function saved_search_post()
	{
		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
	    if(physicain_id())
	    {
		    $position = 'Both';
		    if($this->input->post('Position'))
		    {
		    	$position = $this->input->post('Position');
		    }
		    $user_data=array(
				'medical_specialties_id'	=> $this->input->post('medical_specialty'),
				'user_id'	=>  physicain_id(),
				'provice_id'	=> $this->input->post('canada_province'),
				'city_postal_code'	=> $this->input->post('citypostalcode'),
				'position' => $position,
				
			);
			$data = array();
			$data['saved_msg'] = '';
			if($this->common_model->get_count('saved_search',$user_data))
			{
				$data['saved_msg'] = 1;
				$this->response_array['response_code'] = 200;		
				$this->response_array['message'] = "Data inserted successfully.";
				$this->response_array['data'] = $data;	
			}else{
				$res = $this->common_model->insert('saved_search',$user_data);
				if($res){
					$this->response_array['response_code'] = 200;	
					$this->response_array['message'] = "Data inserted successfully.";
					$this->response_array['data'] = $data;			
				} 
			}		
		}
		$this->api_response();
	}
	public function job_preview_post($job_id_value='')
  	{
  		$job_id = base64_decode($job_id_value);
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
		$data = array();
  		if($jobs_data =$this->common_model->get_row('post_jobs',array('id'=>$job_id)))
  		{
			$facility_hours_array	= '';
			$facility_hours_all		= '';
			$jobs_image				= '';
			$post_images				= '';
			$facility_hours_array	= '';
			$facility_days_name 	= array();
  			/*if($jobs_data->uploadImageArray)
  			{
  				$jobs_image = unserialize($jobs_data->uploadImageArray);
  			}*/
  			if($jobs_data->uploadImageArray)
  			{
  				$jobs_image = unserialize($jobs_data->uploadImageArray);
  				if(!empty($jobs_image))
	  			{
	  				foreach($jobs_image as $image)
	  				{
	  					if(file_exists('assets/uploads/jobs/thumbs/'.$image['imageName']))
	  						$post_images[] = $image;
	  				}
	  				
	  			}
  			}
  			if($jobs_data->facility_hours_array)
  			{
  				$facility_hours_array = unserialize($jobs_data->facility_hours_array);
  			}

  			$facility_days_name = array();
	  		if(!empty($facility_hours_array))
	  		{
	  			$facility_hour_loop = $facility_hours_array;
	  			if(!empty($facility_hour_loop))
	  			{
	  				foreach($facility_hour_loop as $facility_loop)
		  			{
		  				foreach($facility_loop['master_days_name'] as $facility_l)
		  				{
		  					$id_name = get_days_name($facility_l['id']);
		  					if (!in_array($id_name, $facility_days_name))
		  					{
		  						$facility_days_name[] = $id_name;
		  						$facility_hours_all[$id_name][] = 
		  						array(
										'start_time' =>$facility_loop['start_time'],
										'end_time'   => $facility_loop['end_time'],
									);
		  					}else
		  					{
		  						
		  						$facility_hours_all[$id_name][] = 
		  						array(					
										'start_time' =>$facility_loop['start_time'],
										'end_time'   => $facility_loop['end_time'],
									);
		  					}
		  				}
		  			}
	  			}
	  		}
		  	// if(!empty($jobs_data->nursing_array))
	  		// {
	  		// 	$nursing_pro = '';
	  		// 	$nursing_array = unserialize($jobs_data->nursing_array);

	  		// 	foreach($nursing_array as $key1 =>$nursing_a)
	  		// 	{
	  		// 		if($nursing_a)
	  		// 		{
	  		// 			$nursing_pro .= "<span>".get_nursing($key1)."</span>";	
	  		// 		}
	  				
	  		// 	}
	  			
		  	// 	$data['nursing_array'] = $nursing_pro;
	  		// }
	  		
	  		// if(!empty($jobs_data->health_array))
	  		// {
	  		// 	$health_pro = '';
	  		// 	$health_array = unserialize($jobs_data->health_array);
	  		// 	foreach($health_array as $key => $health_a)
	  		// 	{
	  		// 		if($health_a)
	  		// 		{
	  		// 			$health_pro .= "<span>".get_health_professional($key)."</span>";
	  		// 		}	
	  		// 	}
	  			
		  	// 	$data['health_professionals'] = $health_pro;

	  		// }
		    if(!empty($jobs_data->hour))
		    {
		        
		        $hours = get_hours($jobs_data->hour);
		        $data['jobhours'] = $hours;
		    }
		    if(!empty($jobs_data->compensation))
		    {
		        
		        $compensation = get_compensation($jobs_data->compensation);
		        $data['compensation'] = $compensation;
		    }

		    if(!empty($jobs_data->start_date) && !empty($jobs_data->start_month) && !empty($jobs_data->start_year))
		    {
		        $mon=['January','February','March','April','May','June','July','August','September','October','November','December'];	        
    			$data['start_date'] = $jobs_data->start_date.' '.$mon[($jobs_data->start_month)-1].' '.$jobs_data->start_year;
		    }

		    if(!empty($jobs_data->medical_specialty_name))
		    {
		        $medical_spec_name = get_medical_specialty($jobs_data->medical_specialty_name);
		        $data['medical_spec_name'] = $medical_spec_name;
		        $medical_spec_logo = get_medical_specialty_logo($jobs_data->medical_specialty_name);
		        $logo_image = '';
		        if(!empty($medical_spec_logo))
		        {
			        if(file_exists('assets/uploads/logo/'.$medical_spec_logo))
				    {
			  			$logo_image = site_url().'assets/uploads/logo/'.$medical_spec_logo;
			  			
			  		}else{
		  				$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
		  			}	
			  	}else{
		  			$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
		  		}	
		  		$data['medical_spec_logo'] = $logo_image;
		        
		    }
		    if(!empty($jobs_data->facility_type))
		    {
		        $facility_type = get_facility_type($jobs_data->facility_type);
		        $data['facility_type'] = $facility_type;
		    }
		    if(!empty($jobs_data->facility_state))
		    {
		        $provience_id=$jobs_data->facility_state;
		        $province_name = get_province_name($provience_id);
		        $data['province_name'] = $province_name;
		    }
		    $allJobs='';
		    $receipt_unique_id = '';
			if($this->session->userdata('payment_message'))
	  		{
	  			$data['payment_message'] = 'Payment has been Successfully Completed.';
	  			$receipt_unique_id       = $this->session->userdata('payment_message');
	  			$this->session->unset_userdata('payment_message');
	  		}else{
	  			$data['payment_message'] = '';
	  		}
	  		$data['receipt_unique_id']				= $receipt_unique_id;
			$data['allJobs']						= $allJobs;	
			$data['jobs_data']						= $jobs_data;
			$data['jobs_image']						= $post_images;
			$data['facility_days_name']				= $facility_days_name;
			$data['facility_hours_all']				= $facility_hours_all;
			$data['status']							= $jobs_data->status;
			$this->response_array['response_code']	= 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;	
		}
		$this->api_response();
	}
	
	public function favourite_job_get()
  	{
  		$user_id = physicain_id();
  		$status = '';
  		$image_new = '';
  		$result ='';
  		$check_post_value = '';
  		$favourite_job =$this->common_model->favourite_job_res($user_id);
  		$medical_specialty  = '';
  		if(!empty($favourite_job))
  		{
	  		foreach($favourite_job as $value)
		  	{
	  			$date_posted = '';
	  			$date_created = date('d M y',strtotime($value->marked_date));
	  			$post_image  = '';
	  			if($value->uploadImageArray)
	  			{
	  				$image_new = unserialize($value->uploadImageArray);
	  				//echo '<pre>'; print_r($image_new);
	  				//echo $value->id;
	  				if(file_exists('assets/uploads/jobs/'.$image_new[0]['imageName']))
				    {
		  				if(!empty($image_new))
			  			{
			  				
			  				$post_image = site_url().'assets/uploads/jobs/'.$image_new[0]['imageName'];
			  			}
			  		}	
	  			}
	  			
	  			$medical_specialty = get_medical_specialty($value->medical_specialty_name);
		  		$result[] = (object)array(
						'job_name'    => $value->listing_title,
						'id'          => $value->job_id,	
						'image_path'  => $post_image,
						'marked_date' => $date_created,
						'medical_specialty' => $medical_specialty,
						'favourite_id' => $value->favourite_job_id,
						'encrypt_id'          => base64_encode($value->job_id)

						); 	
			}  		
	  	}	
    	$data=array(
			'favourite_jobs'	=> $result,
    		);
    	if(!empty($data)){
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
 
  	}
	public function saved_search_get()
  	{
  		$user_id = physicain_id();
  		$image_new = '';
  		$result = array();
  		$data = array();
  		$saved_search =$this->common_model->get_result('saved_search',array('user_id'=>$user_id),'',array('saved_search_id','desc'));
  		if(!empty($saved_search))
  		{
	  		foreach($saved_search as $value)
		  	{
		  		
	  			$city_postal_code = '-';
	  			$medical_specialty = '-';
	  			$provice = '-';
	  			$medical_specialty = get_medical_specialty($value->medical_specialties_id);
	  			if($medical_specialty == false)
	  			{
	  				$medical_specialty = '-';
	  			}
	  			$provice = get_province_name($value->provice_id);
	  			if($provice == false)
	  			{
	  				$provice = '-';
	  			}
	  			if($value->city_postal_code)
	  			{
	  				$city_postal_code = $value->city_postal_code;
	  			}
	  				
	  			
		  		
				$medical_spec_logo = get_medical_specialty_logo($value->medical_specialties_id);
		        $logo_image = '';
		        if(!empty($medical_spec_logo))
		        {
			        if(file_exists('assets/uploads/logo/'.$medical_spec_logo))
				    {
			  			$logo_image = site_url().'assets/uploads/logo/'.$medical_spec_logo;
			  			
			  		}else{
		  				$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
		  			}	
			  	}
		  		$result[] = (object)array(	
						'city_postal_code'  => $city_postal_code ,
						'medical_specialty' => $medical_specialty,
						'provice'           => $provice,
						'position'          => $value->position,
						'search_id'         => $value->saved_search_id,
						'medical_spec_logo' => $logo_image
						);	
			}  		
	  	}	
    	$data = array('saved_search'	=> $result,);
    	if(!empty($data)){
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
 
  	}
  	function delete_savesearch_post()
  	{
  		$user_id = physicain_id();
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$search_id = $_POST['search_id'];
  		$id = medicaluser_id();
  		if($this->common_model->delete('saved_search',array('user_id'=>$user_id ,'saved_search_id'=>$search_id)))
		{

			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Delete successfully .";
		}
		$this->api_response();	
  	}
  	function mark_unfavourite_post()
  	{
  		$user_id = physicain_id();
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$favourite_id = $_POST['favourite_id'];
  		$id = medicaluser_id();
  		if($this->common_model->delete('favourite_job',array('user_id'=>$user_id ,'job_id'=>$favourite_id)))
		{

			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Delete successfully .";
		}
		$this->api_response();	
  	}
  	function get_saved_search_value_post(){
  		$user_id 			 = physicain_id();
  		$save_search_id		 = $_POST['save_search_id'];
  		$saved_search  		 = '';
  		$saved_search 		 = $this->common_model->get_row('saved_search',array('user_id'=>$user_id,'saved_search_id'=>$save_search_id),'',array('saved_search_id','desc'));
  		//echo $this->db->last_query();
    	$data=array(
			'saved_search_data'	 => $saved_search,
    		);
    	if(!empty($data)){
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
  	}
  	function get_city_post()
  	{
  		$provice	= $_POST['provice'];
  		//$provice    = get_province_name($provice_id);
  		$data['result'] = $this->common_model->get_province('location_complete_data',array('province_name'=>$provice),array('id,city_name'),'','city_name',1300);

		if(!empty($data['result'])){
      		
      		$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
      	}else{
			$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";		
		}
		$this->api_response();
  	}
  	
  	function get_latlog_post()
  	{
		$provice	= $_POST['state'];
		$city		= $_POST['city'];
  		$postal_code = $_POST['postal_code'];
  		$address= $postal_code.'+'.$city.'+'.$provice;
  		if($address){
	        $address=urlencode($address);
	        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        $response = curl_exec($ch);
	        curl_close($ch);
	        if($response){
	            $json_a = json_decode($response, TRUE); //json decoder
	            $data=array();

	            if($json_a['status'] == 'OK'){
	                $get_data['latitude']=$json_a['results'][0]['geometry']['location']['lat']; // get lat for json
	                $get_data['longitude']=$json_a['results'][0]['geometry']['location']['lng']; // get ing for json
	             	$data=array(
						'get_latlong'	 => $get_data,
		    		);
	            }
	            elseif($get_data = $this->common_model->get_row('location_complete_data',array('province_name'=>$provice,'city_name'=>$city,'postal_code'=>$postal_code),'',array())){
		    		$data=array(
						'get_latlong'	 => $get_data,
		    		);
		         }
	         }
	         elseif($get_data = $this->common_model->get_row('location_complete_data',array('province_name'=>$provice,'city_name'=>$city,'postal_code'=>$postal_code),'',array())){
		    		$data=array(
						'get_latlong'	 => $get_data,
		    		);
	          }

	      	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
  	}
  	
  	public function clone_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
	    $id = $_POST['job_id'];	
  		$post_data_update = $this->common_model->get_row('post_jobs',array('id'=>$id));
  		if(!empty($post_data_update))
		{
			
			//$nursing_unserialize = $post_data_update->nursing_array;
			//$nursing_array = unserialize($nursing_unserialize);
			//$health_array = unserialize($post_data_update->health_array);
			$FacilityHoursArray = unserialize($post_data_update->facility_hours_array);
			//$facility_hours = '';
			$uploadImageArray = '';
			//$post_data_update->nursing_array = $nursing_array;
			//$post_data_update->health_array = $health_array;
			//$post_data_update->uploadImageArray = $uploadImageArray;
			$post_data_update->facility_hours = $FacilityHoursArray;
			$imagesarray=array();
			if($post_data_update->uploadImageArray)
			{
			$uploadImageArray =unserialize($post_data_update->uploadImageArray);
				foreach ($uploadImageArray as $value)
			 	{
					
			 		if(file_exists('assets/uploads/jobs/'.$value['imageName']))
				    {
						$image=getimagesize('assets/uploads/jobs/'.$value['imageName']);
						if($image){
						$imagesarray[]= array(
									'lastModifiedDate'	=> date('Y-m-d H:i:s'),
									'size'				=> filesize('assets/uploads/jobs/'.$value['imageName']),
									'type'				=> $image['mime'],
									'name'				=> $value['imageName'],
									'url'				=> $value['imageName'],
						         );
						}
					}	
				} 
			}
			//$post_data_update->imagesarray 		= $imagesarray;
			$post_data_update->uploadImageArray = '';
			$post_data_update->facility_postal_code		= get_postalcode_id($post_data_update->facility_postal_code);
	  		unset($post_data_update->id);
	  		unset($post_data_update->created);
	  		unset($post_data_update->modified);
	  		unset($post_data_update->posted);
	  		unset($post_data_update->job_verify);
	  		unset($post_data_update->status); 
  			$data['post_data_clone'] = $post_data_update;
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}
  		$this->api_response();
  	}
	
	/*Message Center*/
  	public function save_message_post()
	{
		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
		
	    if(physicain_id() && $this->input->post('job_id') && $this->input->post('contact_subject') && $this->input->post('default_msg'))
	    {
	    	$job_id=$this->input->post('job_id');
	    	if($this->input->post('check_status') == 'insert_data')
	    	{
		    	
		    	if($row_count_payment = $this->common_model->check_payment_status($job_id))
				{
					
					$row_get_statistics = '';	
					//$row_get_statistics = $this->common_model->count_rows('statistics_job', array('job_id'=>$job_id),'','');
					$find_id = $_SERVER['REMOTE_ADDR'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_USER_AGENT'];
					$row_get_statistics = $this->common_model->count_rows('statistics_job', array('job_id'=>$job_id,'ip_address' => md5($find_id)),'','');
					//if($row_get_statistics)
					if($row_get_statistics < 1)
					{
				
						$this->common_model->insert('statistics_job', array('job_id'=>$job_id,'ip_address' => md5($find_id),'user_id' => physicain_id()));
					}
				}
				

			}	
			
	    	$job_detail =$this->common_model->get_row('post_jobs',array('id'=>$job_id),array('user_id'));
	    	$receiver_id=$job_detail->user_id;
		    $msg_data=array(
				'job_id'		=> $this->input->post('job_id'),
				'msg_subject'	=> $this->input->post('contact_subject'),
				'msg_body'		=> $this->input->post('default_msg'),
				'sender_id'		=> physicain_id(),
				'receiver_id'	=> $receiver_id,
				'date_time'		=> date('Y-m-d H:i:s A'),
			);
			if( $this->common_model->insert('jobs_messages',$msg_data)){
				$get_info_user = get_user_info($receiver_id);
		        $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
		        $sender_info_user = get_user_info(physicain_id());
		        $sender_name = $sender_info_user->user_first_name.' '.$sender_info_user->user_last_name;
				$email_template = $this->common_model->get_row('email_templates',array('id'=>8));
				$image_url = base_url().'assets/front/images/click-here-7.png';
		    	$param=array
		    		(
						
						
						'template'  =>  
						array(
					    		'temp'  =>  $email_template->template_body,
				        		'var_name'  =>  
				        					array(
			  										'username'    => $username,
													'message' => $this->input->post('default_msg'),
													'sender_id' => physicain_id(),
													'sender_name' => $sender_name,
													'job_id' => $this->input->post('job_id'),
													'job_name' => get_job_name($this->input->post('job_id')),
													'image_url' => $image_url
												),   
						),   
						'email' =>  
					    array(
					            'to'        =>   $get_info_user->user_email, 
					            'from'      =>   GLOBAl_INFO_EMAIL,
					            'from_name' =>   SITE_NAME,
					            'subject'   =>   $email_template->template_subject,
					     ),
					);   
		       	$this->chapter247_email->send_mail($param); 
				$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data inserted successfully.";
			} 
		}
		$this->api_response();
	}

  	public function check_msg_send_post()
	{
		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
	    if(physician_logged_in())
	    {
	    	$job_id = $this->input->post('job_id');
	    	$physicain_id = physicain_id();
	    	$check_both_user_mag_status = '';
	    	$check_both_user_mag_status = $this->common_model->count_rows('jobs_messages',array('job_id'=>$job_id,'sender_id'=>$physicain_id,'medical_delete' => 1,'physician_delete'=>1));
	    	if($check_both_user_mag_status > 0)
	    	{
	    		 $this->common_model->delete('jobs_messages',array('job_id'=>$job_id,'sender_id'=>$physicain_id));
	    	}
	    	$jobs_send_messages = $this->common_model->get_row('jobs_messages',array('job_id'=>$job_id,'sender_id'=>$physicain_id),array());
			if($jobs_send_messages){
				$encrypt_id =  base64_encode($jobs_send_messages->msg_id);
				$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data received successfully.";
				$this->response_array['data'] = $encrypt_id;	
			} 
		}
		$this->api_response();
	}
	public function medical_msg_get($sorting_value=''){
  		$this->response_array['response_code'] = 500;	
		$this->response_array['message'] = "Invalid request";
		$sorting_value = $this->uri->segment(3);
		if(medicaluser_logged_in())
	    {
	  		$medicaluser_id = medicaluser_id();
	  		if($messages = $this->common_model->get_messages('receiver_id',$medicaluser_id,$sorting_value))
	  		{
	  			//echo $this->db->last_query();
	  			$username = '';
	  			foreach ($messages as $value) {
	  				$count_unreadmsg='';
	  				$status_unreadmsg = 0;
	  				if($this->common_model->get_unreadmessages($value->msg_id,$medicaluser_id)){
						$count_unreadmsg='unreadmsg';
						$status_unreadmsg = 1;
					}
					$flag=0;
					if($value->flag){
						$flag=1;
					}
					if(!empty($value->sender_id))
					{
						$get_info_user = get_user_info($value->sender_id);
						if(!empty($get_info_user))
						{
							$userfirstname = $get_info_user->user_first_name;
							$userlastname = $get_info_user->user_last_name;
			        		$username = $userfirstname.' '.$userlastname;
			        	}		
					}
					
	  				$medical_specialty = get_medical_specialty($value->medical_specialty_name);

	  				$medical_spec_logo = get_medical_specialty_logo($value->medical_specialty_name);

	  				 $logo_image = '';
			        if(!empty($medical_spec_logo))
			        {
				        if(file_exists('assets/uploads/logo/'.$medical_spec_logo))
					    {
				  			$logo_image = site_url().'assets/uploads/logo/'.$medical_spec_logo;
				  			
				  		}else{
			  				$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
			  			}	
				  	}else{
			  			$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
			  		}	

	  				$messages_data[] = (object)array(
	  				        'flag'						=> $flag,    
	  				        'count_unreadmsg'			=> $count_unreadmsg,    
							'listing_title'				=> $value->listing_title,
							'facility_city'				=> $value->facility_city,
							'job_id'					=> $value->job_id,
							'msg_subject'				=> $value->msg_subject,
							'msg_body'					=> $value->msg_body,
							'msg_id'					=> base64_encode($value->msg_id),
							'msg_id_without_encrypt'    => $value->msg_id,
							'date_time'					=> date('d M y',strtotime($value->date_time)),
							'medical_specialty_name'	=> $medical_specialty,
							'msg_status'				=> $status_unreadmsg,
							'user_name'					=> $username,
							'medical_spec_logo' => $logo_image,
							'encrypt_id'               => base64_encode($value->job_id)
							); 
	  			}
	  			if(!empty($sorting_value) && $sorting_value == 1 )
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_ASC, $job_id,SORT_DESC, $messages_data); 
	  			}
	  			if(!empty($sorting_value) && $sorting_value == 2)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_DESC, $job_id,SORT_DESC, $messages_data); 
	  			}
	  			
	  			
	  			//echo $sorting_value.'</br>';
	  			//echo '<pre>'; print_r($messages_data);
		    	$data=array(
						'messages_data'	 => $messages_data,
			    		);
		    	if(!empty($data)){
					$this->response_array['response_code'] = 200;				
					$this->response_array['message'] = "Data show successfully.";
					$this->response_array['data'] = $data;	
				}
			}
		}
		$this->api_response();
  	}
  	public function physician_msg_get($sorting_value=''){
  		$this->response_array['response_code'] = 500;
  		$sorting_value = $this->uri->segment(3);	
		$this->response_array['message'] = "Invalid request";
		if(physician_logged_in())
	    {
	  		$physicain_id = physicain_id();
	  		if($messages = $this->common_model->get_messages('sender_id',$physicain_id,$sorting_value))
	  		{
	  			foreach ($messages as $value) {
	  				$count_unreadmsg='';
					$status_unreadmsg = 0;
	  				if($this->common_model->get_unreadmessages($value->msg_id,$physicain_id)){
						$count_unreadmsg='unreadmsg';
						$status_unreadmsg = 1;
					}
									
	  				$medical_specialty = get_medical_specialty($value->medical_specialty_name);
	  				$medical_spec_logo = get_medical_specialty_logo($value->medical_specialty_name);

	  				 $logo_image = '';
			        if(!empty($medical_spec_logo))
			        {
				        if(file_exists('assets/uploads/logo/'.$medical_spec_logo))
					    {
				  			$logo_image = site_url().'assets/uploads/logo/'.$medical_spec_logo;
				  			
				  		}else{
			  				$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
			  			}	
				  	}else{
			  			$logo_image = site_url().'assets/uploads/logo/no-preview.jpg';
			  		}	


	  				$messages_data[] = (object)array(
	  				        'count_unreadmsg'			=> $count_unreadmsg,    
							'listing_title'				=> $value->listing_title,
							'facility_city'				=> $value->facility_city,
							'job_id'					=> $value->job_id,
							'msg_subject'				=> $value->msg_subject,
							'msg_body'					=> $value->msg_body,
							'msg_id'					=> base64_encode($value->msg_id),
							'msg_id_without_encrypt'    => $value->msg_id,
							'date_time'					=> date('d M y',strtotime($value->date_time)),
							'medical_specialty_name'	=> $medical_specialty,
							'msg_status'				=> $status_unreadmsg,
							'medical_spec_logo' => $logo_image,

							'encrypt_id'               => base64_encode($value->job_id)
							); 
	  			}
	  			if(!empty($sorting_value) && $sorting_value == 1)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_ASC, $job_id,SORT_DESC, $messages_data); 
	  			}
	  			if(!empty($sorting_value) && $sorting_value == 2)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_DESC, $job_id,SORT_DESC, $messages_data); 
	  			}
		    	$data=array(
						'messages_data'	 => $messages_data,
			    		);
		    	if(!empty($data)){
					$this->response_array['response_code'] = 200;				
					$this->response_array['message'] = "Data show successfully.";
					$this->response_array['data'] = $data;	
				}
			}
		}
		$this->api_response();
  	}

  	public function msg_detail_post(){
  		$this->response_array['response_code'] = 500;	
		$this->response_array['message'] = "Invalid request";
		$msg_id = $_POST['msgId'];
		if(physician_logged_in() && $msg_id)
	    {
	    	$physicain_id = physicain_id();
	    	$msg_id=base64_decode($msg_id);
	    	$this->common_model->update('jobs_messages',array('read_status'=>0),array('parent_id'=>$msg_id,'receiver_id'=>$physicain_id));

	    	$first_msg = $this->common_model->get_row('jobs_messages',array('msg_id'=>$msg_id));
	    	$job_id=$first_msg->job_id;
	    	$job_detail = $this->common_model->get_row('post_jobs',array('id'=>$job_id),array('medical_specialty_name,listing_title,facility_city'));
	  		
	  		$messages = $this->common_model->get_messages_detail($msg_id,$physicain_id);
	  		if($messages && $first_msg && $job_detail)
	  		{
	  			$medical_specialty = get_medical_specialty($job_detail->medical_specialty_name);
	  			$msg_subject=0;
	  			foreach ($messages as $value) {
	  				$msg_receive=0;
	  				if($value->sender_id != $physicain_id){
	  					$msg_receive=1;
	  				}
	  				if($value->msg_subject ){
	  					$msg_subject=$value->msg_subject ;
	  				}
	  				$messages_data[] = (object)array(
	  				        'msg_receive'	=> $msg_receive,
							'msg_body'		=> $value->msg_body,
							'date_time'		=> date('d M y H:i:s',strtotime($value->date_time)),
							); 
	  			}
		    	$data=array(
						'messages_data'		=> $messages_data,
						'msg_subject'		=> $msg_subject,
						'medical_specialty'	=> $medical_specialty,
						'listing_title'		=> $job_detail->listing_title,
						'facility_city'		=> $job_detail->facility_city,
			    		);
		    	if(!empty($data)){
					$this->response_array['response_code'] = 200;				
					$this->response_array['message'] = "Data show successfully.";
					$this->response_array['data'] = $data;	
				}
			}
		}
		$this->api_response();
  	}

  	public function insert_msg_reply_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
	    $msgId = $_POST['msgId'];	
	    $reply = $_POST['reply'];	
  	
  		if(physician_logged_in() && $msgId && $reply)
  		{	
  			$msg_id=base64_decode($msgId);
  			$first_msg = $this->common_model->get_row('jobs_messages',array('msg_id'=>$msg_id));
  			$check_msg_present = $this->common_model->check_message_exists($msg_id);
  			if($check_msg_present > 0)
  			$this->common_model->update('jobs_messages',array('physician_delete' => 0,'medical_delete' => 0),$msg_id);	
  			
  			if($first_msg){
				$msg_data=array(
					'job_id'		=> $first_msg->job_id,
					'msg_body'		=> $this->input->post('reply'),
					'sender_id'		=> physicain_id(),
					'receiver_id'	=> $first_msg->receiver_id,
					'parent_id'		=> $msg_id,
					'date_time'		=> date('Y-m-d H:i:s A'),
				);
				$this->common_model->insert('jobs_messages',$msg_data);
				$get_info_user = get_user_info($first_msg->receiver_id);
		        $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
		        $sender_info_user = get_user_info(physicain_id());
		        $sender_name = $sender_info_user->user_first_name.' '.$sender_info_user->user_last_name;
				$email_template = $this->common_model->get_row('email_templates',array('id'=>8));
				$image_url = base_url().'assets/front/images/click-here-7.png';
		    	$param=array
		    		(
						
						
						'template'  =>  
						array(
					    		'temp'  =>  $email_template->template_body,
				        		'var_name'  =>  
				        					array(
			  										'username'    => $username,
													'message' => $this->input->post('reply'),
													'sender_id' => physicain_id(),
													'sender_name' => $sender_name,
													'job_id' => $first_msg->job_id,
													'job_name' => get_job_name($first_msg->job_id),
													'image_url' => $image_url
												),   
						),   
						'email' =>  
					    array(
					            'to'        =>   $get_info_user->user_email, 
					            'from'      =>   GLOBAl_INFO_EMAIL,
					            'from_name' =>   SITE_NAME,
					            'subject'   =>   $email_template->template_subject,
					     ),
					);   
		       	$this->chapter247_email->send_mail($param); 
	  			$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data Get successfully .";
			}	
  		}
  		$this->api_response();
  	}

  	

  	public function msg_detail_medical_post(){
  		$this->response_array['response_code'] = 500;	
		$this->response_array['message'] = "Invalid request";
		$msg_id = $_POST['msgId'];
		if(medicaluser_logged_in() && $msg_id)
	    {
	    	$medicaluser_id = medicaluser_id();
	    	$msg_id=base64_decode($msg_id);
	    	$this->common_model->update('jobs_messages',array('read_status'=>0),array('msg_id'=>$msg_id,'receiver_id'=>$medicaluser_id));
	    	//echo $this->db->last_query();
	    	$this->common_model->update('jobs_messages',array('read_status'=>0),array('parent_id'=>$msg_id,'receiver_id'=>$medicaluser_id));
	    	//echo $this->db->last_query();
	    	$first_msg = $this->common_model->get_row('jobs_messages',array('msg_id'=>$msg_id));
	    	$job_id=$first_msg->job_id;
	    	$job_detail = $this->common_model->get_row('post_jobs',array('id'=>$job_id),array('medical_specialty_name,listing_title,facility_city'));	  		
	  		$messages = $this->common_model->get_messages_detail($msg_id,$medicaluser_id);
	  		if($messages && $first_msg && $job_detail)
	  		{
	  			$username = '';
	  			$medical_specialty = get_medical_specialty($job_detail->medical_specialty_name);
	  			$msg_subject='';
	  			foreach ($messages as $value) {
	  				if($value->msg_subject ){
	  					$msg_subject=$value->msg_subject ;
	  				}

	  				$msg_receive=0;
	  				if($value->sender_id != $medicaluser_id){
	  					$msg_receive=1;
	  				}
	  				$messages_data[] = (object)array(
	  				       'msg_receive'	=> $msg_receive,
							'msg_body'		=> $value->msg_body,
							'date_time'		=> date('d M y H:i:s',strtotime($value->date_time)),
							); 
	  			}
	  			if(!empty($value->sender_id))
				{
					$get_info_user = get_user_info($first_msg->sender_id);
					if(!empty($get_info_user))
					{
						$userfirstname = $get_info_user->user_first_name;
						$userlastname = $get_info_user->user_last_name;
		        		$username = $userfirstname.' '.$userlastname;
		        	}		
				}
		    	$data=array(
		    			'msg_subject'		=> $msg_subject,
						'messages_data'		=> $messages_data,
						'medical_specialty'	=> $medical_specialty,
						'listing_title'		=> $job_detail->listing_title,
						'facility_city'		=> $job_detail->facility_city,
						'user_name'         => $username
						
 			    		);
		    	if(!empty($data)){
					$this->response_array['response_code'] = 200;				
					$this->response_array['message'] = "Data show successfully.";
					$this->response_array['data'] = $data;	
				}
			}
		}
		$this->api_response();
  	}

  	public function insert_msg_reply_medical_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
	    $msgId = $_POST['msgId'];	
	    $reply = $_POST['reply'];	
  	
  		if(medicaluser_logged_in() && $msgId && $reply)
  		{	
  			$msg_id=base64_decode($msgId);
  			$check_msg_present = '';
  			$first_msg = $this->common_model->get_row('jobs_messages',array('msg_id'=>$msg_id));
  			$check_msg_present = $this->common_model->check_message_exists($msg_id);
  			if($check_msg_present > 0)
  			$this->common_model->update_msg('jobs_messages',array('physician_delete' => 0,'medical_delete' => 0),$msg_id);	
  			
  			if($first_msg){
				$msg_data=array(
					'job_id'		=> $first_msg->job_id,
					'msg_body'		=> $this->input->post('reply'),
					'sender_id'		=> medicaluser_id(),
					'receiver_id'	=> $first_msg->sender_id,
					'parent_id'		=> $msg_id,
					'date_time'		=> date('Y-m-d H:i:s A'),
				);
				$this->common_model->insert('jobs_messages',$msg_data);
				$get_info_user = get_user_info($first_msg->sender_id);
		        $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
		        $sender_info_user = get_user_info(medicaluser_id());
		        $sender_name = $sender_info_user->user_first_name.' '.$sender_info_user->user_last_name;
				$email_template = $this->common_model->get_row('email_templates',array('id'=>8));
				$image_url = base_url().'assets/front/images/click-here-7.png';
		    	$param=array
		    		(
						
						
						'template'  =>  
						array(
					    		'temp'  =>  $email_template->template_body,
				        		'var_name'  =>  
				        					array(
			  										'username'    => $username,
													'message' => $this->input->post('reply'),
													'sender_id' => medicaluser_id(),
													'sender_name' => $sender_name,
													'job_id' => $first_msg->job_id,
													'job_name' => get_job_name($first_msg->job_id),
													'image_url'=> $image_url
												),   
						),   
						'email' =>  
					    array(
					            'to'        =>   $get_info_user->user_email, 
					            'from'      =>   GLOBAl_INFO_EMAIL,
					            'from_name' =>   SITE_NAME,
					            'subject'   =>   $email_template->template_subject,
					     ),
					);   
		       	$this->chapter247_email->send_mail($param); 
	  			$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data Get successfully .";
			}	
  		}
  		$this->api_response();
  	}

  	public function markflag_post()
	{
		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
        $msg_id=$_POST['msg_id'];
  		$user_id = medicaluser_id();
  		if($msg_id && $user_id){
  			$msg_id=base64_decode($msg_id);
  			$user_data=array(
					'flag'	=> 1,
					'flagged_date'  =>date('Y-m-d H:i:s A')
				);
  			    $condition=array('receiver_id'=>$user_id, 'msg_id'=>$msg_id);
  			    $this->common_model->update('jobs_messages',$user_data,$condition);
  				$email_template = $this->common_model->get_row('email_templates',array('id'=>11));

  				$msg_detail = get_msg_detail($msg_id);
  				if(!empty($msg_detail))
  				{
					$sender_info_user = get_user_info(medicaluser_id());
			        $sender_name = $sender_info_user->user_first_name.' '.$sender_info_user->user_last_name;
			        $sender_id = $sender_info_user->user_id;
			        $image_url = base_url().'assets/front/images/click-here-7.png';
			        $reciver_info_user = get_user_info($msg_detail->sender_id);
			        $reciver_name = $reciver_info_user->user_first_name.' '.$reciver_info_user->user_last_name;
			        $reciver_id = $reciver_info_user->user_id;
				    $param=array
				    		(
								'template'  =>  
								array(
							    		'temp'  =>  $email_template->template_body,
						        		'var_name'  =>  
						        					array(
					  										'job_id'    => $msg_detail->job_id,
					  										'job_name'  => get_job_name($msg_detail->job_id),
					  										'site_url' => site_url(),
					  										'sender_id' => $sender_id,
					  										'sender_name' => $sender_name,
					  										'reciver_id' => $reciver_id,
					  										'reciver_name' =>$reciver_name,
					  										'image_url' => $image_url

															
														),   
								),   
								'email' =>  
							    array(
							            'to'        =>   ADMIN_EMAIL, 
							            'from'      =>   GLOBAl_INFO_EMAIL,
							            'from_name' =>   SITE_NAME,
							            'subject'   =>   $email_template->template_subject,
							     ),
							);   
				    $status=$this->chapter247_email->send_mail($param); 
				}    
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Flag Marked successfully.";
  		}
		$this->api_response();
	}
	public function get_item_sperate_post(){
		$data = array();
		$unique_id = $_POST['unique_id'];
		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
		$data['all_city_name'] = '';
		$all_city = '';
		if($unique_id)
		{
			$city_detail =$this->common_model->city_payment($unique_id
	  				);
			//echo $this->db->last_query();
			//echo '<pre>'; print_r($city_detail);
			if(!empty($city_detail)){
                foreach($city_detail as $city_d)
                {
                	if(!empty($city_d['city_name'])){
                		$all_city .= $city_d['city_name'].',';
                	} 
                }
                $data['all_city_name'] = trim($all_city,',');
          	}
          	if(!empty($data['all_city_name']))
          	{
          		$this->response_array['response_code'] = 200;	
				$this->response_array['message'] = "Data show successfully.";
				$this->response_array['data'] = $data;	
          	}
			
		}
		
	}
	public function physician_update_post()
	{

		$user_id = physicain_id();
		$this->form_validation->set_rules('user_name_title', 'Physician title', 'required');
        $this->form_validation->set_rules('user_first_name','Physician first name', 'required');
        $this->form_validation->set_rules('user_last_name', 'Physician last name', 'required');
        $this->form_validation->set_rules('traning_year', 'Doctor Traning Year', 'required');
        $this->form_validation->set_rules('physician_gender', 'Physician Gender', 'required');
        $this->form_validation->set_rules('physician_dob_date', 'Physician DOB date', 'required');
        $this->form_validation->set_rules('physician_dob_month', 'Physician DOB month', 'required');
        $this->form_validation->set_rules('physician_dob_year', 'Physician DOB year', 'required');
        $this->form_validation->set_rules('physician_profession', 'Physician profession', 'required');
        $this->form_validation->set_rules('medical_specialty', 'Medical Specialty', 'required');
        $this->form_validation->set_rules('medical_school', 'Medical School', 'required');
        $this->form_validation->set_rules('user_email', 'User Email', "trim|required|unique[users.user_email,users.user_id,$user_id]");
       if($this->input->post('physician_minc'))
	    {
	        $this->form_validation->set_rules('physician_minc', 'Doctor minc', 'required|max_length[10]');
	    }
  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == TRUE)  {
  	    	$salt = $this->salt();
		    $user_data=array(
				'user_name_title' => $this->input->post('user_name_title'),
				'user_first_name' => trim($this->input->post('user_first_name')),
				'user_last_name'  => trim($this->input->post('user_last_name')),
				'user_email'	  => trim($this->input->post('user_email')),
				'user_updated'    => date('Y-m-d H:i:s A'),
			);
		 	$id = physicain_id();
		    $con = array('user_id'=> $id);
			$res=$this->common_model->update('users',$user_data,$con);
			if($res){
				$physician_data=array(
					'physician_gender'         => $this->input->post('physician_gender'),
					'physician_dob_date'       => $this->input->post('physician_dob_date'),
					'physician_dob_month'      => $this->input->post('physician_dob_month'),
					'physician_dob_year'       => $this->input->post('physician_dob_year'),
					'physician_profession'     => $this->input->post('physician_profession'),
					'medical_specialty'        => $this->input->post('medical_specialty'),
					'medical_school'           => $this->input->post('medical_school'),
					'traning_year'             => $this->input->post('traning_year'),
					'physician_minc'           => $this->input->post('physician_minc'),
					'physician_country'        => $this->input->post('physician_country'),
					'physician_state'          => $this->input->post('physician_state'),
					'physician_city'           => $this->input->post('physician_city'),
					'physician_address'        => $this->input->post('physician_address'),
					'physician_postal_code'    => $this->input->post('physician_postal_code'),
					'physician_phone_number'   => $this->input->post('physician_phone_number'),
					'medical_selectSpecialty'  => $this->input->post('medical_selectSpecialty'),
					'physician_selectState'    => $this->input->post('physician_selectState'),
					'physician_default_msg'    => $this->input->post('physician_default_msg'),
				);
			  $this->common_model->update('physicians',$physician_data,array('user_id'=>$id));
			  $this->response_array['response_code'] = 200;				
			  $this->response_array['message'] = "Data updated successfully.";	
			  
			}
			else{
	  		 	$this->response_array['response_code'] = 500;
				$this->response_array['message'] = "Invalid Form Request";
				
			}
		} 
		else{
	 		 	$this->response_array['response_code'] = 500;
				$this->response_array['message'] = "Validation error";
				$this->response_array['error'] = $this->form_validation->error_array() ;
		}
		$this->api_response();
	}
	public function get_physician_profile_detail_get()
  	{
  		$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
  		$id = physicain_id();
  		$physician_profile_detail = $this->user_model->physician_detail($id);
  		//echo "Hello";print_r($physician_profile_detail);exit;
  		$physician_profile_det = array(
  				'user_name_title' => $physician_profile_detail->user_name_title,
  				'user_first_name' => $physician_profile_detail->user_first_name,
  				'user_last_name' => $physician_profile_detail->user_last_name,
  				'user_email' => $physician_profile_detail->user_email,
  				'user_name_title' => $physician_profile_detail->user_name_title,
  				'physician_gender' => $physician_profile_detail->physician_gender,
  				'physician_dob_date' => $physician_profile_detail->physician_dob_date,
  				'physician_dob_month' => $physician_profile_detail->physician_dob_month,
  				'physician_dob_year' => $physician_profile_detail->physician_dob_year,
  				'physician_profession' => $physician_profile_detail->physician_profession,
  				'medical_specialty' => $physician_profile_detail->medical_specialty,
  				'medical_school' => $physician_profile_detail->medical_school,
  				'traning_year' => $physician_profile_detail->traning_year,
  				'physician_minc' => $physician_profile_detail->physician_minc,
  				'physician_country' => $physician_profile_detail->physician_country,
  				'physician_state' => $physician_profile_detail->physician_state,
  				'physician_city' => $physician_profile_detail->physician_city,
  				'physician_address' => $physician_profile_detail->physician_address,
  				'physician_postal_code' => $physician_profile_detail->physician_postal_code,
  				'physician_phone_number' => $physician_profile_detail->physician_phone_number,
  				'medical_selectSpecialty' => $physician_profile_detail->medical_selectSpecialty,
  				'physician_selectState' => $physician_profile_detail->physician_selectState,
  				'physician_default_msg' => $physician_profile_detail->physician_default_msg
  			);
  		if(!empty($physician_profile_detail))
  		{
  			$data['user_detail'] = $physician_profile_det;
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}
  		$this->api_response();
  	}
  	 public function statistics_job_get()
  	{

  		$result = array();
  		$message_count = 0;
  		$visitor_count = 0;
  		$statistics_jobs =$this->common_model->statistics_jobs();
  		if(!empty($statistics_jobs))
  		{
  			foreach($statistics_jobs as $value)
	  		{
	  			
	  			$statics_ratio = 0;
	  			$message_count = all_row_count('jobs_messages',array('job_id'=> $value->id,'parent_id'=>0));
	  			
	  			$visitor_count = all_row_count('statistics_job',array('job_id'=> $value->id));
	  			$msg_ratio ='';
	  			if(!empty($message_count) && !empty($visitor_count))
	  				$statics_ratio = ($message_count/$visitor_count)*100;
	  			//$statics_ratio = ($job_count/$message_count)*100;
	  			$remaining_ratio = 100 - $statics_ratio;
	  			
	  			$result[] = (object)array(
						'job_id'                => $value->id,
						'message_rate'     		=> $message_count,
						'job_view_rate'     	=> $visitor_count,
						'listing_title'			=> $value->listing_title,
						'medical_specialty'     => get_medical_specialty($value->medical_specialty_name),
						'facility_city'    		=> $value->facility_city,
						'msg_ratio'				=> $message_count,
						'job_ratio'				=> $visitor_count,
						'statics_ratio'			=> $statics_ratio,
						'remaining_ratio'       => $remaining_ratio,
						'statics_ratio_float'	=> number_format($statics_ratio,2),
						'remaining_ratio_float' => number_format($remaining_ratio,2)

						); 
	  		}
	  	}	
    	$data=array('statistics_job'=> $result);
    	if(!empty($data)){
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
 
  	}
  	function delete_msg_medical_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$msg_id = $_POST['msg_id'];
  		$user_data  = array('medical_delete'=>1);
  		if($this->common_model->update_msg('jobs_messages',$user_data,$msg_id))
		{

			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Delete successfully .";
		}
  		
		$this->api_response();	
  	}
  	function delete_msg_physician_post()
  	{
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$msg_id = $_POST['msg_id'];
  		$user_data  = array('physician_delete'=>1);
  		if($this->common_model->update_msg('jobs_messages',$user_data,$msg_id))
		{

			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Delete successfully .";
		}
  		
		$this->api_response();	
  	}
  	function count_message_post(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
		$msg_count = 0;
		$data = array();
  		$user_id = '';
  		$user_data = 0;
  		if(medicaluser_logged_in())
  		{
  			$user_id = medicaluser_id();
  			$user_data  = array('read_status'=> 1,'medical_delete'=>0,'receiver_id'=>$user_id);
  			$msg_count = all_row_count('jobs_messages',$user_data);
  			//echo $this->db->last_query();
  		}elseif(physician_logged_in()){
  			$user_id = physicain_id();
  			$user_data  = array('read_status'=> 1,'physician_delete'=>0,'receiver_id'=>$user_id);
  			$msg_count = all_row_count('jobs_messages',$user_data);
  			//echo $this->db->last_query();
  		}

  		
  		//echo $this->db->last_query();
  		$data = array('msg_count'=> $msg_count);
  		/*if($data)
		{*/
			$this->response_array['data'] = $data;	
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data count successfully .";
		// }
  		
		$this->api_response();	
  	}
  	function get_city_subscribtion_post(){
  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
  		$city_subscription_id='';
  		$job_id_encrypt = '';
  		$job_id = '';
  		$job_id_encrypt = $_POST['job_id'];
  		$search = $_POST['search'];
  		if($search)
  		{
	  		$job_id =  base64_decode($job_id_encrypt);
	   		if($city_subscription	= $this->common_model->city_subscription($job_id)){
	   			foreach ($city_subscription as $value) {
	   				$city_subscription_id = $city_subscription_id."".$value->city_id.",";
	   			}
	   			$city_subscription_id = rtrim($city_subscription_id, ",");
	   		}
	   		
			$othercity_data		= $this->common_model->get_result_city($city_subscription_id,$search);
			
			if(!empty($othercity_data))
			{
				$data['othercity_data']		= $othercity_data;
			}else{
				$data['othercity_data']		= 0;
			}
			
			if($data)
			{
				
				$this->response_array['data'] = $data;	
				$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data get successfully .";
			}
		}	
  		
		$this->api_response();	
  	}
  	function get_zip_code_post()
  	{
  		$search = '';
  		$city = '';
  		$stateval = '';
  		$city	= $_POST['city'];
  		$search	= $_POST['search'];
  		//$data['result'] = $this->common_model->get_result_zipcode($city,$search);
  		//$stateval = $_POST['stateval'];
  		$data['result'] = $this->common_model->get_result_zipcode($city,$search,$stateval);
		if(!empty($data['result'])){
      		
      		$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
      	}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
  	}
  	function contact_us_post(){
  	  	$this->form_validation->set_rules('name', 'Name', 'required');
      	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');  
      	$this->form_validation->set_rules('mobile', 'Contact No.','required|numeric|min_length[10]|max_length[12]'); 
      	$this->form_validation->set_rules('message', 'Message', 'required'); 
      	$this->form_validation->set_rules('subject', 'Subject', 'required'); 
      	$this->form_validation->set_rules('city', 'City', 'required'); 
      	$this->form_validation->set_rules('captcha', 'Captcha','required|callback_captcha_check'); 
      	$this->form_validation->set_error_delimiters('<div class="form_error">', '</div>');
      	if ($this->form_validation->run() == TRUE)
      	{ 
          $insert = array('name'=> $this->input->post('name'),
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mobile'),
            'city' => $this->input->post('city'),
            'subject' => $this->input->post('subject'),
            'message' => $this->input->post('message'),
            'created' => date('Y-m-d H:i:s')
             );
          $lats_id= $this->common_model->insert('contact_us',$insert);
           $image_url = base_url().'assets/front/images/click-here-7.png';
           /*  mail function start */
           if($lats_id)
           {
	            
	            $email_template=$this->common_model->get_row('email_templates',array('id'=>7));
	            $image_url = base_url().'assets/front/images/click-here-7.png';
	            $param=array(
	            'template'  =>  array(
	                    'temp'  =>  $email_template->template_body,
	                    'var_name'  =>  array(
	                            'username'  =>  $this->input->post('name'),
	                            'site_url' =>  site_url(),
	                            'image_url' => $image_url
	                                 
	                            ), 
	            ),      
	            'email' =>  array(
	                'to'    =>   $this->input->post('email'),
	                'from'    =>   GLOBAl_INFO_EMAIL,
	                'from_name' =>   SITE_NAME,
	                'subject' =>   $email_template->template_subject,
	              )
	            );  
	            $status=$this->chapter247_email->send_mail($param);
	        }    

           /*  mail function end */
      
         	$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data inserted successfully.";	
			$this->api_response();
		} 
		else
		{
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->api_response();
		}
    
  	}

  	function captcha_check($str=''){
      	if($str!=$this->session->userdata("cap_code")):
            $this->form_validation->set_message('captcha_check', 'Captcha is Invalid, Please try again');
            return FALSE;
        else:
           return TRUE;
        endif;
     }

  	public function forgot_password_post(){  
	  	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	  	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	  	if ($this->form_validation->run() == TRUE){

	    	if($user=$this->common_model->get_row('users',array('user_role !=' => 3,'user_email'=>trim($_POST['email'])))){
	    	
		        $new_password_key=trim(md5($user->user_email));
		        $this->common_model->update('users',array('new_password_key'=>$new_password_key,'new_password_requested'=>date('Y-m-d H:i:s')),array('user_id'=>$user->user_id));
		      
		        $link= site_url().'reset-password/'.$new_password_key; 
	         	$email_template=$this->common_model->get_row('email_templates',array('id'=>6));
	         	$image_url = base_url().'assets/front/images/click-here-7.png';
	            $param=array(
	            'template'  =>  array(
	                    'temp'  =>  $email_template->template_body,
	                    'var_name'  =>  array(
	                            'username'  =>  $user->user_first_name.' '.$user->user_last_name,
	                            'activation_key'  =>  $link,
		                        'site_url' =>  site_url(), 
		                        'image_url' => $image_url  
	                                 
	                            ), 
	            ),      
	            'email' =>  array(
	                'to'    =>  $user->user_email,
	                'from'    =>   GLOBAl_INFO_EMAIL,
	                'from_name' =>   SITE_NAME,
	                'subject' =>   $email_template->template_subject,
	              )
	            );  
	            $status=$this->chapter247_email->send_mail($param);
		       
		  		$this->response_array['response_code'] = 200;				
				$this->response_array['message'] = "Data saved successfully.";	
				$this->api_response();
			}else{
				$this->response_array['response_code'] = 500;
				$this->response_array['message'] = "Invalid Form Request";	
				$this->api_response();
			}	
		} 
		else
		{
		 	$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			$this->api_response();
		}
  	}
  	function reset_password_post(){   
	   
		$this->form_validation->set_rules('password', 'New Password', 'trim|required|min_length[6]');
 		$this->form_validation->set_rules('confirm_password','Confirm Password', 'trim|required|min_length[6]|matches[password]');
  		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  		$activation_key = $_POST['activation_key'];
  		if ($this->form_validation->run() == TRUE)
  		{
      		$user=$this->common_model->get_row('users',array('new_password_key'=>trim($activation_key)));
      		if($user)
      		{
	            $salt = $this->salt();
				$user_data  = array('new_password_key'=>'','salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('password')))));
	          	if($this->common_model->update('users',$user_data,array('user_id'=>$user->user_id))){                
	             	$this->response_array['response_code'] = 200;				
					$this->response_array['message'] = "Password update successfully.";	
	          	}else{
		    		$this->response_array['response_code'] = 500;
					$this->response_array['message'] = "Invalid Form Request.";	
				
		    	}
		       
		    }else{
		    	$this->response_array['response_code'] = 500;
				$this->response_array['message'] = "Your activation key expired.";	
				
		    }	
		}else{
			$this->response_array['response_code'] = 500;
			$this->response_array['message'] = "Invalid Form Request";	
			$this->response_array['error'] = $this->form_validation->error_array() ;
			
		}
		$this->api_response();
      	
    }
    function check_read_unread_status_get($sorting_value=''){
    	$sorting_value = $this->uri->segment(3);
    	$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";	
    	if(physician_logged_in())
	    {
	  		$physicain_id = physicain_id();
	  		
	  		if($messages = $this->common_model->get_messages('sender_id',$physicain_id))
	  		{
	  			
	  			foreach ($messages as $value) 
	  			{
	  				$count_unreadmsg='';
	  				$status_unreadmsg = 0;
	  				if($this->common_model->get_unreadmessages($value->msg_id,$physicain_id)){
						$count_unreadmsg='unreadmsg';
						$status_unreadmsg = 1;
					}
	  				//echo $this->db->last_query();
					$messages_data[] = (object)array(
	  				        'count_unreadmsg'			=> $count_unreadmsg,   
	  				        'job_id'					=> $value->job_id,
							'msg_status'				=> $status_unreadmsg 
							); 

				}
				if(!empty($sorting_value) && $sorting_value == 1)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_ASC, $job_id,SORT_DESC, $messages_data); 
	  			}
	  			if(!empty($sorting_value) && $sorting_value == 2)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_DESC, $job_id,SORT_DESC, $messages_data); 
	  			}
				$data=array('status_msg'	 => $messages_data);
			}		

	  	}		
	  	if(medicaluser_logged_in())
	    {
	  		$medicaluser_id = medicaluser_id();
	  		if($messages = $this->common_model->get_messages('receiver_id',$medicaluser_id))
	  		{
	  			//echo $this->db->last_query();
	  			foreach ($messages as $value) {
	  				$count_unreadmsg='';
	  				$status_unreadmsg = 0;
	  				if($this->common_model->get_unreadmessages($value->msg_id,$medicaluser_id)){
						$count_unreadmsg='unreadmsg';
						$status_unreadmsg = 1;
					}
	  				
					$messages_data[] = (object)array(
	  				        'count_unreadmsg'			=> $count_unreadmsg,   
	  				        'job_id'					=> $value->job_id,
							'msg_status'				=> $status_unreadmsg 
							); 

				}
				if(!empty($sorting_value) && $sorting_value == 1)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_ASC, $job_id,SORT_DESC, $messages_data); 
	  			}
	  			if(!empty($sorting_value) && $sorting_value == 2)
	  			{
	  				foreach ($messages_data as $key => $value) {

						$job_id[$key] = $value->job_id;

						$msg_status[$key] = $value->msg_status;

					}
					array_multisort($msg_status,SORT_DESC, $job_id,SORT_DESC, $messages_data); 
	  			}

				$data=array('status_msg'	 => $messages_data);

			}		
		}
			
  		if(!empty($data))
		{
			$this->response_array['data'] = $data;	
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data get successfully .";
		}
  		
		$this->api_response();	
    }
    public function clone_job_listing_get()
  	{
  		$user_id = medicaluser_id();
  		$status = '';
  		$image_new = '';
  		$result ='';
  		$check_post_value = '';
  		$post_jobs =$this->common_model->get_result('post_jobs',array('user_id'=>$user_id,'status !=' =>4),'',array('id','desc'));
  		if(!empty($post_jobs))
  		{
	  		foreach($post_jobs as $value)
	  		{

	  			
	  			$result[] = (object)array(
						'id'                => $value->id,
						'listing_title'     => $value->listing_title,
						
						); 
	  		}
	  	}	
    	$data=array(
			'post_jobs'	=> $result,
    		);
    	if(!empty($data)){
			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data show successfully.";
			$this->response_array['data'] = $data;	
			
		}else{
			$this->response_array['response_code'] = 500;	
		}
		$this->api_response();
 
  	}
  	function checked_verified_jobs_get()
    {
    	

  		$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
		$verified_jobs = '';
  		$result = $this->common_model->all_verified_jobs();
  		if(!empty($result))
  		{
	  		foreach($result as $value)
	  		{

	  			$medical_spec_name = get_medical_specialty($value->medical_specialty_name);
	  			$post_image  = '';
	  			if($value->uploadImageArray)
	  			{
	  				$image_new = unserialize($value->uploadImageArray);
	  				
	  				if(!empty($image_new))
		  			{
		  				foreach($image_new as $image)
		  				{
		  					if(file_exists('assets/uploads/jobs/'.$image['imageName']))
		  						$post_image[] = $image['imageName'];
		  					
		  				}
		  				
		  			}
			  			
	  			}

	  			$verified_jobs[] = (object)array(
						'medical_spec_name' => $medical_spec_name,
						'job_id'                => $value->id,
						'facility_city'     => $value->facility_city,
						'image_path'        => $post_image,
						'job_verify'		=> $value->job_verify,
						'listing_title'     => $value->listing_title,
						
						); 
	  			$data['verified_jobs'] = $verified_jobs;
	  			
	  		}
	  	}	

  		if(!empty($data))
  		{
  			
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}
  		$this->api_response();
    }
    function valid_postal_code_post(){
    	$this->response_array['response_code'] = 500;
		$this->response_array['message'] = "Invalid Form Request";
    	$postal_code = $_POST['postal_code'];
    	$city = $_POST['city'];
    	$province = $_POST['province'];
  		$check_both_user_mag_status = $this->common_model->count_rows('location_complete_data',array('city_name'=>$city,'province_name'=>$province,'postal_code' => $postal_code));
  		//echo $this->db->last_query();
  		if(!empty($check_both_user_mag_status) && $check_both_user_mag_status >0)
  		{
  			
  			$data['result'] = $check_both_user_mag_status;
  			$this->response_array['response_code'] = 200;				
			$this->response_array['message'] = "Data Get successfully .";
			$this->response_array['data'] = $data;		
  		}else{
  			$this->response_array['error'] = '0';
  		}
  		$this->api_response();
    }
    
    public function reload_captcha_post(){

        /*$ranStr = md5(microtime());
        $ranStr = substr($ranStr, 0, 6);
        $font = './assets/front/Route159-LightItalic.otf';
        //$this->session->set_userdata("cap_code",$ranStr);
        $newImage = imagecreatefromjpeg('assets/uploads/cap_bg.jpg');
        $txtColor = imagecolorallocate($newImage, 3, 63, 88);
        imagettftext($newImage, 22, 0, 14, 35, $txtColor, $font, $ranStr);
        ob_start(); 
            imagejpeg($newImage); 
            $contents = ob_get_contents();
        ob_end_clean();

        $dataUri = "data:image/jpeg;base64," . base64_encode($contents);*/
        $this->response_array['response_code'] = 200;				
		$this->response_array['message'] = "Data Get successfully .";
		$this->response_array['data'] = generate_captcha($this->input->post('captcha_for'));	
  		$this->api_response();
    }
  	
  	public function rotate_image_post()
	{	    
        if ($this->input->post('img_url') && file_exists("assets/uploads/jobs/".$this->input->post('img_url')))  
        {
        	echo "TRUE";
        	rotate_image($this->input->post('img_url'));
		} 
		else{
			echo "FALSE";
		}
		die;
	}
 }