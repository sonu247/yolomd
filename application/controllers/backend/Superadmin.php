<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Superadmin extends CI_Controller {
	public function __construct(){
        parent::__construct();
        clear_cache();
        $this->load->model('superadmin_model');
        $this->load->model('user_model');
        $this->load->model('common_model');

    }
   
	public function index(){
		$this->login();
	}
	private function _check_login(){
		if(superadmin_logged_in()===FALSE)
			redirect('backend/superadmin/login');
	}
	
	public function login(){
		if(superadmin_logged_in()===TRUE) redirect('backend/superadmin/dashboard');
		$data['title']='Admin login';
		 $this->form_validation->set_rules('password', 'Password', 'trim|required');
		 $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		if ($this->form_validation->run() == TRUE){
		$this->load->model('user_model');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
			if($this->user_model->login($email,$password,'superadmin')){
			redirect('backend/superadmin/dashboard');
			}else{
			$this->session->set_flashdata('msg_error','Please fill correct email and password details.');
			}
		}
		$this->load->view('backend/login');
	}
	
	public function logout(){
		$this->_check_login(); //check  login authentication
		$this->session->sess_destroy();
		redirect(base_url().'backend/superadmin/login');
	}
    
   
    
    public function dashboard(){    
	    $this->_check_login(); //check login authentication
	    $data['template']='backend/dashboard';
	    $this->load->view('templates/superadmin_template',$data);
    }

	public function profile(){
	$this->_check_login(); //check login authentication
	$data['title']='profile';
	$this->form_validation->set_rules('firstname', 'First Name', 'required');
	$this->form_validation->set_rules('lastname', 'Last Name', 'required');
	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	if ($this->form_validation->run() == TRUE)	{
		$user_data  = array(
						'user_first_name'	=>	$this->input->post('firstname'),
						'user_last_name'	=>  $this->input->post('lastname'),
						'user_email'		=>	$this->input->post('email'),
						);
		if($this->superadmin_model->update('users', $user_data,array('user_id'=>superadmin_id()))){
			 $this->session->set_flashdata('msg_success','Profile updated successfully.');
			redirect('backend/superadmin/profile');
		}else{
			$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			redirect('backend/superadmin/profile');
		}
	  }else{

		$data['user'] = $this->superadmin_model->get_row('users', array('user_id'=>superadmin_id()));
		$data['template']='backend/profile';
		$this->load->view('templates/superadmin_template',$data);
	  }
	}
	public function change_password(){
		$this->_check_login(); //check login authentication
		$data['title']='change_password';
		$this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|callback_password_check');
		$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[6]|matches[confpassword]');
		$this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == TRUE){
			$salt = $this->salt();
			$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('newpassword')))));
			$id =superadmin_id();
			if($this->superadmin_model->update('users',$user_data,array('user_id'=>$id))){
			$this->session->set_flashdata('msg_success','Password updated successfully.');
			}else{
			$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			
			}
		}
		$data['template']='backend/change_password';
		$this->load->view('templates/superadmin_template',$data);
	}
	public function password_check($oldpassword){
		$this->_check_login(); //check login authentication
		$this->load->model('user_model');
		$user_info = $this->user_model->get_row('users',array('user_id'=>superadmin_id()));
		       $salt = $user_info->salt;
		if($this->common_model->password_check(array('password'=>sha1($salt.sha1($salt.sha1($oldpassword)))),superadmin_id())){
		return TRUE;
		}else{
		$this->form_validation->set_message('password_check', 'The %s does not match.');
		return FALSE;
		}
	}
	public function changeuserstatus_t($id="",$status="",$offset="",$table_name="")	{
		$this->_check_login(); //check login authentication
		 $data['title']='';

		if($status==0) $status=1;
		else $status=0;
		$data=array('status'=>$status);
		if($this->superadmin_model->update($table_name,$data,array('user_id'=>$id)))
		$this->session->set_flashdata('msg_success','Status Updated successfully.');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function option()
	{
		$this->_check_login(); //check login authentication
		$data['title']='option';
		
		$this->form_validation->set_rules('name[]','All Field','required');
		if ($this->form_validation->run() == TRUE){
		$name=$this->input->post('name');
		$ids=$this->input->post('ids');
		$i=0;
			foreach ($name as $value) 
			{
					$post_data = array('option_value' => htmlentities($value));
					$optionid=array('id'=>$ids[$i]);
					if($ids[$i]!=26){
					$this->superadmin_model->update('options',$post_data,$optionid);
						}
					$i++;
			}
			$this->session->set_flashdata('msg_success','Data updated successfully.');
			redirect('backend/superadmin/option');				
		  }
		$data['option'] = $this->superadmin_model->get_result('options',array(),'',array('order','ASC'));
		$data['template'] ='backend/option';
		$this->load->view('templates/superadmin_template', $data);
	} 
	function salt(){
		return substr(md5(uniqid(rand(), true)), 0, 10);
	}
	function error_404(){
     
      $data['template']='backend/404';  
      $this->load->view('templates/superadmin_template',$data); 

    } 
    public function login_user($id=''){
		$this->_check_login();
		$data['title']='Admin login';
		$row_get = '';	
		$row_get = $this->common_model->count_rows('users', array('user_id'=>$id),'','');
		if($row_get)
		{
			$admin=$this->superadmin_model->get_row('users', array('user_id'=>$id));
			
	 	    $res = $this->user_model->login($admin->user_email,$admin->password,'user',1);
	        if($res){			 
	        	if(medicaluser_logged_in())
	        	{
		        	redirect('job-listing');	
		        }elseif(physician_logged_in()){
		        	redirect("jobs/.");
		        }else{
		        	redirect($_SERVER['HTTP_REFERER']);
		        }
				
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}		
	}
	public function login_photographer($id=''){
		$this->_check_login();
		$data['title']='Admin login';
		$row_get = '';	
		$row_get = $this->common_model->count_rows('users', array('user_id'=>$id),'','');
		if($row_get)
		{
			$admin=$this->superadmin_model->get_row('users', array('user_id'=>$id));
	 	    $res = $this->user_model->login($admin->user_email,$admin->password,'photographer',1,3);
	        if($res){			 
		        	
		        if(photographer_logged_in())
	        	{
		        	redirect('backend/photographer_jobs');	
		        }else{
					redirect($_SERVER['HTTP_REFERER']);
				}
				
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}		
	}




}