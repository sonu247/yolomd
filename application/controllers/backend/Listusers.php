<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Listusers extends CI_Controller {
	public function __construct()
   	{
        parent::__construct();
       // Your own constructor code
       $this->load->model('common_model');
       $this->load->model('user_model');
       if(!superadmin_logged_in())
        {
        	redirect('backend/superadmin');
        }
       
    }
	/***************************************************************************
	 * Function for show list of all users with filteration options
	 ***************************************************************************/

	function physician($offset=0)
	{

		$per_page=25;
    	$data['offset']=$offset;
	    $data['result_data'] = $this->user_model->user_result($offset,$per_page);
	    //echo $this->db->last_query();
    	$config=backend_pagination();
    	$config['base_url'] = site_url().'backend/listusers/physician/';
	    $config['total_rows'] = $this->user_model->user_result(0,0);
    	$config['per_page'] = $per_page;
    	$config['uri_segment'] = 4;
	    if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	   	 if($config['total_rows'] < $offset){
           $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');  	
           redirect('backend/listusers/physician/0');
    	}
	    
	   	$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links(); 
		if(isset($_POST['apply']))
		{ 
			$act_typ = $this->input->post('act_type');
			$all_id = $this->input->post('check_id');
			if($act_typ == 4)
			{
				if(!empty($all_id))
				{
					foreach($all_id as $all_i)
					{  
						$this->common_model->delete('physicians',array('user_id'=>$all_i));
						
						$this->common_model->delete('users',array('user_id'=> $all_i,'user_role'=>2));
						
					}
					$this->session->set_flashdata('msg_success','Doctor details Delete successfully.');
					redirect('backend/listusers/physician'); 
				}
				               
			}
			elseif($act_typ ==2)
			{
				$this->session->set_flashdata('msg_success','Doctor details Deactivate successfully.');
			}
			elseif($act_typ ==1){
				$this->session->set_flashdata('msg_success','Doctor details Activate successfully.');
			}else{
				$this->session->set_flashdata('msg_success','Doctor details Banned successfully.');
			}
			
			$data_update['user_status'] = $act_typ;
			$data_update['user_role'] = 2;
			if(!empty($all_id))
			{
				foreach($all_id as $all_i)
				{ 
					$res =$this->common_model->update('users',$data_update,$array = array('user_id' =>$all_i));
				}
			}	
			   
			redirect('backend/listusers/physician');   
		
		} 
		$data['template'] = 'backend/user/physician_list';
		$this->load->view('templates/superadmin_template',$data);
   		
	}
	function medical($offset=0)
	{
		$per_page=25 ;
    	$data['offset']=$offset;
    	$config=backend_pagination();
    	$config['base_url'] = site_url().'backend/listusers/medical/';
	    $config['total_rows'] = $this->user_model->medical_result(0,0);
	    $config['per_page'] = $per_page;
    	$config['uri_segment'] = 4;
    	
	    $config['per_page'] = $per_page;
	    $data['result_data'] = $this->user_model->medical_result($offset,$per_page);
		if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	    $data['total_records'] = $config['total_rows'];
	    if($config['total_rows'] < $offset)
	    {
	       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
	       redirect('backend/listusers/medical/0');
	    }
	    //$config['first_url'] = $config['base_url'].$config['suffix'];
	    $this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();  
	    if(isset($_POST['apply']))
		{ 
			$act_typ = $this->input->post('act_type');
			$all_id = $this->input->post('check_id');
			if($act_typ == 4)
			{
				if(!empty($all_id))
				{
					foreach($all_id as $all_i)
					{  
						$this->common_model->delete('users',array('user_id'=> $all_i,'user_role'=>1));
						$this->common_model->delete('post_jobs',array('user_id'=> $all_i));
					}
					$this->session->set_flashdata('msg_success','Medical User details Delete successfully.');
					redirect('backend/listusers/medical'); 
				}
				               
			}
			elseif($act_typ ==2)
			{
				$this->session->set_flashdata('msg_success','Medical User details Deactivate successfully.');
			}
			elseif($act_typ ==1){
				$this->session->set_flashdata('msg_success','Medical User details Activate successfully.');
			}else{
				$this->session->set_flashdata('msg_success','Medical User details Banned successfully.');
			}
			
			$data_update['user_status'] = $act_typ;
			$data_update['user_role'] = 1;
			if(!empty($all_id))
			{
				foreach($all_id as $all_i)
				{ 
					$res =$this->common_model->update('users',$data_update,$array = array('user_id' =>$all_i));
					if ($act_typ==2) {
			        	$this->common_model->update('post_jobs',array('status'=>$act_typ),array('user_id'=>$all_i));
			        }
					
					//exit;
				}
			}	
			   
			redirect('backend/listusers/medical');   
		
		} 
		$data['template'] = 'backend/user/medical_list';
		$this->load->view('templates/superadmin_template',$data);
	}
	function delete()
    {
        
        $id = $this->uri->segment(4);
        $type = $this->uri->segment(5);
        if($type == 2)
        {
        	 $con = array('user_id'=>$id);
        	 $this->common_model->delete('physicians',$con);

        }
        if($type == 1)
        {
        	$con = array('user_id'=>$id,'user_role'=>1);
        }
       	$this->common_model->delete('users',$con);
       	if($type == 2)
        {
        	$this->session->set_flashdata('msg_success','Doctor detail delete successfully.');
    	}
    	if($type == 1)
        {
        	$this->common_model->delete('post_jobs',array('user_id'=> $id));
        	$this->session->set_flashdata('msg_success','Medical User detail delete successfully.');
    	}
        
    }

    function change_status()
    {
        $id = $this->uri->segment(4);
        $value = $this->uri->segment(5);
        $type = $this->uri->segment(6);
        $con = array('user_id'=>$id);
        $update_data = array('user_status'=>$value);
        $this->common_model->update('users',$update_data,$con);
        if ($value==2) {
        	$this->common_model->update('post_jobs',array('status'=>$value),array('user_id'=>$id));
        }
        $get_info_user = get_user_info($id);
        $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
		$email_template = $this->common_model->get_row('email_templates',array('id'=>5));
		$status = '';
    	$status = get_user_status($value) ;
    	$image_url = base_url().'assets/front/images/click-here-7.png';
    	$param=array
    		(
				'template'  =>  
				array(
			    		'temp'  =>  $email_template->template_body,
		        		'var_name'  =>  
		        					array(
	  										'username'    => $username,
	  										'site_url' => site_url(),
											'status' => $status,
											'image_url' => $image_url
										),   
				),   
				'email' =>  
			    array(
			            'to'        =>   $get_info_user->user_email, 
			            'from'      =>   GLOBAl_INFO_EMAIL,
			            'from_name' =>   GLOBAl_INFO_EMAIL,
			            'subject'   =>   $email_template->template_subject,
			     ),
			);   
       	$this->chapter247_email->send_mail($param); 
        if($type == 1)
        {
        	$this->session->set_flashdata('msg_success','Medical User Status changed successfully.');
        	redirect($_SERVER['HTTP_REFERER']);
        }
        if($type == 2)
        {
        	$this->session->set_flashdata('msg_success','Doctor Status changed successfully.');
        	redirect($_SERVER['HTTP_REFERER']); 
        }
        
              
    }
    function edit_physician($id='')
    {
        $id = $this->uri->segment(4);
        if($id == '' || !is_numeric($id)){ 
       		redirect('backend/superadmin/error_404');
        }
        $row_get = $this->common_model->count_rows('users', array('user_id'=>$id),'','');
        if($row_get < 1){ 
       	 	redirect('backend/superadmin/error_404');
        }
        $data['user_details'] = $this->user_model->physician_detail($id);
        if(empty($data['user_details']))
        {
        	redirect('backend/superadmin/error_404');
        }
    	if(isset($_POST['update']))
    	{
	    	
	  	    $this->form_validation->set_rules('user_name_title', 'Doctor title', 'required');
	        $this->form_validation->set_rules('physician_first_name', 'Doctor first name', 'required');
	        $this->form_validation->set_rules('physician_last_name', 'Doctor last name', 'required');
	        $this->form_validation->set_rules('email_address', 'User Email', "trim|required|unique[users.user_email,users.user_id,$id]"); 
	        
	        $this->form_validation->set_rules('physician_gender', 'Doctor Gender', 'required');
	        $this->form_validation->set_rules('traning_year', 'Doctor Traning Year', 'required');
	        $this->form_validation->set_rules('physician_dob_date', 'Doctor DOB date', 'required');
	        $this->form_validation->set_rules('physician_dob_month', 'Doctor DOB month', 'required');
	        $this->form_validation->set_rules('physician_dob_year', 'Doctor DOB year', 'required');
	        $this->form_validation->set_rules('physician_profession', 'Doctor profession', 'required');
	        $this->form_validation->set_rules('medical_specialty', 'Medical Specialty', 'required');
	        $this->form_validation->set_rules('medical_school', 'Medical School', 'required');
	        if($this->input->post('physician_minc'))
	        {
	        	 $this->form_validation->set_rules('physician_minc', 'Doctor minc', 'required|max_length[10]');
	        }
	       

	      
	  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	        if ($this->form_validation->run() == TRUE)  {
				    $user_data=array(
						'user_name_title' => $this->input->post('user_name_title'),
						'user_first_name' => trim($this->input->post('physician_first_name')),
						'user_last_name'  => trim($this->input->post('physician_last_name')),
						'user_email'      => trim($this->input->post('email_address')),
						'user_role'       => 2,
						'user_updated'    => date('Y-m-d H:i:s A'),
						
					);
			
				    $update = $this->common_model->update('users',$user_data,array('user_id'=>$id)); 
				
						$physician_data          = array(
						
						'physician_gender'         => $this->input->post('physician_gender'),
						'physician_dob_date'       => $this->input->post('physician_dob_date'),
						'physician_dob_month'      => $this->input->post('physician_dob_month'),
						'physician_dob_year'       => $this->input->post('physician_dob_year'),
						'physician_profession'     => $this->input->post('physician_profession'),
						'medical_specialty'        => $this->input->post('medical_specialty'),
						'medical_school'           => $this->input->post('medical_school'),
						'traning_year'             => $this->input->post('traning_year'),
						'physician_minc'           => $this->input->post('physician_minc'),
						'physician_country'        => $this->input->post('physician_country'),
						'physician_state'          => $this->input->post('physician_state'),
						'physician_city'           => $this->input->post('physician_city'),
						'physician_address'        => $this->input->post('physician_address'),
						'physician_postal_code'    => $this->input->post('physician_postal_code'),
						'physician_phone_number'   => $this->input->post('physician_phone_number'),
						'medical_selectSpecialty'  => $this->input->post('medical_selectSpecialty'),
						'physician_selectState'    => $this->input->post('physician_selectState'),
						'physician_default_msg'    => $this->input->post('physician_default_msg'),
						);
					$update_val = $this->common_model->update('physicians',$physician_data,array('user_id'=>$id));
				
				
				if($update && $update_val)
	            {
	                $message = 'Doctor detail updated Successfully.';
	                $this->session->set_flashdata('msg_success',$message);
	                redirect('backend/listusers/physician');
	            }
	            else
	            {
	                $message =  "Doctor detail not Update.";
	                $this->session->set_flashdata('msg_error',$message);
	            }
			}

		}


    	if(isset($_POST['change_password']))
    	{
    		
			$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|matches[confpassword]');
			$this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == TRUE){
				$salt = $this->salt();
				$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('newpassword')))));
				if($this->common_model->update('users',$user_data,array('user_id'=>$id)))
				{
					$this->session->set_flashdata('msg_success','Password updated successfully.');
				}else{
					$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			
				}
			}	
		}
		$data['currently_statu'] = currently_status();
    	$data['medical_school'] =$this->common_model->get_result('medical_specialties_annex1',array('status'=>1),'',array('name','asc'));
    	$data['medical_specialty'] = $this->common_model->get_result('medical_specialties_annex2',array('status'=>1),'',array('name','asc'));
    	$data['canada_province'] =$this->common_model->get_province('location_complete_data',array(),'','','province_name');
    	$data['physician_content'] =$this->common_model->get_result('physician_medical_facility_content',array('status'=>1,'type'=>1),'',array('order_by','asc'));
    	

    	$data['template'] = 'backend/user/edit_physician';
		$this->load->view('templates/superadmin_template',$data);
    }
    function edit_medical($id='')
    {
        $id = $this->uri->segment(4);
        if($id == '' || !is_numeric($id)){ 
       		redirect('backend/superadmin/error_404');
        }
        $row_get = $this->common_model->count_rows('users', array('user_id'=>$id),'','');
        if($row_get < 1){ 
       	 	redirect('backend/superadmin/error_404');
        }
        $data['user_details'] =  $this->common_model->get_row('users',array('user_id'=>$id,'user_role'=>1));

        if(empty($data['user_details']))
        {
        	redirect('backend/superadmin/error_404');
        }
    	if(isset($_POST['update']))
    	{
	    	


	  	    $this->form_validation->set_rules('user_name_title', 'Medical title', 'required');
	        $this->form_validation->set_rules('medical_first_name', 'Medical first name', 'required');
	        $this->form_validation->set_rules('medical_last_name', 'Medical last name', 'required');
	        $this->form_validation->set_rules('email_address', 'User Email', "trim|required|unique[users.user_email,users.user_id,$id]");
	        
	       
	  	    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	        if ($this->form_validation->run() == TRUE)  {
				    $user_data=array(
						'user_name_title' => $this->input->post('user_name_title'),
						'user_first_name' => trim($this->input->post('medical_first_name')),
						'user_last_name'  => trim($this->input->post('medical_last_name')),
					    'user_email'      => trim($this->input->post('email_address')),
					);
			
				    $update = $this->common_model->update('users',$user_data,array('user_id'=>$id)); 
				
						
				if($update)
	            {
	                $message = 'Medical User detail updated Successfully.';
	                $this->session->set_flashdata('msg_success',$message);
	                redirect('backend/listusers/medical');
	            }
	            else
	            {
	                $message =  "Medical User detail not Update.";
	                $this->session->set_flashdata('msg_error',$message);
	            }
			}

		}
		
    	if(isset($_POST['change_password']))
    	{
    		
			$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|matches[confpassword]');
			$this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == TRUE){
				$salt = $this->salt();
				$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('newpassword')))));
				if($this->common_model->update('users',$user_data,array('user_id'=>$id)))
				{
					$this->session->set_flashdata('msg_success','Password updated successfully.');
				}else{
					$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			
				}
			}	
		}
    	
    	$data['template'] = 'backend/user/edit_medical';
		$this->load->view('templates/superadmin_template',$data);
    }
    function salt(){
		return substr(md5(uniqid(rand(), true)), 0, 10);
	}
	function email_check($str=''){
	  if($this->common_model->get_row('users',array('user_email'=>$str))):
	    $this->form_validation->set_message('email_check', 'The %s address already exists.');
	       return FALSE;
	    else:
	       return TRUE;
	    endif;
	}
	function postjob($offset=0)
	{

		$per_page=25 ;
    	$data['offset']=$offset;
    	$config=backend_pagination();
    	$config['base_url'] = site_url().'backend/listusers/postjob/';
	    $config['total_rows'] = $this->user_model->postjob_result(0,0);
	    $config['per_page'] = $per_page;
    	$config['uri_segment'] = 4;
    	
	    $config['per_page'] = $per_page;
	    $data['result_data'] = $this->user_model->postjob_result($offset,$per_page);
		if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	    $data['total_records'] = $config['total_rows'];
	    if($config['total_rows'] < $offset)
	    {
	       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
	       redirect('backend/listusers/postjob/0');
	    }
	    //$config['first_url'] = $config['base_url'].$config['suffix'];
	    $this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();  
	    if(isset($_POST['apply']))
		{ 
			$act_typ = $this->input->post('act_type');
			$all_id = $this->input->post('check_id');

			if($act_typ == 5)
			{
				if(!empty($all_id))
				{
					foreach($all_id as $all_i)
					{  
						
						$this->common_model->delete('post_jobs',array('id'=> $all_i));
						
					}
					$this->session->set_flashdata('msg_success','Post Job details Delete successfully.');
					redirect('backend/listusers/postjob'); 
				}
				               
			}
			elseif($act_typ ==2)
			{
				$this->session->set_flashdata('msg_success','Post Job Deactivate successfully.');
			}
			elseif($act_typ ==1){
				$this->session->set_flashdata('msg_success','Post Job Activate successfully.');
			}elseif($act_typ ==3){
				$this->session->set_flashdata('msg_success','Post Job Banned successfully.');
			}elseif($act_typ ==4){
				$this->session->set_flashdata('msg_success','Post Job Pending successfully.');
			}
			if($act_typ == 4){
			 $data_update['status'] = 0;
			}else{
				$data_update['status'] = $act_typ;
			}
	
			if(!empty($all_id))
			{
				foreach($all_id as $all_i)
				{ 
					
					if($act_type == 1)
		            {
		                $posted_date = date('Y-m-d h:i:s');
		                $posted_data = array('posted'=> $posted_date);
		                $posted_where = array('id'=> $all_i);
		                $this->common_model->update('post_jobs',$posted_data,$array = array('id' =>$all_i));
		            }
					$res =$this->common_model->update('post_jobs',$data_update,$array = array('id' =>$all_i));
					
					//exit;
				}
			}	
			   
			redirect('backend/listusers/postjob');   
		
		} 
		$data['template'] = 'backend/user/postjob';
		$this->load->view('templates/superadmin_template',$data);
	}
	function change_status_postjob()
    {
        $id = $this->uri->segment(4);
        $value = $this->uri->segment(5);
        $state_status = '';
        $state_status = $this->uri->segment(6);
        if($value == 1)
      	{
	        $posted_date = date('Y-m-d h:i:s');
	        $posted_data = array('posted'=> $posted_date);
	        $posted_where = array('id'=> $id);
	        $this->common_model->update('post_jobs',$posted_data,$array = array('id' =>$id));
	        

      	}
      	

      	if(!empty($state_status ) &&  $state_status  == 5)
	      {
	        if($value == 1 || $value ==2 || $value ==3)
	        {
	            $row_get_count = $this->common_model->count_rows('payment_city', array('job_id'=>$id ),'','');
	            if($row_get_count > 0)
	            {
	               $effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime(date("Y/m/d"))));
                   $update_data = array('payment_status'=>1,'valid_date'=>$effectiveDate);
	               $this->common_model->update('payment_city',$update_data ,$array = array('job_id' =>$id));

	            }
	           
	        }
	      }
	       if($id)
	       {
		    	$job_detail = '';
		    	$job_detail = get_job_detail($id);
		    	$get_info_user = get_user_info($job_detail->user_id);
		    	$get_info_user->user_role;

		    	if(!empty($get_info_user))
		    	{
			    	if($get_info_user->user_role ==1)
			    	{
			            $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
						$email_template = $this->common_model->get_row('email_templates',array('id'=>4));
						$status = '';
				    	$status = get_job_status($value) ;
				    	$image_url = base_url().'assets/front/images/click-here-7.png';
				    	$param=array
				    		(
								
								
								'template'  =>  
								array(
							    		'temp'  =>  $email_template->template_body,
						        		'var_name'  =>  
						        					array(
					  										'username'    => $username,
					  										'site_url' => site_url(),
					  										'job_id'    => $id,
			                                                'job_name'  => get_job_name($id),
															'status' => $status,
															'image_url'=> $image_url
														),   
								),   
								'email' =>  
							    array(
							            'to'        =>   $get_info_user->user_email, 
							            'from'      =>   GLOBAl_INFO_EMAIL,
							            'from_name' =>   GLOBAl_INFO_EMAIL,
							            'subject'   =>   $email_template->template_subject,
							     ),
							);
						  
				    	$status=$this->chapter247_email->send_mail($param); 
				    }	
			    }	
		    }	




        $con = array('id'=>$id);
        $update_data = array('status'=>$value);
        $this->common_model->update('post_jobs',$update_data,$con);
    	$this->session->set_flashdata('msg_success','Post Job status changed successfully.');
    	redirect($_SERVER['HTTP_REFERER']);     
    }
    
    function postjob_detail()
    {
        
        $id = $this->uri->segment(4);
        $con = array('id'=>$id);
  		$data['post_data'] = $this->common_model->get_row('post_jobs',array('id'=>$id));
  		//echo $data['post_data']->user_id;
//exit();
  		if(empty($data['post_data'])){
  			redirect('backend/listusers/postjob');  	     
  		}else{
  				$user_id = $data['post_data']->user_id;
  				$data['receipts_detail'] = $this->user_model->receipts_information_job($user_id);  		
		  		$data['template'] = 'backend/user/postjob_detail';
				$this->load->view('templates/superadmin_template',$data);
  	     }
  	     
  	     		
  		
        
    }
    function payment($offset=0)
    {
        $per_page= 25;
    	$data['offset']=$offset;
	    $data['result_data'] = $this->user_model->all_receipts_information($offset,$per_page);
    	$config=backend_pagination();
    	$config['base_url'] = site_url().'backend/listusers/payment/';
	    $config['total_rows'] = $this->user_model->all_receipts_information(0,0);
    	$config['per_page'] = $per_page;
    	$config['uri_segment'] = 4;
	    if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	   	 if($config['total_rows'] < $offset){
           $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');  	
           redirect('backend/listusers/payment/0');
    	}
	   	$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links(); 
  		$data['template'] = 'backend/user/payment_history';
		$this->load->view('templates/superadmin_template',$data);
        
    }

}
