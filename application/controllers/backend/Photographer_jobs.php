<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Photographer_jobs extends CI_Controller {
	public function __construct(){
		  ob_start();
	    parent::__construct();  
	     clear_cache();  
	    $this->load->model('superadmin_model');
	    $this->load->model('common_model');
	    $this->load->model('user_model');
	    if(!photographer_logged_in())
        {
        	redirect('backend/photographer_login');
        }         
	}
	public function index($offset=0)
  	{
  		
	    $per_page=25 ;
    	$data['offset']=$offset;
    	$config=backend_pagination();
    	$config['base_url'] = site_url().'backend/photographer_jobs';
	    $config['total_rows'] = $this->user_model->photographer_jobs_result(0,0);
	    $config['per_page'] = $per_page;
    	$config['uri_segment'] = 4;
	    $config['per_page'] = $per_page;
	    $data['result_data'] = $this->user_model->photographer_jobs_result($offset,$per_page);
		if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	    $data['total_records'] = $config['total_rows'];
	    if($config['total_rows'] < $offset)
	    {
	       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
	       redirect('backend/photographer_jobs/0');
	    }
	    //$config['first_url'] = $config['base_url'].$config['suffix'];
	    $this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();  
	    $data['template']='backend/photographer_login/jobs/photographer_jobs';
	    $this->load->view('templates/photographer_template',$data);

    }
    function photograph_edit(){
    	$id = $this->uri->segment(4);
        if($id == '' || !is_numeric($id)){ 
       		redirect('backend/photographer_login/error_404');
        }
        
        $row_get = $this->common_model->count_rows('assign_user', array('assign_name'=>photographer_id(),'job_id'=>$id),'','');
         if($row_get < 1){ 
       	 	redirect('backend/photographer_login/error_404');
         }
       
    	$data = '';
    	$check_img = '';
    	
    	$image = '';
		$data['jobs'] = $this->user_model->get_row('post_jobs',array('id'=>$id));
		if(!empty($data['jobs']))
		$check_img =  unserialize($data['jobs']->uploadImageArray);
		//echo '<pre>'; print_r($check_img);
		if(isset($_POST['submit']))
		{
			$get_files = array();
			$i=0;
			if(!empty($_FILES))
			{
			    $image = $_FILES['job_image'];
			    if(array_key_exists('name',$image))
			    {
			    	
			    	foreach($image['name'] as $val)
			    	{
			    		
			    		if(empty($val))
			    		{
			    			$i++;
			    		}
			    	}
			    	if($i == 5)
			    	{
			    		$filtered = '';
			    	}else{
			    		$filtered = 1;
			    	}
			    }
			} 
			
		    if(empty($filtered) && empty($check_img))
		    {
		     	
		     	$this->session->set_flashdata('msg_error','Please select images.');
		      	redirect('backend/photographer_jobs/photograph_edit/'.$id);

		    }else{
   			
   				$serialize_data = '';
   				$get_files = '';
   				$id = $this->input->post('job_id');
   				$get_files = $this->photographer_image_check();
   				if($get_files)
   				{
   					$full_image = '';
		    		foreach($get_files as $image_name)
		    		{
			    		
			    		foreach($image_name as $img_name)
			    		{
				    		//exit;
				    		$existing_image_name = $img_name;
						    $title_name = $data['jobs']->listing_title;
						    $list_name = url_title($title_name);
						    $extension = end((explode('.', $existing_image_name)));
						    $new_image_name= $list_name.'_'.strtotime("now").'_'.rand(1000,10000).'.'.$extension;
						    $old_image_path = './assets/uploads/jobs/'.$existing_image_name;
						    $new_image_path = './assets/uploads/jobs/'.$new_image_name;
					    	if(file_exists($old_image_path))
					    	{
						   
					    		rename($old_image_path , $new_image_path);
					    	}
					    	$full_image[] = array('imageName' =>$new_image_name); 
					    }	
				    }
	    			
   					$serialize_data = serialize($full_image);
   					$update = $this->common_model->update('post_jobs',array('uploadImageArray'=>$serialize_data),array('id'=>$id));
   					$this->session->set_flashdata('msg_success','Image update  successfully.');
   				}
   				redirect('backend/photographer_jobs/photograph_edit/'.$id);
   			}
		}
    	$data['template']='backend/photographer_login/jobs/photograph_edit';
	    $this->load->view('templates/photographer_template',$data);
    }
    function photographer_image_check($str='')
  	{ 

	   	
	   	if(empty($_FILES)){
	        $this->form_validation->set_message('photographer_image_check', 'Choose Image');
	        return FALSE;
	    }
	    $uploadData = array();
		$quality = 90;
		if (!empty($_FILES)):

  			$min = 0;
			$max = 10485760;
			$numbers =  $_FILES[ 'job_image' ][ 'size' ];
  			$newNumbers = array_filter(
   			 $numbers,
			    function ($value) use($min,$max) {
			        return ($value > $max);
			    }
			);
			
			if(!empty($newNumbers))
			{
				foreach($newNumbers as $newNum)
				{
					if(!empty($newNum))
					{
						$this->session->set_flashdata('msg_error','You can not upload max file size 10mb.');
		      	  		return FALSE;
					}
				}
			}
			
  			
			$filesCount = count($_FILES['job_image']['name']);
			if(!empty($filesCount))
			{
				$j = 0;
				$size_img = '';
				foreach($_FILES['job_image']['tmp_name'] as $size_img)
				{
					
					if(!empty($size_img))
					{
						$image = getimagesize($size_img);
					    if ($image[0] < 700 || $image[1] < 400) {
					        $this->session->set_flashdata('msg_error','Oops! Your image needs to be atleast 700 x 400 pixels.');
		      	  			return FALSE;
					    }
					    if ($image[0] > 2000 || $image[1] > 2000) {
					        $this->session->set_flashdata('msg_error','Oops! Your image needs to be maximum of 2000 x 2000 pixels.');
		      	  			return FALSE;
					    }
					}    
					

				}
			}
				
            for($i = 0; $i < $filesCount; $i++){
               	$name = '';
               	$total_img = '';
               	$tempPath = '';
               	$info = '';
               	$width = '';
               	$height = '';
               	$type = '';
               	$attr = '';
               	$origWidth = '';
               	$origHeight = '';
               	$image = '';
                $tempPath = $_FILES[ 'job_image' ][ 'tmp_name' ][$i];
	    		$name = $_FILES[ 'job_image' ]['name' ][$i];
	    		if(!empty($tempPath))
	    		{
	    			$info = list($width, $height, $type, $attr) = getimagesize($tempPath);
	    			 $destination_url = './assets/uploads/jobs/'.$name;
					if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($tempPath);
					elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($tempPath);
					elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($tempPath);
					$origWidth = $width;
					$origHeight = $height;
					$maxWidth = 900;
					$maxHeight = 600;
					if ($maxWidth == 0)
				    {
				        $maxWidth  = $height;
				    }

				    if ($maxHeight == 0)
				    {
				        $maxHeight = $width;
				    }
				    if($origWidth <=0)
				    {
				    	$origWidth = 1;
				    }
				    if($origHeight <=0)
				    {
				    	$origHeight = 1;
				    }
				    // Calculate ratio of desired maximum sizes and original sizes.
				    $widthRatio = $maxWidth / $origWidth;
				    $heightRatio = $maxHeight / $origHeight;

				    // Ratio used for calculating new image dimensions.
				    $ratio = min($widthRatio, $heightRatio);

				    // Calculate new image dimensions.
				    $newWidth  = (int)$origWidth  * $ratio;
				    $newHeight = (int)$origHeight * $ratio;

				    // Create final image with new dimensions.
				    $newImage = imagecreatetruecolor($newWidth, $newHeight);
				    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
				    if(imagejpeg($newImage, $destination_url, $quality))
				    {
				    	$uploadData[$i]['imageName'] = $name;
				    }

	    		}else{
	    			$total_img = $_POST['all_image'][$i];
	    			if(!empty($total_img))
	    			{
	    				$uploadData[$i]['imageName'] = $total_img;
	    			}

	    		}
            }
            return $uploadData;
		       
		
	    else:
	        $this->form_validation->set_message('photographer_image_check', 'The %s field required.');
	        return FALSE;
	    endif;
    }
    function delete_image(){
    	$id = $this->uri->segment(4);
    	$index = '';
    	$remove_indx = '';
    	$index = $this->uri->segment(5);
    	$remove_indx = $index - 1;
    	$final_array = '';
    	$get_final_data = '';
        if($id == '' || !is_numeric($id)){ 
       		redirect('backend/photographer_login/error_404');
        }
    	$data = '';
		$jobs = $this->user_model->get_row('post_jobs',array('id'=>$id));
		if(!empty($jobs))
		{
			$image = unserialize($jobs->uploadImageArray);
			$final_array = array_splice($image, $remove_indx, 1);
			$get_final_data = serialize($image);
			$update = $this->common_model->update('post_jobs',array('uploadImageArray'=>$get_final_data),array('id'=>$id));
			 $this->session->set_flashdata('msg_success','Image removed  successfully.');
		}else{
			$this->session->set_flashdata('msg_success','Image not removed  successfully.');
		}

		redirect($_SERVER['HTTP_REFERER']);
    }
    function marked_complete(){
    	$id = $this->uri->segment(4);
    	$send_mail = ADMIN_EMAIL;
    	$row_get = '';	
		$row_get = $this->common_model->count_rows('assign_user', array('job_id'=>$id,'marked_complete'=>1),'','');
		$job_detail = get_job_detail($id);
		if(!empty($id))
		{
	    	if(empty($row_get))
			{
				
				$sender_info_user = get_user_info(photographer_id());
		        $sender_name = $sender_info_user->user_first_name.' '.$sender_info_user->user_last_name;
		        $sender_id = $sender_info_user->user_id;
		        $job_user_info = get_user_info($job_detail->user_id);
		        $job_user_name = $job_user_info->user_first_name.' '.$job_user_info->user_last_name;
				$email_template = $this->common_model->get_row('email_templates',array('id'=>12));
				$job_name = get_job_name($id);
				$image_url = base_url().'assets/front/images/click-here-7.png';
			    $param=array
			    		(
							'template'  =>  
							array(
						    		'temp'  =>  $email_template->template_body,
					        		'var_name'  =>  
					        					array(
				  										'job_id'    => $id,
				  										'job_name'  => $job_name,
				  										'site_url' => site_url(),
				  										'sender_id' => $sender_id,
				  										'sender_name' => $sender_name,
				  										'image_url' => $image_url
														
													),   
							),   
							'email' =>  
						    array(
						            'to'        =>   $send_mail, 
						            'from'      =>   GLOBAl_INFO_EMAIL,
						            'from_name' =>   GLOBAl_INFO_EMAIL,
						            'subject'   =>   $email_template->template_subject,
						     ),
						);   
			    $status=$this->chapter247_email->send_mail($param);
			    $email_template_user = $this->common_model->get_row('email_templates',array('id'=>16));
				$job_name = get_job_name($id);
			    $param_user =array
			    		(
							'template'  =>  
							array(
						    		'temp'  =>  $email_template_user->template_body,
					        		'var_name'  =>  
					        					array(
				  										'username' =>  $job_user_name,
				  										'job_id'    => $id,
				  										'job_name'  => $job_name,
				  										'site_url' => site_url(),
				  										'sender_id' => $sender_id,
				  										'sender_name' => $sender_name,
				  										'image_url'=> $image_url
														
													),   
							),   
							'email' =>  
						    array(
						            'to'        =>    $job_user_info->user_email, 
						            'from'      =>   GLOBAl_INFO_EMAIL,
						            'from_name' =>   GLOBAl_INFO_EMAIL,
						            'subject'   =>   $email_template_user->template_subject,
						     ),
						);  

			    	//echo '<pre>'; print_r( $param_user);

			    $status1=$this->chapter247_email->send_mail($param_user);
			   
			    if($status)
			    {
			    	$update = $this->common_model->update('assign_user',array('marked_complete'=>1),array('job_id'=>$id));
			    	$this->session->set_flashdata('msg_success','Marked complete and sent notification to the admin.');
			    }else{
			    	$this->session->set_flashdata('msg_error','Notification not send successfully.');
			    }
			   
			}else{
				$this->session->set_flashdata('msg_error','You have already Marked completed and sent notification to the admin.');
			}
		}else{
			$this->session->set_flashdata('msg_error','Id not found.');
		}
		redirect($_SERVER['HTTP_REFERER']); 	
    }
    
}
?>