<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Payment_receipts extends CI_Controller {
	public function __construct()
   	{
        parent::__construct();
       // Your own constructor code
       $this->load->model('common_model');
       $this->load->model('user_model');
       if(!superadmin_logged_in())
        {
        	redirect('backend/superadmin');
        }
       
    }
	/***************************************************************************
	 * Function for show list of all users with filteration options
	 ***************************************************************************/

	function payment_list($offset=0)
	{
    
    $per_page=25;
    $data['offset']=$offset;
    $data['result_data'] = $this->user_model->all_receipts_information_for_payment_receipts($offset,$per_page);
    $config=backend_pagination();
    $config['base_url'] = site_url().'backend/payment_receipts/payment_list/';
    $config['total_rows'] = $this->user_model->all_receipts_information_for_payment_receipts(0,0);
    $config['per_page'] = $per_page;
    $config['uri_segment'] = 4;
    if(!empty($_SERVER['QUERY_STRING'])){
     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
    }
    else{
     $config['suffix'] ='';
    }
    $config['first_url'] = $config['base_url'].$config['suffix'];
    if($config['total_rows'] < $offset){
      $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
      redirect('backend/payment_receipts/payment_list/0');
    }
    $this->pagination->initialize($config);
    $data['pagination']=$this->pagination->create_links(); 
    $data['template'] = 'backend/payment/payment_history';
    $this->load->view('templates/superadmin_template',$data);
  }
  function change_status()
  {
    $id = $this->uri->segment(4);
    $value = $this->uri->segment(5);
    $status = $this->uri->segment(6);
    if($status == 1 || $status == 2 || $status == 3 )
    {
     $con = array('unique_id'=>$id);
     $effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime(date("Y/m/d"))));
     $update_data = array('payment_status'=>$value,'valid_date'=>$effectiveDate);
     $this->common_model->update('payment_city',$update_data,$con);
     $this->session->set_flashdata('msg_success','Payment Status changed successfully.');
    }   
    else
    {
      $this->session->set_flashdata('msg_error',"Please check job status, if status is pending or draft then you can't change status.");
    }
    redirect($_SERVER['HTTP_REFERER']);
          
  }
  function receipts_id_post()
  {
    $id = $this->uri->segment(4);
    if($id == '' || !is_numeric($id)){ 
      redirect('backend/superadmin/error_404');
    }
    $row_get = $this->common_model->count_rows('payment', array('payment_id'=>$id),'','');
    if($row_get < 1){ 
      redirect('backend/superadmin/error_404');
    }
    $this->response_array['response_code'] = 500;
    $this->response_array['message'] = "Invalid Form Request";
    if(!empty($id))
    {
      $pay_unq = '';
      $jobs_detail =$this->common_model->receipts_payment($id
          );
      $pay_unq = $jobs_detail['unique_id'];
      $city_detail =$this->common_model->city_payment($pay_unq
          );
      if(!empty($city_detail))
      {
        $data['city_detail'] = $city_detail;
      }
      if($jobs_detail['job_verify'] == 1)
      {
        $ver = 'Yes';
      }else{
        $ver = 'No';
      }
      $y_amount = 0;
      if(!empty($jobs_detail['yolomd_verify']))
      {
        $y_amount = $jobs_detail['yolomd_verify'];
      }
      $date_created = date('d M y',strtotime($jobs_detail['date_time']));
      $user_info = '';
      

    
      $data['jobs_detail'] = 
      array(
              'item' => ucfirst($jobs_detail['item']),
              'job_id' => $jobs_detail['id'],
              'job_title' => $jobs_detail['listing_title'],
              'verify' => $ver,
              'payment_amount' => $jobs_detail['payment_amount'],
              'discount' => $jobs_detail['discount'],
              'yolomd_verify' => '$'.$jobs_detail['yolomd_verify'],
              'date' => $date_created,
              'transaction_id' => $jobs_detail['txn_id'],
              'unique_id'=> $jobs_detail['unique_id'],
              'user_id' => $jobs_detail['user_id'],
              'job_status' => $jobs_detail['status']
            );
     
      $data['template'] = 'backend/payment/receipts_download';
      $this->load->view('templates/superadmin_template',$data);
    }    
  }

}