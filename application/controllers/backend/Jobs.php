<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Jobs extends CI_Controller {
	public function __construct()
 	{
    parent::__construct();
     // Your own constructor code
    $this->load->model('common_model');
    $this->load->model('user_model');
    $this->load->helper('dev');
    if(!superadmin_logged_in())
    {
      redirect('backend/superadmin');
    }
     
  }
	/***************************************************************************
	 * Function for show list of all users with filteration options
	 ***************************************************************************/
  function index($offset=0)
  {
    
    $per_page=25 ;
    $data['offset']=$offset;
    $config=backend_pagination();
    $config['base_url'] = site_url().'backend/jobs/index';
    $config['total_rows'] = $this->user_model->postjob_result(0,0);
    $config['per_page'] = $per_page;
    $config['uri_segment'] = 4;
    
    $config['per_page'] = $per_page;
    $data['result_data'] = $this->user_model->postjob_result($offset,$per_page);
    if(!empty($_SERVER['QUERY_STRING'])){
      $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
    }
    else{
      $config['suffix'] ='';
    }
    $config['first_url'] = $config['base_url'].$config['suffix'];
    $data['total_records'] = $config['total_rows'];
    if($config['total_rows'] < $offset)
    {
       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
       redirect('backend/jobs/index/0');
    }
    $this->pagination->initialize($config);
    $data['pagination']=$this->pagination->create_links();  
    if(isset($_POST['apply']))
    { 
      $act_typ = '';
      $act_typ = $this->input->post('act_type');
      $all_id = $this->input->post('check_id');
      if($act_typ == 5)
      {
        if(!empty($all_id))
        {
          foreach($all_id as $all_i)
          {  
            
            $this->common_model->delete('post_jobs',array('id'=> $all_i));
            
          }
          $this->session->set_flashdata('msg_success','Post Job details delete successfully.');
          redirect($_SERVER['HTTP_REFERER']);   
        }
                       
      }
      elseif($act_typ ==2)
      {
        $this->session->set_flashdata('msg_success','Post Job Deactivate successfully.');
      }
      elseif($act_typ ==1){
        $this->session->set_flashdata('msg_success','Post Job Activate successfully.');
      }elseif($act_typ ==3){
        $this->session->set_flashdata('msg_success','Post Job Banned successfully.');
      }elseif($act_typ ==4){
        $this->session->set_flashdata('msg_success','Post Job Pending successfully.');
      }
      if($act_typ == 4){
       $data_update['status'] = 0;
      }else{
        $data_update['status'] = $act_typ;
      }
      
      if(!empty($all_id))
      {
        foreach($all_id as $all_i)
        { 
          
          if(!empty($_GET['status']) && $_GET['status'] == 5)
          {
            if($act_typ == 1 || $act_typ ==2 || $act_typ ==3)
            {
                $row_get_count = $this->common_model->count_rows('payment_city', array('job_id'=>$all_i),'','');
                if($row_get_count > 0)
                {
                   
                   $effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime(date("Y/m/d"))));
                   $update_data = array('payment_status'=>1,'valid_date'=>$effectiveDate);
                   $this->common_model->update('payment_city', $update_data ,$array = array('job_id' =>$all_i));

                }
               
            }
          }
  
          if(!empty($act_type) &&  $act_type == 1)
          {
              $posted_date = date('Y-m-d h:i:s');
              $posted_data = array('posted'=> $posted_date);
              $posted_where = array('id'=> $all_i);
              $this->common_model->update('post_jobs',$posted_data,$array = array('id' =>$all_i));
          }
          $res =$this->common_model->update('post_jobs',$data_update,$array = array('id' =>$all_i));

        }
      } 
      redirect($_SERVER['HTTP_REFERER']);     
    } 
    $data['template'] = 'backend/jobs/postjob';
    $this->load->view('templates/superadmin_template',$data);
  }
	
  function postjob_detail()
  {
        
    $id = $this->uri->segment(4);
    if($id == '' || !is_numeric($id)){ 
      redirect('backend/superadmin/error_404');
    }
    $row_get = $this->common_model->count_rows('post_jobs', array('id'=>$id),'','');
    if($row_get < 1){ 
      redirect('backend/superadmin/error_404');
    }
    $con = array('id'=>$id);
    $data['post_data'] = $this->common_model->get_row('post_jobs',array('id'=>$id));
    $data['receipt_all'] = $this->common_model->get_result('payment',array('job_id'=>$id),array('payment_amount,txn_id,date_time,payment_id'),array('payment_id','desc'));
   
    if(empty($data['post_data'])){
      redirect('backend/jobs');         
    }else
    {
      $user_id = $data['post_data']->user_id;
      $data['user'] = $this->common_model->get_row('users',array('user_id'=>$user_id));
      $data['receipts_detail'] = $this->user_model->receipts_information_job($user_id);     
      $data['template'] = 'backend/jobs/postjob_detail';
      $this->load->view('templates/superadmin_template',$data);
    }
  }
  
  function delete_postjob()
  {
      
    $id = $this->uri->segment(4);
    $con = array('id'=>$id);
    $this->common_model->delete('post_jobs',$con);
    $this->session->set_flashdata('msg_success','Post Job delete successfully.');
    redirect($_SERVER['HTTP_REFERER']);   
      
  }
  function change_status_postjob()
  {
    $id = $this->uri->segment(4);
    $value = $this->uri->segment(5);

    if($value == 1)
    {
      $posted_date = date('Y-m-d h:i:s');
      $posted_data = array('posted'=> $posted_date);
      $posted_where = array('id'=> $id);
      $this->common_model->update('post_jobs',$posted_data,$array = array('id' =>$id));
    }
    $con = array('id'=>$id);
    $update_data = array('status'=>$value);
    $this->common_model->update('post_jobs',$update_data,$con);
    $this->session->set_flashdata('msg_success','Post Job status changed successfully.');
    redirect($_SERVER['HTTP_REFERER']);          
  }
}
?>