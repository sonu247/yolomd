<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Messages extends CI_Controller {
	public function __construct(){
	    parent::__construct();  
	     clear_cache();  
	    $this->load->model('superadmin_model'); 
      $this->load->model('common_model');
      $this->load->model('user_model');
      if(!superadmin_logged_in())
      {
        redirect('backend/superadmin');
      }        
	}
  public function index($offset=0)
  {
    
    $per_page=25;
    $data['offset']=$offset;
    $data['messages'] = $this->superadmin_model->get_all_message_flagged($offset,$per_page);
    $config=backend_pagination();
    $config['base_url'] = base_url().'backend/messages/index';
    $config['total_rows'] = $this->superadmin_model->get_all_message_flagged(0,0);
    $config['per_page'] = $per_page;
    $config['uri_segment'] = 4;
    if(!empty($_SERVER['QUERY_STRING'])){
     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
    }
    else{
     $config['suffix'] ='';
    }
    $config['first_url'] = $config['base_url'].$config['suffix'];
    if($config['total_rows'] < $offset)
    {
       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
       redirect('backend/messages/index/0');
    }
    
    $this->pagination->initialize($config);
    $data['pagination']=$this->pagination->create_links();  
    $data['template'] = 'backend/messages/index';
    $this->load->view('templates/superadmin_template',$data);
  }
    
  function change_status()
  {
      $id = $this->uri->segment(4);
      $status = $this->uri->segment(5);
      $con = array('msg_id'=>$id);

      $update_data = array('admin_marked'=>$status);
      $this->common_model->update('jobs_messages',$update_data,$con);

      $first_msg = $this->common_model->get_row('jobs_messages',array('msg_id'=>$id));
      if (!empty($first_msg) && !empty($get_info_user = get_user_info($first_msg->sender_id))) {

        $email_template = $this->common_model->get_row('email_templates',array('id'=>10));
        $param=array(
              'template'  =>  
              array(
                'temp'  =>  $email_template->template_body,
                'var_name'  =>  
                      array(
                      'name'        => $get_info_user->user_first_name,
                      'text'        => $first_msg->msg_body,
                      'posttitle'   => get_job_name($first_msg->job_id),
                      'status'      => 'marked',
                    ),   
              ),   
              'email' =>  
                array(
                        'to'        =>   $get_info_user->user_email, 
                        'from'      =>   GLOBAl_INFO_EMAIL,
                        'from_name' =>   GLOBAl_INFO_EMAIL,
                        'subject'   =>   $email_template->template_subject,
                 ),
            );   
        $this->chapter247_email->send_mail($param);  
      }
      $this->session->set_flashdata('msg_success','Message Marked successfully.');
      redirect($_SERVER['HTTP_REFERER']);
              
  }
  function get_message_detail($msg_id='')
  {
    if($msg_id == '' || !is_numeric($msg_id)){ 
      redirect('backend/superadmin/error_404');
    }
    
     $row_get = $this->common_model->count_rows('jobs_messages', array('msg_id'=>$msg_id, 'flag'=>1),'','');
      if($row_get < 1){ 
          redirect('backend/superadmin/error_404');
      }
    $data['first_msg'] = '';
    $first_msg = $this->common_model->get_row('jobs_messages',array('msg_id'=>$msg_id));
    $messages = $this->user_model->get_messages_detail_admin($msg_id);
   
    $data['messages_data'] = '';
    $data['msg_subject'] = '';
    if($messages && $first_msg)
    {
       
      $msg_subject='';
      foreach ($messages as $value)
      {
        if($value->msg_subject )
        {
          $msg_subject=$value->msg_subject ;
        }
        $messages_data[] = (object)array(
          'msg_body'    => $value->msg_body,
          'date_time'   => date('d M y H:i:s',strtotime($value->date_time)),
          'sender_id' => $value->sender_id,
          'receiver_id' => $value->receiver_id
        ); 
      }
     
      $data['messages_data'] = $messages_data;
      $data['msg_subject'] = $msg_subject;
      $data['first_msg'] = $first_msg;
     
    }
    $data['template'] = 'backend/messages/msg_detail';
    $this->load->view('templates/superadmin_template',$data);
  }

}