<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cms extends CI_Controller {
	public function __construct(){
        parent::__construct();
        clear_cache();
        $this->load->model('common_model','com');
        if(!superadmin_logged_in())
        {
        	redirect('backend/superadmin');
        }

    }
    
	public function home_page($tab=''){

       	if($tab == '' || !is_numeric($tab) || $tab == 0 || $tab < 0){ 
	      redirect('backend/superadmin/error_404');
	    }
	    if($tab >4){ 
	      redirect('backend/superadmin/error_404');
	    }
        if(!empty($_POST['pm']))
        {
            $this->form_validation->set_rules('pm_title1', 'Title 1', 'trim|required');
            //$this->form_validation->set_rules('pm_subtitle1', 'Sub-title 1', 'trim|required');
            $this->form_validation->set_rules('pm_label1', 'Link 1', 'trim|required');
            $this->form_validation->set_rules('pm_link1', 'Label 1', 'trim|required');
            $this->form_validation->set_rules('pm_title2', 'Title 2', 'trim|required');
            //$this->form_validation->set_rules('pm_subtitle2', 'Sub-title 2', 'trim|required');
            $this->form_validation->set_rules('pm_label2', 'Link 2', 'trim|required');
            $this->form_validation->set_rules('pm_link2', 'Label 2', 'trim|required');
            // if(!empty($_FILES['background_image']['name']))
            // {
            //     $this->form_validation->set_rules('background_image','','callback_background_image_check');
            // }    
            
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == TRUE)
            {
                
                $pm_det = array(
                                      'title1'       => $this->input->post('pm_title1'),
                                      'sub_title1'   => $this->input->post('pm_subtitle1'),
                                      'label1' => $this->input->post('pm_label1'),
                                      'link1'     => $this->input->post('pm_link1'),
                                      'title2'       => $this->input->post('pm_title2'),
                                      'sub_title2'   => $this->input->post('pm_subtitle2'),
                                      'label2' => $this->input->post('pm_label2'),
                                      'link2'     => $this->input->post('pm_link2'),
                                         
                                    );
                    // if(!empty($_FILES['background_image']['name']))
                    // {
                    //      $pm_det['image']           = $this->background_image_check();
                    // }
                    
                    $pm_up = $this->com->update('cms_physicians_medical',$pm_det,array('id'=>1));
                  
                    if($pm_up)
                    {
                        $this->session->set_flashdata('msg_success',"Doctor's and Medical details has been  successfully Updated.");
                        redirect('backend/cms/home_page/2');
                    }else{
                        $this->session->set_flashdata('msg_error',"Doctor's and Medical details not Updated, Please try again.");
                        redirect('backend/cms/home_page/2');
                    }
               
            }else{
                $this->session->set_flashdata('msg_error',"Doctor's and Medical all fields  required , Please try again.");
                redirect('backend/cms/home_page/2');
            }
        }  
        if(!empty($_POST['s4']))
        {
            $this->form_validation->set_rules('s4_title1', 'Title 1', 'trim|required');
            //$this->form_validation->set_rules('s4_subtitle1', 'Sub-title 1', 'trim|required');
            $this->form_validation->set_rules('s4_title2', 'Title 2', 'trim|required');
           // $this->form_validation->set_rules('s4_subtitle2', 'Sub-title 2', 'trim|required');
            $this->form_validation->set_rules('s4_title3', 'Title 3', 'trim|required');
            //$this->form_validation->set_rules('s4_subtitle3', 'Sub-title 3', 'trim|required');
            
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == TRUE)
            {
                
                $pm_det = array(
                                      'title1'       => $this->input->post('s4_title1'),
                                      'sub_title1'   => $this->input->post('s4_subtitle1'),
                          
                                      'title2'       => $this->input->post('s4_title2'),
                                      'sub_title2'   => $this->input->post('s4_subtitle2'),
                                      'title3'       => $this->input->post('s4_title3'),
                                      'sub_title3'   => $this->input->post('s4_subtitle3'),
                                         
                                    );
                    
                    
                    $pm_up = $this->com->update('cms_physicians_medical',$pm_det,array('id'=>2,'section'=>2));
                  
                    if($pm_up)
                    {
                        $this->session->set_flashdata('msg_success','Section4 details has been  successfully Updated.');
                        redirect('backend/cms/home_page/4');
                    }else{
                        $this->session->set_flashdata('msg_error','Section4 details not Updated, Please try again.');
                    }
               
            }else{
                $this->session->set_flashdata('msg_error','Section4  all fields  required , Please try again.');
                redirect('backend/cms/home_page/4');
            } 
        }  
        
        $data['result_data'] = $this->com->banner_result();
        if(!empty($_SERVER['QUERY_STRING'])){
         $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
        
        }
        else{
         $config['suffix'] ='';
        }
       
      
        
        /*add category*/    
        $data['result_city_data'] = $this->com->city_result();
        $data['pm_details'] =  $this->com->get_row('cms_physicians_medical',array('id'=>1,'section'=>1));
       
        $data['s4_details'] =  $this->com->get_row('cms_physicians_medical',array('id'=>2,'section'=>2));
       
		$data['template'] = 'backend/cms/home_page';
		$this->load->view('templates/superadmin_template',$data);
	}
	public function add_banner($id='')
    {
        $id = $this->uri->segment(4);
       	$row_get = $this->common_model->count_rows('cms_home_page', array('cms_id'=>$id),'','');
        if($row_get < 1 && $id != ''){ 
       	 	redirect('backend/superadmin/error_404');
        }
        if(!empty($_POST['banner']))
        {
            $this->form_validation->set_rules('banner', 'banner', 'required');
            if(!empty($id))
            {
                if(!empty($_FILES['banner_image']['name']))
                {
                    $this->form_validation->set_rules('banner_image','','callback_banner_image_check');
                }    
            }else{
                $this->form_validation->set_rules('banner_image','','callback_banner_image_check'); 
            }

           
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == TRUE)
            {
                if(!empty($id))
                {
                    $banner_img_name = $this->banner_image_check();
                    $banner_det = array(
                                          'cms_title'       => $this->input->post('banner_title'),
                                          'cms_sub_title'   => $this->input->post('banner_subtitle'),
                                          'cms_button_link' => $this->input->post('banner_link'),
                                          //'order_by'     => $this->input->post('order_by'),
                                          'label'       => $this->input->post('banner_label'),
                                         
                                        );
                    if(!empty($_FILES['banner_image']['name']))
                    {
                         $banner_det['cms_image']           = $banner_img_name;
                    }
                    $banner_up = $this->com->update('cms_home_page',$banner_det,array('cms_id'=>$id));
                  
                    if($banner_up)
                    {
                        $this->session->set_flashdata('msg_success','Banner details has been  successfully Updated.');
                        redirect('backend/cms/home_page/1');
                    }else{
                        $this->session->set_flashdata('msg_error','Banner details not Updated, Please try again.');
                        redirect('backend/cms/home_page/1');
                    }
                }else
                {
                    
                    $banner_img_name = $this->banner_image_check();
                    $banner_det = array(
                                          'cms_title'       => $this->input->post('banner_title'),
                                          'cms_sub_title'   => $this->input->post('banner_subtitle'),
                                          'cms_button_link' => $this->input->post('banner_link'),
                                          //'order_by'     => $this->input->post('order_by'),
                                          'label'       => $this->input->post('banner_label'),
                                          'cms_image'           => $banner_img_name
                                        );
                    $banner_id = $this->com->insert('cms_home_page',$banner_det);
                  

                    if($banner_id)
                    {
                        $this->session->set_flashdata('msg_success','Banner details inserted successfully.');
                        redirect('backend/cms/home_page/1');
                    }else{
                        $this->session->set_flashdata('msg_error','Banner details insertion failed, Please try again.');
                    }
                }
                
                
            }
        }
        if(!empty($id))
        {
            $data['title'] = 'Edit Banner';
            $data['button_title'] = 'Update Banner Detail';
            $data['banner_details'] =  $this->com->get_row('cms_home_page',array('cms_id'=>$id));

        }else
        { 
            $data['title'] = 'Add Banner';
            $data['button_title'] = 'Add Banner Detail';
        }
       
        $data['template'] = 'backend/cms/add_banner';
        $this->load->view('templates/superadmin_template',$data); 
    }
    function delete()
    {
        
        $id = $this->uri->segment(4);
        $table = $this->uri->segment(5);
        if($table == 'cms_home_page')
        {
            $con = array('cms_id'=>$id);
            $banner_details =  $this->com->get_row('cms_home_page',array('cms_id'=>$id));
            unlink('./assets/uploads/banner/'.$banner_details->cms_image);
        }else{
            $con = array('search_city_id'=>$id);
            $city_details =  $this->com->get_row('search_job_city',array('search_city_id'=>$id));
             unlink('./assets/uploads/city/'.$city_details->search_city_image);
        }
        
        $this->com->delete($table,$con);

         if($table == 'cms_home_page')
        {
            $this->session->set_flashdata('msg_success','Banner detail delete successfully.');
        }
        else{
            $this->session->set_flashdata('msg_success','Search by City Job detail delete successfully.');
        }
        
    }

    function change_status()
    {
        $id = $this->uri->segment(4);
        $table = $this->uri->segment(5);
        $value = $this->input->post('change_status');
        if($table == 'cms_home_page')
        {
            $con = array('cms_id'=>$id);
        }else{
            $con = array('search_city_id'=>$id);
        }
        
        $update_data = array('status'=>$value);
        $this->com->update($table,$update_data,$con);
         if($table == 'cms_home_page')
        {
        $this->session->set_flashdata('msg_success','Banner status changed successfully.');
        }else{
            $this->session->set_flashdata('msg_success','Search by City Job status changed successfully.');
        }

                    
    }
    function banner_image_check($str='')
    { 
    

        if(empty($_FILES['banner_image']['name'])){
          $this->form_validation->set_message('banner_image_check', 'Choose Banner Image  1600 X 600 pixel.');
         return FALSE;
        }
        $image = getimagesize($_FILES['banner_image']['tmp_name']);

        if ($image[0] < 1600 || $image[1] < 600) {
             $this->form_validation->set_message('banner_image_check', 'Oops! Your Banner image needs to be 1600 X 600 pixels.');
             return FALSE;
        }
        if ($image[0] > 1600 || $image[1] > 600) {
             $this->form_validation->set_message('banner_image_check', 'Oops! Your Banner image needs to be  1600 X 600 pixels.');
             return FALSE;
        }

        if(!empty($_FILES['banner_image']['name'])){
          $config['upload_path'] = './assets/uploads/banner/';
          $config['allowed_types'] = 'jpeg|jpg|png|gif';
          $config['max_size']  = '5024';
          $config['max_width']  = '1600';
          $config['max_height']  = '600';
          $this->load->library('upload', $config);
          if ( ! $this->upload->do_upload('banner_image')){
               $error_check = $this->form_validation->set_message('banner_image_check', $this->upload->display_errors());
              
              return FALSE;
          }
          else
          {
              $data = $this->upload->data(); // upload image
              $file_name = $data['file_name'];
              return $file_name;
             
          }
        }else{
            $this->form_validation->set_message('banner_image_check', 'The %s field required.');
              return FALSE;
        }
            
         
    }
    public function add_city($id='')
    {
        $id = $this->uri->segment(4);
        $row_get = $this->common_model->count_rows('search_job_city', array('search_city_id'=>$id),'','');
        if($row_get < 1 && $id != ''){ 
       	 	redirect('backend/superadmin/error_404');
        }
        if(!empty($_POST['city']))
        {
            $this->form_validation->set_rules('city_name', 'City Name', 'trim|required');
            $this->form_validation->set_rules('order_by', 'Order', 'trim|required|numeric');
            if(!empty($id))
            {
                if(!empty($_FILES['city_image']['name']))
                {
                    $this->form_validation->set_rules('city_image','','callback_city_image_check');
                }    
            }else{
                $this->form_validation->set_rules('city_image','','callback_city_image_check'); 
            }

           
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == TRUE)
            {
                if(!empty($id))
                {
                    $city_det = array(
                                          'search_city_name'       => $this->input->post('city_name'),
                                          'order_by'     => $this->input->post('order_by'),
                                            
                                        );
                    if(!empty($_FILES['city_image']['name']))
                    {
                         $city_det['search_city_image']           = $this->city_image_check();
                    }
                    $city_up = $this->com->update('search_job_city',$city_det,array('search_city_id'=>$id));
                    
                    if($city_up)
                    {
                        $this->session->set_flashdata('msg_success','City details has been  successfully Updated.');
                        redirect('backend/cms/home_page/3');
                    }else{
                        $this->session->set_flashdata('msg_error','City details not Updated, Please try again.');
                    }
                }else
                {
                    
                    $city_img_name = $this->city_image_check();
                    $city_det = array(
                                          'search_city_name'    => $this->input->post('city_name'),
                                          'order_by'            => $this->input->post('order_by'),
                                          'search_city_image'   => $city_img_name
                                        );
                    $banner_id = $this->com->insert('search_job_city',$city_det);
                   
                    if($banner_id)
                    {
                        $this->session->set_flashdata('msg_success','City details inserted successfully.');
                        redirect('backend/cms/home_page/3');
                    }else{
                        $this->session->set_flashdata('msg_error','City details insertion failed, Please try again.');
                    }
                }
                
                
            }
        }
        if(!empty($id))
        {
            $data['title'] = 'Edit City';
            $data['button_title'] = 'Update City Detail';
            $data['city_details'] =  $this->com->get_row('search_job_city',array('search_city_id'=>$id));

        }else
        { 
            $data['title'] = 'Add City';
            $data['button_title'] = 'Add City Detail';
        }
       
        $data['template'] = 'backend/cms/add_city';
        $this->load->view('templates/superadmin_template',$data); 
    }
     function city_image_check($str='')
    { 
    

        if(empty($_FILES['city_image']['name'])){
          $this->form_validation->set_message('city_image_check', 'Choose City Image atleast 350 x 380 pixel and maximum of 1050 x 1140 pixels.');
         return FALSE;
        }
        $image = getimagesize($_FILES['city_image']['tmp_name']);

        if ($image[0] < 350 || $image[1] < 380) {
             $this->form_validation->set_message('city_image_check', 'Oops! Your City image needs to be atleast 350 x 380 pixels.');
             return FALSE;
        }
        if ($image[0] > 1050 || $image[1] > 1140) {
             $this->form_validation->set_message('city_image_check', 'Oops! Your City image needs to be maximum of 1050 x 1140 pixels.');
             return FALSE;
        }

        if(!empty($_FILES['city_image']['name'])){
          $config['upload_path'] = './assets/uploads/city/';
          $config['allowed_types'] = 'jpeg|jpg|png|gif';
          $config['max_size']  = '5024';
          $config['max_width']  = '1050';
          $config['max_height']  = '1140';
          $this->load->library('upload', $config);
          if ( ! $this->upload->do_upload('city_image')){
               $error_check = $this->form_validation->set_message('city_image_check', $this->upload->display_errors());
              
              return FALSE;
          }
          else
          {
              $data = $this->upload->data(); // upload image
              $file_name = $data['file_name'];
              return $file_name;
             
          }
        }else{
            $this->form_validation->set_message('city_image_check', 'The %s field required.');
              return FALSE;
        }
            
         
    }
    function add_pm_deatil()
    {
        
        if(!empty($_POST['pm']))
        {
            $this->form_validation->set_rules('pm_title1', 'Title 1', 'trim|required');
            $this->form_validation->set_rules('pm_subtitle1', 'Sub-title 1', 'trim|required');
            $this->form_validation->set_rules('pm_label1', 'Link 1', 'trim|required');
            $this->form_validation->set_rules('pm_link1', 'Label 1', 'trim|required');
            $this->form_validation->set_rules('pm_title2', 'Title 2', 'trim|required');
            $this->form_validation->set_rules('pm_subtitle2', 'Sub-title 2', 'trim|required');
            $this->form_validation->set_rules('pm_label2', 'Link 2', 'trim|required');
            $this->form_validation->set_rules('pm_link2', 'Label 2', 'trim|required');
            if(!empty($_FILES['background_image']['name']))
            {
                $this->form_validation->set_rules('background_image','','callback_background_image_check');
            }    
            
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == TRUE)
            {
                
                $pm_det = array(
                                      'cms_title'       => $this->input->post('pm_title1'),
                                      'cms_sub_title'   => $this->input->post('pm_subtitle1'),
                                      'cms_button_link' => $this->input->post('pm_label1'),
                                      'order_by'     => $this->input->post('pm_link1'),
                                      'cms_title'       => $this->input->post('pm_title2'),
                                      'cms_sub_title'   => $this->input->post('pm_subtitle2'),
                                      'cms_button_link' => $this->input->post('pm_label2'),
                                      'order_by'     => $this->input->post('pm_link2'),
                                         
                                    );
                    if(!empty($_FILES['background_image']['name']))
                    {
                         $pm_det['image']           = $pm_img_name;
                    }
                    $pm_up = $this->com->update('cms_physicians_medical',$pm_det,array('id'=>$id));

                    if($pm_up)
                    {
                        $this->session->set_flashdata('msg_success',"Doctor's and Medical details has been  successfully Updated.");
                        redirect('backend/cms/home_page');
                    }else{
                        $this->session->set_flashdata('msg_error',"Doctor's and Medical details not Updated, Please try again.");
                    }
               
                //redirect('backend/cms/home_page');
                
            }else{
                 //redirect('backend/cms/home_page');
            }
        }
    }

     function background_image_check($str='')
    { 
    

        if(empty($_FILES['background_image']['name'])){
          $this->form_validation->set_message('background_image_check', 'Choose City Image atleast 1600 x 593 pixel and maximum of 2000 x 2000 pixels.');
         return FALSE;
        }
        $image = getimagesize($_FILES['background_image']['tmp_name']);
       
        if ($image[0] < 1600 || $image[1] < 593) {
             $this->form_validation->set_message('background_image_check', 'Oops! Your City image needs to be atleast 1600 x 593 pixels.');
             return FALSE;
        }
        if ($image[0] > 2000 || $image[1] > 2000) {
             $this->form_validation->set_message('background_image_check', 'Oops! Your City image needs to be maximum of 2000 x 2000 pixels.');
             return FALSE;
        }

        if(!empty($_FILES['background_image']['name'])){
          $config['upload_path'] = './assets/uploads/background/';
          $config['allowed_types'] = 'jpeg|jpg|png|gif';
          $config['max_size']  = '5024';
          $config['max_width']  = '2000';
          $config['max_height']  = '2000';
          $this->load->library('upload', $config);
          if ( ! $this->upload->do_upload('background_image')){
               $error_check = $this->form_validation->set_message('background_image_check', $this->upload->display_errors());
              
              return FALSE;
          }
          else
          {
              $data = $this->upload->data(); // upload image
              $file_name = $data['file_name'];
              return $file_name;
             
          }
        }else{
            $this->form_validation->set_message('background_image_check', 'The %s field required.');
              return FALSE;
        }
            
         
    }
    function footer_menu($status = "")
    {
        
       
        $data['menu'] = $this->common_model->get_result('cms_menu',array('parent_id'=>0),'',array('name', "desc")); 
        
        if(isset($_POST['update']))     
        {
            $mid  = $this->input->post('m_id');

            $this->form_validation->set_rules('e_menu_name', 'Menu Name', 'trim|required|callback_check_menu_name['.$mid.']');
            $this->form_validation->set_rules('e_m_sequence', 'Menu Sequence', 'trim|required|numeric');
             $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                //=======================================

                $menu_name = $this->input->post('e_menu_name');
                $status   = $this->input->post('e_status');
                
                $update_array = array(
                    'name'        => $menu_name,
                    'status'      => $status,
                    'parent_id'   => 0,
                    'order_by'    => $this->input->post('e_m_sequence'),
                );
                $update = $this->common_model->update('cms_menu',$update_array,array('id'=>$mid)); 
                if($update)
                {
                    $message = 'Menu updated Successfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/footer_menu');
                }
                else
                {
                    $message = 'Menu not updated.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/footer_menu');
                }
            }
            else
            {
                $data['main_id'] = $this->input->post('m_id'); 
                
            }   
            

           
        }
        if(isset($_POST['add']))
        {
            
            $this->form_validation->set_rules('menu_name', 'Menu Title', 'trim|required|callback_check_menu_name');
                $this->form_validation->set_rules('order_by', 'Menu Sequence', 'trim|required|numeric');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                $menu_name = $this->input->post('menu_name');
                $menu_status  = $this->input->post('status');
                
                $insert_array = array(
                    'name'        => $menu_name,
                    'status'      => $menu_status,
                    'parent_id'   => 0,
                    'order_by'    => $this->input->post('order_by'),
                );
                $insert = $this->common_model->insert('cms_menu',$insert_array); 
                if($insert)
                {
                    $message = 'Menu addad Sucessfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/footer_menu');
                }
                else
                {
                    $message = 'Menu not added.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/footer_menu');
                }
            }
            
        }
                
                
        $data['template'] = 'backend/cms/footer_menu';
        $this->load->view('templates/superadmin_template',$data); 
       
       
    }
    function check_menu_name($str='',$id='')
    {
        $unq_id = trim($id);
        if(!empty($unq_id))
        {
          $con = array('name'=>trim($str),'id !=' =>$unq_id,'parent_id'=>0);
        }else{
          $con = array('name'=>trim($str),'parent_id'=>0);
        }
        if($this->common_model->get_row('cms_menu',$con)):
        $this->form_validation->set_message('check_menu_name', 'The %s already exists.');
          return FALSE;
        else:
          return TRUE;
        endif;
    }
    function sub_menu($mid="")
    {
        
        $mid = $this->uri->segment(4);
        if($mid == '' || !is_numeric($mid)){ 
          redirect('backend/superadmin/error_404');
        }
         $parent_category = $this->common_model->get_row('cms_menu',array('id'=>$mid),array('*'));
        if(empty($parent_category))
        {
          redirect('backend/superadmin/error_404');
        }
        $data['mid'] = $this->uri->segment(4);
        $idncid = $parent_category->parent_id;
        $data['parent_id'] = $idncid;
        $p1 = $parent_category->name;
        $data['parentc'] = $parent_category->name;
        $parent_ncategory = $this->common_model->get_row('cms_menu',array('parent_id'=>$idncid),array('*'));
        $data['submenu'] = $this->common_model->get_result('cms_menu',array('parent_id'=>$mid),"",array('id','desc')); 
            
        
        if(isset($_POST['add']))          
        { 
            $this->form_validation->set_rules('submenu_name', 'Submenu Title', 'Trim|required|callback_check_submenu_name'); 
            $this->form_validation->set_rules('sm_sequence', 'Sequence', 'required');
            $this->form_validation->set_rules('url_link', 'Custom Url Link', 'Trim|required');
             $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
               
                    
                    $url_slug     = $this->input->post('url_link');
                    $menu_array = array(
                        'name'            => $this->input->post('submenu_name'),
                        'status'          => $this->input->post('status'),
                        'parent_id'       => $mid,
                        'order_by'        => $this->input->post('sm_sequence'),
                        'link'         => $url_slug,
                    );  
               
                    $insert = $this->common_model->insert('cms_menu',$menu_array);

                    if($insert)
                    {
                        $message = 'Submenu added Sucessfully.';
                        $this->session->set_flashdata('msg_success',$message);
                        redirect('backend/cms/sub_menu/'.$mid);
                    }
                    else
                    {
                        $message =  "Submenu not Inserted.";
                        $this->session->set_flashdata('msg_error',$message);
                    }
                
            }
           
        }
        
        if(isset($_POST['update']))     
        {
          $main_id = $this->input->post('e_m_id'); 
           $this->form_validation->set_rules('e_submenu_name', 'Submenu Title', 'Trim|required|callback_check_submenu_name['.$main_id.']');   
           $this->form_validation->set_rules('e_sm_sequence', 'Sequence', 'required');

            $this->form_validation->set_rules('e_url_link', 'Custom Url Link', 'Trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
           if($this->form_validation->run() == TRUE)
            {
                
                
                $url_slug     = $this->input->post('e_url_link');
                    $menu_array = array(
                        'name'            => $this->input->post('e_submenu_name'),
                        'status'          => $this->input->post('e_status'),
                        'order_by'        => $this->input->post('e_sm_sequence'),
                        'link'         => $url_slug,
                    );  
                $update = $this->common_model->update('cms_menu',$menu_array,array('id'=>$main_id)); 
                
                if($update)
                {
                    $message = 'Submenu updated Successfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/sub_menu/'.$mid);
                }
                else
                {
                    $message =  "Submenu not Update.";
                    $this->session->set_flashdata('msg_error',$message);
                }
           }else{
            $data['main_id'] = $this->input->post('e_m_id'); 
           }     
        }


        $data['template'] = 'backend/cms/submenu';
        $this->load->view('templates/superadmin_template',$data);   
       
    }
    function check_submenu_name($str='',$id='')
    {
        $unq_id = trim($id);
        if(!empty($unq_id))
        {
          $con = array('name'=>trim($str),'id !=' =>$unq_id,'parent_id !='=>0);
        }else{
          $con = array('name'=>trim($str),'parent_id !='=>0);
        }
        if($this->common_model->get_row('cms_menu',$con)):
        $this->form_validation->set_message('check_submenu_name', 'The %s already exists.');
          return FALSE;
        else:
          return TRUE;
        endif;
    }
    function delete_menu()
    {
        
        $id = $this->uri->segment(4);
        $table = $this->uri->segment(5);
        if($table == 'cms_menu')
        {
            $con = array('id'=>$id);
        }
        $this->com->delete($table,$con);
        $this->session->set_flashdata('msg_success','Menu delete successfully.');
        
    }

    function change_status_menu()
    {
        $id = $this->uri->segment(4);
        $table = $this->uri->segment(5);
        $value = $this->input->post('change_status');
        if($table == 'cms_menu')
        {
            $con = array('id'=>$id);
        }
        $update_data = array('status'=>$value);
        $this->com->update($table,$update_data,$con);
        $this->session->set_flashdata('msg_success','Menu status changed successfully.');
                    
    }
    function setting()
    {

       
        // $this->form_validation->set_rules('name[]','All Field','required');
        // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if(isset($_POST['update']))
        {
        //if ($this->form_validation->run() == TRUE){

            $name=$this->input->post('name');
            $ids=$this->input->post('ids');
            
            $i=0;
            foreach ($name as $value) 
            {
                    $post_data = array('option_value' => $value);
                    $optionid=array('id'=>$ids[$i]);
                    $this->common_model->update('options',$post_data,$optionid);
                    $i++;
            }
           
            $this->session->set_flashdata('msg_success','Data updated successfully.');
            redirect('backend/cms/setting');                
            //}
        }
        $opt = $this->common_model->get_result('options',array('status'=>1),'',array('order','asc'));
        $data['option'] = $opt;
        $data['template'] = 'backend/cms/need_help_content';
        $this->load->view('templates/superadmin_template',$data);   
        
    }
    function appex1()
    {
        $data['appex1'] = $this->common_model->get_result('medical_specialties_annex1',array(),'',array('order_by', "asc")); 
        if(isset($_POST['update']))     
        {
            $mid  = $this->input->post('m_id');

            $this->form_validation->set_rules('e_appex1_name', 'Canadian Schools Name', 'trim|required|callback_check_appex1_name['.$mid.']');
            $this->form_validation->set_rules('e_a_sequence', 'Canadian Schools Sequence', 'trim|required|numeric');
             $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                //=======================================

                $a_name = $this->input->post('e_appex1_name');
                $status   = $this->input->post('e_status');
                
                $update_array = array(
                    'name'        => $a_name,
                    'status'      => $status,
                    'order_by'    => $this->input->post('e_a_sequence'),
                );
                $update = $this->common_model->update('medical_specialties_annex1',$update_array,array('id'=>$mid)); 
                if($update)
                {
                    $message = 'Canadian Medical Schools updated Successfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/appex1');
                }
                else
                {
                    $message = 'Canadian Medical Schools not updated.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/appex1');
                }
            }
            else
            {
                $data['main_id'] = $this->input->post('m_id'); 
                
            }   
            

           
        }
        if(isset($_POST['add']))
        {
            
            $this->form_validation->set_rules('appex1_name', 'Canadian Schools Name', 'trim|required|callback_check_appex1_name');
                $this->form_validation->set_rules('order_by', 'Canadian Schools Sequence', 'trim|required|numeric');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                $appex1_name = $this->input->post('appex1_name');
                $status  = $this->input->post('status');
                
                $insert_array = array(
                    'name'        => $appex1_name,
                    'status'      => $status,
                    'order_by'    => $this->input->post('order_by'),
                );
                $insert = $this->common_model->insert('medical_specialties_annex1',$insert_array); 
                if($insert)
                {
                    $message = 'Canadian Medical Schools addad Sucessfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/appex1');
                }
                else
                {
                    $message = 'Canadian Medical Schools not added.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/appex1');
                }
            }
            
        }
                
        $data['template'] = 'backend/cms/appex1';
        $this->load->view('templates/superadmin_template',$data);   
    }
    function check_appex1_name($str='',$id='')
    {
        $unq_id = trim($id);
        if(!empty($unq_id))
        {
          $con = array('name'=>trim($str),'id !=' =>$unq_id);
        }else{
          $con = array('name'=>trim($str));
        }
        if($this->common_model->get_row('medical_specialties_annex1',$con)):
        $this->form_validation->set_message('check_appex1_name', 'The %s already exists.');
          return FALSE;
        else:
          return TRUE;
        endif;
    }
    function appex2()
    {
        $data['appex2'] = $this->common_model->get_result('medical_specialties_annex2',array(),'',array('order_by', "asc")); 
        
        if(isset($_POST['add']))
        {
            
            $this->form_validation->set_rules('appex2_name', 'Medical Specialties Name', 'trim|required|callback_check_appex2_name');
                $this->form_validation->set_rules('order_by', 'Medical Specialties Sequence', 'trim|required|numeric');
            if(!empty($_FILES['logo']['name']))
            {
            $this->form_validation->set_rules('logo','','callback_logo_check');
            }
                
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
            if($this->form_validation->run() == TRUE)
            {
             
                $appex2_name = $this->input->post('appex2_name');
                $status  = $this->input->post('status');
                
                $insert_array = array(
                    'name'        => $appex2_name,
                    'status'      => $status,
                    'order_by'    => $this->input->post('order_by'),
                   
                );
                if(!empty($_FILES['logo']['name']))
                {
                  
                  $insert_array['logo'] = $this->logo_check();
                }
                
                $insert = $this->common_model->insert('medical_specialties_annex2',$insert_array); 
                if($insert)
                {
                    $message = 'Medical Specialty addad Sucessfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/appex2');
                }
                else
                {
                    $message = 'Medical Specialty Annex2 not added.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/appex2');
                }
            }
            
        }
         if(isset($_POST['update']))     
        {
            $mid  = $this->input->post('m_id');

            $this->form_validation->set_rules('e_appex2_name', 'Medical Specialties Name', 'trim|required|callback_check_appex2_name['.$mid.']');
            $this->form_validation->set_rules('e_a_sequence', 'Medical Specialties Sequence', 'trim|required|numeric');
             $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
              if(!empty($_FILES['e_logo']['name']))
                {
              $this->form_validation->set_rules('e_logo','','callback_e_logo_check');
            }

            if($this->form_validation->run() == TRUE)
            {
                //=======================================

                $a_name = $this->input->post('e_appex2_name');
                $status   = $this->input->post('e_status');
                
                $update_array = array(
                    'name'        => $a_name,
                    'status'      => $status,
                    'order_by'    => $this->input->post('e_a_sequence'),
                );
                 if(!empty($_FILES['e_logo']['name']))
                {
                  
                  $update_array['logo'] = $this->e_logo_check();
                }
                $update = $this->common_model->update('medical_specialties_annex2',$update_array,array('id'=>$mid)); 
                if($update)
                {
                    $message = 'Medical Specialty updated Successfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/appex2');
                }
                else
                {
                    $message = 'Medical Specialty not updated.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/appex2');
                }
            }
            else
            {
                $data['main_id'] = $this->input->post('m_id'); 
                
            }   
            

           
        }       

        $data['template'] = 'backend/cms/appex2';
        $this->load->view('templates/superadmin_template',$data);   
    }
    function logo_check($str='')
    { 
        if(empty($_FILES['logo']['name'])){
          $this->form_validation->set_message('logo_check', 'Choose Logo Image  30 X 30 pixel.');
         return FALSE;
        }
        $new_name = array();
        $new_name = explode('.', $_FILES['logo']['name']);
        if(!empty($new_name[1]) && ($new_name[1] =='jpeg' || $new_name[1] =='jpg' || $new_name[1] =='gif' || $new_name[1] =='png')){ 
            $image = getimagesize($_FILES['logo']['tmp_name']);
            if ($image[0] != 30 || $image[1] != 30) {
                 $this->form_validation->set_message('logo_check', 'Oops! Your logo image needs to be 30 x 30 pixels.');
                 return FALSE;
                
            }
        }elseif($new_name[1] =='svg'){
          $xml = simplexml_load_file($_FILES['logo']['tmp_name']);
          $attr = $xml->attributes();
          //printf("%s x %s", $attr->width, $attr->height);
            if ($attr->width != 30 || $attr->height != 30) {
                 $this->form_validation->set_message('logo_check', 'Oops! Your logo image needs to be 30 x 30 pixels.');
                 return FALSE;
                 
            }
        }

      
        if(!empty($_FILES['logo']['name'])){
          $config['upload_path'] = './assets/uploads/logo/';
          $config['allowed_types'] = 'jpeg|jpg|png|gif|svg';
          $config['max_width']  = '30';
          $config['max_height']  = '30';
          $config['file_name']  =   date('YmdHis').'_'.$_FILES['logo']['tmp_name'];
          $this->load->library('upload', $config);
          if ( ! $this->upload->do_upload('logo')){
               $error_check = $this->form_validation->set_message('logo_check', 'This image not uploaded. You can upload only jpeg,jpg,png,gif,svg formate image, with 30x30 pixels');
              return FALSE;
          }
          else
          {
              $data = $this->upload->data(); // upload image
              $file_name = $data['file_name'];
              return $file_name;
             
          }
        }else{
            $this->form_validation->set_message('logo_check', 'The %s field required.');
              return FALSE;
        }
            
         
    }
     function e_logo_check($str='')
    { 
        if(empty($_FILES['e_logo']['name'])){
          $this->form_validation->set_message('e_logo_check', 'Choose Logo Image  30 X 30 pixel.');
         return FALSE;
        }
        $new_name = array();
        $new_name = explode('.', $_FILES['e_logo']['name']);
        if(!empty($new_name[1]) && ($new_name[1] =='jpeg' || $new_name[1] =='jpg' || $new_name[1] =='gif' || $new_name[1] =='png')){ 
            $image = getimagesize($_FILES['e_logo']['tmp_name']);
            if ($image[0] != 30 || $image[1] != 30) {
                 $this->form_validation->set_message('e_logo_check', 'Oops! Your logo image needs to be 30 x 30 pixels.');
                 return FALSE;
                
            }
        }elseif($new_name[1] =='svg'){
          $xml = simplexml_load_file($_FILES['e_logo']['tmp_name']);
          $attr = $xml->attributes();
          //printf("%s x %s", $attr->width, $attr->height);
            if ($attr->width != 30 || $attr->height != 30) {
                 $this->form_validation->set_message('e_logo_check', 'Oops! Your logo image needs to be 30 x 30 pixels.');
                 return FALSE;
                 
            }
        }

      
        if(!empty($_FILES['e_logo']['name'])){
          $config['upload_path'] = './assets/uploads/logo/';
          $config['allowed_types'] = 'jpeg|jpg|png|gif|svg';
          $config['max_width']  = '30';
          $config['max_height']  = '30';
          $config['file_name']  =   date('YmdHis').'_'.$_FILES['e_logo']['tmp_name'];
          $this->load->library('upload', $config);
          if ( ! $this->upload->do_upload('e_logo')){
               $error_check = $this->form_validation->set_message('e_logo_check', 'This image not uploaded. You can upload only jpeg,jpg,png,gif,svg formate image, with 30x30 pixels');
              return FALSE;
          }
          else
          {
              $data = $this->upload->data(); // upload image
              $file_name = $data['file_name'];
              return $file_name;
             
          }
        }else{
            $this->form_validation->set_message('e_logo_check', 'The %s field required.');
              return FALSE;
        }
            
         
    }
    function check_appex2_name($str='',$id='')
    {
        $unq_id = trim($id);
        if(!empty($unq_id))
        {
          $con = array('name'=>trim($str),'id !=' =>$unq_id);
        }else{
          $con = array('name'=>trim($str));
        }
        
        if($this->common_model->get_row('medical_specialties_annex2',$con)):
        $this->form_validation->set_message('check_appex2_name', 'The %s already exists.');
          return FALSE;
        else:
          return TRUE;
        endif;
    }
    function common_delete()
    {
        
        $id = $this->uri->segment(4);
        $table = $this->uri->segment(5);
        if($table == 'medical_specialties_annex1')
        {
            $con = array('id'=>$id);
        }
        if($table == 'medical_specialties_annex2')
        {
            $con = array('id'=>$id);
        }
        if($table == 'physician_medical_facility_content')
        {
            $con = array('id'=>$id);
        }
        if($table == 'city_price')
        {
            $con = array('id'=>$id);
        }
        $this->com->delete($table,$con);
        
        if($table == 'physician_medical_facility_content')
        {
            $this->session->set_flashdata('msg_success','Doctor and Medical content delete successfully.');
        }
         if($table == 'medical_specialties_annex2')
        {
            $this->session->set_flashdata('msg_success','Medical Specialty delete successfully.');
        }
        if($table == 'medical_specialties_annex1')
        {
            $this->session->set_flashdata('msg_success','Canadian Medical Schools delete successfully.');
        }
        if($table == 'city_price')
        {
            $this->session->set_flashdata('msg_success','City delete successfully.');
        }
        
    }

    function common_change_status()
    {
        
        $id = $this->uri->segment(4);
        $table = $this->uri->segment(5);

        $value = $this->input->post('change_status');
        if($table == 'medical_specialties_annex1')
        {
            $con = array('id'=>$id);
        }
        if($table == 'medical_specialties_annex2')
        {
            $con = array('id'=>$id);
        }
        if($table == 'physician_medical_facility_content')
        {
            $con = array('id'=>$id);
        }
        if($table == 'location_data')
        {
            $con = array('id'=>$id);
        }
        $update_data = array('status'=>$value);
        $this->com->update($table,$update_data,$con);
        if($table == 'physician_medical_facility_content')
        {
             $this->session->set_flashdata('msg_success','Doctor and Medical Content status changed successfully.');
        }
         if($table == 'medical_specialties_annex2')
        {
        $this->session->set_flashdata('msg_success','Medical Specialty status changed successfully.');
        }
        if($table == 'medical_specialties_annex1')
        {
        $this->session->set_flashdata('msg_success','Canadian Medical Schools status changed successfully.');
        }
        if($table == 'location_data')
        {
            $this->session->set_flashdata('msg_success','City status changed successfully.');
        }
                    
    }

    public function signup_content() {

         $data['sign_up_content'] = $this->common_model->get_result('physician_medical_facility_content',array(),'',array('type', "asc",'order_by','desc')); 
        if(isset($_POST['add']))
        {
            
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
             $this->form_validation->set_rules('description', 'Description', 'trim|required');
             $this->form_validation->set_rules('status', 'Status', 'trim|required');
                $this->form_validation->set_rules('order_by', 'Sequence', 'trim|required|numeric');
                   $this->form_validation->set_rules('type', 'Type', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                $title = $this->input->post('title');
                 $description = $this->input->post('description');
                $status  = $this->input->post('status');
                
                $insert_array = array(
                    'title'        => $title,
                    'description' => $description,
                    'type'        => $this->input->post('type'),
                    'status'      => $status,
                    'order_by'    => $this->input->post('order_by'),
                );
                $insert = $this->common_model->insert('physician_medical_facility_content',$insert_array); 
               
                if($insert)
                {
                    $message = 'Doctor and Medical content addad Sucessfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/signup_content');
                }
                else
                {
                    $message = 'Doctor and Medical content not added.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/signup_content');
                }
            }
            
        }
        if(isset($_POST['update']))
        {
          $this->form_validation->set_rules('edit_title[]', 'Title', 'trim|required');
             $this->form_validation->set_rules('edit_description[]', 'Description', 'trim|required');
            $this->form_validation->set_rules('edit_order[]', 'Sequence', 'trim|required|numeric');
                   
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                
                for ($i=0; $i < count($_POST['main_id']); $i++) {   
                
                  
                        $content_data['title'] =$_POST['edit_title'][$i];
                        $content_data['description'] = $_POST['edit_description'][$i];
                        $content_data['order_by'] = $_POST['edit_order'][$i]; 
                        $this->common_model->update('physician_medical_facility_content',$content_data,array('id'=>$_POST['main_id'][$i]));
                        
                }

                $message = 'Doctor and Medical content update Sucessfully.';
                $this->session->set_flashdata('msg_success',$message);
                redirect('backend/cms/signup_content');
            }
        }
                
        $data['template']= 'backend/cms/sign_up_content';
        $this->load->view('templates/superadmin_template',$data); 
    }
    function set_order_by()
    {
      $appex2 = $this->common_model->get_result('medical_specialties_annex2',array(),'',array('id', "asc"));
      $con ='';
      $i = 1;
      foreach($appex2 as $app2)
      {
         $con = array('status'=>1);
         $this->common_model->update('medical_specialties_annex2',$con,array('id'=>$app2->id));
         $i++;
      }
      $where = '';
      $appex1 = $this->common_model->get_result('medical_specialties_annex1',array(),'',array('id', "asc")); 
       $j = 1; 
      foreach($appex1 as $app1)
      {
         $where = array('status'=>1);
         $this->common_model->update('medical_specialties_annex1',$where,array('id'=>$app1->id));
         $j++;
      }
        
    }
    public function city_manage() {

        $data['city_manage'] = $this->common_model->get_result('location_data',array('searching'=>1),'',array()); 
        $data['other_manage'] = $this->common_model->get_group_by_city(array('searching'=>0));
        if(isset($_POST['add']))
        {
            
            
            $this->form_validation->set_rules('city_name', 'City Name', 'trim|required');
            $this->form_validation->set_rules('price', 'Price', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'trim|required');
           
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                $city_name = $this->input->post('city_name');
                $price = $this->input->post('price');
                $status  = $this->input->post('status');
                
                $insert_array = array(
                    'city_name'   => $city_name,
                    'price'       => $price,
                    'status'      => $status,
                    
                );
                $insert = $this->common_model->insert('location_data',$insert_array); 

                if($insert)
                {
                    $message = 'City added Sucessfully.';
                    $this->session->set_flashdata('msg_success',$message);
                    redirect('backend/cms/city_manage');
                }
                else
                {
                    $message = 'City not added.';
                    $this->session->set_flashdata('msg_error',$message);
                    redirect('backend/cms/city_manage');
                }
            }
            
        }
        if(isset($_POST['update']))
        {
            

            $this->form_validation->set_rules('edit_name[]', 'City Name', 'trim|required');
             $this->form_validation->set_rules('edit_price[]', 'Price', 'trim|required');
          
                   
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == TRUE)
            {
                
               

                //echo '<pre>'; print_r($_POST);
                //sexit;
                for ($i=0; $i < count($_POST['main_id']); $i++) {   
                
                        $content_data['city_name'] =$_POST['edit_name'][$i];
                        $content_data['price'] = $_POST['edit_price'][$i];
                      
                        $this->common_model->update('location_data',$content_data,array('id'=>$_POST['main_id'][$i]));
                        
                }
                if($_POST['other_id'] == 'other_id')
                {
                	$price['price'] = $_POST['other_price'];
                	$this->common_model->update('location_data',$price,array('searching'=>0));
                }

                $message = 'City update Sucessfully.';
                $this->session->set_flashdata('msg_success',$message);
                redirect('backend/cms/city_manage');
            }
        }
                
        $data['template']= 'backend/cms/city_manage';
        $this->load->view('templates/superadmin_template',$data); 
    }
   
    function create_content()
    {

       
        // $this->form_validation->set_rules('name[]','All Field','required');
        // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if(isset($_POST['update']))
        {
        //if ($this->form_validation->run() == TRUE){

            $name=$this->input->post('name');
            $ids=$this->input->post('ids');
            
            $i=0;
            foreach ($name as $value) 
            {
                    $post_data = array('content' => $value);
                    $optionid=array('id'=>$ids[$i]);
                    $this->common_model->update('create_page',$post_data,$optionid);
                    $i++;
            }
           //echo $this->db->last_query();
           //exit;
            $this->session->set_flashdata('msg_success','Data updated successfully.');
            redirect('backend/cms/create_content');                
            //}
        }
        $opt = $this->common_model->get_result('create_page');
        $data['option'] = $opt;
        $data['template'] = 'backend/cms/create_content';
        $this->load->view('templates/superadmin_template',$data);   
        
    }
     function change_other_city_status()
    {
        $status = $this->uri->segment(4);
        $con = array('searching'=>0);
        $this->com->update('location_data',array('status'=>$status),$con);
        $this->session->set_flashdata('msg_success','City status changed successfully.');   
        redirect('backend/cms/city_manage');           
    }
    function test(){
      // if(!empty($city_postal_code) || $canada_province != 'All')
    // {
     //$city_postal_code =  'Montreal';
     //$canada_province =  'Quebec';
     $city_postal_code =  'Montreal';
     $address = '';
     if(!empty($city_postal_code))
     {
       $address .= $city_postal_code.',';
     }
     if(!empty($canada_province) && $canada_province != 'All')
     {
       $address .= $canada_province.',';
     }
     echo $address .= 'canada';
        
            $prepAddr = str_replace(' ','+',$address);

            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');

            $output= json_decode($geocode);

             echo $lat = $output->results[0]->geometry->location->lat;
             echo $long = $output->results[0]->geometry->location->lng;

             
  //       }

    }

}