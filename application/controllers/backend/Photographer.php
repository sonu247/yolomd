<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Photographer extends CI_Controller {
	public function __construct(){
	    parent::__construct();  
	    clear_cache();  
	    $this->load->model('common_model');
       	$this->load->model('user_model');
	    if(!superadmin_logged_in())
        {
        	redirect('backend/superadmin');
        }         
	}
	public function index($offset=0)
  	{
  		
	    $per_page=25 ;
    	$data['offset']=$offset;
    	$config=backend_pagination();
    	$config['base_url'] = site_url().'backend/photographer/index';
	    $config['total_rows'] = $this->user_model->photographer_result(0,0);
	    $config['per_page'] = $per_page;
    	$config['uri_segment'] = 4;
    	
	    $config['per_page'] = $per_page;
	    $data['result_data'] = $this->user_model->photographer_result($offset,$per_page);
		if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	    $data['total_records'] = $config['total_rows'];
	    if($config['total_rows'] < $offset)
	    {
	       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
	
	       redirect('backend/photographer/index/0');
	    }
	    //$config['first_url'] = $config['base_url'].$config['suffix'];
	    $this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();  
	    if(isset($_POST['apply']))
		{ 
			$act_typ = $this->input->post('act_type');
			$all_id = $this->input->post('check_id');
			if($act_typ == 4)
			{
				if(!empty($all_id))
				{
					foreach($all_id as $all_i)
					{  
						
						$this->common_model->delete('users',array('user_id'=> $all_i,'user_role'=>3));
						
					}
					$this->session->set_flashdata('msg_success','Photographer details Delete successfully.');
					redirect('backend/photographer'); 
				}
				               
			}
			elseif($act_typ ==2)
			{
				$this->session->set_flashdata('msg_success','Photographer details Deactivate successfully.');
			}
			elseif($act_typ ==1){
				$this->session->set_flashdata('msg_success','Photographer details Activate successfully.');
			}else{
				$this->session->set_flashdata('msg_success','Photographer details Banned successfully.');
			}
			
			$data_update['user_status'] = $act_typ;
			$data_update['user_role'] = 3;
			if(!empty($all_id))
			{
				foreach($all_id as $all_i)
				{ 
					$res =$this->common_model->update('users',$data_update,$array = array('user_id' =>$all_i));
					
					//exit;
				}
			}	
			   
			redirect('backend/photographer');   
		
		} 
		
	    $data['template']='backend/photographer/index';
	    $this->load->view('templates/superadmin_template',$data);

    }
    public function photographer_add_edit($id='')
    {    
 
    	$data['check_id'] = '';
    	$id = $this->uri->segment(4);
    	$data['check_id'] = $id ;
    	
    	if(!empty($id))
    	{
    		$row_get = $this->common_model->count_rows('users', array('user_id'=>$id),'','');
		    if($row_get < 1){ 
		      redirect('backend/superadmin/error_404');
		    }
    		$data['title']='Edit Photographer';
    		$data['data_request']='Update';
    		$data['user_details'] =  $this->common_model->get_row('users',array('user_id'=>$id,'user_role'=>3));
    		if(isset($_POST['Update']))
    		{
	    	
		  	    $this->form_validation->set_rules('user_name_title', 'Photographer title', 'required');
		        $this->form_validation->set_rules('photographer_first_name', 'Photographer first name', 'required|alpha_numeric_spaces');
		        $this->form_validation->set_rules('photographer_last_name', 'Photographer last name', 'required|alpha_numeric_spaces');
		        $this->form_validation->set_rules('city', 'Photographer city', 'required|alpha_numeric_spaces');
		        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		        if($this->form_validation->run() == TRUE)
		        {
				    $user_data=array(
						'user_name_title' => $this->input->post('user_name_title'),
						'user_first_name' => trim($this->input->post('photographer_first_name')),
						'user_last_name'  => trim($this->input->post('photographer_last_name')),
						'city'  => trim($this->input->post('city')),
						
					);
			
				    $update = $this->common_model->update('users',$user_data,array('user_id'=>$id)); 
				
						
					if($update)
		            {
		                $message = 'Photographer User detail updated Successfully.';
		                $this->session->set_flashdata('msg_success',$message);
		                redirect('backend/photographer');
		            }else
	            	{
	                	$message =  "Photographer User detail not Update.";
	                	$this->session->set_flashdata('msg_error',$message);
	            	}
		        }    
		    }
    	}else{
    		$data['title']='Add Photographer';
    		$data['data_request']='Add';
    		if(isset($_POST['Add']))
    		{
	    	
		  	    $this->form_validation->set_rules('user_name_title', 'Photographer title', 'required');
		        $this->form_validation->set_rules('photographer_first_name', 'Photographer first name', 'required|alpha_numeric_spaces');
		        $this->form_validation->set_rules('photographer_last_name', 'Photographer last name', 'required|alpha_numeric_spaces');
		        $this->form_validation->set_rules('email_address', 'Photographer Email Address', 'trim|required|valid_email|callback_email_check');
		        $this->form_validation->set_rules('password', 'New Password', 'trim|required|matches[confpassword]|min_length[6]');
        		$this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required|min_length[6]');  
        		 $this->form_validation->set_rules('city', 'Photographer city', 'required|alpha_numeric_spaces');
		        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		        if($this->form_validation->run() == TRUE)
		        {
				    $salt = $this->salt();
				    $user_data=array(
						'user_name_title' => $this->input->post('user_name_title'),
						'user_first_name' => trim($this->input->post('photographer_first_name')),
						'user_last_name'  => trim($this->input->post('photographer_last_name')),
						 'user_email'      => trim($this->input->post('email_address')),
						 'password'			=> sha1($salt . sha1($salt . sha1(trim($this->input->post('password'))))),
						'salt'				=> $salt,
						'user_role'			=> 3,
						'user_status'		=> 1,
						'last_login_ip'		=> $this->input->ip_address(),
						'user_created'		=> date('Y-m-d H:i:s A'),
						'city'  => trim($this->input->post('city')),
					);
			
				    $insert = $this->common_model->insert('users',$user_data); 
				
						
					if($insert)
		            {
		                $message = 'Photographer detail insert Successfully.';
		                $this->session->set_flashdata('msg_success',$message);
		                redirect('backend/photographer');
		            }else{

	                	$message =  "Photographer detail not inserted.";
	                	$this->session->set_flashdata('msg_error',$message);
		            }
		        }    
		    }
    	}
		
    	if(isset($_POST['change_password']))
    	{
    		
			$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|matches[confpassword]');
			$this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == TRUE){
				$salt = $this->salt();
				$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('newpassword')))));
				if($this->common_model->update('users',$user_data,array('user_id'=>$id)))
				{
					$this->session->set_flashdata('msg_success','Password updated successfully.');
				}else{
					$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			
				}
			}	
		}
    	
    	$data['template']='backend/photographer/photographer_add_edit';
		$this->load->view('templates/superadmin_template',$data);
    }
    private function salt()
  	{
    	return substr(md5(uniqid(rand(), true)), 0, 10);
  	}
  	function email_check($str=''){
	  if($this->common_model->get_row('users',array('user_email'=>$str,'user_role ' => 3))):
		    $this->form_validation->set_message('email_check', 'The %s already exists.');
		       return FALSE;
		else:
	       return TRUE;
	    endif;
	 }

    public function photographer_delete($id='')
    {
    	
        $id = $this->uri->segment(4);
        $type = $this->uri->segment(5);
        
        if($type == 3)
        {
        	 $con = array('user_id'=>$id,'user_role'=>3);
        }
       	$this->common_model->delete('users',$con);
        $this->session->set_flashdata('msg_success','Photographer detail delete successfully.');
    	
    	
    }
    public function changeuserstatus_t($id="",$status="",$offset="",$table_name="")	{
		
		 $data['title']='';
		if($status==0) $status=1;
		else $status=0;
		$data=array('status'=>$status);
		if($this->superadmin_model->update($table_name,$data,array('id'=>$id)))
		$this->session->set_flashdata('msg_success','Status Updated successfully.');
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	function verified_jobs($offset=0)
  	{

	    $per_page=25;
	    $data['offset']=$offset;
	    $config=backend_pagination();
	    $data['photographer_user_list'] = $this->common_model->get_result('users',array('user_role'=>3));
	    $config['base_url'] = site_url().'backend/photographer/verified_jobs';
	    $config['total_rows'] = $this->user_model->verified_jobs_result(0,0);
	    $config['per_page'] = $per_page;
	    $config['uri_segment'] = 4;
	    
	    $config['per_page'] = $per_page;
	    $data['result_data'] = $this->user_model->verified_jobs_result($offset,$per_page);
	    //echo $this->db->last_query();
	    if(!empty($_SERVER['QUERY_STRING'])){
	      $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	      $config['suffix'] ='';
	    }
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	    $data['total_records'] = $config['total_rows'];
	    if($config['total_rows'] < $offset)
	    {
	       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
	       redirect('backend/photographer/verified_jobs/0');
	    }
	    $this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();  
	    $data['post_job_id'] = '';
	    if(isset($_POST['assign_user']))
	    {
	    	$this->form_validation->set_rules('photographer_user','Photographer','trim|required');
	    	if ($this->form_validation->run() == TRUE)
			{
				$data_insert['job_id'] = $this->input->post('job_id');
				$data_insert['assign_name'] = $this->input->post('photographer_user');
				if($this->common_model->count_rows('assign_user',array('job_id'=>$this->input->post('job_id')),'',''))
				{
					$this->session->set_flashdata('msg_error','The job is alraedy assign to the photographer.');
					redirect('backend/photographer/verified_jobs');
				}else{
					$this->common_model->insert('assign_user',$data_insert);
					$this->session->set_flashdata('msg_success','Verified job assign to the photographer successfully.');
					redirect('backend/photographer/verified_jobs');
				}
	        	
			}else{
				$data['post_job_id'] = $this->input->post('job_id');
			}
	    }
	    $data['update_photographer_user'] = '';
	    $data['update_post_job_id'] = '';
	    if(isset($_POST['assign_user_update']))
	    {
	    	$this->form_validation->set_rules('update_photographer_user','Photographer','trim|required');
	    	if ($this->form_validation->run() == TRUE)
			{
				
				if($this->common_model->update('assign_user',array('assign_name'=>$this->input->post('update_photographer_user')),array('job_id'=>$this->input->post('job_id_update'))))
				{
					
					$this->session->set_flashdata('msg_success','Verified Job assignee updated successfully.');
					redirect('backend/photographer/verified_jobs');
				}else{
					$this->session->set_flashdata('msg_error','Verified Job assignee not updated.');
					redirect('backend/photographer/verified_jobs');
				}
	        	
			}else{
				$data['update_post_job_id'] = $this->input->post('job_id_update');
				$data['update_photographer_user'] = $this->input->post('update_photographer_user');
			}
	    }
	    $data['template'] = 'backend/photographer/verified_jobs';
	    $this->load->view('templates/superadmin_template',$data);
  	}
	
	function change_status()
    {
        $id = $this->uri->segment(4);
        $value = $this->uri->segment(5);
        $type = $this->uri->segment(6);
        $con = array('user_id'=>$id);
        $update_data = array('user_status'=>$value);
        $this->common_model->update('users',$update_data,$con);
        if($type == 3)
        {
        	 $this->session->set_flashdata('msg_success','Photographer Status changed successfully.');
        	redirect($_SERVER['HTTP_REFERER']);
        }
        
              
    }
}