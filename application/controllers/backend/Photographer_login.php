<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Photographer_login extends CI_Controller {
	public function __construct(){
        parent::__construct();
        clear_cache();
        $this->load->model('superadmin_model');
        $this->load->model('user_model');
        $this->load->model('common_model');

    }
   
	public function index(){
		$this->login();
	}
	private function _check_login(){
		if(photographer_logged_in()===FALSE)
			redirect('backend/photographer_login/login');
	}
	
	public function login(){
		if(photographer_logged_in()===TRUE) redirect('backend/photographer_login/photographer_jobs');
		$data['title']='Photographer login';
		 $this->form_validation->set_rules('password', 'Password', 'trim|required');
		 $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		if ($this->form_validation->run() == TRUE){
			$this->load->model('user_model');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			if($this->user_model->login($email,$password,'photographer','',3)){
			redirect('backend/photographer_jobs');
			}else{
			$this->session->set_flashdata('msg_error','Please fill correct email and password details.');
			}
		}
		$this->load->view('backend/photographer_login/login');
	}
	
	public function logout(){
		$this->_check_login(); //check  login authentication
		$this->session->sess_destroy();
		redirect(base_url().'backend/photographer_login/login');
	}
    
   
    
    public function dashboard(){    
	    $this->_check_login(); //check login authentication
	    $data['template']='backend/photographer_login/dashboard';
	    $this->load->view('templates/photographer_template',$data);
    }

	public function profile(){
	$this->_check_login(); //check login authentication
	$data['title']='profile';
	$this->form_validation->set_rules('firstname', 'First Name', 'required');
	$this->form_validation->set_rules('lastname', 'Last Name', 'required');
	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
	$this->form_validation->set_rules('city', 'City', 'required|alpha_numeric_spaces');
	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	if ($this->form_validation->run() == TRUE)	{
		$user_data  = array(
						'user_first_name'	=>	$this->input->post('firstname'),
						'user_last_name'	=>  $this->input->post('lastname'),
						'user_email'		=>	$this->input->post('email'),
						'city'  => trim($this->input->post('city')),
						);
		if($this->superadmin_model->update('users', $user_data,array('user_id'=>photographer_id()))){
			 $this->session->set_flashdata('msg_success','Profile updated successfully.');
			redirect('backend/photographer_login/profile');
		}else{
			$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			redirect('backend/photographer_login/profile');
		}
	  }else{

		$data['user'] = $this->superadmin_model->get_row('users', array('user_id'=>photographer_id()));
		$data['template']='backend/photographer_login/profile';
		$this->load->view('templates/photographer_template',$data);
	  }
	}
	public function change_password(){
		$this->_check_login(); //check login authentication
		$data['title']='change_password';
		$this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|callback_password_check');
		$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[6]|matches[confpassword]');
		$this->form_validation->set_rules('confpassword','Confirm Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == TRUE){
			$salt = $this->salt();
			$user_data  = array('salt'=>$salt,'password' => sha1($salt.sha1($salt.sha1($this->input->post('newpassword')))));
			$id =photographer_id();
			if($this->superadmin_model->update('users',$user_data,array('user_id'=>$id))){
			$this->session->set_flashdata('msg_success','Password updated successfully.');
			}else{
			$this->session->set_flashdata('msg_error','Update failed, Please try again.');
			
			}
		}
		$data['template']='backend/photographer_login/change_password';
		$this->load->view('templates/photographer_template',$data);
	}
	public function password_check($oldpassword){
		$this->_check_login(); //check login authentication
		$this->load->model('user_model');
		$user_info = $this->user_model->get_row('users',array('user_id'=>photographer_id()));
		       $salt = $user_info->salt;
		if($this->common_model->password_check(array('password'=>sha1($salt.sha1($salt.sha1($oldpassword)))),photographer_id())){
		return TRUE;
		}else{
		$this->form_validation->set_message('password_check', 'The %s does not match.');
		return FALSE;
		}
	}
	public function changeuserstatus_t($id="",$status="",$offset="",$table_name="")	{
		$this->_check_login(); //check login authentication
		 $data['title']='';

		if($status==0) $status=1;
		else $status=0;
		$data=array('status'=>$status);
		if($this->superadmin_model->update($table_name,$data,array('user_id'=>$id)))
		$this->session->set_flashdata('msg_success','Status Updated successfully.');
		redirect($_SERVER['HTTP_REFERER']);
	}

	
	function salt(){
		return substr(md5(uniqid(rand(), true)), 0, 10);
	}
	function error_404(){
     
      $data['template']='backend/404';  
      $this->load->view('templates/photographer_template',$data); 

    } 
    
}