<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contactus extends CI_Controller {
	public function __construct(){
	    parent::__construct();  
	     clear_cache();  
	    $this->load->model('superadmin_model'); 
      $this->load->model('common_model');
      $this->load->model('user_model');
      if(!superadmin_logged_in())
      {
        redirect('backend/superadmin');
      }        
	}
  public function index($offset=0)
  {
    $data['title']='Contact Us'; 
    $per_page=25;
    $data['offset']=$offset;
    $data['contacts'] = $this->superadmin_model->contactus_model($offset,$per_page);

    $config=backend_pagination();
    $config['base_url'] = base_url().'backend/contactus/index';
    $config['total_rows'] = $this->superadmin_model->contactus_model(0,0);
    $config['per_page'] = $per_page;
    $config['uri_segment'] = 4;
    if(!empty($_SERVER['QUERY_STRING'])){
     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
    }
    else{
     $config['suffix'] ='';
    }
    $config['first_url'] = $config['base_url'].$config['suffix'];
    if($config['total_rows'] < $offset)
    {
       $this->session->set_flashdata('msg_warning','Something went wrong ..! Please check it ');    
       redirect('backend/contactus/index/0');
    }
    
    $this->pagination->initialize($config);
    $data['pagination']=$this->pagination->create_links();  
    $data['template'] = 'backend/contactus/index';
    $this->load->view('templates/superadmin_template',$data);
  }
    
  public function contactus_reply($id='')
  {  
   
   
    if($id == '' || !is_numeric($id)){ 
        redirect('backend/superadmin/error_404');
    }
    $row_get = $this->common_model->count_rows('contact_us', array('id'=>$id),'','');
    if($row_get < 1){ 
          redirect('backend/superadmin/error_404');
    }
    $data['title']='Reply';
    $data['contacts'] = $this->superadmin_model->get_result('contact_us',array('id' => $id));
    $data['contacts_msg'] = $this->superadmin_model->get_result_msg($id);
    $this->superadmin_model->update('contact_us',array('read_status'=>1),array('id' => $id));
    $this->form_validation->set_rules('message', 'Message', 'required');
   
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

    if ($this->form_validation->run() == TRUE)
    {   
     // $data_insert['reply'] = $data['contacts'][0]->reply." ".$this->input->post('message');
      $data_insert['status'] = 1;       
      $this->superadmin_model->update('contact_us',$data_insert,$array = array('id' => $id));
      $insert_array = array('contact_parent_id' => $id,'message' => $this->input->post('message'));
      $this->common_model->insert('contact_us_reply',$insert_array);
      
       /*  mail function start */
          $email_template=$this->common_model->get_row('email_templates',array('id'=>15));
          $image_url = base_url().'assets/front/images/click-here-7.png';
          $param=array(
          'template'  =>  array(
                  'temp'  =>  $email_template->template_body,
                  'var_name'  =>  array(
                          'username'  =>  $data['contacts'][0]->name,
                          'site_url' =>  site_url(),
                          'reply' => $this->input->post('message'),
                          'image_url' => $image_url
                               
                    ), 
          ),      
          'email' =>  array(
               'to'    =>   $data['contacts'][0]->email,
              'from'    =>   GLOBAl_INFO_EMAIL,
              'from_name' =>   GLOBAl_INFO_EMAIL,
              'subject' =>   $email_template->template_subject,
            )
          );  
          $status=$this->chapter247_email->send_mail($param);
           

         /*  mail function end */
         

        $this->session->set_flashdata('msg_success','Message sent successfully.');
        redirect('backend/contactus/contactus_reply/'.$id);
      }
    $data['template'] = 'backend/contactus/reply';
    $this->load->view('templates/superadmin_template',$data);
  }
}