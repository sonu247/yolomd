<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Faq extends CI_Controller {
	public function __construct(){
	    parent::__construct();  
	     clear_cache();  
	     $this->load->model('superadmin_model');
	    if(!superadmin_logged_in())
        {
        	redirect('backend/superadmin');
        }         
	}
	public function index($offset=0)
  	{
  		
	    $data['title']='Faq';    
	    $per_page=25;
	    $data['offset']=$offset;
	    $data['faq'] = $this->superadmin_model->faq_model($offset,$per_page);
	    $config=backend_pagination();
	    $config['base_url'] = base_url().'backend/comments/index';
	    $config['total_rows'] = $this->superadmin_model->faq_model(0,0);
	    $config['per_page'] = $per_page;
	    $config['uri_segment'] = 4;
	    if(!empty($_SERVER['QUERY_STRING'])){
	     $config['suffix'] = "?".$_SERVER['QUERY_STRING'];
	    }
	    else{
	     $config['suffix'] ='';
	    }
	    $data['total_records'] = $config['total_rows'];
	    $config['first_url'] = $config['base_url'].$config['suffix'];
	    $this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();
	    $data['offset']=$offset;  

	    //$data['blogs'] = $this->superadmin_model->get_result('blogs',array(),array(),array('order_by','asc'));  
	    $data['template']='backend/faq/index';
	    $this->load->view('templates/superadmin_template',$data);

    }
    public function faq_add()
    {    
       	ini_set('memory_limit', '-1');	
    	
    	$data['title']='add_faq';
		$this->form_validation->set_rules('question','Question','trim|required');
		$this->form_validation->set_rules('answer','Answer','trim|required');
		$this->form_validation->set_rules('order_by', 'Order By', 'required|numeric');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == TRUE)
		{
			$data_insert['question'] =  ucfirst(strip_tags($this->input->post('question')));
			$data_insert['answer'] =  ucfirst(strip_tags($this->input->post('answer')));
			$data_insert['status']=$this->input->post('status');
			$data_insert['order_by']=$this->input->post('order_by');
			$data_insert['created'] =	date('Y-m-d h:i:s A');
			$data_insert['updated'] =	date('Y-m-d h:i:s A');

	        $this->superadmin_model->insert('faq',$data_insert);

			$this->session->set_flashdata('msg_success','Question Added successfully.');
			redirect('backend/faq/');
		   }	  		
    	$data['template']='backend/faq/faq_add';
		$this->load->view('templates/superadmin_template',$data);
    }

    public function faq_edit($id='')
    {    
    	if($id==''){ redirect('backend/faq'); }
       	ini_set('memory_limit', '-1');	
    	$row_get = $this->common_model->count_rows('faq', array('id'=>$id),'','');
	    if($row_get < 1){ 
	      redirect('backend/superadmin/error_404');
	    }
    	$data['title']='edit_faq';
		$this->form_validation->set_rules('question','Question','trim|required|strip_tags');
		$this->form_validation->set_rules('answer','Answer','trim|required');
		$this->form_validation->set_rules('order_by', 'Question Order By', 'required|numeric');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if ($this->form_validation->run() == TRUE)
		{

			$data_insert['question'] =  ucfirst(strip_tags($this->input->post('question')));
			$data_insert['answer'] =  ucfirst(strip_tags($this->input->post('answer')));
			//$data_insert['slug']	=	url_title($this->input->post('blog_title'), '-', TRUE);
			$data_insert['order_by']=$this->input->post('order_by');
			$data_insert['updated'] =	date('Y-m-d h:i:s A');

	        $this->superadmin_model->update('faq',$data_insert,$array = array('id' => $id, ));


			$this->session->set_flashdata('msg_success','Question Updated successfully.');
			redirect('backend/faq/faq_edit'.'/'.$id);
		   }	  

		$data['faq'] = $this->superadmin_model->get_result('faq',$array = array('id' =>$id));
    	$data['template']='backend/faq/faq_edit';
		$this->load->view('templates/superadmin_template',$data);
    }
    public function faq_delete($id='')
    {
    	
        if(empty($id)){ redirect('backend/faq/'); }
        if($this->superadmin_model->delete('faq',array('id'=>$id)))
        {
           $this->session->set_flashdata('msg_success','Question deleted successfully.');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('msg_error','Failed, Please try again.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    public function changeuserstatus_t($id="",$status="",$offset="",$table_name="")	{
		
		 $data['title']='';
		if($status==0) $status=1;
		else $status=0;
		$data=array('status'=>$status);
		if($this->superadmin_model->update($table_name,$data,array('id'=>$id)))
		$this->session->set_flashdata('msg_success','Status Updated successfully.');
		redirect($_SERVER['HTTP_REFERER']);
	}
	
}