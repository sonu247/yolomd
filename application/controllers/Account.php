<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Account extends REST_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('account_model');         
	}
	private function physician_login()
	{
		if(physician_logged_in()===FALSE)
			redirect('account/login');
	}
	public function index_get()
	{
		$this->physician_login(); //check  login authentication
		$data['title']='Dashboard';   
		$data['template'] ='account/dashboard';
		$this->load->view('templates/frontend_template',$data); 
	} 
	public function login_post()
	{
		$data['title']='Login';
		if(physician_logged_in()===TRUE) redirect('account/index');   
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if($this->form_validation->run() == TRUE){
			$data  = array('user_role'=>1, 'status'=>1, 'email'=>$this->input->post('email'),'password' => sha1(trim($this->input->post('password'))));
			if($this->account_model->login($data,'store')){
				redirect('account/index');
			}else{
				$this->session->set_flashdata('theme_danger', 'Incorrect Email or Password.');
				redirect('account/login');
			}
		}   
		$data['template']='account/login';
		$this->load->view('templates/frontend_template',$data); 
	}

}