<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cart extends CI_Controller {
public function __construct(){
    parent::__construct();  
     clear_cache();
     $this->load->library('zip');
     $this->load->model('user_model');
     $this->load->helper('cookie');
    

 }
private function PaypalVarData(){
    $paydata=$this->session->userdata('paydata');
       $i=0;
       $cartdata='';
       if($paydata['placement_city']){
         foreach($paydata['placement_city'] as $value) {
            $cartdata .= '&L_PAYMENTREQUEST_0_NAME'.$i.'='.urlencode($value['city_name']);
            $cartdata .= '&L_PAYMENTREQUEST_0_AMT'.$i.'='.urlencode(number_format($value['city_placement_charge'],2,'.',''));
            $cartdata .= '&L_PAYMENTREQUEST_0_QTY'.$i.'='. urlencode(1);
            $i++;
         }      
        }
        if($paydata['yolomd_verify_fee']){
            $cartdata .= '&L_PAYMENTREQUEST_0_NAME'.$i.'='.urlencode('YoloMD Verified Services');
            $cartdata .= '&L_PAYMENTREQUEST_0_AMT'.$i.'='.urlencode(number_format($paydata['yolomd_verify_fee'],2,'.',''));
            $cartdata .= '&L_PAYMENTREQUEST_0_QTY'.$i.'='. urlencode(1);
        }
       $gross_total=$paydata['grand_total'];
    return array(
        'ItemTotalPrice' => number_format($gross_total,2,'.',''),
        'Discount_amt'   => 0,
        'ShippinCost'    => 0,
        'GrandTotal'     => number_format($gross_total,2,'.',''),
        'Cartdata'       => $cartdata,
        'TotalTaxAmount' => 0.00,
        'HandalingCost'  => 0.00,
        'InsuranceCost'  => 0.00,
        );
     
}
public function paymentByPaypal(){
    $paydata  = $this->session->userdata('paydata');
    if(empty($paydata)){
        redirect('access');
    }
    $job_id = $paydata['job_id'];;
    $en_job_id = base64_encode($job_id);
    $PayPalReturnURL = base_url('cart/paymentByPaypal'); 
    $PayPalCancelURL =  base_url('job-preview/'.$en_job_id);
    require_once(APPPATH."third_party/paypal_express/config.php");
    require_once(APPPATH."third_party/paypal_express/Paypal.class.php");
    $paypalmode = ($PayPalMode=='sandbox') ? '.sandbox' : '';
    $paypalVD=$this->PaypalVarData();
    $padata ='&METHOD=SetExpressCheckout'.
    '&RETURNURL='.urlencode($PayPalReturnURL).
    '&CANCELURL='.urlencode($PayPalCancelURL).
    '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE");
    $padata.=$paypalVD['Cartdata'];
    $padata.='&NOSHIPPING=1'. //set 1 to hide buyer's shipping address, in-case products that does not require shipping
    '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($paypalVD['ItemTotalPrice']).
    '&PAYMENTREQUEST_0_TAXAMT='.urlencode($paypalVD['TotalTaxAmount']).
    '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($paypalVD['ShippinCost']).
    '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($paypalVD['HandalingCost']).
    '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($paypalVD['Discount_amt']).
    '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($paypalVD['InsuranceCost']).
    '&PAYMENTREQUEST_0_AMT='.urlencode($paypalVD['GrandTotal']).
    '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode).
    '&LOCALECODE=GB'. //PayPal pages to match the language on your website.
    '&LOGOIMG='.FRONTEND_THEME_URL.'images/YoloMD_logo.png'. //site logo
    '&CARTBORDERCOLOR=1761a9'. //border color of cart
    '&ALLOWNOTE=1';          
    ############# set session variable we need later for "DoExpressCheckoutPayment" #######
    //We need to execute the "SetExpressCheckOut" method to obtain paypal token
    $paypal= new MyPayPal();
    $httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode); //Respond according to message we receive from Paypal
    if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
        //Redirect user to PayPal store with Token received.
        $paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
        header('Location: '.$paypalurl);
    }else{
        $this->session->set_flashdata('msg_error',urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
        redirect('job-preview/'.$en_job_id);
    }
    //Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
    if(isset($_GET["token"]) && isset($_GET["PayerID"]))
    {
        //we will be using these two variables to execute the "DoExpressCheckoutPayment"
        //Note: we haven't received any payment yet.
        $token = $_GET["token"];
        $payer_id = $_GET["PayerID"];
        $paypalVD=$this->PaypalVarData();
        $padata = '&TOKEN='.urlencode($token).
        '&PAYERID='.urlencode($payer_id).
        '&PAYMENTREQUEST_0_PAYMENTACTION ='.urlencode("SALE"); 
        $padata.=$paypalVD['Cartdata'].
        '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($paypalVD['ItemTotalPrice']).
        '&PAYMENTREQUEST_0_TAXAMT='.urlencode($paypalVD['TotalTaxAmount']).
        '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($paypalVD['ShippinCost']).
        '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($paypalVD['HandalingCost']).
        '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($paypalVD['Discount_amt']).
        '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($paypalVD['InsuranceCost']).
        '&PAYMENTREQUEST_0_AMT='.urlencode($paypalVD['GrandTotal']).
        '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode);
        // echo $padata; die;
        //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
        $paypal= new MyPayPal();
        $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
        //Check if everything went ok..
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
              // add to database
             $orderData              = array(
                'payment_method'        => 'paypal_express_pay',
                'paypal_payment_status' => $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"],
                'transaction_id'        => urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]),
                'other_payment_info'    =>$httpParsedResponseAr,
             );
             //This transaction couldn't be completed. Please redirect your customer to PayPal.
            
             $job_id        = $paydata['job_id'];
             $city_id       = '';
             $itemname      = '';
             $yolomd_verify = 0;
             $check_job_status = '';
             $en_job_id     = base64_encode($job_id);
             $unique_id     = rand(100000,999999).$job_id;
            if($paydata['placement_city']){
             foreach($paydata['placement_city'] as $value) {
                $check_job_status =  get_job_detail($job_id);
                if(!empty($check_job_status))
                {
                    if($check_job_status->status == 0 || $check_job_status->status == 4)
                    {
                        $data_pc['payment_status'] = 0; 
                    }else{
                        $data_pc['payment_status'] = 1;
                        $effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime(date("Y/m/d"))));
                        $data_pc['valid_date'] = $effectiveDate;
                    }
                }
                $itemname .= $value['city_name'].', ';
                $data_pc['city_charge']  = $value['city_placement_charge'];
                $data_pc['job_id']       = $job_id;
                $data_pc['user_id']      = medicaluser_id();
                $data_pc['city_name']    = $value['city_name'];
                $data_pc['city_id']      = $value['city_id'];
                $data_pc['unique_id']    = $unique_id;
                $data_pc['payment_date'] = date('Y-m-d H:i:s A');
                $this->common_model->insert('payment_city',$data_pc);
             }      
            }
            if($paydata['yolomd_verify_fee']){
                 $yolomd_verify =1;
                 $itemname .='Yolomd verified facility';
                 $this->common_model->update('post_jobs',array('job_verify'=>1),array('id'=>$job_id));
            }
            $data['job_id']         = $job_id;
            $data['user_id']        = medicaluser_id();
            $data['item']           = trim($itemname,', ');
            $data['yolomd_verify']  = number_format($paydata['yolomd_verify_fee'],2,'.','');;
            $data['payment_status'] = 1;
            $data['payment_amount'] = number_format($paydata['grand_total'],2,'.','');
            //$data['discount']       = number_format($paydata['discount'],2,'.','');
            $data['txn_id']         = $orderData['transaction_id'];
            $data['custom']         = json_encode($orderData);
            $data['unique_id']      = $unique_id;
            $data['date_time']      = date('Y-m-d H:i:s A');
            $payment_id = $this->common_model->insert('payment',$data);
            $this->session->set_flashdata('theme_success','Payment done successfully. ');
            $this->session->set_userdata('payment_message',$unique_id);
            $get_info_user = '';
            $get_info_user = get_user_info($data['user_id']);
            //echo $get_info_user->user_email;
            //exit;
            $username = $get_info_user->user_first_name.' '.$get_info_user->user_last_name;
            $email_template = $this->common_model->get_row('email_templates',array('id'=>9));
            $city_detail =$this->common_model->city_payment($data['unique_id']);
            if(!empty($city_detail))
            {
                $get_charges = '<table width="100%" style="width:100%;">';
                foreach($city_detail as $city_d){ 
                    if(!empty($city_d['city_name']))
                        $get_charges .= '<tr><td style="color: #504a4a;font-family: Arial,Helvetica,sans-serif;font-size: 15px;line-height: 19px;margin-top: 0;margin-bottom: 20px;padding: 5px 0;font-weight: normal;width:20%;"><b>'.$city_d['city_name'].'</b></td><td style="color: #504a4a;font-family: Arial,Helvetica,sans-serif;font-size: 15px;line-height: 19px;margin-top: 0;margin-bottom: 20px;padding: 0;font-weight: normal;">';
                    if(!empty($city_d['city_charge'])) 
                        $get_charges .= '$'.number_format($city_d['city_charge'],2);
                    // if(!empty($city_d['valid_date']) && $city_d['valid_date'] != '0000-00-00')
                    // { 
                    //     $get_charges .=  '(Active Untill : <span >'.date('d M y', strtotime($city_d['valid_date'])).'</span>)';
                    // }else{
                    //      $get_charges .= ' (NA - Not Active)';
                    // } 
                        $get_charges .= '</td></tr>';
                  
                }
                 $get_charges .= '</table>';
           } 
            $site_url = site_url().'/receipts-download/'.$unique_id;
            $image_url = base_url().'assets/front/images/click-here-7.png';
            $param=array
                    (
                        'template'  =>  
                        array(
                            'temp'  =>  $email_template->template_body,
                            'var_name'  =>  
                            array(
                                    'username' => $username,
                                    'job_id'    => $job_id,
                                    'job_name'  => get_job_name($job_id),
                                    'site_url' => site_url(),
                                    'tid' => $data['txn_id'],
                                    'amount' => '$'.$data['payment_amount'],
                                    //'services' => $data['item'],
                                    'yolomd_verify' =>  '$'.$data['yolomd_verify'],
                                    'get_charges' => $get_charges,
                                    'site_url' => $site_url,
                                    'image_url'=> $image_url
                                    
                                ),   
                        ),   
                        'email' =>  
                        array(
                                'to'        =>   $get_info_user->user_email, 
                                'from'      =>   GLOBAl_INFO_EMAIL,
                                'from_name' =>   GLOBAl_INFO_EMAIL,
                                'subject'   =>   $email_template->template_subject,
                         ),
                    );   
                $status=$this->chapter247_email->send_mail($param); 
                $email_template_admin = $this->common_model->get_row('email_templates',array('id'=>13));
                $param_admin=array
                    (
                        'template'  =>  
                        array(
                            'temp'  =>  $email_template_admin->template_body,
                            'var_name'  =>  
                            array(
                                    'username' => $username,
                                    'job_id'    => $job_id,
                                    'job_name'  => get_job_name($job_id),
                                    'site_url' => site_url(),
                                    'user_id' => $data['user_id'],
                                    'tid' => $data['txn_id'],
                                    'amount' => '$'.$data['payment_amount'],
                                    //'services' => $data['item'],
                                    'yolomd_verify' =>  '$'.$data['yolomd_verify'],
                                    'get_charges' => $get_charges,
                                    'site_url' => site_url().'backend/payment_receipts/receipts_id_post/'.$payment_id,
                                    'image_url'=> $image_url
                                    
                                ),   
                        ),   
                        'email' =>  
                        array(
                            'to'        =>   ADMIN_EMAIL, 
                            'from'      =>   GLOBAl_INFO_EMAIL,
                            'from_name' =>   GLOBAl_INFO_EMAIL,
                            'subject'   =>   $email_template_admin->template_subject,
                         ),
                    );   
            $this->chapter247_email->send_mail($param_admin); 
            redirect('job-preview/'.$en_job_id);
        }
        else{
            $this->session->set_flashdata('msg_error', '<b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]));
            redirect('access/'.$en_job_id);
        }
      } 
    }  
    public function generate_pdf()
    {
        
        $data = [];
        $id = $this->uri->segment(3);
        if(!empty($id))
        {
            $pay_unq = '';
            $jobs_detail =$this->common_model->receipts_user_id($id
                );
            $pay_unq = $jobs_detail['unique_id'];
            $city_detail =$this->common_model->city_payment($pay_unq
                );
            if(!empty($city_detail))
            {
                $data['city_detail'] = $city_detail;
            }
            if($jobs_detail['job_verify'] == 1)
            {
                $ver = 'Yes';
            }else{
                $ver = 'No';
            }
            $y_amount = 0;
            if(!empty($jobs_detail['yolomd_verify']))
            {
                $y_amount = $jobs_detail['yolomd_verify'];
            }
            $date_created = date('d M y',strtotime($jobs_detail['date_time']));
            $paid_amount  = $jobs_detail['payment_amount'];
            $data['jobs_detail'] = array(
                    'item'              => ucfirst($jobs_detail['item']),
                    'job_id'            => $jobs_detail['id'],
                    'job_title'         => $jobs_detail['listing_title'],
                    'verify'            => $ver,
                    'payment_amount'    => '$'.$jobs_detail['payment_amount'],
                    'yolomd_verify'     => '$'.$jobs_detail['yolomd_verify'],
                    'date'              => $date_created,
                    'transaction_id'    => $jobs_detail['txn_id'],
                    'paid_amount'       => '$'.number_format($paid_amount,2)
            );
            
        }
        //load the view and saved it into $html variable
        $html=$this->load->view('account/ReceiptsPdf', $data, true);
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Receipt.pdf";
        //load mPDF library
        $this->load->library('m_pdf');
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");  
    }    
    function image_download($image_name=''){
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$image_name);
        $url = '';        
        $url = base_url().'assets/uploads/jobs/'.$image_name;
        readfile($url);

    }
    function get_profile_name($email='')
    {
        if(!empty($email))
        {
            
        }else{
            //redirect('/');
            $this->session->set_userdata('check_social_login_val',1);
            $this->session->set_userdata('social_login_error','Valid Login Email Required');
            $link= site_url();
            $this->session->set_userdata('check_social_login_val',1);
                     echo "<script language=\"javascript\">
                     window.top.location.href = \"$link \";
                     </script>";
                     exit;
        }    
    }
    function redirect_facebook($id=''){
        redirect('jobs/');
    }
    public function social_login_preview()
    {
        ?>
        <script type="text/javascript">
            (function() {
                if (typeof window.janrain !== 'object') window.janrain = {};
                if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};
                
                janrain.settings.tokenUrl = '<?php echo SOCIAL_LOGIN_URL ?>';

                function isReady() { janrain.ready = true; };
                if (document.addEventListener) {
                  document.addEventListener("DOMContentLoaded", isReady, false);
                } else {
                  window.attachEvent('onload', isReady);
                }

                var e = document.createElement('script');
                e.type = 'text/javascript';
                e.id = 'janrainAuthWidget';

                if (document.location.protocol === 'https:') {
                  e.src = 'https://rpxnow.com/js/lib/yolomd-live/engage.js';
                } else {
                  e.src = 'http://widget-cdn.rpxnow.com/js/lib/yolomd-live/engage.js';
                }

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(e, s);
            })();
        </script>
        <style type="text/css">
            .social-login-content .janrainContent #janrainView #janrainProviderPages + div div a{display: none;}
            #janrainAuthReturnExperience{background-color: transparent !important;border-color: transparent !important;}
            .janrainContent{background-color: transparent !important;border-color: transparent !important;}
        </style>
        <div id="janrainEngageEmbed" class="social-login-content"></div>
        <?php
        die;
    }
    function logins(){
        $link= site_url();
        $this->load->library('janrain');
        $application_domain = 'https://yolomdlog.rpxnow.com/';
        $app_id = 'pfpnhjchfmadelkaijfe';
        $api_key = 'a3b93dccd719679b6be9924ef37171d7e89fce5b';
        $engage_pro = true;
        $debug_array = '';

        $token = $this->input->post('token');

        $format = ENGAGE_FORMAT_JSON;
        $extended = false;

        $result = engage_auth_info($api_key, $token, $format, $extended);
        if ($result) {
            $array_out = true;
            $auth_info_array = engage_parse_result($result, $format, $array_out);
            if (is_array($auth_info_array)) {
                $data_pc['verifiedEmail'] = $auth_info_array['profile']['verifiedEmail'];
                $row = $this->common_model->get_row('social_login',array('verifiedEmail'=>$data_pc['verifiedEmail']));
                if (empty($row)) {
                    $data_pc['providerName'] = $auth_info_array['profile']['providerName'];
                    $data_pc['preferredUsername'] = $auth_info_array['profile']['preferredUsername'];
                    $data_pc['displayName'] = $auth_info_array['profile']['displayName'];
                    $data_pc['email'] = $auth_info_array['profile']['email'];
                    $data_pc['url'] = $auth_info_array['profile']['url'];
                    $data_pc['photo'] = $auth_info_array['profile']['photo'];
                    $data_pc['providerSpecifier'] = $auth_info_array['profile']['providerSpecifier'];
                    $this->common_model->insert('social_login',$data_pc);
                }
                $link='';
                if($this->user_model->login($data_pc['verifiedEmail'],'','user','','',1)){
                    if(medicaluser_logged_in()){
                        $link = site_url().'job-listing';
                    }elseif(physicain_id()){
                        $link= site_url().'jobs/';
                    }
                }else{
                    $this->session->set_userdata('social_login_error','This Email does not exists in our database, please try to login with other email.');
                    $this->session->set_userdata('check_social_login_val',1);
                }
            }
            else{
                $this->session->set_userdata('social_login_error','Something went wrong, please try again.');
                $this->session->set_userdata('check_social_login_val',1);
            }
        }
        echo "<script language=\"javascript\">window.top.location.href = \"$link \"</script>";
        exit;
    }
    public function test_mail()
    {
        $info = phpinfo();
        print_r($info);die;
        ?>
        <!-- <script type="text/javascript">
            alert(mobile = /iPhone|iPad|iPod/i.test(navigator.userAgent));
        </script> -->
        <?php
        $email_template=$this->common_model->get_row('email_templates',array('id'=>7));
        $image_url = base_url().'assets/front/images/click-here-7.png';
        $param=array(
        'template'  =>  array(
                'temp'  =>  $email_template->template_body,
                'var_name'  =>  array(
                        'username'  =>  "dewanshu",
                        'site_url' =>  site_url(),
                        'image_url' => $image_url
                             
                        ), 
        ),      
        'email' =>  array(
            'to'    =>   "rinkesh.chapter247@gmail.com",
            'from'    =>   "test.chapter247@gmail.com",
            'from_name' =>  "Test Chapter",
            'subject' =>   $email_template->template_subject,
          )
        );  
        print_r($status=$this->chapter247_email->send_mail($param));
        die;
    }

    public function upload()
    {
        $source_path = $_FILES[ 'file' ][ 'tmp_name' ];
        $destination_path = './assets/uploads/jobs/'.$_FILES[ 'file' ][ 'name' ];
        //move_uploaded_file($source_path, $destination_path);
        $info = getimagesize($source_path);

        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_path);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_path);

        $exif = exif_read_data($source_path);

        imagealphablending( $image, false ); 
        imagesavealpha( $image, true );
        if (!empty($exif['Orientation']))
        {
            switch ($exif['Orientation'])
            {
                case 3:
                    $image = imagerotate($image, 180, 0);
                    break;
                case 6:
                    $image = imagerotate($image, - 90, 0);
                    break;
                case 8:
                    $image = imagerotate($image, 90, 0);
                    break;
            }
        }
        if ($info['mime'] == 'image/jpeg') imagejpeg($image, $destination_path);
        elseif ($info['mime'] == 'image/png') imagepng($image, $destination_path);

        $upload_file = explode('.', $_FILES[ 'file' ][ 'name' ]);
        $param2=array(
            'image_library' =>  'gd2',
            'source_image'  =>  './assets/uploads/jobs/'.$_FILES[ 'file' ][ 'name' ],
            'new_image'     =>  './assets/uploads/jobs/thumbs/'.$_FILES[ 'file' ][ 'name' ],
            'create_thumb'  =>  FALSE,
            'maintain_ratio'=>  FALSE,
            'width'         =>  600,
            'height'        =>  450,
        );
        $this->load->library('image_lib', $param2); 
        if ( ! $this->image_lib->resize())
        {
            die($this->image_lib->display_errors());
        }   
        die;
    }
}            