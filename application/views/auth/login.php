<!-- Modal -->
<div class="modal fade custom-modal" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-controller="userLogin">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="socal_login_er();"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
       <div class="themeform">
        <section class="">
        <div class="col-md-12 ">
        <div class="login-warp login-popup-block">
        <h3 class="heading text-center">Login</h3>
        <div ng-if="(check_user_login_value == '3') && favorite_error != null" class="error text-center">
           <h4>{{favorite_error}}</h4>
          </div>
           <div ng-if="(check_user_login_value == '3') &&  saved_search_error != null" class="error text-center">
           <h4>{{saved_search_error}}</h4>
          </div>
          <div ng-if="(check_user_login_value == '3') &&  contact_error != null" class="error text-center">
           <h4>{{contact_error}}</h4>
          </div>
          <div ng-if="(check_user_login_value == '3') &&  contact_facility_error != null" class="error text-center">
           <h4>{{contact_facility_error}}</h4>
          </div>
          
         
       <!--  <div class="error">
          <div class="error-block">
          <div ng-if="{{Message}}">
          <div class="message">{{Message}}</div>
          </div>
          </div>
        </div> -->

         <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="login()">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4 text-right block">
            <label for="exampleInputEmail1" class="label required">Email :</label>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 block">
            <input type="email" class="form-control" id="exampleInputEmail1" autocomplete="off" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required." ng-model="user_login.email" >
            <label ng-bind="Error.email" class="error ng-binding"></label>
            </div>
          </div>
          <div class="form-group row">
           <div class="col-md-4 col-sm-4 col-xs-4 text-right block">
            <label for="exampleInputPassword1" class="label required">Password :</label>
           </div>
           <div class="col-md-8 col-sm-8 col-xs-8 block password-block">
            <input type="password" class="form-control" data-bvalidator="required" autocomplete="off" ng-model="user_login.password">
            <label ng-bind="Error.password" class="error ng-binding"></label>
            <div ng-if="ErrorMessage" class="error">
              {{ErrorMessage}}
            </div>
              <!-- <a href="#" tooltip-placement="left" uib-tooltip="Password field must required six character minimum" class="btn btn-sm btn-circle btn-theme">
                <i class="fa fa-question" aria-hidden="true"></i>
              </a> -->
           </div>
          </div>
          <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4"></div>
          <div class="col-md-8 col-sm-8 col-xs-8 block">
           <button type="submit" class="btn btn-login btn-block">Login with email</button>
           
          <a ng-click="closepopup()" data-target="#forgetpasswordModal" data-toggle="modal" class="forgot-password-link" style="cursor:pointer;">Forgot Password?</a>
          </div>
          </div>
        </form>
        </div></div>
        <div class="clearfix"></div>

        <div class="login-divider"><span>Or Login with</span></div>

        <div class="social-login">



        <?php if(!medicaluser_logged_in() &&  !physician_logged_in()) {?>
        <iframe id="the_widget" class="sign_in_box" src="<?php echo SOCIAL_LOGIN_PREVIEW_URL ?>"></iframe> 
        <?php } ?>



  
         <div class="clearfix">
         <!--  
            <div class="col-md-4 icon col-sm-4 col-xs-4 ">
              <a href="#" class="social-btn gplus">
              <span><i class="fa fa-google-plus" aria-hidden="true"></i></span> Google+</a>
            </div>

            <div class="col-md-4 icon col-sm-4 col-xs-4 ">
              <a href="#" class="social-btn facebook">
              <span><i class="fa fa-facebook"></i></span> Facebook</a>
            </div>

            <div class="col-md-4 icon col-sm-4 col-xs-4 ">
              <a href="#" class="social-btn linkedin">
              <span><i class="fa fa-linkedin"></i></span> Linkedin</a>
            </div>
         -->
          </div>
          <div ng-if="social_login_error" class="error text-center">
            {{social_login_error}}
          </div>
         <div class="clearfix"></div>
  
         <h4 class="signup-line">Don’t have an account?</h4>
         <div class="text-center">
          <a ng-click="closepopup()" href="{{ ((check_user_login_value == '3' && favorite_error != '') || (check_user_login_value == '3' &&  contact_error != '') || ((check_user_login_value == '3') &&  saved_search_error != '')) ? 'physician-signup' : 'signup' }}" class="btn btn-signup pop-up-signup-btn">Sign Up</a>
         </div>
         <div class="clearfix"></div>
        </div>
       <!--  <janrain-social-login token-url="tokenUrl()" application-id="applicationId()"></janrain-social-login>  -->
        </section>
       </div>
      </div>
    </div>
  </div>
</div>
<!-- validation -->

<div class="modal fade custom-modal"  id="forgetpasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  ng-controller="userLogin">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content" style="min-height:400px!important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
       <div class="themeform">
        <section class="">
        <div class="col-md-12 ">
        <div class="login-warp login-popup-block">
        <h3 class="heading text-center" >Forgot Password</h3>
         <form  method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="forgetPassword()" id="forgot_pass" ng-show="forgot_password_data">
         <div id="search_loader_forget" class="search_loader_forget" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left: 0;text-align:center; ">
        <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif" style="margin-top:77px;">
        </div>
         <div class="notify-message">
         What e-mail address did you use to sign up? </br>
         <p style="text-align:center;">Your new password information will be sent shortly.</p>
         </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4 text-right block">
            <label for="exampleInputEmail1" class="label required">Email :</label>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 block">
            <input type="email" class="form-control" id="exampleInputEmail1" autocomplete="off" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required." ng-model="user_forget.email" >
            <label ng-bind="Error.email" class="error ng-binding"></label>
            </div>
          </div>
          <div class="row form-group">
          <div class="col-md-4 col-sm-4 col-xs-4"></div>
          <div class="col-md-8 col-sm-8 col-xs-8 block">
           <button type="submit" class="btn btn-login btn-block">Submit</button>
           
          </div>
          </div>
        </form>
        <div  class="forgot-success-msg" ng-bind-html="Message_new" ng-if="Message_new">
        </div>
        </div></div>
    <div class="clearfix"></div>

      

        <div class="social-login">

          </div>
         <div class="clearfix"></div>
      
        <div class="login-divider"></div>
         <div class="password-bottom-section">
         <div class="col-md-6 col-sm-6 col-xs-6 text-center" style="">
          <h4 class="signup-line">Already have an account?</h4>
         <a  ng-click="resetVarible('1')" class="btn btn-signup " data-target="#signupModal" data-toggle="modal" href="#"><span>Login</span></a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-6 text-center">
          <h4 class="signup-line">Don't have an account?</h4>
          <a ng-click="closepopupForget()" href="<?php echo base_url('signup');?>" class="btn btn-signup pop-up-signup-btn">Sign Up</a>
         </div>
         <div class="clearfix"></div>
        </div>
       

        </section>
       </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/jquery.bvalidator.js"></script>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/themes/presenters/default.min.js"></script>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/themes/gray/gray.js"></script>
<link href="<?php echo FRONTEND_THEME_URL ?>js/themes/gray/gray.css" rel="stylesheet" />
<script type="text/javascript">
    $(document).ready(function () {
      $('form').bValidator();
    });
</script>


<style>
div.widget{
  height:50px!important;
}
</style>