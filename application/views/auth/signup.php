<?php 
  $this->load->view('include/header_menu');
    //$this->output->enable_profiler(TRUE);
?>

<div ng-cloak>
<div class="">
<div class="home-signup-warp">
<div class="container home-signup">
 <div class="col-md-6 col-sm-6">
  <div class="section left clearfix">
    <div class="pull-left icon-block">
      <span><img src="<?php echo FRONTEND_THEME_URL ?>images/test/physicianjob_icon.svg"></span>
    </div>
    <div class="pull-left content-block">
    <!-- ngIf: physician_medical_section.title1 -->
    <h2 class="heading ng-scope">
      Doctors &nbsp;and Residents <br>looking for a job?
    </h2><!-- end ngIf: physician_medical_section.title1 -->
    <div class="wow fadeInLeft" data-wow-offset="10" style="visibility: visible; animation-name: fadeInLeft;">
    <!-- ngIf: physician_medical_section.sub_title1 --><p ng-if="physician_medical_section.sub_title1"  class="">Whether you are a family doctor or a specialist, looking for a hospital or a community practice, your next career opportunity is waiting for you. Signup and search now for available positions across Canada.</p><!-- end ngIf: physician_medical_section.sub_title1 -->
    <br>
    <!-- ngIf: physician_medical_section.label1 -->
    <div>
    <a href="<?php echo site_url()?>{{physician_medical_section.link1}}" class="button">SIGN UP AS A DOCTOR</a> 
    </div><!-- end ngIf: physician_medical_section.label1 -->
    </div>
    </div>
    
  </div>
 </div>
 <div class="col-md-6 col-sm-6">
  <div class="section right clearfix">
    <div class="pull-left icon-block">
      <span><img src="<?php echo FRONTEND_THEME_URL ?>images/test/facility-managers.svg"></span>
    </div>
    <div class="pull-left content-block">
    <!-- ngIf: physician_medical_section.title2 -->
    <h2 class="heading" >Facility Managers or Owners <br>looking to post a job?</h2><!-- end ngIf: physician_medical_section.title2 -->
    <div class="wow fadeInRight" data-wow-offset="10" style="visibility: visible; animation-name: fadeInRight;">
      <!-- ngIf: physician_medical_section.sub_title2 -->
      <p >Finding the right doctor to join your organization or expand your practice can be a difficult endeavor. Signup and post a job now to gain exposure to thousands of residents and doctors across Canada.</p>
      <!-- end ngIf: physician_medical_section.sub_title2 -->
      <br>
      <!-- ngIf: physician_medical_section.label2 -->
      <div ><a href="<?php echo site_url()?>{{physician_medical_section.link2}}" class="button ng-binding">SIGN UP AS A MEDICAL FACILITY</a> </div>
      <!-- end ngIf: physician_medical_section.label2 -->
    </div>
    </div>

  </div>

<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<br><br><br><br><br>
</div>
</div>
</div>


<?php 
  $this->load->view('include/footer_menu');
?>
</div>          

<style type="text/css">
  @media(max-width: 800px){
    body{padding-top: 65px;}
  }
</style>
