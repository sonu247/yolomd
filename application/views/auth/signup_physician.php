<?php
$this->load->view('include/header_menu');
?>
<div ng-cloak>
    <div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
        <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
    </div>
    <div class="min-content-block" style="">
        <div class="themeform">
            <!-- <div class="container"> -->
         <div class="col-md-7 col-sm-7 full-height left-side" ng-controller="PhysicianSignup">
                
                  <!--   <div class="physician-section"> -->
                        <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="nextStep()" class="site-form">
                            <div ng-switch on="physicianStep" class="for msection form -section-media-view-table">
                                <section ng-switch-when="1" class="form-section {{animationClass}} wow " ng-class="{{animationClass}}">
                                    <div class="form-section-block">
                                        <h3 class="heading text-center">Doctor Registration</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4 label-block">
                                                        <label class="required label title" >Name Title<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-8 col-sm-8 col-xs-8 signup-full-column physician-name-title">
                                                        <div class="radio radio-info radio-inline">
                                                            <input type="radio" name="user_name_title" id="user_name_title" ng-model="PhysicianData.user_name_title" value="Mr.">
                                                            <label  for="user_name_title">Mr.</label>
                                                        </div>
                                                        <div class="radio radio-info radio-inline">
                                                            <input type="radio" name="user_name_title" id="user_name_title1" ng-model="PhysicianData.user_name_title" value="Miss">
                                                            <label for="user_name_title1">Miss</label>
                                                        </div>
                                                        <div class="radio radio-info radio-inline">
                                                            <input type="radio" name="user_name_title" id="user_name_title2"  ng-model="PhysicianData.user_name_title" value="Mrs.">
                                                            <label for="user_name_title2">Mrs.</label>
                                                        </div>
                                                        <div class="radio radio-info radio-inline">
                                                            <input type="radio" name="user_name_title" id="user_name_title3" ng-model="PhysicianData.user_name_title" value="Dr." data-bvalidator="required"  >
                                                            <label for="user_name_title3">Dr.</label>
                                                        </div>
                                                    </div>
                                                    <label ng-if="Error.user_name_title"> {{ Error.user_name_title}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4  label-block">
                                                        <label class="label">First Name<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
                                                        <input type="text" maxlength="30" name="physician_first_name"  ng-model="PhysicianData.physician_first_name"  class="form-control"  data-bvalidator="required" data-bvalidator-msg="First Name is required" >
                                                        <label class="error" ng-bind="Error.physician_first_name"></label>
                                                    </div>
                                                    <!--  -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4 label-block">
                                                        <label class="label">Last Name<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
                                                        <input type="text" maxlength="30" name="physician_last_name" ng-model="PhysicianData.physician_last_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Last Name is required">
                                                        <label class="error" ng-bind="Error.physician_last_name"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4 label-block">
                                                        <label class="label">Login Email<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
                                                        <input type="text" name="email_address"  ng-model="PhysicianData.email_address"  class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required" id="contact_person_email_address">
                                                        <label class="error" ng-bind="Error.email_address"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4  label-block">
                                                        <label class="label">Confirm Email<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-7  signup-full-column">
                                                        <input type="text" name="conf_email_address"  ng-model="PhysicianData.conf_email_address"  class="form-control" data-bvalidator="equal[contact_person_email_address]l,required" data-bvalidator-msg="Please enter the same Email again">
                                                        <label class="error" ng-bind="Error.conf_email_address"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4  label-block">
                                                        <label class="label">Password<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-7  signup-full-column">
                                                        <input type="password" name="password"  ng-model="PhysicianData.password"  class="form-control" data-bvalidator="required,minlen[6]" data-bvalidator-msg="Password should be at least 6 characters"
                                                        id="password">
                                                        <span class="help-tip-btn">
                                                            <a href="#" class="btn btn-sm btn-circle btn-theme">
                                                                 <i class="fa fa-question" aria-hidden="true"></i>
                                                            </a>
                                                            <div class="tooltip top password-tooltip-btn" role="tooltip">
                                                              <div class="tooltip-arrow"></div>
                                                              <div class="tooltip-inner">
                                                                Password requires six characters minimum
                                                              </div>
                                                            </div>
                                                            
                                                        </span>
                                                        <label class="error" ng-bind="Error.password"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 col-xs-4  label-block">
                                                        <label class="label">Confirm Password<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 col-xs-7  signup-full-column">
                                                        <input type="password" name="confpassword" ng-model="PhysicianData.confpassword" class="form-control"
                                                        data-bvalidator="equal[password],required,minlen[6]"  data-bvalidator-msg="Please enter the same password again">
                                                        
                                                        <label class="error" ng-bind="Error.confpassword"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                                        <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                            <div class="stepToggle-btn">
                                                <span class>
                                                    <button type="submit" class="btn btn-success down" >Next <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </section>
                                <section ng-switch-when="2" class="form-section  {{animationClass}} wow " ng-class="{{animationClass}}">
                                    <div class="form-section-block">
                                        <h3 class="heading text-center">Professional Profile</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label title">Gender<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <div class="radio radio-info radio-inline">
                                                            <input type="radio" name="physician_gender" ng-model="PhysicianData.physician_gender" id="physician_gender" value="Male" >
                                                            <label class="radio-inline" for="physician_gender">Male </label>
                                                        </div>
                                                        <div class="radio radio-info radio-inline">
                                                            <input type="radio" name="physician_gender" id="physician_gender1" ng-model="PhysicianData.physician_gender" value="Female" data-bvalidator="required" >
                                                            <label class="radio-inline" for="physician_gender1">Female</label>
                                                        </div>
                                                        <label class="error" ng-bind="Error.physician_gender"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label">Date of birth<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <div class="row dobBlock">
                                                            <div class="col-md-4 col-sm-4 col-xs-4 date">
                                                                <div class="">
                                                                    <select name="physician_dob_date" ng-model="PhysicianData.physician_dob_date" class="form-control" data-bvalidator="required">
                                                                        <option value="">Date</option>
                                                                        <?php
                                                                        for ($i = 1; $i <= 31; $i++) {
                                                                        ?>
                                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label class="error" ng-bind="Error.physician_dob_date"></label>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4 month">
                                                                <div class="">
                                                                    <?php $month_name = month_name(); ?>
                                                                    <select name="physician_dob_month" ng-model="PhysicianData.physician_dob_month" class="form-control" data-bvalidator="required">
                                                                        <option value="">Month</option>
                                                                        <?php
                                                                        foreach ($month_name as $key => $month_n) {
                                                                        ?>
                                                                        <option value="<?php echo $key; ?>"><?php echo $month_n; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label class="error" ng-bind="Error.physician_dob_month"></label>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4 year">
                                                                <div class="">
                                                                    <select name="physician_dob_year" ng-model="PhysicianData.physician_dob_year" class="form-control" data-bvalidator="required">
                                                                        <option value="">Year</option>
                                                                        <?php
                                                                        for ($j = date('Y') - 85; $j <= date('Y') - 15; $j++) {
                                                                        ?>
                                                                        <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label class="error" ng-bind="Error.physician_dob_year"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label">I am currently a<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <select name="physician_profession" ng-model="PhysicianData.physician_profession" class="form-control" ng-if="currently_status" data-bvalidator="required">
                                                            <option value="">Select Physician</option>
                                                            <option ng-repeat="(key,value) in currently_status" ng-if="key" value="{{key}}">{{value}}</option>
                                                        </select>
                                                        <label class="error" ng-bind="Error.physician_profession"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label">Medical School<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <select name="medical_school" ng-model="PhysicianData.medical_school" class="form-control" ng-if="medical_school" data-bvalidator="required">
                                                            <option value="">Medical School </option>
                                                            <option ng-repeat="medical_s in medical_school" ng-if="medical_s.id" value="{{medical_s.id}}">
                                                                {{medical_s.name}}
                                                            </option>
                                                        </select>
                                                        <label class="error" ng-bind="Error.medical_school"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label">Graduation Year<font> :</font>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <select name="traning_year" ng-model="PhysicianData.traning_year" class="form-control" data-bvalidator="required">
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            for ($j = date('Y') - 50; $j <= date('Y') + 5; $j++) {
                                                            ?>
                                                            <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <label class="error" ng-bind="Error.traning_year"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label">Medical Specialty<font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <select name="medical_specialty" ng-model="PhysicianData.medical_specialty" class="form-control" ng-if="medical_specialty" data-bvalidator="required">
                                                            <option value="">Select Medical Specialty</option>
                                                            <option ng-repeat="medical_spe in medical_specialty" ng-if="medical_spe.id" value="{{medical_spe.id}}">{{medical_spe.name}}</option>
                                                        </select>
                                                        <label class="error" ng-bind="Error.medical_specialty"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                                        <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                            <input type="hidden" name="physicianStep" value="1" >
                                            <div class="stepToggle-btn">
                                                <span class>
                                                    <a href="#" class="btn btn-success up" ng-click="previousStep()">
                                                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
                                                </span>
                                                <button type="submit" class="btn btn-success down">Next <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!--     <section ng-switch-when="3" class="form-section ">
                                    <div class="form-section-block">
                                        <h3 class="heading text-center">MINC</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                        <label class="label">MINC <font> :</font> </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                        <input type="text" name="physician_minc" ng-model="PhysicianData.physician_minc" class="form-control" data-bvalidator="number,maxlen[10]" value="CAMD" placeholder="CAMD">
                                                        <font class="charcter-limit">(character limit is 10 numbers)</font>
                                                        
                                                        <a href="http://www.minc-nimc.ca/what-is-a-minc/" target="new" class="whats-link"><i class="fa fa-question-circle-o" aria-hidden="true"></i> what is this?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4"></div>
                                        <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                            <div class="stepToggle-btn">
                                                <span class>
                                                    <a href="#" class="btn btn-success up" ng-click="previousStep(2)"><i class="fa fa-angle-left" aria-hidden="true"></i> Back</a>
                                                </span>
                                                <button type="submit" class="btn btn-success down" >
                                                Next <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </section> -->
                                <section ng-switch-when="3"  class="form-section  {{animationClass}} wow " ng-class="{{animationClass}}"><div class="form-section-block">
                                    <h3 class="heading text-center">Contact</h3>
                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label"> Country: </label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <select name="physician_country" id="country" ng-model="PhysicianData.physician_country" class="form-control">
                                                        <option value="">Select Country</option>
                                                        <option value="Canada" >Canada</option>
                                                    </select>
                                                    <span class="optional-tag">(optional)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label"> Province: </label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <select name="physician_state" ng-model="PhysicianData.physician_state" class="form-control"  ng-if="canada_province">
                                                        <option value="">Select Provice</option>
                                                        <option ng-repeat="canada_pro in canada_province" ng-if="canada_pro.id" value="{{canada_pro.id}}">{{canada_pro.province_name}}</option>
                                                    </select>
                                                    <span class="optional-tag">(optional)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label"> City:</label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <input type="text" name="physician_city" ng-model="PhysicianData.physician_city" class="form-control">
                                                    <span class="optional-tag">(optional)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label"> Address:</label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <textarea name="physician_address" ng-model="PhysicianData.physician_address" class="form-control" ></textarea>
                                                    <span class="optional-tag">(optional)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label">Postal Code:</label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <input type="text" name="physician_postal_code" ng-model="PhysicianData.physician_postal_code" class="form-control" style="text-transform: uppercase;" placeholder="A1B 2C3" data-bvalidator="minlen[5],maxlen[7]">
                                                    <span class="optional-tag">(optional)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label">Phone Number<font> :</font> </label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
                                                    <input type="text" name="physician_phone_number" ng-model="PhysicianData.physician_phone_number" placeholder="(123) 456-7890" class="form-control" mask='999-999-9999' data-bvalidator="minlen[12],maxlen[12],required,pattern[\d{3}-\d{3}-\d{4}]" data-bvalidator-msg="Invalid Phone number" >
                                                    <label class="error" ng-bind="Error.physician_phone_number"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4"></div>
                                    <div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
                                        <div class="stepToggle-btn">
                                            <span class>
                                                <a href="#" class="btn btn-success up" ng-click="previousStep()">
                                                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back
                                                </a>
                                            </span>
                                            <button type="submit" class="btn btn-success down">
                                            Next <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                                <section ng-switch-when="4"  class="form-section  {{animationClass}} wow " ng-class="{{animationClass}}">
                                <div class="form-section-block">
                                    <h3 class="heading text-center">Profile Customization</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label">Default Search Specialty<font> :</font></label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <select name="medical_selectSpecialty" ng-model="PhysicianData.medical_selectSpecialty" class="form-control" ng-if="medical_specialty" >
                                                        <option value="">Select Medical Specialty</option>
                                                        <option ng-repeat="medical_spe in medical_specialty" ng-if="medical_spe.id" value="{{medical_spe.id}}">{{medical_spe.name}}</option>
                                                    </select>
                                                    <span class="optional-tag">(optional)</span>
                                                    <label class="error" ng-bind="Error.medical_specialty"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label">Default Search Province<font> :</font></label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7 ">
                                                    <select name="physician_selectState" ng-model="PhysicianData.physician_selectState" class="form-control" ng-if="canada_province">
                                                        <option value="">Select Province</option>
                                                        <option ng-repeat="canada_pro in canada_province" ng-if="canada_pro.id" value="{{canada_pro.province_name}}">{{canada_pro.province_name}}</option>
                                                    </select>
                                                    <span class="optional-tag">(optional)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label">Default Message<font> :</font> </label>
                                                </div>
                                                <div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
                                                    <textarea name="physician_default_msg" ng-model="PhysicianData.physician_default_msg" class="form-control" rows="7" placeholder="Type a generic message to quickly click and send messages to medical facilities from their ad page"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group clearfix">
                                                <div class="col-md-4 col-sm-4 label-block col-xs-4 ">
                                                    <label class="label">Opt <font> :</font> </label>
                                                </div>
                                                <div class="col-md-6 col-sm-8 signup-full-column col-xs-8 ">
                                                    <input type="text" name="opt_number" ng-model="PhysicianData.opt_number" value="" class="form-control">
                                                    <label class="error" ng-bind="Error.opt_number"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                    <div class="col-md-4 col-sm-4 col-xs-4 term-col4"></div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-4 col-sm-4 label-block col-xs-4">
                                                
                                            </div>
                                            <div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
                                                

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4 term-col4"></div>
                                    <div class="col-md-6 col-sm-7 col-xs-7 physician-submit-block">
                                        <div class="form-group termblock-checkbox">
                                            <div class="checkbox checkbox-info">
                                                <input type="checkbox" name="term" id="term" value="1" data-bvalidator="required" data-bvalidator-msg="Accept Term & Conditions.">
                                                <label for="term">I accept the<a href="{{site_url}}terms-of-use" target="_blank"> Terms of Use</a></label>
                                            </div>
                                            <label class="error" ng-bind="Error.term"></label>
                                            
                                        </div>
                                        <div class="row">
                                                <div class="col-md-6">
                                                 <div class="captchaImg-block">
                                                    <img src="<?php echo generate_captcha('physician')?>" class="captchaImg"/>
                                                    <button type="button" class="btn btn-theme refresh_btn" ng-click="ReloadCaptcha()" tooltip-placement="top" uib-tooltip="Refresh Captcha"><i class="fa fa-refresh"></i></button>
                                                 </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="captcha" ng-model="PhysicianData.captcha" class="form-control" id="captcha" data-bvalidator="required,maxlen[6]" maxlength="6" size="6" Placeholder="Enter text shown in the image"/>
                                                    <label ng-bind="Message" class="error"></label> 
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>

                                        <div class="physician-submit-warp pull-right">
                                            <button type="submit" name="" value="Register as a physician" class="btn down btn-blue">
                                            Submit <span><i class="fa fa-angle-right"></i></span>
                                            </button>
                                        </div>
                                        <div class="stepToggle-btn pull-right" style="margin-top:0px;">
                                            <span class><a href="#" class="btn btn-success up" ng-click="previousStep(4)">
                                            <i class="fa fa-angle-left" aria-hidden="true"></i> Back</a></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </section>
                                <section ng-switch-when="5"  class="form-section  {{animationClass}} wow " ng-class="{{animationClass}}">
                                <div class="form-section-block">
                                    <div class="success-block text-center">
                                        <div class="icon">
                                            <!--    <i class="fa fa-check fa-1" aria-hidden="true"></i> -->
                                            <img src="<?php echo FRONTEND_THEME_URL ?>/images/success-icon.png" class="img-responsive center-block">
                                        </div>
                                        <div class="success-content">
                                            <img src="<?php echo FRONTEND_THEME_URL ?>/images/congratulation.png" class="img-responsive center-block">
                                            Physician registration completed!
                                            <div class="clearfix"></div>
                                            <br>
                                            <a href="#" data-toggle="modal" class="button" data-target="#signupModal">CLICK HERE TO LOGIN</a>
                                            <div class="clearfix"></div>
                                            <br>
                                            <p class="text-center info-line">Please check your spam folder for confirmation e-mail </p>
                                            <!--        <div class="stepToggle-btn pull-right" style="margin-top:0px;">
                                                <span class><a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep(5)"><i class="fa fa-angle-left" aria-hidden="true"></i></a></span>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>
     <!--            </div> -->
                <div class="clearfix"></div>
                <div class="process-bar col-md-7 col-sm-7 col-xs-12">
                    <div class="">
                        <div class="progress-section">
                            <!--progress bar strip-->
                            <div class="progress">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{progressBar}}%">
                                    <span class="sr-only">{{progressBar}}% Complete </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="completion"><font> {{progressBar}}% </font> Complete</div>
                            <!--progress bar strip end-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
          
  <!--           <div class="visible-xs hidden-md hidden-sm">
                <a href="#">View Tips</a>
            </div> -->
        </div>
         <!-- <div class="right section"> -->
         <div class="col-md-5 col-sm-5 register-right-section page-container ">
            <div class="col-md-10 col-md-offset-1 register-right-content signup-page">
                <!--     <br>
                <h1>Doctor Facility Registration</h1>
                <br> -->
                <div class="content-block">
                    <div class="content-block-inner">
                        <h2 class="text-center">DOCTOR SIGN UP</h2>
                        <br>
                        <div ng-repeat="physician_c in physician_content" ng-if="physician_content">
                            <div class="signup-step-content">
                                <div ng-if="physician_c.title" class="title">
                                    {{physician_c.title}}
                                </div>
                                <div ng-if="physician_c.description" class="description">
                                    {{physician_c.description}}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div ng-if="physician_medical_section.link1" class="text-center">
                            <a href="<?php echo site_url() ?>{{physician_medical_section.link1}}" class="signup-btn">{{physician_medical_section.label1}} <span><i class="fa fa-chevron-right"></i></span></a> </div>
                        </div>
                    </div>
                </div>
            </div>
         <div class="clearfix"></div>
        </div>
    </div>
    <script type="text/javascript">
    $(function () {
    $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
    <script type="text/javascript">
    wow = new WOW(
    {
    boxClass: 'wow',
    animateClass: 'animated',
    offset: 200,
    mobile: true,
    live: true
    }
    )
    wow.init();
    //event.stopPropagation();
    </script>
    <!-- validation -->
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/jquery.bvalidator.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/themes/presenters/default.min.js"></script>
    <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/themes/gray/gray.js"></script>
    <link href="<?php echo FRONTEND_THEME_URL ?>js/themes/gray/gray.css" rel="stylesheet" />
    <script type="text/javascript">
    // jQuery(document).ready(function ($) {

    // var windowWidthNew   = $(window).width();
    // var windowHeight = $(window).height();
    // if(windowWidthNew >= 767){        
    //   jQuery(window).on("resize", function () {
    //     var windowHeight = $(window).height();
    //     $('.formPage-section').height(windowHeight);
    //     $('body').height(windowHeight);
    //     var formSection = $('.form-section-block').innerHeight();
    //     var leftSide = $('.left-side').innerHeight();
    //     if (formSection > leftSide) {
    //     $('.formsection').css('height', '100%');
    //     } else {
    //     $('.formsection').css('height', '100%');
    //     }
    //     $('.left-side').css('height', windowHeight);
    //   }).resize();
    // }
    // else{
    //    $('.formsection').css('min-height','1000px' );
    // }

   
    // });

       jQuery(document).ready(function ($) {
        jQuery(window).on("resize", function () {
            var windowWidthNew   = $(window).width();
            var windowHeight = $('window').height();

            //alert($('.form-section-block').height());
            var windowHeight = $(window).height();
                if(windowHeight >= 769){

                  $('.register-right-content').removeClass('vertical-center');
                  $('.register-right-content').addClass('vertical-center');

                  $('.site-form').removeClass('vertical-center');
                  $('.site-form').addClass('vertical-center');
                  console.log("height");
                }else{

                  $('.site-form').removeClass('vertical-center');
                  $('.site-form').addClass('check-iphone');

                  $('.register-right-content').removeClass('vertical-center');

                }
        }).resize();
    });
    </script>
  <style type="text/css">
    html{position: relative;}
    body{
        overflow: hidden;
        position: absolute;
        width: 100%;
        top: 0;
        left: 0;
    }
    .register-right-content{margin-top: 15px;}
    .left-side{/*padding-bottom: 100px;padding-top: 80px;*/}
    .content-section{
        padding-bottom: 100px;padding-top: 100px;
    }
    .min-content-block{
        position: absolute;
        overflow: hidden;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .form-section{
        display: block;
        position: relative;
    }
    .site-form{
        display: block;
        margin-bottom: 100px;
        margin-top: 80px;
    }
    .vertical-center{
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }
    @media(max-width: 480px){
        body{overflow:auto !important;position: relative;}
        .min-content-block{overflow-y: auto;height: auto;}
    }

    .form-section{width: 100%;}
    </style>
</div>