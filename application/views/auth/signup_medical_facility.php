<?php
$this->load->view('include/header_menu');
?>
<div ng-cloak>
	<div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
		<img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
	</div>
<div class="min-content-block" style="">
		<div class="themeform">
			<!-- <div class="container"> -->
			<div class="col-md-7 col-sm-7 full-height left-side" ng-controller="MedicalFacilitySignup">
				
				<!-- 	<div class="medical-section fadeInDown wow" ng-class="fadeInDown"> -->
						<form method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="SubmitMedicalSignup()" class="site-form">
							<div ng-switch on="medicalStep" class="formse ction form-sec tion-media-view-table">
								<section ng-switch-when="1" class="form-section form-section-med ia-view fadeInDown wow" ng-class="fadeInDown">
									<div class="form-section-block">
										<h3 class="heading text-center">Medical Facility Registration </h3>
										
										<div class="row">
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="required label title" > Name Title <font>:</font> </label>
													</div>
													<div class="col-md-8 col-sm-7 signup-full-column col-xs-7">
														<div class="radio radio-info radio-inline">
															<input type="radio" name="user_name_title" id="user_name_title" ng-model="MedicalData.user_name_title" value="Mr.">
															<label for="user_name_title">Mr.</label>
														</div>
														<div class="radio radio-info radio-inline">
															<input type="radio" name="user_name_title" id="user_name_title1"  ng-model="MedicalData.user_name_title" value="Miss">
															<label for="user_name_title1">Miss</label>
														</div>
														<!-- <div class="radio radio-info radio-inline">
															<input type="radio" name="user_name_title" id="user_name_title2"  ng-model="MedicalData.user_name_title" value="Mrs.">
															<label for="user_name_title2">Mrs.</label>
														</div> -->
														<div class="radio radio-info radio-inline">
															<input type="radio" name="user_name_title" id="user_name_title3"  ng-model="MedicalData.user_name_title" value="Dr." data-bvalidator="required" >
															<label for="user_name_title3">Dr.</label>
														</div>
														<div class="clearfix"></div>
														<label ng-if="Error.user_name_title" >
															<span class="error">*Requried</span>
														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label required">First Name<font> :</font> </label>
													</div>
													<div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
														<input type="text" name="contact_person_first_name" ng-model="MedicalData.contact_person_first_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="First Name is required">
														<label ng-if="Error.contact_person_first_name" class="error-warp" >
															<span class="error">*Requried</span>
														</label>
													</div>
												</div>
												
											</div>
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label required">Last Name<font> :</font> </label>
													</div>
													<div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
														<input type="text" name="contact_person_last_name" ng-model="MedicalData.contact_person_last_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Last Name is required">
														<label ng-if="Error.contact_person_last_name" class="error-warp">
															<span class="error">*Requried</span>
														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label required">Login Email<font> :</font> </label>
													</div>
													<div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
														<input type="text" name="contact_person_email_address" ng-model="MedicalData.contact_person_email_address"  class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required." id="contact_person_email_address">
														<label ng-bind="Error.contact_person_email_address" class="error"></label>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label required">Confirm Email<font> :</font> </label>
													</div>
													<div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
														<input type="text" name="conf_email_address" ng-model="MedicalData.conf_email_address" class="form-control" data-bvalidator="equal[contact_person_email_address]l,required" data-bvalidator-msg="Please enter the same Email again" >
														<label ng-bind="Error.conf_email_address" class="error"></label>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label required">Password<font> :</font> </label>
													</div>
													<div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
														<input type="password" name="password" ng-model="MedicalData.password" class="form-control" data-bvalidator="required,minlen[6]" id="password" data-bvalidator-msg="Password should be at least 6 <br/> characters">
                                                        <span class="help-tip-btn">
                                                            <a href="#" class="btn btn-sm btn-circle btn-theme">
                                                                 <i class="fa fa-question" aria-hidden="true"></i>
                                                            </a>
                                                            <div class="tooltip top password-tooltip-btn" role="tooltip">
                                                              <div class="tooltip-arrow"></div>
                                                              <div class="tooltip-inner">
                                                                Password requires six characters minimum
                                                              </div>
                                                            </div>
                                                            
                                                        </span>														
														<label ng-bind="Error.password" class="error"></label>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label required">Confirm Password<font> :</font> </label>
													</div>
													<div class="col-md-6 col-sm-7 signup-full-column col-xs-7">
														<input type="password" name="confpassword" ng-model="MedicalData.confpassword" class="form-control" data-bvalidator="equal[password],required,minlen[6]" data-bvalidator-msg="Please enter the same password again">													
														<label ng-bind="Error.confpassword" class="error"></label>
													</div>
												</div>
											</div>
										</div>
										
									<!-- 	</section>
									<section class="form-section"> -->
										<div class="row">
											<div class="col-md-4 col-sm-4 term-col4 col-xs-4 "></div>
											<div class="col-md-6 col-sm-7 physician-submit-block col-xs-7">
												<div class="form-group medicaltermblock-checkbox">
													<div class="checkbox checkbox-info">
														<input type="checkbox" name="term" id="term"  data-bvalidator="required" >
														<label for="term">I accept the<a href="{{site_url}}terms-of-use" target="_blank"> Terms of Use</a></label>
													</div>
													<label ng-bind="Error.term" class="error"></label>

												</div>
										
													<div class="row">
													<div class="col-md-6 ">
													 <div class="captchaImg-block">
														<img src="<?php echo generate_captcha('medical')?>" class="captchaImg"/>

                                                		<button type="button" class="btn btn-theme refresh_btn" ng-click="ReloadCaptcha()" tooltip-placement="top" uib-tooltip="Refresh Captcha"><i class="fa fa-refresh"></i></button>
													 </div>
													</div>
													<div class="col-md-6">
														<input type="text" name="captcha" ng-model="MedicalData.captcha" class="form-control" id="captcha" data-bvalidator="required,maxlen[6]" maxlength="6" size="6" Placeholder="Enter text shown in the image"/>
														<label ng-bind="Message" class="error"></label>				
													</div>
													</div>
													<div class="clearfix"></div>
												<div class="physician-submit-warp pull-right">
													<button type="submit" name="" value="Register as a physician" class="btn-blue"  data-bvalidator="required" >
													Submit <span><i class="fa fa-angle-right"></i></span>
													</button>
												</div>
												<!--
														<div class="col-md-10 col-md-offset-1">
															<div ng-if="Message">
																	<div class="alert alert-danger">{{Message}}</div>
															</div>
												</div> -->
											</div>
										</div>
									</div>
								</section>
								<section ng-switch-when="2" class="form-section">
									<!-- 	  	<h3>Registration done sucessfully.....!</h3><br>
									Click to <a href="#" class="login-icon icon"  data-toggle="modal" data-target="#signupModal">Login</a> -->
									<div class="success-block text-center">
										<div class="icon">
											<!-- 	<i class="fa fa-check fa-1" aria-hidden="true"></i> -->
											<img src="<?php echo FRONTEND_THEME_URL ?>/images/success-icon.png" class="img-responsive center-block">
										</div>
										<div class="success-content">
											<img src="<?php echo FRONTEND_THEME_URL ?>/images/congratulation.png" class="img-responsive center-block">
											Medical facility registration completed!
											<div class="clearfix"></div>
											<br>
											
											<a href="#" data-toggle="modal" class="button" data-target="#signupModal">CLICK HERE TO LOGIN</a>
											<div class="clearfix"></div>
											<br>
											<p class="text-center info-line">Please check your spam folder for confirmation e-mail </p>
										</div>
									</div>
								</section>
							</div>
						</form>
					<!-- </div> -->
			
			</div>
			<div class="col-md-5 col-sm-5 register-right-section page-container" style="">
				<div class="col-md-10 col-md-offset-1 register-right-content signup-page">
					<!-- 	 <h1>Medical Facility Registration</h1> -->
					<!-- 	 <br> -->
					<div class="content-block">
						<div class="content-block-inner">
							<h2 class="text-center">MEDICAL FACILITY REGISTRATION</h2>
							<br>
							<div ng-repeat="medical_f in medical_facility" ng-if="medical_facility">
								<!--  <span class="count">{{$index+1}}</span> -->
								<div class="signup-step-content" >
									<div ng-if="medical_f.title" class="title">
										{{medical_f.title}}
									</div>
									<div ng-if="medical_f.description" class="desc">
										{{medical_f.description}}
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- </div> -->
			</div>
		</div>
		<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
		<script>
			new WOW().init();
		</script>
		<script type="text/javascript">
		// jQuery(document).ready(function($) {
		// 	jQuery(window).on("resize", function() {
		// 		var windowWidthNew   = $(window).width();
		// 		var windowHeight = $(window).height();
		// 		if(windowWidthNew >= 767){
					
		// 			$('.formPage-section').height(windowHeight);
		// 			$('body').height(windowHeight);
		// 			var formSection = $('.form-section-block').innerHeight();
		// 			var leftSide = $('.left-side').innerHeight();
		// 			if(formSection > leftSide){
		// 			$('.formsection').css('height','100%');
		// 			}
		// 			else{
		// 				$('.formsection').css('height','100%' );
		// 			}
		// 			$('.left-side').css('height', windowHeight);
		// 		}
		// 		else{
		// 			$('.formsection').css('min-height','1100px' );
		// 		}
		// 	}).resize();
		// });

		jQuery(document).ready(function ($) {
        jQuery(window).on("resize", function () {
            var windowWidthNew   = $(window).width();
            var windowHeight = $('window').height();

            //alert($('.form-section-block').height());
            var windowHeight = $(window).height();
                if(windowHeight >= 769){
                  $('.register-right-content').removeClass('vertical-center');
                  $('.register-right-content').addClass('vertical-center');

                  $('.site-form').removeClass('vertical-center');
                  $('.site-form').addClass('vertical-center');
                  console.log("height");
                }else{
                  $('.site-form').removeClass('vertical-center');
                  $('.site-form').addClass('check-iphone');

                  $('.register-right-content').removeClass('vertical-center');

                }
        }).resize();
    });
		</script>
		 <style type="text/css">
    html{position: relative;}
    body{
        overflow: hidden;
        position: absolute;
        width: 100%;
        top: 0;
        left: 0;
    }
    .register-right-content{margin-top: 15px;}
    .left-side{padding-bottom: 50px;}
    .content-section{
        padding-bottom: 100px;padding-top: 100px;
    }
    .min-content-block{
        position: absolute;
        overflow: hidden;
        height: 100%;
        width: 100%;
        top: 0;
        left: 0;
    }
    .form-section{
        display: block;
        position: relative;
    }
    .site-form{
        display: block;
        margin-bottom: 70px;
        margin-top: 80px;
    }
    .vertical-center{
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }
    @media(max-width: 480px){
        body{overflow:auto !important;position: relative;}
        .min-content-block{overflow-y: auto;height: auto;}
    }

    .form-section{width: 100%;}
    </style>
	</div>
</div>