<?php  $segment= $this->uri->segment(3); 
 $main_segment= $this->uri->segment(2);
 $footer_menu = ''; 
 $need_help_content = '';
 $home_title = '';
 $home_page='';
 $appex1 = '';
 $appex2 = '';
 $signup_content = '';
 $postjob ='';
 $city = '';
 $faq = '';
 $faq_add ='';
 $faq_ind ='';
 $content = '';
 $photographer = '';
 $photographer_list = '';
 $verified_job = '';
if($segment=='dashboard' || $segment=='change_password' || $segment=='profile') $dashboard='active'; else $dashboard='';
if($main_segment=='cms'){
    $home_page='active';
    if($segment=='home_page' || $segment == 'add_banner' || $segment == 'add_city')
    {
      $home_title = 'active';
    }elseif($segment=='footer_menu' || $segment=='sub_menu')
    {
      $footer_menu = 'active';
    }elseif($segment=='appex1')
    {
      $appex1 = 'active';
    }elseif($segment=='appex2')
    {
      $appex2 = 'active';
    }elseif($segment=='signup_content')
    {
        $signup_content = 'active';
    }elseif($segment=='city_manage')
    {
        $city = 'active';

    }elseif($segment=='create_content')
    {
         $content = 'active';
        
    }else{
       $home_page='';
    }
    
}
if($main_segment=='payment_receipts'){
   $payment_receipts = 'active';
}
if($main_segment=='faq' && $segment == ''){
   $faq = 'active';
   $faq_ind = 'active';
  
}
if($main_segment=='faq' && $segment == 'faq_add'){
  $faq = 'active';
  $faq_add = 'active';  
}
if($main_segment=='cms' &&  $segment == 'setting'){
   $setting = 'active';
}
if($main_segment=='listusers'&&$segment!='postjob'){
   $listusers = 'active';
   if($segment=='physician' || $segment == 'edit_physician')
    {
      $physician = 'active';
    }elseif($segment=='medical' || $segment == 'edit_medical')
    {
      $medical = 'active';
    }
    elseif($segment=='payment')
    {
      $postjob = 'active';
    }else{
      $listusers = '';
    }
}
if($main_segment=='jobs'){

  if(!empty($_GET['status']) && $_GET['status'] == 5)
  {
    $pending_jobs = 'active';
  }elseif(!empty($_GET['status']) && ($_GET['status'] == 1 || $segment == 'edit_medical'))
  {
    $active_jobs = 'active';
  }
  elseif(!empty($_GET['status']) && $_GET['status'] == 2)
  {
    $deactive_jobs = 'active';
  }
  elseif(!empty($_GET['status']) && $_GET['status'] == 3)
  {
    $banned_jobs = 'active';
  }
  elseif(!empty($_GET['status']) && $_GET['status'] == 4)
  {
    $job_in_draft = 'active';
  }
  elseif($segment=='' || (!empty($_GET['status']) && $_GET['status'] == 'All'))
  {
    $all_jobs = 'active';
  }
  $jobs = 'active';
}
if($main_segment=='photographer'){
   $photographer = 'active';
   if($segment=='photographer_add_edit' || $segment== '')
    {
      $photographer_list = 'active';
    }elseif($segment=='verified_jobs')
    {
      $verified_job = 'active';
    }elseif($segment== 'index'){
      $photographer_list = 'active';
    }
}
if($main_segment=='email_templates' || $segment=='email_templates_add' || $segment=='email_templates_edit') $email_template='active'; else $email_template='';
if($main_segment=='contactus') $contactus='active'; else $contactus=''; 
if($main_segment=='messages') $messages='active'; else $messages=''; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="">
  <link rel="icon" href="<?php echo base_url()?>assets/front/images/fevicon.png" type="image/x-icon" />
  <title>YoloMD Admin</title>
  <!-- Bootstrap core CSS -->
  <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  
  <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap-reset.css" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo BACKEND_THEME_URL ?>css/font-awesome.min.css" rel="stylesheet" />
  <link href="<?php echo BACKEND_THEME_URL ?>plugin/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
  <link rel="stylesheet" href="<?php echo BACKEND_THEME_URL ?>css/owl.carousel.css" type="text/css">
  <!-- Custom styles for this template -->
  <link href="<?php echo BACKEND_THEME_URL ?>css/style.css" rel="stylesheet">
  <link href="<?php echo BACKEND_THEME_URL ?>css/style-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo BACKEND_THEME_URL ?>css/easyzoom.css">
  <script src="<?php echo BACKEND_THEME_URL ?>js/jquery.js"></script>

</head>
<body>   
<section id="container" >
    <!--header start-->
    <header class="header">
    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-padding">
    <div class="sidebar-toggle-box">
    <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
    </div>

    <!--logo start-->
    <a href="<?php echo base_url('backend/superadmin')?>" class="logo">Yolo<span>MD</span></a>
    <!--logo end-->
    </div>
    <!-- <div class="col-md-8 col-lg-8"> -->
    <div class="top-nav col-md-10 col-lg-10 col-sm-10 col-xs-10">
    <div class="col-md-10 col-lg-10 col-sm-8 col-xs-8 padding-left">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

    </div>
    <!--search & user info start-->
    <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4 no-padding">

    <ul class="nav pull-right ">
    <li class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle admin-logout" href="#">
        <img alt="" src="<?php echo BACKEND_THEME_URL ?>img/avatar1_small.jpg">
        <span class="username"><?php echo superadmin_name() ?></span>
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu extended logout">
        <div class="log-arrow-up"></div>
         <li><a href="<?php echo base_url()?>" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i>Frontend Url</a></li>
        <li><a href="<?php echo base_url()?>backend/superadmin/profile"><i class="fa fa-suitcase"></i>My Profile</a></li>
        <li><a href="<?php echo base_url()?>backend/superadmin/change_password"><i class="fa fa-cog"></i>Change Password</a></li>                           
        <li><a href="<?php echo base_url()?>backend/superadmin/logout"><i class="fa fa-key"></i> Log Out</a></li>
      </ul>
    </li>

    <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
    </div>
    </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
      
       <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
            <li>
              <a class="<?php echo $dashboard; ?>" href="<?php echo base_url('backend/superadmin/dashboard') ?>">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>            
             
             <li class="sub-menu">
              <a  href="javascript:;" class="<?php if(!empty($listusers)) echo $listusers; ?>">
                <i class="fa fa-user"></i>
                <span>Users Managment</span>
              </a>
              <ul class="sub">
                <li class="sub-menu">
                  <a class="<?php if(!empty($physician)) echo $physician; ?>"  href="<?php echo base_url('backend/listusers/physician') ?>">  <i class="fa fa-user-md"></i></i>Doctor's Section</a>
                </li>
                <li class="sub-menu">
                  <a class="<?php if(!empty($medical)) echo $medical; ?>"  href="<?php echo base_url('backend/listusers/medical') ?>">  <i class="fa fa-heartbeat"></i></i>Medical User</a>
                </li>
               <!-- <li class="sub-menu">
                  <a class="<?php if(!empty($payment)) echo $payment; ?>"  href="<?php echo base_url('backend/listusers/payment') ?>"> <i class="fa fa-money" aria-hidden="true"></i>
                  Payment</a>
                </li> -->
              
              </ul>
            </li>
           <li class="sub-menu">
              <a  href="javascript:;" class="<?php if(!empty($jobs)) echo $jobs; ?>">
                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                <span>Jobs Managment</span>
              </a>
              <ul class="sub">
                <li class="sub-menu">
                  <a class="<?php if(!empty($pending_jobs)) echo $pending_jobs; ?>"  href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=5&order=DESC&verify=All'); ?>">  <i class="fa fa-minus-square" aria-hidden="true"></i>Pending Jobs</a>
                </li>
                <li class="sub-menu">
                  <a class="<?php if(!empty($active_jobs)) echo $active_jobs; ?>"  href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=1&order=DESC&verify=All'); ?>">  <i class="fa fa-check" aria-hidden="true"></i> Active Jobs</a>
                </li>                               
               <li class="sub-menu">
                  <a class="<?php if(!empty($deactive_jobs)) echo $deactive_jobs; ?>"
                 href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=2&order=DESC&verify=All'); ?>"> <i class="fa fa-times" aria-hidden="true"></i>Deactive Jobs</a>
                </li>
                <li class="sub-menu">
                  <a class="<?php if(!empty($banned_jobs)) echo $banned_jobs; ?>" href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=3&order=DESC&verify=All'); ?>"> <i class="fa fa-ban" aria-hidden="true"></i>Banned Jobs</a>
                </li>
                <li class="sub-menu">
                  <a class="<?php if(!empty($job_in_draft)) echo $job_in_draft; ?>" href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=4&order=DESC&verify=All'); ?>"> <i class="fa fa-circle" aria-hidden="true"></i>Jobs In Draft</a>
                </li>

                <li class="sub-menu">
                  <a class="<?php if(!empty($all_jobs)) echo $all_jobs; ?>"  href="<?php echo base_url('backend/jobs') ?>"><i class="fa fa-list-alt" aria-hidden="true"></i>All Jobs</a>
                </li>
              </ul>
            </li>
            <!-- <li class="sub-menu">
              <a  href="<?php //echo base_url('backend/payment_receipts/payment_list') ?>" 
              class="<?php //if(!empty($payment_receipts)) echo $payment_receipts; ?>">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <span>Payment Receipts</span>
              </a>          
            </li> -->
            <li class="sub-menu">
              <a  href="javascript:;" class="<?php echo $home_page; ?>">
                <i class="fa fa-laptop"></i>
                <span>Content Management</span>
              </a>
              <ul class="sub">
                <li class="sub-menu">
                  <a class="<?php echo $home_title; ?>"  href="<?php echo base_url('backend/cms/home_page/1') ?>">  <i class="fa fa-home"></i></i>Home Page </a>
                </li>
                <li class="sub-menu">
                  <a class="<?php echo $footer_menu; ?>"  href="<?php echo base_url('backend/cms/footer_menu') ?>">  <i class="fa fa-list"></i></i>Footer Menu </a>
                </li>
                
                <li class="sub-menu">
                  <a class="<?php echo $appex1; ?>"  href="<?php echo base_url('backend/cms/appex1') ?>"> <i class="fa fa-medkit" aria-hidden="true"></i> Canadian Schools</a>
                </li>
                <li class="sub-menu">
                  <a class="<?php echo $appex2; ?>"  href="<?php echo base_url('backend/cms/appex2') ?>"> <i class="fa fa-plus-square" aria-hidden="true"></i> Medical Specialties List  
                  </a>
                </li>
                <li class="sub-menu">
                  <a class="<?php echo $signup_content; ?>"  href="<?php echo base_url('backend/cms/signup_content') ?>"> <i class="fa fa-stethoscope" aria-hidden="true"></i>Doctor & Medical Content</a>
                </li>
<!--                  <li class="sub-menu">
                  <a class="<?php //echo $city; ?>"  href="<?php //echo base_url('backend/cms/city_manage') ?>"> <i class="fa fa-building-o" aria-hidden="true"></i>City Manage</a>
                </li> -->
                 <li class="sub-menu">
                  <a class="<?php echo $content; ?>"  href="<?php echo base_url('backend/cms/create_content') ?>"><i class="fa fa-book"></i>Page Content </a>
                </li>
                
              </ul>
            </li>
            <li class="sub-menu">
              <a  href="javascript:;" class="<?php if(!empty($faq)) echo $faq; ?>">
                <i class="fa fa-user"></i>
                <span>FAQ Managment</span>
              </a>
              <ul class="sub">
                <li class="sub-menu">
                  <a class="<?php if(!empty($faq_ind)) echo $faq_ind; ?>"  href="<?php echo base_url('backend/faq') ?>"><i class="fa fa-question-circle"></i>Faq</a>
                </li>
                <li class="sub-menu">
                  <a class="<?php if(!empty($faq_add)) echo $faq_add; ?>"  href="<?php echo base_url('backend/faq/faq_add') ?>">   <i class="fa fa-question-circle"></i>Faq Add</a>
                </li>
             
              
              </ul>
            </li>
            <li class="sub-menu">
            <a  href="<?php echo base_url('backend/email_templates') ?>" class="<?php echo $email_template; ?>">
              <i class="fa fa-envelope-o"></i>
              <span>Email Templates</span>
            </a>
            
          </li>
              <!-- <li class="sub-menu">
              <a  href="javascript:;" class="<?php if(!empty($photographer)) echo $photographer; ?>">
                <i class="fa fa-camera" aria-hidden="true"></i>
                <span>Photographers</span>
              </a> -->
              <!-- <ul class="sub">
                <li class="sub-menu">
                  <a class="<?php if(!empty($photographer_list)) echo $photographer_list; ?>"  href="<?php echo base_url('backend/photographer') ?>"><i class="fa fa-camera" aria-hidden="true"></i>Photographers</a>
                </li> -->
              <!--   <li class="sub-menu">
                  <a class="<?php //if(!empty($verified_job)) echo $verified_job; ?>"  href="<?php //echo base_url('backend/photographer/verified_jobs') ?>">   <i class="fa fa-check"></i>Verified Job</a>
                </li>
             
               -->
              <!-- </ul> -->
            <!-- </li> -->
            <!-- support -->
            <li  >
           <a class="<?php echo $messages; ?>" href="<?php echo base_url() ?>backend/messages"> <i class="fa fa-flag-o  <?php echo $messages; ?>"></i>Flagged Messages</a>
          <!-- setting -->
          </li>

          <li  >
           <a class="<?php echo $contactus; ?>" href="<?php echo base_url() ?>backend/contactus"> <i class="fa fa-phone <?php echo $contactus; ?> "></i>  Support</a>
          <!-- setting -->
          </li>

            <li >
                  <a class="<?php if(!empty($setting)) echo $setting; ?>"  href="<?php echo base_url('backend/cms/setting') ?>">  <i class="fa fa-cog"></i></i>Setting</a>
            </li>
          </ul>
   
    <!--multi level menu end-->
   
    <!-- sidebar menu end-->
    </div>
  </aside>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
       <?php msg_alert(); ?>  
        <section class="panel">