    </section>
                </div>
            </div>
        </section>
    </section>
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php echo BACKEND_THEME_URL ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>js/jasny-bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo BACKEND_THEME_URL ?>js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.nicescroll.js" type="text/javascript"></script>

<script src="<?php echo BACKEND_THEME_URL ?>js/owl.carousel.js" ></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/jquery.customSelect.min.js" ></script>
<script src="<?php echo BACKEND_THEME_URL ?>js/respond.min.js" ></script>
<!--common script for all pages-->
<script src="<?php echo BACKEND_THEME_URL ?>js/common-scripts.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({
      //selector: "textarea",
      selector: ".tinymce_edittor",
      relative_urls : false,
     remove_script_host : false,
     convert_urls : true,
      menubar: false,
      relative_urls : false,
      remove_script_host : false,
      convert_urls : false,

     // language : 'he_IL',
     // theme_advanced_buttons1_add : "ltr,rtl"
      height :250,
      plugins: [
          "advlist autolink lists link image charmap print preview anchor media",
          "searchreplace visualblocks code fullscreen",
          "insertdatetime table contextmenu paste textcolor directionality",
      ],
      image_advtab: true,
      toolbar: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | preview code ",     
       file_browser_callback : elFinderBrowser,  
  });
function elFinderBrowser (field_name, url, type, win) {
tinymce.activeEditor.windowManager.open({
  file: '<?php echo base_url("elfinders") ?>',// use an absolute path!
  title: 'File Manager',
  width: 900,
  height: 450,
  resizable: 'yes'
}, {
  setUrl: function (url) {
    win.document.getElementById(field_name).value = url;
  }
});
return false;
}
</script>
<script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });
    $(function(){
        $('select.styled').customSelect();
    });
</script>
</body>
</html>