  <div id="ng_load_plugins_before"></div>
  <!-- Include all compiled plugins (below), or include individual files as needed -->

  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/ng-tags-input.min.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/select2.min.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/select2.js"></script>

  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/bootstrap.min.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/angular-sanitize.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/angular-ui-router.min.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/ocLazyLoad.min.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/app.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/service/service.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/controller/CommonCtrl.js"></script>
  <!--graph-->
  <script src = "<?php echo FRONTEND_THEME_URL ?>js/vendor/zingchart.min.js" ></script>  
  <script src = "<?php echo FRONTEND_THEME_URL ?>js/vendor/zingchart-angularjs.js" ></script> 
  <!-- Slick Slider -->
  <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/slick.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/slick.min.js"></script>
  <!-- UI bootstrap -->

  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/ui-bootstrap-tpls-1.3.3.js"></script>
  <!-- Mask -->
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/ngMask.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>file_upload/exif.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>file_upload/angular-file-upload.min.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>file_upload/console-sham.min.js"></script>

  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/selectMultipleCheckbox.js" type="text/javascript" ></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.10.1/lodash.js" type="text/javascript" ></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-local-storage/0.2.7/angular-local-storage.js"></script> -->

  <script src="<?php echo FRONTEND_THEME_URL ?>js/lodash.js" type="text/javascript" ></script>

  <script src="<?php echo FRONTEND_THEME_URL ?>js/angular-local-storage.js"></script>
  
  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBOrLgvREO-Gt4k0XP7qCEa6vskE5Zz1q0&extension=.js" async defer></script>

  
   
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/customGoogleMap.js"></script>
  <!-- For Pdf -->
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/rpx_style.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL?>js/vendor/ng-infinite-scroll.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
    // site preloader -- also uncomment the div in the header and the css style for #preloader
      $(window).load(function(){
        setTimeout(function(){
          // $('.site-loader').hide();
        }, 2000);
      });
      // $('iframe').contents().find('#widget').css('opacity','0.1');
    });
    function window_play() {
      $(document).ready(function() {
        var windowWidthNew   = $(window).width();
        if(windowWidthNew < 769){
          var result = $('body').find('.dashboard-right');
          if(result.length > 0){
            $(".dashboard-right").append( $( "<div class='info-cover'><div class='cover'><h2>Scroll or Swipe to Checkout more options</h2><img src='<?php echo FRONTEND_THEME_URL ?>/images/icons/swipe-icon.svg'/></div></div>" ) );
          }
          setTimeout(function(){
            $('.dashboard-right').find('.table').animate({'margin-left': -200},500);
          }, 1000);
          // $('.dashboard-right').find('.table').css("margin-left", "200px");
          setTimeout(function(){
            $('.info-cover').fadeOut();
            $('.dashboard-right').find('.table').animate({'margin-left': 0},500);
          }, 3500);
        }
      });
    }
  </script>
  <!--  <iframe id="the_widget" class="sign_in_box" src="" scrolling="no" ></iframe> -->
  <script type="text/javascript"> 
  // $(document).ready(function(){
  //     setupInit();
  // });
  </script>
  </body>
</html>