<html lang="en" ng-app="yolomd">
<head>

  <title data-ng-bind="'YoloMD | ' + $state.current.data.pageTitle"></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <link rel="icon" href="<?php echo FRONTEND_THEME_URL ?>images/fevicon.png" type="image/x-icon" />

<!--   <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php //echo FRONTEND_THEME_URL ?>images/apple-touch-icon-72x72.svg" sizes="72x72" /> -->
  <!-- <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php //echo FRONTEND_THEME_URL ?>images/apple-touch-icon-114x114.svg" sizes="114x114" />
  <link rel="apple-touch-icon-precomposed" type="image/x-icon" href="<?php //echo FRONTEND_THEME_URL ?>images/apple-touch-icon-144x144.svg" sizes="144x144" /> -->
 

  

  <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">

  <base href="<?php echo base_url()?>" >
  <?php 
  $meta_title = "Job Search and Recruitment For Doctors Across Canada";
  $meta_image = FRONTEND_THEME_URL."images/apple-touch-icon-144x144.jpg";
  $meta_url = base_url();
  $meta_description = "YoloMD, Job Search and Recruitment For Doctors Across Canada";
  if($this->uri->segment(1) != '' && $this->uri->segment(1) == 'jobs' && $this->uri->segment(2)!= '')
  {
               
    $job_detail = '';
    $job_detail  = get_job_detail($this->uri->segment(2));
    if($job_detail)
    {
      if(!empty($job_detail->listing_title))
      {
        $meta_title = $job_detail->listing_title;
      }
      if(!empty($job_detail->listing_description))
      {
        $meta_description =  $job_detail->listing_description;
      }
      if(!empty($job_detail->id))
      {
        $meta_url = base_url().'jobs/'.$job_detail->id;
      }
      if($job_detail->uploadImageArray)
      {
          $image_new = unserialize($job_detail->uploadImageArray);
          if(file_exists('assets/uploads/jobs/'.$image_new[0]['imageName']))
          {
            if(!empty($image_new))
            {
                
              $meta_image = base_url().'assets/uploads/jobs/'.$image_new[0]['imageName'];
            }
          } 
        }

      }?> 
  <?php }?> 

  <meta property="fb:app_id" content="943245869113092"/>
  <meta property="og:title" content="<?php if(!empty( $meta_title)) echo  $meta_title; ?>" name='title'/>
  <meta property="og:site_name" content="American Eagle Outfitters" />
  <meta property="og:image" content="<?php if(!empty( $meta_image)) echo  $meta_image; ?>" />
  <meta property="og:url" content="<?php if(!empty( $meta_url)) echo  $meta_url; ?>" />
  <meta property="og:type" content="yolomddotcom:jobs" />
  <meta property="og:description" content="<?php if(!empty( $meta_description)) echo  $meta_description; ?>"/>
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <!-- CSS -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'> 
  <!-- Bootstrap -->
  <link href="<?php echo FRONTEND_THEME_URL ?>css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/slick.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/slick-theme.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/style.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/animate.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/select.css" rel="stylesheet">
  <link href="<?php echo FRONTEND_THEME_URL ?>css/select2.css" rel="stylesheet">

  <link href="<?php echo FRONTEND_THEME_URL ?>css/ie.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo FRONTEND_THEME_URL ?>css/ng-tags-input.min.css" />
  <?php $this->load->view('include/js_var'); ?>
  <!-- js -->
  <!--[if lte IE 8]> <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui/0.4.0/angular-ui-ieshiv.js"></script><![endif]-->
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/jquery.js"></script>

  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/angular.min.js"></script>
 
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/angular-route.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/base64.js"></script>
  <!-- <script src="<?php //echo base_url();?>sample/demo.js"></script> -->
  <script src="https://apis.google.com/js/platform.js" async defer></script>

</head>

<body ng-cloak  class="{{bodyClass}}" ng-class="{'form-page-header':(headerClass == 'PhysicianSignup' || headerClass == 'MedicalFacilitySignup' || headerClass == 'PostJob' || headerClass == 'CreateClone')}">
   
<div ng-if="showGlobalLoader" class="site-loader">
 <img src="<?php echo FRONTEND_THEME_URL ?>images/small-loader-GIF.gif" alt="" class="img-responsive">
</div>
