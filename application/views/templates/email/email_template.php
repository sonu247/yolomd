<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head><title>yolomd</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <header http-equiv="MIME-version: 1.0\n" content="Content-type: text/html; charset= iso-8859-1\n">
      
      </header><!-- /header -->
      <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
    </head>
    
    <body style="width:100% !important; margin:0 !important; padding:0 !important; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; background-color:#71889a;font-family: arial;background:url(<?php echo base_url();?>assets/front/images/default-list-thumb.jpg);">
      <table align="center" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="height:auto !important; margin:0; padding:0; width:100% !important;color:#222222;font-family: arial;background:url(<?php echo base_url();?>assets/front/images/default-list-thumb.jpg); ">
        <tr>
          <td>
            <div id="tablewrap" style="width:100% !important; max-width:550px !important;margin-top:40px !important; margin-right: auto !important; margin-bottom:40px !important; margin-left: auto !important;overflow: hidden;">
              <table id="contenttable" align="center" width="550" align="center" cellpadding="0" cellspacing="0" border="0" style="background-color:#FFFFFF; margin-top:50px !important; margin-right: auto !important; margin-bottom:50px !important; margin-left: auto !important; width: 550px !important; max-width:550px !important;overflow: hidden;">
                <tr>
                  <td width="100%">
                    <table border="0" cellspacing="7" cellpadding="" width="100%" style="">
                      <tr>
                        <td bgcolor="#ffffff" colspan="2" style="text-align:center;"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/front/images/YoloMD_logo-2.png"  border="0" width="200"></a>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%">
                  <table border="0" cellspacing="0" cellpadding="0" width="100%" style="">
                    <tr>
                      <td colspan="2" style="color: #fff;">
                        <img src="<?php echo base_url();?>assets/front/images/search-bg-img.png" class="bannerImg" id="bannerImg" width="550" style="width: 552px;">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td>
                  <table border="0" cellspacing="0" cellpadding="10" width="100%" style="border: 30px solid #fff;">
                    <tr>
                      <td width="100%" style="font-size: 13px;color: #555;line-height: 20px;">
                        <?php echo $email_message; ?>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="100%">
                  <table border="0" cellspacing="20" cellpadding="0" width="100%" style="">
                    <tr>
                      <td style="text-align:center;"><a href="<?php echo base_url();?>jobs/"><img src="<?php echo base_url();?>assets/front/images/search-a-job.png"  border="0" width="175"></a>
                    </td>
                    <td style="text-align:center;"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/front/images/post-job-btn.png"  border="0" width="175"></a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="vertical-align: middle;text-align: center;">
              <table width="100%" cellspacing="10" style="background-color:#d0d8df;">
                <tr><td width="50%" style="text-align: right;font-family: arial;font-size: 14px;">Follow Us</td>
                <td width="50%"  style="text-align: left;">
                  <a href="<?php echo base_url();?>" style="display:inline-block;vertical-align: middle;text-decoration: none;float: left;">
                  <img src="<?php echo base_url();?>assets/front/images/faceboook.png"></a>
                  <a href="<?php echo base_url();?>" style="display:inline-block;vertical-align: middle;text-decoration: none;float: left;">
                  <img src="<?php echo base_url();?>assets/front/images/snapchat.png"></a>
                  <a href="<?php echo base_url();?>" style="display:inline-block;vertical-align: middle;text-decoration: none;float: left;">
                  <img src="<?php echo base_url();?>assets/front/images/instagram.png"></a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="background-color:#d0d8df;">
            <table cellpadding="2" cellspacing="5" style="color:#0b5fa5;width:50%;table-layout:fixed;line-height: 13px;background-color:#d0d8df;margin-left: auto;margin-right: auto;" align="center">
              <tr>
                <td style="text-align: center;width:21%;border-right: 1px solid #6e7f88;"><a href="<?php echo base_url();?>/contact-us" style="color:#6e7f88;font-family: arial;text-decoration: none;font-size: 13px;font-weight:600;">Contact Us</a></td>
                <td style="text-align: center;width:25%;border-right: 1px solid #6e7f88;"><a href="<?php echo base_url();?>/privacy-policy" style="color:#6e7f88;font-family: arial;font-size: 13px;font-weight:600;text-decoration: none;">Privacy-policy</a></td>
                <td style="text-align: center;width:10%;"><a href="<?php echo base_url();?>/faq" style="color:#6e7f88;font-family: arial;font-size: 13px;font-weight:600;text-decoration: none;">FAQ&#39;s</a></td>
                
              </tr>
            </table>
          </td>
        </tr>
        <tr style="background:#d0d8df">
          <td style="text-align:center;color:#838383;font-size:12px;padding-top:5px;padding-bottom:5px;font-family: arial;" colspan="2">&copy;  <?php echo date('Y') ?> YoloMD | 4060 Saint-Catherine Street West, <br>Westmount, QC, Canada, H3Z 2Z3</td>
        </tr>
        <tr>
          <td style="background-color:#d0d8df;">
            <table width="100%" cellpadding="10" style="border-top: 1px solid #dad8d8;color: #3e4f56;font-size:12px;background-color:#c1ccd6;">
              <tr>
                <td style="text-align:right;font-family: arial;"><img src="<?php echo base_url();?>assets/front/images/phone-receiver.png" width="10">&nbsp;<b>Phone Number :</b><a href="tel: +1 800-000-0000" style="text-decoration: none;color:#3e4f56;">  +1 800-000-0000 </a></td>
                <td style="text-align:left;font-family: arial;"><img src="<?php echo base_url();?>assets/front/images/envelope.png" width="10">&nbsp;<b>Mail Us :</b> <a href="mailto:support@yolomd.com" style="color:#3e4f56;text-decoration: none;"><font color="color: #3e4f56;" style="color: #3e4f56;">support@yolomd.com</font></a></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </div>
  </td>
</tr>
</table>
</body>
</html>