<?php  $segment= $this->uri->segment(3); 
 $main_segment= $this->uri->segment(2);
 
if($segment=='dashboard') $dashboard='active'; else $dashboard='';
if($segment=='change_password') $change_password='active'; else $change_password='';
if($segment=='profile') $profile='active'; else $profile='';
if($main_segment=='photographer_jobs' && ($segment =='' || $segment == 'photograph_edit' )) $jobs='active'; else $jobs='';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="">
  <link rel="icon" href="<?php echo base_url()?>assets/img/favicon.png" type="image/x-icon" />
  <title>YoloMD Photographer Admin</title>
  <!-- Bootstrap core CSS -->
  <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  
  <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo BACKEND_THEME_URL ?>css/bootstrap-reset.css" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo BACKEND_THEME_URL ?>css/font-awesome.min.css" rel="stylesheet" />
  <link href="<?php echo BACKEND_THEME_URL ?>plugin/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
  <link rel="stylesheet" href="<?php echo BACKEND_THEME_URL ?>css/owl.carousel.css" type="text/css">
  <!-- Custom styles for this template -->
  <link href="<?php echo BACKEND_THEME_URL ?>css/style.css" rel="stylesheet">
  <link href="<?php echo BACKEND_THEME_URL ?>css/style-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo BACKEND_THEME_URL ?>css/easyzoom.css">
  <script src="<?php echo BACKEND_THEME_URL ?>js/jquery.js"></script>

</head>
<body>   
<section id="container" >
    <!--header start-->
    <header class="header">
    <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 no-padding">
    <div class="sidebar-toggle-box">
    <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
    </div>

    <!--logo start-->
    <a href="<?php echo base_url('backend/photographer_login')?>" class="logo">Yolo<span>MD</span></a>
    <!--logo end-->
    </div>
    <!-- <div class="col-md-8 col-lg-8"> -->
    <div class="top-nav col-md-10 col-lg-10 col-sm-10 col-xs-10">
    <div class="col-md-10 col-lg-10 col-sm-8 col-xs-8 padding-left">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

    </div>
    <!--search & user info start-->
    <div class="col-md-2 col-lg-2 col-sm-4 col-xs-4 no-padding">

    <ul class="nav pull-right ">
    <li class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle admin-logout" href="#">
        <img alt="" src="<?php echo BACKEND_THEME_URL ?>img/avatar1_small.jpg">
        <span class="username"><?php echo photographer_name() ?></span>
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu extended logout">
        <div class="log-arrow-up"></div>
         <li><a href="<?php echo base_url()?>" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i>Frontend Url</a></li>
        <li><a href="<?php echo base_url()?>backend/photographer_login/profile"><i class="fa fa-suitcase"></i>My Profile</a></li>
        <li><a href="<?php echo base_url()?>backend/photographer_login/change_password"><i class="fa fa-cog"></i>Change Password</a></li>                           
        <li><a href="<?php echo base_url()?>backend/photographer_login/logout"><i class="fa fa-key"></i> Log Out</a></li>
      </ul>
    </li>

    <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
    </div>
    </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
      
       <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
          <!--   <li>
              <a class="<?php //echo $dashboard; ?>" href="<?php //echo base_url('backend/photographer_login/dashboard') ?>">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>     -->
             <li>
              <a class="<?php echo $profile; ?>" href="<?php echo base_url('backend/photographer_login/profile') ?>">
                <i class="fa fa-suitcase"></i>
                <span>My Profile</span>
              </a>
            </li> 
             <li>
              <a class="<?php echo $jobs; ?>" href="<?php echo base_url('backend/photographer_jobs') ?>">
                <i class="fa fa-graduation-cap"></i>
                <span>Jobs</span>
              </a>
            </li>     
             <li>
              <a class="<?php echo $change_password; ?>" href="<?php echo base_url('backend/photographer_login/change_password') ?>">
                <i class="fa fa-cog"></i>
                <span>Change Password</span>
              </a>
            </li>            
              <li>
              <a class="" href="<?php echo base_url('backend/photographer_login/logout') ?>">
                <i class="fa fa-key"></i>
                <span>Logout</span>
              </a>
            </li>    
          </ul>
                        
    <!--multi level menu end-->
   
    <!-- sidebar menu end-->
    </div>
  </aside>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
       <?php msg_alert(); ?>  
        <section class="panel">