<?php 
  $this->load->view('include/header_menu');
?>

<div ng-cloak>
<div class="theme-bg">

<div class="container">

<div class="">
 <div class="col-md-10 col-md-offset-1" >
  <div class="contact-warp">
 <div class="themeform">
 <form method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="SubmitContactUS()">
  <div class="">
  	<div  data-target="#common_modal_msg" data-toggle="modal"></div>
   <section class="" >
	<div class="form-section-block">
	<h3 class="heading text-center">Contact Us</h3>

	<div class="contactBlock clearfix">
	 <div class="col-md-4 col-sm-4 column">
	   <div class="imgBlock">
	 	<img src="<?php echo FRONTEND_THEME_URL ?>/images/message.svg" width="60">
	   </div>
	 	<span class="shadow"></span>
	 	<?php $get_result = get_particular_option();?>
	 
     <!--  <div><span>CA: </span>+1 999-000-8765</div> -->
	   <p><a href="#"> <?php if(!empty($get_result)){ if(!empty($get_result[16]->option_value)) echo $get_result[16]->option_value; } ?></a></p>
	 </div>
	 <div class="col-md-4 col-sm-4 column">
	   <div class="imgBlock">
	 	<img src="<?php echo FRONTEND_THEME_URL ?>/images/contact-no.svg" width="60">
	   </div>
	 	<span class="shadow"></span>
	   <p><a href="#">
	   <?php if(!empty($get_result)){ ?>
      <?php if(!empty($get_result[20]->option_value)) { ?>
     <?php  echo $get_result[20]->option_value; ?>
      <?php } ?>
      <?php if(!empty($get_result[26]->option_value)) { ?>
     <?php if(!empty($get_result[9]->option_value)) echo $get_result[26]->option_value; ?>
      <?php } ?>
      <?php } ?>
      </a></p>
	 </div>
	 <div class="col-md-4 col-sm-4 column">
	   <div class="imgBlock">
	 	<img src="<?php echo FRONTEND_THEME_URL ?>/images/address.svg" width="60">
	   </div>
	 	<span class="shadow"></span>
	   <p><a href="#"><?php if(!empty($get_result)){if(!empty($get_result[18]->option_value)) echo $get_result[18]->option_value;} ?></a></p>
	 </div>
	</div>
	<hr>

	<div class="getintouch">
	<h4>Get in Touch with us</h4>
	<div class="col-md-7 col-sm-7 col-xs-12 form-section">

	<div class="row">
	  <div class="col-md-12">
	   <div class="form-group clearfix">
	    <div class="col-md-4 col-sm-4 label-block">
		   <label class="label"> Name<font> :</font> </label>
	    </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
	 	 <input type="text" name="name" ng-model="Contactus_detail.name" id="name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Name Required" >
		 <label class="error" ng-bind="Error.name"></label>
	   </div>
	  </div>
	 </div>
	</div>

	<div class="row">
	 <div class="col-md-12">
	  <div class="form-group clearfix">
	   <div class="col-md-4 col-sm-4 label-block">
		<label class="label">Email<font> :</font> </label>
	   </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
		<input type="email" name="email" id="email" ng-model="Contactus_detail.email" class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Email Required">
		 <label class="error" ng-bind="Error.email"></label>
	   </div>
	  </div>
	 </div>
	</div>

	<div class="row">
	 <div class="col-md-12">
	  <div class="form-group clearfix">
	   <div class="col-md-4 col-sm-4 label-block">
		<label class="label">Contact No.<font> :</font> </label>
	   </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
		<input type="text" name="mobile" class="form-control" ng-model="Contactus_detail.mobile" data-bvalidator="required,minlen[10],maxlen[12]" data-bvalidator-msg-equal="Valid number is required" id="mobile">
	 	<label class="error" ng-bind="Error.mobile"></label>
	   </div>
	  </div>
	 </div>
	</div>

	<div class="row">
	 <div class="col-md-12">
	  <div class="form-group clearfix">
	   <div class="col-md-4 col-sm-4 label-block">
		 <label class="label"> City<font> :</font> </label>
	   </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
		 <input type="text" name="city"  ng-model="Contactus_detail.city" class="form-control" data-bvalidator="required" 
		 id="city">
	   <label class="error" ng-bind="Error.city"></label>
	   </div>
	  </div>
	 </div>
	</div>

	<div class="row">
	 <div class="col-md-12">
	  <div class="form-group clearfix">
	   <div class="col-md-4 col-sm-4 label-block">
		<label class="label">Subject<font> :</font> </label>
	   </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
		 <input type="text" name="subject" ng-model="Contactus_detail.subject" class="form-control" 
		 data-bvalidator="required" id="subject">
	    <label class="error" ng-bind="Error.subject"></label>
	   </div>
	  </div>
	 </div>
	</div>

	<div class="row">
	 <div class="col-md-12">
	  <div class="form-group clearfix">
	   <div class="col-md-4 col-sm-4 label-block">
		<label class="label">Message<font> :</font> </label>
	   </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
		<textarea class="form-control" rows="5" name="message" ng-model="Contactus_detail.message" data-bvalidator="required" ></textarea>
		  <label class="error" ng-bind="Error.message"></label>
	   </div>
	  </div>
	 </div>
	</div>	
	<div class="row">
	 <div class="col-md-12">
	  <div class="form-group clearfix">
	   <div class="col-md-4 col-sm-4 label-block">
	   </div>
	   <div class="col-md-8 col-sm-8 signup-full-column">
	     <div class="row">
			<div class="col-md-6">
			  <div class="captchaImg-block">
				<img src="<?php echo generate_captcha()?>" class="captchaImg"/>
				<button type="button" class="btn btn-theme refresh_btn" ng-click="ReloadCaptcha()" tooltip-placement="top" uib-tooltip="Refresh Captcha"><i class="fa fa-refresh"></i></button>
			  </div>
			</div>
			<div class="col-md-6 ">
				<input type="text" name="captcha" ng-model="Contactus_detail.captcha" class="form-control" id="captcha" data-bvalidator="required,maxlen[6]" maxlength="6" size="6" Placeholder="Enter text shown in the image"/>
			</div>
		 </div>
			<label ng-bind="Error.captcha" class="error"></label>
	   </div>
	  </div>
	 </div>
	</div>	
	
	<div class="col-md-4 col-sm-4"></div>
	<div class="col-md-8 col-sm-8 signup-full-column">
	<div class="">
	  <span class>
		 <button type="submit" class="button" >Submit
		 </button>
		</span>
	</div>
	</div>

	<div class="col-md-10 col-md-offset-1">
	  <div ng-if="Message">
		<!-- <div class="alert alert-danger">{{Message}}</div> -->
	  </div>
	</div>
		<div id="search_loader" class="search_loader" style="width: 100%;height:100%!important;display: none;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;top:0px;">
	    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
		</div>
	</div>

	<div class="col-md-5 col-sm-5 col-xs-12">
	<div class="contactContent">
	  <p>
	 	Questions or comments about the YoloMD experience? Is there a service or feature you were hoping to see on the website?  Write us and we would be happy to respond to any of your concerns.
	  </p>
	  <p>
	 	Let us know!
	  </p>
	  <div class="clearfix"></div>
	  <div class="contactSocial">
	  	<ul>
	  		<li>
	  		  <a href="#"><div class="hexagon"><i class="fa fa-facebook" aria-hidden="true"></i></div><span>facebook</span></a>
	  		</li>
	  		<li>
	  		  <a href="#"><div class="hexagon"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></div><span>snapchat</span></a>
	  		</li>
	  		<li>
	  		  <a href="#"><div class="hexagon"><i class="fa fa-instagram" aria-hidden="true"></i></div><span>Instagram</span></a>
	  		</li>
	  	</ul>
	  </div>
	 </div>
	</div>

	<div class="clearfix"></div>

	</div>

	</div>
  </section>

  </div> 
  </form>
  </div>
 
  </div>
 </div>
 </div>
</div>
</div>
<?php 
$this->load->view('include/common_modal_msg');
$this->load->view('include/footer_menu');
?>

</div>