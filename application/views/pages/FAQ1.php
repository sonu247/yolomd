<?php 
  $this->load->view('include/header_menu');
?>
<div ng-cloak>
<div class="theme-bg">
<div class="container">
<div class="formPage-section">
 <div class="col-md-10 col-md-offset-1" >
  <div class="faqBlock-warp">

   <h3 class="heading text-center">FAQ's</h3>
   <section class="faqBlock" >

	<div class="">

	 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading1">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1" class="accordion-toggle">
	          How can i search for a job.
	        </a>
	      </h4>
	    </div>
	    <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
	      <div class="panel-body">
	        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading2">
	      <h4 class="panel-title">
	        <a class="collapsed accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
	         How can i post a job.
	        </a>
	      </h4>
	    </div>
	    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
	      <div class="panel-body">
	        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading3">
	      <h4 class="panel-title">
	        <a class="collapsed accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
	          How to purchase plan for premium listing
	        </a>
	      </h4>
	    </div>
	    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
	      <div class="panel-body">
	        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading4">
	      <h4 class="panel-title">
	        <a class="collapsed accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
	          How to purchase plan for premium listing
	        </a>
	      </h4>
	    </div>
	    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
	      <div class="panel-body">
	        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading5">
	      <h4 class="panel-title">
	        <a class="collapsed accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
	          How to purchase plan for premium listing
	        </a>
	      </h4>
	    </div>
	    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
	      <div class="panel-body">
	        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
	      </div>
	    </div>
	  </div>

	 </div>

	</div>

   </section>

 </div>
 </div>
</div>
</div>
</div>
<?php 
$this->load->view('include/footer_menu');
?>


</div>