<?php 
  $this->load->view('include/header_menu');
    //$this->output->enable_profiler(TRUE);
?>
<div ng-cloak>
<div class="theme-bg">
<div class="container two-col-holder no-padding">
	<!--Login container start here-->
	<div class="container login-container-warp "><br><br>
	<div class="col-md-10 col-lg-10 center-block resetblock-warp">
	<div class="login-page-warp">
<!-- 	 <div class="col-md-7 col-lg-7 login-left-warp">
	 	<h3>Almost There!</h3>
	 	<p>Create a new account or sign in to your existing account, to join our Prep Insider Rewards Program.</p>
	 	<h3>Prep Insider Benefits Include:</h3>
	 	<ul>
	 		<li>Special Promotions</li>
	 		<li>Exclusive Discounts</li>
	 		<li>Insider Info</li>
	 	</ul>
	 	<b>Get started on being rewarded for future purchases!</b>
	 </div> -->
	 <div class="resetblock-holder login-warp login-popup-block ">
	 	<div class="panel-heading text-center text-uppercase">
	 	 <h3>Reset Password</h3>
	 	</div>
	   <div class="col-md-6 col-lg-6 col-sm-6 no-padding-left">
	    <div class="left-block">
	 	 <div class="">
		  <div class="">
		  <!-- <div class="col-md-6 login-logo">
		  	<img src="images/corporate.png" width="60%" class="img-center">
		  </div> -->
		   <div class="">
			<div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left: 0;text-aling:center;">
			    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
			</div>
				<div  data-target="#common_modal_msg" data-toggle="modal">
						
				</div>
				<form method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="SubmitChangePassword()" name="change_password_form">
					<div class="form-section">
					 <div class="themeform">
					
					  <div class="form-group row">
			           <div class="col-md-4 col-sm-4 col-xs-4 text-right block">
			            <label for="exampleInputPassword1" class="label required">New Password <font>:</font></label>
			           </div>
			           <div class="col-md-7 col-sm-7 col-xs-7 block input-tooltip">
			           <input type="password" name="password" ng-model="ChangePassword.password"  class="form-control" autocomplete="off" data-bvalidator="required,minlen[6]" id="password">
			           	<span class="help-tip-btn">
                            <a href="#" class="btn btn-sm btn-circle btn-theme">
                               <i class="fa fa-question" aria-hidden="true"></i>
                            </a>
                            <div class="tooltip top password-tooltip-btn" role="tooltip">
								<div class="tooltip-arrow"></div>
								<div class="tooltip-inner">
									Password requires six characters minimum
								</div>
                            </div>
                        </span>
			           <label class="error" ng-if="Error.password"> {{ Error.password }}</label>
			           </div>
			          </div>

			          <div class="form-group row">
			           <div class="col-md-4 col-sm-4 col-xs-4 text-right block no-padding-left">
			            <label for="exampleInputPassword1"  class="label required">Confirm Password <font>:</font></label>
			           </div>
			           <div class="col-md-7 col-sm-7 col-xs-7 block">
			            <input type="password" name="confirm_password" ng-model="ChangePassword.confirm_password" class="form-control"   data-bvalidator="required,equal[password],minlen[6]" >
			             <label class="error" ng-if="Error.confirm_password"> {{ Error.confirm_password }}</label>
			           </div>
			          </div>
			         <div class="row">
			          <div class="col-md-4 col-sm-4 col-xs-4"></div>
			          <div class="col-md-7 col-sm-7 col-xs-7 block">
			           <button type="submit" class="btn-blue btn-block">Reset Password</button>
			           
			          </div>
			         </div>

			          <div class="clearfix"></div>
			          <div class="col-md-4"></div>
			          <div class="col-md-8 error" ng-if="Message" style="margin-top:2px;">
			          <h4>{{Message}}</h4>
			          </div>
			          <div class="clearfix"></div>
			         </div>
			        </div>
			        </form>
		   </div>		   
		  </div>
		</div>      		
	   </div></div>
	   <div class="col-md-6 col-lg-6 col-sm-6">
		   <div class="right-block">
		    <h4><b>Set a new password:</b></h4>
			<p>Choose a password that you can recognize/remember easily.  
			For security, you will need to enter your new password across all of your devices.</p>
			
			<h4><b>Create new password:</b></h4>
			<p>We'll ask for this password whenever you sign in.</p> 
			<h4><b>Secure password tips:</b></h4>
			<p>Use at least 6 characters.
			Do not use the same password for multiple online accounts for security purpouse.</p>
			
			 <a href="#" class="button text-center" style="width:225px;" class="button" data-toggle="modal" data-target="#signupModal" ng-click="resetVarible()">Login</a>
			</div>
	   </div>
	   <div class="clearfix"></div>
	 </div>
	<div class="clearfix"></div>
     </div>
	 <br>
	 </div><br>
    </div>
	<!--login container end-->
</div>
</div>
<?php 
	$this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>
</div>