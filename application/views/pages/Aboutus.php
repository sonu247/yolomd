<?php
$this->load->view('include/header_menu');
?>
<!-- <div ng-cloak>
	<div class="theme-bg">
		<div class="container">
			<div class="col-md-10 col-md-offset-1" >
				<div class="faqBlock-warp">
					<h3 class="heading text-center">About us</h3>
					<section class="faqBlock static_page">
						<div class="">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default"> -->
									<div  ng-bind-html="CONTENT.content"></div>
<!-- 								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div> -->




<!-- <div class="about-us-wrapper">
	<div class="aboutus-banner">
		<div class="container">
			<div class="overlay">
				<div class="col-sm-7 center-block">
					<div class="about-us-heading text-center"><span>About Us</span></div>
					<p class="about-us-pera text-center">
						YoloMD is a healthcare job search and recruitment website for doctors and medical facilities across the country. Established in 2015 by two young family physicians practicing in the heart of Montreal, YoloMD was designed to facilitate the sometimes daunting task of starting or finding a new career.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="aboutus-healthcare-managers">
		<h3 class="aboutus-heading-bottom">Platform Exclusively For </h3>
		<div class="flex">
			<div class="left-section">
				<div class="healthcare-contain">
					<h3 class="healthcare-heading">Healthcare Managers</h3>
					<p class="healthcare-pera">Weather you are a rural hospital with vacant positions, a busy emergency department looking to fill holes in your scheduling or a successful clinic looking to expand your practice and services; our platform provides you with the opportunity to be seen by thousands of physicians from across the country.</p>
				</div>
			</div>
			<div class="right-section">
				<div class="healthcare-contain">
					<h3 class="healthcare-heading">Doctors</h3>
					<p class="healthcare-pera">Looking for a change in scenery or preparing for your first job? YoloMD is the perfect place to start. From busy city walk-ins to scenic rural practices, hospitals to suburban clinics, our website has something for all tastes and types. See what the entire country has to offer.</p>
				</div>
			</div>
			<div class="aboutus-doctors">
				<img src="http://yolomd.com/assets/front/images/aboutus/about-doctor.png">
			</div>
		</div>
	</div>
	<div class="aboutus-our-service">
		<div class="text-center">
			<h3 class="aboutus-heading-bottom">Our Services</h3>
		</div>
		<div class="aboutus-service-wrp">
			<div class="service-left-section">
				<div class="bottom-left-img width-50">					
				</div>
				<div class="bottom-right-contain width-50">
					<h3>For Doctors</h3>
					<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<ul>
						<li>100% FREE to Sign-Up and Search for Jobs</li>
						<li>Save clinics & hospitals to your favourites list</li>
						<li>1-Click contact of facilities right from their ad</li>
						<li>Easily search for jobs on your computer, tablet or phone</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="service-right-section">			
				<div class="bottom-right-img width-50"></div>
				<div class="bottom-left-contain width-50">
					<h3>For Hospitals & Clinics</h3>
					<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<ul>
						<li>100% FREE to Sign-Up and Post a Job</li>
						<li>Entice future candidates with beautiful photos of your facilities</li>
						<li>Doctors can easily e-mail you from your ad</li>
						<li>Easily Post a Job from your computer, tablet or phone</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div> -->






<?php
$this->load->view('include/footer_menu');
?>
</div>