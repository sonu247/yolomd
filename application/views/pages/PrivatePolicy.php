<?php 
  $this->load->view('include/header_menu');
?>
<div ng-cloak>
<div class="theme-bg">
<div class="container">
<div class="">
 <div class="col-md-10 col-md-offset-1" >
  <div class="faqBlock-warp">

   <h3 class="heading text-center">Privacy policy</h3>
   <section class="faqBlock static_page">

	<div class="">

	 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default">
	   
	    <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1" ng-if="CONTENT.content">
	      <div class="" ng-bind-html="CONTENT.content">
	        
	      </div>
	    </div>
	  </div>
	 </div>

	</div>

   </section>

 </div>
 </div>
</div>
</div>
</div>
<?php 
$this->load->view('include/footer_menu');
?>


</div>