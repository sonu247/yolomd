
<?php 
  $this->load->view('include/header_menu');
?>

<div ng-cloak>
<div class="theme-bg">
<div class="container">
<div class="">
 <div class="col-md-10 col-md-offset-1" >
  <div class="faqBlock-warp">
   <h3 class="heading text-center">FAQ's</h3>
   <section class="faqBlock" >

	<div class="">

	 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default" ng-repeat="fq in FAQData">
	    <div class="panel-heading" role="tab" id="heading1" ng-if="fq.question">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse{{fq.id}}" aria-expanded="true" aria-controls="collapse1" class="accordion-toggle">
	         {{fq.question}}
	        </a>
	      </h4>
	    </div>
	    <div id="collapse{{fq.id}}" ng-class="{'panel-collapse collapse in' : ($index == '0'),'panel-collapse collapse' : ($index != '0')}"  role="tabpanel" aria-labelledby="heading1" ng-if="fq.answer">
	      <div class="panel-body">
	       {{fq.answer}}
	      </div>
	    </div>
	  </div>
	 </div>
	 <div class="clearfix"></div>
	</div>

   </section>

 </div>
 </div>
</div>
</div>
</div>
<?php 
$this->load->view('include/footer_menu');
?>


</div>