<?php
$this->load->view('include/header_menu');
?>
<div ng-cloak>
  <div class="" id="jobs">
    <div class="formPage-section">
      <!-- <div class="container"> -->
      <div class="col-md-7 col-sm-7 full-height left-side job-list-left-side
        "  id='main_content' style="background-image:none;background-color:#ffffff;">
        <div ng-include="'template/job/detail'" ng-if="showDetails"></div>
        <div class="listView-warp" ng-if="!showDetails">
          <div  data-target="#common_modal_msg" data-toggle="modal"></div>
          <div class="listView-warp" >
            <div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left: 0;">
              <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
            <div class="list-filter-block">
              <div class="filtter_btn_section">
                <a ng-click="toggle_filters()"><i class="fa fa-filter" aria-hidden="true"></i> {{toggle_text}} <i class="{{toggle_fa}}" aria-hidden="true"></i></a>
              </div>
              <div ng-if="checkdisable" class="searchPanel {{filter_toggle}}">
                <div class="col-md-3 col-sm-6 col-xs-6 medium-width">
                  <div class="form-group">
                    <span class="heading">Medical Specialty : </span>
                    <select name="medical_specialty" ng-model="search_data.medical_specialty" class="form-control" ng-change="searchResult('1')">
                      <option value="">All Specialties </option>
                      <option ng-repeat ="medical_s in medical_specialty track by $index" value="{{medical_s.id}}">{{medical_s.name}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 medium-width">
                  <div class="form-group">
                    <span class="heading">Province : </span>
                    <select name="canada_province" ng-model="search_data.canada_province" class="form-control" ng-change="searchResult('1')">
                      <option value="All">All Provinces </option>
                      <option ng-repeat ="canada_pro in canada_province track by $index"  value="{{canada_pro.province_name}}">{{canada_pro.province_name}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 medium-width city-filter">
                  <div class="form-group">
                    <span class="heading">City : </span>
                    <!-- <input type="text" class="form-control" placeholder="Any" ng-model="search_data.citypostalcode" ng-model-options='{ debounce: 1000 }' ng-change="searchResultCity()"/> -->
                    <select ui-select2 name="citypostalcode" ng-model="search_data.citypostalcode" class="form-control" ng-change="searchResultCity()" id="Citypostalcode">
                      <option value="">All Cities </option>
                      <option ng-repeat ="canada_postal in citypostalcode track by $index" ng-if="canada_postal.facility_city != ''"  value="{{canada_postal.facility_city}}">{{canada_postal.facility_city}}</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 medium-width ">
                  <div class="form-group last">
                    <span class="heading">Job Type : </span>
                    <select class="form-control" ng-model="search_data.Position" ng-change="searchResult()">
                      <option value="Any">Any</option>
                      <option value="Permanent">Permanent</option>
                      <option value="Locum">Locum</option>
                    </select>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
              <div class="">
                <div class="sortBlock">
                  <div class="pull-left" ng-if="job_count != '0' && job_count != ''">
                    <!--<span>Sort By :</span>&nbsp;&nbsp;
                    <span>
                      <select class="sort-select" style="cursor:pointer!important;" ng-model="search_data.sorting" ng-change="searchResult()">
                          <option value="1">Premium Access</option>
                        <option value="2">YoloMD Verified</option>
                      </select>
                    </span>-->
                    <span>Total Jobs :</span>&nbsp;&nbsp;
                    <span><b>{{job_count}}</b>
                    </span>
                  </div>
                  
                  <div class="pull-right">
                    <!-- <a ng-click="cleardata()"  ng-if="search_data.canada_province != 'All' || search_data.medical_specialty != '' || search_data.Position != 'Any' || search_data.citypostalcode != ''" class="btn btn-danger">Reset</a> -->
                    <a ng-click="SaveSearch(check_user_login_value)" data-target="#signupModal"  ng-if="check_user_login_value == '3'" data-toggle="modal" class="btn btn-danger">Save Search</a>
                    <a ng-click="SaveSearch(check_user_login_value)" type="submit" ng-if="check_user_login_value == '2'" class="btn btn-danger">Save Search</a>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <!--listing-->
            <div class="listView">
              <div class="clearfix"></div>
              <div class="listBlockWarp row" ng-if="jobs" >
                <div class="col-md-6 col-sm-6 col-xs-6"  ng-repeat="job in jobs">
                  <div class="listBlock" ng-mouseover="changemarkericon($index)" ng-mouseout="reversemarkericon($index,job.icon)" >
                    <div class="favourite" tooltip-placement="left" uib-tooltip="Favourite" ng-if="check_user_login_value == '3'"><a href="#"  data-target="#signupModal" data-toggle="modal" ng-click="favorite_mark(job.job_id,check_user_login_value,$index,'joblist')">Like</a></div>
                    
                    <div ng-if="check_user_login_value == '2' && job.row_favourite == ''" class="favourite" tooltip-placement="left" uib-tooltip="Favourite">
                      <a href="#" ng-click="favorite_mark(job.job_id,check_user_login_value,$index,'joblist')" >Like</a></div>
                      <div class="favourite active" tooltip-placement="left" uib-tooltip="Mark as Unfavourite" ng-if="job.row_favourite !='' && check_user_login_value == '2' "><a href="#" ng-click="unfavorite_mark(job.job_id,check_user_login_value,$index,'joblist')"></a></div>
                      <div class="listBlockSlider">
                        <slick class="slide" current-index="index" responsive="breakpoints" infinite=true speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload=true data="job.jobs_image">
                        <div  ng-repeat="uploadImage in job.jobs_image  track by $index" ng-if="job.jobs_image">
                          <div ng-if="uploadImage" class="slide" style="background-image:url('{{uploadImage}}')">
                            <img  src="{{uploadImage}}" class="img-responsive">
                          </div>
                        </div>
                        <div  ng-if="job.jobs_image == ''">
                          <div class="slide" style="background-image:url('assets/front/images/default-list-thumb.jpg')">
                          </div>
                        </div>
                        </slick>
                      </div>
                      
                      <a href="" ng-click="saveLocaldata(job.encrpt_id)">
                        <div class="info">
                          <!--  <div class="verified-tag" ng-if="job.job_verify==1">YoloMd Verified</div> -->
                          <div class="info-block">
                            <div class="info-text">
                              <h4  ng-if="job.listing_title!=''">{{job.listing_title}}</h4>
                              <div class="location" ng-if="job.facility_state!=''"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i> &nbsp;{{job.facility_city}}, {{job.facility_state}} </div>
                            </div>
                          </div>
                        </div>
                      </a>
                      <span class="message">
                        <div style="position:relative;">
                          <span  data-toggle="modal" ng-if="check_user_login_value == '3'" data-target="#signupModal" data-toggle="modal" tooltip-placement="top" uib-tooltip="Contact" ng-click="setjobID(job.job_id,check_user_login_value,'insert_data',job.listing_title)">
                            <i class="fa fa-envelope-icon" aria-hidden="true"></i>
                          </span>
                          <span  data-toggle="modal" ng-if="check_user_login_value == '2'" data-target="#contactmessage" tooltip-placement="top" uib-tooltip="Contact"  ng-click="setjobID(job.job_id,check_user_login_value,'insert_data',job.listing_title)">
                            <i class="fa fa-envelope-icon" aria-hidden="true"></i>
                          </span>
                        </div>
                      </span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div ng-if="is_load_more" ng-click="load_more();" class="clearfix loadMorebtn">
                    <i class="fa fa-refresh" aria-hidden="true"></i> Load More
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-12" ng-if="jobs==0">
                  <div class="noJobFound">
                    <h3 class="text-center">
                    No Jobs Found
                    </h3>
                    <p>We don't have any Jobs related to this location.</p>
                    <img src="<?php echo FRONTEND_THEME_URL ?>images/no-results.png">
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
          <div class="view-map-btn hidden-md hidden-sm visible-xs">
            <a href="#" class="view-map" ng-click="toggeleclass(0)" ><span>{{map_listing}}</span>
            <img src="<?php echo FRONTEND_THEME_URL ?>images/map-view.svg"></a>
          </div>
        <div class="col-md-5 col-sm-5 map-preview" ng-class="toggleclassfunctioning">
          <div id="map_canvas" class="map-preview map-preview-canvas" >
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
  <script>
    new WOW().init();
  </script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/markerclusterer.js"></script>
  <script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/searchMap.js"></script>
  <style type="text/css">
    body{overflow: hidden;}
    .left-side{padding-bottom: 0px;}
  </style>
  <?php $this->load->view('include/common_modal_msg'); ?>
  <!-- Message Modal -->
  <div class="modal fade custom-modal" id="contactmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog login-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div id="message_loader" style="width: 100%; height: 100%; display: none; background-color: rgba(255, 255, 255, 0.9); position: absolute; z-index: 100; left: 0;">
          <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
        </div>
        <div class="modal-body">
          <div class="themeform">
            <section class="">
              <div class="col-md-12 ">
                <div class="login-warp jobList-conatctmodal">
                  <h3 class="heading text-center">Contact Facility
                  </h3>
<!--                   <h4 class="list-title-name text-center"><b>{{listing_name}}</b>
                  </h4> -->
                  <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="submit_message()" >
                    <div ng-if="contact.msgSend==0">
                      
                      
                      <div ng-if="contact.datainsert==0">
                        <div class="form-group row">
                          <div class="col-md-12 center-block" style="float:none;">
                            <select name="contact_subject" ng-model="contact.contact_subject" class="form-control"  data-bvalidator="required" data-bvalidator-msg="Contact Subject Required">
                              <option value="">Select Contact Subject</option>
                              <option>Interested to meet</option>
                              <option>Request for more information</option>
                              <option>Other </option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-12 center-block" style="float:none;">
                            <textarea class="form-control" rows="8" data-bvalidator="required" data-bvalidator-msg="Contact Facility detail is Required" ng-model="contact.default_msg">{{contact.default_msg}}</textarea>
                          </div>
                        </div>
                        <div class="col-md-12 center-block text-center" style="float:none;">
                          <button type="submit" class="btn btn-theme">Send Message</button>
                        </div>
                      </div>
                      
                    </div>
                    <div ng-if="error_msg" class="error" style="margin-left:40px;">
                      {{error_msg}}
                    </div>
                  </form>
                  <div ng-if="contact.msgSend==1" class="text-center">
                    <div class="info-icon">
                      <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                    </div>
                    <br>
                    <h4>Oops! you have already sent a message to this Job. </h4><br>
                    <a target="_new" class="button" href="physician-message">CLICK HERE TO VIEW THIS JOB SENT MESSAGES</a>
                  </div>
                  <div ng-if="contact.msgSend==2">
                    <div class="info-icon">
                      <span><i class="fa fa-check" aria-hidden="true"></i></span>
                    </div>
                    <br>
                    <h4 class="text-center">Your message has been sent successfully. </h4><br>
                    <div class="text-center">
                      <a target="_new" class="button" href="physician-message">Click here to view recent sent message</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
  @media (min-width: 800px){
  body.modal-open {
  padding-right: 0px !important;
  overflow-y: hidden;
  }
  }
  </style>
  <script type="text/javascript">
    $(window).bind('orientationchange', function(event) {
      if(this.innerWidth>=800){
        $("#map_canvas").parent("div").css('display', 'block');
        $("#map_canvas").parent("div").addClass('map-preview-overflow');
      }
      else{
        $("#map_canvas").parent("div").removeClass('map-preview-overflow');
        var controller = document.getElementById('jobs');
        if(controller!=null){
          angular.element(controller).scope().toggeleclass(1);
        }
      }
      //alert('new orientation:' + event.orientation);
    });
    if( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i)){
      $('[uib-tooltip]').each(function(index, el) {
        $(this).removeAttr('uib-tooltip');
      });
    }
  </script>
  