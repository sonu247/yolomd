<?php 
  $this->load->view('include/header_menu');
?>

<meta property="og:title" content="{{meta_title}}" name='title'/>
<meta property="og:url" content="{{meta_url}}" />
<meta property="og:image" content="{{meta_image}}" />
<meta property="og:description" content="{{meta_description}}" name='description'/> 
<div class="two-column-warp" >
<div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
</div>
<div class="formPage-section">
 <div class="col-md-7 col-sm-7 full-height left-side" >
    <section >
 <div class="form-section-block">
	<div class="col-md-12 center-block no-padding">
	  <div class="jobPreview jobPostpagePreview row">
			<div class="row">
			<div class="postJobSliderWarp">
			<slick class="slider single-item postJobSlider" current-index="index" responsive="breakpoints" dots=true infinite=true speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload=true data="jobs_image" ng-if="jobs_image">
		   <div  ng-repeat="uploadImage in jobs_image" >
		   <div class="jobPostSlide" style="background-image:url('<?php echo base_url('assets/uploads/jobs/') ?>/{{uploadImage.imageName}}')">
		   </div>
		   </div>
		</slick>

		<slick class="slider single-item postJobSlider" current-index="index" responsive="breakpoints" dots=true infinite=true speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload=true data="jobs_image" ng-if="!jobs_image">
		   <div class="jobPostSlide jobPostSlideNopreview" style="background-image:url('<?php echo base_url('assets/front/images/default-list-thumb.jpg') ?>')">
		   </div>

		</slick>
		<a  title="Back" href="" ng-click="checkback()" >
	    <span class="back-btn"><i class="fa fa-angle-left" aria-hidden="true"></i></span></a>
		<div class="postJobSlide-favorite active" ng-if="marked_favourite == '1' && check_user_login_val == '2'">
		  <a href="#" tooltip-placement="left" uib-tooltip="Unfavourite"  ng-click="unfavorite_mark(jobDetail.id,check_user_login_val,$index,'detail')">Like</a>
		  
		</div>
		
		<div class="postJobSlide-favorite"  ng-if="check_user_login_val == '2' && marked_favourite == ''">
		  <a href="#" tooltip-placement="left" uib-tooltip="Favourite" ng-click="favorite_mark(jobDetail.id,check_user_login_val,$index,'detail')">Like</a>
		</div>
		<div class="postJobSlide-favorite"   ng-if="check_user_login_val == '3'">
		  <a href="#"  data-target="#signupModal" data-toggle="modal" ng-click="favorite_mark(jobDetail.id,check_user_login_val,$index,'detail')">Like</a>
		</div>
	    	<div class="JobSliderelement">
	    	 <span data-toggle="modal" ng-if="check_user_login_value == '3'" data-target="#signupModal" tooltip-placement="top" uib-tooltip="Contact" class="message" ng-click="setjobID(job.job_id,check_user_login_val,'not_insert_data',jobDetail.listing_title)">
	    	  <i class="fa-envelope-icon" aria-hidden="true"></i>
	    	 </span>
	    	  <span data-toggle="modal" ng-if="check_user_login_value == '2'" data-target="#contactmessage" tooltip-placement="top" uib-tooltip="Contact" class="message"  ng-click="setjobID(jobDetail.id,check_user_login_val,'not_insert_data',jobDetail.listing_title)">
	    	  <i class="fa-envelope-icon" aria-hidden="true"></i>
	    	 </span>
	    	</div>

			</div>
			</div>

			<div class="row title">
			 <div class="col-md-12 " ng-if="jobDetail.listing_title">
			 	<div class="name">{{jobDetail.listing_title}}   			 	
			 	</div>
			 </div>
			 <div class="col-md-12 jobSpeciality" >
			 	<div class="city" ng-if="jobDetail.facility_city">
			 	 <div class="jobSpeciality-tool">
			 	 <span class="tb-cell"><img src="<?php echo FRONTEND_THEME_URL ?>/images/map-marker.svg" width="18"></span>
			 	 <span class="tb-cell">{{jobDetail.facility_city}},  {{ProvinceName}}</span>
			 	 </div>
			 	</div>
			 	<div class="pull-right Speciality-text" ng-if="MedicalSpecialtyValue">
			 	 <div class="jobSpeciality-tool jobSpeciality-tool-icon">
			 	  <span class="tb-cell"><img src="{{MedicalSpecLogo}}" width="30"> </span>
			 	  <span class="tb-cell">{{MedicalSpecialtyValue}} </span>
			 	 </div>
			 	</div>
			 	<div class="clearfix"></div>
			 </div>
			</div>

			<div class="col-group row" ng-if="jobDetail.listing_description">
				<div class="col-md-12 jobDetailBlock">
				<div class="more">
					{{jobDetail.listing_description}}
				</div>
				</div>
			</div>


			<div class="col-group row" ng-if="FacilityType != null || jobDetail.parking != null || jobDetail.emr != null || jobDetail.no_of_fulltime != null || jobDetail.part_time_doctor != null || jobDetail.exam_room != null">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Facility Details </b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock">

				<div class="deatailElement">
				<ul class="row">
			<li class="col-md-6" ng-if="FacilityType">
			 <div class="block">
			  <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/medical-kit.svg"> Type :</label> <span data-toggle="tooltip" data-placement="top" title="Type"><b>{{FacilityType}}</b></span>
			 </div>
			</li>
			<li class="col-md-6" ng-if="jobDetail.parking">
				<div class="block">
				<label><img src="<?php echo FRONTEND_THEME_URL ?>/images/parking.svg"> Parking :</label> <span data-toggle="tooltip" data-placement="top" title="Parking">{{jobDetail.parking}}</span>
				</div>
			</li>
			</ul>
			</div>

			<div class="deatailElement">
			<ul class="row">
			<li class="col-md-6" ng-if="HoursValue">
				<div class="block">
				 <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/watch.svg">  Hours :</label>
				 <span>{{HoursValue}}</span>
				</div>
			</li>

			<li class="col-md-6">
				<div class="block">
				<label><img src="<?php echo FRONTEND_THEME_URL ?>/images/position.svg">  Job Type :</label>
				<span>{{jobDetail.position}}</span>
				</div>
			</li>

			<li class="col-md-6"  ng-if="StartDateValue">
				<div class="block">
				<label><img src="<?php echo FRONTEND_THEME_URL ?>/images/calendar.svg">  Start Date :</label>
				<span>
				{{StartDateValue}}
				</span>
				</div>
			</li>
			<li class="col-md-6" ng-if="jobDetail.emr">
			<div class="block">
			<label><img src="<?php echo FRONTEND_THEME_URL ?>/images/emr.svg" > EMR :</label>
			<span data-toggle="tooltip" data-placement="top" title="EMR">{{jobDetail.emr}}</span>
			</div>
			</li>
			</ul>			
			</div>

			<div class="deatailElement">
			<ul class="row">

			<li class="col-md-12" ng-if="jobDetail.no_of_fulltime != null ||jobDetail.part_time_doctor != null || jobDetail.exam_room != null">
			<div class="block requirment-detail-block">
			<label><img src="<?php echo FRONTEND_THEME_URL ?>/images/group.svg" > </label>&nbsp;
			 <span class="full-time">
			 <b>{{jobDetail.no_of_fulltime}}</b> <label>Full-time doctors</label>
			 </span> &nbsp;
			<span class="part-time">
			<b>{{jobDetail.part_time_doctor}}</b> <label>Part-time doctors</label>
			</span>&nbsp;
			<span class="exam-rooms"><b>{{jobDetail.exam_room}}</b> <label>Exam rooms</label></span>
			</div>
			</li>
			<!-- <li class="col-md-12" ng-if="NursingArray">
			 <div class="block blockTable nursing">
				<span class="cell"><label>Nursing :</label></span>
				<span ng-bind-html="NursingArray" class="nursingTagBlock cell">
			 </span>
			 </div>
			</li> -->
			</ul>				
			</div>
			</div>
			</div>
			<div class="col-group row" ng-if="jobDetail.call_responsibility">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Call Responsibilities</b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock">
				 {{jobDetail.call_responsibility}}
				</div>
			</div> 	
			<div class="col-group row" ng-if="CompensationValue">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Compensation</b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock" ng-if="CompensationValue">
				 {{CompensationValue}}
				</div>
			</div> 	    	
		<!-- 	<div class="col-group row" ng-if="HealthProfessional">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Allied Health Professionals </b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock healthProfessional" ng-bind-html="HealthProfessional">
				</div>
			</div> -->
	<!--Public Transport Group-->
			<div class="col-group row" ng-if="jobDetail.transport_metro_subway != null || jobDetail.transport_m_s_value != null || jobDetail.transport_bus != null || jobDetail.transport_bus_value != null || jobDetail.transport_other != null || jobDetail.transport_other_value != null || jobDetail.transport_train_value != null || jobDetail.transport_train != null">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Public Transport </b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock">
				<div class="public-transport">
				 <div class="block row">
				<div ng-if="jobDetail.transport_metro_subway" class="transportName">
				<img src="<?php echo FRONTEND_THEME_URL ?>/images/metro.svg"> Metro/Subway
				</div>
				<div ng-if="jobDetail.transport_m_s_value != '' && jobDetail.transport_m_s_value != null " class="transportDetail">
				({{jobDetail.transport_m_s_value}})
				</div>
			  </div>
			 <div class="clearfix"></div>
			 <div class="block row">
				<div ng-if="jobDetail.transport_bus" class="transportName">
				<img src="<?php echo FRONTEND_THEME_URL ?>/images/bus.svg"> Bus
				</div>
				<div ng-if="jobDetail.transport_bus_value != '' && jobDetail.transport_bus_value != null" class="transportDetail">
				({{jobDetail.transport_bus_value}})
				</div>
			</div>
			 <div class="clearfix"></div>
			 <div class="block row">
					<div ng-if="jobDetail.transport_train" class="transportName">
					<img src="<?php echo FRONTEND_THEME_URL ?>/images/train.svg"> Train
					</div>
					<div ng-if="jobDetail.transport_train_value != '' && jobDetail.transport_train_value != null" class="transportDetail">
					({{jobDetail.transport_train_value}})
					</div>
				</div>
			<div class="clearfix"></div>
			 <div class="block row">
				<div ng-if="jobDetail.transport_other" class="transportName">
				<img src="<?php echo FRONTEND_THEME_URL ?>/images/transport-other.svg"> Other
				</div>
				<div ng-if="jobDetail.transport_other_value != '' && jobDetail.transport_other_value != null" class="transportDetail">
				({{jobDetail.transport_other_value}})
				</div>
			</div>
				</div>
				</div>
			</div>


			<!--Facility Hours Group-->
			<div class="col-group row" ng-if="FacilityDaysName != null || FacilityHoursAll != null">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Facility Hours </b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock">
				 <div class="row">
				  <div class="col-md-12">
				 	<table class="table table-condensed">
				 	 <tr ng-repeat="facility_days_name in FacilityDaysName"  ng-init="parentIndex = $index">
				 	 	<td >
				 	 	<div class="FacilityDaysName">
				 	 	{{facility_days_name}}
				 	 	</div>
				 	 	<k ng-repeat="(time_by_date_key,time_by_date_value) in FacilityHoursAll"  ng-init="parentIndex1 = $index">
				 	 	<div  class="FacilityDate">
		 	 		 <div ng-repeat="facility_full_name  in FacilityHoursAll[time_by_date_key] track by $index" ng-if="facility_days_name == time_by_date_key" >
		 	 		
		 	 	{{facility_full_name.start_time}}  - {{facility_full_name.end_time}}
		 	 	<span ng-if="FacilityHoursAll[time_by_date_key].length != 1 && FacilityHoursAll[time_by_date_key].length != ($index+1)">,</span>
		 	 		  </div>
				 	 	</div>
				 	 	</k>
				 	 	</td>
				 	 </tr>
				 	</table>
				 </div>
				 </div>
				</div>
			</div>
		<!--Special Qualification Group-->
			<div class="col-group row" ng-if="jobDetail.special_qualification">
				<div class="col-md-3 col-xs-4 label-name">
					<b>Special Qualifications</b>
				</div>
				<div class="col-md-9 col-xs-8 jobDetailBlock text-justify">
				 {{jobDetail.special_qualification}}
				</div>
			</div> 	
			<br>
			<div class="col-md-3"></div>
			<div class="col-md-9">


			  <div class="pull-left social-group">
			  	<ul>
			  	 <li>

 					<?php
                    $site_url = site_url().'jobs/{{jobDetail.id}}';
                    $image_url = base_url()."assets/uploads/jobs/{{jobs_image[0]['imageName']}}";
                    $title='{{jobDetail.listing_title}}';
                    $url=urlencode($site_url);
                    $summary='{{jobDetail.listing_description}}';
                    $image=$image_url;
                ?>
               
                <a ng-click="facebookbtn(jobDetail.encrpt_id,jobs_image[0]['imageName'],jobDetail.listing_title,jobDetail.listing_description)"><i class="fa fa-facebook" aria-hidden="true"></i></a>


			  	 </li>
			  	 <li><a ng-click="googleplusbtn(jobDetail.encrpt_id)" title="share on google+"><i class="fa fa-google-plus" aria-hidden="true" ></i></a></li>
			  	 <li><a ng-click="linkedinbtn(jobDetail.encrpt_id,jobDetail.listing_title,jobDetail.listing_description)"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			  	</ul>

			  	<div class="clearfix"></div>


			  	<div class="share-on">Share on Social Media</div>
			  </div>
			  <div class="pull-right" >
	   			 <a href="#" ng-click="setjobID(job.job_id,check_user_login_val,'not_insert_data',jobDetail.listing_title)" data-toggle="modal" ng-if="check_user_login_value == '3'" data-target="#signupModal" tooltip-placement="top" uib-tooltip="Click here to contact" class="btn detailContact-btn btn-blue"><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp;Contact Facility</a> 
	   			  <a href="#" data-toggle="modal" ng-if="check_user_login_value == '2'" data-target="#contactmessage" tooltip-placement="top" uib-tooltip="Click here to contact" ng-click="setjobID(jobDetail.id,check_user_login_val,'not_insert_data',jobDetail.listing_title)" class="btn detailContact-btn btn-blue"><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp;Contact Facility</a>

	   		  </div>
			 
			</div>
		</div>
		</div>
	<div class="clearfix"></div>
	<br><br>
   </div>
</section>
 </div>
   <div class="view-map-btn hidden-md hidden-sm visible-xs">
    <a href="#" class="view-map" ng-click="toggeleclass()" >{{map_listing}}
      <img src="<?php echo FRONTEND_THEME_URL ?>images/map-view.svg"></a>
  </div>
  <div class="col-md-5 col-sm-5 map-preview" ng-class="toggleclassfunctioning">
	 <div id="map_canvas" class="map-preview">
 	 </div>
  </div>
 <div class="clearfix"></div>
</div>
</div>

<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
<script>
	new WOW().init();
</script>
<script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/markerclusterer.js"></script> 
<script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/searchMap.js"></script> 

<style type="text/css">
	body{overflow: hidden;}
	.left-side{padding-bottom: 0px;}
</style>

<?php $this->load->view('include/common_modal_msg'); ?>

<!-- Message Modal -->
<div class="modal fade custom-modal" id="contactmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
       <div id="message_loader" style="width: 100%; height: 100%; display: none; background-color: rgba(255, 255, 255, 0.9); position: absolute; z-index: 100; left: 0;">
        <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
       </div>

      <div class="modal-body">
       <div class="themeform">
        <section class="">
        <div class="col-md-12 ">
        <div class="login-warp jobList-conatctmodal">
        <h3 class="heading text-center">Contact Facility
        </h3>
<!--         <h4 class="list-title-name text-center"><b>{{listing_name}}</b>
        </h4> -->
         <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="submit_message()" >
        <div ng-if="contact.msgSend==0">
       
  
         <div ng-if="contact.datainsert==0">
          <div class="form-group row">
           <div class="col-md-12 center-block" style="float:none;">
            <select name="contact_subject" ng-model="contact.contact_subject" class="form-control"  data-bvalidator="required" data-bvalidator-msg="Contact Subject Required">
              <option value="">Select Contact Subject</option>
              <option>Interested to meet</option>
              <option>Request for more information</option>
              <option>Other </option>
            </select>
            </div>
          </div>
          <div class="form-group row">
           <div class="col-md-12 center-block" style="float:none;">
            <textarea class="form-control" rows="8" data-bvalidator="required" data-bvalidator-msg="Contact Facility detail is Required" ng-model="contact.default_msg">{{contact.default_msg}}</textarea>
          </div>
          </div>
          <div class="col-md-12 center-block text-center" style="float:none;">
           <button type="submit" class="btn btn-theme">Send Message</button>
          </div>
         </div>
        
        </div>
        <div ng-if="error_msg" class="error" style="margin-left:40px;">
        {{error_msg}}
        </div>
        </form>
        <div ng-if="contact.msgSend==1" class="text-center">
          <div class="info-icon">
          <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
          </div>
          <br>
          <h4>Oops! you have already sent a message to this Job. </h4><br>
          <a target="_new" class="button" href="physician-message">CLICK HERE TO VIEW THIS JOB SENT MESSAGES</a>
        </div>

        <div ng-if="contact.msgSend==2">
         <div class="info-icon">
          <span><i class="fa fa-check" aria-hidden="true"></i></span>
         </div>
          <br>
          <h4 class="text-center">Your message has been sent successfully. </h4><br>
          <a target="_new" class="button" href="physician-message">Click here to view recent sent message</a>
         </div>

        </div>
        </div>
        <div class="clearfix"></div>
        </section>
       </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
@media (min-width: 800px){

  body.modal-open {
      padding-right: 0px !important;
      overflow-y: hidden;
  }
}
</style>
<script type="text/javascript">
$(window).bind('orientationchange', function(event) {
  if(this.innerWidth>=800){
    $("#map_canvas").parent("div").css('display', 'block');
    $("#map_canvas").parent("div").addClass('map-preview-overflow');
  }
  else{
    $("#map_canvas").parent("div").removeClass('map-preview-overflow');
    var controller = document.getElementById('jobs');
    if(controller!=null){
      angular.element(controller).scope().toggeleclass(1);
    }
  }
});

if( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i)){
	$('[uib-tooltip]').each(function(index, el) {
		$(this).removeAttr('uib-tooltip');
	});
}
</script>