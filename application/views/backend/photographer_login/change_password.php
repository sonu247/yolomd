<div class="bread_parent">
<ul class="breadcrumb">
  <!--   <li><a href="<?php //echo base_url('backend/photographer_login/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li> -->
    <li><i class="fa fa-cog"></i> Change Password</li>
           
</ul>
</div>

<div class="row">
     <div class="col-lg-14">
        <section class="panel">
            <header class="panel-heading heading_class"><i class="fa fa-cog"></i> Change Password</header>
  <form role="form" method="post" class="form-horizontal tasi-form" action="<?php echo current_url()?>">
        <div class="panel">


      <div class="form-group">
        <label class="col-sm-2 col-sm-2">Old Password</label>
         <div class="col-sm-10">
        <input type="password" placeholder="Old Password" class="form-control" name="oldpassword" value="">
         <div style="float:left">
        <?php echo form_error('oldpassword'); ?>
        </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 col-sm-2">New Password</label>
         <div class="col-sm-10">
        <input type="password" placeholder="New Password" class="form-control" name="newpassword" value="">
         <div style="float:left">
        <?php echo form_error('newpassword'); ?>
        </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 col-sm-2">Confirm Password</label>
         <div class="col-sm-10">
          <input type="password" placeholder="Confirm Password" class="form-control" name="confpassword" value="">
           <div style="float:left">
          <?php echo form_error('confpassword'); ?>
          </div>
          </div>
      </div>
     
    <div class="form-group">
     <label class="col-sm-2 col-sm-2"></label>
     <div class="col-sm-10">
      <button class="btn btn-primary" type="submit">Change Password</button>
      </div>
    </div>    

  </form>
  </div>
</section>
  </div>
  </div>
