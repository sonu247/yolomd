<div class="">
<ul class="breadcrumb">
<!-- <li><a href="<?php echo base_url('backend/photographer_login/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li> -->
<li class=""><a href="<?php echo base_url('backend/photographer_jobs');?>">Photographer Jobs</a></li>
<li>Photographer Job Edit</li> 
<div class="pull-right">  
 <?php 
                        $checked_mark = '';
                        $checked_mark = check_marked_complete($jobs->id);
                        if($checked_mark)
                        {
                        ?>
                        <span title="Request already sent of   Marked Completed" class="btn btn-success btn-xs tooltips" data-toggle="tooltip">
                    Request already sent of   Marked Completed            </span>
                          <?php }else{ ?>
                             <span title="Send request for Marked Complete" data-toggle="tooltip"><a  class="btn btn-info btn-xs tooltips" href="<?php echo site_url();?>backend/photographer_jobs/marked_complete/<?php if(!empty($jobs->id)) echo $jobs->id; ?>"  title="Send request for Marked Completed">Send request for Marked Completed
                              </a>
                          </span>
                           
                            <?php } ?>
                            </div>
</ul>
</div>
<section>
<div class="row">
<div class="col-lg-12">

<header class="panel-heading">Photographer Job Edit</header>    
  <!-- form start -->
  <form role="form" method="post" action=""  enctype="multipart/form-data">
   
  <input type="hidden" name="job_id" value="<?php  if(!empty($jobs->id)) echo $jobs->id; ?>"/>
  <div class="">
  <table class="responsive table table-striped">
      <tr>
      <td width="300"> Title : </td>
      <td>   <?php if(!empty($jobs->listing_title)){ echo $jobs->listing_title;}  ?> </td>
      </tr>
      <tr>
      <td> Facility Name :  </td>
       <td> <?php if(!empty($jobs->facility_name)){echo $jobs->facility_name;}  ?> 
       </td>
       </tr>
       <tr>
       <td>
        Facility Address : </td> 
        <td>   <?php if(!empty($jobs->facility_address)){ echo  $jobs->facility_address;}  ?></td>
        </tr>
        <tr>
        <td>City: </td>
        <td> 
          <?php if(!empty($jobs->facility_city)){ echo  $jobs->facility_city;}  ?> </td>
          </tr>
         <tr><td>State : </td>
         <td> 
                     <?php if(!empty($jobs->facility_state)){ echo $jobs->facility_state;}  ?>   
                     </td>
    </table>
  </div>
  <div class="">
  <div class="box-body" style="margin-top:20px;">
   <div style="margin-left:10px;">Upload : </div>
       <div class="">
        <div class="">

        <?php
      $i = 1;
      $count_img = 0;
      $total_loop = 5;
      $final_loop = '';
      $up_arr = '';
      if(!empty($jobs->uploadImageArray))
      $up_arr=unserialize($jobs->uploadImageArray);

      if(!empty($up_arr))
      $count_img= count($up_arr);
      $final_loop = $total_loop - $count_img ;
      if(!empty($up_arr))
      {
         
         foreach($up_arr as $up_ar)
         {
     ?>  
     <!--already upload image start-->
         <div class="images-block" >
         <div class="">
         <div>
          <label> Image <?php echo $i;?> </label>
         </div>
         
           <?php 
           if(!empty($up_ar['imageName']))
          {
            if(file_exists("assets/uploads/jobs/".$up_ar['imageName'])>0){  ?>
           <div><img src="<?php echo base_url(); ?>assets/uploads/jobs/<?php echo $up_ar['imageName'];?>" style="width:90px;margin:0 auto;float:none;" class="icons"/>
           </div>
           <?php }else{?>
           <div>
            <img src="<?php echo base_url(); ?>assets/uploads/no-preview.jpg" style="width:90px;" class="icons"/>
           </div>
           <?php
           } ?>
           <div style="" class="clearfix">
           <a href="<?php echo base_url().'backend/photographer_jobs/delete_image/'.$jobs->id.'/'.$i; ?>" class="btn btn-danger btn-xs " rel="tooltip"  data-placement="top" data-original-title="Delete This Question" onclick="if(confirm('Are you sure want to delete?')){return true;} else {return false;}"  ><i class="fa fa-close"></i></a> 
         
          <?php }else{ ?>
            <div style="margin-top:10px;">
          <div>
          <img src="<?php echo base_url(); ?>assets/uploads/default.png" style="width:90px;" class="icons"/>
          </div>
            </div>
          <?php }
          
          ?>
          <div class="fileinput-block">
            <input type="file" style="float: left; margin-left:5px;" name="job_image[]"  value="" accept="image/*" class="fileinput-btn" >
            <label for="imginput" class="fileinput-label">Choose File</label>
            <input type="hidden"  name="all_image[]" id="all_image_<?php echo $i; ?>_p" value="<?php echo $up_ar['imageName'];?>">
          </div>
       
          </div> 
           </div>
           </div>
         
          <?php
          $i++;
          } 
          } ?>


        
          <?php 
          $k = $i;
          for($j=0; $j< $final_loop; $j++)
          {

          ?>
           
          <div class="images-block">
          <div class="">
             <div>
             <label> Image <?php echo $k;?> </label>
              </div> 
          <div>
          <img style="width:90px;margin:0 auto;float:none;" id="preview_<?php echo $j;?>_new" src="<?php echo base_url(); ?>assets/uploads/no-preview.jpg" alt="" />
          </div>
          <div class="fileinput-block">
          <input type="file" name="job_image[]"  accept="image/*" class="fileinput-btn" id="<?php echo $j;?>" onchange="readURL(this,this.id);"  > 
          <label for="imginput" class="fileinput-label">Choose File</label>
          </div>
          <div class="clearfix"></div>
          </div>
         </div>
         
           <div>
          
          <input type="hidden" name="all_image[]" id="all_image_<?php echo $j; ?>_q" value="">
          
        </div>
          <?php
          $k++; 
          }
          ?>
         
            </div>
         </div> 
         <div class="clearfix"></div>
          <div style="color:red; margin-left:10px;">Your image needs to be atleast 700 x 400 pixels and maximum  2000 x 2000 pixels.</div>     
      </div>
  </div>
        <!--preview end-->
      <div class="box-footer" style="margin-left:10px; margin-top:30px;">
            <input type="submit" name="submit" value="Update"  class="btn btn-primary"/>                                
      </div>
  </form>
                  


</div>
</section>

<script>
function readURL(input,id) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#preview_'+id+'_new').css('display','block');
          $('#preview_'+id+'_new')
              .attr('src', e.target.result)
              .width(90)
              .height();
             $('#preview_'+id+'_new')
              .attr('src', e.target.result)
              .width(90)
              .height();
      };

      reader.readAsDataURL(input.files[0]);
  }
}

</script>

<style type="text/css">
.images-block{
  min-height: 150px;
  padding-left: 10px;
  margin-top: 10px;
  float: left;
  width: 20%;
  text-align: center;
    border-right: 1px solid #ddd;
}
.fileinput-label{    
/*  position: absolute;*/
  display: inline-block;
  width: 100px;
  height: 23px;
  background: #ccc;
  border-radius: 4px;
  text-align: center;  line-height: 23px;

}
.fileinput-btn{
  opacity: 0;
  position: absolute;
  z-index: 10;
  height: 30px;   
  cursor: pointer;  
  max-width: 130px;
}
.btn-danger{
  position: relative;
  z-index: 100;
  top: -1px;
  margin-right: 5px;
}
.fileinput-block {
  position: relative;
  height: 35px;
  padding-top: 5px;
  display: inline-block;
}
.images-block img {
    border: 1px solid #ccc;
    padding: 2px;
    max-height: 60px;
    border-radius: 2px;
}
@media(max-width: 768px){
.images-block{width: 50%;}
}
</style>