
<style type="text/css">
  .users_m {
    width: 10%;
}
.title_m {
    width: 12%;
}
.specaility_m {
    width: 14%;
}
.facility {
    width: 14%;
}
.statuss{width: 130px}
.verifys{width: 120px}
</style>
<div class="bread_parent">
<ul class="breadcrumb">
   <!--  <li><a href="<?php //echo base_url('backend/superadmin/dashboard');?>">Dashboard  </a></li> -->
    <li class="">Photographer Jobs</li>
</ul>
</div>

            <section class="panel">
              
              <!--   <div   id="demo" class="collapse" > -->

                <div class="col-lg-14"> 
                   
                         <form action="<?php echo base_url() ?>backend/photographer_jobs/index/0" method="get" accept-charset="utf-8">

                           
                          
                            <label class="col-md-2 no-padding-left title_m" >
                                <input type="text" name="title" value="<?php echo $this->input->get('title'); ?>" class="form-control" placeholder="Job Title">
                          </label>                        
                           <label class="col-md-2 no-padding-left specaility_m" >
                                <!--<input type="text" name="specialty" value="<?php echo $this->input->get('specialty'); ?>" class="form-control" placeholder="Medical Specialty">-->
                               <?php $medicalfasility_lists=get_medicalfasility_list();
                                 $specialty=$this->input->get('specialty'); ?>
                           <select name="specialty" class="form-control">
                              <option value="All">Specialty</option> 
                              <?php if(!empty($medicalfasility_lists)){ 
                                foreach($medicalfasility_lists as $medicalfasility_list){ ?>
                              <option value="<?php echo $medicalfasility_list->id ?>" <?php if(!empty($specialty)){ 
                                if($medicalfasility_list->id ==$specialty) { echo 'selected'; }}  ?> >
                                <?php echo $medicalfasility_list->name ;?></option>
                                <?php } } ?>
                                
                             
                            </select>
                          </label>
                         

                          
                           <?php
                            $ASC= $DESC="";  
                            if($this->input->get('order')) {
                                if($this->input->get('order')=="DESC")
                                {
                                  $DESC='selected';
                                }
                                else{
                                  $ASC='selected';
                                }
                              }
                          
                         
                            ?>

                          <label class="col-md-1 no-padding-left verifys" >
                          <select name="order" class="form-control">
                            <option value="DESC" <?php  echo $DESC; ?>>New</option>
                           <option value="ASC" <?php  echo $ASC; ?>>Old</option>  
                            </select>
                          </label>
                           
                          <button type="submit" class="btn btn-primary" title="Search" data-toggle="tooltip"><i class="fa fa-search" aria-hidden="true" ></i>
                          </button>
                         
                           <a href="<?php echo current_url(); ?>" title="Refresh" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i>
                          </a>
                        
                        </form>
                        
                        </div>

          <!--===============category table=================-->

          
                 <header class="panel-heading"  ></header>
                 <form method="post" action="" id="" name="" >
                  <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                      <tr>
                          <th width="110">
                          S.No
                          </th>
                          <th>Job ID</th>
                          <th>Job Title</th>
                          <th >Medical Specialty</th>         
                      
                          <th width="">Actions</th>                    
                      </tr>
                    </thead>
                    <tbody>
                    <?php
               $i= $offset + 1;
               if($result_data != "")
               {
                $jk=0;
                foreach ($result_data as $res_data)
                {
                  $jk++;
                ?>
                      <tr>
                      <td>
                     
                      <?php echo $i; ?>
                      </td>
                       
                       <td>
                       <a href="<?php echo base_url();?>backend/photographer_jobs/photograph_edit/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>">
                       <?php if(!empty($res_data['id'])) echo '#'.$res_data['id']; ?>
                       </a>
                       </td>
                                 
                        <td>   <a href="<?php echo base_url();?>backend/photographer_jobs/photograph_edit/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"><?php if(!empty($res_data['listing_title'])) echo ucfirst($res_data['listing_title']);  ?></a>
                            </td>
                         <td><?php echo get_medical_specialty($res_data['medical_specialty_name']); ?></td>
                        
                        
                        <td>
                        <?php 
                        $checked_mark = '';
                        $checked_mark = check_marked_complete($res_data['id']);
                        if($checked_mark)
                        {
                        ?>
                        <span title="Request already sent of   Marked Completed" class="btn btn-success btn-xs tooltips" data-toggle="tooltip">
                    Request already sent of   Marked Completed            </span>
                          <?php }else{ ?>
                             <span title="Send request for Marked Complete" data-toggle="tooltip"><a  class="btn btn-info btn-xs tooltips" href="<?php echo site_url();?>backend/photographer_jobs/marked_complete/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"  title="Send request for Marked Completed">Send request for Marked Completed
                              </a>
                          </span>
                           
                            <?php } ?>
                             <span title="Edit Photograph" data-toggle="tooltip"><a target="_blank" class="btn btn-primary btn-xs tooltips" href="<?php echo site_url();?>backend/photographer_jobs/photograph_edit/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"  title="Edit Photograph"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								              </a>
                          </span>
                         
                        </td> 
                      </tr>

                      <?php $i++;} }else{?>
                        <tr ><td colspan="8" class="error" style="text-align:center; color:red!important;"><h4>Job record not found.</h4></td></tr>
                <?php } ?>
                </tbody>
                
              </table>
              <?php if(!empty($pagination)) echo $pagination;?>
                   </form>
                
               </section>
 
  </div>
<script type="text/javascript" >
 $(document).ready(function(){
  setTimeout(function(){
  $(".flash").fadeOut("slow", function () {
  $(".flash").remove();
      }); }, 5000);
 });
</script>
<script>
$(document).ready(function(){
    $(".dropdown-toggle_get_val").dropdown();
});

</script>
  