
<div class="bread_parent">
<ul class="breadcrumb">
  <!--   <li><a href="<?php //echo base_url('backend/photographer_login/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li> -->
    <li><i class="fa fa-suitcase"></i> My Profile</li>
           
</ul>
</div>


<div class="row">
     <div class="col-lg-14">
        <section class="panel">
          <header class="panel-heading heading_class"><i class="fa fa-suitcase"></i> Profile</header>
  <form role="form" method="post" class="form-horizontal tasi-form" action="<?php echo current_url()?>">
  <div class="panel">

   
  <div class="col-lg-12">
      <div class="form-group">
        <label class="col-sm-2 col-sm-2">First Name</label>
         <div class="col-sm-10">
        <input type="text" placeholder="First Name" class="form-control" name="firstname" value="<?php if(!empty($user->user_first_name)) echo $user->user_first_name ; ?>">
         <div style="float:left">
        <?php echo form_error('firstname'); ?>
        </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 col-sm-2">Last Name</label>
         <div class="col-sm-10">
        <input type="text" placeholder="Last Name" class="form-control" name="lastname" value="<?php if(!empty($user->user_last_name)) echo $user->user_last_name ; ?>">
         <div style="float:left">
        <?php echo form_error('lastname'); ?>
        </div>
        </div>
      </div>

      <div class="form-group">
       <label class="col-sm-2 col-sm-2">Email Address</label>
       <div class="col-sm-10">
          <input type="email" placeholder="Email Address" class="form-control" name="email" value="<?php if(!empty($user->user_email)) echo $user->user_email;?>" readonly>
           <div style="float:left">
          <?php echo form_error('email'); ?>
          </div>
          </div>
        
      </div>

     <div class="form-group">
       <label class="col-sm-2 col-sm-2">City</label>
       <div class="col-sm-10">
          <input type="text" placeholder="City" class="form-control" name="city" value="<?php if(!empty($user->city)) echo $user->city;?>">
           <div style="float:left">
          <?php echo form_error('city'); ?>
          </div>
          </div>
        
      </div>
    <div class="form-group">
     <label class="col-sm-2 col-sm-2"></label>
     <div class="col-sm-10">
      <button class="btn btn-primary" type="submit">Update Profile</button>   
      </div> 
    </div>    

    </div>

  </form>
</div>
</section>

  </div>
  </div>
