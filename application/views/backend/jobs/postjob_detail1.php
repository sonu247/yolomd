<script
src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBOrLgvREO-Gt4k0XP7qCEa6vskE5Zz1q0">
</script>

<script>
var lat = "<?php if(!empty($post_data->latitude)) echo $post_data->latitude; ?>";
var lon = "<?php if(!empty($post_data->longitude)) echo $post_data->longitude; ?>";
var myCenter=new google.maps.LatLng(lat,lon);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>">Dashboard  </a></li>
    <li><a href="<?php echo base_url('backend/jobs/all_jobs');?>">Post Jobs List </a></li>
    <li>Post Job Detail</li>
           
</ul>
</div>

 <?php
  //print_r($post_data);

    $status_array = array('0'=>'Pending','1'=>'Active','2'=>'Deactive','3'=>'Banned','4'=>'Draft');?>
                 
  

        <section class="">
        <br>
        <div class="col-md-12">
        <header class="panel-heading">
           <?php if(!empty($post_data->listing_title)) echo ucfirst($post_data->listing_title); ?> Post Job - Detail 
           <div class="pull-right" >
               <div class="pull-left">
            <?php if(!empty($post_data->job_verify)){ 
                                   if($post_data->job_verify=='1'){
                                        echo '<div class="btn btn-success " style="margin: 2px 13px 6px 0px; height: 22px;  font-size: 12px;  padding-top: 1px !important; ">Verified</div>';}
                                   else{echo '<div class="btn btn-warning" style="margin: 2px 13px 6px 0px; height: 22px;  font-size: 12px;  padding-top: 1px !important; ">Not Verified</div>';}}
                            else{ echo '<div class="btn btn-warning" style="margin: 2px 13px 6px 0px; height: 22px;  font-size: 12px;  padding-top: 1px !important; ">Not Verified</div>';}?>
                </div>
                <div class="dropdown pull-left" style="right:10px;">
                               <button class="<?php 
                            if($post_data->status == 1)
                             echo 'btn btn-success';
                            elseif($post_data->status == 2)
                              echo 'btn btn-danger';
                              elseif($post_data->status == 3)
                                echo 'btn btn-info';
                               elseif($post_data->status == 0)
                                echo 'btn btn-warning';
                              elseif($post_data->status == 4)
                                 echo 'btn btn-default';
                              ?> btn-xs  dropdown-toggle_get_val" id="menu<?php if(!empty($post_data->id)) echo $post_data->id; ?>" type="button" data-toggle="dropdown"><?php 
                            foreach($status_array as $k => $status_a)
                            {
                               if($k == $post_data->status) 
                                echo $status_a;
                            }
                            if($post_data->status != 4)
                            {
                             ?>
                        
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu<?php if(!empty($post_data->id)) echo $post_data->id; ?>">
                               <?php
                               foreach($status_array as $k => $status_a)
                                {
                                  if($k != $post_data->status && $k != 4)
                                  {
                              ?>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url();?>backend/listusers/change_status_postjob/<?php if(!empty($post_data->id)) echo $post_data->id; ?>/<?php echo $k; ?>"><?php echo $status_a; ?></a></li>
                              <?php } } ?>
                              
                            </ul>
                            <?php 
                                }
                              ?>
                </div>
                <div class="pull-right">
                 <a  target="_blank" href="<?php echo site_url();?>" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" title="Go to Frontside">
                <i class="fa fa-home" aria-hidden="true"></i>
                </a>
                </div>
           </div>
        </header>
        </div>
                 
        <div class ="panel-body">
          <form  class="form-horizontal" >
          
          <div class="col-md-6">
          <div class="clearfix"> 
          <div class="col-md-12"><header class="panel-heading" >
             Contact Information
          </header></div>
           <div class="col-lg-12">
               <div class ="form-group">
               <label class ="col-md-3">Full Name </label>
               <div class ="col-sm-8">
                 <?php if(!empty($post_data->contact_name_title)) echo $post_data->contact_name_title; ?>
                  <?php if(!empty($post_data->contact_first_name)) echo $post_data->contact_first_name; ?>
                   <?php if(!empty($post_data->contact_last_name)) echo $post_data->contact_last_name; ?>
               </div>
               </div>
              </div> 
           <div class="col-lg-12">
               <div class ="form-group">
               <label class ="col-md-3">Email Address </label>
               <div class ="col-sm-8">
                 <?php if(!empty($post_data->contact_email_address)) echo $post_data->contact_email_address; ?>
                  
               </div>
               </div>
              </div>

          </div>  
          <!--end row-->
          <div class="clearfix">
            <div class="col-md-12"><header class="panel-heading" >
              Medical Director Information
            </header></div>
                       
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Director Full Name </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->director_name_title)) echo $post_data->director_name_title; ?>
                <?php if(!empty($post_data->director_first_name)) echo $post_data->director_first_name; ?>
                 <?php if(!empty($post_data->director_last_name)) echo $post_data->director_last_name; ?>
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Gender </label>
             <div class ="col-sm-8">
                <?php if(!empty($post_data->director_gender)) echo $post_data->director_gender; ?>
                
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Director Date of Birth </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->director_dob_date)) echo $post_data->director_dob_date; ?>/<?php if(!empty($post_data->director_dob_month)) echo $post_data->director_dob_month; ?>/<?php if(!empty($post_data->director_dob_year)) echo $post_data->director_dob_year; ?>
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Director Phone Number </label>
             <div class ="col-sm-8">
                <?php if(!empty($post_data->director_phone_number)) echo $post_data->director_phone_number; ?>
                
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Director Other Number </label>
             <div class="col-sm-8">
                <?php if(!empty($post_data->director_other_number)) echo $post_data->director_other_number; ?>
                
             </div>
             </div>
            </div> 

          </div>  
          <!--end row-->

           <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
           I am looking for
          </header></div>
                     
             <div class="col-lg-12">
               <div class ="form-group">
               <label class ="col-md-3">Medical Specialties </label>
               <div class ="col-sm-8">
                 <?php if(!empty($post_data->medical_specialty_name)) echo get_medical_specialty($post_data->medical_specialty_name); ?>
                
               </div>
               </div>
              </div> 
              
          </div>  
          <!--end row-->
          <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
            Facility Location
          </header></div>
                     
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Facility Name </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_name)) echo $post_data->facility_name; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Province </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_state)) echo get_province_name($post_data->facility_state); ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">City </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_city)) echo $post_data->facility_city; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Address </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_address)) echo $post_data->facility_address; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Postal Code </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_postal_code)) echo $post_data->facility_postal_code; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3"> Latitude</label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->latitude)) echo $post_data->latitude; ?>
              
             </div>
             </div>
            </div> 
             <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3"> Longitude</label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->longitude)) echo $post_data->longitude; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
            <div style="color:red;" class="padding"><h4>Please provide correct detail of address which is nearest of canada. If you want to show location on map.</h4></div>
            <?php if(!empty($post_data->longitude) && !empty($post_data->longitude)) {?>
            <div id="googleMap" style="width:500px;height:250px;"></div>
            <?php }
            //else{
             ?>
           <!--  <div style="color:red;" class="padding"><h4>Please provide correct detail of address. If you want to show location on map.</h4></div> -->
           <!--  <?php //} ?> -->

            </div>
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3"> Fax</label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_fax)) echo $post_data->facility_fax; ?>
              
             </div>
             </div>
            </div> 
            
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3"> Phone Number</label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_phone_number)) echo $post_data->facility_phone_number; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Ext </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_ext_number)) echo $post_data->facility_ext_number; ?>
              
             </div>
             </div>
            </div> 
              
          </div>  
          <!--end row-->
          <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
            Public Transport 
          </header></div>
                     
            <div class="col-lg-12">
               <div class="form-group">
               <label class ="col-md-3">Transport </label>
               <div class ="col-sm-8">
                <div class ="col-sm-5">
               <!--   <input type="checkbox"  <?php //if(!empty($post_data->transport_metro_subway)) echo 'checked'; ?> disabled > -->
                        <?php if(!empty($post_data->transport_metro_subway)) echo '   Metro/Subway';?> 
                        </div>
                         <div class   ="col-sm-4">
                       <?php if(!empty($post_data->transport_m_s_value)) echo  'Value : ' .$post_data->transport_m_s_value;?>
                </div>
               </div>

               <div class="col-sm-8">
                <div class ="col-sm-5">
                <!--  <input type="checkbox"  <?php// if(!empty($post_data->transport_bus)) echo 'checked'; ?> disabled > -->
                      <?php if(!empty($post_data->transport_bus)) echo '   Bus';?> 
                </div>
                <div class="col-sm-4">
                  <?php if(!empty($post_data->transport_bus_value)) echo  'Value : ' .$post_data->transport_bus_value;?>
                </div>
               </div>
                 <label class ="col-md-3"></label>
                <div class ="col-sm-8">
                <div class ="col-sm-5">
                <!--  <input type="checkbox"  <?php //if(!empty($post_data->transport_other)) echo 'checked'; ?> disabled > -->
                        <?php if(!empty($post_data->transport_other)) echo '  Other';?> 
                        </div>
                         <div class   ="col-sm-4">
                       <?php if(!empty($post_data->transport_other_value)) echo  'Value : ' .$post_data->transport_other_value;?>
                </div>
               </div>
               </div>
              </div> 
              
          </div>  
        <!--end row-->

          <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
              Facility Details
            </header></div>
                       
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Facility Type  </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->facility_type)) echo get_facility_type($post_data->facility_type); ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Parking </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->parking)) echo $post_data->parking; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">EMR </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->emr)) echo $post_data->emr; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Number of full-time doctors </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->no_of_fulltime)) echo $post_data->no_of_fulltime; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Part-time doctors </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->part_time_doctor)) echo $post_data->part_time_doctor; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3"> Number of exam rooms </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->exam_room)) echo $post_data->exam_room; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class   ="form-group">
             <label class ="col-md-3"> Nursing </label>
             <div class   ="col-sm-8">
               <?php 
               if(!empty($post_data->nursing_array))
              {
                  $get_unserial = unserialize($post_data->nursing_array);
                 
                  $value = '';
                  foreach($get_unserial as $key => $get_unseri)
                  {
                      $value .= get_nursing($key).',';
                  }
                  echo trim($value,',');
              }
              
                ?>
              
             </div>
             </div>
            </div> 
                 
          </div>  
            <!--end row-->
          </div>

          <div class="col-md-6">
          <div class="clearfix">
           
                <div class="col-md-12">
               <header class="panel-heading" >
                  Other
              </header>      
            </div>
            <div class="col-lg-12">
               <div class ="form-group">
                   <label class ="col-md-3">Created Time</label>
                   <div class ="col-sm-8">
                       <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $post_data->created; ?>
                  </div>
               </div>
            </div> 
          
           <div class="col-lg-12">
               <div class ="form-group">
                   <label class ="col-md-3">Posted On</label>
                   <div class ="col-sm-8">
                       <i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $post_data->modified; ?>
                  </div>
               </div>
            </div>
            
            <div class="col-lg-12">
               <div class ="form-group">
                   <label class ="col-md-3">User</label>
                   <div class ="col-sm-8">
                       <i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo base_url();?>backend/listusers/edit_medical/<?php if(!empty($post_data->user_id)) echo $post_data->user_id; ?>"> 
                        <?php if(!empty($user->contact_first_name)) echo $user->user_first_name; ?>
                        <?php if(!empty($user->contact_last_name)) echo $user->user_last_name; ?>(#<?php if(!empty($post_data->user_id)) echo $post_data->user_id; ?>)</a>
                  </div>
               </div>
            </div>
            <div class="col-lg-12">
               <div class ="form-group">
                   <label class ="col-md-3">Job Id</label>
                   <div class ="col-sm-8">
                      #<?php echo  $post_data->id; ?>
                  </div>
               </div>
            </div>
          <div class="col-md-12"><header class="panel-heading" >
              Facility Hours
            </header> </div>
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Facility Hours  </label>
             <div class  ="col-sm-8">
              
              <?php 
                $facility_days_name = array();
                $facility_time = unserialize($post_data->facility_hours_array);
                $dateOrders = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday","Saturday","Sunday"];
                if(!empty($facility_time))
                {
                  $facility_hour_loop = $facility_time;
                  if(!empty($facility_hour_loop))
                  {
                    foreach($facility_hour_loop as $facility_loop)
                    {
                      if(!empty($facility_loop['master_days_name']))
                      { 
                        foreach($facility_loop['master_days_name'] as $facility_l)
                        {
                          $id_name = get_days_name($facility_l['id']);
                          if (!in_array($id_name, $facility_days_name))
                          {
                            $facility_days_name[] = $id_name;
                            $facility_hours_all[$id_name][] = 
                            array(
                              'start_time' =>$facility_loop['start_time'],
                              'end_time'   => $facility_loop['end_time'],
                            );
                          }else
                          {
                            
                            $facility_hours_all[$id_name][] = 
                            array(          
                              'start_time' =>$facility_loop['start_time'],
                              'end_time'   => $facility_loop['end_time'],
                            );
                          }
                        }
                      }  
                    }
                  }
                  $facility_days_name;
                  $facility_hours_all;

                  $FacilityDaysNameAll = array();
                  foreach($dateOrders as $key1 => $value1)
                  {
                    foreach($facility_days_name as $key => $value)
                    {
                      if($value == $value1)
                      {
                        $FacilityDaysNameAll[] = $value;
                      }
                    }
                  }
                  if(!empty($FacilityDaysNameAll))
                  {
                    foreach($FacilityDaysNameAll as $facilityday)
                    {
                      echo $facilityday.'<br/>';
                      if(!empty($facilityday))
                      {
                        foreach($facility_hours_all[$facilityday] as $full_name)
                        {
                          echo '('.$full_name['start_time']  .'-'. $full_name['end_time'].')<br/>';
                        }
                      }  
                    }
                  }
                  
                }
                ?>
              
             </div>
             </div>
            </div> 
          </div>  
            <!--end row-->
          <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
              Job Description
            </header></div>
                       
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Hours </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->hour)) echo get_hours($post_data->hour); ?>
                
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Position </label>
             <div class ="col-sm-8">
                <?php if(!empty($post_data->position)) echo $post_data->position; ?>
                
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Start Date </label>
             <div class ="col-sm-8">
               
               <?php 
                  $total_date = '';
                  if(!empty($post_data->start_date) && !empty($post_data->start_month) && !empty($post_data->start_year))
                  {
                    $total_date = $post_data->start_date.'/'.$post_data->start_month.'/'.$post_data->start_year;

                    echo date('d M Y',strtotime($total_date));
                  }else{ echo "-";}


               ?>
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Compensation </label>
             <div class ="col-sm-8">
                <?php if(!empty($post_data->compensation)) echo get_compensation($post_data->compensation); ?>
                
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class ="form-group">
             <label class ="col-md-3">Call Responsibilities </label>
             <div class ="col-sm-8">
                <?php if(!empty($post_data->call_responsibility)) echo $post_data->call_responsibility; ?>
                
             </div>
             </div>
            </div> 

          </div>  
            <!--end row-->
          <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
              Allied Health Professionals
            </header></div>
                       
            <div class="col-lg-12">
              <div class ="form-group">
                 <label class ="col-md-3">Allied Health Professionals </label>
                 <div class ="col-sm-8">
                  
                   <?php 
                   if(!empty($post_data->health_array))
                  {
                      $get_health_array = unserialize($post_data->health_array);
                     
                      $health_val = '';
                      foreach($get_health_array as $key => $health_value)
                      {
                          $health_val .= get_health_professional($key).',';
                      }
                      echo trim($health_val,',');
                  }
                  
                    ?>
                  
                 </div>
                 </div>
            </div> 
                
           </div>  
          <!--end row-->
          <div class="clearfix">

          <div class="col-md-12"><header class="panel-heading" >
              Listing Description
            </header></div>
            <div class="col-lg-12">
             <div class="form-group">
             <label class="col-md-3">Listing Title </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->listing_title)) echo $post_data->listing_title; ?>
              
             </div>
             </div>
            </div> 
            <div class="col-lg-12">
             <div class="form-group">
             <label class="col-md-3">Listing Description </label>
             <div class ="col-sm-8">
               <?php if(!empty($post_data->listing_description)) echo $post_data->listing_description; ?>
              
             </div>
             </div>
            </div> 

          </div>  
          <!--end row-->

          <div class="clearfix">
            <div class="col-md-12"><header class="panel-heading" >
                Special Qualifications
              </header></div>
            <div class="col-lg-12">
                 <div class ="form-group">
                 <label class ="col-md-3">Special Qualifications
                   </label>
                 <div class ="col-sm-8">
                   <?php if(!empty($post_data->special_qualification)) echo $post_data->special_qualification; ?>
                  
                 </div>
                 </div>
              </div> 
          </div>  
          <!--end row-->

          <div class="clearfix">

            <div class="col-md-12"><header class="panel-heading" >
              Upload
            </header>      </div>
            <div class="col-lg-12">
               <div class ="form-group">
                <label class ="col-md-3"><?php if(empty($post_data->uploadImageArray)){ echo 'No Images';}else{ echo 'Images';} ?> 
                 </label>
               <div class ="col-sm-8">
               <?php 
                 if(!empty($post_data->uploadImageArray))
                {
                    $get_image_array = unserialize($post_data->uploadImageArray);
                
                    foreach($get_image_array as $key =>$get_image)
                    {
                        foreach($get_image as $key1=>$image_name)
                        {
                          ?>
                          <!-- Trigger the modal with a button -->
                          <img data-toggle="modal" data-target="#myModal<?php if(!empty($key)) echo $key; ?>" style="width:90px; height:90px;" src="<?php echo base_url();?>assets/uploads/jobs/<?php if(!empty($image_name)) echo $image_name;?>">
                          <!-- Modal -->
                          <div id="myModal<?php if(!empty($key)) echo $key; ?>" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style="height:40px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h4 class="modal-title"></h4>
                                 
                                </div>
                                <div class="modal-body">
                                 <img style="width:100%; height:100%;" src="<?php echo base_url();?>assets/uploads/jobs/<?php if(!empty($image_name)) echo $image_name;?>">
                                </div>
                                
                              </div>

                            </div>
                          </div>
                       <?php
                        }
                    }
                   
                }
                
                  ?>
                  </div>
               </div>
            </div> 

            
        <!--end row-->
          </div>

          <div class="clearfix"></div>


          <div class="col-md-12"><header class="panel-heading" >
             Payment Information
            </header></div>
          
            
            <div class="col-lg-12">
             <div class ="form-group" style="color:red;">
             For more information, please click here 
             <a target="_blank" href="<?php echo site_url(); ?>backend/payment_receipts/payment_list/0?payment_id=&job_id=<?php if(!empty($receipts_detail['job_id'])) echo $receipts_detail['job_id']; ?>&order=DESC" class="button">Payment Receipt</a>
             </div>
             </div>
           
             <div class="clearfix"></div>
      
          <!--end panel-->
        </form>
      </div>
        </section>

