<script
src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBOrLgvREO-Gt4k0XP7qCEa6vskE5Zz1q0">
</script>

<script>
var lat = "<?php if(!empty($post_data->latitude)) echo $post_data->latitude; ?>";
var lon = "<?php if(!empty($post_data->longitude)) echo $post_data->longitude; ?>";
var myCenter=new google.maps.LatLng(lat,lon);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="panel-body ">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>">Dashboard  </a></li>
    <li><a href="<?php echo base_url('backend/jobs');?>">Post Jobs List </a></li>
    <li>Post Job Detail</li>
           
</ul>


 <?php
    $status_array = array('0'=>'Pending','1'=>'Active','2'=>'Deactive','3'=>'Banned','4'=>'Draft');?>
    <section class="">
    <div class="col-md-12">
    <header class="panel-heading">
        Detail -<?php if(!empty($post_data->listing_title)) echo ucfirst($post_data->listing_title); ?>
       <div class="pull-right" >
           
            <div class="dropdown pull-left" style="right:10px;">
                <button class="<?php 
                if($post_data->status == 1)
                 echo 'btn btn-success';
                elseif($post_data->status == 2)
                  echo 'btn btn-danger';
                  elseif($post_data->status == 3)
                    echo 'btn btn-info';
                   elseif($post_data->status == 0)
                    echo 'btn btn-warning';
                  elseif($post_data->status == 4)
                     echo 'btn btn-default';
                  ?> btn-xs  dropdown-toggle_get_val" id="menu<?php if(!empty($post_data->id)) echo $post_data->id; ?>" type="button" data-toggle="dropdown"><?php 
                foreach($status_array as $k => $status_a)
                {
                   if($k == $post_data->status) 
                    echo $status_a;
                }
                if($post_data->status != 4)
                {
                 ?>
            
                <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="menu<?php if(!empty($post_data->id)) echo $post_data->id; ?>">
                   <?php
                   foreach($status_array as $k => $status_a)
                    {
                      if($k != $post_data->status && $k != 4)
                      {
                  ?>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url();?>backend/listusers/change_status_postjob/<?php if(!empty($post_data->id)) echo $post_data->id; ?>/<?php echo $k; ?>"><?php echo $status_a; ?></a></li>
                  <?php } } ?>
                  
                </ul>
                <?php 
                    }
                  ?>
            </div>
            <?php 
            if(!empty($post_data->status)){
            ?>
            <div class="pull-right">
             <a  target="_blank" href="<?php echo site_url();?>post-job-update/<?php if(!empty($post_data->id)) echo base64_encode($post_data->id); ?>" data-toggle="tooltip" class="btn btn-info btn-xs tooltips" title="Go to Frontside">
            <i class="fa fa-home" aria-hidden="true"></i>
            </a>
            </div>
            <?php } ?>
       </div>
    </header>
   <br>
    <div class="col-md-8">           
    <table class="responsive table table-striped" >
   <?php 
   if(!empty($post_data->contact_name_title) || !empty($post_data->contact_first_name) || !empty($post_data->contact_last_name))
   {
   ?>
    <tr>
      <th width="250">
        User Name : 
      </th>
      <td>
         <?php 
               if(!empty($post_data->contact_name_title)) echo $post_data->contact_name_title; 
               if(!empty($post_data->contact_first_name)) echo ' '.$post_data->contact_first_name; 
               if(!empty($post_data->contact_last_name)) echo ' '.$post_data->contact_last_name; 
             ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->contact_email_address))
    {
    ?>
    <tr>
      <th>
        Email Address :
      </th>
      <td>
        <?php if(!empty($post_data->contact_email_address)) echo $post_data->contact_email_address; ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->contact_phone_number))
    {
    ?>
     <tr>
      <th>
        Phone Number :
      </th>
      <td>
        <?php if(!empty($post_data->contact_phone_number)) echo $post_data->contact_phone_number; ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->contect_ext_number))
    {
    ?>
     <tr>
      <th>
       Ext :
      </th>
      <td>
        <?php if(!empty($post_data->contect_ext_number)) echo $post_data->contect_ext_number; ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->contact_fax))
    {
    ?>
     <tr>
      <th>
        Fax :
      </th>
      <td>
        <?php if(!empty($post_data->contact_fax)) echo $post_data->contact_fax; ?>
      </td>
    </tr>
     <?php } 
    if(!empty($post_data->medical_specialty_name))
    {
    ?>
     <tr>
      <th>
        Medical Specialties :
      </th>
      <td>
        <?php if(!empty($post_data->medical_specialty_name)) echo get_medical_specialty($post_data->medical_specialty_name); ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->special_qualification))
    {
    ?>
     <tr>
      <th>
        Special Qualifications :
      </th>
      <td>
         <?php if(!empty($post_data->special_qualification)) echo $post_data->special_qualification; ?>
                  
      </td>
    </tr>
   <?php } 
    if(!empty($post_data->hour))
    {
    ?>
     <tr>
      <th>
        Hours :
      </th>
      <td>
        <?php if(!empty($post_data->hour)) echo get_hours($post_data->hour); ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->position))
    {
    ?>
     <tr>
      <th>
        Position Type :
      </th>
      <td>
        <?php if(!empty($post_data->position)) echo $post_data->position; ?>
      </td>
    </tr>
    <?php } 
     if(!empty($post_data->start_date) || !empty($post_data->start_month) || !empty($post_data->start_year))
    {
    ?>
     <tr>
      <th>
        Start Date :
      </th>
      <td>
       <?php 
                  $total_date = '';
                  if(!empty($post_data->start_date) && !empty($post_data->start_month) && !empty($post_data->start_year))
                  {
                    $total_date = $post_data->start_month.'/'.$post_data->start_date.'/'.$post_data->start_year;

                    echo date('d M Y',strtotime($total_date));
                  }else{ echo "-";}


               ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->compensation))
    {
    ?>
     <tr>
      <th>
        Compensation :
      </th>
      <td>
         <?php if(!empty($post_data->compensation)) echo get_compensation($post_data->compensation); ?>
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->call_responsibility))
    {
    ?>
     <tr>
      <th>
        Call Responsibilities :
      </th>
      <td>
        <?php if(!empty($post_data->call_responsibility)) echo $post_data->call_responsibility; ?>
                
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->facility_name))
    {
    ?>
    <tr>
      <th>
        Facility Name :
      </th>
      <td>
         <?php if(!empty($post_data->facility_name)) echo $post_data->facility_name; ?>
                
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->facility_state))
    {
    ?>
    <tr>
      <th>
        Province :
      </th>
      <td>
         <?php if(!empty($post_data->facility_state)) echo get_province_name($post_data->facility_state); ?>
                
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->facility_city))
    {
    ?>
    <tr>
      <th>
        City :
      </th>
      <td>
        <?php if(!empty($post_data->facility_city)) echo $post_data->facility_city; ?>
                
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->facility_address))
    {
    ?>
    <tr>
      <th>
        Street number and name :
      </th>
      <td>
         <?php if(!empty($post_data->facility_address)) echo $post_data->facility_address; ?>
                
      </td>
    </tr>
   <?php } 
    if(!empty($post_data->facility_suite_office))
    {
    ?>
    <tr>
      <th>
        Suite or Office number :
      </th>
      <td>
       <?php if(!empty($post_data->facility_suite_office)) echo $post_data->facility_suite_office; ?>
                
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->facility_postal_code))
    {
    ?>
    <tr>
      <th>
        Postal Code :
      </th>
      <td>
        <?php if(!empty($post_data->facility_postal_code)) echo $post_data->facility_postal_code; ?>
                
      </td>
    </tr>
   <?php } 
    if(!empty($post_data->transport_m_s_value) || !empty($post_data->transport_bus_value) || !empty($post_data->transport_train_value) || !empty($post_data->transport_other_value))
    {
    ?>
    <tr>
      <th>
        Public Transport :
      </th>
      <td>
      <div>
         <?php if(!empty($post_data->transport_metro_subway)) echo '   Metro/Subway';?> 
            
        <?php if(!empty($post_data->transport_m_s_value)) echo  'Value : ' .$post_data->transport_m_s_value;?>
      </div>
      <div>
      <?php if(!empty($post_data->transport_bus)) echo '   Bus';?> 
               
      <?php if(!empty($post_data->transport_bus_value)) echo  'Value : ' .$post_data->transport_bus_value;?>
      </div>
      <div>
      <?php if(!empty($post_data->transport_train)) echo ' Train';?> 
                       
      <?php if(!empty($post_data->transport_train_value)) echo  'Value : ' .$post_data->transport_train_value;?>
      </div>
      <div>
      <?php if(!empty($post_data->transport_other)) echo '  Other';?> 
                       
      <?php if(!empty($post_data->transport_other_value)) echo  'Value : ' .$post_data->transport_other_value;?>
      </div>    
                
      </td>
    </tr>
    <?php } 
    if(!empty($post_data->parking))
    {
    ?>
     <tr>
      <th>
        Parking :
      </th>
      <td>
       <?php if(!empty($post_data->parking)) echo $post_data->parking; ?>
      </td>
    </tr> 
    <?php } 
    if(!empty($post_data->facility_hours_array))
    {
    ?>
    <tr>
      <th>
        Facility Hours :
      </th>
      <td>
        <?php 
                $facility_days_name = array();
                $facility_time = unserialize($post_data->facility_hours_array);
                $dateOrders = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday","Saturday","Sunday"];
                if(!empty($facility_time))
                {
                  $facility_hour_loop = $facility_time;
                  if(!empty($facility_hour_loop))
                  {
                    foreach($facility_hour_loop as $facility_loop)
                    {
                      if(!empty($facility_loop['master_days_name']))
                      { 
                        foreach($facility_loop['master_days_name'] as $facility_l)
                        {
                          $id_name = get_days_name($facility_l['id']);
                          if (!in_array($id_name, $facility_days_name))
                          {
                            $facility_days_name[] = $id_name;
                            $facility_hours_all[$id_name][] = 
                            array(
                              'start_time' =>$facility_loop['start_time'],
                              'end_time'   => $facility_loop['end_time'],
                            );
                          }else
                          {
                            
                            $facility_hours_all[$id_name][] = 
                            array(          
                              'start_time' =>$facility_loop['start_time'],
                              'end_time'   => $facility_loop['end_time'],
                            );
                          }
                        }
                      }  
                    }
                  }
                  $facility_days_name;
                  $facility_hours_all;

                  $FacilityDaysNameAll = array();
                  foreach($dateOrders as $key1 => $value1)
                  {
                    foreach($facility_days_name as $key => $value)
                    {
                      if($value == $value1)
                      {
                        $FacilityDaysNameAll[] = $value;
                      }
                    }
                  }
                  $total_c = '';
                  $i = 1;
                  if(!empty($FacilityDaysNameAll))
                  {
                    $total_c = count($FacilityDaysNameAll);
                    foreach($FacilityDaysNameAll as $facilityday)
                    {
                      echo $facilityday.' - ';
                      if(!empty($facilityday))
                      {
                        foreach($facility_hours_all[$facilityday] as $full_name)
                        {
                          echo '('.$full_name['start_time']  .'-'. $full_name['end_time'].')';
                        }
                      }
                      if($total_c != $i)
                      {
                         echo '<br/> '  ;
                      }
                      $i++;
                    }
                  }
                  
                }
                ?>
      </td>
    </tr> 
    <?php } 
    if(!empty($post_data->facility_type))
    {
    ?>
    <tr>
      <th>
       Facility Type  :
      </th>
      <td>
        <?php if(!empty($post_data->facility_type)) echo get_facility_type($post_data->facility_type); ?>
      </td>
    </tr> 
     <?php } 
    if(!empty($post_data->emr))
    {
    ?>
    <tr>
      <th>
       EMR :
      </th>
      <td>
        <?php if(!empty($post_data->emr)) echo $post_data->emr; ?>
      </td>
    </tr> 
    <?php } 
    if(!empty($post_data->no_of_fulltime))
    {
    ?>
    <tr>
      <th>
        Number of full-time doctors :
      </th>
      <td>
       <?php if(!empty($post_data->no_of_fulltime)) echo $post_data->no_of_fulltime; ?>
      </td>
    </tr> 
    <?php } 
    if(!empty($post_data->part_time_doctor))
    {
    ?>
    <tr>

      <th>
        Part-time doctors :
      </th>
      <td>
        <?php if(!empty($post_data->part_time_doctor)) echo $post_data->part_time_doctor; ?>
      </td>
    </tr> 
    <?php } 
    if(!empty($post_data->exam_room))
    {
    ?>
    <tr>
      <th>
        Number of exam rooms :
      </th>
      <td>
         <?php if(!empty($post_data->exam_room)) echo $post_data->exam_room; ?>
      </td>
    </tr> 
    <?php } 
    
    ?>
    <?php 
    if(!empty($post_data->listing_title))
    {
    ?>
    <tr>
      <th>
        Listing Title :
      </th>
      <td>
         <?php if(!empty($post_data->listing_title)) echo $post_data->listing_title; ?>
      </td>
     </tr> 
     <?php } 
    if(!empty($post_data->listing_description))
    {
    ?>
     <tr>
      <th>
        Listing Description :
      </th>
      <td>
           <?php if(!empty($post_data->listing_description)) echo $post_data->listing_description; ?>
      </td>
   
    </tr> 
    <?php } ?>
    </table>
    </div>
    <div class="col-md-4">
     <table class="responsive table table-striped" >
   <tr>
      <th width="140">
        Job Id : 
      </th>
      <td>
         <?php 
               if(!empty($post_data->id)) echo '#'.$post_data->id; 
             ?>
      </td>
    </tr> <tr>
      <th>
       Created Time :
      </th>
      <td>
         <?php if(!empty($post_data->created)) echo $post_data->created; ?>
      </td>
   
      </tr> <tr>
      <th>
        Updated On
      </th>
      <td>
         <?php if(!empty($post_data->modified)) echo $post_data->modified; ?>
      </td>
     </tr> <tr>

      <th>
        Posted On
      </th>
      <td>
         <?php if(!empty($post_data->posted)) echo $post_data->posted; ?>
      </td>
     </tr> 
     <tr>
     
      <td colspan="2">
        <?php 
                 if(!empty($post_data->uploadImageArray))
                {
                    $get_image_array = unserialize($post_data->uploadImageArray);
                
                    foreach($get_image_array as $key =>$get_image)
                    {
                        foreach($get_image as $key1=>$image_name)
                        {
                          if(!empty($image_name) && file_exists('./assets/uploads/jobs/'.$image_name))
                          {
                          ?>
                          <!-- Trigger the modal with a button -->
                          <img data-toggle="modal" data-target="#myModal<?php if(!empty($key)) echo $key; ?>" style="width:90px;" src="<?php echo base_url();?>assets/uploads/jobs/<?php  echo $image_name;?>">
                          <?php } ?>
                          <!-- Modal -->
                          <div id="myModal<?php if(!empty($key)) echo $key; ?>" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header" style="height:40px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h4 class="modal-title"></h4>
                                 
                                </div>
                                <div class="modal-body">

                                 <img style="width:100%; height:100%;" src="<?php echo base_url();?>assets/uploads/jobs/<?php if(!empty($image_name)) echo $image_name;?>">
                                </div>
                                
                              </div>

                            </div>
                          </div>
                       <?php
                        }
                    }
                   
                }else{
                  echo '<span style="color:red!important;">No Images</span>';
                }
                
                  ?>
               
      </td>
      </tr>

    </table>
       <?php if(!empty($post_data->longitude) && !empty($post_data->longitude)) {?>
          <div id="googleMap" style="width:400px;height:500px;"></div>
          <?php }
           
         ?>

         <!-- <div class="col-md-12"><header class="panel-heading" >
             Payment Information
            </header></div> -->
            
           <!-- <table class="responsive table table-striped">
           <tr>
           <th>Payment Amount</th>
           <th>Payment Date</th>
           <th>Receipt</th>
           </tr>
           <?php  //if(!empty($receipt_all)) { foreach($receipt_all as $rec_all) { ?>
           <tr>
           <td><?php //if(!empty($rec_all->payment_amount)) echo $rec_all->payment_amount; ?></td>
           <td><?php //if(!empty($rec_all->date_time)) echo $rec_all->date_time; ?></td>
           <td><a  class="btn btn-primary btn-xs tooltips" href="<?php //echo site_url(); ?>backend/payment_receipts/receipts_id_post/<?php// if(!empty($rec_all->payment_id)) echo $rec_all->payment_id; ?>" class="button" >Receipt</a></td>
           </tr>
           <?php// }}else{ ?>
           <td colspan="4" style="color:red!important;">No record found.</td>
           <?php //} ?>
           </table> -->
    </div>
    </div>

    </section>
  </div>


