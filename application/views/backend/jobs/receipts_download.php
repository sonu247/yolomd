
<div class="">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <div class="col-md-6 col-md-offset-3">

		   	<div class="dashboard-right">
        <div class="header">
         <!--  <img src="<?php //echo FRONTEND_THEME_URL ?>images/YoloMD_logo.png" class="img-responsive" style="width:150px!important;"> -->
        </div>
        
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.png" class="">Payment Receipts- <?php if(!empty($jobs_detail['job_title'])) echo $jobs_detail['job_title'];  ?>
        <span style="float:right; margin-top:-10px;" ng-if="JobDetail">
        <a href="<?php echo site_url(); ?>cart/generate_pdf/<?php if(!empty($jobs_detail['unique_id'])) echo $jobs_detail['unique_id'];  ?>" class="button" target="_self"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
        Pdf Receipt</a>
        </span>
        </h3>
        
		   	<div class="jobList-block">

        	<div class="jobListTile-block">
           
            <div class="table-responsive">
        	<table class="table table-hover jobListTable table-striped table-condensed table-bordered">
        	
        	<tr>
          <td class="text-center">Transaction Id</td>
        	<td class="pull-left">  
            <?php if(!empty($jobs_detail['transaction_id'])) echo $jobs_detail['transaction_id'];  ?>
          </td>
        	</tr>
          <tr  >
          <td class="text-center">Pay Date</td>
          <td class="pull-left">  
              
               <?php if(!empty($jobs_detail['date'])) echo $jobs_detail['date'];  ?>
          </td>
          </tr>
          <tr>
          <td class="text-center">Job title</td>
          <td class="pull-left">  
            <?php if(!empty($jobs_detail['job_title'])) echo $jobs_detail['job_title'];  ?>
          </td>
          </tr>
          <tr >
          <td class="text-center">Posted Job Id</td>
          <td class="pull-left">  
              <?php if(!empty($jobs_detail['job_id'])) echo $jobs_detail['job_id'];  ?>
          </td>
          </tr>
          <tr >
          <td class="text-center">YoloMD Verify</td>
          <td class="pull-left">  
             <?php if(!empty($jobs_detail['verify'])) echo $jobs_detail['verify'];  ?>
          </td>
          </tr>
          <tr>
          <td class="text-center">Services</td>
          <td class="pull-left">  
            <?php if(!empty($jobs_detail['item'])) echo $jobs_detail['item'];  ?>
          </td>
          </tr>
          
          <?php 
          if(!empty($city_detail))
          {
            foreach($city_detail as $city_d){ ?>
          <tr>
              <td class="text-center" >
                
                  <?php if(!empty($city_d['city_name'])) echo $city_d['city_name'];  ?>
              </td>
              <td class="pull-left" >  
               
                  <?php if(!empty($city_d['city_charge'])) echo '+$'.number_format($city_d['city_charge'],2);  ?>(Active untill : 
                    <?php if(!empty($city_d['valid_date']) && $city_d['valid_date'] != '0000-00-00') {  ?>
                    <span ng-if="city_d.valid_date != '0000-00-00'">{{formatDate(city_d.valid_date)| date:'d MMM y'}}</span><span ng-if="city_d.valid_date == '0000-00-00'">NA</span>)
                    <?php } ?>
              </td>
          </tr>    
          <?php }} ?>
          
           
          <tr>
          <td class="text-center">Verify Chargesd</td>
          <td class="pull-left">  
             
               <?php if(!empty($jobs_detail['yolomd_verify'])) echo '+'.$jobs_detail['yolomd_verify'];  ?>
          </td>
          </tr>
          <tr >
          <td class="text-center">Payment Amount</td>
          <td class="pull-left"> 
              <?php if(!empty($jobs_detail['payment_amount'])) echo '$'.$jobs_detail['payment_amount'];  ?>
          </td>
          </tr>
          
          <tr>
          <td class="text-center">Discount</td>
          <td class="pull-left">  
             
              <?php if(!empty($jobs_detail['discount'])) echo '-$'.$jobs_detail['discount'];  ?>
          </td>
          </tr>
          <tr >
          <td class="text-center">Paid Amount</td>
          <td class="pull-left"> 
              <?php echo '$'.($jobs_detail['payment_amount'] - $jobs_detail['discount']);  ?>
          </td>
          </tr>
			   </table>
            </div>
            </div>
            </div>

          
           <!--  <div style="color:red;"><h4><b id="not_show">
            If you want to save this receipt please press `ctrl+s` <br/> Or if you would like to take print please press `ctrl+p`.
            </b></h4>
            </div> -->
			</div>
		   </div>
		    	</div>
		    </div>
		  </div>
		</div>
		</div>
	</div>
</div>
<style type="text/css">
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}
table.jobListTable tr:first-child {
  background-color: #f9f9f9;
}
table.jobListTable tr td:first-child{
  line-height: 18px;
  font-size: 14px;
}
table.jobListTable tr th:nth-child(2){width: 215px;}
table.jobListTable tr td:nth-child(5){padding-top: 5px;}
table.jobListTable tr td:nth-child(6){vertical-align: top;}
</style>
<style>
@media print {
   .error{
       display: none;
    }
}
</style>
<script type="text/javascript">

$(document).keypress(function(event) {
    if (event.which == 115) {
        document.getElementById('not_show').style.display = 'none';
        return true;
        if (event == false) {
          document.getElementById('not_show').style.display = 'block';
            return
        }
    }
   
    
});
// $(document).keyup(function(event) {
//     if (event.which != 115 && ) {
//         alert('fdfs');
//     }
   
    
// });
</script>