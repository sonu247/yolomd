
<style type="text/css">
  .users_m {
    width: 10%;
}
.title_m {
    width: 12%;
}
.specaility_m {
    width: 14%;
}
.facility {
    width: 14%;
}
.statuss{width: 130px}
.verifys{width: 120px}
</style>
<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>">Dashboard  </a></li>
    <li class="">Post Job List</li>
</ul>
</div>

            <section class="panel">
              
              <!--   <div   id="demo" class="collapse" > -->

                <div class="col-lg-14"> 
                   
                         <form action="<?php echo base_url() ?>backend/jobs/index/0" method="get" accept-charset="utf-8">

                            <label class="col-md-1 no-padding-left">
                                <input type="text" name="Id" value="<?php echo $this->input->get('Id'); ?>" class="form-control" placeholder="Job Id">
                          </label>
                           <label class="col-md-1 no-padding-left users_m" >
                                <input type="text" name="userid" value="<?php echo $this->input->get('userid'); ?>" class="form-control" placeholder="User Id">
                            </label>
                            <label class="col-md-2 no-padding-left title_m" >
                                <input type="text" name="title" value="<?php echo $this->input->get('title'); ?>" class="form-control" placeholder="Job Title">
                          </label>                        
                           <label class="col-md-2 no-padding-left specaility_m" >
                                <!--<input type="text" name="specialty" value="<?php echo $this->input->get('specialty'); ?>" class="form-control" placeholder="Medical Specialty">-->
                               <?php $medicalfasility_lists=get_medicalfasility_list();
                                 $specialty=$this->input->get('specialty'); ?>
                           <select name="specialty" class="form-control">
                              <option value="All">Specialty</option> 
                              <?php if(!empty($medicalfasility_lists)){ 
                                foreach($medicalfasility_lists as $medicalfasility_list){ ?>
                              <option value="<?php echo $medicalfasility_list->id ?>" <?php if(!empty($specialty)){ 
                                if($medicalfasility_list->id ==$specialty) { echo 'selected'; }}  ?> >
                                <?php echo $medicalfasility_list->name ;?></option>
                                <?php } } ?>
                                
                             
                            </select>
                          </label>
                         <label class="col-md-2 no-padding-left facility">
                              <input type="text" name="FacilityName" value="<?php echo $this->input->get('FacilityName'); ?>" class="form-control" placeholder="Facility Name">
                          </label>

                           <?php 
                          $status_check = $this->input->get('status');  ?>
                          <label class="col-md-2 no-padding-left statuss">
                          <select name="status" class="form-control">
                              <option value="All">Select Status</option> <option value="5" <?php if($status_check == 5) { echo 'selected'; } ?> >Pending</option>             
                              <option value="1" <?php if($status_check== 1) { echo 'selected'; } ?> >Active</option>
                             <option value="2" <?php if($status_check == 2) { echo 'selected'; } ?> >Deactive</option>
                              <option value="3" <?php if($status_check == 3) { echo 'selected'; } ?> >Banned</option>
                               <option value="4" <?php if($status_check == 4) { echo 'selected'; } ?> >Draft</option>
                                
                             
                            </select>
                          </label>
                         
                           <?php
                            $ASC= $DESC="";  
                            if($this->input->get('order')) {
                                if($this->input->get('order')=="DESC")
                                {
                                  $DESC='selected';
                                }
                                else{
                                  $ASC='selected';
                                }
                              }
                          
                          if($this->input->get('verify')) {
                                if($this->input->get('verify')==1)
                                {
                                  $verify='selected';
                                }
                                if($this->input->get('verify')== 2)
                                {
                                  
                               
                                  $notverify='selected';
                                }
                              }
                            ?>

                          <label class="col-md-1 no-padding-left verifys" >
                          <select name="order" class="form-control">
                            <option value="DESC" <?php  echo $DESC; ?>>New</option>
                           <option value="ASC" <?php  echo $ASC; ?>>Old</option>  
                            </select>
                          </label>
                          <!--  <label class="col-md-1 no-padding-left statuss">
                          <select name="verify" class="form-control">
                                <option value="All">All Jobs</option>
                               <option value="1" <?php// if(!empty($verify)) echo $verify; ?>>Verified</option>
                               <option value="2" <?php  //if(!empty($notverify)) echo $notverify; ?>>Not Verified</option>
                            
                             
                            </select>
                          </label> -->
                          <button type="submit" class="btn btn-primary" title="Search" data-toggle="tooltip"><i class="fa fa-search" aria-hidden="true" ></i>
                          </button>
                          <?php if(!empty($_GET['status'])){ ?>
                          <a href="<?php echo site_url(); ?>backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=<?php if(!empty($_GET['status'])) echo $_GET['status'];?>&order=DESC&verify=All" title="Refresh" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i>
                          </a>
                          <?php }else{ ?>
                           <a href="<?php echo current_url(); ?>" title="Refresh" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i>
                          </a>
                          <?php } ?>
                        </form>
                        
                        </div>

          <!--===============category table=================-->

          <?php $status_array = array('0'=>'Pending','1'=>'Active','2'=>'Deactive','3'=>'Banned','4'=>'Draft');?>
           <?php 
           $checkbox_status_array = '';
           $checkbox_status_array = array('4'=>'Pending','1'=>'Active','2'=>'Deactive','3'=>'Banned');
           ?>
                 <header class="panel-heading"  ></header>
                 <form method="post" action="" id="" name="" >
                  <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                      <tr>
                          <th width="110">
                          <div class="checkselect">
                            <div class="checkbox checkboxli">
                             <input type="checkbox" id="select_all" name="select_all"> 
                             <label for="select_all"></label>
                            </div>
                            <select id="moreaction" name="act_type" class="form-control">
                             <option value="">Action</option>
                            <?php 
                            if(!empty($_GET['status']) && $_GET['status'] != 4 && $_GET['status'] != 5)
                            {
                                foreach($checkbox_status_array as $key_val => $checkbox_status)
                                {
                                  if($key_val != $_GET['status'] && $key_val != 4)
                                  { 
                            ?>
                             <option value="<?php echo $key_val;?>"><?php echo $checkbox_status;?></option>
                          <?php   }
                                }
                            }elseif(!empty($_GET['status']) &&  $_GET['status'] == 5){
                                  foreach($checkbox_status_array as $key_val => $checkbox_status)
                                {
                                  if($key_val != 4 )
                                  { 
                              ?>
                              <option value="<?php echo $key_val;?>"><?php echo $checkbox_status;?></option>
                          <?php   }
                                }
                            }elseif(empty($_GET['status']))
                            { 
                              foreach($checkbox_status_array as $key_val => $checkbox_status)
                                {
                                   if($key_val != 4 )
                                  { 
                                  
                          ?>
                                      <option value="<?php echo $key_val;?>"><?php echo $checkbox_status;?></option>
                          <?php } } }?>
                           <option value="5">Delete</option>
                            </select>  
                            <input type="hidden" name="apply" value="" id="apply">
                            <!--<input type="submit" name="apply_new" value="apply" onclick="return conformmessages()"> -->
                            </div>
                          </th>
                          <th width="5%;">Job ID</th>
                          <th width="10%;">User Id</th>
                          <th>Job Title</th>
                          <th >Medical Specialty</th>  
                          <th>Facility Name</th>            
                          <!-- <th width="11%;">Verified/Not Verified</th>  -->
                          <th width="3%;">Status</th>
                          <th width="">Actions</th>                    
                      </tr>
                    </thead>
                    <tbody>
                    <?php
               $i= $offset + 1;
               if($result_data != "")
               {
                $jk=0;
                foreach ($result_data as $res_data)
                {
                  $jk++;
                ?>
                      <tr>
                      <td>
                      <div class="checkboxli">
                      <input type="checkbox" value="<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>" id="check_id<?php echo $i?>" name="check_id[]">
                      <label for="check_id<?php echo $i?>"></label>                      
                      </div>
                      <?php echo $i; ?>.
                      </td>
                       
                        <td> <a href="<?php echo base_url();?>backend/jobs/postjob_detail/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"><?php if(!empty($res_data['id'])) echo '#'.$res_data['id']; ?></a></td>
                         <td><a href="<?php echo base_url();?>backend/listusers/edit_medical/<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>">
                         <?php                          
                          if(!empty($res_data['user_first_name'])) { $user_first_name=$res_data['user_first_name'];}
                          if(!empty($res_data['user_last_name'])) { $user_last_name=$res_data['user_last_name'];}
                          if(!empty($res_data['user_id'])) echo '#'.$res_data['user_id']; ?></a></td>                      
                        <td>   <a href="<?php echo base_url();?>backend/jobs/postjob_detail/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"><?php if(!empty($res_data['listing_title'])) echo ucfirst($res_data['listing_title']);  ?></a>
                            </td>
                         <td><?php echo get_medical_specialty($res_data['medical_specialty_name']); ?></td>
                          <td><?php if(!empty($res_data['facility_name'])) echo ucfirst($res_data['facility_name']);  ?></td>

                        <!--   <td></td> -->
                       
                        <td>
                         <div class="dropdown">
                               <button class="<?php 
                            if($res_data['status'] == 1)
                             echo 'btn btn-success';
                            elseif($res_data['status'] == 2)
                              echo 'btn btn-danger';
                              elseif($res_data['status'] == 3)
                                echo 'btn btn-info';
                               elseif($res_data['status'] == 0)
                                echo 'btn btn-warning';
                              elseif($res_data['status'] == 4)
                                 echo 'btn btn-info';
                              ?> btn-xs  dropdown-toggle_get_val" id="menu<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>" type="button" data-toggle="dropdown"><?php 
                            foreach($status_array as $k => $status_a)
                            {
                               if($k == $res_data['status']) 
                                echo $status_a;
                            }
                            if($res_data['status'] != 4 )
                            {
                             ?>
                        
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>">
                               <?php
                               foreach($status_array as $k => $status_a)
                                {
                                  if($k != $res_data['status'] && $k != 4 && $k != 0)
                                  {
                              ?>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url();?>backend/listusers/change_status_postjob/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>/<?php echo $k; ?>/<?php if(!empty($_GET['status'])) echo $_GET['status'];?>"><?php echo $status_a; ?></a></li>
                              <?php } } ?>
                              
                            </ul>
                            <?php 
                                }
                              ?>
                            </div>
                        </td>
                        
                        
                        <td>
                             <span title="Post Job detail" data-toggle="tooltip"><a  class="btn btn-primary btn-xs tooltips" href="<?php echo site_url();?>backend/jobs/postjob_detail/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"  title="Post Job details"><i class="fa fa-eye" aria-hidden="true"></i>
								              </a>
                          </span>
                         
                          <a href="<?php echo site_url();?>backend/jobs/delete_postjob/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" onclick="return confirm('Are you sure do you want to delete?');" title="Delete Post Job details"><i class="fa fa-trash-o"></i></a>
                        </td> 
                      </tr>

                      <?php $i++;} }else{?>
                        <tr ><td colspan="8" class="error" style="text-align:center; color:red;"><h4>Post Job record not found.</h4></td></tr>
                <?php } ?>
                </tbody>
                
              </table>
              <?php if(!empty($pagination)) echo $pagination;?>
                   </form>
                
               </section>
 
  </div>


<script>

 function conformmessages()
 {
  var checkedValue = document.querySelector('input:checked').value;
       
  var k= ($("#moreaction").val());

  if(k==5 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Delete?");
  }
  else if(k==2 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Deactive?");
  }
  else if(k==1 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Active?");
  }else if(k==4 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Pending?");
  }
  else{
    return false;
  }
  
}


$("#select_all").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
$('#moreaction').on('change', function(e){
    if(conformmessages())
    {
      $("#apply").val(1);
      this.form.submit() 
    }
  });
</script>
<script type="text/javascript" >
 $(document).ready(function(){
  setTimeout(function(){
  $(".flash").fadeOut("slow", function () {
  $(".flash").remove();
      }); }, 5000);
 });
</script>
<script>
$(document).ready(function(){
    $(".dropdown-toggle_get_val").dropdown();
});
</script>

 
  