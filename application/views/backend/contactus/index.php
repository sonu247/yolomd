<div class="bread_parent">
  <ul class="breadcrumb">
       <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
       <li><b>Contact Us</b></li>
  </ul>
  </div>
  
  <div class="clearfix"></div> 
    <section class="panel">
     <div class="col-lg-14"> 
        <form action="<?php echo site_url() ?>backend/contactus/index/0" method="get" accept-charset="utf-8">
          <label class="col-lg-3">
            <input type="text" name="name" value="<?php echo $this->input->get('name'); ?>" class="form-control" placeholder="Search By User Name">
          </label>
           <label class="col-lg-3">
            <input type="text" name="subject" value="<?php echo $this->input->get('subject'); ?>" class="form-control" placeholder="Search By Subject">
          </label>
          <?php
            $ASC= $DESC="";  
            if($this->input->get('order')) {
                if($this->input->get('order')=="DESC"){
                  $DESC='selected';}
                else{
                  $ASC='selected';}
              }
          ?>
          <label>
            <select name="order" class="form-control">
             <option value="DESC" <?php  echo $DESC; ?>>New </option>
              <option value="ASC" <?php  echo $ASC; ?>>Old </option>              
             
            </select>
          </label>
          <label class="search-label">
           
               <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
            <a href="<?php echo current_url() ?>" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i></a>
          </label>
        </form>
      </div>
      <header class="panel-heading"  ></header>
      <table id="datatable_example" class="table table-striped table-hover" >
        <thead class="thead_color">
          <tr>
           <th width="3%">S.No</th>
            <th width="3%">Id</th>
            <th width="15%">User Name</th>
            <th width="15%">Contact No.</th>
            <th width="15%">Subject</th>  
            <th width="20%"><i class="fa fa-calendar"></i>  Created</th>
            <th width="7%">Reply</th>
            <th width="7%">Status</th>
            <th width="5%">Actions</th>
          </tr>
        </thead>
          <?php  
          if(!empty($contacts)):
            $j=$offset+1; 
          foreach($contacts as $row): 
          ?>
            <tbody>
            <tr>
               <td><?php echo $j ?></td>
                <td><a href="<?php echo base_url().'backend/contactus/contactus_reply/'.$row->id ?>"><?php if(!empty($row->id)) echo '#'.$row->id ?></a></td>
                <td><?php if(!empty($row->name)) echo ucfirst($row->name); ?></td>
                <td><?php if(!empty($row->mobile)) echo $row->mobile; ?></td>
                <td><?php if(!empty($row->subject)) echo $row->subject; ?></td>
                <td class="to_hide_phone"><?php echo date('d M Y,h:i  A',strtotime($row->created)); ?></td>
                <td><?php if($row->status==0) { echo "<label class='btn btn-danger btn-xs' style='color:white!important;'> No </label>"; }else{ echo "<label class='btn btn-success btn-xs' style='color:white!important;'> Yes </label>"; }   ?></td>
                <td><?php if($row->read_status==0) { echo "<label class='btn btn-danger btn-xs' style='color:white!important;'> Unread </label>"; }else{ echo "<label class='btn btn-success btn-xs' style='color:white!important;'> Read </label>"; }   ?></td>
                
                <td class="ms">
                    <a href="<?php echo base_url().'backend/contactus/contactus_reply/'.$row->id ?>" class="btn btn-primary btn-xs tooltips" rel="tooltip"  data-placement="left" data-original-title="Reply" ><i class="fa fa-reply" aria-hidden="true"></i></a>
                </td>
              </tr> 
            </tbody> 
          <?php $j++;  endforeach; ?>
        <?php else: ?>
          <tr>
           <th colspan="7" class="msg"> <center>No Contact Found.</center></th>
          </tr>
        <?php endif; ?> 
    </table>
    <?php echo $pagination;?>
    </section>
  
  </div>

<script type="text/javascript" >
 $(document).ready(function(){
  setTimeout(function(){
  $(".flash").fadeOut("slow", function () {
  $(".flash").remove();
      }); }, 5000);
 });
</script>