<div class="bread_parent">
<div class="col-md-12">
  <ul class="breadcrumb">
    
      <?php
      if(superadmin_logged_in()===TRUE)
      {
      ?>
       <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
      <?php } ?>
       <li><a href="<?php echo base_url('backend/contactus');?>"></i>Contact Us</a></li>  

       <li><b>Contact Us Detail</b></li>

  </ul>
</div>

<div class="clearfix"></div>
</div>
<?php 
if($contacts != "")
{
?>
<div class="">
 <div class="row">
                  <div class="col-md-6">
                      <!--user info panel-body table start-->
                      <section class="panel">
                          <div class="">
                              <div class="task-thumb-details col-md-12">
                                
                                      <h1><a href="#">Contact Information</a></h1>
                                  
                                  
                              </div>
                          </div>
                          <table class="table table-hover personal-task">
                              <tbody>
                                <tr>
                                    <td><strong>Name:</strong></td>
                                     
                                    <td class="pull-left"><?php if(isset($contacts[0]->name)){ echo ucfirst($contacts[0]->name);  } ?></td>
                                </tr>
                              
                                <tr>                                    
                                    <td><strong>Email:</strong></td>
                                    <td class="pull-left"><?php if(isset($contacts[0]->email)){ echo $contacts[0]->email;  } ?></td>
                                </tr>
                                <tr>                                    
                                    <td><strong>Contact No.:</strong></td>
                                    <td class="pull-left"><?php if(isset($contacts[0]->mobile)){ echo $contacts[0]->mobile;  } ?></td>
                                </tr>

                                <tr>                                    
                                    <td><strong>Created Time:</strong></td>
                                    <td class="pull-left"> <?php if(isset($contacts[0]->created)){ echo date('d M Y,h:i  A',strtotime($contacts[0]->created ));  } ?></td>
                                </tr>
                                 <tr>                                    
                                    <td><strong>Contact purpose:</strong></td>
                                    <td class="pull-left"><?php if(isset($contacts[0]->subject)){ echo ucfirst($contacts[0]->subject);  } ?></td>
                                </tr>

                                 <tr>                                    
                                    <td><strong>Message:</strong></td>
                                    <td class="pull-left"><div style="text-align:left"><?php if(isset($contacts[0]->message)){ echo ucfirst($contacts[0]->message);  } ?></div></td>
                                </tr>

                                 <tr>                                    
                                    <td><strong>Reply Message:</strong></td>
                                    <td class="pull-left"><div style="text-align:left"><?php 
                                    if(!empty($contacts_msg))
                                    {
                                      foreach($contacts_msg as $contacts_ms)
                                      {
                                        if(!empty($contacts_ms->message))
                                        {
                                        echo $contacts_ms->message.'</br>';
                                    ?>
                                      <?php 
                                        }
                                      }
                                    } ?>
                                    </div></td>
                                </tr>
                               

                              </tbody>
                          </table>
                      </section>
                      <!--user info table end-->
                  </div>
                  <div class="col-md-6">
                      <!--work progress start-->
                      <section class="panel">

                        <div class="">
                              <div class="task-thumb-details">
                                      <h1><a href="#">Reply</a></h1>
                                                                     
                              </div>
                              <hr>
                        </div>

                        <div class="tab-pane row-fluid fade in active">
                          <form role="form" class="form-horizontal tasi-form" action="<?php echo current_url()?>" enctype="multipart/form-data" method="post" id="form_valid">
                            <div class="form-body">

                              <div class="form-group">
                            <label class="col-md-3 control-label">Message <span class="mandatory">*</span></label>
                            <div class="col-md-6">
                            <textarea placeholder="Your Message" name="message"   rows="6" class="form-control" value=""></textarea>
                             <?php echo form_error('message'); ?>
                            </div>
                          </div>

                          

                            <div class="form-actions fluid">
                              <div class="col-md-offset-2 col-md-10">
                                <button  class="btn btn-info tooltips" rel="tooltip" data-placement="top" data-original-title="Reply" type="submit"> <i class="fa fa-reply"></i> Reply</button>
                                </div>
                              </div>
                          </form>
                        </div> 
                          
                         
                      </section>
                      <!--work progress end-->
                  </div>
              </div>


           
</div>
<?php }else{?>
<div class="">
 <div class="row">
                  <div class="col-md-6">
                      <!--user info panel-body table start-->
                      <section class="panel">
                          <div class="">
                              <div class="task-thumb-details col-md-12">
                                
                                      <h1 style="color:red;">Invalid contactus reply details.<a href="<?php echo site_url();?>backend/contactus"> "Support"</a></h1>
                                  
                                  
                              </div>
                          </div>
                       </section>
                  </div>
</div>
</div>                           
<?php } ?>

