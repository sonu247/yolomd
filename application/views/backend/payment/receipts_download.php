<div class="">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"> Dashboard  </a></li>
    <li><a href="<?php echo base_url('backend/payment_receipts/payment_list');?>">  Payment Receipts </a></li>
    <li class="">
    Payment Receipt</li>
           
</ul>
</div>
         <header class="panel-heading"  >Payment Receipt- <?php if(!empty($jobs_detail['job_title'])) echo $jobs_detail['job_title'];  ?>
         
         </header>
        

     <table class="responsive table table-striped" >
        <tbody>
          <tr >
          <td width="300">User Detail : </td>
          <td >  
          <a href="<?php echo site_url();?>backend/listusers/edit_medical/<?php if(!empty($jobs_detail['user_id'])) echo $jobs_detail['user_id'];  ?>">
            <?php 
             
             
            if(!empty($jobs_detail['user_id'])) echo '#'.$jobs_detail['user_id'];  ?> |

             <?php 
             if(!empty($jobs_detail['user_id']))
      		{
        		$user_info = '';
        		$user_info = get_user_info($jobs_detail['user_id']);
        		echo $user_info->user_name_title.' '.$user_info->user_first_name.' '.$user_info->user_last_name;
      		}
             ?>
          </a>
          </td>
          </tr>
          <tr  >
          <tr >
          <td width="300">Transaction Id : </td>
          <td >  
            <?php if(!empty($jobs_detail['transaction_id'])) echo $jobs_detail['transaction_id'];  ?>
          </td>
          </tr>
          <tr  >
          <td >Pay Date : </td>
          <td >  
              
               <?php if(!empty($jobs_detail['date'])) echo $jobs_detail['date'];  ?>
          </td>
          </tr>
          <tr>
          <td >Job title : </td>
          <td >  
          <a href="<?php echo site_url();?>backend/jobs/postjob_detail/<?php if(!empty($jobs_detail['job_id'])) echo $jobs_detail['job_id'];  ?>">
            <?php if(!empty($jobs_detail['job_title'])) echo $jobs_detail['job_title'];  ?></a>
          </td>
          </tr>
          <tr >
          <td >Posted Job Id : </td>
          <td >  
              <a href="<?php echo site_url();?>backend/jobs/postjob_detail/<?php if(!empty($jobs_detail['job_id'])) echo $jobs_detail['job_id'];  ?>"><?php if(!empty($jobs_detail['job_id'])) echo '#'.$jobs_detail['job_id'];  ?></a>
          </td>
          </tr>
          <tr >
          <td >YoloMD Verify : </td>
          <td >  
             <?php if(!empty($jobs_detail['verify'])) echo $jobs_detail['verify'];  ?>
          </td>
          </tr>
          <tr>
          <td >Services : </td>
          <td >  
            <?php if(!empty($jobs_detail['item'])) echo $jobs_detail['item'];  ?>
          </td>
          </tr>
          
          <?php 
          if(!empty($city_detail))
          {
            foreach($city_detail as $city_d){ ?>
          <tr>
              <td >
                
                  <?php if(!empty($city_d['city_name'])) echo $city_d['city_name'];  ?>
              </td>
              <td  >  
               
                  <?php if(!empty($city_d['city_charge'])) echo '+$'.number_format($city_d['city_charge'],2);  ?>
                     <?php 
                        if(!empty($city_d['valid_date']) && $city_d['valid_date'] != '0000-00-00') 
                        {  
                         
                       
                          if(strtotime($city_d['valid_date']) == strtotime(date('Y-m-d')))
                          {
                          ?>
                            <span >( Active Untill : <?php echo date('d M y', strtotime($city_d['valid_date']));

                         ?>)</span>
                            
                          <?php 
                          }elseif(strtotime($city_d['valid_date']) < strtotime(date('Y-m-d')))
                          {
                            ?>
                          <span > (Expired)</span>

                          <?php 
                          }else
                          {
                            ?>
                          <span >( Active Untill : <?php echo date('d M y', strtotime($city_d['valid_date']));

                         ?>)</span>
                        <?php } 
                       }else{
                        ?>
                       <span >(NA - Not Active)</span>
                      <?php } ?>
              </td>
          </tr>    
          <?php }} ?>
          
           
          <tr>
          <td >Verify Charged : </td>
          <td >  
             
               <?php if(!empty($jobs_detail['yolomd_verify'])) echo '+'.$jobs_detail['yolomd_verify'];  ?>
          </td>
          </tr>
          <tr >
          <td >Payment Amount : </td>
          <td > 
              <?php if(!empty($jobs_detail['payment_amount'])) echo '$'.$jobs_detail['payment_amount'];  ?>
          </td>
          </tr>
          
          <tr >
          <td >Paid Amount : </td>
          <td> 
              <b><?php if(!empty($jobs_detail['payment_amount'])) echo '$'.$jobs_detail['payment_amount'] ;  ?></b>
          </td>
          </tr>
          </tbody>
     </table>
    
