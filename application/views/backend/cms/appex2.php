<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li class=""><i class="fa fa-plus-square" aria-hidden="true"></i> Medical Specialties List  </li>
           
</ul>
</div>
<div clss="row">
            <div class="col-lg-14">
            <section class="panel">
              <div  class="pull-right" > 
              
              
                  <button type="button" style="padding: 0px 11px;" class="btn_tool btn btn-primary btn-xs tooltips" data-toggle="collapse" data-target="#demo"><h5>Add Medical Specialty  
                   <span class="fa fa-caret-down"></h5></span> 
                  </button> 
                 </div>
                
              <!--   <div   id="demo" class="collapse" > -->
                  <div class="panel collapse <?php if(!empty($_POST['add'])) echo 'in'; else ''; ?> toogle_class" id="demo" >
                 
                    <div class="col-lg-12"> 
                        <form class="form-horizontal tasi-form" action="<?php echo base_url('backend/cms/appex2');?>" name="add_appex1" id="add_appex1" method="post" enctype= "multipart/form-data">
                        <div class="col-xs-4">
                         <!--  <label class="col-sm-2 col-sm-1">Title<span class="mandatory">*</span></label>  -->
                       <!--   <div class="col-sm-10"> -->

                          <input type="text" id="appex2" name="appex2_name" class="form-control" placeholder="Medical Specialty Name" value="<?php echo set_value('appex2_name'); ?>">
                          <div class="left_move">
                           <?php echo form_error('appex2_name'); ?>
                           </div>
                          <!--  </div> -->
                          
                        </div>
                      
                        <div class="col-xs-2">
                         
                          <!-- radio -->
                            
                             <div class="col-sm-10" style="float:left;">
                                <select class="form-control" name="status">
                                    <option value="">Select Status</option>
                                <option value="1" <?php echo set_select('status', '1'); ?>>Active</option>
                                <option value="0" <?php echo set_select('status', '0'); ?>>Deactive</option>
                                </select>
                                <div class="left_move">
                              <?php echo form_error('status'); ?>
                              </div>
                               </div>
                            
                           <!-- end radio -->
                        </div>
                        <div class="col-xs-2">
                          <!--  <label class="col-sm-2 col-sm-1">Sequence<span class="mandatory">*</span></label>  -->
                    <!--       <div class="col-sm-10"> -->
                          <input type="text" class="form-control" name="order_by" value="<?php echo set_value('order_by'); ?>" placeholder="Sequence">
                          <div class="left_move">
                           <?php echo form_error('order_by'); ?>
                           </div>
                          <!--  </div> -->
                       </div>
                       <div class="col-xs-2">
                          <input type="file" name="logo">
                               <div style="margin-top:5px;">
                             <?php 
                              if(form_error('logo'))
                              {
                                echo form_error('logo'); 
                              }else{
                              ?>
                              Choose logo needs to be 30 x 30 pixel.
                              <?php } ?>
                              </div>
                       </div>
                        <div class="col-xs-2 pull-right">
                       
                          <input type="submit" class="btn btn-block btn-primary" value="Add Medical Specialty" name="add">
                        </div>
                        </form>
                        </div>

                 </div><!-- /.box-body -->
            <!--<div class="box-footer">
              Footer
            </div>--><!-- /.box-footer-->
            </section>
           </div>
        </div>


    
      
         
                  


<div class="panel">

                  <header class="panel-heading heading_class"><i class="fa fa-plus-square" aria-hidden="true"></i> Medical Specialties List </header>
                 
                  <table id="example1"class="table table-striped table-hover">
                    <thead class="thead_color">
                      <tr>
                       <th width="5%;">S.No</th>
                        <th width="10%;">ID</th>
                        <th width="40%;">Medical Specialty Title</th>
                         <th width="15%;">Logo</th>
                        <th width="10%;">Sequence</th>
                        <th width="10%;">Status</th>
                        <th width="">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($appex2)){ 
                      $i = 1;
                      foreach ($appex2 as $value) { 
                      ?>
                      <tr>
                       <td><?php echo $i; ?></td>
                        <td><?php echo '#'.$value->id; ?></td>
                        <td>
                          <?php echo $value->name; ?> 
                          &nbsp;
                          
                        </td>
                        <td>
                         <img class="img_class" style="width:25px;" src="<?php echo UPLOAD_PATH;?>/logo/<?php if(!empty($value->logo)) echo $value->logo; else echo 'no-preview.jpg'; ?>">
                         </td>
                        <td> <?php echo $value->order_by; ?> </td>
                        <td><?php if($value->status == 1) {?>
                           <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $value->id;?>','medical_specialties_annex1')">Active</a>
                        <?php } if($value->status == 0) {?>
                          <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $value->id;?>','medical_specialties_annex2')">Deactive</a>
                        <?php } ?>
                        </td> 
                        <td>
                          <span title="Edit Medical Specialty" data-toggle="tooltip"><a href="javascript:void(0)"  data-toggle="modal" class="btn btn-primary btn-xs tooltips" data-target="#myMenuModal<?php echo  $value->id;?>" title="Edit Medical Specialty"><i class="fa fa-pencil-square-o"></i></a>
                          </span>
                           &nbsp;&nbsp;
                          <a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" onclick="delete_data('<?php echo  $value->id;?>','medical_specialties_annex2');" title="Delete Medical Specialty"><i class="fa fa-trash-o"></i></a>
                          <!-- ===== Category Model===== -->
             <div class="modal" id="myMenuModal<?php echo  $value->id;?>">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                  Update Medical Specialty   
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="menuheader"></h4>
                  </div>
                  <form action="" name="edit_menu" id="edit_menu" method="post"  enctype= "multipart/form-data">
                  <div class="modal-body">
                        <input type="hidden" name="m_id" id="m_id" value="<?php if(!empty($value->id))echo  $value->id; else echo set_value('m_id');?>" />
                        <div class="col-xs-6">
                          <label>Title<span class="mandatory">*</span></label> 
                          <input type="text" name="e_appex2_name"  value="<?php if(!empty($value->name))echo  $value->name; else echo set_value('e_appex2_name');?>" class="form-control" placeholder="Medical Specialty Title" required>
                             <?php echo form_error('e_appex2_name'); ?>
                        </div>
                        <div class="col-xs-2">
                          <label>Sequence<span class="mandatory">*</span></label> 
                          <input type="text" class="form-control" name="e_a_sequence" id="m_seq" placeholder="Sequence" value="<?php if(!empty($value->order_by))echo  $value->order_by; else echo set_value('a_sequence');?>">
                          
                             <?php echo form_error('e_a_sequence'); ?>
                            
                        </div>
                        
                        <div class="col-xs-4">
                          <label>Status<span class="mandatory">*</span></label> 
                          <!-- radio -->
                            <div class="form-group">
                               <select class="form-control" name="e_status" id="status">
                                  <option value="1" <?php if($value->status==1) echo 'selected'; else echo '';?>>Active</option>
                                  <option value="0"<?php if($value->status==0) echo  'selected'; else echo '';?>>Deactive</option>
                          </select>

                                 <?php echo form_error('e_status'); ?>
                             </div> 
                           <!-- end radio -->
                        </div>
                         <div class="col-xs-6">
                          <input type="file" name="e_logo">


                               <div style="margin-top:5px;">
                             <?php 
                              if(form_error('e_logo'))
                              {
                                echo form_error('e_logo'); 
                              }else{
                              ?>
                              Choose logo needs to be 30 x 30 pixel.
                              <?php } ?>
                              </div>
                              <img class="img_class" style="width:25px;" src="<?php echo UPLOAD_PATH;?>/logo/<?php if(!empty($value->logo)) echo $value->logo; else echo 'no-preview.jpg'; ?>">
                       </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Medical Specialty" name="update">
                  </div>
                  </form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <?php
            if(!empty($main_id) && $main_id == $value->id)
            {
               if(form_error('e_appex2_name') || form_error('e_status') || form_error('e_a_sequence') || form_error('e_logo'))
               {
            ?>
            <script type="text/javascript">
             
                $(window).load(function(){
                 
                     $('#myMenuModal<?php if(!empty($main_id)) echo $main_id; ?>').modal('show');
                  });
            </script>
            <?php } 
            }?>
                        </td>
                      </tr>
                      <?php $i++; }                        
                      } ?>
                    </tbody>
                   
                  </table>
                  </div>
               </section>
  </div>
  </div>

<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Medical Specialty<span class="fa fa-caret-down"></span></h5>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Medical Specialty <span class="fa fa-caret-up"></span></h5>');
  });
});
</script>
<script>
function delete_data(id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/common_delete/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to delete Add Medical Specialty detail."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/cms/appex2";
    });


  }
 
}
function change_status(val,id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/common_change_status/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to change status."))
  {
    $.post(url,{change_status:val}, function(data){
    window.location.href="<?php echo site_url();?>backend/cms/appex2";
    });


  }
}
</script>
