<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li class=""><i class="fa fa-building-o" aria-hidden="true"></i>
 Add City Manage</li>
           
</ul>
</div>
       

<div class="panel">

     
          <!--===============category table=================-->

                  <?php if(form_error('edit_name[]') || form_error('edit_price[]') || form_error('edit_order[]')){?>
                  <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>    
                  <?php echo form_error('edit_name[]'); ?> 
                   <?php echo form_error('edit_price[]'); ?> 
                  <?php echo form_error('edit_order[]'); ?> 
                  </div>
                <?php } ?>
                 
                 <header class="panel-heading"><i class="fa fa-building-o" aria-hidden="true"></i>
                   City List</header>
                  <form  action="" method="post">
                 
                   <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                      <tr>
                        <th width="200px;">City Name</th>
                        <th width="550px;">Price</th>
                        <th width="550px;">Status</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($city_manage)){ 
                      $i = 1;
                      foreach ($city_manage as $value) { 
                      ?>
                      <tr>
                        <td>
                        <input type="hidden" class="form-control" value="<?php echo $value->id; ?>" 
                          name="main_id[]"> 
                          <input type="text" class="form-control" value="<?php echo $value->city_name; ?>" 
                          name="edit_name[]"> 
                          &nbsp;
                          
                        </td>
                        <td>
                          <input type="text" class="form-control" value="<?php echo $value->price; ?>" name="edit_price[]"> 
                         
                        </td>
                        
      
                       
                        <td><?php if($value->status == 1) {?>
                           <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $value->id;?>','location_data')">Active</a>
                        <?php } if($value->status == 0) {?>
                          <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $value->id;?>','location_data')">Deactive</a>
                        <?php } ?>
                        </td> 
                        
                      </tr>
                      <?php $i++; }                        
                      } ?>
                      <tr>
                      <tr>
                        <td>
                        <input type="hidden" class="form-control" value="other_id" 
                          name="other_id"> 
                          <input type="text" class="form-control" value="Other" 
                          name="other_city" disabled> 
                          &nbsp;
                          
                        </td>
                        <td>
                          <input type="text" class="form-control" value="<?php if(!empty($other_manage->price)) echo $other_manage->price; ?>" name="other_price"> 
                         
                        </td>
                        
      
                       
                        <td><?php 
                       
                        if($other_manage->status == 1) {?>
                           <a href="<?php echo site_url();?>backend/cms/change_other_city_status/0" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" >Active</a>
                        <?php } if($other_manage->status == 0) {?>
                          <a href="<?php echo site_url();?>backend/cms/change_other_city_status/1" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  >Deactive</a>
                        <?php } ?>
                        </td> 
                        
                      </tr>
                     
                      <tr>
                        <td colspan="6"> 
                        <div class="col-xs-2 pull-right">
                        <input type="submit" name="update" Value="Update city" class="btn btn-block btn-primary">
                        </div>
                        </td>
                      </tr>
                    </tbody>
                   
                  </table>
                 
                  </div>
                
               </section>
  </div>
  </div>

<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add city <span class="fa fa-caret-down"></span></h5>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add city <span class="fa fa-caret-up"></span></h5>');
  });
});
</script>
<script>
function delete_data(id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/common_delete/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to delete city."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/cms/city_manage";
    });


  }
 
}
function change_status(val,id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/common_change_status/"+id+"/"+table;

  if(confirm("Are you sure. Do you want to change status."))
  {
    $.post(url,{change_status:val}, function(data){
    window.location.href="<?php echo site_url();?>backend/cms/city_manage";
    });


  }
}
</script>
