
<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li><a href="<?php echo base_url('backend/cms/home_page/1');?>"><i class="fa fa-book" aria-hidden="true"></i> Banner </a></li>
    <li><i class="fa fa-map-o" aria-hidden="true"></i> <?php if(!empty($title)) echo $title; ?></li>
           
</ul>
</div>
<div class="row">
     <div class="col-lg-14">
        <section class="panel">
          <header class="panel-heading heading_class"><i class="fa fa-map-o" aria-hidden="true"></i> <?php if(!empty($title)) echo $title; ?></header>
            <form  class="form-horizontal tasi-form" role="form" method="post" action="" enctype= "multipart/form-data">
              <div class="panel-body">
        
              <div class="form-group">
                <label class="col-sm-2 col-sm-2">Title<!-- <span class="mandatory">*</span> --></label>
                 <div class="col-sm-10">
                 <input  placeholder="Title" class="form-control" name="banner_title" value="<?php if(!empty($banner_details->cms_title)) echo $banner_details->cms_title; else echo set_value('banner_title'); ?>">
                 <div class="left_move">
               <!--  <?php //echo form_error('banner_title'); ?> -->
                </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 col-sm-2">Sub-title<!-- <span class="mandatory">*</span> --></label>
                 <div class="col-sm-10">
                 <input placeholder="Sub-title" class="form-control" name="banner_subtitle" value="<?php if(!empty($banner_details->cms_sub_title)) echo $banner_details->cms_sub_title; else echo set_value('banner_subtitle'); ?>">
                 <div class="left_move">
               <!--  <?php //echo form_error('banner_subtitle'); ?> -->
                </div>
                </div>
              </div>
               <div class="form-group">
                      <label class="col-sm-2 col-sm-2">Button Label<!-- <span class="mandatory">*</span> --> </label>
                       <div class="col-sm-10">
                      <input type="text" placeholder="Button Label" class="form-control" name="banner_label" value="<?php if(!empty($banner_details->label)) echo $banner_details->label; else echo set_value('banner_label'); ?>">
                      <div class="left_move">
                      <!--  <?php //echo form_error('banner_label'); ?> -->
                      </div>
                      </div>
                    </div>
              <div class="form-group">
                <label class="col-sm-2 col-sm-2">Button to redirect<!-- <span class="mandatory">*</span> --></label>
                 <div class="col-sm-10">
                  <div class="input-group">
                          <span class="input-group-addon" id="addon"><?php echo base_url(); ?></span><input type="text" name="banner_link" class="form-control url_addlink" placeholder="Button Link" value="<?php if(!empty($banner_details->cms_button_link)) echo $banner_details->cms_button_link; else echo set_value('banner_link'); ?>">
                          </div>
                 
                <div class="left_move">
                <div style="margin-top:10px;">
                Example:- (test_controller/test_method)   
                </div>
                 <div class="left_move">
              <!--   <?php// echo form_error('banner_link'); ?> -->
                </div>
                </div>
                </div>
               
              </div>

             <!--  <div class="form-group">
               <label class="col-sm-2 col-sm-1">Order<span class="mandatory">*</span></label>
                <div class="col-sm-10">
               <input name="order_by" value="<?php //if(!empty($banner_details->order_by)) echo $banner_details->order_by; else echo set_value('order_by'); ?>" class="form-control" type="text" placeholder="Order">
                <?php //echo form_error('order_by', '<div class="error">', '</div>'); ?>
                </div>
                </div> -->

              <div class="form-group">
               <label class="col-sm-2 col-sm-2">Banner Image<span class="mandatory">*</span></label>
                <div class="col-sm-10">
                <input type="file" name="banner_image">
                <div class="left_move">
                <div>

                <?php 
                if(form_error('banner_image'))
                {
                  echo form_error('banner_image'); 
                }else{
                ?>
                Choose Banner Image  1600 X 600 pixel.
                <?php } ?>
                </div>
                 <div class="left_move">
                <?php if($this->uri->segment(4)){ ?>
                <img class="img_class" style="margin-top:10px;" src="<?php echo UPLOAD_PATH ;?>banner/<?php if(!empty($banner_details->cms_image)) echo $banner_details->cms_image; else echo 'default.png'; ?>">
                </div>
                <?php } ?>
                </div>
                </div>
              </div>
              <input type="hidden" name="banner" value="1">
              <div class="form-group">
              <label class="col-sm-2 col-sm-2"></label>
               <div class="col-sm-10">
                <button class="btn btn-primary" type="submit"><?php if(!empty($button_title)) echo $button_title; ?></button> 
                </div>   
              </div>    
          </div>
      </form>
  </section>
  </div>
  </div>