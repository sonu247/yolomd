<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li class=""><i class="fa fa-stethoscope" aria-hidden="true"></i> Add Doctor and Medical Content</li>
           
</ul>
</div>
<div clss="row">
            <div class="col-lg-14">
            <section class="panel">
              <div  class="pull-right" >
              
              
                   <button type="button" style="padding: 0px 11px;" class="btn_tool btn btn-primary btn-xs tooltips" data-toggle="collapse" data-target="#demo"><h5>Add Doctor and Medical Content
                  <span class="fa fa-caret-down"></span></h5> 
                  </button> 
                 </div>
                
              <!--   <div   id="demo" class="collapse" > -->
                  <div class="panel collapse <?php if(!empty($_POST['add'])) echo 'in'; else ''; ?> toogle_class" id="demo" >
                 
                    <div class="col-lg-12"> 
                        <form class="form-horizontal" action="" name="add_appex1" id="add_appex1" method="post">
                        <div class="col-xs-2">
                         <!--  <label class="col-sm-2 col-sm-1">Title<span class="mandatory">*</span></label>  -->
                       <!--   <div class="col-sm-10"> -->

                          <input type="text" id="title" name="title" class="form-control" placeholder="Title" value="<?php echo set_value('title'); ?>">
                          <div class="left_move">
                           <?php echo form_error('title'); ?>
                           </div>
                          <!--  </div> -->
                          
                        </div>
                        <div class="col-xs-2">
                         <!--  <label class="col-sm-2 col-sm-1">Title<span class="mandatory">*</span></label>  -->
                       <!--   <div class="col-sm-10"> -->

                          <input type="text" id="description" name="description" class="form-control" placeholder="Description" value="<?php echo set_value('description'); ?>">
                          <div class="left_move">
                           <?php echo form_error('description'); ?>
                           </div>
                          <!--  </div> -->
                          
                        </div>
                      <div class="col-xs-2">
                       
                       
                        <select class="form-control" name="type">
                        <option value="">Select Type</option>
                                <option value="1" <?php echo set_select('type', '1'); ?>>Doctor</option>
                                <option value="2" <?php echo set_select('type', '2'); ?>>Medical</option>
                        </select>
                        <?php echo form_error('type'); ?>
                        </div>
                        <div class="col-xs-2">
                       
                       
                        <select class="form-control" name="status">
                        <option value="">Status</option>
                                <option value="1" <?php echo set_select('status', '1'); ?>>Active</option>
                                <option value="0" <?php echo set_select('status', '0'); ?>>Deactive</option>
                        </select>
                        <?php echo form_error('status'); ?>
                        </div>
                        <div class="col-xs-2">
                          <!--  <label class="col-sm-2 col-sm-1">Sequence<span class="mandatory">*</span></label>  -->
                    <!--       <div class="col-sm-10"> -->
                          <input type="text" class="form-control" name="order_by" value="<?php echo set_value('order_by'); ?>" placeholder="Sequence">
                           <?php echo form_error('order_by'); ?>
                          <!--  </div> -->
                            
                       </div>

                        <div class="col-xs-2 pull-right">
                       
                          <input type="submit" class="btn btn-block btn-primary" value="Add Content" name="add">
                        </div>
                        </form>
                        </div>

                 </div><!-- /.box-body -->
            <!--<div class="box-footer">
              Footer
            </div>--><!-- /.box-footer-->
            </section>
           </div>
        </div>


    
      
         
                  

<div class="panel">

     
          <!--===============category table=================-->

                  <?php if(form_error('edit_title[]') || form_error('edit_description[]') || form_error('edit_order[]')){?>
                  <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>    
                  <?php echo form_error('edit_title[]'); ?> 
                   <?php echo form_error('edit_description[]'); ?> 
                  <?php echo form_error('edit_order[]'); ?> 
                  </div>
                <?php } ?>
                 
                 <header class="panel-heading heading_class"><i class="fa fa-stethoscope" aria-hidden="true"></i> Doctor and Medical Content List</header>
                  <form  action="" method="post">
                 
                   <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                      <tr>
                        <th width="200px;">Title</th>
                        <th width="550px;">Description</th>
                        <th  width="100px;">Sequence</th>
                         <th width="150px;">Type</th>
                        <th  width="100px;">Status</th>
                        <th width="">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($sign_up_content)){ 
                      $i = 1;
                      foreach ($sign_up_content as $value) { 
                      ?>
                      <tr>
                        <td>
                        <input type="hidden" class="form-control" value="<?php echo $value->id; ?>" 
                          name="main_id[]"> 
                          <input type="text" class="form-control"value="<?php echo $value->title; ?>" 
                          name="edit_title[]"> 
                          &nbsp;
                          
                        </td>
                        <td>
                          <input type="text" class="form-control" value="<?php echo $value->description; ?>" name="edit_description[]"> 
                         
                        </td>
                        
                        <td> <input type="text" class="form-control" value="<?php echo $value->order_by; ?>" id="edit_order[]" name="edit_order[]"> </td>
                        <td>
                          <?php if($value->type == 1) {?>
                           Doctor
                        <?php } if($value->type == 2) {?>
                         Medical
                        <?php } ?>
                        </td>
                        <td><?php if($value->status == 1) {?>
                           <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $value->id;?>','physician_medical_facility_content')">Active</a>
                        <?php } if($value->status == 0) {?>
                          <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $value->id;?>','physician_medical_facility_content')">Deactive</a>
                        <?php } ?>
                        </td> 
                        <td>
                         
                          <a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" onclick="delete_data('<?php echo  $value->id;?>','physician_medical_facility_content');" title="Delete Content"><i class="fa fa-trash-o"></i></a>
                          <!-- ===== Category Model===== -->
           

                           </td>
                      </tr>
                      <?php $i++; }                        
                      } ?>
                      <tr>
                        <td colspan="6"> 
                        <div class="col-xs-3 pull-right">
                        <input type="submit" name="update" Value="Update Doctor and Medical Content" class="btn btn-block btn-primary">
                        </div>
                        </td>
                      </tr>
                    </tbody>
                   
                  </table>
                 
                  </div>
                
               </section>
  </div>
  </div>

<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Doctor and Medical Content <span class="fa fa-caret-down"></span></h5>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Doctor and Medical Content <span class="fa fa-caret-up"></span></h5>');
  });
});
</script>
<script>
function delete_data(id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/common_delete/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to delete Doctor and Medical Content."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/cms/signup_content";
    });


  }
 
}
function change_status(val,id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/common_change_status/"+id+"/"+table;

  if(confirm("Are you sure. Do you want to change status."))
  {
    $.post(url,{change_status:val}, function(data){
    window.location.href="<?php echo site_url();?>backend/cms/signup_content";
    });


  }
}
</script>
