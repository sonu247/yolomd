
<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li><a href="<?php echo base_url('backend/cms/home_page/3');?>"><i class="fa fa-search" aria-hidden="true"></i> Search by City </a></li>
    <li><i class="fa fa-building-o" aria-hidden="true"></i> <?php if(!empty($title)) echo $title; ?></li>
           
</ul>
</div>
 <div class="row">
     <div class="col-lg-14">
        <section class="panel">
        
          <header class="panel-heading heading_class"><i class="fa fa-building-o" aria-hidden="true"></i> <?php if(!empty($title)) echo $title; ?></header>

            <form class="form-horizontal tasi-form" role="form" method="post" action="" enctype= "multipart/form-data">       
            <div class="panel-body">

                <div class="form-group">
                  <label class="col-sm-2 col-sm-2">City Name<span class="mandatory">*</span></label>
                  <div class="col-sm-10">
                  <input type="text" name="city_name" placeholder="City Name" class="form-control" value="<?php if(!empty($city_details->search_city_name)) echo $city_details->search_city_name; else echo set_value('city_name'); ?>">
                  <div class="left_move">
                  <?php echo form_error('city_name'); ?>
                  </div>
                  </div>

                </div>

              <div class="form-group">
               <label class="col-sm-2 col-sm-2">Order<span class="mandatory">*</span></label>
               <div class="col-sm-10">
               <input name="order_by" value="<?php if(!empty($city_details->order_by)) echo $city_details->order_by; else echo set_value('order_by'); ?>" class="form-control" type="text" placeholder="Order">
                <div class="left_move">
                <?php echo form_error('order_by', '<div class="error">', '</div>'); ?>
                </div>
                </div>
                </div>

              <div class="form-group">
                <label class="col-sm-2 col-sm-2">Image<span class="mandatory">*</span></label>
                <div class="col-sm-10">
                <input type="file" name="city_image">
                 <div class="left_move">
                 <div style="margin-top:5px;">
               <?php 
                if(form_error('city_image'))
                {
                  echo form_error('city_image'); 
                }else{
                ?>
                Choose City Image  atleast 350 x 380 pixel and maximum size is 1050 x 1140.
                <?php } ?>
                </div>
                <div style="float:left">
                <?php if($this->uri->segment(4)){ ?>
                <img class="img_class" style="margin-top:10px;" src="<?php echo UPLOAD_PATH ;?>city/<?php if(!empty($city_details->search_city_image)) echo $city_details->search_city_image; else echo 'default.png'; ?>">
                </div>
                <?php } ?>
                </div>
                </div>
              </div>
              <input type="hidden" name="city" value="1">
              <div class="form-group">
              <label class="col-sm-2 col-sm-1"></label>
              <div class="col-sm-10">
                <button class="btn btn-primary" type="submit"><?php if(!empty($button_title)) echo $button_title; ?></button> 
                </div>   
              </div>    
            </div>
      </form>
      </section>
  </div>
  </div>



