<div class="">
  <ul class="breadcrumb" >
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li class=""><i class="fa fa-list"></i> Menu</li>
    
  </ul>
</div>
<!-- <div clss="row">
  <div class="col-lg-14">
    <section class="panel">
      <div  class="pull-right" >
        
        
        <button type="button" style="padding: 0px 11px;" class="btn_tool btn btn-primary" data-toggle="collapse" data-target="#demo"><h5>Add Menu
        <span class="fa fa-caret-down"></span> </h5>
        </button>
      </div>
      
      
      <div class="panel collapse <?php //if(!empty($_POST['add'])) echo 'in'; else ''; ?> toogle_class" id="demo" >
        
        <div class="col-lg-12">
          <form class="form-horizontal tasi-form" action="<?php //echo base_url('backend/cms/footer_menu');?>" name="add_menu" id="add_menu" method="post">
            <div class="col-xs-4">
              
              <input type="text" id="menu_name" name="menu_name" class="form-control" placeholder="Menu Title" value="<?php //echo set_value('menu_name'); ?>">
              <div class="left_move">
                <?php //echo form_error('menu_name'); ?>
              </div>
              
              
            </div>
            
            <div class="col-xs-3">
              
              
              <div class="col-sm-10" style="float:left;">
                <select class="form-control" name="status">
                  <option value="">Select Status</option>
                  <option value="1" <?php// echo set_select('status', '1'); ?>>Active</option>
                  <option value="0" <?php //echo set_select('status', '0'); ?>>Deactive</option>
                </select>
                <div class="left_move">
                  <?php //echo form_error('status'); ?>
                </div>
              </div>
              
              
            </div>
            <div class="col-xs-3">
              
              <input type="text" class="form-control" name="order_by" value="<?php// echo set_value('order_by'); ?>" placeholder="Sequence">
              <div class="left_move">
                <?php //echo form_error('order_by'); ?>
              </div>
              
            </div>
            <div class="col-xs-2 pull-right">
              
              <input type="submit" class="btn btn-block btn-primary" value="Add Menu" name="add">
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>-->
<div class="">
  <!--===============category table=================-->
  
  
  <header class="panel-heading heading_class"><i class="fa fa-list"></i> Footer Menu</header>

  <table id="example1"class="table table-striped table-hover" >
    <thead class="thead_color">
      <tr>
        <th width="50px;">S.No</th>
        <th width="100px;">ID</th>
        <th width="200px;">Menu Title</th>
        <th width="300px;">Submenu</th>
        <th width="200px;">Sequence</th>
        <th width="150px;">Status</th>
        <th width="">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php if(!empty($menu)){
      $i = 1;
      foreach ($menu as $value) { ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo '#'.$value->id; ?></td>
        <td>
          <?php echo $value->name; ?>
        </td>
        <td>
          <a  data-toggle="tooltip" href="<?php echo base_url()?>backend/cms/sub_menu/<?php echo $value->id; ?>" title="Add SubMenu" class="btn btn-primary btn-xs ">Add Submenu</a>
        </td>
        <td> <?php echo $value->order_by; ?> </td>
        <td>
          <?php if($value->status == 1) { ?>
          <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $value->id;?>','cms_menu')">Active</a>
          <?php } if($value->status == 0) { ?>
          <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $value->id;?>','cms_menu')">Deactive</a>
          <?php } ?>
        </td>
        <td>
          <span title="Edit Menu" data-toggle="tooltip"><a href="javascript:void(0)"  data-toggle="modal" class="btn btn-primary btn-xs tooltips" data-target="#myMenuModal<?php echo  $value->id;?>" title="Edit Menu"><i class="fa fa-pencil-square-o"></i></a>
          </span>
          &nbsp;&nbsp;
          <!--  <a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" onclick="delete_data('<?php //echo  $value->id;?>','cms_menu');" title="Delete Menu"><i class="fa fa-trash-o"></i></a> -->
          <!-- ===== Category Model===== -->
          <div class="modal" id="myMenuModal<?php echo  $value->id;?>">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  Update Menu
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="menuheader"></h4>
                </div>
                <form action="<?php echo base_url();?>backend/cms/footer_menu" name="edit_menu" id="edit_menu" method="post">
                  <div class="modal-body">
                    <input type="hidden" name="m_id" id="m_id" value="<?php if(!empty($value->id))echo  $value->id; else echo set_value('m_id');?>" />
                    <div class="col-xs-4">
                      <label>Menu Title<span class="mandatory">*</span></label>
                      <input type="text" name="e_menu_name" id="m_name" value="<?php if(!empty($value->name))echo  $value->name; else echo set_value('m_name');?>" class="form-control" placeholder="Menu Title" required>
                      <?php echo form_error('e_menu_name'); ?>
                    </div>
                    <div class="col-xs-4">
                      <label>Menu Sequence<span class="mandatory">*</span></label>
                      <input type="text" class="form-control" name="e_m_sequence" id="m_seq" placeholder="Sequence" value="<?php if(!empty($value->order_by))echo  $value->order_by; else echo set_value('m_sequence');?>">
                      <?php echo form_error('e_m_sequence'); ?>
                    </div>
                    <div class="col-xs-4">
                      <label>Status<span class="mandatory">*</span></label>
                      <!-- radio -->
                      <div class="form-group">
                        <select class="form-control" name="e_status" id="status">
                          <option value="1" <?php if($value->status==1) echo 'selected'; else echo '';?>>Active</option>
                          <option value="0"<?php if($value->status==0) echo  'selected'; else echo '';?>>Deactive</option>
                        </select>
                        <?php echo form_error('e_status'); ?>
                      </div>
                      <!-- end radio -->
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Menu" name="update">
                  </div>
                </form>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          <?php
          if(!empty($main_id) && $main_id == $value->id)
          {
            if(form_error('e_menu_name') || form_error('e_status') || form_error('e_m_sequence'))
            { ?>
              <script type="text/javascript">
              
              $(window).load(function(){
              
              $('#myMenuModal<?php if(!empty($main_id)) echo $main_id; ?>').modal('show');
              });
              </script>
            <?php }
          } ?>
        </td>
      </tr>
      <?php $i++; }
      } ?>
    </tbody>  
  </table>
</div>
</section>
  
</div>
<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Menu <span class="fa fa-caret-down"></span></h5>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Menu <span class="fa fa-caret-up"></span></h5>');
  });
});
</script>
<script>
function delete_data(id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/delete_menu/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to delete Menu detail."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/cms/footer_menu";
    });


  }
 
}
function change_status(val,id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/change_status_menu/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to change status."))
  {
    $.post(url,{change_status:val}, function(data){
    window.location.href="<?php echo site_url();?>backend/cms/footer_menu";
    });


  }
}
</script>