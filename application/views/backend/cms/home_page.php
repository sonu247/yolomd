
<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard  </a></li>
    <li><i class="fa fa-laptop"></i> CMS</li>
           
</ul>
</div>
<section class="panel"> <header class="panel-heading heading_class"><i class="fa fa-home" aria-hidden="true"></i> Home Page Content</header></section>
<div class="panel">
     
      <ul class="nav nav-tabs">
      
        <li class="<?php if($this->uri->segment(4)==1) echo 'active'; else ''; ?>"><a data-toggle="tab" href="#home"><i class="fa fa-map-o" aria-hidden="true"></i> Banner Section</a></li>
        
        <li class="<?php if($this->uri->segment(4)==2) echo 'active'; else ''; ?>" ><a data-toggle="tab" href="#menu1"><i class="fa fa-user-md"></i> Doctor's and Medical job post Section</a></li>
        <li class="<?php if($this->uri->segment(4)==3) echo 'active'; else ''; ?>"><a data-toggle="tab" href="#menu2"><i class="fa fa-building-o" aria-hidden="true"></i> Seach Jobs by city Section</a></li>
        <li class="<?php if($this->uri->segment(4)==4) echo 'active'; else ''; ?>" ><a data-toggle="tab" href="#menu3"><i class="fa fa-hdd-o" aria-hidden="true"></i> Site Representation Section 4</a></li>
      </ul>

      <div class="tab-content">
        <div id="home" class="tab-pane fade <?php if( $this->uri->segment(4)==1) echo 'in active'; else ''; ?>">
          <div class="row">
                  <section class="panel">
                  <header class="panel-heading heading_class"><i class="fa fa-map-o" aria-hidden="true"></i> Banner section
                   <a href="<?php echo site_url('backend/cms/add_banner');?>" ><button  type="button" class="btn btn-primary btn-sm tooltips pull-right" >Add banner</button></a>
                  </header>

                  
 
                   <div class="panel-body">
                      
            <div class="box-body">
              

           
               <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                  <tr>
                    <th width="5%;">S.No</th>
                    <th width="7%;">#Id</th>
                    <th width="15%;">Banner Title</th>
                    <th width="30%;">Sub Title</th>
                    <th width="15%;">Images</th>
                    <th width="10%;">Status</th>
                    <th width="">Actions</th>
                    
                  </tr>
                </thead>
                <tbody>
                <?php
              
               $i= 1 ;
               if($result_data != "")
               {
              
                foreach ($result_data as $res_data)
                {
                ?>
                  <tr>

                    <td>
                    <form role="form" method="post" id="save_category" enctype="multipart/form-data" action=""  enctype="multipart/form-data">
                    <?php echo $i; ?>
                   
                    </td>
                     <td>
                     <?php if(!empty($res_data['cms_id'])) echo '#'.$res_data['cms_id']; ?><!-- </a> --></td>
                    <td>
                    <?php if(!empty($res_data['cms_title'])) echo ucfirst($res_data['cms_title']); ?>

                    </td>
                    <td>
                     <?php if(!empty($res_data['cms_sub_title'])) echo ucfirst($res_data['cms_sub_title']); ?>
                    </td>
                    <td>
                    <img class="img_class" src="<?php echo UPLOAD_PATH;?>/banner/<?php if(!empty($res_data['cms_image'])) echo $res_data['cms_image']; else echo 'default.png'; ?>">
                   
                    </td>
                   
                    <td style="width:15%;">
                    <?php if(!empty($res_data['status']) &&  $res_data['status']== 1){?>
                    <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $res_data['cms_id'];?>','cms_home_page')">Active</a>
                    <?php } ?>
                     <?php if($res_data['status'] == 0){?>
                    <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $res_data['cms_id'];?>','cms_home_page')">Inactive</a>
                    <?php } ?>
                    </td>
                    <td>
                    <a href="<?php echo site_url();?>backend/cms/add_banner/<?php echo $res_data['cms_id'];?>" class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" title="Edit Banner Detail" rel="tooltip"  data-placement="top" data-original-title="Edit Subcategory" ><i class="fa fa-pencil-square-o"></i></a></a> 
                    <a data-toggle="tooltip" title="Delete this banner!" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-placement="top" data-original-title="Delete this Banner" onclick="delete_data('<?php echo $res_data['cms_id'];?>','cms_home_page');" ><i class="fa fa-trash-o"></i>
                   </a>
                 
                   </form> 
                    </td>
                   
                  </tr>
                  
                <?php $i++; } ?>
               
                <?php }else{?>
                <tr ><td colspan="8" class="error"><h4>Banner detail record not found.</h4></td></tr>
                <?php } ?>
                </tbody>
                
              </table>
            
              
            </div><!-- /.box-body -->
            </div>
            </section>
          </div>
        </div>
        <div id="menu1" class="tab-pane fade <?php if($this->uri->segment(4)==2) echo 'in active'; else ''; ?>">
          <div class="row">
              <div class="col-lg-14">
                  <section class="panel">
                     <header class="panel-heading heading_class"><i class="fa fa-user-md"></i> Doctor's and Medical Job Post Section</header>
                    <form class="form-horizontal tasi-form" role="form" method="post" action="" enctype= "multipart/form-data">
                     <div class="panel-body">
              
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Doctor Title<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                    <input  placeholder="Title 1" class="form-control" name="pm_title1" value="<?php if(!empty($pm_details->title1)) echo $pm_details->title1; else set_value('pm_title1');?>">
                      <div class="left_move">
                    <?php echo form_error('pm_title1'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Doctor Sub-title<!-- <span class="mandatory">*</span> --></label>
                     <div class="col-sm-10">
                     <input placeholder="Sub-title 1" class="form-control" name="pm_subtitle1" value="<?php if(!empty($pm_details->sub_title1)) echo $pm_details->sub_title1; else set_value('pm_subtitle1');?>">
                       <div class="left_move">
                   <!--  <?php //echo form_error('pm_subtitle1'); ?> -->
                    </div>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Doctor Button Label<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                    <input type="text" placeholder="Label 1" class="form-control" name="pm_label1" value="<?php if(!empty($pm_details->label1)) echo $pm_details->label1; else set_value('pm_label1');?>">
                      <div class="left_move">
                    <?php echo form_error('pm_label1'); ?>
                    </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Doctor Button Link<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                       <div class="input-group">
                          <span class="input-group-addon" id="addon"><?php echo base_url(); ?></span>

                    <input type="text" placeholder="Link 1" class="form-control url_addlink" name="pm_link1" value="<?php if(!empty($pm_details->link1)) echo $pm_details->link1; else set_value('pm_link1');?>">
                     </div>
                      <div class="left_move">
                      <div style="margin-top:10px;">
                      
                     Example:- (test_controller/test_method)
                     </div>
                       <div class="left_move">
                    <?php echo form_error('pm_link1'); ?>
                    </div>
                    </div>
                    
                    </div>
                  </div>
                    <div class="form-group">
                  <div class="no-padding">
                    <header class="panel-heading colum" style="margin-bottom:20px;">
                    </header>
                  </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Medical Job Post Title<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                     <input  placeholder="Title 2" class="form-control" name="pm_title2" value="<?php if(!empty($pm_details->title2)) echo $pm_details->title2; else set_value('pm_title2');?>">
                     <div class="left_move">
                    <?php echo form_error('pm_title2'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Medical Job Post Sub-title<!-- <span class="mandatory">*</span> --></label>
                     <div class="col-sm-10">
                     <input placeholder="Sub-title 2" class="form-control" name="pm_subtitle2" value="<?php if(!empty($pm_details->sub_title2)) echo $pm_details->sub_title2; else set_value('sub_title2');?>">
                       <div class="left_move">
                   <!--  <?php //echo form_error('pm_subtitle2'); ?> -->
                    </div>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Medical Job Post Button Label<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                    <input type="text" placeholder="Label 2" class="form-control" name="pm_label2" value="<?php if(!empty($pm_details->label2)) echo $pm_details->label2; else set_value('pm_label2');?>">
                      <div class="left_move">
                    <?php echo form_error('pm_label2'); ?>
                    </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2">Medical Job Post Button Link<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                      <div class="input-group">
                          <span class="input-group-addon" id="addon"><?php echo base_url(); ?></span>
                         
                    <input type="text" placeholder="Link 2" class="form-control url_addlink" name="pm_link2" value="<?php if(!empty($pm_details->link2)) echo $pm_details->link2; else set_value('pm_link2');?>">
                     </div>
                      <div class="left_move">
                      <div style="margin-top:10px;">
                      Example:- (test_controller/test_method)
                    </div>
                      <div class="left_move">
                    <?php echo form_error('pm_link2'); ?>
                    </div>
                    </div>
                   
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label class="col-sm-2 col-sm-1">Background Image<span class="mandatory">*</span></label>
                     <div class="col-sm-10">
                    <input type="file" name="background_image">
                     <?php// echo form_error('background_image'); ?>
                     <div style="float:left;">   
                    <?php //if(!empty($pm_details->id)){ ?>
                    <img style="width:90px; height:90px; margin-top:10px;" src="<?php// echo UPLOAD_PATH ;?>background/<?php// if(!empty($pm_details->image)) echo $pm_details->image; else echo 'default.png'; ?>">
                    <?php //} ?>
                    </div>
                    </div>
                  </div> -->
                  <input type="hidden" name="pm" value="1">
                  <div class="form-group">
                  <label class="col-sm-2 col-sm-2"></label>
                   <div class="col-sm-10">
                    <button class="btn btn-primary" type="submit">Update Doctor and Medical Job Post Detail</button> 
                    </div>   
                  </div>    
                </div>
            </form>
         </section>
        </div>
        </div>

        </div>
        <div id="menu2" class="tab-pane fade <?php if($this->uri->segment(4)==3) echo 'in active'; else ''; ?>">
          <div class="row">
                  <section class="panel">
                 <header class="panel-heading heading_class"><i class="fa fa-search" aria-hidden="true"></i> Search Jobs by City Section
                  <a href="<?php echo site_url('backend/cms/add_city');?>"><button type="button" class="btn btn-primary btn-sm tooltips pull-right">Add City</button></a>
                  </header>
            <div class="panel-body">
                <div class="box-body">
                 
               
                 <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                   
                      <tr>
                        <th width="100px">S.No</th>
                        <th width="100px">#Id</th>
                        <th width="250px">City Name</th>
                        <th width="200px">Image</th>
                        <th width="100px">Order by</th>
                        <th width="100px">Status</th>
                        <th width="">Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                  $k= 1;
                   if($result_city_data != "")
                   {
                    foreach ($result_city_data as $res_c_data)
                    {
                    ?>
                      <tr>

                        <td>
                        <form role="form" method="post" id="save_category" enctype="multipart/form-data" action=""  enctype="multipart/form-data">
                        <?php echo $k; ?>
                       
                        </td>
                         <td><!-- <a href="<?php //echo site_url();?>admin/category/edit_category/<?php //echo $res_data['cat_id'];?>"> -->
                         <?php if(!empty($res_c_data['search_city_id'])) echo '#'.$res_c_data['search_city_id']; ?><!-- </a> --></td>
                        <td>
                        <?php if(!empty($res_c_data['search_city_name'])) echo ucfirst($res_c_data['search_city_name']); ?>

                        </td>
                        <td><img class="img_class" src="<?php echo UPLOAD_PATH;?>/city/<?php if(!empty($res_c_data['search_city_image'])) echo $res_c_data['search_city_image']; else echo 'default.png'; ?>">
                       
                        </td>
                        <td>
                        <?php if(!empty($res_c_data['order_by'])) echo $res_c_data['order_by']; ?>
                        </td>
                        
                        
                        <td style="width:15%;">
                        <?php if($res_c_data['status'] == 1){?>
                        <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $res_c_data['search_city_id'];?>','search_job_city')">Active</a>
                        <?php } ?>
                         <?php if($res_c_data['status'] == 0){?>
                        <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $res_c_data['search_city_id'];?>','search_job_city')">Inactive</a>
                        <?php } ?>
                       </td>
                       <td>
                       <a href="<?php echo site_url();?>backend/cms/add_city/<?php echo $res_c_data['search_city_id'];?>" class="btn btn-primary btn-xs tooltips" data-toggle="tooltip" title="Edit City Detail" rel="tooltip"  data-placement="top" data-original-title="Edit Subcategory" ><i class="fa fa-pencil-square-o"></i></a></a> 
                        <a data-toggle="tooltip" title="Delete this city!" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-placement="top" data-original-title="Delete this City" onclick="delete_data('<?php echo $res_c_data['search_city_id'];?>','search_job_city');" ><i class="fa fa-trash-o"></i>
                       </a>
                     
                       </form> 
                        </td>
                       
                      </tr>
                      
                   
                   
                    <?php $k++; }}else{?>
                    <tr ><td colspan="8" class="error"><h4>City record not found.</h4></td></tr>
                    <?php } ?>
                    </tbody>
                    
                  </table>
                  
                  </div>
                  </div>
                   </section>
          </div>
        </div>
        <div id="menu3" class="tab-pane fade <?php if($this->uri->segment(4)== 4) echo 'in active'; else ''; ?>">
        <div class="row">
            <div class="col-lg-14">
                <section class="panel">
                      <header class="panel-heading heading_class"><i class="fa fa-hdd-o" aria-hidden="true"></i> Site Representation Section 4</header>
                        <form class="form-horizontal tasi-form" role="form" method="post" action="" enctype= "multipart/form-data">
                          <div class="panel-body">
             
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2">Title 1<span class="mandatory">*</span></label>
                       <div class="col-sm-9">
                      <input  placeholder="Title 1" class="form-control" name="s4_title1" value="<?php if(!empty($s4_details->title1)) echo $s4_details->title1; else set_value('s4_title1');?>">
                        <div class="left_move">
                      <?php echo form_error('s4_title1'); ?>
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                     <label class="col-sm-2 col-sm-2">Sub-title 1<!-- <span class="mandatory">*</span> --></label>
                       <div class="col-sm-9">
                       <input placeholder="Sub-title 1" class="form-control" name="s4_subtitle1" value="<?php if(!empty($s4_details->sub_title1)) echo $s4_details->sub_title1; else set_value('s4_subtitle1');?>">
                         <div class="left_move">
                    <!--   <?php //echo form_error('s4_subtitle1'); ?> -->
                      </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2">Title 2<span class="mandatory">*</span></label>
                       <div class="col-sm-9">
                       <input  placeholder="Title 2" class="form-control" name="s4_title2" value="<?php if(!empty($s4_details->title2)) echo $s4_details->title2; else set_value('s4_title2');?>">
                        <div class="left_move">
                      <?php echo form_error('s4_title2'); ?>
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2">Sub-title 2<!-- <span class="mandatory">*</span> --></label>
                       <div class="col-sm-9">
                       <input placeholder="Sub-title 2" class="form-control" name="s4_subtitle2" value="<?php if(!empty($s4_details->sub_title2)) echo $s4_details->sub_title2; else set_value('sub_title2');?>">
                         <div class="left_move">
                     <!--  <?php //echo form_error('s4_subtitle2'); ?> -->
                      </div>
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-2 col-sm-2">Title 3<span class="mandatory">*</span></label>
                       <div class="col-sm-9">
                       <input  placeholder="Title 3" class="form-control" name="s4_title3" value="<?php if(!empty($s4_details->title3)) echo $s4_details->title3; else set_value('s4_title3');?>">
                       <div class="left_move">
                      <?php echo form_error('s4_title3'); ?>
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2">Sub-title 3<!-- <span class="mandatory">*</span> --></label>
                       <div class="col-sm-9">
                       <input placeholder="Sub-title 3" class="form-control" name="s4_subtitle3" value="<?php if(!empty($s4_details->sub_title3)) echo $s4_details->sub_title3; else set_value('sub_title3');?>">
                         <div class="left_move">
                     <!--  <?php //echo form_error('s4_subtitle3'); ?> -->
                      </div>
                      </div>

                    </div>
                     
                    <input type="hidden" name="s4" value="1">
                    <div class="form-group">
                    <label class="col-sm-2 col-sm-1"></label>
                     <div class="col-sm-10">
                      <button class="btn btn-primary" type="submit">Update Site Representation Section 4</button>    
                      </div>
                    </div>    
                  </div>
                  </form>
            </section>
        </div>
        </div>
        </div>
      </div>
    </div>
<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});
</script>
<script>
function delete_data(id,table)
{
  if(table == 'cms_home_page')
  {
      get_link = 1;
  }else{
    get_link = 3;
  }
  var url = "<?php echo site_url();?>backend/cms/delete/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to delete Banner detail."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/cms/home_page/"+get_link;
    });


  }
 
}
function change_status(val,id,table)
{
  if(table == 'cms_home_page')
  {
      get_link = 1;
  }else{
    get_link = 3;
  }
  var url = "<?php echo site_url();?>backend/cms/change_status/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to change status."))
  {
    $.post(url,{change_status:val}, function(data){
    window.location.href="<?php echo site_url();?>backend/cms/home_page/"+get_link;
    });


  }
}
</script>
