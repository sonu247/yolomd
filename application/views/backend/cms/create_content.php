  <div class="bread_parent">
<ul class="breadcrumb">
     <li><a href="<?php echo base_url('backend/superadmin/dashboard')?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class=""><i class="fa fa-cog"></i> CMS Page</li>
           
</ul>
</div>

<header class="panel-heading heading_class"><i class="fa fa-cog"></i> CMS Page</header>
  <div clss="row">
           <div class="col-lg-14">
           
           
        <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
          <?php 
          if(validation_errors() == true){?>
          <div class="alert alert-danger-update"> <?php echo validation_errors(); ?></div>
          <?php } //echo form_error('name[]'); ?>
             <div class="form-body" style="padding:10px">
                <?php $i=0;
                if(!empty($option)){
                 foreach ($option as $value) { 
                  ?>
                 <?php if($value->id==1 ){ ?>
                   <div class="form-group">
                   <label class="col-md-3 control-label"><?php echo $value->title;?></label> 
          <div class="col-md-9">
         <textarea name="name[]"  class="tinymce_edittor form-control" rows="1" cols="6">
         <?php echo $value->content;?>
        </textarea>
        </div>
        </div>
           <?php }elseif($value->id==2 ){ ?>
             <div class="form-group">
                   <label class="col-md-3 control-label"><?php echo $value->title;?></label> 
                  <div class="col-md-9">
                 <textarea name="name[]" class="tinymce_edittor form-control" rows="1" cols="6">
                 <?php echo $value->content;?>
                </textarea>
             </div>
             </div>
              <?php }elseif($value->id==3 ){ ?>
             <div class="form-group">
                   <label class="col-md-3 control-label"><?php echo $value->title;?></label> 
                  <div class="col-md-9">
                 <textarea name="name[]" class="tinymce_edittor form-control" rows="1" cols="6">
                 <?php echo $value->content;?>
                </textarea>
             </div>
             </div>
               <?php }elseif($value->id==4 ){ ?>
             <div class="form-group">
                   <label class="col-md-3 control-label"><?php echo $value->title;?></label> 
                  <div class="col-md-9">
                 <textarea name="name[]" class="tinymce_edittor form-control" rows="1" cols="6">
                 <?php echo $value->content;?>
                </textarea>
             </div>
             </div>
          
           <?php }elseif($value->id==5 ){ ?>
             <div class="form-group">
                   <label class="col-md-3 control-label"><?php echo $value->title;?></label> 
                  <div class="col-md-9">
                 <textarea name="name[]" class="tinymce_edittor form-control" rows="1" cols="6">
                 <?php echo $value->content;?>
                </textarea>
             </div>
             </div>
           <?php }else{?>
             <div class="form-group">
                   <label class="col-md-3 control-label"><?php echo $value->title;?></label> 
                       <div class="col-md-9">  
                     <input type="text" <?php if($value->id==22){ ?> pattern="https?://.+" title="Include http://"  <?php } ?> placeholder="<?php echo $value->title ?>" class="form-control" name="name[]" value="<?php echo $value->content;?>">
                    
                    
                   
                   </div>
                </div> 
                <?php } ?>
                   <input type="hidden" name="ids[]" value="<?php echo $value->id;?>">
                <?php $i++;}} ?>
             </div>
             <div class="form-actions fluid">
                <div class="col-md-10 pull-right" style="padding:10px">
                   <input class="btn btn-primary" type="submit" name="update" value="Update Content">
                   <a href="<?php echo base_url()?>backend/superadmin/dashboard">
                   <button class="btn btn-danger" type="button">Cancel</button>  </a>                            
                </div>
            
          </form>
          <div class="clearfix"></div>
          </div>
          
         </section>
           </div>
        </div>
