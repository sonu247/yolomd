<div class="bread_parent">
<ul class="breadcrumb">
     <li><a href="<?php echo base_url('backend/superadmin/dashboard')?>"><i class="fa fa-dashboard"></i>  Dashboard</a></li>
    <li ><a href="<?php echo base_url('backend/cms/footer_menu')?>"><i class="fa fa-list"></i> Menu</a></li>
    <li class=""><i class="fa fa-navicon" aria-hidden="true"></i> Submenu</li>
           
</ul>
</div>

      <!-- Content Wrapper. Contains page content -->
    <div clss="row">
           <div class="col-lg-14">
            <section class="panel">
                  <div  class="pull-right" >
              
              
                      <button type="button" style="padding: 0px 11px;" class="btn_tool btn btn-block btn-primary" data-toggle="collapse" data-target="#demo"><h5>Add Submenu
                   <span class="fa fa-caret-down"></span> </h5>
                  </button> 
                    </div>
              <!--   <div   id="demo" class="collapse" > -->
                <div class="panel-body collapse <?php if(!empty($_POST['add'])) echo 'in'; else ''; ?> toogle_class" id="demo" style="border-bottom:1px solid rgb(221,221,221);">
                    <div class="col-lg-12"> 
               
                    <form class="form-horizontal" action="" name="add_submenu" id="add_submenu" method="post" >
                   
                    <input type="hidden" name="m_parent" value="<?php echo $mid; ?>">
                      <div class="col-xs-2">
                       
                     
                        <input type="text" id="submenu_name" name="submenu_name" class="form-control" placeholder="Submenu" value="<?php echo set_value('submenu_name'); ?>">
                        <div class="left_move">
                        <?php echo form_error('submenu_name'); ?>
                        </div>
                       
                      </div>
                      <div class="col-xs-1">
                       
                        
                        <input type="text" class="form-control" value="<?php echo set_value('sm_sequence'); ?>" name="sm_sequence" placeholder="Sequence">
                        <div class="left_move">
                         <?php echo form_error('sm_sequence'); ?>
                         </div>
                        
                      </div>
                      <div class="col-xs-2">
                       
                       
                        <select class="form-control" name="status">
                        <option value="">Status</option>
                                <option value="1" <?php echo set_select('status', '1'); ?>>Active</option>
                                <option value="0" <?php echo set_select('status', '0'); ?>>Deactive</option>
                        </select>
                        <?php echo form_error('status'); ?>
                        </div>
                      
                   
                      <div class="col-xs-5">
                       
                         <div class="input-group">
                        <span class="input-group-addon" id="addon"><?php echo base_url(); ?></span> 
        
                        <input type="text" name="url_link"  class="form-control" value="<?php echo set_value('url_link'); ?>" class="form-control url_addlink" placeholder="Url Link" value="">
                          </div>
                         <div class="left_move">
                          <?php echo form_error('url_link'); ?> 
                          </div> 
                          
                          
                      </div>
                      <div class="col-xs-2 pull-right">

                        <input type="submit" class="btn btn-block btn-primary" name="add" value="Add Submenu">
                      </div>
                    
                    </form>
             </div><!-- /.box-body -->
           
    </div>

 <div class="panel-body">
<div class="row">
         <header class="panel-heading"><i class="fa fa-navicon" aria-hidden="true"></i>    Submenu of <strong> <?php echo ucfirst($parentc); ?>      </header>
              
                    <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                   
                      <tr>
                      <th width="50px;">S.No</th>
                        <th width="100px;">ID</th>
                        <th width="200px;">Submenu Title</th>
                        <th width="200px;">Sequence</th>
                        <th width="300px;">Url Link</th>
                        <th width="150px;">Status</th>
                        <th width="">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($submenu)){ 
                      $i = 1;
                      foreach ($submenu as $value) {
                      ?>
                      <tr>
                       <td><?php echo $i; ?></td>
                        <td><?php echo '#'.$value->id; ?></td>
                        
                        <td>
                          <?php echo $value->name; ?> 
                          &nbsp;
                          
                        </td>
                        <td><?php echo $value->order_by; ?></td> 
                        <td><?php if(!empty($value->link)){echo $value->link; }
                          ?> </td> 
                      
                        <td><?php if($value->status == 1) {?>
                           <a href="#" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Make inactive" rel="tooltip"  data-placement="top" data-original-title="Make inactive" onclick="change_status('0','<?php echo $value->id;?>','cms_menu')">Active</a>
                        <?php } if($value->status == 0) {?>
                          <a href="#" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  onclick="change_status('1','<?php echo $value->id;?>','cms_menu')">Deactive</a>
                        <?php } ?>
                        </td> 
                        <td>
                          <span data-toggle="tooltip" title="Edit Submenu">
                          <a href="javascript:void(0)" data-toggle="modal" 
                          data-target="#myModal<?php echo $value->id; ?>" class="btn btn-primary btn-xs tooltips" title="Edit Submenu"><i class="fa fa-pencil-square-o"></i></a></span>
                           &nbsp;&nbsp;
                           <a href="javascript:void(0)" class="btn btn-danger btn-xs tooltips" data-toggle="tooltip" onclick="delete_data('<?php echo  $value->id;?>','cms_menu');" title="Delete Submenu"><i class="fa fa-trash-o"></i></a>
                        </td>



                        <!-- ===== Category Model===== -->
            <div class="modal" id="myModal<?php echo $value->id; ?>">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                  Update Submenu
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="menuheader"></h4>
                  </div>
                  <form action="<?php echo base_url();?>backend/cms/sub_menu/<?php echo $mid ;?>" name="edit_submenu" id="edit_submenu" method="post">
                  <div class="modal-body">
            
                        <input type="hidden" name="e_m_id" id="e_m_id" value="<?php if(!empty($value->id)) echo  $value->id; else echo set_value('e_m_id');?>" />
                       
                        <div class="col-xs-4">
                          <label>Submenu<span class="mandatory">*</span></label> 
                          <input type="text" id="m_name" name="e_submenu_name" class="form-control" placeholder="Submenu Title" value="<?php if(!empty($value->name))echo  $value->name; else echo set_value('e_submenu_name');?>">
                           <?php echo form_error('e_submenu_name'); ?>   
                        </div>
                        <div class="col-xs-4">
                          <label>Sequence<span class="mandatory">*</span></label> 
                          <input type="text" class="form-control" name="e_sm_sequence" id="subm_seq" placeholder="Sequence" value="<?php if(!empty($value->order_by))echo  $value->order_by; else echo set_value('e_sm_sequence');?>">
                           <?php echo form_error('e_sm_sequence'); ?>   
                        </div>
                        <div class="col-xs-4">
                       
                          <label>Status<span class="mandatory">*</span></label> 
                          <select class="form-control" name="e_status" id="status">
                                  <option value="1" <?php if($value->status==1) echo 'selected'; else echo '';?>>Active</option>
                                  <option value="0"<?php if($value->status==0) echo  'selected'; else echo '';?>>Deactive</option>
                          </select>
                          <?php echo form_error('e_status'); ?>   
                        </div>
                      <div class="clearfix"></div>
                        <div class="col-xs-8">
                        <br/>
                          <label>Url Link<span class="mandatory">*</span></label> 
                          <div class="input-group">
                          <span class="input-group-addon" id="addon1"><?php echo base_url(); ?></span><input type="text" id="url_link" style="width: 130px;" name="e_url_link" class="form-control url_addlink" placeholder="Url Link" value="<?php if(!empty($value->link))echo  $value->link; else echo set_value('e_url_link');?>">
                          
                           </div>
                            <?php echo form_error('e_url_link'); ?>
                        </div>
                        
                       
                  </div>
                  <div class="clearfix"></div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Submenu" name="update">
                  </div>
                  </form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                      </tr>
                      <?php
            if(!empty($main_id) && $main_id == $value->id)
            {
              if(form_error('e_submenu_name') || form_error('e_status') || form_error('e_sm_sequence') || form_error('e_url_link'))
              {
            ?>
            <script type="text/javascript">
                $(window).load(function(){
                     $('#myModal<?php if(!empty($main_id)) echo $main_id; ?>').modal('show');
                  });
            </script>
            <?php } }?>
                      <?php $i++; }                        
                      } ?>
                    </tbody>
                   
                  </table>
                   </div>
</section>
  </div>
  </div>

          
<script>
            var url_id = "<?php echo $mid; ?>";
function delete_data(id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/delete_menu/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to delete Submenu detail."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/cms/sub_menu/"+url_id;
    });


  }
 
}
function change_status(val,id,table)
{
  
  var url = "<?php echo site_url();?>backend/cms/change_status_menu/"+id+"/"+table;
  if(confirm("Are you sure. Do you want to change status."))
  {
    $.post(url,{change_status:val}, function(data){
    window.location.href="<?php echo site_url();?>backend/cms/sub_menu/"+url_id;
    });


  }
}
</script>
<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('<h5> Add Submenu <span class="fa fa-caret-down"></span> </h5>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('<h5>Add Submenu <span class="fa fa-caret-up"></span></h5>');
  });
});
</script>
<style>
.error{
  color:red;
}
</style>
