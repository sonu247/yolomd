<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>">Dashboard  </a></li>
     <li><a href="<?php echo base_url('backend/listusers/physician');?>"> Doctor's Section</a></li>
   <li class="">Doctor Detail</li>
</ul>
</div>

<?php $status_array = array('1'=>'Active','2'=>'Deactive','3'=>'Banned');?>
<div class="row">
     <div class="col-lg-14">
        <section class="panel">
          <header class="panel-heading heading_class"  ><i class="fa fa-user-md"></i>   Doctor Detail
              <div class="pull-right">
               <div class="dropdown">
                              <button class=" <?php
                                 foreach($status_array as $k => $status_a)
                                  {
                                    if(!empty($user_details->user_status) && $k == $user_details->user_status)
                                    {
                                 
                            if($k == 1)
                             echo 'btn btn-success';
                            elseif($k == 2)
                              echo 'btn btn-danger';
                             elseif($k == 3)
                                echo 'btn btn-info';
                              else
                                echo '';
                            }
                          }
                              ?> btn-xs  dropdown-toggle_get_val" id="menu1" type="button" data-toggle="dropdown"><?php 
                              foreach($status_array as $k => $status_a)
                              {
                                if(!empty($user_details->user_status) && $k == $user_details->user_status) 
                                  echo $status_a;
                              }
                               ?>
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                 <?php
                                 foreach($status_array as $k => $status_a)
                                  {
                                    if(!empty($user_details->user_status) && $k != $user_details->user_status)
                                    {
                                ?>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url();?>backend/listusers/change_status/<?php if(!empty($user_details->user_id)) echo $user_details->user_id; ?>/<?php echo $k; ?>/2"><?php echo $status_a; ?></a></li>
                                <?php } } ?>
                                
                              </ul>
                              <a target="_new" href="<?php echo base_url().'backend/superadmin/login_user/'.$user_details->user_id ?>" class="btn btn-warning btn-xs tooltips" rel="tooltip"  data-placement="top" data-original-title="Proxy Login"><div  data-toggle="tooltip" title="Proxy Login">Proxy Login</div></a>

                              <button type="button" style="padding: 0px 11px;" class="btn_tool btn btn-primary btn-xs tooltips" data-toggle="collapse" data-target="#demo" data-toggle="tooltip" title="Update Password">Update Password
                                <span class="fa fa-caret-down"></span> 
                                </button> 
                            
                              
                       </div> 
</div>

          </header>

          <div class="panel-body collapse <?php if(!empty($_POST['change_password'])) echo 'in'; else ''; ?>" id="demo" style="padding:0px; ">
                 
                    <div class="col-lg-14"> 
                      
                        <h5 class="panel-heading" style="color:rgb(55,58,60);"><i class="fa fa-cog"></i> Update Password</h5>
                          <form class="form-horizontal" action="" name="add_menu" id="add_menu" method="post">
                        <div class="col-xs-5">
                       <label class="col-sm-3 col-sm-3 no-padding">New Password<span class="mandatory">*</span></label> 
                       <div class="col-sm-9">
                         <input type="password" placeholder="New Password" class="form-control" name="newpassword" value="">
                          <div class="left_move">
                           <?php echo form_error('newpassword'); ?>
                           </div>
                           </div>
                          <!--  </div> -->
                          
                        </div>
                      
                        <div class="col-xs-5">
                          <label class="col-sm-3 col-sm-3 no-padding">Confirm Password<span class="mandatory">*</span></label> 
                          <!-- radio -->
                            
                             <div class="col-sm-9" style="float:left;">
                                 <input type="password" placeholder="Confirm Password" class="form-control" name="confpassword" value="">
                                <div class="left_move">
                              <?php echo form_error('confpassword'); ?>
                              </div>
                               </div>
                            
                           <!-- end radio -->
                        </div>
                     
                        <div class="col-xs-2 pull-right">
                       
                          <input type="submit" class="btn btn-block btn-primary" value="Update Password" name="change_password">
                        </div>
                        </form>
                        </div>

                        </div><!-- /.box-body -->     
         
          <div class ="panel-body">

          <div class="row">
          
            <form role="form" method="post" class="form-horizontal" action="<?php echo current_url()?>">           
            <div class="col-lg-12">
            <header class="panel-heading no-padding" style="margin-bottom:20px;">
                        <i class="fa fa-registered" aria-hidden="true"></i>
                      Doctor Registration
                      </header>
               <div class   ="form-group">
               <label class ="col-md-3 col-md-3">Doctor Name Title<span class="mandatory">*</span></label>
               <div class   ="col-sm-9">
                  <div class ="left_move">

                    <input type ="radio" name="user_name_title" id="user_name_title"  value="Mr." <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Mr.') echo  'checked'; else echo '';?>> 
                     <lable style="padding:5px 4px;"> Mr. </label>
                
                 
                    <input style="margin-left:20px;" type ="radio" name="user_name_title" id="user_name_title1"  value="Miss" <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Miss') echo  'checked'; else echo '';?>>
                     <lable style="padding:5px 5px;"> Miss </label>
                 
                    <input type ="radio" style="margin-left:20px;" name="user_name_title" id="user_name_title2"   value="Mrs." <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Mrs.') echo  'checked'; else echo '';?>> 
                     <lable style="padding:5px 5px;"> Mrs. </label>
                  
                 
                    <input type ="radio" style="margin-left:20px;" name="user_name_title" id="user_name_title3"  value="Dr." <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Dr.') echo  'checked'; else echo '';?>> 
                     <lable style="padding:5px 5px;">Dr.</label>
                     
                 </div>
                   
               </div>
               <div class ="left_move">
                    <?php echo form_error('user_name_title'); ?>
                   </div>
               </div>
               
               <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">Doctor First Name<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="physician_first_name" placeholder="Doctor First Name" class="form-control" value="<?php if(!empty($user_details->user_first_name)) echo  $user_details->user_first_name; else echo set_value('physician_first_name');?>">
                     <div class ="left_move">
                      <?php echo form_error('physician_first_name'); ?>
                     </div>
                   </div>
               </div>
               <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">Doctor Last Name<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="physician_last_name" placeholder="Doctor Last Name" class="form-control" value="<?php if(!empty($user_details->user_last_name)) echo  $user_details->user_last_name; else echo set_value('physician_last_name');?>">
                     <div class ="left_move">
                      <?php echo form_error('physician_last_name'); ?>
                     </div>
                   </div>
               </div>
                <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">Doctor Email<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="email_address" placeholder="Doctor Email" class="form-control" value="<?php if(!empty($user_details->user_email)) echo  $user_details->user_email; else echo set_value('email_address');?>" >
                      <div class ="left_move">
                        <?php echo form_error('email_address'); ?>
                      </div>
                   </div>
               </div>
              

            </div>
             <div class="col-lg-12">
             <header class="panel-heading no-padding" style="margin-bottom:20px;">
                       <i class="fa fa-user-md"></i> Doctor Professional Profile
                      </header>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">Doctor Gender<span class="mandatory">*</span></label>

                 <div class="col-sm-9 ">
                    <div class="left_move">
                      <input type="radio" name="physician_gender"  value="Male" <?php if(!empty($user_details->physician_gender) && $user_details->physician_gender == 'Male') echo  'checked'; else echo '';?>>
                      <lable style="padding:0px 7px;">Male</lable> </div>
                      <div class="left_move" >
                      
                       <input type="radio" name="physician_gender" value="Female" <?php if(!empty($user_details->physician_gender) && $user_details->physician_gender == 'Female') echo  'checked'; else echo '';?>>
                       <label style="padding:0px 7px;" >
                       Female
                       </label>
                       </div>
                        
                    </div>
                    <div style="left_move">
                       <?php echo form_error('physician_gender'); ?>
                        </div>
               </div>

                <div class="form-group">
                  <label class="col-md-3 col-md-3">Doctor Date of birth<span class="mandatory">*</span></label>
                 <div class="col-sm-2 ">
                      <select name="physician_dob_date"  class="form-control">
                      <option value="">Date</option>
                        <?php 
                        for($i=1; $i<=31; $i++)
                        {
                        ?>
                        <option value="<?php echo $i; ?>" <?php if(!empty($user_details->physician_dob_date) && $user_details->physician_dob_date == $i) echo  'selected'; else echo '';?>><?php echo $i; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                   
                       <?php echo form_error('physician_dob_date'); ?>
                 
                  </div>
                  <div class="col-sm-2 ">
                    <select name="physician_dob_month"  class="form-control">
                    <option value="">Month</option>
                    <?php 
                    for($k=1; $k<=12; $k++)
                    {
                    ?>
                    <option value="<?php echo $k; ?>" <?php if(!empty($user_details->physician_dob_month) && $user_details->physician_dob_month == $k) echo  'selected'; else echo '';?>><?php echo $k; ?></option>
                    <?php
                    }
                    ?>
                    </select>
                    <div style="left_move">
                       <?php echo form_error('physician_dob_month'); ?>
                   </div>
                    </div>
                    <div class="col-sm-2 ">
                    <select name="physician_dob_year"  class="form-control">
                    <option value="">Year</option>
                    <?php 
                    for($j=1931; $j<=2001; $j++)
                    {
                    ?>
                    <option value="<?php echo $j; ?>" <?php if(!empty($user_details->physician_dob_year) && $user_details->physician_dob_year == $j) echo  'selected'; else echo '';?>><?php echo $j; ?></option>
                    <?php
                    }
                    ?>
                    </select>
                   <div style="left_move">
                       <?php echo form_error('physician_dob_year'); ?>
                   </div>
                 </div>
               </div>
               
              

               <div class="form-group">
                  <label class="col-md-3 col-md-3">Doctor current position<span class="mandatory">*</span></label>
                 <div class="col-sm-9 ">
                    <select name="physician_profession"  class="form-control" >

                    <option value="">Select Physician</option>
                     <?php 
                     if(!empty($currently_statu))
                     {
                     foreach($currently_statu as $k=>$currently_s){?>
                    <option value="<?php if(!empty($k)) echo $k; ?>" <?php if(!empty($user_details->physician_profession) && $user_details->physician_profession == $k) echo  'selected'; else echo '';?>><?php if(!empty($currently_s)) echo $currently_s; ?></option>
                    <?php }} ?>
                    </select>
                   <div style="left_move">
                       <?php echo form_error('physician_profession'); ?>
                   </div>
                 </div>
               </div>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">Medical School<span class="mandatory">*</span></label>

                 <div class="col-sm-9 ">
                    <select name="medical_school"  class="form-control" >
                    <option value="">Medical School </option>
                    <?php 
                     if(!empty($medical_school))
                     {
                     foreach($medical_school as $medical_s){?>
                    <option  value="<?php if(!empty($medical_s->id)) echo $medical_s->id; ?>" <?php if(!empty($user_details->medical_school) && $user_details->medical_school == $medical_s->id) echo  'selected'; else echo '';?>><?php if(!empty($medical_s->name)) echo $medical_s->name; ?></option>
                    <?php }} ?>
                    </select>
                   <div style="left_move">
                       <?php echo form_error('medical_school'); ?>
                   </div>
                 </div>
               </div>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">I finished my training in / Doctor will finish my training in <span class="mandatory">*</span></label>
                 <div class="col-sm-9 ">
                    <select name="traning_year"  class="form-control">
                    <option value="">Select Year</option>
                    <?php 
                    for($m=1966; $m<=2021; $m++)
                    {
                    ?>
                    <option value="<?php echo $m; ?>"  <?php if(!empty($user_details->traning_year) && $user_details->traning_year == $m) echo  'selected'; else echo '';?>><?php echo $m; ?></option>
                    <?php
                    }
                    ?>
                    </select>
                   <div style="left_move">
                       <?php echo form_error('traning_year'); ?>
                   </div>
                 </div>
               </div>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">Medical Specialty<span class="mandatory">*</span></label>
                 <div class="col-sm-9 ">
                    <select name="medical_specialty"  class="form-control" >

                    <option value="">Select Medical Specialty</option>
                     <?php 
                     if(!empty($medical_school))
                     {
                     foreach($medical_specialty as $medical_spe){?>
                    <option  value="<?php if(!empty($medical_spe->id)) echo $medical_spe->id; ?>" <?php if(!empty($user_details->medical_specialty) && $user_details->medical_specialty == $medical_spe->id) echo  'selected'; else echo '';?>><?php if(!empty($medical_spe->name)) echo $medical_spe->name; ?></option>
                    <?php }} ?>
                    </select>
                   <div style="left_move">
                       <?php echo form_error('medical_specialty'); ?>
                   </div>
                 </div>
               </div>
               </div>
                <!-- <div class="col-lg-12">
             <header class="panel-heading no-padding" style="margin-bottom:20px;">
                     <i class="fa fa-hourglass" aria-hidden="true"></i> MINC 
                      </header>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">MINC</label>
                 <div class="col-sm-9 ">
                      <input type="text" name="physician_minc" placeholder="MINC" class="form-control" value="<?php //if(!empty($user_details->physician_minc)) echo  $user_details->physician_minc; else echo set_value('physician_minc');?>">
                       <?php //echo form_error('physician_minc'); ?>
                  
                 </div>
               </div>
               </div> -->

                <div class="col-lg-12">
             <header class="panel-heading no-padding" style="margin-bottom:20px;">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
 Contact
                      </header>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">Country</label>
                 <div class="col-sm-9 ">
                    <select name="physician_country" id="country"  class="form-control">
                    <option value="Canada">Canada</option>
                    
                    </select>
                   <div style="left_move">
                    
                   </div>
                 </div>
               </div>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">Province</label>
                 <div class="col-sm-9 ">
                   <select name="physician_state" ng-model="PhysicianData.physician_state" class="form-control"  ng-if="canada_province">
                   <option value="">Select Province</option>

                   <?php 
                     if(!empty($canada_province))
                     {
                     foreach($canada_province as $canada_pro){?>
                   <option  value="<?php if(!empty($canada_pro->id)) echo $canada_pro->id; ?>" <?php if(!empty($user_details->physician_state) && $user_details->physician_state == $canada_pro->id) echo  'selected'; else echo '';?>><?php if(!empty($canada_pro->province_name)) echo $canada_pro->province_name; ?></option>
                    <?php }} ?>
                   </select>
                   <div style="left_move">
                      
                   </div>
                 </div>
               </div>
                <div class="form-group">
                  <label class="col-md-3 col-md-3">City</label>
                 <div class="col-sm-9 ">
                    <input type="text" name="physician_city" class="form-control" value="<?php if(!empty($user_details->physician_city)) echo  $user_details->physician_city; else echo set_value('physician_city');?>">
                   <div style="left_move">
                     
                   </div>
                 </div>
               </div>
               <div class="form-group">
                  <label class="col-md-3 col-md-3">Address</label>
                 <div class="col-sm-9 ">
                   <textarea name="physician_address"  class="form-control"><?php if(!empty($user_details->physician_address)) echo $user_details->physician_address; else echo set_value('physician_address');?></textarea>
                   <div style="left_move">
                      
                   </div>
                 </div>
               </div>
                <div class="form-group">
                  <label class="col-md-3 col-md-3">Postal Code</label>
                 <div class="col-sm-9 ">
                   <input type="text" name="physician_postal_code" class="form-control" value="<?php if(!empty($user_details->physician_postal_code)) echo  $user_details->physician_postal_code; else echo set_value('physician_postal_code');?>">
                   <div style="left_move">
                      
                   </div>
                 </div>
               </div>
                <div class="form-group">
                  <label class="col-md-3 col-md-3">Phone Number</label>
                 <div class="col-sm-9 ">
                   <input type="text" name="physician_phone_number"  class="form-control" value="<?php if(!empty($user_details->physician_phone_number)) echo  $user_details->physician_phone_number; else echo set_value('physician_phone_number');?>">
                   <div style="left_move">
                      
                   </div>
                 </div>
               </div>

               </div>
               <div class="col-lg-12">
               
                    <header class="panel-heading no-padding" style="margin-bottom:20px;">
                      <i class="fa fa-envelope" aria-hidden="true"></i>
                      Profile Customization
                      </header>
                      <div class="form-group">
                        <label class="col-md-3 col-md-3">Default Search Specialty</label>
                         <div class="col-sm-9 ">
                            <select name="medical_selectSpecialty"  class="form-control" >

                            <option value="">Select Medical Specialty</option>
                             <?php 
                             if(!empty($medical_school))
                             {
                             foreach($medical_specialty as $medical_spe){?>
                            <option  value="<?php if(!empty($medical_spe->id)) echo $medical_spe->id; ?>" <?php if(!empty($user_details->medical_selectSpecialty) && $user_details->medical_selectSpecialty == $medical_spe->id) echo  'selected'; else echo '';?>><?php if(!empty($medical_spe->name)) echo $medical_spe->name; ?></option>
                            <?php }} ?>
                            </select>
                           <div style="left_move">
                               
                           </div>
                         </div>
                       </div>

                       <div class="form-group">
                          <label class="col-md-3 col-md-3">Default Search Province</label>
                         <div class="col-sm-9 ">
                           <select name="physician_selectState" ng-model="PhysicianData.physician_selectState" class="form-control"  ng-if="canada_province">
                           <option value="">Select Province</option>

                           <?php 
                             if(!empty($canada_province))
                             {
                             foreach($canada_province as $canada_pro){?>
                           <option  value="<?php if(!empty($canada_pro->province_name)) echo $canada_pro->province_name; ?>" <?php if(!empty($user_details->physician_selectState) && $user_details->physician_selectState == $canada_pro->province_name) echo  'selected'; else echo '';?>><?php if(!empty($canada_pro->province_name)) echo $canada_pro->province_name; ?></option>
                            <?php }} ?>
                           </select>
                           <div style="left_move">
                              
                           </div>
                         </div>
                       </div>
                         <div class="form-group">
                         <label class="col-md-3 col-md-3">Message</label>
                         <div class="col-sm-9 ">
                        <textarea name="physician_default_msg"  class="form-control"><?php if(!empty($user_details->physician_default_msg)) echo  $user_details->physician_default_msg; else echo set_value('physician_default_msg');?></textarea>
                        <div class ="left_move" style="margin-top:5px;">
                         Type a generic message to quickly click and send messages to medical facilities from their ad page
                         </div>
                         </div>
                         </div>                                     
             
              <div class="col-lg-12">
            
               <div class="form-group">
               <label class="col-md-3 col-md-3">
                  
               </label>
               <div class="col-sm-9">
              <input class="btn btn-primary" type="submit" name="update" value="Update Doctor Detail">
               </div>
               </div>
              </div>       
          </div>    
          </form>
  </div>
</section>
  </div>
  </div>
<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('Update Password <span class="fa fa-caret-down"></span>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('Update Password <span class="fa fa-caret-up"></span>');
  });
});
</script>
<style>
label{
text-align:right;
}
</style>