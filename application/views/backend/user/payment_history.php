<script>
function delete_data(id)
{
   var url = "<?php echo site_url();?>backend/listusers/delete_postjob/"+id;
  if(confirm("Are you sure. Do you want to delete Post Job Detail."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/listusers/postjob";
    });


  }
 
}

</script>
<div class="">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"> Dashboard  </a></li>
    <li class="">
 Payment</li>
           
</ul>
</div>
           <section class="panel">
              
              <!--   <div   id="demo" class="collapse" > -->

                <div class="col-lg-14"> 
                   
                         <form action="<?php echo base_url() ?>/backend/payment_receipts/payment_list/0" method="get" accept-charset="utf-8">

                            <label class="col-md-2 no-padding-left">
                                <input type="text" name="transaction_id" value="<?php echo $this->input->get('transaction_id'); ?>" class="form-control" placeholder="Transaction Id">
                          </label>
                           <label class="col-md-2 no-padding-left users_m" >
                                <input type="text" name="job_id" value="<?php echo $this->input->get('job_id'); ?>" class="form-control" placeholder="Job Id">
                            </label>
                            
                           <?php
                            $ASC= $DESC="";  
                            if($this->input->get('order')) {
                                if($this->input->get('order')=="DESC")
                                {
                                  $DESC='selected';
                                }
                                else{
                                  $ASC='selected';
                                }
                              }                          
                        
                            ?>

                          <label class="col-md-2 no-padding-left verifys" >
                          <select name="order" class="form-control">
                            <option value="DESC" <?php  echo $DESC; ?>>New</option>
                           <option value="ASC" <?php  echo $ASC; ?>>Old</option>  
                            </select>
                          </label>
                          
                          <button type="submit" class="btn btn-primary" title="Search" data-toggle="tooltip"><i class="fa fa-search" aria-hidden="true" ></i>
                          </button>
                          <a href="<?php echo current_url() ?>" title="Refresh" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i>
                          </a>
                        </form>
                        
                        </div>
               <header class="panel-heading"  ></header>
                 <form method="post" action="" id="" name="" >
                  <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                    <tr>
                    <th width="100">
                       S.No.
                    </th>
                    <th>Payment Id</th>
                    <th>Item</th>
                    <th width="6%;">Job Id</th>
                    <th width="10%;">Transaction Id</th>              
                    <th width="11%;">Yolomd Verify</th>
                    <th width="11%;">Payment Amount</th>
                    <th width="12%;"><i class="fa fa-calendar" aria-hidden="true"></i>
                    Payment Date</th>
                    <th width="5%;">Status</th>
                    
                     
                     <th width="10%;">Receipt</th>   
                  </tr>
                    </thead>
                    <tbody>
                    <?php
                   // echo '<pr>';
                  //  print_r($result_data);
               $i= $offset + 1;
               if($result_data != "")
               {
                $jk=0;
                foreach ($result_data as $res_data)
                {
                  $jk++;
                ?>
                      <tr>
                      <td>
                        
                      <?php echo $jk; ?>.
                      </td>
                      <td>
                        
                      <?php if(!empty($res_data['payment_id'])) echo '#'.$res_data['payment_id']; ?>
                      </td>
                        <td><?php if(!empty($res_data['listing_title'])) echo $res_data['listing_title']; ?></td>
                        <td><a href="<?php echo base_url();?>backend/jobs/postjob_detail/<?php if(!empty($res_data['job_id'])) echo $res_data['job_id']; ?>"><?php if(!empty($res_data['job_id'])) echo '#'.$res_data['job_id']; ?></a></td>
                        <td>
                              <?php if(!empty($res_data['txn_id'])) echo $res_data['txn_id']; ?>
                        </td>
                        <td>
                         <?php if(!empty($res_data['yolomd_verify'])){ 
                          if($res_data['yolomd_verify']!="0.00"){ echo '$'.$res_data['yolomd_verify'];}else{ echo '-';}} ?>
                        </td>
                         <td><?php if(!empty($res_data['payment_amount'])) echo '$'.$res_data['payment_amount']; ?></td>
                        
                       <td> <?php if(!empty($res_data['date_time'])) echo $res_data['date_time']; ?> </td>
                        
                       
                        <!--   <td></td> -->
                         <td> 

                         <?php 
                         $payment_status = get_payment_status($res_data['unique_id']);
                         if($payment_status == 1) {?>
                           <a href="javascript:void(0);" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Active" rel="tooltip"  data-placement="top" data-original-title="Active" >Active</a>
                        <?php } if($payment_status == 0) {?>
                          <a href="<?php echo site_url();?>backend/payment_receipts/change_status/<?php if(!empty($res_data['unique_id'])) echo $res_data['unique_id']; ?>/1/<?php if(!empty($res_data['status'])) echo $res_data['status']; ?>" class="btn btn-danger btn-xs tooltips" rel="tooltip"   onclick="return confirm('Are you sure, do you want to change payment status?')" data-toggle="tooltip" title="Make Active" data-placement="top" data-original-title="Make Active"  >Pending</a>
                        <?php } ?>
                         </td>
                         <td>
                        <a target="_blank" class="btn btn-primary btn-xs tooltips" href="<?php echo site_url(); ?>backend/jobs/receipts_id_post/<?php if(!empty($res_data['payment_id'])) echo $res_data['payment_id']; ?>" class="button" >Receipt</a>
                        </td>
                       
                      </tr>

                      <?php $i++;} }else{?>
                        <tr ><td colspan="9" class="error" style="text-align:center; color:red;"><h4>Payment detail record not found.</h4></td></tr>
                <?php } ?>
                </tbody>
                
              </table>
              <?php if(!empty($pagination)) echo $pagination;?>
                   </form>
                
               
 
  </div>
  <script>

 function conformmessages()
 {
  var checkedValue = document.querySelector('input:checked').value;
       
  var k= ($("#moreaction").val());

  if(k==5 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Delete?");
  }
  else{
    return false;
  }
  
}


$("#select_all").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
$('#moreaction').on('change', function(e){
    conformmessages();
    $("#apply").val(1);
    this.form.submit()
  });
</script>


 
  