<div class="bread_parent">
<div class="col-md-12">
  <ul class="breadcrumb">
    
      <?php
      if(superadmin_logged_in()===TRUE)
      {
      ?>
       <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
      <?php } ?>
       <li><a href="<?php echo base_url('backend/messages');?>"></i>Flagged Messages</a></li>  

       <li><b>Message Detail</b></li>
       <div style="float:right;">
        <?php if(!empty($first_msg->admin_marked) && $first_msg->admin_marked == 1) {?>
                           <a href="javascript:void(0);" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Marked" rel="tooltip"  data-placement="top" data-original-title="Marked" >Marked</a>
                        <?php } if(!empty($first_msg->admin_marked) && $first_msg->admin_marked == 0) {?>
                          <a href="<?php echo site_url();?>backend/messages/change_status/<?php if(!empty($first_msg->msg_id)) echo $first_msg->msg_id; ?>/1" class="btn btn-danger btn-xs tooltips" rel="tooltip"   onclick="return confirm('Are you sure, do you want to marked completed?')" data-toggle="tooltip" title="Make Marked" data-placement="top" data-original-title="Make Marked"  >Unmarked</a>
                        <?php }?>
       </div>
  </ul>
</div>

<div class="clearfix"></div>
</div>
<?php 
if($messages_data != "" || $msg_subject != "")
{
   $job_info = '';
  if(!empty($first_msg->job_id))
   $job_info = get_job_detail($first_msg->job_id);
?>
<div class="">
 <div class="row">
                 <div class="col-md-4">
                  <section class="panel">
                          <header class="panel-heading">Job Detail</header>      
                  <table class="responsive table table-striped">
                          <tr><th width="150;">Message Subject : </th><td><?php if(!empty($msg_subject)) echo $msg_subject; ?></td></tr>
                           <tr>
                          <th>
                            Job Id : 
                          </th>
                          <td>
                             <?php 
                                   if(!empty($first_msg->job_id)) echo '#'.$first_msg->job_id; 
                                 ?>
                          </td>
                        </tr> 
                          <tr><th width="120;">Listing title : </th><td><?php if(!empty($first_msg->job_id)) echo get_job_name($first_msg->job_id); ?></td></tr>
                          <tr>
                          <th>
                            Medical Specialties :
                          </th>
                          <td>
                            <?php if(!empty($job_info->medical_specialty_name)) echo get_medical_specialty($job_info->medical_specialty_name); ?>
                          </td>
                        </tr>
                         <?php 
                        if(!empty($job_info->facility_name))
                        {
                        ?>
                        <tr>
                          <th>
                            Facility Name :
                          </th>
                          <td>
                             <?php if(!empty($job_info->facility_name)) echo $job_info->facility_name; ?>
                                    
                          </td>
                        </tr>
                        <?php } 
                        if(!empty($job_info->facility_state))
                        {
                        ?>
                        <tr>
                            <th>
                              Province :
                            </th>
                            <td>
                               <?php if(!empty($job_info->facility_state)) echo get_province_name($job_info->facility_state); ?>
                                      
                            </td>
                          </tr>
                          <?php } 
                          if(!empty($job_info->facility_city))
                          {
                          ?>
                          <tr>
                            <th>
                              City :
                            </th>
                            <td>
                              <?php if(!empty($job_info->facility_city)) echo $job_info->facility_city; ?>
                                      
                            </td>
                          </tr>
                          <?php } 
                          if(!empty($job_info->facility_address))
                          {
                          ?>
                          <tr>
                            <th>
                              Street number and name :
                            </th>
                            <td>
                               <?php if(!empty($job_info->facility_address)) echo $job_info->facility_address; ?>
                                      
                            </td>
                          </tr>
                         <?php } 
                          if(!empty($job_info->facility_suite_office))
                          {
                          ?>
                          <tr>
                            <th>
                              Suite or Office number :
                            </th>
                            <td>
                             <?php if(!empty($job_info->facility_suite_office)) echo $job_info->facility_suite_office; ?>
                                      
                            </td>
                          </tr>
                          <?php } 
                          if(!empty($job_info->facility_postal_code))
                          {
                          ?>
                          <tr>
                            <th>
                              Postal Code :
                            </th>
                            <td>
                              <?php if(!empty($job_info->facility_postal_code)) echo $job_info->facility_postal_code; ?>
                                      
                            </td>
                          </tr>
                          <?php } ?>
                         <tr>
                        <th>
                         Created Time :
                        </th>
                        <td>
                           <?php if(!empty($job_info->created)) echo $job_info->created; ?>
                        </td>
                     
                        </tr> <tr>
                        <th>
                          Updated On
                        </th>
                        <td>
                           <?php if(!empty($job_info->modified)) echo $job_info->modified; ?>
                        </td>
                       </tr> <tr>

                        <th>
                          Posted On
                        </th>
                        <td>
                           <?php if(!empty($job_info->posted)) echo $job_info->posted; ?>
                        </td>
                       </tr> 
                          </table>
                   </section>
                 </div>
                  <div class="col-md-8">
                        <section class="panel" >

                          <header class="panel-heading">
                              Messages
                              
                          </header>
                          <div class="panel-body">
                          <div class="timeline-messages">
                          <?php if($messages_data){ 
                            foreach($messages_data as $messages){
                            ?>
                               <!-- <h5 class="pull-right">12 August 2013</h5>-->
                               <?php if(!empty($first_msg->sender_id) && $messages->sender_id == $first_msg->sender_id) {?>
                              

                              <div class="msg-time-chat">
                                      
                                      <div class="message-body msg-in">
                                          <span class="arrow"></span>
                                          <div class="text">
                                              <p class="attribution"><a href="#">Sender : <?php 
                                               $user_info = '';
                                                $user_info = get_user_info($messages->sender_id);
                                                if(!empty($user_info->user_first_name) || !empty($user_info->user_last_name))
                                                {
                                                    echo ucfirst($user_info->user_first_name).' '.$user_info->user_last_name;
                                                }
                                              ?></a> at <?php if(!empty($messages->date_time)) echo $messages->date_time;?></p>
                                              <p><?php if(!empty($messages->msg_body)) echo $messages->msg_body;?></p>
                                          </div>
                                      </div>
                                </div>

                              <?php }elseif($messages->sender_id != $first_msg->sender_id){ ?>
                              
                              <div class="msg-time-chat">
                                     
                                      <div class="message-body msg-out">
                                          <span class="arrow"></span>
                                          <div class="text">
                                              <p class="attribution"> <a href="#">Reciever : <?php 
                                               $user_info = '';
                                                $user_info = get_user_info($messages->sender_id);
                                                if(!empty($user_info->user_first_name) || !empty($user_info->user_last_name))
                                                {
                                                    echo ucfirst($user_info->user_first_name).' '.$user_info->user_last_name;
                                                }
                                              ?></a> at <?php if(!empty($messages->date_time)) echo $messages->date_time;?></p>
                                              <p><?php if(!empty($messages->msg_body)) echo $messages->msg_body;?></p>
                                          </div>
                                      </div>
                                  </div>









                              <?php } ?>
                              <?php } }?>
                          </div>
                          </div>
                      </section>
                  </div>
                 
              </div>


           
</div>
<?php }else{?>
<div class="">
 <div class="row">
                  <div class="col-md-12">
                      <!--user info panel-body table start-->
                      <section class="panel">
                          <div class="">
                              <div class="task-thumb-details col-md-12">
                                
                                      <h1 style="color:red;">Flagged Message not present.</h1>
                            
                              </div>
                          </div>
                       </section>
                  </div>
</div>
</div>                           
<?php } ?>
