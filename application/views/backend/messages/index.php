<div class="bread_parent">
  <ul class="breadcrumb">
       <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
       <li><b>Flagged messages</b></li>
  </ul>
  </div>
  
  <div class="clearfix"></div> 
    <section class="panel">
     <div class="col-lg-14"> 
        <form action="<?php echo site_url() ?>backend/messages/index/0" method="get" accept-charset="utf-8">
          <label class="col-lg-3">
            <input type="text" name="MsgId" value="<?php echo $this->input->get('MsgId'); ?>" class="form-control" placeholder="Search By Msg Id">
          </label>
           <label class="col-lg-3">
            <input type="text" name="JobId" value="<?php echo $this->input->get('JobId'); ?>" class="form-control" placeholder="Search By Job Id">
          </label>
          <?php
            $Marked= $Unmarked="";  
            if($this->input->get('status')) {
                if($this->input->get('status')=="1"){
                  $Marked='selected';}
                if($this->input->get('status')=="2"){
                  $Unmarked='selected';}
              }
          ?>
          <label>
            <select name="status" class="form-control">
             <option value="All">Marked Status</option>
             <option value="1" <?php  echo $Marked; ?>>Marked </option>
              <option value="2" <?php  echo $Unmarked; ?>>Unmarked </option>              
             
            </select>
          </label>
          <?php
            $ASC= $DESC="";  
            if($this->input->get('order')) {
                if($this->input->get('order')=="DESC"){
                  $DESC='selected';}
                else{
                  $ASC='selected';}
              }
          ?>
          <label>
            <select name="order" class="form-control">
             <option value="DESC" <?php  echo $DESC; ?>>New </option>
              <option value="ASC" <?php  echo $ASC; ?>>Old </option>              
             
            </select>
          </label>
          <label class="search-label">
           
               <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
            <a href="<?php echo current_url() ?>" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i></a>
          </label>
        </form>
      </div>
      <header class="panel-heading"  ></header>
      <table id="datatable_example" class="table table-striped table-hover" >
        <thead class="thead_color">
          <tr>
           <th width="5%">S.No</th>
            <th width="5%">Msg Id</th>
            <th width="5%">Job Id</th>
            <th width="10%">Sent by</th>
            <th width="10%">Recived by</th>
            <th width="10%">Marked Status</th>  
            <th width="15%"><i class="fa fa-calendar"></i> Marked date</th>
            <th width="5%">Actions</th>
          </tr>
        </thead>
          <?php  
          if(!empty($messages)):
            $j=$offset+1; 
          foreach($messages as $row): 
          ?>
            <tbody>
            <tr>
                <td><?php echo $j ?></td>
                <td><a href="<?php echo base_url().'backend/messages/get_message_detail/'.$row->msg_id ?>" ><?php if(!empty($row->msg_id)) echo '#'.$row->msg_id; ?></a></td>
                <td> <a href="<?php echo base_url().'backend/jobs/postjob_detail/'.$row->job_id ?>" ><?php if(!empty($row->job_id)) echo '#'.$row->job_id; ?></a></td>
                <td><?php 
                
                $sender_info = '';
                $sender_info = get_user_info($row->sender_id);
                if(!empty($sender_info->user_role) && $sender_info->user_role == 1)
                {
                  ?>
                  <a href="<?php echo base_url().'backend/listusers/edit_medical/'.$sender_info->user_id ?>">
                  <?php if(!empty($row->sender_id)) echo '#'.$row->sender_id.'|<br/>'.$sender_info->user_first_name.' '.$sender_info->user_last_name; ?>
                   </a>
                  <?php
                }elseif(!empty($sender_info->user_role) && $sender_info->user_role == 2)
                {

                 ?>
                  <a href="<?php echo base_url().'backend/listusers/edit_physician/'.$sender_info->user_id ?>">
                  <?php if(!empty($row->sender_id)) echo '#'.$row->sender_id.'|<br/>'.$sender_info->user_first_name.' '.$sender_info->user_last_name; ?>
                   </a>
                  <?php
                }
                ?>
                 </td>
                <td>
                <?php 
                
                $receiver_info = '';
                $receiver_info = get_user_info($row->receiver_id);
                if(!empty($receiver_info->user_role) && $receiver_info->user_role == 1)
                {
                  ?>
                  <a href="<?php echo base_url().'backend/listusers/edit_medical/'.$receiver_info->user_id ?>">
                  <?php if(!empty($row->receiver_id)) echo '#'.$row->receiver_id.'<br/>'.$receiver_info->user_first_name.' '.$receiver_info->user_last_name; ?>
                   </a>
                  <?php
                }elseif(!empty($receiver_info->user_role) && $receiver_info->user_role == 2)
                {

                 ?>
                  <a href="<?php echo base_url().'backend/listusers/edit_physician/'.$receiver_info->user_id ?>">
                  <?php if(!empty($row->receiver_id)) echo '#'.$row->receiver_id.'<br/>'.$receiver_info->user_first_name.' '.$receiver_info->user_last_name; ?>
                   </a>
                  <?php
                }
                ?>
                  
                </td>
                <td>
                  <?php 
                   if($row->admin_marked == 1) {?>
                     <a href="javascript:void(0);" class="btn btn-success btn-xs tooltips" data-toggle="tooltip" title="Marked" rel="tooltip"  data-placement="top" data-original-title="Marked" >Marked</a>
                  <?php } if($row->admin_marked == 0) {?>
                    <a href="<?php echo site_url();?>backend/messages/change_status/<?php if(!empty($row->msg_id)) echo $row->msg_id; ?>/1" class="btn btn-danger btn-xs tooltips" rel="tooltip"   onclick="return confirm('Are you sure, do you want to marked completed?')" data-toggle="tooltip" title="Make Marked" data-placement="top" data-original-title="Make Marked"  >Unmarked</a>
                  <?php }?>
                       
                  
                </td>
                  <td class="to_hide_phone"><?php echo date('d M Y,h:i  A',strtotime($row->flagged_date)); ?></td>
                <td class="ms">
                    <a href="<?php echo base_url().'backend/messages/get_message_detail/'.$row->msg_id ?>" class="btn btn-primary btn-xs tooltips" rel="tooltip"  data-placement="left" data-original-title="Message detail" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                   
                </td>
              </tr> 
            </tbody> 
          <?php $j++;  endforeach; ?>
        <?php else: ?>
          <tr>
           <th colspan="7" class="msg"> <center>No Flagged Message Found.</center></th>
          </tr>
        <?php endif; ?> 
    </table>
    <?php echo $pagination;?>
    </section>
  
  </div>

<script type="text/javascript" >
 $(document).ready(function(){
  setTimeout(function(){
  $(".flash").fadeOut("slow", function () {
  $(".flash").remove();
      }); }, 5000);
 });
 function get_message()
{

}
</script>
<!--Message center modal-->

<div class="modal fade custom-modal" id="messageCenter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog  modal-large" role="document">
    <div class="modal-content" style="min-height:30px!important;">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
     
      <div class="modal-body" >
       <div class="themeform">
        <section class="">
     <div class="">
<!--       <h3 class="heading">{{listing_title}}</h3> -->

      
<!--    <h3 class="header"><img src="<?php //echo FRONTEND_THEME_URL ?>/images/icons/my-message.png" class="">Messages</h3> -->
   <div class="message-center">
   <div class="clearfix message-header">
     <div class="message-title pull-left">
     {{listing_title}}
     </div>
     <span class="city-name pull-right">
     <i class="fa fa-map-marker" aria-hidden="true"></i> {{facility_city}}
     </span>
     <span class="pull-right">
     <img src="<?php echo FRONTEND_THEME_URL ?>images/medal-new.svg" width="15"> {{medical_specialty}}
     </span>
   </div>

   <div class="message-subject" ng-if="msg_subject">
   <img src="<?php echo FRONTEND_THEME_URL ?>images/msg-mark.svg" width="15" > {{msg_subject}}</div>
   <div class="table-scroll">
  <table class="table table-hover table-striped message-table table-condensed">
     <tr ng-repeat="messages in messages" ng-if="messages">
     <td>    
       <div class="message-body">
       <div ng-if="messages.msg_receive" class="icon-block">
          <span class="icon"><img src="<?php echo FRONTEND_THEME_URL ?>images/incoming.svg" width="15"></span>
       </div> 
       <div ng-if="messages.msg_receive==0" class="icon-block">
       <span class="icon"><img src="<?php echo FRONTEND_THEME_URL ?>images/outgoing.svg" width="15"></span> 
       </div>

       <div class="message-text">
        {{messages.msg_body}}
          <div class="clearfix"></div>
           <div class="msg-utility">
            <i class="fa fa-calendar" aria-hidden="true"></i> {{messages.date_time}}
           </div>
       </div>
       </div>

     </td>
     </tr>
   </table>
   </div>
   <div>

             <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="insertmsg()" >
             <div class="row">
              <div class="form-group col-md-12">
               <textarea class="form-control" rows="6" data-bvalidator="required" data-bvalidator-msg="Message is Required" ng-model="replymsg.reply" placeholder="Type your Message"></textarea>
              </div>
             </div>
             <button type="submit" class="btn btn-theme">Send</button>
             </form>

   </div>
   </div>



     </div>
        </section>
       </div>
      </div>
    </div>
  </div>
</div>