<div class="bread_parent">
<div class="col-md-12">
  <ul class="breadcrumb">
      <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
       <li><a href="<?php echo base_url('backend/faq/');?>"><b>FAQ</b></a></li>
       <li><b>Faq Add</b></li>
  </ul>
</div>
<div class="clearfix"></div>
</div> <br>
<div class="panel-body ">
<div class="tab-pane row-fluid fade in active" id="tab-1">
<form role="form" class="form-horizontal tasi-form" action="<?php echo current_url()?>" enctype="multipart/form-data" method="post" id="form_valid">
  <div class="form-body">
    <div class="form-group">
      <label class="col-md-3 control-label">Question <span class="mandatory">*</span></label>
      <div class="col-md-6">
        <input type="text" placeholder="Question" class="form-control" name="question" value="<?php echo set_value('question');?>" data-bvalidator="required" data-bvalidator-msg="Question required"><?php echo form_error('question'); ?>
      </div>
    </div> 
    <div class="form-group">
      <label class="col-md-3 control-label">Answer <span class="mandatory">*</span></label>
      <div class="col-md-6">
        <textarea name="answer" rows="10" class="tinymce_edittor form-control" placeholder="Answer" data-bvalidator="required" data-bvalidator-msg="Answer required"><?php echo set_value('answer');?></textarea>
        <?php echo form_error('answer'); ?>
      </div>
    </div> 
    <div class="form-group">
      <label class="col-md-3 control-label">Status<span class="mandatory">*</span></label>
      <div class="col-md-6">
        <select name="status" class="form-control ">
          <option value="1">Publish</option>
          <option value="0">Unpublish</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">Order By<span class="mandatory">*</span></label>
      <div class="col-md-6">
        <input type="number" min='1' placeholder="Please input numeric values like-1,2,3.." class="form-control" name="order_by" value="<?php echo set_value('order_by');?>" data-bvalidator="required" data-bvalidator-msg="Question Order By required"><?php echo form_error('order_by'); ?>
      </div>
    </div>
  <div class="form-actions fluid">
    <div class="col-md-offset-2 col-md-10">
      <a class="btn btn-danger tooltips" rel="tooltip" data-placement="top" data-original-title="Back to FAQ" href="<?php echo base_url('backend/faq/');?>"><i class="icon-remove"></i> Back</a>                         
      <button  class="btn btn-primary tooltips" rel="tooltip" data-placement="top" data-original-title="Add New Question" type="submit"> <i class="icon-plus"></i> Add Question</button>
      </div>
    </div>
  </form>
</div>                     
</div>


