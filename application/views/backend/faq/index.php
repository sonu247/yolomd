<div class="bread_parent">
  <div class="col-md-11">
  <ul class="breadcrumb">
      <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
       <li><b>FAQ</b></li>
  </ul>
  </div>
  <div class="col-md-1">
    <div class="btn-group pull-right" >
      <a class="btn btn-primary tooltips" href="<?php echo base_url('backend/faq/faq_add');?>" id="add" data-original-title="Click to add the New Question"><i class="icon-plus">
          Add Question &nbsp;</i>
      </a>
    </div>
  </div>
  <div class="clearfix"></div> 
    <div class="panel-body" >     
       <div class="clearfix"></div>  
    <div class="adv-table">
      <table id="datatable_example" class="responsive table table-striped" >
        <thead class="thead_color">
          <tr>
            <th  width="2%">#</th>
            <th  >Question(s)</th>
            <th  >Answer(s)</th>
            <th  width="6%">Order By</th>  
            <th  width="5%">Status</th>
            <th  width="10%">Created</th>
            <th width="130">Actions</th>
          </tr>
        </thead>
          <?php  
          if(!empty($faq)):
            $j=$offset+1;  foreach($faq as $row): 
          ?>
            <tbody>
            <tr>
                <td><?php echo $j?></td>
                <td><?php echo character_limiter($row->question,100,'...') ?> ?</td>
                <td><?php echo character_limiter($row->answer,100,'...') ?></td>
                <td><?php echo $row->order_by ?></td>
                </td>
                <td> 
                <?php if($row->status==1){ ?>
                <a class="label label-success label-mini tooltips" href="<?php echo base_url('backend/faq/changeuserstatus_t/'.$row->id.'/'.$row->status.'/0/faq')?>"  rel="tooltip" data-placement="top" data-original-title="Click to Unpublish" >Publish</a> 
                <?php } 
                else if($row->status==0){ ?><a class="label label-warning label-mini tooltips"  href="<?php echo base_url('backend/faq/changeuserstatus_t/'.$row->id.'/'.$row->status.'/0/faq')?>" rel="tooltip" data-placement="top" data-original-title="Click to publish" >Unpublish</a> 
                <?php } ?>
                </td>
                <td class="to_hide_phone"><i class="fa fa-calendar"></i> <?php echo date('d M Y,h:i  A',strtotime($row->created)); ?></td>
                <td class="ms">
                    <a href="<?php echo base_url().'backend/faq/faq_edit/'.$row->id ?>" class="btn btn-primary btn-xs tooltips" rel="tooltip"  data-placement="left" data-original-title="Edit This Question" >Edit <i class="icon-pencil"></i></a>
                    <a href="<?php echo base_url().'backend/faq/faq_delete/'.$row->id ?>" class="btn btn-danger btn-xs tooltips" rel="tooltip"  data-placement="top" data-original-title="Delete This Question" onclick="if(confirm('Are you sure want to delete?')){return true;} else {return false;}" >Delete <i class="icon-trash "></i></a> 
                </td>
              </tr> 
            </tbody> 
          <?php $j++;  endforeach; ?>
        <?php else: ?>
          <tr>
           <th colspan="6" class="msg"> <center>No Question found.</center></th>
          </tr>
        <?php endif; ?> 
    </table>
    </div> 
  </div> 
</div> 
<?php echo $pagination;?>