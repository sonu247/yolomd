<div class="bread_parent">
<div class="col-md-12">
  <ul class="breadcrumb">
      <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="icon-home"></i> Dashboard  </a></li>  
       <li><a href="<?php echo base_url('backend/faq/');?>"><b>FAQ</b></a></li>
       <li><b>Faq Edit</b></li>

  </ul>
</div>
<?php if ($faq) {
  foreach ($faq as $row) {
  ?>
<div class="clearfix"></div>
</div> <br>
<div class="panel-body ">
  <div class="tab-pane row-fluid fade in active" id="tab-1">
    <form role="form" class="form-horizontal tasi-form" action="<?php echo current_url()?>" method="post" id="form_valid">
      <div class="form-body">
        <div class="form-group">
          <label class="col-md-3 control-label">Last Updated</label>
          <div class="col-md-6"><?php echo date('d M Y,h:i  A',strtotime($row->updated)); ?></div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Question  <span class="mandatory">*</span></label>
          <div class="col-md-6">
            <input type="text" placeholder="Question" class="form-control" name="question" value="<?php if ($this->input->post('question')){ echo set_value('question'); } else { echo $row->question;} ?>" data-bvalidator="required" data-bvalidator-msg="Question required"><?php echo form_error('question'); ?>
          </div>
        </div> 
        <div class="form-group">
          <label class="col-md-3 control-label">Answer <span class="mandatory">*</span></label>
          <div class="col-md-6">
            <textarea name="answer" rows="8" class="tinymce_edittor form-control" placeholder="Answer" data-bvalidator="required" data-bvalidator-msg="Blog Short Description required"><?php if ($this->input->post('answer')){ echo set_value('answer'); } else { echo $row->answer;} ?></textarea>
            <?php echo form_error('answer'); ?>
          </div>
        </div> 
        <div class="form-group">
          <label class="col-md-3 control-label">Order By <span class="mandatory">*</span></label>
          <div class="col-md-6">
            <input type="number" min='1' placeholder="Please input numeric values like-1,2,3.." class="form-control" name="order_by" value="<?php if ($this->input->post('order_by')){ echo set_value('order_by'); } else { echo $row->order_by;} ?>" data-bvalidator="required" data-bvalidator-msg="Question Order By required"><?php echo form_error('order_by'); ?>
          </div>
        </div>
        <div class="form-actions fluid">
          <div class="col-md-offset-2 col-md-10">
            <a class="btn btn-danger tooltips" rel="tooltip" data-placement="top" data-original-title="Back to FAQs" href="<?php echo base_url('backend/faq/');?>"><i class="icon-remove"></i> Back</a>                              
            <button  class="btn btn-info tooltips" rel="tooltip" data-placement="top" data-original-title="Update Question" type="submit"> <i class="fa fa-refresh"></i> Update Question</button>
          </div>
        </div>
    </form>
  </div>                     
</div>
<?php 
}
}
?>