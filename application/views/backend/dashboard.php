<ul class="breadcrumb">
    <li><a  href="<?php echo base_url('backend/superadmin/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>   
</ul>
<header class="panel-heading">
 Hello <?php echo superadmin_name();?>, Welcome to the admin section of  <?php echo SITE_NAME ?> ............. <i class="fa fa-smile-o" aria-hidden="true"></i>

</header>
<div class="panel-body">
<div class="col-md-10 col-md-offset-1 no-padding">
<div class="dashboard-toggle">
  <div class="col-md-4">
  	<div class="toggle-block">
  	 <h4 class="head"><i class="fa fa-user"></i> Users Managment</h4>
  	 <ul>
  	 	<li><a href="<?php echo base_url('backend/listusers/physician') ?>">Doctor's Section</a></li>
  	 	<li><a href="<?php echo base_url('backend/listusers/medical') ?>">Medical User</a></li>	
  	 </ul>
  	</div>
  </div>
  <div class="col-md-4">
  	<div class="toggle-block">
  	 <h4 class="head"><i class="fa fa-question-circle" aria-hidden="true"></i> FAQ Managment</h4>
  	 <ul>
  	 	<li><a href="<?php echo base_url('backend/faq') ?>">Faq</a></li>
  	 	<li><a href="<?php echo base_url('backend/faq/faq_add') ?>">Faq Add</a></li>	
  	 </ul>
  	</div>
  </div>
  <div class="col-md-4">
    <div class="toggle-block">
     <h4 class="head"><i class="fa fa-user"></i>  Profile Managment</h4>
     <ul>
      <li><a href="<?php echo base_url()?>backend/superadmin/profile">My Profile</a></li>
      <li><a href="<?php echo base_url()?>backend/superadmin/change_password">Change Password</a></li> 
     </ul>
    </div>
  </div>  
  <!-- <div class="col-md-4">
  	<div class="toggle-block">
  	 <h4 class="head"><i class="fa fa-camera" aria-hidden="true"></i> Photographer</h4>
  	 <ul>
  	 	<li><a href="<?php //echo base_url('backend/photographer') ?>">Photographers</a></li>
  	 	<li><a href="<?php //echo base_url('backend/photographer/verified_jobs') ?>">Verified Job</a></li>
  	 </ul>
  	</div>
  </div> -->
  <div class="clearfix"></div>
  <br>
  <div class="col-md-4">
  	<div class="toggle-block">
  	 <h4 class="head"><i class="fa fa-graduation-cap"></i> Jobs Managment</h4>
  	 <ul>
  	 	<li><a href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=5&order=DESC&verify=All'); ?>">Pending Jobs</a></li>
  	 	<li><a href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=1&order=DESC&verify=All'); ?>">Active Jobs</a></li>
  	 	<li><a href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=2&order=DESC&verify=All'); ?>">Deactive Jobs</a></li>
  	 	<li><a href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=3&order=DESC&verify=All'); ?>">Banned Jobs</a></li>
  	 	<li><a href="<?php echo base_url('backend/jobs/index/0?Id=&userid=&title=&specialty=&FacilityName=&status=4&order=DESC&verify=All'); ?>">Jobs In Draft</a></li>
  	 	<li><a href="<?php echo base_url('backend/jobs') ?>">All Jobs</a></li>  	 	
  	 </ul>
  	</div>
  </div>
  <div class="col-md-4">
  	<div class="toggle-block">
  	 <h4 class="head"><i class="fa fa-laptop"></i> Content Management</h4>
  	 <ul>
  	 	<li><a href="<?php echo base_url('backend/cms/home_page/1') ?>">Home Page</a></li>
  	 	<li><a href="<?php echo base_url('backend/cms/footer_menu') ?>">Footer Menu</a></li>
  	 	<li><a href="<?php echo base_url('backend/cms/appex1') ?>">Canadian Schools</a></li>
  	 	<li><a href="<?php echo base_url('backend/cms/appex2') ?>">Medical Specialties List</a></li>
  	 	<li><a href="<?php echo base_url('backend/cms/signup_content') ?>">Doctor &amp; Medical Content</a></li>
<!--   	 	<li><a href="<?php //echo base_url('backend/cms/city_manage') ?>">City Manage</a></li>  -->
  	 	<li><a href="<?php echo base_url('backend/cms/create_content') ?>">Page Content</a></li>   	 	 	 	
  	 </ul>
  	</div>
  </div>
   <div class="col-md-4">
  	<div class="toggle-block">
  	 <h4 class="head"><i class="fa fa-bookmark" aria-hidden="true"></i> Others</h4>
  	 <ul>

<!--   	 	<li><a href="<?php //echo base_url('backend/payment_receipts/payment_list') ?>">Payment Receipts</a></li> -->
  	 	<li><a href="<?php echo base_url('backend/email_templates') ?>">Email Templates</a></li>
  	 	<li><a href="<?php echo base_url() ?>backend/messages">Flagged Messages</a></li>
  	 	<li><a href="<?php echo base_url() ?>backend/contactus">Support</a></li>
  	 	<li><a href="<?php echo base_url('backend/cms/setting') ?>">Setting</a></li> 
      <li>&nbsp;</li>
      <li>&nbsp;</li>
            <li>&nbsp;</li>

  	 </ul>
  	</div>
  </div>

</div>
</div>


</div>

<style>
.panel-body {
  background-color: #ecf0f5;
}
 </style>



