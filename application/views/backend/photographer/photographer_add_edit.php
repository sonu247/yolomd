<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"> Dashboard  </a></li>
    <li><a href="<?php echo base_url('backend/photographer');?>"> Photographers</a></li>
    <li class=""> <?php if(!empty($title)) echo $title; ?></li>
</ul>
</div>
  <?php $status_array = array('1'=>'Active','2'=>'Deactive','3'=>'Banned');?>
<div class="row">
     <div class="col-lg-12">
        <section class="panel">
           <header class="panel-heading heading_class"  ><i class="fa fa-camera"></i> 
          Photographer Detail
          <div class="pull-right">
          <div class="dropdown">
          <?php if(!empty($check_id))
                              {?>
                              <button class="<?php
                                 foreach($status_array as $k => $status_a)
                                  {
                                    if(!empty($user_details->user_status) && $k == $user_details->user_status)
                                    {
                                 
                            if($k == 1)
                             echo 'btn btn-success';
                            elseif($k == 2)
                              echo 'btn btn-danger';
                             elseif($k == 3)
                                echo 'btn btn-info';
                              else
                                echo '';
                            }
                          }
                              ?> btn-xs  dropdown-toggle_get_val" id="menu1" type="button" data-toggle="dropdown"><?php 
                              foreach($status_array as $k => $status_a)
                              {
                                if(!empty($user_details->user_status) && $k == $user_details->user_status) 
                                  echo $status_a;
                              }
                               ?>
                              <span class="caret"></span></button>
                              
                              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                 <?php
                              
                                 foreach($status_array as $k => $status_a)
                                  {
                                    if(!empty($user_details->user_status) && $k != $user_details->user_status)
                                    {
                                ?>

                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url();?>backend/photographer/change_status/<?php if(!empty($user_details->user_id)) echo $user_details->user_id; ?>/<?php echo $k; ?>/3"><?php echo $status_a; ?></a></li>
                                <?php } }  ?>
                                
                              </ul>
                              <?php } ?>
                               <?php 
                              if(!empty($check_id))
                              {
                            ?>
                               <a target="_new" href="<?php echo base_url();?>backend/superadmin/login_photographer/<?php if(!empty($user_details->user_id)) echo $user_details->user_id ?>" class="btn btn-warning btn-xs tooltips" rel="tooltip"  data-placement="top" data-original-title="Proxy Login"><div  data-toggle="tooltip" title="Proxy Login">Proxy Login</div></a>
                                
                
                               <button type="button" style="padding: 0px 11px;" class="btn_tool btn btn-primary btn-xs tooltips" data-toggle="collapse" data-target="#demo" data-toggle="tooltip" title="Update Password">Update Password
                  <span class="fa fa-caret-down"></span> 
                  </button> 
                  <?php } ?>
                              
                       </div> 
</div>
              </header>     
             <div class="panel-body collapse <?php if(!empty($_POST['change_password'])) echo 'in'; else ''; ?>" id="demo" style="padding:0px; ">
                 
                    <div class="col-lg-14"> 
                        <form class="form-horizontal" action="" name="add_menu" id="add_menu" method="post">
                        <h5 class="panel-heading"> <i class="fa fa-cog"></i> Update Password</h5>
                        <div class="col-xs-5">
                       <label class="col-sm-3 col-sm-3 no-padding">New Password<span class="mandatory">*</span></label> 
                       <div class="col-sm-9">
                         <input type="password" placeholder="New Password" class="form-control" name="newpassword" value="">
                          <div class="left_move">
                           <?php echo form_error('newpassword'); ?>
                           </div>
                           </div>
                          <!--  </div> -->
                          
                        </div>
                      
                        <div class="col-xs-5">
                          <label class="col-sm-3 col-sm-3 no-padding">Confirm Password<span class="mandatory">*</span></label> 
                          <!-- radio -->
                            
                             <div class="col-sm-9" style="float:left;">
                                 <input type="password" placeholder="Confirm Password" class="form-control" name="confpassword" value="">
                                <div class="left_move">
                              <?php echo form_error('confpassword'); ?>
                              </div>
                               </div>
                            
                           <!-- end radio -->
                        </div>
                     
                        <div class="col-xs-2 pull-right">
                       
                          <input type="submit" class="btn btn-block btn-primary" value="Update Password" name="change_password">
                        </div>
                        </form>
                        </div>
 
                        </div><!-- /.box-body -->  

          <form role="form" method="post" class="form-horizontal" action="<?php echo current_url()?>">
          <div class ="panel-body">
          
          <div class="row">

          <header class="panel-heading" style="margin-bottom:20px; width:100%; ">
                   <i class="fa fa-registered" aria-hidden="true"></i> <?php if(!empty($title)) echo $title; ?>
                      </header>
                     
            <div class="col-lg-12">
               <div class   ="form-group">
               <label class ="col-md-3 col-md-3">Name Title<span class="mandatory">*</span></label>
               <div class   ="col-sm-9">
                 
                    <input type ="radio" name="user_name_title" id="user_name_title"  value="Mr." <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Mr.') echo  'checked'; else echo '';?>> 
                     <lable style="padding:5px 5px;">Mr.</label>
                
                 
                    <input type ="radio" style="margin-left:20px;" name="user_name_title" id="user_name_title1"  value="Miss" <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Miss') echo  'checked'; else echo '';?>>
                     <lable style="padding:5px 5px;">Miss</label>
                 
                    <input type ="radio" style="margin-left:20px;" name="user_name_title" id="user_name_title2"   value="Mrs." <?php if(!empty($user_details->user_name_title) && $user_details->user_name_title == 'Mrs.') echo  'checked'; else echo '';?>> 
                     <lable style="padding:5px 5px;">Mrs.</label>
                 
                    <div class ="">
                    <?php echo form_error('user_name_title'); ?>
                   </div>
               </div>
              
               </div>
               
               <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">First Name<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="photographer_first_name" placeholder="Photographer First Name" class="form-control" value="<?php if(!empty($user_details->user_first_name)) echo  $user_details->user_first_name; else echo set_value('photographer_first_name');?>">
                     <div class ="left_move">
                      <?php echo form_error('photographer_first_name'); ?>
                     </div>
                   </div>
               </div>
               <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">Last Name<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="photographer_last_name" placeholder="Photographer Last Name" class="form-control" value="<?php if(!empty($user_details->user_last_name)) echo  $user_details->user_last_name; else echo set_value('photographer_last_name');?>">
                     <div class ="left_move">
                      <?php echo form_error('photographer_last_name'); ?>
                     </div>
                   </div>
               </div>
                <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">Email<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="email_address" placeholder="Photographer Email" class="form-control" value="<?php if(!empty($user_details->user_email)) echo  $user_details->user_email; else echo set_value('email_address');?>"   <?php if(!empty($check_id)) echo 'disabled style="background:rgb(238,238,238);"';?>>
                      <?php echo form_error('email_address'); ?>
                   </div>
               </div>
               <?php 
                if(empty($check_id))
                {
               ?>
                
                    <div class   ="form-group">
                      <label class ="col-md-3 col-md-3">Password<span class="mandatory">*</span></label>
                    <div class   ="col-sm-9">
                      <input type="password" placeholder="Password" class="form-control" name="password" value="">
                          <div class="left_move">
                           <?php echo form_error('password'); ?>
                    
                      </div>
                    </div>
                    </div>
                    <div class   ="form-group">
                      <label class ="col-md-3 col-md-3">Confirm Password<span class="mandatory">*</span></label>
                    <div class   ="col-sm-9">
                      <input type="password" placeholder="Confirm Password" class="form-control" name="confpassword" value="">
                                <div class="left_move">
                              <?php echo form_error('confpassword'); ?>
                    
                      </div>
                    </div>
                      </div>
                        
               <?php 
                }
                ?>
               <div class   ="form-group">
                  <label class ="col-md-3 col-md-3">City<span class="mandatory">*</span></label>
                   <div class   ="col-sm-9">
                      <input type="text" name="city" placeholder="City" class="form-control" value="<?php if(!empty($user_details->city)) echo  $user_details->city; else echo set_value('city');?>"   >
                      <?php echo form_error('city'); ?>
                   </div>
               </div>

            </div>
            <!--  <div class="col-lg-12">
             <header class="panel-heading" style="margin-bottom:20px;">
                        Change Password <i class="fa fa-cogs"></i>
                      </header>
              
                        
              </div> -->
              <div class="col-lg-12">
               <header class="panel-heading" style="margin-bottom:20px;">
                      </header>
               <div class="form-group">
               <label class="col-md-3 col-md-3">
                 
               </label>
               <div class="col-sm-9">
               <input class="btn btn-primary" type="submit" name="<?php if(!empty($data_request)) echo $data_request; ?>" value="<?php if(!empty($data_request)) echo $data_request; ?> Photographer Detail">
               </div>
               </div>
              </div>       
          </div>    
          </form>
  </div>
</section>
  </div>
  </div>
<script>
$(document).ready(function(){
    $(".dropdown-toggle_get_val").dropdown();
});
</script>
<script>
$(document).ready(function(){
  $("#demo").on("hide.bs.collapse", function(){
    $(".btn_tool").html('Update Password <span class="fa fa-caret-down"></span>');
  });
  $("#demo").on("show.bs.collapse", function(){
    $(".btn_tool").html('Update Password <span class="fa fa-caret-up"></span>');
  });
});
</script>
<style>
label{
text-align:right;
}
</style>