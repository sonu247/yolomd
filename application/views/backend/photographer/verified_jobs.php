
<style type="text/css">
  .users_m {
    width: 10%;
}
.title_m {
    width: 12%;
}
.specaility_m {
    width: 14%;
}
.facility {
    width: 14%;
}
.statuss{width: 130px}
.verifys{width: 120px}
</style>
<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>">Dashboard  </a></li>
    <li class="">Verified Jobs</li>
</ul>
</div>

            <section class="panel">
              
              <!--   <div   id="demo" class="collapse" > -->

                <div class="col-lg-14"> 
                   
                         <form action="<?php echo base_url() ?>backend/photographer/verified_jobs/0" method="get" accept-charset="utf-8">

                            <label class="col-md-1 no-padding-left">
                                <input type="text" name="Id" value="<?php echo $this->input->get('Id'); ?>" class="form-control" placeholder="Job Id">
                          </label>
                           <label class="col-md-1 no-padding-left users_m" >
                                <input type="text" name="user_name" value="<?php echo $this->input->get('user_name'); ?>" class="form-control" placeholder="User Name">
                            </label>
                            <label class="col-md-2 no-padding-left title_m" >
                                <input type="text" name="title" value="<?php echo $this->input->get('title'); ?>" class="form-control" placeholder="Job Title">
                          </label>                        
                           <label class="col-md-2 no-padding-left specaility_m" >
                                <!--<input type="text" name="specialty" value="<?php echo $this->input->get('specialty'); ?>" class="form-control" placeholder="Medical Specialty">-->
                               <?php $medicalfasility_lists=get_medicalfasility_list();
                                 $specialty=$this->input->get('specialty'); ?>
                           <select name="specialty" class="form-control">
                              <option value="All">Specialty</option> 
                              <?php if(!empty($medicalfasility_lists)){ 
                                foreach($medicalfasility_lists as $medicalfasility_list){ ?>
                              <option value="<?php echo $medicalfasility_list->id ?>" <?php if(!empty($specialty)){ 
                                if($medicalfasility_list->id ==$specialty) { echo 'selected'; }}  ?> >
                                <?php echo $medicalfasility_list->name ;?></option>
                                <?php } } ?>
                                
                             
                            </select>
                          </label>
                        
                           <?php
                            $ASC= $DESC="";  
                            if($this->input->get('order')) {
                                if($this->input->get('order')=="DESC")
                                {
                                  $DESC='selected';
                                }
                                else{
                                  $ASC='selected';
                                }
                              }
                          
                          
                            ?>

                          <label class="col-md-1 no-padding-left verifys" >
                          <select name="order" class="form-control">
                            <option value="DESC" <?php  echo $DESC; ?>>New</option>
                           <option value="ASC" <?php  echo $ASC; ?>>Old</option>  
                            </select>
                          </label>
                          
                          <button type="submit" class="btn btn-primary" title="Search" data-toggle="tooltip"><i class="fa fa-search" aria-hidden="true" ></i>
                          </button>
                         
                           <a href="<?php echo current_url(); ?>" title="Refresh" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i>
                          </a>
                        
                        </form>
                        
                        </div>

          <!--===============category table=================-->

          <?php $status_array = array('0'=>'Pending','1'=>'Active','2'=>'Deactive','3'=>'Banned','4'=>'Draft');?>
           <?php 
           $checkbox_status_array = '';
           $checkbox_status_array = array('4'=>'Pending','1'=>'Active','2'=>'Deactive','3'=>'Banned');
           ?>
                 <header class="panel-heading"  ></header>
                 <form method="post" action="" id="" name="" >
                  <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                      <tr>
                          <th width="110">
                          S.No
                          </th>
                          <th width="5%;">Job ID</th>
                          <th width="10%;">User Name</th>
                          <th>Job Title</th>
                          <th >Medical Specialty</th>  
                          <th>Assignee Name</th>            
                          <th width="5%;">Status</th>
                          <th width="">Actions</th>                    
                      </tr>
                    </thead>
                    <tbody>
                    <?php
               $i= $offset + 1;
               if($result_data != "")
               {
                $jk=0;
                foreach ($result_data as $res_data)
                {
                  $jk++;
                ?>
                      <tr>
                      <td>
                     
                      <?php echo $i; ?>
                      </td>
                       
                        <td> <a href="<?php echo base_url();?>backend/jobs/postjob_detail/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"><?php if(!empty($res_data['id'])) echo '#'.$res_data['id']; ?></a></td>
                         <td><a href="<?php echo base_url();?>backend/listusers/edit_medical/<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>">
                         <?php                          
                          if(!empty($res_data['user_first_name'])) { $user_first_name=$res_data['user_first_name'];}
                          if(!empty($res_data['user_last_name'])) { $user_last_name=$res_data['user_last_name'];}
                          if(!empty($res_data['user_id'])) echo $user_first_name." ".$user_last_name; ?></a></td>                      
                        <td>   <a href="<?php echo base_url();?>backend/jobs/postjob_detail/<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>"><?php if(!empty($res_data['listing_title'])) echo ucfirst($res_data['listing_title']);  ?></a>
                            </td>
                         <td><?php echo get_medical_specialty($res_data['medical_specialty_name']); ?></td>
                          <td><?php 
                          $get_data = '';
                          $get_assign_val = '';
                          $get_data = get_assignee_detail($res_data['id']);
                          if(!empty($get_data)) 
                            {
                              $get_assign_val = get_user_info($get_data);

                              echo ucfirst($get_assign_val->user_first_name).' '.$get_assign_val->user_last_name;
                              if(!empty($get_assign_val->city)) echo '('.$get_assign_val->city.')';
                            }else{ echo '-';}

                              ?></td>
                        <td>
                               <?php
                               foreach($status_array as $k => $status_a)
                                {
                                  if($res_data['status'] == $k)
                                  {
                              ?>
                              
                          <span class="btn btn-<?php 
                          if($status_a == 'Active') echo 'success'; elseif($status_a == 'Deactive') echo 'danger'; elseif($status_a == 'Pending') echo 'warning';elseif($status_a == 'Draft') echo 'info';elseif($status_a == 'Banned') echo 'info';?> btn-xs tooltips">
                             <?php echo $status_a; ?>
                             </span>
                              <?php  } } ?>
                          
                           
                        </td>
                        
                        
                        <td>
                           <?php  
                          if(!empty($get_data)) 
                            {
                              ?>
                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal_Update" onclick="get_job_id_update('<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>','<?php if(!empty($get_data)) echo $get_data; ?>')">Edit Assign Photographer</button>
                              <?php }else{ ?>
                          <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal" onclick="get_job_id('<?php if(!empty($res_data['id'])) echo $res_data['id']; ?>')">Assign Photographer</button>
                          <?php } ?>
                        </td> 
                      </tr>

                      <?php $i++;} }else{?>
                        <tr ><td colspan="8" class="error" style="text-align:center; color:red;"><h4>Post Job record not found.</h4></td></tr>
                <?php } ?>
                </tbody>
                
              </table>
              <?php if(!empty($pagination)) echo $pagination;?>
                   </form>
                
               </section>
 
  </div>
<script type="text/javascript" >
 $(document).ready(function(){
  setTimeout(function(){
  $(".flash").fadeOut("slow", function () {
  $(".flash").remove();
      }); }, 5000);
 });
</script>
<script>
$(document).ready(function(){
    $(".dropdown-toggle_get_val").dropdown();
});
function get_job_id(id){
  $("#job_id").val(id);
}
function get_job_id_update(id,user_id){
  $("#job_id_update").val(id);
  $("#update_photographer_user").val(user_id).attr('selected');
}
</script>
  <?php if(form_error('photographer_user')){ ?>
  <script>
  $(document).ready(function(){
    var get_post_job_id = '';
    var get_post_job_id = "<?php if(!empty($post_job_id)) echo $post_job_id;?>";
    if(get_post_job_id != '')
    {
      $("#job_id").val(get_post_job_id);
      $("#myModal").modal('show');
    }
   
  });
  $(document).ready(function(){
    var get_post_job_id_update = '';
    var update_photographer_user = '';
    var get_post_job_id_update = "<?php if(!empty($update_post_job_id)) echo $update_post_job_id;?>";
    var update_photographer_user = "<?php if(!empty($update_photographer_user)) echo $update_photographer_user;?>";
    if(get_post_job_id_update != '' && update_photographer_user != '')
    {
      $("#job_id_update").val(get_post_job_id_update);
      $("#update_photographer_user").val(update_photographer_user).attr('selected');
      $("#myModal_Update").modal('show');
    }  
  });
</script>
  <?php } ?>
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign Photographer</h4>
        </div>
        <div class="modal-body">
        <form action="" method="post" class="form-horizontal">
         <input type="hidden" id="job_id" name="job_id">
              <div class="form-group">
                  <label class="col-md-3 col-md-3">Photographer</label>
                 <div class="col-sm-9 ">
                    <select name="photographer_user" class="form-control" id="photographer_user">
                   <option value="">Select Photographer</option>
                   <?php if(!empty($photographer_user_list)){
                            foreach($photographer_user_list as $photographer_user){
                   ?>
                   <option value="<?php if(!empty($photographer_user->user_id)) echo $photographer_user->user_id;?>"><?php if(!empty($photographer_user->user_first_name)) echo $photographer_user->user_first_name;?> <?php if(!empty($photographer_user->user_last_name)) echo ' '.$photographer_user->user_last_name;?><?php if(!empty($photographer_user->city)) echo '('.$photographer_user->city.')';?></option>
                   <?php } 
                    }?>

                   </select>
                  <span style="color:red;"><?php echo form_error('photographer_user'); ?></span>
                 </div>
               </div>
              <div class="form-group">
               <label class="col-md-3 col-md-3">
                  
               </label>
               <div class="col-sm-9">
              <input class="btn btn-primary" type="submit" name="assign_user" value="Assign Photographer">
               </div>
               </div>
         </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="myModal_Update" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Assign Photographer</h4>
        </div>
        <div class="modal-body">
        <form action="" method="post" class="form-horizontal">
         <input type="hidden" id="job_id_update" name="job_id_update">
              <div class="form-group">
                  <label class="col-md-3 col-md-3">Photographer</label>
                 <div class="col-sm-9 ">

                    <select name="update_photographer_user" class="form-control" id="update_photographer_user">
                   <option value="">Select Photographer</option>
                   <?php if(!empty($photographer_user_list)){
                            foreach($photographer_user_list as $photographer_user){
                   ?>
                   <option value="<?php if(!empty($photographer_user->user_id)) echo $photographer_user->user_id;?>"><?php if(!empty($photographer_user->user_first_name)) echo $photographer_user->user_first_name;?> <?php if(!empty($photographer_user->user_last_name)) echo ' '.$photographer_user->user_last_name;?><?php if(!empty($photographer_user->city)) echo '('.$photographer_user->city.')';?></option>
                   <?php } 
                    }?>

                   </select>
                  <span style="color:red;"><?php echo form_error('photographer_user'); ?></span>
                 </div>
               </div>
              <div class="form-group">
               <label class="col-md-3 col-md-3">
                  
               </label>
               <div class="col-sm-9">
              <input class="btn btn-primary" type="submit" name="assign_user_update" value="Edit Assign Photographer">
               </div>
               </div>
         </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  