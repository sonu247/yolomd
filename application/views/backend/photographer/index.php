<script>
function delete_data(id)
{
   var url = "<?php echo site_url();?>backend/photographer/photographer_delete/"+id+'/3';
  if(confirm("Are you sure. Do you want to delete Photographer Detail."))
  {

    $.post(url, function(data){
        window.location.href="<?php echo site_url();?>backend/photographer";
    });


  }
 
}

</script>
<div class="bread_parent">
<ul class="breadcrumb">
    <li><a href="<?php echo base_url('backend/superadmin/dashboard');?>"> Dashboard  </a></li>
    <li class=""> Photographers</li>
   <a href="<?php echo site_url();?>backend/photographer/photographer_add_edit" class="btn btn-primary btn-xs tooltips" style="float:right;">Add Photographer</a> 
</ul>

</div>


            <section class="panel">
              
                
              <!--   <div   id="demo" class="collapse" > -->
                 
                 
                    <div class="col-lg-14"> 
                     
                         <form style="margin-bottom:16px;" action="<?php echo base_url() ?>backend/photographer/index/0" method="get" accept-charset="utf-8">
                          <label class="col-md-2 no-padding-left" ><input type="text" name="UserId" value="<?php echo $this->input->get('UserId'); ?>" class="form-control" placeholder="Search by User Id"></label>
                          <label class="col-md-2 no-padding-left"><input type="text" name="UserName" value="<?php echo $this->input->get('UserName'); ?>" class="form-control" placeholder="Search by UserName"></label>
                           <label class="col-md-2 no-padding-left"><input type="text" name="Email" value="<?php echo $this->input->get('Email'); ?>" class="form-control" placeholder="Search by Email"></label>
                           <?php 
                          $status_check = $this->input->get('status');  ?>
                          <label class="col-md-2 no-padding-left">
                          <select name="status" class="form-control">
                              <option value="All">Select Status</option>              
                              <option value="1" <?php if($status_check== 1) { echo 'selected'; } ?> >Active</option>
                             <option value="2" <?php if($status_check == 2) { echo 'selected'; } ?> >Deactive</option>
                              <option value="3" <?php if($status_check == 3) { echo 'selected'; } ?> >Banned</option>
                            </select>
                          </label>
                         
                           <?php
                            $ASC= $DESC="";  
                            if($this->input->get('order')) {
                                if($this->input->get('order')=="DESC")
                                {
                                  $DESC='selected';
                                }
                                else{
                                  $ASC='selected';
                                }
                              }

                            ?>

                          <label class="col-md-2 no-padding-left">
                          <select name="order" class="form-control">
                            <option value="DESC" <?php  echo $DESC; ?>>New</option>
                           <option value="ASC" <?php  echo $ASC; ?>>Old</option>  
                            
                             
                            </select>
                          </label>
                          <button type="submit" class="btn btn-primary" title="Search" data-toggle="tooltip"><i class="fa fa-search" aria-hidden="true" ></i></button>
                          <a href="<?php echo current_url() ?>" title="Refresh" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-refresh" aria-hidden="true"></i></a>

                        </form>
                        </div>
                        
     
          <!--===============category table=================-->

           <?php $status_array = array('1'=>'Active','2'=>'Deactive','3'=>'Banned');?>
                 
                 
                    <header class="panel-heading"  ></header>
                  <form method="post" action="" id="" name="" >
                  <table id="example1"class="table table-striped table-hover" >
                    <thead class="thead_color">
                    <tr>
                    <th width="7%">
                    <div class="checkselect">
                    <div class="checkbox checkboxli">
                    <input type="checkbox" id="select_all" name="select_all"> 
                    <label for="select_all"></label>
                    </div>
                    
                    <select id="moreaction" name="act_type" class="form-control">
                      <option value="">Action</option>
                      <option value="1">Active</option>
                      <option value="2">Deactive</option>
                      <option value="3">Banned</option>
                      <option value="4">Delete</option>
                    </select>  
                    <input type="hidden" name="apply" id="apply" value="" >
                   <!--  <input type="submit" name="apply_new" value="apply" onclick="return conformmessages()"> -->
                    </div>
                    </th>
                    <th width="7%;">User ID</th>
                    <th width="15%;">User Name</th>
                    <th width="20%;">Email</th>
                    <th width="15%;"><i class="fa fa-calendar" aria-hidden="true"></i> Created Date & Time</th>
                    <th width="15%;"><i class="fa fa-calendar" aria-hidden="true"></i>
                        Updated Date & Time</th>
                    <th width="10%">Status</th>
                    <th width="10%">Actions</th>
                    
                  </tr>
                    </thead>
                    <tbody>
                    <?php
               $i= $offset + 1;
               if($result_data != "")
               {

                foreach ($result_data as $res_data)
                {
               
                ?>
                      <tr>
                      <td>
                      <div class="checkboxli">
                      <input type="checkbox" value="<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>" id="check_id<?php echo $i; ?>" name="check_id[]">
                      <label for="check_id<?php echo $i; ?>"></label> 
                      </div>
                      <?php echo $i; ?>.
                      </td>
                        
                        <td>
                         <a href="<?php echo base_url();?>backend/photographer/photographer_add_edit/<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>">
                        <?php if(!empty($res_data['user_id'])) echo '#'.$res_data['user_id']; ?>
                        </a>
                        </td>
                        <td>
                         <a href="<?php echo base_url();?>backend/photographer/photographer_add_edit/<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>">
                         <?php if(!empty($res_data['user_first_name'])) echo ucfirst($res_data['user_first_name']).' '; if(!empty($res_data['user_last_name'])) echo $res_data['user_last_name']; ?>
                          </a>
                          
                        </td>
                        <td><?php if(!empty($res_data['user_email'])) echo $res_data['user_email']; ?></td>
                        
                        <td> <?php if(!empty($res_data['user_created'])) echo $res_data['user_created']; ?> </td>
                         <td> <?php if(!empty($res_data['user_updated'])) echo $res_data['user_updated']; ?> </td>
                      
                       <td> <div class="dropdown">
                            <button class="<?php 
                            if($res_data['user_status'] == 1)
                             echo 'btn btn-success';
                            elseif($res_data['user_status'] == 2)
                              echo 'btn btn-danger';
                             elseif($res_data['user_status'] == 3)
                                echo 'btn btn-info';
                              else
                                echo '';
                              ?> btn-xs  dropdown-toggle_get_val" id="menu1" type="button" data-toggle="dropdown"><?php 
                            foreach($status_array as $k => $status_a)
                            {
                              if(!empty($res_data['user_status']) && $k == $res_data['user_status']) 
                                echo $status_a;
                            }
                             ?>
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                               <?php
                               foreach($status_array as $k => $status_a)
                                {
                                  if(!empty($res_data['user_status']) && $k != $res_data['user_status'])
                                  {
                              ?>
                              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url();?>backend/photographer/change_status/<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>/<?php echo $k; ?>/3"><?php echo $status_a; ?></a></li>
                              <?php } } ?>
                              
                            </ul>
                            </div>
                             </td>
                        
                        <td>
                         <a target="_new" href="<?php echo base_url().'backend/superadmin/login_photographer/'.$res_data['user_id'] ?>" class="btn btn-warning btn-xs tooltips" rel="tooltip"  data-placement="top" data-original-title="Proxy Login"><i class="fa fa-lock" aria-hidden="true"></i></a>
                             <span title="Edit Photographer User details" data-toggle="tooltip"><a class="btn btn-primary btn-xs tooltips" href="<?php echo site_url();?>backend/photographer/photographer_add_edit/<?php if(!empty($res_data['user_id'])) echo $res_data['user_id']; ?>"  title="Edit Photographer details"><i class="fa fa-pencil-square-o"></i></a>
                          </span>
                          
                          <a href="javascript:void(0)" data-toggle="tooltip" class="btn btn-danger btn-xs tooltips" onclick="delete_data('<?php echo  $res_data['user_id'];?>');" title="Delete Photographer details"><i class="fa fa-trash-o"></i></a>
                      
                         
                        </td> 
                       
                      </tr>
                      <?php $i++;} }else{?>
                        <tr ><td colspan="8" class="error" style="text-align:center; color:red;"><h4>Photographer record not found.</h4></td></tr>
                <?php } ?>
                </tbody>
                
              </table>
              <?php if(!empty($pagination)) echo $pagination;?>
                   
                 
               </section>
  
  </div>


<script>

 function conformmessages()
 {
  var checkedValue = document.querySelector('input:checked').value;
       
  var k= ($("#moreaction").val());
  if(k==4 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Delete?");
  }
  else if(k==2 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Deactive?");
  }
  else if(k==1 && checkedValue!=0)
  {
     return confirm("Are you sure you want to Active?");
  }
  else if(k==3 && checkedValue!=0)
  {
      return confirm("Are you sure you want to Banned?");
  }else{
    return false;
  }
  
}


$("#select_all").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
$('#moreaction').on('change', function(e){
    if(conformmessages())
    {
      $("#apply").val(1);
      this.form.submit() 
    }
  });
</script>
<script type="text/javascript" >
 $(document).ready(function(){
  setTimeout(function(){
  $(".flash").fadeOut("slow", function () {
  $(".flash").remove();
      }); }, 5000);
 });
</script>
<script>
$(document).ready(function(){
    $(".dropdown-toggle_get_val").dropdown();
});
</script>
