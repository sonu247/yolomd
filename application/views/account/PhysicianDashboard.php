<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <!--Left Section-->
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/PhysicianLeftNavigation'); ?>
		   </div>
		    <!--Left Section End-->
		    <!--Right Section-->
		    <div class="col-md-9 col-sm-9">
		    <!--Right Section Breadcrumb-->
		    <div class="breadcrumb-block">
			 <ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="#">Library</a></li>
				<li class="active">Data</li>
			 </ol>
		    </div>
		    <!--Right Section Breadcrumb End-->
		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-dashboard.png" class="">My Dashboard</h3>

		   	<div class="message-center">
		   	<div class="row">
		   	<br><br>
		   	 <h3 class="text-center">Dashboard</h3>
        	</div>

		</div>

		   	</div>
		   </div>
		   <!--Right Section End-->
		  </div>
		</div>
		</div>
	</div>
</div>

<?php 
  $this->load->view('include/footer_menu');
?>