<!-- modal -->
<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-controller="Overview"  id="paymentoverviewModal">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <div class="themeform">
        <div class="col-md-12 ">
          <h3 class="heading text-center">Placement Overview</h3>

          <table class="table" ng-controller="userLogin">
      <tr><th>City Name </th><th>Charge</th></tr>
        <tr ng-repeat="city_content in placement_city" ng-if="city_content">
        <td>
          {{city_content.city_name}}
          </td>
        <td>
           {{city_content.city_placement_charge}}
           </td>
        </tr>

        <tr>
          <td>
             Yolomd Verify Fee
            </td>
            <td>
              {{yolomd_verify_fee}}
            </td>
         </tr>
        <tr>
          <td>
            Gross Total
          </td>
          <td>
            {{gross_total}}
          </td>
          </tr>
          <tr>
            <td>
              Discount
            </td>
            <td>
              {{discount}}
            </td>
          </tr>
      </table>

        </div>
        <div class="clearfix"></div>
       </div>
      </div>
    </div>
  </div>
</div>