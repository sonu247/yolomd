<?php 
   $this->load->view('include/header_menu');
   ?>
<div class="two-column-warp" >
   <!-- <div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
      <img src="<?php //echo FRONTEND_THEME_URL ?>images/loading-new.gif">
      </div> -->
   <div class="formPage-section">
      <div class="col-md-7 col-sm-7 full-height left-side" >
         <section class=" {{animationClass}} wow" ng-class="{{animationClass}}">
            <div class="form-section-block">
               <div class="col-md-12 center-block no-padding">
                  <div  data-target="#common_modal_msg" data-toggle="modal"></div>
                  <div class="jobPreview jobPostpagePreview row">
                     <div class="row">
                        <div class="postJobSliderWarp">
                           <slick class="slider single-item postJobSlider" current-index="index" responsive="breakpoints" dots=true infinite=true speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload=true data="jobs_image" ng-if="jobs_image">
                              <div  ng-repeat="uploadImage in jobs_image" >
                                 <div class="jobPostSlide" style="background-image:url('<?php echo base_url('assets/uploads/jobs/') ?>/{{uploadImage.imageName}}')">
                                 </div>
                              </div>
                           </slick>
                           <slick class="slider single-item postJobSlider" current-index="index" responsive="breakpoints" dots=true infinite=true speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload=true data="jobs_image" ng-if="!jobs_image">
                              <div class="jobPostSlide jobPostSlideNopreview" style="background-image:url('<?php echo base_url('assets/front/images/default-list-thumb.jpg') ?>')">
                              </div>
                           </slick>
                           <a ng-href="post-job-update/{{encrypt_id}}#10" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Change uploaded photos"  class="edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </div>
                     </div>
                     <div class="row title">
                        <div class="col-md-12 " ng-if="jobDetail.listing_title">
                           <div class="name">{{jobDetail.listing_title}}   			<span class="edit-link"><a ng-href="post-job-update/{{encrypt_id}}#9" data-toggle="tooltip" tooltip-placement="right" uib-tooltip="Edit List Title" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span> 	
                           </div>
                        </div>
                        <div class="col-md-12 jobSpeciality" >
                           <div class="city" ng-if="jobDetail.facility_city">
                              <div class="jobSpeciality-tool">
                                 <span class="tb-cell"><img src="<?php echo FRONTEND_THEME_URL ?>/images/map-marker.svg" width="18"></span>
                                 <span class="tb-cell">{{jobDetail.facility_city}},  {{ProvinceName}}</span>
                                 <span class="edit-link"><a ng-href="post-job-update/{{encrypt_id}}#5" data-toggle="tooltip" tooltip-placement="right" uib-tooltip="Edit City" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                 </span>                                 
                              </div>
                           </div>
                           <div class="pull-right Speciality-text" ng-if="MedicalSpecialtyValue">
                              <div class="jobSpeciality-tool jobSpeciality-tool-icon">
                                 <span class="tb-cell"><img src="{{MedicalSpecLogo}}" width="30"> </span>
                                 <span class="tb-cell">{{MedicalSpecialtyValue}}</span>
                                 <span class="edit-link"><a ng-href="post-job-update/{{encrypt_id}}#2" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit Medical Specialty"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                 </span>                                 
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div class="col-group row" ng-if="jobDetail.listing_description">
                        <div class="col-md-12 jobDetailBlock">
                           <div class="col-md-12">
                              <div class="more">
                                 {{jobDetail.listing_description}}
                              </div>
                           </div>
                           <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#9" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit Description" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                        </div>
                     </div>
                     <div class="col-group row" ng-if="FacilityType != null || jobDetail.parking != null || jobDetail.emr != null || jobDetail.no_of_fulltime != null || jobDetail.part_time_doctor != null || jobDetail.exam_room != null">
                        <div class="col-md-3 col-xs-4 label-name">
                           <b>Facility Details </b>
                        </div>
                        <div class="col-md-9 col-xs-8 jobDetailBlock">
                           <div class="deatailElement">
                              <ul class="row">
                                 <li class="col-md-6 col-xs-12" ng-if="FacilityType">
                                    <div class="block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/medical-kit.svg"> Type :</label> 
                                       <span data-toggle="tooltip" data-placement="top" title="Type"><b>{{FacilityType}}</b></span>
                                    </div>
                                 </li>
                                 <li class="col-md-6 col-xs-12" ng-if="jobDetail.parking">
                                    <div class="block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/parking.svg"> Parking :</label> <span data-toggle="tooltip" data-placement="top" title="Parking">{{jobDetail.parking}}</span>
                                    <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#6" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="deatailElement">
                              <ul class="row">
                                 <li class="col-md-6 col-xs-12" ng-if="HoursValue">
                                    <div class="block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/watch.svg">  Hours :</label>
                                       <span>{{HoursValue}}</span>
                                    </div>
                                 </li>
                                 <li class="col-md-6 col-xs-12">
                                    <div class="block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/position.svg">  Job Type :</label>
                                       <span>{{jobDetail.position}}</span>
                                    </div>
                                 </li>
                                 <li class="col-md-6 col-xs-12"  ng-if="StartDateValue">
                                    <div class="block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/calendar.svg">  Start Date :</label>
                                       <span>
                                       {{StartDateValue}}
                                       </span>
                                    </div>
                                 </li>
                                 <li class="col-md-6 col-xs-12" ng-if="jobDetail.emr">
                                    <div class="block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/emr.svg" > EMR :</label>
                                       <span data-toggle="tooltip" data-placement="top" title="EMR">{{jobDetail.emr}}</span>
	                                   <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#4" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>				
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="deatailElement">
                              <ul class="row">
                                 <li class="col-md-12" ng-if="jobDetail.no_of_fulltime != null ||jobDetail.part_time_doctor != null || jobDetail.exam_room != null">
                                    <div class="block requirment-detail-block">
                                       <label><img src="<?php echo FRONTEND_THEME_URL ?>/images/group.svg" > </label>&nbsp;
                                       <span class="full-time">
                                       <b>{{jobDetail.no_of_fulltime}}</b> <label>Full-time doctors</label>
                                       </span> &nbsp;
                                       <span class="part-time">
                                       <b>{{jobDetail.part_time_doctor}}</b> <label>Part-time doctors</label>
                                       </span>&nbsp;
                                       <span class="exam-rooms"><b>{{jobDetail.exam_room}}</b> <label>Exam rooms</label></span>
                                    <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#8" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>	
                                    </div>
                                 </li>
                                 <!-- <li class="col-md-12" ng-if="NursingArray">
                                    <div class="block blockTable nursing">
                                    <span class="cell"><label>Nursing :</label></span>
                                    <span ng-bind-html="NursingArray" class="nursingTagBlock cell">
                                    </span>
                                    </div>
                                    </li> -->
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!--Special Qualifications Group-->
                     <div class="col-group row" ng-if="jobDetail.call_responsibility">
                        <div class="col-md-3 col-xs-4 label-name">
                           <b>Call Responsibilities</b>
                        </div>
                        <div class="col-md-9 jobDetailBlock" ng-if="jobDetail.call_responsibility">
                           {{jobDetail.call_responsibility}}
                        </div>
                        <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#4" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                     </div>
                     <div class="col-group row" ng-if="CompensationValue">
                        <div class="col-md-3 col-xs-4 label-name">
                           <b>Compensation</b>
                        </div>
                        <div class="col-md-9 col-xs-8 jobDetailBlock" ng-if="CompensationValue">
                           {{CompensationValue}}
                        </div>
                        <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#4" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                     </div>
                     <!--Call Responsblity Group-->
                     <!--Allied Health Professionals Group-->
                     <!--Allied Health Professionals Group-->
                     <!-- <div class="col-group row" ng-if="HealthProfessional">
                        <div class="col-md-3 col-xs-4 label-name">
                        	<b>Allied Health Professionals </b>
                        </div>
                        <div class="col-md-9 col-xs-8 jobDetailBlock healthProfessional" ng-bind-html="HealthProfessional">
                        	
                        </div>
                        <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#9" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                        </div> -->
                     <!--Public Transport Group-->
                     <div class="col-group row" ng-if="jobDetail.transport_metro_subway != null || jobDetail.transport_m_s_value != null || jobDetail.transport_bus != null || jobDetail.transport_bus_value != null || jobDetail.transport_other != null || jobDetail.transport_other_value != null || jobDetail.transport_train_value != null || jobDetail.transport_train != null || jobDetail.transport_bus_value != '0' || jobDetail.transport_other != '0' ">
                        <div class="col-md-3 col-xs-4 label-name">
                           <b>Public Transport </b>
                        </div>
                        <div class="col-md-9 col-xs-8 jobDetailBlock">
                           <div class="public-transport">
                              <div class="block row">
                                 <div class="transportName" ng-if="jobDetail.transport_metro_subway != '' && jobDetail.transport_metro_subway != null && jobDetail.transport_metro_subway != '0' ">
                                    <img src="<?php echo FRONTEND_THEME_URL ?>/images/metro.svg"> Metro/Subway
                                 </div>
                                 <div ng-if="jobDetail.transport_m_s_value != '' && jobDetail.transport_m_s_value != null " class="transportDetail">
                                    ({{jobDetail.transport_m_s_value}})
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="block row">
                                 <div ng-if="jobDetail.transport_bus != '' && jobDetail.transport_bus != null && jobDetail.transport_bus != '0' " class="transportName">
                                    <img src="<?php echo FRONTEND_THEME_URL ?>/images/bus.svg"> Bus
                                 </div>
                                 <div ng-if="jobDetail.transport_bus_value != '' && jobDetail.transport_bus_value != null" class="transportDetail">
                                    ({{jobDetail.transport_bus_value}})
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="block row">
                                 <div ng-if="jobDetail.transport_train != '' && jobDetail.transport_train != null && jobDetail.transport_train != '0' " class="transportName">
                                    <img src="<?php echo FRONTEND_THEME_URL ?>/images/train.svg"> Train
                                 </div>
                                 <div ng-if="jobDetail.transport_train_value != '' && jobDetail.transport_train_value != null" class="transportDetail">
                                    ({{jobDetail.transport_train_value}})
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="block row">
                                 <div ng-if="jobDetail.transport_other != '' && jobDetail.transport_other != null && jobDetail.transport_other != '0' " class="transportName">
                                    <img src="<?php echo FRONTEND_THEME_URL ?>/images/transport-other.svg"> Other
                                 </div>
                                 <div ng-if="jobDetail.transport_other_value != '' && jobDetail.transport_other_value != null" class="transportDetail">
                                    ({{jobDetail.transport_other_value}})
                                 </div>
                              </div>
                           </div>
                        </div>
                        <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#6" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                     </div>
                     <!--Facility Hours Group-->
                     <div class="col-group row" ng-if="FacilityDaysName != null || FacilityHoursAll != null">
                        <div class="col-md-3 col-xs-4 label-name">
                           <b>Facility Hours </b>
                        </div>
                        <div class="col-md-9 col-xs-8 jobDetailBlock">
                           <div class="row">
                              <div class="col-md-10">
                                 <table class="table table-condensed">
                                    <tr ng-repeat="facility_days_name in FacilityDaysName" ng-init="parentIndex = $index">
                                       <td >
                                          <div class="FacilityDaysName">
                                             {{facility_days_name}}
                                          </div>
                                          <k ng-repeat="(time_by_date_key,time_by_date_value) in FacilityHoursAll" ng-init="parentIndex1 = $index">
                                             <div  class="FacilityDate">
                                                <div ng-repeat=" facility_full_name in FacilityHoursAll[time_by_date_key] track by $index" ng-if="facility_days_name == time_by_date_key" >
                                                   {{facility_full_name.start_time}} <span ng-if="facility_full_name.start_time != '' && facility_full_name.end_time != ''">- </span> {{facility_full_name.end_time}}
                                                   <span ng-if="FacilityHoursAll[time_by_date_key].length != 1 && FacilityHoursAll[time_by_date_key].length != ($index+1)">,</span>
                                                </div>
                                             </div>
                                          </k>
                                       </td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#7" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                     </div>
                     <!--Special Qualification Group-->
                     <div class="col-group row" ng-if="jobDetail.special_qualification">
                        <div class="col-md-3 col-xs-4 label-name">
                           <b>Special Qualifications</b>
                        </div>
                        <div class="col-md-9 col-xs-8 jobDetailBlock text-justify">
                           {{jobDetail.special_qualification}}
                        </div>
                        <span class="col-group-edit"><a ng-href="post-job-update/{{encrypt_id}}#3" data-toggle="tooltip" tooltip-placement="left" uib-tooltip="Edit" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
                     </div>
                     <br>
                     <div class="col-group jobPreview-btnWarp row">
                        <div class="col-md-3"></div>
                        <!-- <div class="pull-right">
                           <a ng-href="job-listing" class="btn btn-blue btn-continue" >Submit Listing <img src="<?php //echo FRONTEND_THEME_URL ?>images/go-next.png"></a>
                           </div> -->
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <br><br>
            </div>
         </section>

      </div>
         <div class="view-map-btn hidden-md hidden-sm visible-xs">
            <a href="#" class="view-map" ng-click="toggeleclass()" >{{map_listing}}
            <img src="<?php echo FRONTEND_THEME_URL ?>images/map-view.svg"></a>
         </div>      
      <div class="col-md-5 col-sm-5 map-preview" ng-class="toggleclassfunctioning">
         <div id="map" class="map-preview">
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
</div>
<style type="text/css">
   body{overflow-y:hidden;}
   .angular-google-map-container { height: 100%; margin-top: -45px;}
   /*.map-preview{height: 100%;}*/
   a{cursor:pointer!important;}
   .preview{margin-right:10px;}
   .left-side{padding-bottom: 0px;}
</style>
<?php $this->load->view('include/common_modal_msg'); ?>