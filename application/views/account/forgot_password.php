<div class="container two-col-holder no-padding">
	<!--Login container start here-->
	<div class="container login-container-warp"><br><br>
	<div class="col-md-12 col-lg-12">
	<div class="login-page-warp">
	<?php echo msg_alert_front(); ?>
	 <div class="col-md-7 col-lg-7 login-left-warp">
	 	<h3>Almost There!</h3>
	 	<p>Create a new account or sign in to your existing account, to join our Prep Insider Rewards Program.</p>
	 	<h3>Prep Insider Benefits Include:</h3>
	 	<ul>
	 		<li>Special Promotions</li>
	 		<li>Exclusive Discounts</li>
	 		<li>Insider Info</li>
	 	</ul>
	 	<b>Get started on being rewarded for future purchases!</b>
	 </div>
	 <div class="col-md-5 col-lg-5" >
	 	<div class="login-holder">
	 	<div class="panel panel-default">

		  <div class="panel-heading text-center"><h4><b>Forgot Password</b></h4></div>
		  <div class="panel-body">
		  <!-- <div class="col-md-6 login-logo">
		  	<img src="images/corporate.png" width="60%" class="img-center">
		  </div> -->
		   <div class="col-md-12">
		   
		   	<form class="form-signin" method="post" role="form" action="<?php echo current_url(); ?>" id="form_valid">
		   	<div class="input-group">Remember Password ? <span style="color:gray;">|</span> <a href="<?php echo base_url().'account/login'; ?>">Login</a></div>
		   			
			<div class="input-group">
			<span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>			  
			  <input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Email address" aria-describedby="basic-addon1"  autofocus  data-bvalidator-msg="valid email is required" data-bvalidator="email,required" >
			</div>
			  <?php echo form_error('email') ?>
          <button class="btn btn-primary pull-left load_loader" type="submit">Submit</button>&nbsp;&nbsp;
		  <label></label>
	      </form>
		   </div>		   
		  </div>
		</div>      		
	 	</div>
	 </div>
	<div class="clearfix"></div>
	
	 <?php $this->load->view('stores/include/store_create'); ?>
	 
     </div>
	 <br>
	 </div><br>
    </div>
	<!--login container end-->
</div>
