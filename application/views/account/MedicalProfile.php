<?php 
   $this->load->view('include/header_menu');
   ?>
<div class="page-container" >
   <div class="">
      <div class="container">
         <div class="dashboard">
            <div class="row">
               <!--Left Section-->
               <div class="col-md-3 col-sm-3 dashboard-Left-warp">
                  <?php $this->load->view('account/MedicalLeftNavigation'); ?>
               </div>
               <!--Left Section End-->
               <!--Right Section-->
               <div class="col-md-9 col-sm-9">
                  <div class="dashboard-right">
                     <h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/profile.svg" class="">Edit Profile</h3>
                     <div class="message-center">
                        <div class="row">
                           <div class="col-md-12">
                              <h4 class="extraLines">Use the form below to update your profile. The e-mail address registered with your account is your unique identifier.</h4>
                           </div>
                           <div class="clearfix"></div>
                           <br><br>
                           <div  data-target="#common_modal_msg" data-toggle="modal"></div>
                           <div class="formsection2">
                              <div class="panel-group postJobpanelGroup" >
                                 <form method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="MedicalProfileUpdate()">
                                    <div class="form-section" style="height:0%">
                                       <div class="themeform">
                                          <div class="">
                                             <div class="col-md-12">
                                                <div class="form-group row">
                                                   <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                      <label class="required label title" > Name Title <font>:</font> </label>
                                                   </div>
                                                   <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                      <div class="radio radio-info radio-inline">
                                                         <input type="radio" name="user_name_title" id="user_name_title" ng-model="MedicalDetailData.user_name_title" value="Mr." class="ng-pristine ng-untouched ng-valid"> 
                                                         <label for="user_name_title">Mr.</label>
                                                      </div>
                                                      <div class="radio radio-info radio-inline">
                                                         <input type="radio" name="user_name_title" id="user_name_title1" ng-model="MedicalDetailData.user_name_title" value="Miss" class="ng-pristine ng-untouched ng-valid"> 
                                                         <label for="user_name_title1">Miss</label>
                                                      </div>
                                                      <div class="radio radio-info radio-inline">
                                                         <input type="radio" name="user_name_title" id="user_name_title2" ng-model="MedicalDetailData.user_name_title" value="Mrs." class="ng-pristine ng-untouched ng-valid"> 
                                                         <label for="user_name_title2">Mrs.</label>
                                                      </div>
                                                      <div class="radio radio-info radio-inline">
                                                         <input type="radio" name="user_name_title" id="user_name_title3" ng-model="MedicalDetailData.user_name_title" value="Dr." data-bvalidator="required" data-bvalidator-msg="Name Title Required." class="ng-pristine ng-untouched ng-valid">
                                                         <label for="user_name_title3">Dr.</label>
                                                      </div>
                                                      <div class="clearfix"></div>
                                                      <label ng-bind="Error.user_name_title" class="error ng-binding"></label>
                                                      <!-- ngIf: Error.user_name_title -->
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="">
                                             <div class="col-md-12">
                                                <div class="form-group row">
                                                   <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                      <label class="label required"> First Name <font>:</font> </label>
                                                   </div>
                                                   <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                      <input type="text" maxlength="30" name="user_first_name" ng-model="MedicalDetailData.user_first_name" class="form-control ng-pristine ng-untouched ng-valid" data-bvalidator-msg="First Name Required.">
                                                      <label ng-bind="Error.user_first_name" class="error ng-binding"></label>
                                                      <!-- ngIf: Error.contact_person_first_name -->
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="form-group row">
                                                   <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                      <label class="label required">Last Name <font>:</font> </label>
                                                   </div>
                                                   <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                      <input type="text" maxlength="30" name="user_last_name" ng-model="MedicalDetailData.user_last_name" class="form-control ng-pristine ng-untouched ng-valid"  data-bvalidator-msg="Last Name Required.">
                                                      <label ng-bind="Error.user_last_name" class="error ng-binding"></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="">
                                             <div class="col-md-12">
                                                <div class="form-group row">
                                                   <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                      <label class="label required">Email ID<font>:</font> </label>
                                                   </div>
                                                   <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                      <input type="text" name="user_email" ng-model="MedicalDetailData.user_email" class="form-control ng-pristine ng-untouched ng-valid" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required." id="user_email">
                                                      <label ng-bind="Error.user_email" class="error ng-binding"></label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- 	</section>
                                             <section class="form-section"> -->
                                          <div class="clearfix">
                                             <br>
                                             <div class="col-md-4 col-sm-4 term-col4"></div>
                                             <div class="col-md-6 col-sm-8 signup-full-column view-profile-page center-btn">
                                                <button type="submit" name="" value="Register as a physician" class="btn-blue" data-bvalidator="required">
                                                Update profile
                                                </button>
                                                <div class="col-md-10 col-md-offset-1">
                                                   <!-- ngIf: Message -->
                                                </div>
                                             </div>
                                          </div>
                                          <br>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--Right Section End-->
            </div>
         </div>
      </div>
   </div>
</div>
<?php 
   $this->load->view('include/common_modal_msg');
   $this->load->view('include/footer_menu');
   
   ?>