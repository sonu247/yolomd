<?php 
  $this->load->view('include/header_menu');
?>
<div class="paypalWarp">
 <div class="container">
  <div class="paypal-container">
    <div class="col-md-12">
     <div ng-if="CheckPostValue">
     <div class="alert alert-success" ng-if="UpdateMassage" ng-hide="UpdateAlertMessage">
            <strong>Success!</strong>  {{UpdateMassage}}.
      </div>
    </div> 
    <div class="paypalContentBlock">
    <div class="paypalContent">
    <div class="col-md-6">
    <div class="left">
    <h3 class="text-center paypalheading" ng-if="PriorityPlacement.option_name != ''" ng-bind-html="PriorityPlacement.option_name"></h3>
    <div class="listContent" ng-if="PriorityPlacement.option_value != ''" ng-bind-html="PriorityPlacement.option_value">
    </div>
    <div class="priceCheck clearfix">
      <p><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>Select the region where you would like to Priority Place your Job</p>
        <ul>
          <li ng-repeat="city_content in CityData" ng-if="CityData">
            <div class="checkbox checkbox-info">
            <input type="checkbox" name="placement_array[]" id="{{city_content.id}}" ng-model="PostData.placement_array[city_content.id]" value="{{city_content.id}}" ng-click="nextStep()">
            <label for="{{city_content.id}}">{{city_content.name}} <span class="price"><font>+</font>{{city_content.price | currency}}</span></label>
          </div>
          </li>
        </ul>
      </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="listContent">
     <h3 class="text-center paypalheading" ng-if="VerifiedFacility.option_name != ''" ng-bind-html="VerifiedFacility.option_name"></h3>
      <div ng-if="VerifiedFacility.option_value != '' " ng-bind-html="VerifiedFacility.option_value">
      </div>
   
    </div>
    <div class="priceCheck verifiedCheck clearfix ">
      <ul>
        <li class="verified-facility">
          <div class="checkbox checkbox-info">
          <input type="checkbox" name="term" id="verified"  name="yolomd_verify" id="verified" ng-model="PostData.yolomd_verify" ng-click="nextStep()">
          <label for="verified" class="text-capitalize" ng-if="PhotographyFee.option_value != '' ">&nbsp;&nbsp;<font>+</font><b>{{PhotographyFee.option_value | currency}}</b> Add YoloMD Verified Facility</label>
        </div>
        </li>       
       </ul>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="clearfix"></div>
    <div class="stepredirect clearfix pull-right">
      <div class="pull-right text-right">
      <!-- <div class="payment-loader"><img src="<?php //echo FRONTEND_THEME_URL ?>images/loader.gif" class="img-responsive"></div> -->
       <h3 ng-if="gross_total > 0" class="grossTotal pull-left">{{gross_total | currency}}</h3>
       
       <a class="button" data-toggle="modal" data-target="#paymentoverviewModal" ng-click="getpaydata()" ng-if="gross_total > 0">Proceed To Pay</a>
       <a class="button" ng-href="{{site_url}}post-job-update/{{jobId}}">Skip</a>
       <div class="clearfix"></div>
       <!-- <div ng-if="gross_total >0 && yolomd_verify_fee > 0" class="discount_label">
         40% discount added
       </div> -->
      </div>
    </div>
   <div class="clearfix"></div>
    </div>

    </div>
    </div>
    </div>
    <div class="clearfix"></div>
  </div>
  </div>
<?php 
  $this->load->view('include/footer_menu');
?>

<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  id="paymentoverviewModal">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <div class="themeform">
        <div class="col-md-12 ">
        <div class="placement-cartHeading">
          <h3 class="">{{listing_title}}</h3>
        </div>
        <div class="placement-cart">
        
          <div ng-if="!yolomd_verify_fee">
              <div class="alert alert-info" role="alert">Receive a <font>{{discount_amount}}% discount</font> for YoIoMD Verified Status when you purchase Priority Placement and YoIoMD Verification photographs together
              </div>
           </div>

           <div ng-if="placement_city==0 || placement_city=='NULL' "> 

             <div class="alert alert-info" role="alert">Receive a <font>{{discount_amount}}% discount</font> for YoIoMD Verified Status when you purchase Priority Placement and YoIoMD Verification photographs together
              <a href="#" data-dismiss="modal" aria-label="Close">Select Regions </a>
            </div>
          </div>

          
            <div  ng-if="placement_city" class="placmentCartPrice">

            <div class="placmentCartCity">
<!--               <div ng-if="placement_city" class="text-center">
                <h4>Placement City</h4>
              </div> -->
              <div>
                <table class="table table-striped cityTable" >
                  <tr ng-repeat="city_content in placement_city">
                    <td class="text-right"><label>{{city_content.city_name}} :</label></td>    
                    <td class="text-right"><span>
                       + {{city_content.city_placement_charge | currency}}
                    </span></td>
                  </tr>
                </table>
                </div>
            </div>

            <div ng-if="yolomd_verify_fee" class="verifyFee">
               <table class="table table-striped">
                <tr>
                  <td class="text-right">
                   <label>Yolomd Verify Fee : </label>
                  </td>
                  <td> + 
                      {{yolomd_verify_fee | currency}}
                  </td>
                </tr>
               </table>
               </div>

              <div ng-if="discount" class="discount">
               <table class="table table-striped" >
                <tr >
                  <td class="text-right">
                    <label>Discount : </label>
                  </td>
                  <td> - 
                    {{discount | currency}}
                  </td>
                </tr>
               </table>
               </div>

               <div ng-if="gross_total" class="discount">
               <table class="table table-striped" >
                <tr >
                  <td class="text-right">
                    <label>Amount To Pay : </label>
                  </td>
                  <td>
                    {{gross_total | currency}}
                  </td>
                </tr>
               </table>
               </div>

            </div>
           </div>
   
          
      <div class="paybuttonBlock text-right">
        <div class="block pull-left">
          <img src="<?php echo FRONTEND_THEME_URL ?>images/paypal-logo.svg" class="img-responsive">
        </div>
        <div class="block pull-right">
          <form class="paypal" action="{{actionUrl}}" method="post" >
          <input type="hidden" name="cmd" value="_xclick" />
          <input type="hidden" name="no_note" value="1" />
          <input type="hidden" name="lc" value="UK" />
          <input type="hidden" name="currency_code" value="USD" />
          <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
          <input name="shipping" type="hidden" value="0.00">
          <input type="submit" name="submit" value="Confirm Payment " class="btn btn-pay" />
        </form>
        </div>
        <div class="clearfix"></div>
      </div>
        <div class="clearfix"></div>
        </div>
        </div>
        <div class="clearfix"></div>
       </div>
      </div>
    </div>
  </div>
</div>
