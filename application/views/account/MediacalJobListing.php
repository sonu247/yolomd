<?php
$this->load->view('include/header_menu');
?>
<script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.10.0.js" type="text/javascript"></script>
<div class="page-container">
  <div class="">
    <div class="container" >
      <div class="dashboard">
        <div class="row">
          <!--Left Section-->
          <div class="col-md-3 col-sm-3 dashboard-Left-warp">
            <?php $this->load->view('account/MedicalLeftNavigation'); ?>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="dashboard-right">
             <!--  <div class='info-cover'><div class='cover'><h2>Scroll or Swipe to Checkout more options</h2><img src='<?php echo FRONTEND_THEME_URL ?>/images/icons/swipe-icon.svg'/></div></div> -->
              <h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.svg" class="">My Job Listings</h3>
              <div class="jobList-block">
                <div class="jobListTile-block">
                  <div  data-target="#common_modal_msg" data-toggle="modal"></div>
                  <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
                  </div>
                  <div class="table-responsive">
                    <table class="table table-hover medicaljobListTable joblisting-table-preview table-striped" ng-if="post_jobs!=0">
                      <tr>
                        <th class="text-center hidden-md hidden-lg hidden-sm"><img src="<?php echo FRONTEND_THEME_URL ?>/images/settings-new.svg" width="18" style="position: relative;top: -2px;"> Action</th>                      
                        <th class="text-center s-no-th">S.No.</th>
                        <th class="text-center img-th"><img src="<?php echo FRONTEND_THEME_URL ?>/images/camera.svg" width="18" style="position: relative;top: -2px;"> Image</th>
                        <th class="title-th">Job Title</th>
                        <th class="speciality-th"><img src="<?php echo FRONTEND_THEME_URL ?>/images/medal-new.svg" width="20"> Speciality</th>
                        <th class="text-center date-th">
                          <img src="<?php echo FRONTEND_THEME_URL ?>/images/calendar.svg" width="18" style="position: relative;top: -2px;"> Date</th>
                        <th class="text-center status-th">Status</th>
                        <th class="text-center hidden-xs  action-panel-th"><img src="<?php echo FRONTEND_THEME_URL ?>/images/settings-new.svg" width="18" style="position: relative;top: -2px;"> Action</th>
                      </tr>
                      
                      <tr ng-repeat="post_detail in ItemsByPage[currentPage] | orderBy:columnToOrder:reverse" ng-if="post_jobs" >
                        <td class="hidden-md hidden-lg hidden-sm">
                          <div class="action-panel">
                            <a ng-href="{{site_url}}job-preview/{{post_detail.encrpt_id}}" class="btn btn-sm view-preview"  ng-if="post_detail.status != 'Draft'" target="_blank">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="View Preview"><i class="fa fa-eye" aria-hidden="true"></i></span>
                            </a>
                            <a ng-href="{{site_url}}post-job-update/{{post_detail.encrpt_id}}" class="btn btn-sm edit">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit Job"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                              </span>
                            </a>
                            <a class="btn btn-sm btn-delete delete" ng-click="delete_post_job(post_detail.id,$index,currentPage)">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Delete Job"><i class="fa fa-trash" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </td>
                        <td class="text-center">
                          {{pageSize *((currentPage + 1) -1)+$index+1}}.
                        </td>
                        <td>
                          <a href="{{site_url}}post-job-update/{{post_detail.encrpt_id}}" ng-if="post_detail.image_path == null || post_detail.image_path== ''">
                          <img src="<?php echo FRONTEND_THEME_URL ?>/images/no-preview.jpg" class="img-thumbnail"></a>
                          <a href="{{site_url}}post-job-update/{{post_detail.encrpt_id}}" ng-if="post_detail.image_path != null && post_detail.image_path != ''">
                          <img src="{{post_detail.image_path}}" class="img-thumbnail"></a>
                        </td>
                        <td>
                          <a ng-href="{{site_url}}post-job-update/{{post_detail.encrpt_id}}" data-toggle="tooltip" tooltip-placement="top" uib-tooltip=""> <span class="postjob-title-name"><b># </b>{{post_detail.id}}<span ng-if="post_detail.listing_title" >,{{post_detail.listing_title}}</span></span>
                            
                            <div class="addressname">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Location"> <i class="fa fa-map-marker" aria-hidden="true"></i> {{post_detail.facility_city}}</span>
                            </div>
                          </a>
                        </td>
                        <td ng-init="getMedicalSpec(post_detail.medical_specialty_name,$index)">
                          <img src="{{post_detail.medical_spec_logo}}" class="pull-left">
                          {{post_detail.medical_spec_name?post_detail.medical_spec_name:"----"}}
                        </td>
                        <td>
                          <div class="date">
                            <div ng-if="post_detail.posted == ''"><label data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Created Date"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> </label><span>{{post_detail.created}}</span></div>
                            <div ng-if="post_detail.posted"><label data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Posted Date"><i class="fa fa-external-link" aria-hidden="true"></i> </label><span>{{post_detail.posted}}</span></div>
                          </div>
                        </td>
                        <td>
                          <span  data-toggle="tooltip" tooltip-placement="Saved in draft" ng-if="post_detail.status == 'Draft'" class="btn btn-status btn-warning btn-xs">In Draft</span>
                          <span  data-toggle="tooltip" tooltip-placement="Admin will Aprrrove" ng-if="post_detail.status == 'Pending'" class="btn btn-status btn-info btn-xs">Pending</span>
                          <span  data-toggle="tooltip" tooltip-placement="Click tp deactive" ng-if="post_detail.status == 'Active'" class="btn btn-status btn-success btn-xs" ng-click="changestatus(2,post_detail.id,currentPage,$index)" >Active</span>
                          <span  data-toggle="tooltip" tooltip-placement="Click tp active" ng-if="post_detail.status == 'Deactive'" class="btn btn-status btn-danger btn-xs" ng-click="changestatus(1,post_detail.id,currentPage,$index)" >Deactive</span>
                          <span  data-toggle="tooltip" tooltip-placement="Job is banned by admin" ng-if="post_detail.status == 'Banned'" class="btn btn-status btn-default btn-xs">Banned</span>
                        </td>
                        <td class="hidden-xs">
                          <div class="action-panel">
                            <a ng-href="{{site_url}}job-preview/{{post_detail.encrpt_id}}" class="btn btn-sm view-preview"  ng-if="post_detail.status != 'Draft'" target="_blank">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="View Preview"><i class="fa fa-eye" aria-hidden="true"></i></span>
                            </a>
                            <a ng-href="{{site_url}}post-job-update/{{post_detail.encrpt_id}}" class="btn btn-sm edit">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit Job"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                              </span>
                            </a>
                            <a class="btn btn-sm btn-delete delete" ng-click="delete_post_job(post_detail.id,$index,currentPage)">
                              <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Delete Job"><i class="fa fa-trash" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <table class="table table-hover jobListTable table-striped" ng-if="post_jobs==0" >
                      <tr>
                        <td  class="text-center nofound-block-td">
                          <div class="text-center nofound-block" >
                            <div class="info-icon">
                              <span>
                                <img src="<?php echo FRONTEND_THEME_URL ?>images/doctor-user.svg" width="50">
                              </span>
                            </div>
                            <h4 class="text-center">No Jobs Added</h4>
                            <div class="col-md-8 col-md-offset-2 center-block nofound-desc">Currently there are no jobs added to your list<br>Click below to post a new job</div>
                            <div><a href="<?php echo base_url('post-job'); ?>" class="button">Post A Job</a></div>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
              <nav>
                <ul class="pagination pull-right">
                  <li><a href="#"  aria-label="Previous" ng-class="{'active1' : (currentPage == '0')}" ng-click="firstPage()" ng-if="post_jobs_count != pageSize && post_jobs_count > pageSize">1</a></li>
                  <li  ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active1' : (n == currentPage)}" href="#" ng-click="setPage()" ng-if="post_jobs_count != pageSize " ng-bind="n+1">1</a></li>
                  <li  ><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active1' : (currentPage == last_index)}" ng-if="post_jobs_count != pageSize && post_jobs_count > pageSize">{{last_index + 1}}</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
.active1{
background:rgb(3,166,237)!important;
color:white!important;
}
.jobListTable tr th:nth-child(3){width: 165px;}
.jobListTable tr th:nth-child(4){width: 165px;}
table.jobListTable tr:first-child{background-color: #f9f9f9;}
table.jobListTable tr td:nth-child(4){vertical-align: middle;}
table.jobListTable tr td:nth-child(5){vertical-align: middle;}
.jobListTable tr th:last-child {
width: 155px;
}
</style>
<!-- <div class="modal fade custom-modal" id="CreateClone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
<div class="modal-dialog login-dialog" role="document">
<div class="modal-content" style="min-height:30px!important;">
<div class="modal-header" >
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

<div class="modal-body" >
<div class="themeform">
<section class="">
<div class="info-modal-block">
<h3 class="heading text-center"> Job Cloning</h3>
<div class="info-icon">
  <span><i class="fa fa-clone" aria-hidden="true"></i></span>
</div>
<div class="clearfix"></div>
<h3 class="text-center title">{{name_listing}}</h3>
<h4 class="text-center">
Clone job functionality will create copy of all parameter of <span>"{{name_listing}}"</span>.</h4>
<div class="clearfix"></div>
<div>
  <br><br>
  <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">Not now</button>
  <button class="button pull-right" type="button" ng-click="create_clone(job_list_id)">Click here to clone the job</button>
</div>
<div class="clearfix"></div>
</div>
</section>
</div>
</div>
</div>
</div>
</div>

-->
<script type="text/javascript">
  
</script>



<?php
$this->load->view('include/common_modal_msg');
$this->load->view('include/footer_menu');
?>


