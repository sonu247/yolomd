<?php 
  $this->load->view('include/header_menu');
?>
<div class="content-container" ng-controller="PostJob">
<div class="formPage-section">
<div class="col-md-7 col-sm-7 full-height left-side">
<div class="themeform" >
<!-- <h1>Post Job</h1> -->
<form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="nextStep()">
<div ng-switch on="postJob" ng-class="{'formsection':(postJob!='13')}" 

 class="">
	<section ng-switch-when="1" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   			<h3 class="heading text-center">Contact Information</h3>
			<div class="row">
				<div class="col-md-12">
				<div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block ">
						<label class="required label title" style="padding:0 8px;">Title<font> :</font> </label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
						<div class="radio radio-info radio-inline">
						<input type="radio" name="contact_name_title" id="contact_name_title" ng-model="PostJobData.contact_name_title" value="Mr."> 
						<label  for="contact_name_title">Mr.</label>
					</div>
					<div class="radio radio-info radio-inline">
						<input type="radio" name="contact_name_title" id="contact_name_title1" ng-model="PostJobData.contact_name_title" value="Miss">
						<label for="contact_name_title1">Miss</label>
					</div>
					<div class="radio radio-info radio-inline">
						<input type="radio" name="contact_name_title" id="contact_name_title2"  ng-model="PostJobData.contact_name_title" value="Mrs."> 
						<label for="contact_name_title2">Mrs.</label>
					</div>
					<div class="radio radio-info radio-inline">
						<input type="radio" name="contact_name_title" id="contact_name_title3" ng-model="PostJobData.contact_name_title" value="Dr."   data-bvalidator="required" data-bvalidator-msg="Name Title Required."> 
						<label for="contact_name_title3">Dr.</label>
					</div>
					<label class="error" ng-if="Error.contact_name_title"> {{ Error.contact_name_title }}</label>
				</div>
				
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group clearfix">
					  <div class="col-md-4 col-sm-4 label-block">
					   <label class="label"> First Name<font> :</font> </label>
					  </div>
					  <div class="col-md-6 col-sm-8 signup-full-column">
						<input type ="text" name="contact_first_name"  ng-model="PostJobData.contact_first_name"  class="form-control" data-bvalidator="alpha,required" data-bvalidator-msg="First Name Required" value="">
					   <label class="error" ng-bind="Error.contact_first_name"></label>
					  </div>
					</div>
				</div>
			</div>
			<div class="row">
			  <div class="col-md-12">
				<div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label">Last Name<font> :</font> </label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<input type="text" name="contact_last_name" ng-model="PostJobData.contact_last_name" class="form-control" data-bvalidator="alpha,required" data-bvalidator-msg="Last Name Required">
				<label class="error" ng-bind="Error.contact_last_name"></label>
				</div>
				</div>
			  </div>
			</div>
			<div class="row">
			  <div class="col-md-12">
				<div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label">Email<font> :</font> </label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<input type="text" name="contact_email_address"  ng-model="PostJobData.contact_email_address"  class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required" id="contact_person_email_address">
				<label class="error" ng-bind="Error.contact_email_address"></label>
				</div>
				</div>
			  </div>
			</div>
			
		  <div class="col-md-4 col-sm-4"></div>
		  <div class="col-md-6 col-sm-8 signup-full-column">
		  <div class="stepToggle-btn">
				<span class="">
				 <button type ="submit" class="btn btn-success down" tooltip-placement="top" uib-tooltip="Next Step"><i class="fa fa-angle-down" aria-hidden="true"></i>
				 </button>
				</span>
			</div>
		  </div>
		  <div class="clearfix"></div>
   		</div>
   </section>

	<section ng-switch-when="2" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
		  <div class="form-section-block">
		<h3 class="heading text-center">Medical Director Information</h3>
		<div class="row">
		   <div class="col-md-12">
			 <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label">Director First name <font> :</font></label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<input type="text" name="director_first_name" ng-model="PostJobData.director_first_name"  class="form-control drName" data-bvalidator="alpha,required" data-bvalidator-msg="First Name Required">
				<span class="short-name">Dr.</span>
				<label class="error" ng-bind="Error.director_first_name"></label>
				</div>
			  </div>
			</div>
		</div>
		<div class="row">
		  <div class="col-md-12">
			<div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label">Director Last name <font> :</font></label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<input type="text" name="director_last_name" ng-model="PostJobData.director_last_name" class="form-control" data-bvalidator="alpha,required" data-bvalidator-msg="Last Name Required">
				<label class="error" ng-bind="Error.director_last_name"></label>
				</div>
				</div>
		  </div>
		</div>
		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label title" style="padding:0 8px;">Gender<font> :</font></label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<div class="radio radio-info radio-inline">
				<input type="radio" name="director_gender" ng-model="PostJobData.director_gender" id="director_gender" value="Male" >
				<label class="radio-inline" for="director_gender">Male </label>
				</div>
				<div class="radio radio-info radio-inline">
				<input type="radio" name="director_gender" id="director_gender1" ng-model="PostJobData.director_gender" value="Female" data-bvalidator="required" data-bvalidator-msg="Required.">
				<label class="radio-inline" for="director_gender1">Female</label>
				</div>
				
				<label class="error" ng-bind="Error.director_gender"></label>
				</div>
			  </div>
			</div>
		</div>
		<div class="row">
		  <div class="col-md-12">
				<div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
					<label class="label">Date of birth<font> :</font></label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<div class="row">
				<div class="col-md-4">
				 <div class="">
				<select name="person_dob_date" ng-model="PostJobData.director_dob_date" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
				<option value="">Date</option>
				<?php 
				for($i=1; $i<=31; $i++)
				{ ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
				</select>
				</div>
				 <label class="error" ng-bind="Error.director_dob_date"></label>
				</div>
				<div class="col-md-4">
				 <div class="">
				<select name="director_dob_month" ng-model="PostJobData.director_dob_month" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
				<option value="">Month</option>
				<?php 
				for($k=1; $k<=12; $k++)
				{ ?>
				<option value="<?php echo $k; ?>"><?php echo $k; ?></option>
				<?php } ?>
				</select>
				</div>
				 <label class="error" ng-bind="Error.director_dob_month"></label>
				</div>
				
				<div class="col-md-4">
				 <div class="">
				<select name="director_dob_year" ng-model="PostJobData.director_dob_year" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
				<option value="">Year</option>
				<?php 
				for($j=date('Y')-85; $j<=date('Y')-15; $j++)
				{ ?>
				<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
				<?php } ?>
				</select>
				</div>
				 <label class="error" ng-bind="Error.director_dob_year"></label>
				</div>
				</div>
				</div>
				</div>
			</div>
		</div>	
		<div class="row">
		  <div class="col-md-12">
			<div class="form-group clearfix">
			  <div class="col-md-4 col-sm-4 label-block">
				<label class="label">Phone Number <font> :</font></label>
			  </div>
			  <div class="col-md-3 col-sm-4 signup-full-column">
				<input type="text" name="director_phone_number" ng-model="PostJobData.director_phone_number" class="form-control" mask='000-000-0000' data-bvalidator="maxlen[12],required" data-bvalidator-msg="Valid Number Required" >
				 <label class="error" ng-bind="Error.director_phone_number"></label>
			  </div>

			  <div class="col-md-1 col-sm-4 label-block">
				<label class="label">Ext.<font> :</font></label>
			  </div>
			  <div class="col-md-2 col-sm-4 signup-full-column">
				<input type="text" name="director_other_number" ng-model="PostJobData.director_other_number" class="form-control"  data-bvalidator="digit,maxlen[5]">
				<span class="optional-tag">(optional)</span>
			  </div>

			</div>
		  </div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group clearfix">
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 label-block"></div>
		<div class="col-md-6 col-sm-8 signup-full-column">
		<div class="stepToggle-btn">
		<span class>
			<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
		</span>
			<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
			</button>
		</div>
		</div>
		<div class="clearfix"></div>

    </section>

	<section ng-switch-when="3" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   		  <h3 class="heading text-center">I am looking for</h3>
			<div class="row">
			  <div class="col-md-12">
				<div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label">Medical Specialties <font> :</font> </label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					<select name="medical_specialty_name" ng-model="PostJobData.medical_specialty_name" class="form-control" ng-if="medical_specialty" data-bvalidator="required" data-bvalidator-msg="Medical Specialities Required">
					<option value="">Select Medical Specialties</option>
					<option ng-repeat="medical_s in medical_specialty" ng-if="medical_s.id" value="{{medical_s.id}}">{{medical_s.name}}</option>
					</select>
					<label class="error" ng-bind="Error.medical_specialty_name"></label>
					</div>
				</div>
			  </div>
			</div>
			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
			<div class="stepToggle-btn">
					<span class="">
					<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
					</span>
					<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<div class="clearfix"></div>
   		</div>
    </section>

    <section ng-switch-when="4" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   		<h3 class="heading text-center">Facility Location</h3>
		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label"> Facility Name<font> :</font> </label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<input type="text" name="facility_name"  ng-model="PostJobData.facility_name"  class="form-control" data-bvalidator="alpha,required" data-bvalidator-msg="Facility Name Required" >
				<label class="error" ng-bind="Error.facility_name"></label>
				</div>
			  </div>
			</div>
		</div>

		
		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label"> Province<font> :</font> </label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<select name="facility_state" ng-model="PostJobData.facility_state" class="form-control"  ng-if="canada_province" data-bvalidator="required" data-bvalidator-msg="Province Required">
				<option value="">Select Province</option>
				<option ng-repeat ="canada_pro in canada_province" ng-if="canada_pro.id" value="{{canada_pro.id}}">{{canada_pro.province_name}}</option>
				</select>
				<label class="error" ng-bind="Error.facility_state"></label>
				</div>
			  </div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label"> City<font> :</font></label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<input type="text" name="facility_city" ng-model="PostJobData.facility_city" class="form-control" data-bvalidator="alpha,required" data-bvalidator-msg="City Required" >
				<label class="error" ng-bind="Error.facility_city"></label>
				
				</div>
			  </div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
			    <div class="col-md-4 col-sm-4 label-block">
				<label class="label"> Address<font> :</font></label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<textarea name ="facility_address" ng-model="PostJobData.facility_address" class="form-control" data-bvalidator="required" data-bvalidator-msg="Address Required"></textarea>
				<label class="error" ng-bind="Error.facility_address"></label>
				</div>
			  </div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label">Postal Code<font> :</font></label>
				</div>
				<div class="col-md-3 col-sm-8 signup-full-column">
				<input type="text" name="facility_postal_code" ng-model="PostJobData.facility_postal_code" class="form-control" data-bvalidator="required,maxlen[7]" data-bvalidator-msg="Valid Code Required">
				<label class="error" ng-bind="Error.facility_postal_code"></label>
				</div>

				<div class="col-md-1 col-sm-4 label-block">
				<label class="label"> Fax<font> :</font> </label>
				</div>

				<div class="col-md-2 col-sm-4 signup-full-column">
				<input type="text" name="facility_fax"  ng-model="PostJobData.facility_fax"  class="form-control" data-bvalidator="required,maxlen[15]" data-bvalidator-msg="Valid Fax Required" >
				<label class="error" ng-bind="Error.facility_fax"></label>

				</div>
			  </div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
			  <div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label">Phone Number<font> :</font></label>
				</div>
				<div class="col-md-3 col-sm-8 signup-full-column">
				<input type="text" name="facility_phone_number" ng-model="PostJobData.facility_phone_number" class="form-control" mask='000-000-0000' data-bvalidator="maxlen[12],required" data-bvalidator-msg="valid Number Required">
				<label class="error" ng-bind="Error.facility_phone_number"></label>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-md-1 col-sm-4 label-block">
				<label class="label">Ext<font> :</font></label>
				</div>
				<div class="col-md-2 col-sm-4 signup-full-column">
				 <input type="text" name="facility_ext_number" ng-model="PostJobData.facility_ext_number" class="form-control"  data-bvalidator="digit,maxlen[5]" >
				 <span class ="optional-tag">(optional)</span>
				</div>

				
				
			  </div>
			</div>
		</div>

		<div class="col-md-4 col-sm-4 label-block"></div>
		<div class="col-md-6 col-sm-8 signup-full-column">
			<div class="stepToggle-btn">
				<span class="">
							<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
						</span>
				<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
			</div>
		</div>
		<div class="clearfix"></div>

   	</div>
   </section>

	<section ng-switch-when="5" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
           <div class="form-section-block">
             <h3 class="heading text-center">Public Transport </h3>
             <div class="row">
             <h4 class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
             </h4>
             <br>
              <div class="col-md-12" ng-if="transport_type">
                <div class="form-group clearfix">
<!--                   <div class="col-md-4 col-sm-4 label-block">
                    <label class="label"> Transport <font> :</font> </label>
                  </div> -->
                  <div class="col-md-8 col-md-offset-2 col-sm-8 signup-full-column transportBlock" >
                  <div >
                  <div class="checkBlock">
                       <div class="checkbox checkbox-info">
                       <input type="checkbox" name="transport[]"  id="transport_metro_subway" ng-model="PostJobData.transport_metro_subway" >
                       <label for="transport_metro_subway">Metro/Subway</label>
                       </div>
                  </div>
                  <div class="form-group" ng-show="PostJobData.transport_metro_subway">
                        <input type ="text" name="transport_m_s_value"  ng-model="PostJobData.transport_m_s_value"  class="form-control"  >
                     </div>

                  <div class="clearfix"></div>
                  <div class="checkBlock">
                       <div class="checkbox checkbox-info">
                       <input type="checkbox" name="transport[]"  id="transport_bus" ng-model="PostJobData.transport_bus">
                       <label for="transport_bus">Bus</label>
                       </div>
                  </div>
                  <div class="form-group" ng-show="PostJobData.transport_bus">
                    <input type ="text" name="transport_bus_value"  ng-model="PostJobData.transport_bus_value"  class="form-control" >
                  </div>
                  <div class="clearfix"></div>
                  <div class="checkBlock">
                       <div class="checkbox checkbox-info">
                       <input type="checkbox" name="transport[]"  id="transport_other" ng-model="PostJobData.transport_other" data-bvalidator="required" data-bvalidator-msg="Transport Required">
                       <label for="transport_other">Other</label>
                       </div>   
                  </div>
                   <div class="form-group" ng-show="PostJobData.transport_other">
                        <input type ="text" name="transport_other_value"  ng-model="PostJobData.transport_other_value"  class="form-control" >
                   </div>

                  <div class="clearfix"></div>
                  </div>
                 <!--  <label class="error" ng-bind="Error.transport_array"></label> -->
                 </div>

	               <div class="col-md-4 col-sm-4 label-block"></div>
				   <div class="col-md-6 col-sm-8 signup-full-column">
						<div class="stepToggle-btn">
							<span class="">
										<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
									</span>
							<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
							</button>
						</div>
					</div>

                </div>
              </div>
            </div>
        </div>
     </section>

	<section ng-switch-when="6" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
	   		<div class="form-section-block">
	   			<h3 class="heading text-center">Facility Details</h3>
	   			<div class="row">
				  <div class="col-md-12">
					<div class="form-group clearfix">
						<div class="col-md-4 col-sm-4 label-block">
						<label class="label"> Facility Type <font> :</font> </label>
						</div>
						<div class="col-md-6 col-sm-8 signup-full-column">
						<select name="facility_type" ng-model="PostJobData.facility_type" class="form-control"  ng-if="facility_type" data-bvalidator="required" data-bvalidator-msg="Type Required">
						<option value="">Select Facility Type</option>
						<option ng-repeat ="(key,value) in facility_type"  ng-if="key" value="{{key}}">{{value}}</option>
						</select>
						 <label class="error" ng-bind="Error.facility_type"></label>
						</div>
					</div>
				  </div>
				</div>
				<div class="row">
				   <div class="col-md-12">
					  <div class="form-group clearfix">
						<div class="col-md-4 col-sm-4 label-block">
						<label class="required label title" style="padding:0 8px;"> Parking<font> :</font> </label>
						</div>
						<div class="col-md-6 col-sm-8 signup-full-column">
						<!-- <input type="text" name="paking"  ng-model="PostJobData.paking"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Paking is Required" > -->
	
						
	
	
							<div class="radio radio-info radio-inline">
							<input type="radio" name="parking" id="parking_free" value="Free"  ng-model="PostJobData.parking">
							<label class="radio-inline" for="parking_free" >Free </label>
							</div>
							<div class="radio radio-info radio-inline">
							<input type="radio" name="parking" id="parking_paid" value="Paid" ng-model="PostJobData.parking" data-bvalidator="required" data-bvalidator-msg="Parking Required.">
							<label class="radio-inline" for="parking_paid">Paid</label>
							</div>
	
	
						<label class="error" ng-bind="Error.parking"></label>
						</div>
					  </div>
					</div>
				</div>
						   		<div class="row">
					<div class="col-md-12">
					  <div class="form-group clearfix">
						<div class="col-md-4 col-sm-4 label-block">
						<label class="label">EMR<font> :</font> </label>
						</div>
						<div class="col-md-6 col-sm-8 signup-full-column">
						<input type="text" name="emr"  ng-model="PostJobData.emr"  class="form-control" >
						<span class ="optional-tag">(optional)</span>
						</div>
					  </div>
					</div>
				</div>
			<div class="row">
				<div class="col-md-12">
				  <div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label">Number of full-time doctors<font> :</font> </label>
					</div>
					<div class="col-md-2 col-sm-8 signup-full-column">
					<input type="number" name="no_of_fulltime"  ng-model="PostJobData.no_of_fulltime"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Required" min="0" >
					<label class="error" ng-bind="Error.no_of_fulltime"></label>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-md-2 col-sm-4  no-padding label-block">
						<label class="label">Part-time doctors<font> :</font> </label>
					 </div>
					<div class="col-md-2 col-sm-8 signup-full-column">
						<input type="number" name="part_time_doctor" min="0" ng-model="PostJobData.part_time_doctor"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Required" >
						<label class="error" ng-bind="Error.part_time_doctor"></label>
					</div>
	
	
	
				  </div>
				</div>
			</div>
	
			<div class="row">
				  <div class="col-md-12">
					<div class="form-group clearfix">
					  <div class="col-md-4 col-sm-4 label-block">
						<label class="label">Number of exam rooms<font> :</font> </label>
						</div>
					  <div class="col-md-2 col-sm-8 signup-full-column">
						<input type="number" name="exam_room"  ng-model="PostJobData.exam_room"  class="form-control" data-bvalidator="required" min="0" data-bvalidator-msg="Required" >
						<label class="error" ng-bind="Error.exam_room"></label>
						</div>
					</div>
				  </div>
				</div>
			<div class="row">
				  <div class="col-md-12" ng-if="nursing">
					<div class="form-group clearfix">
					  <div class="col-md-4 col-sm-4 label-block">
						<label class="label"> Nursing <font> :</font> </label>
					  </div>
					  <div class="col-md-7 col-sm-8 signup-full-column">
						<div class="row checkbox-group">
	
						<div class="col-md-6"  ng-repeat ="nurs in nursing" ng-if="nurs.id" >
						<div class="checkbox checkbox-info" ng-if="($index + 1) != nursing.length">
	
						<input type="checkbox"  id="{{nurs.id}}" name="nursing_array[]"  ng-model="PostJobData.nursing_array[nurs.id]" value="{{nurs.id}}" ng-if="($index + 1) != nursing.length">
						<label for="{{nurs.id}}">{{nurs.name}}</label>
	
						</div>
	
						<div class="checkbox checkbox-info" ng-if="($index + 1) == nursing.length">
	
						<input type="checkbox"  id="{{nurs.id}}" name="nursing_array[]"  ng-model="PostJobData.nursing_array[nurs.id]" value="{{nurs.id}}" ng-if="($index + 1) == nursing.length" data-bvalidator="required" data-bvalidator-msg="Required.">
						<label for="{{nurs.id}}">{{nurs.name}}</label>
						</div>
						</div>
						</div>
						<!-- <label class="error" ng-bind="Error.nursing_array"></label> -->
						</div>
					</div>
				  </div>
				</div>
				<div class="col-md-4 col-sm-4 label-block"></div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<div class="stepToggle-btn">
					<span class>
						<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
					</span>
						<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
					</button>
				</div>
				</div>
				<div class="clearfix"></div>
	   		</div>
	    </section>

	<section ng-switch-when="7" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   			<h3 class="heading text-center">Facility Hours</h3>
   			<div class="row">
				<div class="col-md-12">
				  <div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label"> Facility Hours<font> :</font> </label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					<div class="row dobBlock">
					<div class="col-md-4 col-sm-8 date">
						<div ng-repeat ="(key,value) in days_name">
							<div class="checkbox checkbox-info">
								<input type="checkbox" id="{{key}}" name="facility_hours[]" value="{{value}}" ng-model="PostJobData.facility_hours[key]" >
								<label for="{{key}}">{{value}}</label>



							</div>
						</div>
					   <label class="error" ng-bind="Error.facility_hours"></label>
					</div>

					<div class="col-md-4 col-sm-8 month">

					<select name="start_time" ng-model="PostJobData.start_time" data-bvalidator="required" class="form-control"  data-bvalidator-msg="Required">
								<option value="">Start Time</option>
								<?php
								     $time = '12:00'; // start
								     for ($i = 0; $i <= 24; $i++)
								     {
								         $prev = date('g:i a',strtotime($time)); // format the start time
								         $next = strtotime('+30mins',strtotime($time)); // add 30 mins
								         $time = date('g:i a',$next); // format the next time
								         echo "<option value=\"$prev\">$prev</option>";
								     }
								 ?>
								</select>


					</div>

					<div class="col-md-4 col-sm-8 year">

					<select name="end_time" ng-model="PostJobData.end_time" data-bvalidator="required" class="form-control"  data-bvalidator-msg="Required">
								<option value="">End Time</option>
								<?php
								     $time = '12:00'; // start
								     for ($i = 0; $i <= 24; $i++)
								     {
								         $prev = date('g:i a', strtotime($time)); // format the start time
								         $next = strtotime('+30mins', strtotime($time)); // add 30 mins
								         $time = date('g:i a', $next); // format the next time
								         echo "<option value=\"$prev\">$prev</option>";
								     }
								 ?>
								</select>
					
					</div>

					</div>

					<div class="addMoreWarp">
					<br>
						<a href="#" class="">
						<i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
					</div>

					</div>
				</div>			  
				</div>
			</div>
			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
   			<div class="stepToggle-btn">
				<span class>
					<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
				</span>
				<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
			</div>
			</div>
			<div class="clearfix"></div>
   		</div>
    </section>

	<section ng-switch-when="8" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   			<h3 class="heading text-center">Job Description</h3>
   			<div class="row">
				<div class="col-md-12">
				  <div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label">Hours<font> :</font> </label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					<select name="hour" ng-model="PostJobData.hour" data-bvalidator="required" class="form-control"  ng-if="hours" data-bvalidator-msg="Required">
					<option value="">Select Hours</option>
					<option ng-repeat ="(key,value) in hours" ng-if="key" value="{{key}}">{{value}}</option>
					</select>
					<label class="error" ng-bind="Error.hour"></label>
					</div>
				  </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
				 <div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label title" style="padding:0 8px;">Position<font> :</font></label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					<div class="radio radio-info radio-inline">
					<input type="radio" name="position" ng-model="PostJobData.position" id="permanent" value="Permanent" >
					<label class="radio-inline" for="permanent">Permanent </label>
					</div>
					<div class="radio radio-info radio-inline">
					<input type="radio" name="position" id="locum" ng-model="PostJobData.position" value="Locum" data-bvalidator="required" data-bvalidator-msg="Required.">
					<label class="radio-inline" for="locum">Locum</label>
					</div>
					<label class="error" ng-bind="Error.position"></label>
					</div>
				 </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group clearfix">
					  <div class="col-md-4 col-sm-4 label-block">
						<label class="label">Start Date<font> :</font></label>
					  </div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					  <div class="row">
					  <div class="col-md-4">
							<div class="">
								<select name="start_month" ng-model="PostJobData.start_month" ng-if="month_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
									<option value="">Month</option>
									<option ng-repeat ="(key,value) in month_name" ng-if="key" value="{{key}}">{{value}}</option>
								</select>
							  </div>
							<label class="error" ng-bind="Error.start_month"></label>
						</div>
						<div class="col-md-4">
								<div class="">
									<select name="start_date" ng-model="PostJobData.start_date" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
										<option value="">Date</option>
											<?php 
											for($i=1; $i<=31; $i++)
											{ ?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
											<?php } ?>
									</select>
								</div>
						<label class="error" ng-bind="Error.start_date"></label>
						</div>
					    <div class="col-md-4">
						<div class="">
							<select name="start_year" ng-model="PostJobData.start_year" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
							 <option value="">Year</option>
								<?php 
								for($j=date('Y'); $j<=date('Y')+2; $j++)
								{ ?>
									<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
								<?php } ?>
							</select>
						</div>
					   <label class="error" ng-bind="Error.start_year"></label>
					  </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label">Compensation <font> :</font> </label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					<select name="compensation" ng-model="PostJobData.compensation" class="form-control"  ng-if="compensation">
					<option value="">Select Compensation</option>
					<option ng-repeat ="(key,value) in compensation" ng-if="key" value="{{key}}">{{value}}</option>
					</select>
					<span class ="optional-tag">(optional)</span>
					</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
				  <div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="required label title" style="padding:0 8px;"> Call Responsibilities<font> :</font></label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
				<!-- 	<input type="text" name="call_responsibility" ng-model="PostJobData.call_responsibility" class="form-control" data-bvalidator="required" data-bvalidator-msg="Call Responsibilities Required"> -->
					<div class="radio radio-info radio-inline">
						<input type="radio" name="call_responsibility" id="responsibilities_free" value="Free Type" ng-model="PostJobData.call_responsibility">
						<label class="radio-inline" for="responsibilities_free" >Free Type </label>
					</div>
					<div class="radio radio-info radio-inline">
						<input type="radio" name="call_responsibility" id="responsibilities_none" ng-model="PostJobData.call_responsibility" value="None" data-bvalidator="required" data-bvalidator-msg="Required.">
						<label class="radio-inline" for="responsibilities_none" >None</label>
					</div>

					 <label class="error" ng-bind="Error.call_responsibility"></label>
					</div>
				  </div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
   			<div class="stepToggle-btn">
				<span class="">
					<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
				</span>
				<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
			</div>
			</div>
			<div class="clearfix"></div>
   		</div>
   </section>
    
    <section ng-switch-when="9" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
		<div class="form-section-block">
			<h3 class="heading text-center">Allied Health Professionals</h3>
			<div class="row">
			<div class="col-md-12" ng-if="health_professionals">
				<div class="form-group clearfix">
				<div class="col-md-4 col-sm-4 label-block">
				<label class="label"> Allied Health Professionals: </label>
				</div>
				<div class="col-md-6 col-sm-8 signup-full-column">
				<div class="row checkbox-group">
				<div class="col-md-6" ng-repeat ="health_p in health_professionals" ng-if="health_p.id">
				<div class="checkbox checkbox-info" ng-if="($index + 1) != health_professionals.length">
				<input type="checkbox" name="health_array[]" id="{{health_p.id}}" ng-model="PostJobData.health_array[health_p.id]" value="health_p.id" ng-if="($index + 1) != health_professionals.length"  > <label for="{{health_p.id}}" >{{health_p.name}}</label>
				
				</div>
				<div class="checkbox checkbox-info" ng-if="($index + 1) == health_professionals.length">
				<input type="checkbox" name="health_array[]" id="{{health_p.id}}" ng-model="PostJobData.health_array[health_p.id]" value="health_p.id" ng-if="($index + 1) == health_professionals.length" data-bvalidator="required" data-bvalidator-msg="Required." > <label for="{{health_p.id}}" >{{health_p.name}}</label>
				
				</div>
				</div>
			</div>
				<!-- <label class="error" ng-bind="Error.health_array"></label> -->
				</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 label-block"></div>
		<div class="col-md-6 col-sm-8 signup-full-column">
			<div class="stepToggle-btn">
			<span class>
				<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
			</span>
				<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
			</button>
		</div>
		</div>
		<div class="clearfix"></div>
		</div>
</section>

	<section ng-switch-when="10" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   			<h3 class="heading text-center">Listing Description </h3>
   			<div class="row">
			  <div class="col-md-12">
				<div class="form-group clearfix">
				  <div class="col-md-4 col-sm-4 label-block">
					<label class="label">Listing Title<font> :</font></label>
				  </div>
				  <div class="col-md-6 col-sm-8 signup-full-column">
					<input type="text" maxlength="30" ng-trim="false" name="listing_title" ng-model="PostJobData.listing_title" class="form-control" data-bvalidator="required" data-bvalidator-msg="Title Required" placeholder="Ex. Busy Clinic Downtown (as example)">
					<span>{{30 - PostJobData.listing_title.length}} Remaining</span>
					<label class="error" ng-bind="Error.listing_title" ></label>
					</div>
				</div>
			  </div>
			</div>
			<div class="row">
				<div class="col-md-12">
				  <div class="form-group clearfix">
					<div class="col-md-4 col-sm-4 label-block">
					<label class="label"> Listing Description<font> :</font>
					</label>
					</div>
					<div class="col-md-6 col-sm-8 signup-full-column">
					<textarea   maxlength="5000" ng-trim="false" name="listing_description" ng-model="PostJobData.listing_description" class="form-control" data-bvalidator="required" data-bvalidator-msg="Description Required" rows="7" placeholder="Briefly describe your facility, what you are looking for and the benefits of working here; Radiology available, Specialties available etc. Please NO phone numbers, e-mail addresses or hyperlinks">
					</textarea>
					
					<span ng-init="maxLimitOfListingDescription=5000">{{maxLimitOfListingDescription - PostJobData.listing_description.length}} remaining</span>
					<label class="error" ng-bind="Error.listing_description"></label>
					</div>
				  </div>
				</div>
				 
    				
			</div>
			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
   			<div class="stepToggle-btn">
				<span class>
					<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
				</span>
					<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
			</div>
			</div>
			<div class="clearfix"></div>
   		</div>
   </section>

    <section ng-switch-when="11" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   			<h3 class="heading text-center">Special Qualifications </h3>
   			<div class="row">
			  <div class="col-md-12">
				<div class="form-group clearfix">
				  <div class="col-md-4 col-sm-4 label-block">
					<label class="label"> Special Qualifications<font></font>:</label>
				  </div>
				  <div class="col-md-6 col-sm-8 signup-full-column">
					<textarea  maxlength="300" ng-trim="false" name="special_qualification" ng-model="PostJobData.special_qualification" class="form-control" data-bvalidator="required" data-bvalidator-msg="Qualification Required" rows="7" placeholder="Example: ATLS certified, Subspecialty in Interventional Cardiology, Licenced under the College of Physicians and Surgeons of Ontario etc.">
					</textarea>
					<span>{{300 - PostJobData.special_qualification.length}} remaining</span>
					<label class="error" ng-bind="Error.special_qualification"></label>
					</div>
				</div>
			  </div>
			</div>
			
			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
   			<div class="stepToggle-btn">
				<span class>
					<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
				</span>
					<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
			</div>
			</div>
			<div class="clearfix"></div>
   		</div>
   </section>

	<section ng-switch-when="12" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
   		<div class="form-section-block">
   			<h3 class="heading text-center">Upload photos</h3>

   			<div class="uploadPhotoBlock">
   			<div class="row" >
   			<div class="col-md-11 picUploadContent" >
   			<div class="my-drop-zone" nv-file-drop="" uploader="uploader" >
				<!-- <h3>Select files</h3> -->
				<span class="icon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                <span class="text">Choose file or Drag here</span>
                    <input type="file" nv-file-select="" uploader="uploader" multiple="" />

               <div class="leftSide">
	   			<ul>
	   			<li>Please share <font>5</font> relevant pictures of the your medical facility </li>
	   			<li>Like ( 1: Reception, 2: Waiting Room, 3: Exam Room )</li>
	   			</ul>
	   		  </div>
            </div>

   			</div>
              <div class="text-center photo-link">
			 See how to take professional looking photos yourself &nbsp;&nbsp;<a href="#" class="">Click Here</a>
			</div>
   			</div>
   			<div class="clearfix"></div>

   			

			<div class="row">

                <div class="col-md-12" >
                <div class="col-md-1"></div>
                <div ng-repeat="item in uploader.queue" class="col-md-2" >
                
                <div class="canvas-warp">
                   <div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, width: 120 }"></div>
                </div>
                        <div class="progress" style="margin-top: 10px;">
                            <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                        </div>
                        <span ng-show="item.isSuccess" class="btn btn-success btn-xs">
                        <i class="glyphicon glyphicon-ok"></i> Uploded</span>
                        <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                        <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                       
                      <button type="button" class="btn btn-danger btn-xs pull-right" ng-click="item.remove()">
                            <i class="fa fa-close"></i>
                        </button>
                    
                    </div>
                </div>
            <div class="clearfix"></div>
            <br>
	        <div class="text-center" >
	         <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
	                <span class="glyphicon glyphicon-upload"></span> Upload all
	            </button>
	         </div>
	      
            </div>
			<!-- Upload demo area  -->
			</div>



			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
   			<div class="stepToggle-btn">
				<span class="">
					<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
				</span>
					<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
				
			</div>
			</div>
			<div class="clearfix"></div>
			<br><br>


   		</div>

   </section>

    <section ng-switch-when="13" class=" {{animationClass}} wow" ng-class="{{animationClass}}">
       <div class="form-section-block">

		<div class="col-md-12 center-block no-padding">
   			<div class="jobPreview jobPostpagePreview row">
	   			
	   		<div class="form-group row">
	   		<slick class="slider single-item postJobSlider" current-index="index" responsive="breakpoints" dots=false infinite=true speed=300 slides-to-show=1 touch-move=false slides-to-scroll=1 init-onload=true data="PostJobData.uploadImageArray">
			   <div  ng-repeat="uploadImage in PostJobData.uploadImageArray" >
			   <div class="jobPostSlide">
			     <img  src="<?php echo base_url('assets/uploads/jobs/') ?>/{{uploadImage.imageName}}" class="img-responsive">
			   </div>
			   </div>
	    	</slick>
	   		</div>


   			<div class="form-group row title">
   			 <div class="col-md-12 " ng-if="PostJobData.listing_title">
   			 	<div class="name">{{PostJobData.listing_title}}</div>
   			 </div>
   			 <div class="col-md-12 jobSpeciality" >
   			 	<span class="city" ng-if="PostJobData.facility_city">{{PostJobData.facility_city}}</span>
   			 	<div class="pull-right" ng-if="MedicalSpecialtyValue">
   			 	Specialties : <font>{{MedicalSpecialtyValue}}</font>
   			 	</div>
   			 	<div class="clearfix"></div>
   			 </div>
   			</div>

   			<div class="col-group form-group row" ng-if="PostJobData.contact_name_title != null || PostJobData.contact_first_name != null || PostJobData.contact_last_name != null">
   				<div class="col-md-3 label-name">
   				  <b>Contact Information</b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				<div>
   				 <span>{{PostJobData.contact_name_title}} {{PostJobData.contact_first_name}} {{PostJobData.contact_last_name}}</span>
   				 </div>
   				 <div>
   				 	<span>{{PostJobData.contact_email_address}}</span>
   				 </div>
   				</div>
   				<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div>   

   			<div class="col-group form-group row" ng-if="PostJobData.listing_description">
   				<div class="col-md-3 label-name">
   					<b>About</b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				<div class="more">
   					{{PostJobData.listing_description}}
   				</div>
   				</div>
   			   	<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div>

   			<div class="col-group form-group row" ng-if="PostJobData.facility_city != null || ProvinceName != null || PostJobData.facility_address != null || PostJobData.facility_postal_code != null || PostJobData.facility_fax != null || PostJobData.facility_phone_number != null">
   				<div class="col-md-3 label-name">
   					<b>Facility Location</b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				 <div class="locationBlock row">
   				 	
   				 	<div class="col-md-5">
   				 	<div ng-if="PostJobData.facility_city"><label>City :</label> <span>{{PostJobData.facility_city}}</span></div>
   				 	<div ng-if="ProvinceName"><label>State :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{ProvinceName}}</span></span></div>
   				 	<div ng-if="PostJobData.facility_address"><label>Address :</label> <span>{{PostJobData.facility_address}}</span> </div>
   				 	</div>

   				 	<div class="col-md-5">
   				 	<div ng-if="PostJobData.facility_postal_code"><label>Postal Code :</label> <span>{{PostJobData.facility_postal_code}}</span> </div>
   				 	<div ng-if="PostJobData.facility_fax"><label>Fax :</label> <span>{{PostJobData.facility_fax}}</span> </div>
   				 	<div ng-if="PostJobData.facility_phone_number"><label>Phone :</label> <span>{{PostJobData.facility_phone_number}}</span> </div>
   				 	</div>
   				 </div>
   				</div>
   			   	<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div>

   			<div class="col-group form-group row" ng-if="PostJobData.transport_metro_subway != null || PostJobData.transport_m_s_value != null || PostJobData.transport_bus != null || PostJobData.transport_bus_value != null || PostJobData.transport_other != null || PostJobData.transport_other_value != null">
   				<div class="col-md-3 label-name">
   					<b>Public Transport </b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				<div >
					<div ng-if="PostJobData.transport_metro_subway">
					Metro/Subway
					</div>
					<div ng-if="PostJobData.transport_m_s_value != null && PostJobData.transport_metro_subway != null" >
					{{PostJobData.transport_m_s_value}}
					</div>
					<div ng-if="PostJobData.transport_bus">
					Bus
					</div>
					<div ng-if="PostJobData.transport_bus_value != null && PostJobData.transport_bus != null">
					{{PostJobData.transport_bus_value}}
					</div>
					<div ng-if="PostJobData.transport_other">
					Other
					</div>
					<div ng-if="PostJobData.transport_other_value != null && PostJobData.transport_other != null">
					{{PostJobData.transport_other_value}}
					</div>

   				</div>
   				</div>
   	   			<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div>

   			<div class="col-group form-group row" ng-if="FacilityType != null || PostJobData.parking != null || PostJobData.emr != null || PostJobData.no_of_fulltime != null || PostJobData.part_time_doctor != null || PostJobData.exam_room != null">
   				<div class="col-md-3 label-name">
   					<b>Facility Details </b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				<div class="row">
				<div class="col-md-6" ng-if="FacilityType"><label>Type :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{FacilityType}}</span></span></div>

				<div class="col-md-6" ng-if="PostJobData.parking"><label>Parking :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{PostJobData.parking}}</span></span></div>

				<div class="col-md-6" ng-if="PostJobData.emr"><label>EMR :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{PostJobData.emr}}</span></span></div>

				<div class="col-md-6" ng-if="PostJobData.no_of_fulltime"><label>Number of full-time doctors :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{PostJobData.no_of_fulltime}}</span></span></div>

				<div class="col-md-6" ng-if="PostJobData.part_time_doctor"><label>Part-time doctors :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{PostJobData.part_time_doctor}}</span></span></div>

				<div class="col-md-6" ng-if="PostJobData.exam_room"><label>Number of exam rooms :</label> <span><span data-toggle="tooltip" data-placement="top" title="Province">{{PostJobData.exam_room}}</span></span></div>

				</div>
   				</div>

   		   		<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div>

   			<div class="col-group form-group row" ng-if="PostJobData.facility_hours != null || PostJobData.start_time != null || PostJobData.end_time != null">
   				
   				<div class="col-md-3 label-name">
   					<b>Facility Hours </b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				<div class="row">
   					<div class="col-md-4" ng-if="PostJobData.facility_hours"><label>Hours :</label> <span>{{PostJobData.facility_hours}}</span></div>
   					<div class="col-md-4" ng-if="PostJobData.start_time"><label>Start time :</label> <span>{{PostJobData.start_time}}</span></div>
   					<div class="col-md-4" ng-if="PostJobData.end_time"><label>End time :</label> <span>{{PostJobData.end_time}}</span></div>   					
   				</div>
   				</div>
   	   			<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div>
   				

	   		<div class="col-group form-group row" ng-if="HealthProfessional">
   				<div class="col-md-3 label-name">
   					<b>Allied Health Professionals </b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   					{{HealthProfessional}}
   				</div>
   				<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
	   		</div> 

   			<div class="col-group form-group row" ng-if="PostJobData.special_qualification">
   				<div class="col-md-3 label-name">
   					<b>Special Qualifications</b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
   				 {{PostJobData.special_qualification}}
   				</div>
      		<span class="col-group-edit"><a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
   			</div> 

   			<!-- <div class="col-group form-group row" ng-if="uploader.queue">
   				<div class="col-md-3 label-name">
   					<b>Pictures for medical facility</b>
   				</div>
   				<div class="col-md-9 jobDetailBlock">
	   				<div class="row">
	                <div class="col-md-12" >
	                  <div ng-repeat="item in uploader.queue" class="col-md-2" >
		                <div class="canvas-warp">
		                   <div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, width: 70 }"></div>
		                </div>                    
	                    </div>
	                 </div>
	           		<div class="clearfix"></div>
	           		</div>
   				</div>
   			</div>    			   	 -->		    			 			
   			</div>
   			</div>


			<div class="col-md-4 col-sm-4 label-block"></div>
			<div class="col-md-6 col-sm-8 signup-full-column">
		   	<div class="stepToggle-btn">
				<span class="">
							<a href="#" class="btn btn-success up" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Previous Step" ng-click="previousStep()"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
						</span>
				<button type="submit" class="btn btn-success down" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Next Step" ><i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
			</div>
			</div>

			<div class="clearfix"></div>
		</div>
		</section>

 </div>
 </form>
 <div class="process-bar col-md-7 col-sm-7 col-xs-12" ng-if="postJob!='13'">
	<div class="progress-section">
	<!--progress bar strip-->
   	<div class="progress">
		<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{progressBar}}%">
			<span class="sr-only">{{progressBar}}% Complete </span>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="completion"><font> {{progressBar}}% </font> Complete (success)</div>
	<!--progress bar strip end-->
	</div>
  </div>
 </div>
</div>
<div class="col-md-5 col-sm-5 register-right-section page-container" ng-class="{'mapBlock':(postJob=='13')}">
 
     <div ng-if="postJob=='13'">
			<div ng-controller="getmap">
			     <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" events="map.events">
					<ui-gmap-marker ng-repeat="m in map.markers" coords="m.coords" icon="m.icon" idkey="m.id"></ui-gmap-marker>
				</ui-gmap-google-map>
			</div>
	</div>

	 <div class="" ng-if="postJob!='13'">
	 <div class="col-md-10 col-md-offset-1 register-right-content signup-page">
		<div class="content-block">
		<div class="content-block-inner">
			<h3>Paypal Priority Placement:</h3>
			<div ng-if="paypal_priority" >
				<div ng-bind-html="paypal_priority.option_value"></div>
			</div>
			<h3>Verified Facility</h3>
			<div ng-if="verified_facility" >
				<div ng-bind-html="verified_facility.option_value"></div>
			</div>
			</div>
		</div>
      </div>
   </div>
 </div>
<div class="clearfix"></div>
</div>
</div>

<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
<script type="text/javascript">
 wow = new WOW(
    {
      boxClass:     'wow',    
      animateClass: 'animated', 
      offset:       200,         
      mobile:       true,       
      live:         true        
    }
  )
  wow.init();
  //event.stopPropagation();
</script>

<style type="text/css">
	body{overflow: hidden;}
	.angular-google-map-container { height: 100%; margin-top: -45px;}
</style>
