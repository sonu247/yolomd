<?php 
  $this->load->view('include/header_menu');
?>
<div class="page-container">

  <div class="">
    <div class="container">
    <div class="dashboard">

      <div class="row">
        <div class="col-md-3 col-sm-3 dashboard-Left-warp">
        <?php $this->load->view('account/MedicalLeftNavigation'); ?>
       </div>
        <div class="col-md-9 col-sm-9">

        <div class="dashboard-right">
       
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.png" class="">Payment Receipts - <span ng-if="JobDetail.job_title" class="payment-receipts-name ">{{JobDetail.job_title}}</span>
        </h3>
         <div ng-if="JobDetail">
          <div class="table-responsive reciept-download">
        	<table class="table table-hover  table-striped table-condensed table-bordered">
        	
        	<tr ng-if="JobDetail.transaction_id">
          <td class="text-right">Transaction Id :</td>
        	<td class="">  
            {{JobDetail.transaction_id}}
          </td>
        	</tr>
          <tr  ng-if="JobDetail.date">
          <td class="text-right">Pay Date :</td>
          <td class="">  
              {{JobDetail.date}}

          </td>
          </tr>
<!--           <tr ng-if="JobDetail.job_title">
          <td class="text-right">Job title</td>
          <td class="">  
             {{JobDetail.job_title}}
          </td>
          </tr> -->
          <tr ng-if="JobDetail.job_id">
          <td class="text-right">Posted Job Id :</td>
          <td class="">  
              <a href="{{site_url}}post-job-update/{{JobDetail.encrypt_id}}">#{{JobDetail.job_id}}</a>
          </td>
          </tr>
          <tr ng-if="JobDetail.verify">
          <td class="text-right">YoloMD Verify :</td>
          <td class="">  
            {{JobDetail.verify}}
          </td>
          </tr>
          <tr ng-if="JobDetail.item">
          <td class="text-right">Services :</td>
          <td class="">  
             {{JobDetail.item}}

          </td>
          </tr>
          <tr ng-repeat="city_d in CityDetail" ng-if="CityDetail">
              <td class="text-right" ng-if="city_d.city_name">
                 {{city_d.city_name}} :
              </td>

              <td class="" ng-if="city_d.city_charge">  
                +{{city_d.city_charge| currency}}  <span ng-if="city_d.valid_date != '0000-00-00' && formatDate(city_d.valid_date) != '(Expired)' " >(Active Untill : {{formatDate(city_d.valid_date)| date:'d MMM y'}})</span>
                <span ng-if="city_d.valid_date != '0000-00-00' && formatDate(city_d.valid_date) == '(Expired)' " >{{formatDate(city_d.valid_date)| date:'d MMM y'}}</span>
                <span ng-if="city_d.valid_date == '0000-00-00'">(NA - Not Active)</span>
              </td>
          </tr>
           
          <tr ng-if="JobDetail.yolomd_verify">
          <td class="text-right">Verify Charged :</td>
          <td class="">  
              +{{JobDetail.yolomd_verify}}
          </td>
          </tr>
          <tr ng-if="JobDetail.payment_amount" >
          <td class="text-right">Payment Amount :</td>
          <td class=""> 
              {{JobDetail.payment_amount}} 
          </td>
          </tr>
         
         <tr ng-if="JobDetail.payment_amount" class="paytotalamount">
          <td class="text-right">Paid Amount :</td>
          <td class=""> 
              {{JobDetail.paid_amount}} 
          </td>
          </tr>
          
			   
          </table>
          <div class="clearfix"></div>
          <div class="pull-right">        
            <span class="receipt-download-btn-block" ng-if="JobDetail">
              <a ng-href="cart/generate_pdf/{{JobDetail.unique_id}}" class="button" target="_self"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
            Pdf Receipt</a>
            </span>
          </div>
          <div class="clearfix"></div>
          </div>
          </div>
        <div ng-if="message_error" class="error">
         {{message_error}}
          </div>
			</div>
		   </div>
		    	</div>
		    </div>
		  </div>
		</div>
		</div>
	
<style type="text/css">
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}

</style>
<?php 
  $this->load->view('include/footer_menu');
?>