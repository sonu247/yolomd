<?php
   $this->load->view('include/header_menu');
   ?>
<div class="page-container">
   <div class="">
      <div class="container">
         <div class="dashboard">
            <div class="row">
               <!--Left Section-->
               <div class="col-md-3 col-sm-3 dashboard-Left-warp">
                  <?php $this->load->view('account/PhysicianLeftNavigation'); ?>
               </div>
               <!--Left Section End-->
               <!--Right Section-->
               <div class="col-md-9 col-sm-9">
                  <!--Right Section Breadcrumb-->
                  <!--<div class="breadcrumb-block">
                     <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                            <li><a href="#">Library</a></li>
                            <li class="active">Data</li>
                     </ol>
                     </div> -->
                  <!--Right Section Breadcrumb End-->
                  <div class="dashboard-right">
                     <h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/profile.png" class="">Edit Profile</h3>
                     <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                        <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
                     </div>
                     <div class="message-center">
                        <div class="row">
                           <div class="dashboard-edit-profile">
                              <div class="themeform">
                                 <div class="col-md-12">
                                    <h4 class="extraLines">Use the form below to update your profile. The e-mail address registered with your account is your unique identifier.</h4>
                                 </div>
                                 <div class="clearfix"></div>
                                 <br>
                                 <form  method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="PhysicianProfileUpdate()">
                                    <div class="formsection">
                                       <div class="panel-group postJobpanelGroup" >
                                          <div class="sectionDivide panel">
                                             <div id="collapse1" class="" >
                                                <section class="form-section {{animationClass}}">
                                                   <div class="form-section-block">
                                                      <div class="">
                                                         <div class="col-md-12">
                                                            <div class="form-group row">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="required label title" >Name Title<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <div class="radio radio-info radio-inline">
                                                                     <input type="radio" name="user_name_title" id="user_name_title" ng-model="PhysicianData.user_name_title" value="Mr."> 
                                                                     <label  for="user_name_title">Mr.</label>
                                                                  </div>
                                                                  <div class="radio radio-info radio-inline">
                                                                     <input type="radio" name="user_name_title" id="user_name_title1" ng-model="PhysicianData.user_name_title" value="Miss">
                                                                     <label for="user_name_title1">Miss</label>
                                                                  </div>
                                                                  <div class="radio radio-info radio-inline">
                                                                     <input type="radio" name="user_name_title" id="user_name_title2"  ng-model="PhysicianData.user_name_title" value="Mrs."> 
                                                                     <label for="user_name_title2">Mrs.</label>
                                                                  </div>
                                                                  <div class="radio radio-info radio-inline">
                                                                     <input type="radio" name="user_name_title" id="user_name_title3" ng-model="PhysicianData.user_name_title" value="Dr." data-bvalidator="required" data-bvalidator-msg="Name Title Required." > 
                                                                     <label for="user_name_title3">Dr.</label>
                                                                  </div>
                                                               </div>
                                                               <label ng-if="Error.user_name_title"> {{ Error.user_name_title}}</label>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label"> First Name<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <input type="text" maxlength="30" name="user_first_name" ng-model="PhysicianData.user_first_name" class="form-control"  data-bvalidator-msg="First Name Required.">
                                                                  <label ng-bind="Error.user_first_name" class="error ng-binding"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Last Name<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <input type="text" maxlength="30" name="user_last_name" ng-model="PhysicianData.user_last_name" class="form-control"  data-bvalidator-msg="Last Name Required.">
                                                                  <label ng-bind="Error.user_last_name" class="error ng-binding"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Login Email<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <input type="text" name="user_email" ng-model="PhysicianData.user_email" class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required." id="user_email" >
                                                                  <label ng-bind="Error.user_email" class="error ng-binding"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="clearfix"></div>
                                                   </div>
                                                </section>
                                                <section class="form-section {{animationClass}}">
                                                   <div class="form-section-block">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view ">
                                                                  <label class="label title" style="">Gender<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <div class="radio radio-info radio-inline">
                                                                     <input type="radio" name="physician_gender" ng-model="PhysicianData.physician_gender" id="physician_gender" value="Male" >
                                                                     <label class="radio-inline" for="physician_gender">Male </label>
                                                                  </div>
                                                                  <div class="radio radio-info radio-inline">
                                                                     <input type="radio" name="physician_gender" id="physician_gender1" ng-model="PhysicianData.physician_gender" value="Female" data-bvalidator="required" data-bvalidator-msg="Gender Required.">
                                                                     <label class="radio-inline" for="physician_gender1">Female</label>
                                                                  </div>
                                                                  <label class="error" ng-bind="Error.physician_gender"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Date of birth<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <div class="row dobBlock">
                                                                     <div class="col-md-4 col-sm-4 col-xs-4 date">
                                                                        <div class="">
                                                                           <select name="physician_dob_date" ng-model="PhysicianData.physician_dob_date" class="form-control" data-bvalidator="required">
                                                                              <option value="">Date</option>
                                                                              <?php
                                                                                 for ($i = 1; $i <= 31; $i++) {
                                                                                     ?>
                                                                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                              <?php } ?>
                                                                           </select>
                                                                        </div>
                                                                        <label class="error" ng-bind="Error.physician_dob_date"></label>
                                                                     </div>
                                                                     <div class="col-md-4 col-sm-4 col-xs-4 month">
                                                                        <div class="">
                                                                           <?php $month_name = month_name(); ?>
                                                                           <select name="physician_dob_month" ng-model="PhysicianData.physician_dob_month" class="form-control" data-bvalidator="required">
                                                                              <option value="">Month</option>
                                                                              <?php
                                                                                 foreach ($month_name as $key => $month_n) {
                                                                                     ?>
                                                                              <option value="<?php echo $key; ?>"><?php echo $month_n; ?></option>
                                                                              <?php } ?>
                                                                           </select>
                                                                        </div>
                                                                        <label class="error" ng-bind="Error.physician_dob_month"></label>
                                                                     </div>
                                                                     <div class="col-md-4 col-sm-4 col-xs-4 year">
                                                                        <div class="">
                                                                           <select name="physician_dob_year" ng-model="PhysicianData.physician_dob_year" class="form-control" data-bvalidator="required">
                                                                              <option value="">Year</option>
                                                                              <?php
                                                                                 for ($j = date('Y') - 85; $j <= date('Y') - 15; $j++) {
                                                                                     ?>
                                                                              <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                                                              <?php } ?>
                                                                           </select>
                                                                        </div>
                                                                        <label class="error" ng-bind="Error.physician_dob_year"></label>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">I am currently a <font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="physician_profession" ng-model="PhysicianData.physician_profession" class="form-control" data-bvalidator="required">
                                                                     <option value="">Select Physician</option>
                                                                     <option ng-repeat="(key,value) in currently_status" ng-if="key" value="{{key}}">{{value}}</option>
                                                                  </select>
                                                                  <label class="error" ng-bind="Error.physician_profession"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Medical School <font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="medical_school" ng-model="PhysicianData.medical_school" class="form-control"  data-bvalidator="required">
                                                                     <option value="">Medical School </option>
                                                                     <option ng-repeat="medical_s in medical_school" ng-if="medical_s.id" value="{{medical_s.id}}">
                                                                        {{medical_s.name}}
                                                                     </option>
                                                                  </select>
                                                                  <label class="error" ng-bind="Error.medical_school"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">I finished my training in / I will finish my training in <font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="traning_year" ng-model="PhysicianData.traning_year" class="form-control" data-bvalidator="required">
                                                                     <option value="">Select Year</option>
                                                                     <?php
                                                                        for ($j = date('Y') - 50; $j <= date('Y') + 5; $j++) {
                                                                            ?>
                                                                     <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                                                     <?php } ?>
                                                                  </select>
                                                                  <label class="error" ng-bind="Error.traning_year"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Medical Specialty<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="medical_specialty" ng-model="PhysicianData.medical_specialty" class="form-control" data-bvalidator="required">
                                                                     <option value="">Select Medical Specialty</option>
                                                                     <option ng-repeat="medical_spe in medical_specialty" ng-if="medical_spe.id" value="{{medical_spe.id}}">{{medical_spe.name}}</option>
                                                                  </select>
                                                                  <label class="error" ng-bind="Error.medical_specialty"></label>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </section>
                                                <!-- <section class="form-section {{animationClass}}">
                                                   <div class="form-section-block">
                                                               <div class="row">
                                                                       <div class="col-md-12">
                                                                        <div class="form-group clearfix">
                                                                          <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                           <label class="label">MINC <font> :</font> </label>
                                                                          </div>
                                                                          <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                               <input type="text" name="physician_minc" ng-model="PhysicianData.physician_minc" class="form-control" data-bvalidator="number,maxlen[10]" value="CAMD" placeholder="CAMD">
                                                                               <font class="charcter-limit">(character limit is 10 numbers)</font>
                                                                               <label class="error" ng-bind="Error.physician_minc"></label>
                                                                               <a href="http://www.minc-nimc.ca/what-is-a-minc/" target="new" class="whats-link"><i class="fa fa-question-circle-o" aria-hidden="true"></i> what is this?</a>
                                                                         </div>
                                                                       </div>
                                                                       </div>
                                                               </div>
                                                   
                                                         </div>
                                                   </section> -->
                                                <section class="form-section {{animationClass}}">
                                                   <div class="form-section-block">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label"> Country<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="physician_country" id="country" ng-model="PhysicianData.physician_country" class="form-control">
                                                                     <option value="Canada" ng-selected="selected">Canada</option>
                                                                  </select>
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label"> Province<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="physician_state" ng-model="PhysicianData.physician_state" class="form-control"  >
                                                                     <option value="">Select Province</option>
                                                                     <option ng-repeat="canada_pro in canada_province" ng-if="canada_pro.id" value="{{canada_pro.id}}">{{canada_pro.province_name}}</option>
                                                                  </select>
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label"> City<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <input type="text" name="physician_city" ng-model="PhysicianData.physician_city" class="form-control">
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label"> Address<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <textarea name="physician_address" ng-model="PhysicianData.physician_address" class="form-control" ></textarea>
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Postal Code<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <input type="text" name="physician_postal_code" ng-model="PhysicianData.physician_postal_code" class="form-control" data-bvalidator="minlen[5],maxlen[7]">
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Phone Number<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <input type="text" name="physician_phone_number" ng-model="PhysicianData.physician_phone_number" class="form-control" mask='999-999-9999' data-bvalidator="required,maxlen[12]">
                                                                  <!--    <span class="optional-tag">(optional)</span> -->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </section>
                                                <section class="form-section {{animationClass}}">
                                                   <div class="form-section-block">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Default Search Specialty<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="medical_selectSpecialty" ng-model="PhysicianData.medical_selectSpecialty" class="form-control" >
                                                                     <option value="">Select Medical Specialty</option>
                                                                     <option ng-repeat="medical_spe in medical_specialty" ng-if="medical_spe.id" value="{{medical_spe.id}}">{{medical_spe.name}}</option>
                                                                  </select>
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label"> Default Search Province<font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <select name="physician_selectState" ng-model="PhysicianData.physician_selectState" class="form-control"  >
                                                                     <option value="">Select Province</option>
                                                                     <option ng-repeat="canada_pro in canada_province" ng-if="canada_pro.province_name" value="{{canada_pro.province_name}}">{{canada_pro.province_name}}</option>
                                                                  </select>
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <div class="form-group clearfix">
                                                               <div class="col-md-4 col-sm-4 label-block label-block-profile-view">
                                                                  <label class="label">Default Message <font> :</font> </label>
                                                               </div>
                                                               <div class="col-md-6 col-sm-8 signup-full-column view-profile-page">
                                                                  <textarea name="physician_default_msg" ng-model="PhysicianData.physician_default_msg" class="form-control" rows="7" placeholder=""></textarea>
                                                                  <span class="optional-tag">(optional)</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clearfix">
                                                      <br>
                                                      <div class="col-md-4 col-sm-4 term-col4"></div>
                                                      <div class="col-md-8 col-sm-8 signup-full-column view-profile-page center-btn">
                                                         <button type="submit" name="" value="Update Profile" class="btn-blue">
                                                         Update profile
                                                         </button>
                                                      </div>
                                                   </div>
                                                   <br>
                                                </section>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--Right Section End-->
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   $this->load->view('include/common_modal_msg');
   $this->load->view('include/footer_menu');
   ?>