<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <!--Left Section-->
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/PhysicianLeftNavigation'); ?>
		   </div>
		    <div class="col-md-9 col-sm-9">
		   	<div class="dashboard-right">
             <div  data-target="#common_modal_msg" data-toggle="modal"></div>
             <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/saved-search.svg" class="">Saved Searches</h3>

            <div class="myjoblisting-sort">
            </div>
		   	<div class="jobList-block">

        	<div class="jobListTile-block">
            <div class="table-responsive">
        	<table class="table table-hover jobListTable table-striped table-bordered savedSearch" ng-if="saved_search!=0">
        	<tr>
              <th class="text-center hidden-md hidden-lg hidden-sm"><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" width="18"> Action</th>
              <th >S.No.</th>
              <th ><img src="<?php echo FRONTEND_THEME_URL ?>images/medal-new.svg" width="18"> Medical speciality</th>
              <th >Province</th>
         		  <th >City/Postal code</th>
              <th ><img src="<?php echo FRONTEND_THEME_URL ?>images/position.svg" width="18"> Position</th>
              <th class="text-center hidden-xs"><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" width="18"> Action</th>
                
        	</tr>
        	
        	<tr ng-repeat="saved_search in ItemsByPage[currentPage] | orderBy:columnToOrder:reverse" ng-if="saved_search" >
            <td class="hidden-md hidden-lg hidden-sm"> 
              <div class="action-panel" >
               <a  ng-controller="GetSearch"  ng-click="get_id_save_search('',saved_search.search_id)" class="btn btn-sm" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Search Again"><i class="fa fa-search" aria-hidden="true"></i></a>
               <a  ng-click="delete_savesearch(saved_search.search_id,$index,currentPage)" class="btn btn-delete btn-sm" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove job">
                <i class="fa fa-trash"></i>
               </a>
              </div>
            </td>          
        		<td>{{pageSize *((currentPage + 1) -1)+$index+1}}.</td>
                <td> <img ng-if="saved_search.medical_spec_logo" src="{{saved_search.medical_spec_logo}}" > {{saved_search.medical_specialty}}</td>
        		<td >{{saved_search.provice}}</td>
        		<td>{{saved_search.city_postal_code}}</td>
        		<td>{{saved_search.position}}</td>
        		<td class="hidden-xs"> 
        			<div class="action-panel" >
               <a  ng-controller="GetSearch"  ng-click="get_id_save_search('',saved_search.search_id)" class="btn btn-sm" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Search Again"><i class="fa fa-search" aria-hidden="true"></i></a>
        			 <a  ng-click="delete_savesearch(saved_search.search_id,$index,currentPage)" class="btn btn-delete btn-sm" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove job">
                <i class="fa fa-trash"></i>
               </a>
        			</div>
        		</td>
            </tr>
            
        	</tr>


			</table>
      <table class="table table-hover jobListTable table-striped"  ng-if="saved_search==0">
          <tr> 
          <td  class="text-center nofound-block-td"> 
            <div class="nofound-block text-center">
             <div class="info-icon">
              <span>
              <img src="<?php echo FRONTEND_THEME_URL ?>images/saved-search.svg" width="40">
              </span>
             </div>
             <h4 class="text-center">
             You do not have any saved searches
             </h4>
             <div class="col-md-8 col-md-offset-2 center-block nofound-desc">
             Click the “save search” button in the
              menu bar on the job search page to add
              to this list

             </div>  
             <div> <a href="jobs/" class="button">View Jobs</a></div>
            </div>
            </td>
            </tr>
          </table> 
            </div>
			</div>
		   </div>

           <nav>
              <ul class="pagination pull-right">
                <li><a href="#"  aria-label="Previous" ng-class="{'active1' : (currentPage == '0')}" ng-click="firstPage()" ng-if="saved_search_count != pageSize && saved_search_count > pageSize">1</a>

                </li>
                <li  ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active1' : (n == currentPage)}" href="#" ng-click="setPage()" ng-if="saved_search_count != pageSize " ng-bind="n+1">1</a>


                </li>
                <li  ><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active1' : (currentPage == last_index)}" ng-if="saved_search_count != pageSize && saved_search_count > pageSize">{{last_index + 1}}</a>

                </li>
             </ul>
            </nav>
            

		   	</div>
		   </div>
		   <!--Right Section End-->
		  </div>
		</div>
		</div>
	</div>
</div>

<script type="text/javascript">

</script>

<?php 
  $this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>

<style>
.active1{
    background:rgb(3,166,237)!important;
    color:white!important;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}
.jobListTable{border-top-color: transparent;}
table.jobListTable tr:first-child {
  background-color: #f9f9f9;
}
.jobListTable tr th:nth-child(2) {width: 60px;}
.jobListTable tr th:nth-child(3) {width: 190px;}
.jobListTable tr th:nth-child(4) {width: 120px;}
.jobListTable tr th:nth-child(5) {width: 135px;}
.jobListTable tr th:nth-child(6) {width: 95px;}
.jobListTable tr th:nth-child(7) {width: 85px;}

@media(max-width: 768px){
 .jobListTable tr th:first-child{width: 95px;}
 .jobListTable tr th:nth-child(2){display: none;}
 .jobListTable tr td:nth-child(2){display: none;}
}
</style>