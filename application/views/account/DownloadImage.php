<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">
		  <div class="row">
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		   </div>
		    <div class="col-md-9 col-sm-9">
		    
		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/medal-new.svg" class="">Verified Jobs</h3>

		    <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
		   	<div class=""> 
		   	<div class="table-responsive table-vertical-scroll">
		   	
		   	<table class="table table-hover jobListTable table-striped table-bordered" ng-if="verified_jobs!=0">
		   	<tr>
                <th class="text-center">S.No.</th>
                <th class="text-center">Job Detail</th>
                <th class="text-center">Job Images</th>
        	</tr>
			<?php $count = 1; ?>
			  <tr ng-repeat="verify_jobs in ItemsByPage[currentPage] | orderBy:columnToOrder:reverse" ng-if="verified_jobs" >
				<td class="text-center">
	                {{pageSize *((currentPage + 1) -1)+$index+1}}.
	            </td>
				<td>
		 		 <!-- <a href="jobs/{{statistics.job_id}}" target="_new">
		 		 <div class="title">{{statistics.listing_title}}</div>

		 		 </a> -->


                 	 <a ng-href="jobs/{{verify_jobs.job_id}}" data-toggle="tooltip" tooltip-placement="top" uib-tooltip=""> <span class="postjob-title-name"><b># </b>{{verify_jobs.job_id}}<span ng-if="verify_jobs.listing_title" >, 
                {{verify_jobs.listing_title}}</span></span>
               
                <div class="">
                <i class="fa fa-map-marker" aria-hidden="true"></i> {{verify_jobs.facility_city}}
                </div>
                <span >
                <img src="<?php echo FRONTEND_THEME_URL ?>/images/medal-new.svg" width="20"> {{verify_jobs.medical_spec_name}}
                </span>
                 </a>


		 		</td>
		 		
		 		<td class="text-center">
		 		<div ng-repeat="image_name in verify_jobs.image_path track by $index" class="text-center downloaded-img img-thumbnail" >
		 		<span ng-click="showImage(image_name,verify_jobs.listing_title)" data-target="#ShowImage" data-toggle="modal" style="cursor:pointer;">
		 		  <img src="assets/uploads/jobs/{{image_name}}" alt="" class="img-responsive" width="120" height="65">
		 		</span>
		 		</div>
		 		</td>
	          </tr>
	            
			 </table>
       <table class="table table-hover jobListTable table-striped"  ng-if="verified_jobs==0">
          <tr> 
          <td  class="text-center nofound-block-td"> 
           <div class="nofound-block">
              <div class="info-icon">
                  <span>
                   <img src="<?php echo FRONTEND_THEME_URL ?>images/doctor-user.svg" width="50">
                  </span>
              </div>
               <h4 class="text-center">No Verified Jobs Added</h4>
               <div class="col-md-8 col-md-offset-2 center-block nofound-desc">Currently there is no any verified job added in your list,click below to Post new job</div>
                 <div><a href="copy" class="button">Click to add job</a></div>
            </div>
            </td>
            </tr>
          </table>  
		   	</div>

			</div>
			
            <nav class="download-image-nav">
              <ul class="pagination pull-right">
                <li><a href="#"  aria-label="Previous" ng-class="{'active_pagination' : (currentPage == '0')}" ng-click="firstPage()" ng-if="verified_jobs_count != pageSize && verified_jobs_count > pageSize">1</a>
                </li>
                <li ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active_pagination' : (n == currentPage)}" href="#" ng-click="setPage()" ng-if="verified_jobs_count != pageSize " ng-bind="n+1">1</a>
                </li>
                <li><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active_pagination' : (currentPage == last_index)}" ng-if="verified_jobs_count != pageSize && verified_jobs_count > pageSize">{{last_index + 1}}</a>
                </li>
             </ul>
            </nav>

		   	</div>
		   </div>
		  </div>
		</div>
		</div>
	</div>
</div>

<div class="modal fade custom-modal" id="ShowImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="min-height:30px!important;">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
     
      <div class="modal-body" >
       <div class="themeform">
        <section class="">
          <div class="info-modal-block">
          <h3 class="heading text-center">{{title}}</h3>
          <div class="clearfix"></div>
          <div>
            <img src="assets/uploads/jobs/{{image_name_title}}" alt="" class="img-responsive center-block">
          <br><br>
          <!--  <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">Close</button> -->
        
           <a ng-href="cart/image_download/{{image_name_title}}" target="_blank" class="button pull-right"> 
          Download Image
          </a>
          
           </div>
         <div class="clearfix"></div>
         </div>
        </section>
       </div>
      </div>
    </div>
  </div>
</div>

  

 




<?php $this->load->view('account/commonmsgdetail'); 
  $this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>

<style>
.active_pagination{
    background:rgb(3,166,237)!important;
    color:white!important;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}
.jobListTable{border-top-color: transparent;margin-bottom: 0px;}
table.jobListTable tr:first-child {
  background-color: #f9f9f9;
}
table.jobListTable tr td:first-child{
  line-height: 18px;
  font-size: 14px;
}

table.jobListTable tr th:nth-child(2){width: 200px;text-align: left;}
table.jobListTable tr th:nth-child(3){text-align: left;width: 480px;}
.jobListTable tr th:nth-child(4) {width: 110px;}

table.jobListTable tr td:nth-child(5){padding-top: 5px;text-align: center;}
.jobList-block table tr td:last-child{text-align: center;}
.jobListTable tr th:first-child{border-left-color: transparent;}
.jobListTable tr th:last-child{border-right-color: transparent;width: 275px;text-align: center;}
table.jobListTable tr td{vertical-align: middle;}
table.jobListTable tr td a .addressname{margin-left: 15px;font-size: 13px;}
.speciality{color: rgba(60, 64, 70, 0.71);font-size: 13px;}
table.jobListTable tr td .title{font-size: 15px;display: block;}
table.jobListTable tr td .title:first-letter{text-transform: capitalize !important;}
#chart-1-license{display:none;}
#chart-2-license{display:none;}
.modal-body{overflow: auto;}
.graph-block{min-width: 991px;}
</style>
