<style>
@import 'https://fonts.googleapis.com/css?family=Open+Sans';
</style>
<div class="">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <div class="" style="width:70%;margin:0 auto;float:none;font-family: 'Open Sans', sans-serif;">

		   	<div class="dashboard-right">
        <div class="header" style="width:30%;float:left;">
          <img src="<?php echo FRONTEND_THEME_URL ?>images/YoloMD_logo.png" class="img-responsive"  width="100">
        </div>
        <div style="width:70%;float:left;font-size:11px;">
         <div>
          <div style="width:30%;float:right;text-align: right;"></div>
          <div style="width:70%;float:right;"> &nbsp;&nbsp;+1 800-000-0000</div>
         </div>
         <div>
          <div style="width:30%;float:right;text-align: right;"></div>
          <div style="width:70%;float:right;"> &nbsp;&nbsp;4060 Saint-Catherine Street West, &nbsp;&nbsp;Westmount, QC, Canada, H3Z 2Z3
          </div>
         </div>
        </div>

          <div class="table-responsive reciept-download">
            <div class="header" style="text-transform: uppercase;vertical-align: middle;margin-bottom:10px;margin-top:20px;"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.png" class="" width="20"> 
            <?php if(!empty($jobs_detail['job_title'])){?>
            <span><?php echo $jobs_detail['job_title']; ?></span>
            <?php } ?>
            </div>
        	<table class="" width="100%" style="border:1px solid #ccc;font-family: 'Open Sans', sans-serif;font-size:12px;" border="0" cellpadding="0" cellspacing="0">

        	<?php if(!empty($jobs_detail['transaction_id'])){?>
        	<tr >
            <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">Transaction Id :</td>
        	  <td class="" style="padding:5px;">  
            <?php echo $jobs_detail['transaction_id']; ?>
          </td>
        	</tr>
           <?php } ?>
           <?php if(!empty($jobs_detail['date'])){?>
          <tr style="border-bottom:1px solid #ececec;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">Pay Date :</td>
          <td class="" style="padding:5px;">  
               <?php echo $jobs_detail['date']; ?>
          </td>
          </tr>
          <?php } ?>
<!--           <tr ng-if="JobDetail.job_title">
          <td class="text-right">Job title</td>
          <td class="">  
             {{JobDetail.job_title}}
          </td>
          </tr> -->
          <?php if(!empty($jobs_detail['job_id'])){?>
          <tr style="border-bottom:2px solid #ddd;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">Posted Job Id :</td>
          <td class="" style="padding:5px;">
               <?php echo '#'.$jobs_detail['job_id']; ?>
          </td>
          </tr>
          <?php } ?>
          <?php if(!empty($jobs_detail['verify'])){?>
          <tr style="border-bottom:2px solid #ddd;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">YoloMD Verify :</td>
          <td class="" style="padding:5px;">  
             <?php echo $jobs_detail['verify']; ?>
          </td>
          </tr>
          <?php } ?>
          <?php if(!empty($jobs_detail['item'])){?>
          <tr style="border-bottom:1px solid #ececec;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">Services :</td>
          <td class="" style="padding:5px;">  
              <?php echo $jobs_detail['item']; ?>
          </td>
          </tr>
          <?php } ?>
          <?php if(!empty($city_detail)){
                foreach($city_detail as $city_d)
                {
          ?>
                  <tr >
                      <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">
                       
                         <?php if(!empty($city_d['city_name'])) echo $city_d['city_name']; ?> :
                      </td>
                      <td class="" style="padding:5px;">  
                        +
                         <?php if(!empty($city_d['city_charge'])) echo '$'.$city_d['city_charge']; ?> 
                          <?php 
                        if(!empty($city_d['valid_date']) && $city_d['valid_date'] != '0000-00-00') 
                        {  
                         
                       
                          if(strtotime($city_d['valid_date']) == strtotime(date('Y-m-d')))
                          {
                          ?>
                            <span >( Active Untill : <?php echo date('d M y', strtotime($city_d['valid_date']));

                         ?>)</span>
                            
                          <?php 
                          }elseif(strtotime($city_d['valid_date']) < strtotime(date('Y-m-d')))
                          {
                            ?>
                          <span > (Expired)</span>

                          <?php 
                          }else
                          {
                            ?>
                          <span >( Active Untill : <?php echo date('d M y', strtotime($city_d['valid_date']));

                         ?>)</span>
                        <?php } 
                       }else{
                        ?>
                       <span >(NA)</span>
                      <?php } ?>
                      </td>
                  </tr>
          <?php } }?>
          <?php if(!empty($jobs_detail['yolomd_verify'])){?>
          <tr style="border-bottom:1px solid #ececec;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">Verify Charged :</td>
          <td class="" style="padding:5px;">  
              +
               <?php echo $jobs_detail['yolomd_verify']; ?>
          </td>
          </tr>
          <?php } ?>
          <?php if(!empty($jobs_detail['payment_amount'])){?>
          <tr style="border-bottom:1px solid #ececec;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#536c71;">Payment Amount :</td>
          <td class="" style="padding:5px;"> 
               <?php echo $jobs_detail['payment_amount']; ?>
          </td>
          </tr>
          <?php } ?>
         
          <?php if(!empty($jobs_detail['paid_amount'])){?>
         <tr class="paytotalamount" style="background-color:#72797b;">
          <td class="text-right" width="30%" align="right" style="padding:5px;color:#fff;font-size:20px;">Paid Amount :</td>
          <td class="" style="padding:5px;color:#fff;font-size:20px;"> 
               <?php echo $jobs_detail['paid_amount']; ?>
          </td>
          </tr>
			     <?php } ?>
          </table>
          </div>


			</div>
		   </div>
		    	</div>
		    </div>
		  </div>
		</div>
		</div>
	</div>
</div>
<style type="text/css">
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}

</style>
