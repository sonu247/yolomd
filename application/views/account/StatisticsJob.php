<?php 
  $this->load->view('include/header_menu');
?>
<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">
		  <div class="row">
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		   </div>
		    <div class="col-md-9 col-sm-9">
		    
		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/line-chart.svg" class="">Job Statistics </h3>

		   <!-- 	<div zingchart id="chart-1" zc-json="myJson" zc-width="300" zc-height="500px"></div> -->
		   <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
		   	<div class="">
		   	<div class="table-responsive">
		   	
		   	<table class="table table-hover jobListTable table-striped table-bordered"  ng-if="statistics_jobs!=0">
		   	<tr>
                <th class="text-center">S.No.</th>
                <th class="text-center">Job Title</th>
                <th class="text-center">
                <img src="<?php echo FRONTEND_THEME_URL ?>/images/medal-new.svg" width="20"> Specility</th>
                <th class="text-center">View Graph</th>
        	</tr>
			<?php $count = 1; ?>
			  <tr ng-repeat="statistics in ItemsByPage[currentPage] | orderBy:columnToOrder:reverse" ng-if="statistics" >
			 
			  

				 <td class="text-center">
	                {{pageSize *((currentPage + 1) -1)+$index+1}}.
	            </td>
				<td>
		 		 <!-- <a href="jobs/{{statistics.job_id}}" target="_new">
		 		 <div class="title">{{statistics.listing_title}}</div>

		 		 </a> -->

		 		 <a ng-href="jobs/{{statistics.job_id}}" data-toggle="tooltip" tooltip-placement="top" uib-tooltip=""> <span >
                {{statistics.listing_title}}</span>
               
                <div class="addressname" style="margin-left:0px;">
                <span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Location"> <i class="fa fa-map-marker" aria-hidden="true"></i> {{statistics.facility_city}}</span>
                </div>
                 </a>


		 		</td>
		 		<td>
		 		 <div class="title">{{statistics.medical_specialty}}</div>
		 		</td>
		 		
		 		
		 		<td class="view-graph">
		 		
		 		<div  ng-if="statistics.msg_ratio != '0' &&  statistics.job_ratio != '0'" class="progress " ng-click="graph(statistics.msg_ratio,statistics.job_ratio,statistics.statics_ratio,statistics.remaining_ratio,statistics.listing_title)" data-toggle="modal" data-target="#myModal" style="cursor: pointer; height:25px; border-radius:15px;" data-toggle="tooltip" title="Contacted  {{statistics.remaining_ratio_float}}%"  >
		 		
					<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{statistics.statics_ratio_float}}" aria-valuemin="0" aria-valuemax="100" style="width: {{statistics.statics_ratio_float}}% ; cursor: pointer; padding-top:7px; " data-toggle="tooltip" title="Conversation rate {{statistics.statics_ratio_float}}%">
						
					</div>
					<div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: {{statistics.remaining_ratio_float}}%; padding-top:7px;">
					   
					  </div>
					
				
				</div>
				
				<span ng-if="statistics.msg_ratio == '0' ||  statistics.job_ratio == '0'" style="color:rgb(2,166,237);">
				No. of doctors contacts and users visited to the job are 0%.
				</span>
		 		</td>
	          </tr>
	         
				
			 </table>
			 <table class="table table-hover jobListTable table-striped"  ng-if="statistics_jobs==0">
          <tr> 
          <td  class="text-center nofound-block-td"> 
           <div class="nofound-block">
              <div class="info-icon"  style="background-color: ">
                  <span>
                   <img src="<?php echo FRONTEND_THEME_URL ?>images/icons/line-chart-white.svg" width="50">
                  </span>
              </div>
             
              <h4 class="text-center">Currently there are no Job Statistics.</h4>
                
            </div>
            </td>
            </tr>
          </table>  
		   	</div>

			</div>
			
            <nav>
              <ul class="pagination pull-right">
                <li><a href="#"  aria-label="Previous" ng-class="{'active_pagination' : (currentPage == '0')}" ng-click="firstPage()" ng-if="statistics_jobs_count != pageSize && statistics_jobs_count > pageSize">1</a>
                </li>
                <li ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active_pagination' : (n == currentPage)}" href="#" ng-click="setPage()" ng-if="statistics_jobs_count != pageSize " ng-bind="n+1">1</a>
                </li>
                <li><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active_pagination' : (currentPage == last_index)}" ng-if="statistics_jobs_count != pageSize && statistics_jobs_count > pageSize">{{last_index + 1}}</a>
                </li>
             </ul>
            </nav>

		   	</div>
		   </div>
		  </div>
		</div>
		</div>
	</div>
</div>




<?php $this->load->view('account/commonmsgdetail'); 
  $this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>

<style>
.active_pagination{
    background:rgb(3,166,237)!important;
    color:white!important;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}
.jobListTable{border-top-color: transparent;}
table.jobListTable tr:first-child {
  background-color: #f9f9f9;
}
table.jobListTable tr td:first-child{
  line-height: 18px;
  font-size: 14px;
}
table.jobListTable tr th:nth-child(2){width: 275px;text-align: left;}
table.jobListTable tr th:nth-child(3){text-align: left;width: 230px;}
.jobListTable tr th:nth-child(4) {width: 110px;}

table.jobListTable tr td:nth-child(5){padding-top: 5px;text-align: center;}
.jobList-block table tr td:last-child{text-align: center;}
.jobListTable tr th:first-child{border-left-color: transparent;}
.jobListTable tr th:last-child{border-right-color: transparent;width: 260px;text-align: center;}
table.jobListTable tr td{vertical-align: middle;}
table.jobListTable tr td a .addressname{margin-left: 15px;font-size: 13px;}
.speciality{color: rgba(60, 64, 70, 0.71);font-size: 13px;}
table.jobListTable tr td .title{font-size: 15px;display: block;}
table.jobListTable tr td .title:first-letter{text-transform: capitalize !important;}
#chart-1-license{display:none;}
#chart-2-license{display:none;}
.modal-body{overflow: auto;}
.graph-block{min-width: 991px;}
#chart-1-menu{display:none;}
#chart-2-menu{display:none;}

</style>
<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal custom-modal fade statistics-jobs-modal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content"  >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="text-center">{{single_title}}</h3>
      </div>
      <div class="modal-body">
      <div class="graph-block">
      <div class="col-md-5 col-xs-12">
       <div zingchart id="chart-1" zc-json="myJson" zc-width="400" zc-height="568px" style="float:left;" ></div>
       </div>
      <div class="col-md-7 col-xs-12">
       <div class="clearfix hidden-md hidden-sm visible-xs"></div>
       <div zingchart id="chart-2" zc-json="myJson1" zc-width="100%" zc-height="568px" ></div> 
       </div>
      <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>





 