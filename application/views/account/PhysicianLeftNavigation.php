<div ng-controller="SidebarMenuDashboard">
	<div class="dashboard-Left-toggle">
		<a href="#" ng-click="dashboardLeftToggle()"><i class="{{dashboardFaClass}}" aria-hidden="true"></i> Dashboard Menu</a>
	</div>
	<div class="dashboard-Left {{ dashboardToggleClass }}">
		<div class="account-logo text-center">
			<span class="image-block"><img src="<?php echo FRONTEND_THEME_URL ?>/images/user-vector.svg" class="img-responsive"></span>
			<div class="user-name text-center">
				<?php
				if($physicainuser_info=get_physicain_info()){
				echo $physicainuser_info->user_first_name.' '.$physicainuser_info->user_last_name ; } ?>
			</div>
		</div>
		<div class="menu">
			<ul>
				<!-- <li><a href="<?php //echo base_url('physician-dashboard');?>"><img src="<?php //echo FRONTEND_THEME_URL ?>/images/icons/my-dashboard.png" class="fa"> My Dashboard</span></a></li> -->
				<li><a ng-class="{'active' : (state_name == 'PhysicianMessage') , '' : (state_name != '')}" href="<?php echo base_url('physician-message');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/messages.svg" class="fa" > My Messages <span  ng-controller="MessageCount" ><span  ng-if="msg_count != '0'" class="badge" ng-bind-html="msg_count" ng-class="{'plus_tag' : (msg_count >  '100')}"></span></span></a></li>
				<li><a ng-class="{'active' : (state_name == 'PhysicianFavorite') , '' : (state_name != '')}" href="<?php echo base_url('physician-favorite');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-favorite.svg" class="fa"> My Favourites</a></li>
				<li><a ng-class="{'active' : (state_name == 'PhysicianSaveSearch') , '' : (state_name != '')}" href="<?php echo base_url('save-search');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/saved-search.svg" class="fa"> Saved Searches</a></li>
				<li><a ng-class="{'active' : (state_name == 'PhysicianProfile') , '' : (state_name != '')}" href="<?php echo base_url('physician-profile');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/profile.svg" class="fa"> Profile</a></li>
				<li><a ng-class="{'active' : (state_name == 'PhysicianChangePassword') , '' : (state_name != '')}" href="<?php echo base_url('physician-change-password');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/change-password.svg" class="fa">Change Password</a></li>
				<li><a ng-class="{'active' : (state_name == 'MedicalMessage') , '' : (state_name != '')}" href="" ng-controller="logoutCtrl" ng-click="logout()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/logout.svg" class="fa">Log Out </a></li>
			</ul>
		</div>
	</div>
</div>