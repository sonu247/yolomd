<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">

	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		   </div>
		    <div class="col-md-9 col-sm-9" >

		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/receipt.svg" class="">Receipts </h3>
		   	<div class="jobList-block">

        	<div class="jobListTile-block">
           <!--  <div class="alert alert-success" ng-if="UpdateMassage" ng-hide="UpdateAlertMessage">
                <strong>Success!</strong>  {{UpdateMassage}}.
            </div> -->
             <div  data-target="#common_modal_msg" data-toggle="modal"></div>
            <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
              <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
          </div>
          <div class="table-responsive">
        	<table class="table table-hover jobListTable table-striped table-bordered" ng-if="ReceiptsInfo!=0">
        	<tr>
              <th class="text-center" style="width:50px;">S.No.</th>
              <th class="text-center" >Services</th>
              <th class="text-center" style="width:210px;">Job Title</th>
              <th class="text-center" style="width:105px;">Pay Amount</th>
              <th class="text-center" style="width:110px;"><img src="<?php echo FRONTEND_THEME_URL ?>images/calendar.svg" width="18"> Pay Date</th>
              <th class="text-center" style="width:110px;"><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" width="18"> Action</th>
        	</tr>
        	
        	<tr ng-if="ReceiptsInfo" ng-repeat="receipts_info in ItemsByPage[currentPage]"  >
          <td class="text-center">{{pageSize *((currentPage + 1) -1)+$index+1}}.</td>
        	<td>
              {{receipts_info.item|plusless|capitalize}}
          </td>
          <td>
              <a href="{{site_url}}post-job-update/{{encodebase64(receipts_info.job_id)}}">{{receipts_info.listing_title}}</a>
          </td>
  	 		 
          <td class="text-center">
            
             ${{addition(receipts_info.payment_amount,receipts_info.discount)}}
          </td>
        	<td class="text-center">
             <div class="date">
                <div><span><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> {{formatDate(receipts_info.date_time)| date:'d MMM y'}}</span>
                </div>
             </div>
          </td>
         
          <td class="text-center">
           <?php 
            // $pay_id = '';
            // $id = "{{receipts_info.payment_id}}";
            // $pay_id = base64_encode($id);
          ?>
           <a  data-ng-href="{{site_url}}receipts-download/{{receipts_info.unique_id}}" class="btn btn-theme btn-sm">Receipt</a>
          </td>
        	</tr>
         
			   </table>
          <table class="table table-hover jobListTable table-striped"  ng-if="ReceiptsInfo==0">
          <tr> 
          <td  class="text-center nofound-block-td"> 
           <div class="nofound-block">
               <div class="info-icon">
                <span>
                 
                   <img src="<?php echo FRONTEND_THEME_URL ?>images/receipt-white.svg" width="40">
                 
                </span>
               </div>
              <h4>You do not have any saved receipts</h4>
               <div class="col-md-8 col-md-offset-2 center-block nofound-desc">Click “upgrade” on your My Listings page to purchase Priority Access or become a YoloMD Verified Facility
               </div>              
            </div>
            </td>
            </tr>
          </table>  
            </div>
			</div>
		   </div>
            <nav>
              <ul class="pagination pull-right">
                <li><a href="#"  aria-label="Previous" ng-class="{'active1' : (currentPage == '0')}" ng-click="firstPage()" ng-if="receipts_count != pageSize && receipts_count > pageSize">1</a>
                </li>
                <li  ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active1' : (n == currentPage)}" href="#" ng-click="setPage()" ng-bind="n+1">1</a>
                </li>
                <li  ><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active1' : (currentPage == last_index)}"  ng-if="receipts_count != pageSize && receipts_count > pageSize">{{last_index + 1}}</a>
                </li>
             </ul>
            </nav>
		    	</div>
		    </div>
		  </div>
		</div>
		</div>
	</div>
</div>
<style type="text/css">
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}
.jobListTable{border-top-color: transparent;}
table.jobListTable tr:first-child {
  background-color: #f9f9f9;
}
table.jobListTable tr td:first-child{
  line-height: 18px;
  font-size: 14px;
}
table.jobListTable tr th:nth-child(2){width: 215px;text-align: left;}
table.jobListTable tr th:nth-child(3){text-align: left;}
table.jobListTable tr th img{position: relative;top: -2px;}
table.jobListTable tr td:nth-child(5){padding-top: 5px;}
table.jobListTable tr td:nth-child(6){vertical-align: top;}
.jobList-block table tr td:last-child{text-align: center;}
.jobListTable tr th:first-child{border-left-color: transparent;}
.jobListTable tr th:last-child{border-right-color: transparent;}
table.jobListTable tr th{}
</style>
<?php 
  $this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>