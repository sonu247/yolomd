<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <!--Left Section-->
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		   </div>
		    <!--Left Section End-->
		    <!--Right Section-->
		    
		    <div class="col-md-9 col-sm-9">

		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/change-password.svg" class="">Change Password</h3>

		   	<div class="message-center">
		   	<div class="row">
		   	<div class="col-md-12">
		   	<h4 class="extraLines text-center">Use the form below to change your password</h4>
		   	</div>
		   	<div class="clearfix"></div>
		   	<br><br>
		   	<div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
			   	<div class="themeform">
					<div  data-target="#common_modal_msg" data-toggle="modal"></div>
					<form method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="SubmitChangePassword()" name="change_password_form">
					 <div class="form-section">
					 <div class="col-md-12">
					  <div class="form-group row">
			           <div class="col-md-4 col-sm-4 col-xs-11 label-block">
			            <label for="exampleInputPassword1" class="label required">Old Password <font>:</font></label>
			           </div>
			           <div class="col-md-6 col-sm-7 col-xs-11 signup-full-column">
			           <input type="password" name="old_password" ng-model="ChangePassword.old_password"  class="form-control" autocomplete="off" data-bvalidator="required" id="old_password">
			           <label class="error" ng-if="Error.old_password"> {{ Error.old_password }}</label>
			           </div>
			          </div>
			         </div>
			         <div class="clearfix"></div>
			         <div class="col-md-12">
					 <div class="form-group row">
			           <div class="col-md-4 col-sm-4 col-xs-11 label-block">
			            <label for="exampleInputPassword1" class="label required">New Password <font>:</font></label>
			           </div>
			           <div class="col-md-6 col-sm-7 col-xs-11 signup-full-column">
			           <input type="password" name="password" ng-model="ChangePassword.password"  class="form-control" autocomplete="off" data-bvalidator="required,minlen[6]" id="password" data-bvalidator-msg="Password should be at least six characters.">
			           	<span class="help-tip-btn">
                            <a href="#" class="btn btn-sm btn-circle btn-theme ">
                               <i class="fa fa-question" aria-hidden="true"></i>
                            </a>
                            <div class="tooltip top password-tooltip-btn" role="tooltip">
                              <div class="tooltip-arrow"></div>
                              <div class="tooltip-inner">
                               Password requires six characters minimum
                              </div>
                            </div>
                        </span>
			           <label class="error" ng-if="Error.password"> {{ Error.password }}</label>
			           </div>
			          </div>
			         </div>
			         <div class="clearfix"></div>
			         <div class="col-md-12">
			         <div class="form-group row">
			           <div class="col-md-4 col-sm-4 col-xs-11 label-block">
			            <label for="exampleInputPassword1"  class="label required">Confirm Password <font>:</font></label>
			           </div>
			           <div class="col-md-6 col-sm-7 col-xs-11 signup-full-column">
			            <input type="password" name="confirm_password" ng-model="ChangePassword.confirm_password" class="form-control"   data-bvalidator="required,equal[password],minlen[6]" data-bvalidator-msg="Please enter the same password again">
			             <label class="error" ng-if="Error.confirm_password"> {{ Error.confirm_password }}</label>
			           </div>
			          </div>
			         </div>
			         <div class="col-md-12">
			         <div class="row">
			         <div class="col-md-4 col-sm-4"></div>
			         <div class="col-md-8 col-sm-8">
			           <button type="submit" class="btn-blue">Update Password</button>
			           
			          </div>
			         </div>
			         </div>
			         </div>
			        </form>
	        
	        	</div>
        	</div>

		</div>

		   	</div>
		   </div>
		   <!--Right Section End-->
		  </div>
		</div>
		</div>
	</div>
</div>
<?php 
  $this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>