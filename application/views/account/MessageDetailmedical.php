<?php 
  $this->load->view('include/header_menu');
?>
<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">
		  <div class="row">
		    <div class="col-md-3">
		    <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		   </div>
		    <div class="col-md-9">
		     <div id="search_loader" class="search_loader_small" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.9);position: absolute;z-index: 100;left: 0;">
			    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
			</div>
			<?php $this->load->view('account/commonmsgdetail'); ?>
		   
		   </div>
		  </div>
		</div>
		</div>
	</div>
</div>

<?php 
  $this->load->view('include/footer_menu');
?>
