<?php 
  $this->load->view('include/header_menu');
?>
<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <!--Left Section-->
			<div class="col-md-3 col-sm-3 dashboard-Left-warp">			
		     <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		    </div>
		    <!--Left Section End-->
		    <!--Right Section-->
		    <div class="col-md-9 col-sm-9">

		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-dashboard.svg" class="">Dashboard</h3>
		   	<div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
		   	<div class="message-center">
			  <div class="dashboard-list">
			  	<ul>
			  	
			  	  <li><a href="<?php echo base_url('medical-message');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/messages.svg"></a>
			  	  <div>Messages</div>
			  	  </li>			  	 
			  	  <li><a  href="<?php echo base_url('job-listing');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/list.svg"></a>
			  	  <div>My Job Listings</div>
			  	  </li>			  	   		  	
			  	  <!-- <li><a  href="<?php //echo base_url('statistics-jobs');?>"><img src="<?php //echo FRONTEND_THEME_URL ?>/images/icons/large/graph.svg"></a>
			  	  <div>Job Statistics</div>
			  	  </li> -->
			  	 
			  	 <!--  <li><a  href="<?php// echo base_url('receipts');?>"><img src="<?php// echo FRONTEND_THEME_URL ?>/images/icons/large/receipts.svg"></a>
			  	  <div>Receipts</div>
			  	  </li>	 -->
			  	  <li><a  href="<?php echo base_url('medical-profile');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/my-profile.svg"></a>
			  	  <div>My Profile</div>
			  	  </li>		
			  	  <li><a href="<?php echo base_url('medical-change-password');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/change-password.svg"></a>
			  	  <div>Change Password</div>
			  	  </li>		
			  	   <li><a href="" ng-controller="logoutCtrl" ng-click="logout()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/logout.svg"></a>
			  	  <div>Log Out</div>
			  	  </li>		  	  	  	 		  	  
			  	  
			  	</ul>
			  </div>
		   	</div>

		   	</div>
		   </div>
		   <!--Right Section End-->
		  </div>
		</div>
		</div>
	</div>
</div>
<?php 
  $this->load->view('include/footer_menu');
?>