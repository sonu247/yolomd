<?php
$this->load->view('include/header_menu');
?>
<div class="min-content-block ng-cloak" ng-controller="PostJob">
	<!-- <div class="formPage-section formPage-section-media-view"> -->
	<div class="themeform" >
		<div class="col-md-7 col-sm-7 full-height left-side post-job-warp" style="{{style_container}}" >
			<!-- <h1>Post Job</h1> -->
			<form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="nextStep()" class="site-form">
				<div ng-switch on="postJob" ng-class="{'formsection':(postJob!='13')}"  class="form-section-med ia-view-table ">
					<div id="search_loader_step" class="search_loader" style="width: 100%; height: 100%; display: block; background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-align:center; ">
						<img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif" style="margin-top:0px; float:left; margin-left:15px;">
					</div>
					<section ng-switch-when="1" class="form-section form-sectio n-media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block contact-info-block">
							<h3 class="heading text-center">Contact Information</h3>
							<!-- 	<p class="text-center info-text">This information will not be shown in public.
							</p> -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4 ">
											<label class="required label title" >Title<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-8 col-xs-7 signup-full-column">
											<div class="radio radio-info radio-inline">
												<input type="radio" name="contact_name_title" id="contact_name_title" ng-model="PostJobData.contact_name_title" value="Mr.">
												<label  for="contact_name_title">Mr.</label>
											</div>
											<div class="radio radio-info radio-inline">
												<input type="radio" name="contact_name_title" id="contact_name_title1" ng-model="PostJobData.contact_name_title" value="Miss">
												<label for="contact_name_title1">Miss</label>
											</div>
											<!-- 					<div class="radio radio-info radio-inline">
													<input type="radio" name="contact_name_title" id="contact_name_title2"  ng-model="PostJobData.contact_name_title" value="Mrs.">
													<label for="contact_name_title2">Mrs.</label>
											</div> -->
											<div class="radio radio-info radio-inline">
												<input type="radio" name="contact_name_title" id="contact_name_title3" ng-model="PostJobData.contact_name_title" value="Dr." data-bvalidator="required" data-bvalidator-msg="Name Title Required.">
												<label for="contact_name_title3">Dr.</label>
											</div>
											<label class="error" ng-if="Error.contact_name_title"> {{ Error.contact_name_title }}</label>
										</div>
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">First Name<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 col-xs-8 signup-full-column">
											<input type ="text" maxlength="30" name="contact_first_name"  ng-model="PostJobData.contact_first_name"  class="form-control" data-bvalidator="required" data-bvalidator-msg="First Name Required" value="">
											<label class="error" ng-bind="Error.contact_first_name"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Last Name<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<input type="text" maxlength="30" name="contact_last_name" ng-model="PostJobData.contact_last_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Last Name Required">
											<label class="error" ng-bind="Error.contact_last_name"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Email<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<input type="text" name="contact_email_address"  ng-model="PostJobData.contact_email_address"  class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required" id="contact_person_email_address">
											<label class="error" ng-bind="Error.contact_email_address"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Phone Number<font> :</font></label>
										</div>
										<div class="col-md-3 col-sm-4 col-xs-4 signup-full-column">
											<input type="text" only-num-special name="contact_phone_number" ng-model="PostJobData.contact_phone_number" placeholder="(123) 456-7890" class="form-control" mask='999-999-9999' data-bvalidator="required,minlen[8],maxlen[12]" data-bvalidator-msg="Valid number is required">
											<label class="error" ng-bind="Error.contact_phone_number"></label>
										</div>
										<div class="col-md-1 col-sm-1 label-block col-xs-1 no-padding ext-label remove-nopadding-left">
											<label class="label">Ext.<font> :</font> </label>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-2 signup-full-column ext-block">
											<input type="text"  name="contect_ext_number" ng-model="PostJobData.contect_ext_number" class="form-control"  data-bvalidator="digit,maxlen[5]">
											<span class="optional-tag">(optional)</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Fax<font> :</font> </label>
										</div>
										<div class="col-md-3 col-sm-4 col-xs-7 signup-full-column">
											<input type="text"  mask='999-999-9999' name="contact_fax"  placeholder="(123) 456-7890" ng-model="PostJobData.contact_fax"  class="form-control"  data-bvalidator="minlen[8],maxlen[12]" data-bvalidator-msg="Valid Fax Required" >
											<span class="optional-tag">(optional)</span>
											<label class="error" ng-bind="Error.contact_fax"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-7 col-sm-7 col-xs-7 left-select-listing">
								<div class="stepToggle-btn ">
									<?php
										$count_job = '';
										$id = '';
										$id = medicaluser_id();
										$count_job = all_row_count('post_jobs',array('user_id'=> $id));
										if(!empty($count_job) &&  $count_job > 0)
										{
									?>
									
									<span class="">
										<a href="copy"  class="button clone-exist">
											<i class="fa fa-angle-left" aria-hidden="true"></i> Select From Existing Job
										</a>
									</span>
									<?php } ?>
								</div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-4 right-goto-next">
								<!-- 	  <div class="pull-left">
											<a href="#" class="btn btn-preview">Preview</a>
								</div> -->
								<!-- 		  <span class="pull-left preview" ng-if="PreviewShow == '1'">
										<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
								</span> -->
								<div class="stepToggle-btn right">
									<button type="submit" class="btn  down">
									Next <i class="fa fa-angle-right" aria-hidden="true"></i>
									</button>
								</div>
								
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<section ng-switch-when="2" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block">
							<h3 class="heading text-center">I am SEEKING</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Medical Specialties<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<select name="medical_specialty_name" ng-model="PostJobData.medical_specialty_name" class="form-control" ng-if="medical_specialty" data-bvalidator="required" data-bvalidator-msg="Medical Specialities Required">
												<option value="">Select Medical Specialties</option>
												<option ng-repeat="medical_s in medical_specialty" ng-if="medical_s.id" value="{{medical_s.id}}">{{medical_s.name}}</option>
											</select>
											<label class="error" ng-bind="Error.medical_specialty_name"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4 label-block">
								<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
							</div>
							<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
								<!-- 			<span class="pull-left preview" ng-if="PreviewShow == '1'">
									<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
								</span> -->
								<div class="stepToggle-btn">
									
									<span class="">
										<button class="btn  up" ng-click="previousStep()">
										<i class="fa fa-angle-left" aria-hidden="true"></i> Back
										</button>
									</span>
									<button type="submit" class="btn  down">
									Next <i class="fa fa-angle-right" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<section ng-switch-when="3" class="form-section form-section -media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block">
							<h3 class="heading text-center">Special Qualifications </h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Special Qualifications<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<textarea  maxlength="300" ng-trim="false" name="special_qualification" ng-model="PostJobData.special_qualification" class="form-control" data-bvalidator="required" rows="7" placeholder="Example: ATLS certified, Subspecialty in Interventional Cardiology, Licenced under the College of Physicians and Surgeons of Ontario etc.">
											</textarea>
											<span>{{300 - PostJobData.special_qualification.length}} remaining</span>
											<label class="error" ng-bind="Error.special_qualification"></label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-4 label-block col-xs-4">
								<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
							</div>
							<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
								<!--     			<span class="pull-left preview" ng-if="PreviewShow == '1'">
										<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
								</span> -->
								<div class="stepToggle-btn pull-right">
									<span class>
										<button class="btn up" ng-click="previousStep()">
										<i class="fa fa-angle-left" aria-hidden="true"></i> Back
										</button>
									</span>
									<button type="submit" class="btn  down">
									Next <i class="fa fa-angle-right" aria-hidden="true"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<section ng-switch-when="4" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block">
							<h3 class="heading text-center">Job Description</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Hours<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<select name="hour" ng-model="PostJobData.hour" data-bvalidator="required" class="form-control"  ng-if="hours" data-bvalidator-msg="Required">
												<option value="">Select Hours</option>
												<option ng-repeat ="(key,value) in hours" ng-if="key" value="{{key}}">{{value}}</option>
											</select>
											<label class="error" ng-bind="Error.hour"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label title">Position Type<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<div class="radio radio-info radio-inline">
												<input type="radio" name="position" ng-model="PostJobData.position" id="permanent" value="Permanent" >
												<label class="radio-inline" for="permanent">Permanent </label>
											</div>
											<div class="radio radio-info radio-inline">
												<input type="radio" name="position" id="locum" ng-model="PostJobData.position" value="Locum" data-bvalidator="required" data-bvalidator-msg="Required.">
												<label class="radio-inline" for="locum">Locum</label>
											</div>
											<label class="error" ng-bind="Error.position"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Start Date<font> :</font></label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<div class="row dobBlock">
												<div class="col-md-4 col-sm-4 col-xs-4 date">
													<div class="">
														<select name="start_month" ng-model="PostJobData.start_month" ng-if="month_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
															<option value="">Month</option>
															<option ng-repeat ="(key,value) in month_name" ng-if="key" value="{{key}}">{{value}}</option>
														</select>
													</div>
													<label class="error" ng-bind="Error.start_month"></label>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-4 month">
													<div class="">
														<select name="start_date" ng-model="PostJobData.start_date" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
															<option value="">Day</option>
															<?php
															for($i=1; $i<=31; $i++)
															{ ?>
															<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
															<?php } ?>
														</select>
													</div>
													<label class="error" ng-bind="Error.start_date"></label>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-4 year">
													<div class="">
														<select name="start_year" ng-model="PostJobData.start_year" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
															<option value="">Year</option>
															<?php
															for($j=date('Y'); $j<=date('Y')+2; $j++)
															{ ?>
															<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
															<?php } ?>
														</select>
													</div>
													<label class="error" ng-bind="Error.start_year"></label>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Compensation<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<select name="compensation" ng-model="PostJobData.compensation" class="form-control"  ng-if="compensation">
												<option value="">Select Compensation</option>
												<option ng-repeat ="(key,value) in compensation" ng-if="key" value="{{key}}">{{value}}</option>
											</select>
											<span class ="optional-tag">(optional)</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="required label title">Call Responsibilities<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<!-- 	<input type="text" name="call_responsibility" ng-model="PostJobData.call_responsibility" class="form-control" data-bvalidator="required" data-bvalidator-msg="Call Responsibilities Required"> -->
											<div class="radio radio-info radio-inline">
												<input type="radio" name="call_responsibility" id="responsibilities_free" value="Yes" ng-model="PostJobData.call_responsibility">
												<label class="radio-inline" for="responsibilities_free" >Yes </label>
											</div>
											
											<div class="radio radio-info radio-inline">
												<input type="radio" name="call_responsibility" id="responsibilities_none" ng-model="PostJobData.call_responsibility" value="No" data-bvalidator="required" data-bvalidator-msg="Required.">
												<label class="radio-inline" for="responsibilities_none" >No</label>
											</div>
											<label class="error" ng-bind="Error.call_responsibility"></label>
											<div class="alert alert-danger error" style="margin-top: 20px;" ng-if="MassageErr" ng-hide="UpdateAlertErrorMessage">
												{{MassageErr}}
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4 label-block">
								<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
							</div>
							<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
								<!-- 			<span class="pull-left preview" ng-if="PreviewShow == '1'">
									<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
								</span> -->
								<div class="stepToggle-btn pull-right">
									<span class="">
										<button class="btn  up"  ng-click="previousStep()">
										<i class="fa fa-angle-left" aria-hidden="true"></i> Back
										</button>
									</span>
									<button type="submit" class="btn  down">
									Next <i class="fa fa-angle-right" aria-hidden="true"></i>
									</button>
								</div>
								<div class="cleafix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<section ng-switch-when="5" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div id="search_loader_small" class="search_loader_small" style="width: 100%;height: 100%;display: none;background-color: rgba(255, 255, 255, 0.1);position: absolute;z-index: 100;left: 0;text-aling:center;">
							<img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
						</div>
						<div class="form-section-block">
							<h3 class="heading text-center">Facility Location</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Facility Name<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<input type="text" name="facility_name"  ng-model="PostJobData.facility_name"  class="form-control" data-bvalidator="required,maxlen[50]"  >
											<label class="error" ng-bind="Error.facility_name"></label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Province<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<select name="facility_state" ng-model="PostJobData.facility_state" class="form-control"  ng-if="canada_province" data-bvalidator="required"  ng-change="get_city(PostJobData.facility_state)">
												<option value="">Select Province</option>
												<option ng-repeat ="canada_pro in canada_province" ng-if="canada_pro.id" value="{{canada_pro.province_name}}">{{canada_pro.province_name}}</option>
											</select>
											<label class="error" ng-bind="Error.facility_state"></label>
											<div class="loaderview" style="display:none;">
												<img src="<?php echo FRONTEND_THEME_URL ?>/images/mini-loader.gif" class="img-responsive">
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">City<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<select ui-select2 name="facility_city" ng-model="PostJobData.facility_city" class="form-control"   data-bvalidator="required"  ng-change="get_zipcode(PostJobData.facility_city);">
												<option value="">Select City</option>
												<option ng-repeat ="city_name in CityProvince" ng-if="city_name.id" value="{{city_name.city_name}}">{{city_name.city_name}}</option>
											</select>
											<label class="error" ng-bind="Error.facility_city"></label>
											
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Street number and name<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<input name ="facility_address" ng-model="PostJobData.facility_address" class="form-control" data-bvalidator="required"  Placeholder="">
											<label class="error" ng-bind="Error.facility_address"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Suite or Office number<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<input name ="facility_suite_office" ng-model="PostJobData.facility_suite_office" class="form-control"   Placeholder="">
											<span class ="optional-tag">(optional)</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Postal Code<font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column ">
											<!-- <select name="facility_postal_code" ng-model="PostJobData.facility_postal_code" class="form-control"   data-bvalidator="required" data-bvalidator-msg="Postal code Required" >
												<option value="">Select Postal code</option>
												<option ng-repeat ="postal_code in PostalCode" ng-if="postal_code.id" value="{{postal_code.postal_code}}">{{postal_code.postal_code}}</option>
											</select> -->
											<tags-input  replace-spaces-with-dashes="false"   style="height:35px!important; text-transform: uppercase!important;" ng-model="zip_code_data" on-tag-added="tagAdded(zip_code_data)" display-property="postal_code">
											<auto-complete source="loadTags(PostJobData.facility_city,$query)" max-results-to-show="1000" select-first-match="false" debounce-delay="0" min-length="1"
											></auto-complete>
											</tags-input>
											<label style="color:gray;">Postal code should be A1B 2C3</label>
											<label class="error" ng-if="UpdatePostalCodeErr">{{ UpdatePostalCodeErr}}</label>
											<label class="error" ng-if="UpdatePostalValidErr">{{ UpdatePostalValidErr}}</label>
											<div class="loaderview" style="display:none;">
												<img src="<?php echo FRONTEND_THEME_URL ?>/images/mini-loader.gif" class="img-responsive">
											</div>
										</div>
										
									</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-4 col-xs-4 label-block">
								<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
							</div>
							<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
								<!-- 			<span class="pull-left preview" ng-if="PreviewShow == '1'">
									<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
								</span> -->
								
								<div class="stepToggle-btn pull-right">
									<span class="">
										<button class="btn  up"  ng-click="previousStep()">
										<i class="fa fa-angle-left" aria-hidden="true"></i> Back</button>
									</span>
									<button type="submit" class="btn  down" >
									Next <i class="fa fa-angle-right" aria-hidden="true"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<section ng-switch-when="6" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block">
							<h3 class="heading text-center">Public Transport </h3>
							<div class="row">
								<!--          <p class="text-center info-text">If available, provide your facility’s closest public transit information.
								</p> -->
								<br>
								<div class="col-md-12" ng-if="transport_type">
									<div class="form-group clearfix">
										<!--    <div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label"> Transport <font> :</font> </label>
										</div> -->
										<div class="col-md-9 center-block col-sm-10 col-xs-10 transportBlock" >
											<div >
												<div class="transportBlock-panel clearfix">
													<div class="checkBlock">
														<label for="transport_metro_subway">
															<div class="checkbox-info">
																<img src="<?php echo FRONTEND_THEME_URL ?>images/metro.svg">
																<span>Metro/Subway</span>
																<input type="checkbox" name="transport[]"  id="transport_metro_subway" ng-model="PostJobData.transport_metro_subway" ><span></span>
															</div>
														</label>
													</div>
													<div class="form-group" ng-show="PostJobData.transport_metro_subway" ng-if="PostJobData.transport_metro_subway != '0'">
														<input type ="text" name="transport_m_s_value"  ng-model="PostJobData.transport_m_s_value"  placeholder="Metro/Subway description" class="form-control"  >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="transportBlock-panel clearfix">
													<div class="checkBlock">
														<label for="transport_bus">
															<div class="checkbox-info">
																<img src="<?php echo FRONTEND_THEME_URL ?>images/bus.svg"><span>Bus</span>
																<input type="checkbox" name="transport[]"  id="transport_bus" ng-model="PostJobData.transport_bus"><span></span>
															</div>
														</label>
													</div>
													<div class="form-group" ng-show="PostJobData.transport_bus" ng-if="PostJobData.transport_bus != '0'">
														<input type ="text" name="transport_bus_value"  ng-model="PostJobData.transport_bus_value"  placeholder="Bus description" class="form-control" >
														
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="transportBlock-panel clearfix">
													<div class="checkBlock">
														<label for="transport_train">
															<div class="checkbox-info">
																<img src="<?php echo FRONTEND_THEME_URL ?>images/train.svg"><span>Train</span>
																<input type="checkbox" name="transport[]"  id="transport_train" ng-model="PostJobData.transport_train"><span></span>
															</div>
														</label>
													</div>
													<div class="form-group" ng-show="PostJobData.transport_train" ng-if="PostJobData.transport_train != '0'">
														<input type ="text" name="transport_train_value"  ng-model="PostJobData.transport_train_value" placeholder="Train Description" class="form-control" >
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="transportBlock-panel clearfix">
													<div class="checkBlock">
														<label for="transport_other">
															<div class="checkbox-info">
																<img src="<?php echo FRONTEND_THEME_URL ?>images/transport-other.svg"><span>Other</span>
																<input type="checkbox" name="transport[]"  id="transport_other" ng-model="PostJobData.transport_other" >
																<span></span>
															</div>
														</label>
													</div>
													<div class="form-group" ng-show="PostJobData.transport_other" ng-if="PostJobData.transport_other != '0'">
														<input type ="text" name="transport_other_value"  ng-model="PostJobData.transport_other_value"  class="form-control" placeholder="Other description">
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<!--  <label class="error" ng-bind="Error.transport_array"></label> -->
										</div>
										<div class="clearfix"></div>
										<div class="">
											<div class="col-md-9 center-block col-sm-10 col-xs-10 transportBlock">
												<div class="clearfix">
													<div class="pull-left col-sm-4 label-block col-xs-4 no-padding-left parking-name" style="text-align: left;">
														<label class="required label title" style="padding:0 8px;"> Parking<font> :</font> </label>
													</div>
													<div class="pull-left col-md-9 col-sm-9 col-xs-8 parking-options">
														<!-- <input type="text" name="paking"  ng-model="PostJobData.paking"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Paking is Required" > -->
														
														
														
														
														<div class="radio radio-info radio-inline">
															<input type="radio" name="parking" id="parking_free" value="Free"  ng-model="PostJobData.parking">
															<label class="radio-inline" for="parking_free" >Free </label>
														</div>
														<div class="radio radio-info radio-inline">
															<input type="radio" name="parking" id="parking_paid" value="Paid" ng-model="PostJobData.parking" >
															<label class="radio-inline" for="parking_paid">Paid</label>
														</div>
														<div class="radio radio-info radio-inline">
															<input type="radio" name="parking" id="parking_none" value="None" ng-model="PostJobData.parking" data-bvalidator="required" data-bvalidator-msg="Parking Required.">
															<label class="radio-inline" for="parking_none" >None </label>
														</div>
														
														<label class="error" ng-bind="Error.parking"></label>
													</div>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4 col-sm-4 col-xs-4 label-block">
											<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
										</div>
										
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<!-- 				<span class="pull-left preview" ng-if="PreviewShow == '1'">
													<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
											</span> -->
											<div class="stepToggle-btn">
												<span class="">
													<button class="btn up" ng-click="previousStep()">
													<i class="fa fa-angle-left" aria-hidden="true"></i> Back</button>
												</span>
												<button type="submit" class="btn  down">
												Next <i class="fa fa-angle-right" aria-hidden="true"></i>
												</button>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section ng-switch-when="7" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block facility-hours">
							<h3 class="heading text-center">Facility Hours</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-10 col-sm-11 col-xs-11 center-block" >
											<div class="row dobBlock facility-hours-block" ng-repeat="(hours_key,hours_value) in PostJobData.facility_hours" >
												
												<div class="col-md-6 col-sm-4 col-xs-4 form-group date" >
													<div ng-dropdown-multiselect  options="master_days_name" selected-model="PostJobData.facility_hours[hours_key].master_days_name"></div>
													
													
												</div>
												
												<div class="col-md-3 col-sm-4 col-xs-4 form-group month">
													<select name="start_time" class="form-control" ng-model="PostJobData.facility_hours[hours_key].start_time" data-bvalidator="required" >
														<option value="">Start Time</option>
														<option ng-repeat = "val in time_array" value="{{val}}">{{val}}</option>
													</select>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-4 form-group year">
													<select name="end_time" class="form-control" ng-model="PostJobData.facility_hours[hours_key].end_time" data-bvalidator="required" >
														<option value="">End Time</option>
														<option ng-repeat = "val in time_array_eve"  value="{{val}}">{{val}}</option>
													</select>
												</div>
												<div class="remove-btn-block" ng-if="$index != '0'">
													<a ng-click="delete($index)" class="btn btn-danger btn-xs" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove"><i class="fa fa-close"></i></a>
												</div>
											</div>
											<label class="error" ng-if="FacilityHoursErr">{{FacilityHoursErr}}</label>
											<div class="addMoreWarp">
												<br>
												<a href="javascript:void(0);" class="btn btn-success btn-xs" ng-click="addMoreFacilityHours()">
												<i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10 col-sm-11 col-xs-11 center-block" >
									<div class="col-md-4 col-sm-4 col-xs-4 draft-btn-block">
										<div class=""><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8 signup-full-column">
										<!-- 			<span class="pull-left preview" ng-if="PreviewShow == '1'">
											<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
										</span> -->
										
										<div class="stepToggle-btn pull-right">
											<span class>
												<button class="btn up" ng-click="previousStep()">
												<i class="fa fa-angle-left" aria-hidden="true"></i> Back
												</button>
											</span>
											<button type="submit" class="btn  down">
											Next <i class="fa fa-angle-right" aria-hidden="true"></i>
											</button>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<section ng-switch-when="8" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
						<div class="form-section-block">
							<h3 class="heading text-center">Facility Details</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Facility Type <font> :</font> </label>
										</div>
										<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
											<select name="facility_type" ng-model="PostJobData.facility_type" class="form-control"  ng-if="facility_type" data-bvalidator="required" data-bvalidator-msg="Required">
												<option value="">Select Facility Type</option>
												<option ng-repeat ="(key,value) in facility_type"  ng-if="key" value="{{key}}">{{value}}</option>
											</select>
											<label class="error" ng-bind="Error.facility_type"></label>
										</div>
									</div>
								</div>
							</div>
							
							<!-- 				<div class="row">
									<div class="col-md-12">
										<div class="form-group clearfix">
												<div class="col-md-2 col-sm-4 label-block col-xs-4">
													<label class="label">EMR<font> :</font> </label>
												</div>
												<div class="col-md-2 col-sm-8 col-xs-8 signup-full-column">
													<input type="text" name="emr"  ng-model="PostJobData.emr"  class="form-control" >
													<span class ="optional-tag">(optional)</span>
												</div>
										</div>
									</div>
							</div> -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Number of full-time doctors<font> :</font> </label>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 signup-full-column">
											<input type="text" name="no_of_fulltime"  ng-model="PostJobData.no_of_fulltime" maxlength="6" only-num class="form-control" step="1" data-bvalidator="required" data-bvalidator-msg="Required" min="0" >
											<label class="error" ng-bind="Error.no_of_fulltime"></label>
										</div>
										<div class="clearfix visible-600"></div>
										<div class="col-md-2 col-sm-3 col-xs-2 no-padding label-block part-time-block">
											<label class="label">Part-time doctors<font> :</font> </label>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-2 signup-full-column">
											<input type="text" only-num maxlength="6" name="part_time_doctor" min="0" ng-model="PostJobData.part_time_doctor"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Required" >
											<label class="error" ng-bind="Error.part_time_doctor"></label>
										</div>
										
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group clearfix">
										<div class="col-md-4 col-sm-4 label-block col-xs-4">
											<label class="label">Number of exam rooms<font> :</font> </label>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-3 signup-full-column">
											<input type="text" maxlength="6" only-num name="exam_room"  ng-model="PostJobData.exam_room"  class="form-control" data-bvalidator="required" min="0" data-bvalidator-msg="Required" >
											<label class="error" ng-bind="Error.exam_room"></label>
										</div>
										<div class="clearfix visible-600"></div>
										<div class="col-md-2 col-sm-3 label-block col-xs-2 no-padding emr-block">
											<label class="label">EMR<font> :</font> </label>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-2 signup-full-column">
											<input type="text" name="emr"  ng-model="PostJobData.emr"  class="form-control" >
											<span class ="optional-tag">(optional)</span>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="row multicheck-option">
									<div class="col-md-12" ng-if="nursing">
											<div class="form-group clearfix">
												<div class="col-md-4 col-sm-4 label-block col-xs-4">
														<label class="label"> Nursing <font> :</font> </label>
												</div>
												<div class="col-md-7 col-sm-8 col-xs-8 signup-full-column">
														<div class="row checkbox-group">
												
															<div class="col-md-6"  ng-repeat ="nurs in nursing" ng-if="nurs.id" >
																<div class="checkbox checkbox-info" ng-if="($index + 1) != nursing.length">
														
																	<input type="checkbox"  id="{{nurs.id}}" name="nursing_array[]"  ng-model="PostJobData.nursing_array[nurs.id]" value="{{nurs.id}}" ng-if="($index + 1) != nursing.length" ng-click="uncheckAll_nursing($index)">
																	<label for="{{nurs.id}}">{{nurs.name}}</label>
														
																</div>
													
																<div class="checkbox checkbox-info" ng-if="($index + 1) == nursing.length">
														
																	<input type="checkbox"  id="{{nurs.id}}" name="nursing_array[]"  ng-model="PostJobData.nursing_array[nurs.id]" value="{{nurs.id}}" ng-if="($index + 1) == nursing.length" data-bvalidator="required" data-bvalidator-msg="Required." ng-click="uncheckAll_nursing($index)">
																	<label for="{{nurs.id}}">{{nurs.name}}</label>
																</div>
															</div>
														</div>
														
													</div>
											</div>
									</div>
							</div> -->
							<div class="col-md-4 col-sm-4 col-xs-4 label-block">
								<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
							</div>
							<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
								<!-- 				<span class="pull-left preview" ng-if="PreviewShow == '1'">
									<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
								</span> -->
								<div class="stepToggle-btn pull-right">
									<span class>
										<button class="btn  up" ng-click="previousStep()">
										<i class="fa fa-angle-left" aria-hidden="true"></i> Back</button>
									</span>
									<button type="submit" class="btn  down" >
									Next <i class="fa fa-angle-right" aria-hidden="true"></i>
									</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
					</section>
					<!--  <section ng-switch-when="9" class="form-section form-section-media-view {{animationClass}} wow" ng-class="{{animationClass}}">
								<div class="form-section-block">
										<h3 class="heading text-center">Allied Health Professionals</h3>
										<div class="row multicheck-option">
											<div class="col-md-12" ng-if="health_professionals">
													<div class="form-group clearfix">
														<div class="col-md-4 col-sm-4 label-block col-xs-4">
															<label class="label"> Allied Health Professionals: </label>
														</div>
														<div class="col-md-6 col-sm-8 col-xs-8 signup-full-column">
															<div class="row checkbox-group">
																<div class="col-md-6 col-xs-6 allied-health-block" ng-repeat ="health_p in health_professionals" ng-if="health_p.id">
																	<div class="checkbox checkbox-info" ng-if="($index + 1) != health_professionals.length">
																		<input type="checkbox" name="health_array[]" id="{{health_p.id}}" ng-model="PostJobData.health_array[health_p.id]" value="{{health_p.id}}" ng-if="($index + 1) != health_professionals.length"  ng-click="uncheckAll_health_pro($index)"> <label for="{{health_p.id}}" >{{health_p.name}}</label>
																		
																	</div>
																	<div class="checkbox checkbox-info" ng-if="($index + 1) == health_professionals.length">
																		<input type="checkbox" name="health_array[]" id="{{health_p.id}}" ng-model="PostJobData.health_array[health_p.id]" value="{{health_p.id}}" ng-if="($index + 1) == health_professionals.length" data-bvalidator="required" data-bvalidator-msg="Required." ng-click="uncheckAll_health_pro($index)"> <label for="{{health_p.id}}" >{{health_p.name}}</label>
																		
																	</div>
																</div>
														</div>
															<label class="error" ng-bind="Error.health_array"></label>
														</div>
													</div>
											</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 label-block">
											<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a>
									</div>
								</div>
								<div class="col-md-6 col-sm-8 col-xs-8 signup-full-column">
									<div class="stepToggle-btn pull-right">
											<span class>
													<button class="btn  up" ng-click="previousStep()">
													<i class="fa fa-angle-left" aria-hidden="true"></i> Back
													</button>
											</span>
												<button type="submit" class="btn  down">
												Next <i class="fa fa-angle-right" aria-hidden="true"></i>
											</button>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
				</section> -->
				<section ng-switch-when="9" class="form-section form-section- media-view {{animationClass}} wow" ng-class="{{animationClass}}">
					<div class="form-section-block">
						<h3 class="heading text-center">Listing Description </h3>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group clearfix">
									<div class="col-md-4 col-sm-4 label-block col-xs-4">
										<label class="label">Listing Title<font> :</font></label>
									</div>
									<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
										<input type="text" maxlength="30" ng-trim="false" name="listing_title" ng-model="PostJobData.listing_title" class="form-control" data-bvalidator="required" data-bvalidator-msg="Title is required" placeholder="Ex. Busy Clinic Downtown (as example)">
										<span>{{30 - PostJobData.listing_title.length}} Remaining</span>
										<label class="error" ng-bind="Error.listing_title" ></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group clearfix">
									<div class="col-md-4 col-sm-4 label-block col-xs-4">
										<label class="label">Listing Description<font> :</font>
										</label>
									</div>
									<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
										<textarea   maxlength="5000" ng-trim="false" name="listing_description" ng-model="PostJobData.listing_description" class="form-control" data-bvalidator="required" data-bvalidator-msg="Description is required" rows="7" placeholder="Briefly describe your facility, what you are looking for and the benefits of working here; Radiology available, Specialties available etc. Please NO phone numbers, e-mail addresses or hyperlinks">
										</textarea>
										
										<span ng-init="maxLimitOfListingDescription=5000">{{maxLimitOfListingDescription - PostJobData.listing_description.length}} remaining</span>
										<label class="error" ng-bind="Error.listing_description"></label>
									</div>
								</div>
							</div>
							
							
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 label-block">
							<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
						</div>
						<div class="col-md-6 col-sm-7 col-xs-7 signup-full-column">
							<!-- 			<span class="pull-left preview" ng-if="PreviewShow == '1'">
								<a href="#" class="btn btn-save" ng-click="goToPreviewStep()">Go to Preview</a>
							</span> -->
							<div class="stepToggle-btn pull-right">
								<span class>
									<button class="btn  up" ng-click="previousStep()">
									<i class="fa fa-angle-left" aria-hidden="true"></i> Back
									</button>
								</span>
								<button type="submit" class="btn  down">
								Next <i class="fa fa-angle-right" aria-hidden="true"></i>
								</button>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</section>
				
				<section ng-switch-when="10" class="form-section form-secti on-media-view {{animationClass}} wow" ng-class="{{animationClass}}">
					<div class="form-section-block">
						
						<!--  <input type="hidden" name="check_loader" ng-model="check_loader" value="1"> -->
						<h3 class="heading text-center">Upload photos</h3>
						<div class="uploadPhotoBlock">
							<div class="" >
								<div class="col-md-11 picUploadContent" >
									<div class="my-drop-zone" nv-file-drop="" uploader="uploader" >
										<!-- <h3>Select files</h3> -->
										<span class="icon"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
										</span>
										<span class="shadow"></span>
										<span class="text">Click to add file or Drag here</span>
										<input type="file" nv-file-select="" uploader="uploader" multiple="" />
										<div class="leftSide">
											<ul>
												<li>Please share <font>5</font> relevant pictures of the your medical facility </li>
												<li><span>1: Reception,</span><span> 2: Waiting Room,</span><span> 3: Exam Room</span></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-11 center-block">
									<div class="text-center photo-link">
										<p>See how to take professional looking photos yourself.</p>
										<div class="text-center"><a href="{{site_url}}photographer-photo" target="_blank" class="btn btn-theme">Click Here</a></div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							
							<div class="row">
								<div class="col-md-12" >
									<div class="col-md-1"></div>
									<ul class="uploadPhotoList">
										<li ng-repeat="item in uploader.queue" class="itemImage">
											
											<div class="canvas-warp">
												<div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, width: 120 }"></div>
											</div>
											<div class="progress" style="margin-top: 10px;">
												<div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
											</div>
											<span ng-show="item.isCancel">
												<i class="glyphicon glyphicon-ban-circle"></i>
											</span>
											<span ng-show="item.isError">
												<i class="glyphicon glyphicon-remove"></i>
											</span>
											
											<button type="button" class="btn btn-danger btn-xs pull-right" ng-click="item.remove()">
											<i class="fa fa-close"></i>
											</button>
											<button ng-show="item.isSuccess" type="button" class="btn btn-warning btn-xs btn-rotate" ng-click="imagerotate($index,'canvas',item.file.name)">
		                                        <i class="fa fa-repeat"></i>
		                                    </button>
											<span ng-show="item.isSuccess" class="btn btn-success btn-xs pull-left">
												<i class="glyphicon glyphicon-ok"></i> Uploded
											</span>
											
										</li>
									</ul>
								</div>
								<div class="clearfix"></div>
								<br>
								<!-- 	    <div class="text-center" >
									<button type="button" class="btn  btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
									<span class="glyphicon glyphicon-upload"></span> Upload all
									</button>
								</div> -->
								
							</div>
							<!-- Upload demo area  -->
						</div>
						<div class="col-md-4 col-sm-4 label-block col-xs-4">
							<div class="draft-btn-block"><a href="#" class="btn btn-save" ng-click="SaveAsDraft()">Save as Draft</a></div>
						</div>
						<div class="col-md-6 col-sm-8 col-xs-8 signup-full-column">
							<div class="stepToggle-btn pull-right">
								<span class="">
									<button class="btn up" ng-click="previousStep()">
									<i class="fa fa-angle-left" aria-hidden="true"></i> Back
									</button>
								</span>
								<button type="submit" class="btn  down">
								Next <i class="fa fa-angle-right" aria-hidden="true"></i>
								</button>
								
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
						<br><br>
					</div>
				</section>
				
				
			</div>
		</form>
		<div class="process-bar col-md-7 col-sm-7 col-xs-12" ng-if="postJob!='12'">
			<div class="progress-section">
				<!--progress bar strip-->
				<div class="progress">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{progressBar}}%">
						<span class="sr-only">{{progressBar}}% Complete </span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="completion"><font> {{progressBar}}% </font> Complete</div>
				<!--progress bar strip end-->
			</div>
		</div>

	</div>
		<div class="view-map-btn hidden-md hidden-sm visible-xs">
			<a href="#" class="view-map" ng-click="toggeleclass()" ><img src="<?php echo FRONTEND_THEME_URL ?>images/electric-bulb-blue.svg"> Tips!</a>
		</div>
	<div class="col-md-5 col-sm-5 register-right-section page-container" ng-class="toggleclassfunctioning">
		<div class="col-md-10 col-md-offset-1 text-center">
			
		</div>
		
		<div class="col-md-10 col-md-offset-1 register-right-content signup-page step_{{ postJob }}">
			<div class="content-block postJobtipsBlock" ng-switch on="postJob">
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="1" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Contact Information</h3>
					<div class="">
						<ul>
							<li>This information will not be made available for public viewing</li>
							<li>Messages from doctors will be received in your YoloMD inbox</li>
						</ul>
					</div>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="2" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">I Am Seeking</h3>
					<ul>
						<li>If your are looking for multiple specialties you must post these jobs separately
						</li>
						<li>You can also save this job form as a draft to complete at a later date</li>
					</ul>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="3" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Special Qualifications</h3>
					<ul>
						<li>Explain any fellowship training or special certifications you would prefer your potential candidate to have
						</li>
						
					</ul>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="4" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Job Description</h3>
					<ul>
						<li>A “Locum” is a temporary work contract. Please ensure to indicate the duration of the contract when describing the position later in the job form
						</li>
					</ul>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="5" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Facility Location</h3>
					<ul>
						<li>Your facility will have a designated pin on the map of the search results page for your city
						</li>
					</ul>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="6" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Public Transport</h3>
					<ul>
						<li>If available, provide your facility’s closest public transit and parking information
						</li>
					</ul>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="7" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Facility Hours</h3>
					<ul>
						<li>You can choose each day(s) more than once to indicate multiple shifts available on the same day(s)
						</li>
						<li>Or select multiple days if your hours of operation are always the same</li>
					</ul>
				</div>
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="8" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Facility Details</h3>
					<ul class="facility-details-tips-ul">
						<li><b>Hospital</b>: A facility of any size that usually consists of an emergency department and inpatient care
						</li>
						<li><b>Government Clinic</b>: A clinic owned and operated by a government organization that consists of family doctors and/or specialists</li>
						<li><b>Private Family Practice</b>: A single or group of family doctors that own and operate their own practice but still bill the government for services provided</li>
						<li><b>Specialist Clinic</b>: A single or group of specialists that own and operate their own practice but still bill the government for services provided</li>
						<li><b>Private Clinic</b>: A single or group of doctors (family or specialty) that own and operate their own practice and charge patients for the services provided (ie. DO NOT bill the government)</li>
						<li><b>Rehabilitation Centre</b>:  A rehabilitation centre that also employs physicians or provides part-time physician consultation</li>
						<li><b>Retirement Home</b>: A retirement or long term care facility that employs physicians or provides part-time physician consultation</li>
					</ul>
					<!-- <div class="premium-access-tips">
						<ul>
								<li class="upon-request">
								<span class="icon"><img src="<?php //echo FRONTEND_THEME_URL ?>images/money-bag.svg" ></span>
								For outpatient clinics please indicate if nursing services are “included” or available depending on the contract or an extra charge (“upon request”).
							</li>
						</ul>
					</div> -->
				</div>
				<!-- <div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="9" ng-class="fadeIn">
					<img src="<?php //echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Allied Health Professionals</h3>
					<ul>
						<li>Please indicate if any allied health professionals work or consult within your clinic
						</li>
					</ul>
				</div> -->
				<div class="content-block-inner postJobtips fadeIn wow" data-wow-delay="1s" ng-switch-when="9" ng-class="fadeIn">
					<img src="<?php echo FRONTEND_THEME_URL ?>images/electric-light-bulb.svg" class="tip-image">
					<h3 class="text-uppercase">Listing Description</h3>
					<ul>
						<li>When describing your practice please refrain from using all capital letters, excessive exclamation points, providing phone numbers, e-mails or hyperlinks to other websites.
						</li>
						<li>All listings are reviewed for quality assurance before they are posted</li>
					</ul>
				</div>
			</div>
		</div>
		
	</div>
	<div class="clearfix"></div>
</div>
<!-- </div> -->
</div>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
<script type="text/javascript">
wow = new WOW(
{
boxClass:     'wow',
animateClass: 'animated',
offset:       200,
mobile:       true,
live:         true
}
)
wow.init();
//event.stopPropagation();
</script>
<script type="text/javascript">
// jQuery(document).ready(function ($) {
// var windowWidthNew   = $(window).width();
// var windowHeight = $(window).height();
// if(windowWidthNew >= 767){
//   jQuery(window).on("resize", function () {
//     var windowHeight = $(window).height();
//     $('.formPage-section').height(windowHeight);
//     $('body').height(windowHeight);
//     var formSection = $('.form-section-block').innerHeight();
//     var leftSide = $('.left-side').innerHeight();
//     if (formSection > leftSide) {
//     $('.formsection').css('height', '100%');
//     } else {
//     $('.formsection').css('height', '100%');
//     }
//     $('.left-side').css('height', windowHeight);
//   }).resize();
// }
// else{
//    $('.formsection').css('min-height','1000px' );
//    $('.form-section-block').css('min-height','900px');
//    $('.content-container').css('min-height','900px');
//    $('.form-section').css('min-height','900px');
//    $('.formsection').css('min-height','900px');
// }

// });
jQuery(document).ready(function ($) {
jQuery(window).on("resize", function () {
var windowWidthNew   = $(window).width();
var windowHeight = $('window').height();
//alert($('.form-section-block').height());
var windowHeight = $(window).height();
if(windowHeight >= 769){
$('.register-right-content').removeClass('vertical-center');
$('.register-right-content').addClass('vertical-center');
$('.site-form').removeClass('vertical-center');
$('.site-form').addClass('vertical-center');
var step_height_change = $('.register-right-content.signup-page .content-block').height();
	if(step_height_change >=769){
		$('.register-right-content.signup-page .content-block').css('top',50 + 'px');
	}
//console.log("height");
}else{
$('.site-form').removeClass('vertical-center');
$('.site-form').addClass('check-iphone');
$('.register-right-content').removeClass('vertical-center');
}
}).resize();
});
</script>
<style type="text/css">
body{overflow: hidden;}
.angular-google-map-container { height: 100%; margin-top: -45px;}
.map-preview{height: 100%;}
a{
	cursor:pointer!important;
}
.preview{
	margin-right:10px;
}
/*@media(max-width: 580px){
		.content-container{position: relative !important;}
}
@media(max-width: 480px){
.formsection{display: block;}
.form-section{display: block;}
.form-section-media-view-container{
position: relative;
overflow-y: scroll;
height: auto !important;
}
.formsection.form-section-media-view-table{
display: block;
width: 100%;
}
.formPage-section-media-view{height: auto !important;}
.formPage-section-media-view .left-side{height: auto !important;}
.form-section-media-view{height: auto !important;min-height: auto !important;    padding-top: 30px;}
.content-container{position: relative !important;}
body{overflow: scroll !important;}
}*/
html{position: relative;}
body{
overflow: hidden;
position: absolute;
width: 100%;
top: 0;
left: 0;
}
.register-right-content{margin-top: 15px;}
.left-side{/*padding-bottom: 100px;padding-top: 80px;*/}
.content-section{
padding-bottom: 100px;padding-top: 100px;
}
.min-content-block{
position: absolute;
overflow: hidden;
height: 100%;
width: 100%;
top: 0;
left: 0;
}
.form-section{
display: block;
position: relative;
}
.site-form{
display: block;
margin-bottom: 100px;
margin-top: 80px;
}
.vertical-center{
position: relative;
top: 50%;
transform: translateY(-50%);
}
@media(max-width: 480px){
body{overflow:auto !important;position: relative;}
.min-content-block{overflow-y: auto;height: auto;}
}
.form-section{width: 100%;}
.formsection{width: auto;height: auto;display: block;    overflow-y: initial;}
</style>