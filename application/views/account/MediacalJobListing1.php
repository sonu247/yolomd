<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">
	<div class="">
		<div class="container" >
		<div class="dashboard">

		  <div class="row">
		    <!--Left Section-->
		    <div class="col-md-3">
		     <?php $this->load->view('account/MedicalLeftNavigation'); ?>
		   </div>
		    <!--Left Section End-->
		    <!--Right Section-->
		    <div class="col-md-9">
		    <!--Right Section Breadcrumb-->
		    <div class="breadcrumb-block">
			 <ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="#">Library</a></li>
				<li class="active">Data</li>
			 </ol>
		    </div>
		    <!--Right Section Breadcrumb End-->
		   	<div class="dashboard-right">
		   	<h3 class="header pull-left"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.png" class="">My Job Listing</h3>

            <div class="pull-right">
            <select class="sort-select">
                <option value="Position">Sort By</option>
                <option value="Permanent">Permanent</option>
                <option value="Locum">Locum</option>
            </select>
            </div>
		   	<div class="jobList-block">

        	<div class="jobListTile-block">
        	<table class="table table-hover jobListTable table-striped">
        	<tr>
                <th width="10%"></th>
                <th width="27%">Title</th>
                <th width="15%">Speciality</th>
                <th width="15%" class="text-center">Date</th>
                <th width="4%" class="text-center">Status</th>
                <th width="10%" class="text-center">Action</th>
        	</tr>
        	
        	<tr ng-repeat="post_detail in post_jobs" ng-if="post_jobs">
        	<td>
        		<a href="#"><img src="<?php echo FRONTEND_THEME_URL ?>/images/no-preview.jpg" width="80" class="img-thumbnail"></a>
            </td>
        	<td>
        		<a href="#" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Post Id"> <b># </b>{{post_detail.id}}</a>,
                {{post_detail.facility_name}} {{post_detail.facility_name}} {{post_detail.facility_name}}
                <div>
        		<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Location"> <i class="fa fa-map-marker" aria-hidden="true"></i> {{post_detail.facility_city}} ,{{post_detail.facility_city}}</span>
                </div>
        	</td>
        	<td ng-init="getMedicalSpec(post_detail.medical_specialty_name,$index)">
				{{post_detail.medical_spec_name}}
  	 		
  	 		 </td>
        	<td>
             <div class="date">
				<div><label data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Created Date"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> </label><span>{{post_detail.created}}</span>
                </div>
                <div><label data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Posted Date"><i class="fa fa-external-link" aria-hidden="true"></i> </label><span>{{post_detail.created}}</span>
                </div>
             </div>
        	</td>
            <td>
                <span class="btn btn-status btn-success btn-xs" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Status" ng-click="ChangeStatus(post_detail.status,$index)">{{post_detail.status}}</span>
            </td>
        	<td>
        	<div class="action-panel">


        		<a ng-href="{{site_url}}post-job-update/{{post_detail.id}}" class="btn btn-warning btn-sm">
    				<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
    				</span>
        		</a>

        		<button class="btn btn-primary btn-sm">
        		<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Clone"> <i class="fa fa-clone" aria-hidden="true"></i></span>
        		</button>

        		<button class="btn btn-info btn-sm">
        		<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Preview"><i class="fa fa-eye" aria-hidden="true"></i></span>
        		</button>

        		<button class="btn btn-danger btn-sm">
        		<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Delete"><i class="fa fa-trash" aria-hidden="true"></i></span>
        		</button>
        	</div>
        	</td>

        	</tr>
			</table>	
			</div>
		   </div>

            <nav>
              <ul class="pagination pull-right">
                <li>
                  <a href="#" aria-label="Previous" >
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li><a href="#" class="active">1</a></li>
                <li><a href="#">2</a></li>
                <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
            <div class="clearfix"></div>

		   	</div>
		   </div>
		   <!--Right Section End-->
		  </div>
		   
		</div>
		</div>
	</div>
</div>




<?php 
  $this->load->view('include/footer_menu');
?>