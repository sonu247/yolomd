<!--Message center modal-->

<div class="modal fade custom-modal" id="messageCenter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog  modal-large" role="document">
    <div class="modal-content" style="min-height:30px!important;">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
     
      <div class="modal-body" >
       <div class="themeform">
        <section class="">
     <div class="">
<!--       <h3 class="heading">{{listing_title}}</h3> -->

      
<!--    <h3 class="header"><img src="<?php //echo FRONTEND_THEME_URL ?>/images/icons/my-message.png" class="">Messages</h3> -->
   <div class="message-center">
   <div class="clearfix message-header">
     <div class="message-title pull-left">
     {{listing_title}}
     </div>
     <div class="pull-right">
        <span class="">
          <img src="<?php echo FRONTEND_THEME_URL ?>images/medal-new.svg" width="15"> {{medical_specialty}}
        </span>
        <span class="city-name">
          <i class="fa fa-map-marker" aria-hidden="true"></i> {{facility_city}}
        </span>
     </div>
   </div>

   <div class="message-subject" ng-if="msg_subject">
   <img src="<?php echo FRONTEND_THEME_URL ?>images/msg-mark.svg" width="15" > {{msg_subject}}
    <span ng-if="username"> -  <i class="fa fa-user" aria-hidden="true"></i> {{username}}</span>
   </div>
   <div class="table-scroll">
  <table class="table table-hover table-striped message-table table-condensed">
     <tr ng-repeat="messages in messages" ng-if="messages">
     <td>    
       <div class="message-body">
       <div ng-if="messages.msg_receive" class="icon-block">
          <span class="icon" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Receive"><img src="<?php echo FRONTEND_THEME_URL ?>images/incoming.svg" width="15"></span>
       </div> 
       <div ng-if="messages.msg_receive==0" class="icon-block">
       <span class="icon" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Send"><img src="<?php echo FRONTEND_THEME_URL ?>images/outgoing.svg" width="15"></span> 
       </div>

       <div class="message-text">
        {{messages.msg_body}}
          <div class="clearfix"></div>
           <div class="msg-utility">
            <i class="fa fa-calendar" aria-hidden="true"></i> {{messages.date_time}}
           </div>
       </div>
       </div>

     </td>
     </tr>
   </table>
   </div>
   <div>

             <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="insertmsg()" >
             <div id="search_loader" class="search_loader_forget" style="width: 100%; height: 195px; display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left: 0;text-align:center; ">
            <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif" style="margin-top:35px;">
            </div>
             <div class="row">
              <div class="form-group col-md-12">
               <textarea class="form-control" rows="6" data-bvalidator="required" data-bvalidator-msg="Message is Required" ng-model="replymsg.reply" placeholder="Type your Message"></textarea>
              </div>
             </div>
             <button type="submit" class="btn btn-theme">Send</button>
             <div style="color:green;" ng-if="reportSent" class="pull-right" ng-bind-html="msg_sent" >
            
             </div>
             </form>

   </div>
   </div>



     </div>
        </section>
       </div>
      </div>
    </div>
  </div>
</div>