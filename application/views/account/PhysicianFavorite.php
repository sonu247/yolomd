<?php 
  $this->load->view('include/header_menu');
?>

<div class="page-container">
	<div class="">
		<div class="container">
		<div class="dashboard">

		  <div class="row">
		    <!--Left Section-->
		    <div class="col-md-3 col-sm-3 dashboard-Left-warp">
		    <?php $this->load->view('account/PhysicianLeftNavigation'); ?>
		   </div>
		    <!--Left Section End-->
		    <!--Right Section-->
		    <div class="col-md-9 col-sm-9">
		    <!--Right Section Breadcrumb End-->
		   	<div class="dashboard-right">
		   	<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-favorite.svg" class="">Favourite Jobs</h3>
             <div  data-target="#common_modal_msg" data-toggle="modal"></div>
             <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
            <div class="myjoblisting-sort">
            </div>
		   	<div class="jobList-block">

        	<div class="jobListTile-block">
           
            <div class="table-responsive">
        	<table class="table table-hover jobListTable table-bordered table-striped savedSearch" ng-if="favourite_jobs!=0">
        	<tr>
            <th class="text-center hidden-md hidden-lg hidden-sm" ><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" width="18"> Action</th>
            <th >S.No.</th>
            <th ><img src="<?php echo FRONTEND_THEME_URL ?>images/camera.svg" width="18"> Image</th>
            <th >Job Title</th>
         		<th class="text-center" ><img src="<?php echo FRONTEND_THEME_URL ?>images/calendar.svg" width="18"> Marked Date</th>
            <th class="text-center hidden-xs" ><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" width="18"> Action</th>
        	</tr>
        	
        	<tr ng-repeat="favourite_jobs in ItemsByPage[currentPage] | orderBy:columnToOrder:reverse" ng-if="favourite_jobs" >
            <td class="text-center hidden-md hidden-lg hidden-sm">
                  <div class="action-panel">
                   <a href="jobdetail/{{favourite_jobs.encrypt_id}}" class="btn btn-sm" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Job detail"><i class="fa fa-eye" aria-hidden="true"></i></a>
                   <a href="#" ng-click="unfavorite_mark(favourite_jobs.id,$index,currentPage)" class="btn btn-sm btn-delete" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove favourite job"><i class="fa fa-trash"></i></a>
                  </div>
            </td>          
            <td>
                {{pageSize *((currentPage + 1) -1)+$index+1}}.
            </td>
            <td >
                <a href="jobdetail/{{favourite_jobs.encrypt_id}}" ng-if="favourite_jobs.image_path == null || favourite_jobs.image_path== ''">
                <img src="<?php echo FRONTEND_THEME_URL ?>/images/no-preview.jpg" class="img-thumbnail"></a>
                <a href="jobdetail/{{favourite_jobs.encrypt_id}}" ng-if="favourite_jobs.image_path != null && favourite_jobs.image_path != ''">
                <img src="{{favourite_jobs.image_path}}" class="img-thumbnail"></a>
            </td>
             <td>
              <a href="jobdetail/{{favourite_jobs.encrypt_id}}">
                {{favourite_jobs.job_name}}<br/><img src="<?php echo base_url();?>assets/front/images/medal-new.svg" width="20">
                {{favourite_jobs.medical_specialty}}
              </a>
   
            </td>
            <td  class="text-center"  style="width:20%; text-align:center;">
                <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> {{favourite_jobs.marked_date}}
            
            </td>
            <td class="text-center hidden-xs">
                  <div class="action-panel">
                   <a href="jobdetail/{{favourite_jobs.encrypt_id}}" class="btn btn-sm" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Job detail"><i class="fa fa-eye" aria-hidden="true"></i></a>
                   <a href="#" ng-click="unfavorite_mark(favourite_jobs.id,$index,currentPage)" class="btn btn-sm btn-delete" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove favourite job"><i class="fa fa-trash"></i></a>
                  </div>
            </td>
            </tr>
           

			</table>
       <table class="table table-hover jobListTable table-striped"  ng-if="favourite_jobs==0">
          <tr> 
          <td  class="text-center nofound-block-td"> 
            <div class="nofound-block">
             <div class="info-icon">
              <span>
               <img src="<?php echo FRONTEND_THEME_URL ?>images/list.svg" width="35">
              </span>
             </div>
             <h4 class="text-center">
             You do not have any favourite medical facilities
             </h4>
            <div class="col-md-8 col-md-offset-2 center-block nofound-desc">Click the heart on the top right corner of
          pictures to add facilities to your favourites list

            </div>
             <div><a href="jobs/" class="button">Click to add favourite job</a></div>
            </div>
            </td>
            </tr>
          </table> 
            </div>
			</div>
		   </div>

            <nav>
              <ul class="pagination pull-right">
                <li><a href="#"  aria-label="Previous" ng-class="{'active1' : (currentPage == '0')}" ng-click="firstPage()" ng-if="favourite_jobs_count != pageSize && favourite_jobs_count > pageSize">1</a>

                </li>
                <li  ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active1' : (n == currentPage)}" href="#" ng-click="setPage()" ng-if="favourite_jobs_count != pageSize " ng-bind="n+1">1</a>


                </li>
                <li  ><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active1' : (currentPage == last_index)}" ng-if="favourite_jobs_count != pageSize && favourite_jobs_count > pageSize">{{last_index + 1}}</a>

                </li>
             </ul>
            </nav>
            

		   	</div>
		   </div>
		   <!--Right Section End-->
		  </div>
		</div>
		</div>
	</div>
</div>

<script type="text/javascript">

</script>


<?php 
  $this->load->view('include/common_modal_msg');
  $this->load->view('include/footer_menu');
?>
<style>
.active1{
    background:rgb(3,166,237)!important;
    color:white!important;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid rgba(221, 221, 221, 0.18);
}
.jobListTable{border-top-color: transparent;}
table.jobListTable tr:first-child {
  background-color: #f9f9f9;
}
.jobListTable tr th:nth-child(2) {width: 50px;}
.jobListTable tr th:nth-child(3) {width: 105px;}
.jobListTable tr th:nth-child(4){width: 190px;}
.jobListTable tr th:nth-child(5){width: 165px;}
.jobListTable tr td:nth-child(5){vertical-align: middle;}
.jobList-block table tr td:last-child{text-align: center;}
table.jobListTable tr:first-child{background-color: #f9f9f9;}

@media(max-width: 768px){
 .jobListTable tr th:first-child{width: 95px;}
 .jobListTable tr th:nth-child(2){display: none;}
 .jobListTable tr td:nth-child(2){display: none;}

 .jobListTable tr th:nth-child(3){display: none;}
 .jobListTable tr td:nth-child(3){display: none;} 
}
</style>