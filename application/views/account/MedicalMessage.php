<?php
$this->load->view('include/header_menu');
?>
<div class="page-container">
	<div class="">
		<div class="container">
			<div class="dashboard">
				<div class="row">
					<div class="col-md-3 col-sm-3 dashboard-Left-warp">
						<?php $this->load->view('account/MedicalLeftNavigation'); ?>
					</div>
					<div class="col-md-9 col-sm-9">
						<div id="search_loader_step" class="search_loader" style="width: 100%; height: 100%; display: block; background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-align:center; ">
							<img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif" style="margin-top:0px; float:left; margin-left:15px;">
						</div>
						<div class="dashboard-right">
							<h3 class="header"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/messages.svg" class="" style="width:25px;">Messages
							<select ng-if="messages!=0" name="sorting" ng-model="sorting" ng-change="sorting_msg(sorting)" class="sort-select pull-right">
								<option value="All">All Msg.</option>
								<option value="1">Read</option>
								<option value="2">Unread</option>
							</select>
							</h3>
							<div class="clearfix"></div>
							<div class="">
								<div class="table-responsive">
									
									<table class="table table-hover jobListTable messageListTable table-bordered" ng-if="messages!=0">
										<tr>
											<th class="text-center hidden-md hidden-lg hidden-sm"><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" > Action</th>
											<th class="text-center">S.No.</th>
											<th class="text-center">Job Title</th>
											<th class=""><img src="<?php echo FRONTEND_THEME_URL ?>images/clipboard.svg" style="width:18px;"> Subject</th>
											<th class="text-center"><img src="<?php echo FRONTEND_THEME_URL ?>images/calendar.svg" width="18"> Date</th>
											<th class="text-center"><img src="<?php echo FRONTEND_THEME_URL ?>images/flag.svg" width="70"> Flag</th>
											<th class="text-center hidden-xs"><img src="<?php echo FRONTEND_THEME_URL ?>images/settings-new.svg" > Action</th>
										</tr>
										<tr ng-repeat="messages in ItemsByPage[currentPage] | orderBy:columnToOrder:reverse" ng-if="messages" class="{{messages.count_unreadmsg}}">
											<td class="hidden-md hidden-lg hidden-sm">

												<div class="action-panel">
													<a class="btn btn-sm" data-toggle="modal" data-target="#messageCenter" ng-click="showmessagedetail(messages.msg_id,$index,currentPage)">
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="View message detail">
														<i class="fa fa-eye" aria-hidden="true"></i></span>
													</a>
													<a class="btn btn-sm btn-delete" ng-click="delete_msg(messages.msg_id_without_encrypt,$index,currentPage)" >
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove Message">
														<i class="fa fa-trash" aria-hidden="true"></i></span>
													</a>
												</div>
											</td>
											<td class="text-center">
												{{pageSize *((currentPage + 1) -1)+$index+1}}.
												
											</td>
											<td>
												<a href="jobdetail/{{messages.encrypt_id}}" target="_new">
													<div class="title">{{messages.listing_title}}</div>
													
													<div class="speciality pull-left">
														<img src="{{messages.medical_spec_logo}}" width="15">
														{{messages.medical_specialty_name}}
													</div> &nbsp;&nbsp;
													<div class="addressname pull-left"><i class="fa fa-map-marker" aria-hidden="true"></i> {{messages.facility_city}}</div>
												</a>
											</td>
											<td>
												<div class="subject-list">
													<a data-toggle="modal" data-target="#messageCenter" ng-click="showmessagedetail(messages.msg_id,$index,currentPage)" title="View message detail">
														<div>{{messages.msg_subject}}</div>
														<div class="message-body">{{messages.msg_body}}</div>
														<div><i class="fa fa-user" aria-hidden="true"></i> {{messages.user_name}}</div>
													</a>
												</div>
											</td>
											<td class="text-center">
												<div class="subject-list">
													<a data-toggle="modal" data-target="#messageCenter" ng-click="showmessagedetail(messages.msg_id,$index,currentPage)" title="View message detail">
														<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
														{{messages.date_time}}
													</a>
												</div>
											</td>
											<td>
												<!--  <div class="action-panel">
													<a class="btn btn-sm" data-toggle="modal" data-target="#messageCenter" ng-click="showmessagedetail(messages.msg_id,$index,currentPage)">
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="View message detail">
														<i class="fa fa-eye" aria-hidden="true"></i></span>
													</a> -->
													<a ng-if="messages.flag==0" class="btn btn-sm btn-theme" ng-click="markflag(messages.msg_id,$index,currentPage)">
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Report as inappropriate">
														<i class="fa fa-flag"></i></span>
													</a>
													<a ng-if="messages.flag==1" class="btn btn-sm btn-danger" disabled>
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="You flagged" >
														<i class="fa fa-flag"></i></span>
													</a>
													<!--   <a class="btn btn-sm btn-delete" ng-click="delete_msg(messages.msg_id,$index,currentPage)" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove Message">
															<span >
															<i class="fa fa-trash" aria-hidden="true"></i></span>
													</a>
													
												</div> -->
											</td>
											<td class="hidden-xs">
												
												<div class="action-panel">
													<a class="btn btn-sm" data-toggle="modal" data-target="#messageCenter" ng-click="showmessagedetail(messages.msg_id,$index,currentPage)">
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="View message detail">
														<i class="fa fa-eye" aria-hidden="true"></i></span>
													</a>
													<a class="btn btn-sm btn-delete" ng-click="delete_msg(messages.msg_id_without_encrypt,$index,currentPage)" >
														<span data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove Message">
														<i class="fa fa-trash" aria-hidden="true"></i></span>
													</a>
												</div>
											</td>
										</tr>
										
									</table>
									<table class="table table-hover jobListTable table-striped"  ng-if="messages==0">
										<tr>
											<td  class="text-center nofound-block-td">
												<div class="nofound-block">
													<div class="info-icon">
														<span>
															<img src="<?php echo FRONTEND_THEME_URL ?>images/envlope-40X40.svg" width="40">
														</span>
													</div>
													<h4 class="text-center">
													You do not have any messages in your inbox
													</h4>
													<!--<div> <a href="jobs/" class="button">Click to here to send messages</a></div> -->
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
							
							<nav>
								<ul class="pagination pull-right">
									<li><a href="#"  aria-label="Previous" ng-class="{'active_pagination' : (currentPage == '0')}" ng-click="firstPage()" ng-if="post_message_count != pageSize && post_message_count > pageSize">1</a>
								</li>
								<li ng-repeat="n in range(ItemsByPage.length)"> <a ng-class="{ 'active_pagination' : (n == currentPage)}" href="#" ng-click="setPage()" ng-if="post_message_count != pageSize " ng-bind="n+1">1</a>
							</li>
							<li><a href="#"  aria-label="Last" ng-click="lastPage()" ng-class="{ 'active_pagination' : (currentPage == last_index)}" ng-if="post_message_count != pageSize && post_message_count > pageSize">{{last_index + 1}}</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<?php $this->load->view('account/commonmsgdetail');
$this->load->view('include/common_modal_msg');
$this->load->view('include/footer_menu');
?>
<style>
.active_pagination{
background:rgb(3,166,237)!important;
color:white!important;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
border: 1px solid rgba(221, 221, 221, 0.18);
}
.jobListTable{border-top-color: transparent;}
table.jobListTable tr:first-child {
background-color: #f9f9f9;
}
table.jobListTable tr td:first-child{
line-height: 18px;
font-size: 14px;
}
table.jobListTable tr th:nth-child(2){width: 50px;}
table.jobListTable tr th:nth-child(3){width: 275px;text-align: left;}
table.jobListTable tr th:nth-child(4){text-align: left;width: 230px;}
.jobListTable tr th:nth-child(5) {width: 105px;}
table.jobListTable tr th:nth-child(6){width:70px;}
table.jobListTable tr td:nth-child(6){padding-top: 5px;text-align: center;}
.jobList-block table tr td:last-child{text-align: center;}
.jobListTable tr th:first-child{border-left-color: transparent;}
.jobListTable tr th:last-child{border-right-color: transparent;width: 95px;text-align: center;}
table.jobListTable tr td{vertical-align: middle;}
table.jobListTable tr td a .addressname{margin-left: 15px;font-size: 13px;}
.speciality{color: rgba(60, 64, 70, 0.71);font-size: 13px;}
table.jobListTable tr td .title{font-size: 15px;display: block;max-width: 100%;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
table.jobListTable tr td .title:first-letter{text-transform: capitalize !important;}
</style>