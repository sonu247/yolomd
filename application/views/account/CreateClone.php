<?php 
  $this->load->view('include/header_menu');
?>
<div class="content-container two-column-warp ng-cloak">
<div class="formPage-section">

  <div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left: 0;text-aling:center;">
    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
   </div>

  <div class="col-md-7 col-sm-7 full-height left-side post-job-warp" style="{{style_container}}" >
  
  <div class="themeform" >
      
    <form method="post" novalidate data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="create_clone()">
      <div  class="formsection">   
  
        <section class="form-section">
          <div class="info-modal-block">
          <h3 class="heading text-center"> Copy Previous Job</h3>
          <div class="info-icon">
           <span><i class="fa fa-clone" aria-hidden="true"></i></span>
          </div>
          <div class="clearfix"></div>
         <!--  <h3 class="text-center title">{{name_listing}}</h3> -->
         <br>
         <div class="col-md-9 center-block heading-subinfo">
          <h4 class="text-center">If you wish to post a new job with similar parameters as a previous posting; </h4>
           <ul>
             <li><span>Select the previous job from the drop down menu below.</span></li>
             <li><span>Continue on to change the necessary parameters.</span></li>
           </ul>
          </div>
          <br><br>
          <div class="col-md-9 col-sm-10 col-xs-10 center-block no-padding">
          <select name="jobs_list_name" ng-model="jobs_list_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Job Listing Required">
            <option value="">Select Job Listing</option>
            <option ng-repeat="job_l in jobs_list" ng-if="job_l.id" value="{{job_l.id}}">{{job_l.listing_title}}</option>
          </select>
          <div class="clearfix"></div>
          <div>
          <br><br>
          <div class="clone-btn-block">
          <button class="button clone-button" type="submit"><font>Click here to copy previous job</font></button>
          <div class="clearfix visible-mobile"></div>
          <a href="post-job" class="button skip-btn" >Skip <i class="fa fa-angle-right" aria-hidden="true"></i></a>
          </div>
           </div>
           </div>
           </div>
          </section>

      </div>
    </form>
  <!--END sECTION-->
  </div>
  </div>
  
   <div class="col-md-5 col-sm-5 register-right-section page-container"></div>

 
  <div class="clearfix"></div>
 </div>
</div>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
<script type="text/javascript">
 wow = new WOW(
    {
      boxClass:     'wow',    
      animateClass: 'animated', 
      offset:       200,         
      mobile:       true,       
      live:         true        
    }
  )
  wow.init();
  //event.stopPropagation();
</script>
<style type="text/css">
  body {
    overflow: hidden;
  }
</style>