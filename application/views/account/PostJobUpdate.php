<?php
$this->load->view('include/header_menu');
?>
<script src="<?php echo FRONTEND_THEME_URL ?>js/vendor/scrollTo.js"></script>
<div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
  <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
</div>
<div class="page-container">
  <div class="">
    <div class="container" >
      <div class="dashboard">
        <div class="row">
          <div class="col-md-3 col-sm-3 dashboard-Left-warp">
            <?php $this->load->view('account/MedicalLeftNavigation'); ?>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="dashboard-right">
              <h3 class="header" ng-if="PostJobData.listing_title == ''"><img class="" src="<?php echo FRONTEND_THEME_URL ?>images/icons/my-job-listing.png"> Post Job Update </h3>
              <div class="header" ng-if="PostJobData.listing_title">
                <span ng-if="PostJobData.listing_title"><img class="" src="<?php echo FRONTEND_THEME_URL ?>images/icons/edit-icon.png"> {{PostJobData.id}} - {{PostJobData.listing_title}}</span>
                <div class="pull-right">
                  <a ng-if="PostJobData.created" href="javascript:void(0);" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Created date" class="create-date">
                    <span ng-if="PostJobData.created">
                    <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> {{formatDate(PostJobData.created) | date:'d MMM y'}}</span>
                  </a>
                  <a ng-if="PostJobData.posted" href="javascript:void(0);" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Posted Date">
                    <span ng-if="formatDate(PostJobData.posted)"> - {{formatDate(PostJobData.posted)| date:'d MMM y'}}</span>
                  </a>
                </div>
                <div class="clearfix"></div>
              </div>
              <!--
              <div class="col-sm-10 col-md-10 center-block upgrade-message" ng-if="PostJobData.job_verify == '0' && PostJobData.status != '4'">
                <div class="table-cell">
                  Do you want to increase your chances of finding a doctor?<br>Please click here
                </div>
                <div class="table-cell">
                  <a  ng-if="PostJobData.job_verify == '0'" ng-href="{{site_url}}access/{{PostJobData.encrypt_id}}" class="btn"  data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Upgrade" >Upgrade</a>
                </div>
              </div> -->
              
              
              <!--start form-->
              
              <div class="message-center">
                <div class="row">
                  <div class="col-sm-11 col-md-11 center-block">
                    <!--  <div class="alert alert-success" ng-if="UpdateMassage" ng-hide="UpdateAlertMessage">
                      <strong>Success!</strong>  {{UpdateMassage}}.
                    </div> -->
                    <div  data-target="#common_modal_msg" data-toggle="modal"></div>
                    <form method="post" data-bvalidator-validate data-bvalidator-option-validate-till-invalid="true" ng-submit="PostJobUpdate()">
                      <div class="form-section">
                        <div class="themeform">
                          <div class="panel-group postJobpanelGroup postJobpanelGroupupdate">
                            <div class="sectionDivide panel" id="panel1">
                              <div class="section-heading" role="tab" id="heading1">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="1" aria-expanded="true" aria-controls="collapse1"><h3><span class="number">1</span> Contact Information</h3>
                              </a>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                              <section>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group clearfix">
                                      <input type="hidden" name="status" id="status" ng-model="PostJobData.status" value="{{PostJobData.status}}">
                                      <div class="col-md-4 col-sm-4 label-block">
                                        <label class="required label title" >Title <font>:</font> </label>
                                      </div>
                                      <div class="col-md-7 col-sm-8 signup-full-column">
                                        <div class="radio radio-info radio-inline">
                                          <input type="radio" name="contact_name_title" id="contact_name_title" ng-model="PostJobData.contact_name_title" value="Mr.">
                                          <label  for="contact_name_title">Mr.</label>
                                        </div>
                                        <div class="radio radio-info radio-inline">
                                          <input type="radio" name="contact_name_title" id="contact_name_title1" ng-model="PostJobData.contact_name_title" value="Miss">
                                          <label for="contact_name_title1">Miss</label>
                                        </div>
                                        
                                        <div class="radio radio-info radio-inline">
                                          <input type="radio" name="contact_name_title" id="contact_name_title3" ng-model="PostJobData.contact_name_title" value="Dr."   data-bvalidator="required" data-bvalidator-msg="Name Title Required.">
                                          <label for="contact_name_title3">Dr.</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <label class="error" ng-if="Error.contact_name_title"> {{ Error.contact_name_title }}</label>
                                        <!-- ngIf: Error.user_name_title -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group clearfix">
                                      <div class="col-md-4 col-sm-4 label-block">
                                        <label class="label required"> First Name <font>:</font> </label>
                                      </div>
                                      <div class="col-md-7 col-sm-8 signup-full-column">
                                        <input type ="text" maxlength="30" name="contact_first_name"  ng-model="PostJobData.contact_first_name"  class="form-control" data-bvalidator="required" data-bvalidator-msg="First Name Required" value="">
                                        <label class="error" ng-bind="Error.contact_first_name"></label>
                                        <!-- ngIf: Error.contact_person_first_name -->
                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group clearfix">
                                      <div class="col-md-4 col-sm-4 label-block">
                                        <label class="label">Last Name<font> :</font> </label>
                                      </div>
                                      <div class="col-md-7 col-sm-8 signup-full-column">
                                        <input type="text" maxlength="30" name="contact_last_name" ng-model="PostJobData.contact_last_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Last Name Required">
                                        <label class="error" ng-bind="Error.contact_last_name"></label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group clearfix">
                                      <div class="col-md-4 col-sm-4 label-block">
                                        <label class="label">Email<font> :</font> </label>
                                      </div>
                                      <div class="col-md-7 col-sm-8 signup-full-column">
                                        <input type="text" name="contact_email_address"  ng-model="PostJobData.contact_email_address"  class="form-control" data-bvalidator="email,required" data-bvalidator-msg="Valid Login Email Required" id="contact_person_email_address">
                                        <label class="error" ng-bind="Error.contact_email_address"></label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group clearfix">
                                      <div class="col-md-4 col-sm-4 label-block">
                                        <label class="label">Phone Number<font> :</font> </label>
                                      </div>
                                      <div class="col-md-3 col-sm-4 signup-full-column">
                                        <input type="text" only-num-special name="contact_phone_number" ng-model="PostJobData.contact_phone_number" placeholder="(123) 456-7890" class="form-control" mask='999-999-9999' data-bvalidator="required,minlen[8],maxlen[12]" data-bvalidator-msg="Valid number is required" >
                                        <label class="error" ng-bind="Error.contact_phone_number"></label>
                                      </div>
                                      <div class="col-md-1 col-sm-1 label-block no-padding remove-nopadding-left">
                                        <label class="label">Ext.<font> :</font> </label>
                                      </div>
                                      <div class="col-md-2 col-sm-3 signup-full-column">
                                        <input type="text"  name="contect_ext_number" ng-model="PostJobData.contect_ext_number" class="form-control"  data-bvalidator="digit,maxlen[5]">
                                        <span class="optional-tag">(optional)</span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group clearfix">
                                      
                                      <div class="col-md-4 col-sm-4 label-block">
                                        <label class="label">Fax<font> :</font> </label>
                                      </div>
                                      <div class="col-md-3 col-sm-4 signup-full-column">
                                        <input type="text"  mask='999-999-9999' name="contact_fax"  placeholder="(123) 456-7890" ng-model="PostJobData.contact_fax"  class="form-control"  data-bvalidator="minlen[8],maxlen[12]" data-bvalidator-msg="Valid Fax Required" >
                                        <span class="optional-tag">(optional)</span>
                                        <label class="error" ng-bind="Error.contact_fax"></label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                          
                          <div class="sectionDivide panel" id="panel2">
                            <div class="section-heading" role="tab" id="heading3">
                              <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="3" aria-expanded="false" aria-controls="collapse3"><h3><span class="number">2</span>I am seeking</h3>
                            </a>
                          </div>
                          <div id="collapse3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">
                            <section>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group clearfix">
                                    <div class="col-md-4 col-sm-4 label-block">
                                      <label class="label">Medical Specialties<font> :</font> </label>
                                    </div>
                                    <div class="col-md-7 col-sm-8 signup-full-column">
                                      <select disabled name="medical_specialty_name" ng-model="PostJobData.medical_specialty_name" class="form-control" ng-if="medical_specialty" data-bvalidator="required" data-bvalidator-msg="Medical Specialities Required">
                                        <option value="">Select Medical Specialties</option>
                                        <option ng-repeat="medical_s in medical_specialty" ng-if="medical_s.id" value="{{medical_s.id}}">{{medical_s.name}}</option>
                                      </select>
                                      <label class="error" ng-bind="Error.medical_specialty_name"></label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </section>
                          </div>
                        </div>
                        <div class="sectionDivide panel" id="panel3">
                          <div class="section-heading" role="tab" id="heading11">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="11" aria-expanded="false" aria-controls="collapse11"><h3><span class="number">3</span>Special Qualifications </h3>
                          </a>
                        </div>
                        <div id="collapse11" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading11">
                          <section>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group clearfix">
                                  <div class="col-md-4 col-sm-4 label-block">
                                    <label class="label"> Special Qualifications<font> :</font> </label>
                                  </div>
                                  <div class="col-md-7 col-sm-8 signup-full-column">
                                    <textarea maxlength="300" ng-trim="false" name="special_qualification" ng-model="PostJobData.special_qualification" class="form-control" data-bvalidator="required"  rows="7" placeholder="Example: ATLS certified, Subspecialty in Interventional Cardiology, Licenced under the College of Physicians and Surgeons of Ontario etc.">
                                    </textarea>
                                    <span>{{300 - PostJobData.special_qualification.length}} remaining</span>
                                    <label class="error" ng-bind="Error.special_qualification"></label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>
                      </div>
                      <div class="sectionDivide panel" id="panel4">
                        <div class="section-heading" role="tab" id="heading8">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="8" aria-expanded="false" aria-controls="collapse8"><h3><span class="number">4</span>Job Description</h3>
                        </a>
                      </div>
                      <div id="collapse8" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading8">
                        <section>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group clearfix">
                                <div class="col-md-4 col-sm-4 label-block">
                                  <label class="label">Hours<font> :</font> </label>
                                </div>
                                <div class="col-md-7 col-sm-8 signup-full-column">
                                  <select name="hour" ng-model="PostJobData.hour" data-bvalidator="required" class="form-control"  ng-if="hours" data-bvalidator-msg="Required">
                                    <option value="">Select Hours</option>
                                    <option ng-repeat ="(key,value) in hours" ng-if="key" value="{{key}}">{{value}}</option>
                                  </select>
                                  <label class="error" ng-bind="Error.hour"></label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group clearfix">
                                <div class="col-md-4 col-sm-4 label-block title">
                                  <label class="label title" >Position Type<font> :</font></label>
                                </div>
                                <div class="col-md-7 col-sm-8 signup-full-column">
                                  <div class="radio radio-info radio-inline">
                                    <input type="radio" name="position" ng-model="PostJobData.position" id="permanent" value="Permanent" >
                                    <label class="radio-inline" for="permanent">Permanent </label>
                                  </div>
                                  <div class="radio radio-info radio-inline">
                                    <input type="radio" name="position" id="locum" ng-model="PostJobData.position" value="Locum" data-bvalidator="required" data-bvalidator-msg="Required.">
                                    <label class="radio-inline" for="locum">Locum</label>
                                  </div>
                                  <label class="error" ng-bind="Error.position"></label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group clearfix">
                                <div class="col-md-4 col-sm-4 label-block">
                                  <label class="label">Start Date<font> :</font></label>
                                </div>
                                <div class="col-md-7 col-sm-8 signup-full-column account-start-date-block">
                                  <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                      <div class="">
                                        <select name="start_month" ng-model="PostJobData.start_month" ng-if="month_name" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
                                          <option value="">Month</option>
                                          <option ng-repeat ="(key,value) in month_name" ng-if="key" value="{{key}}">{{value}}</option>
                                        </select>
                                      </div>
                                      <label class="error" ng-bind="Error.start_month"></label>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                      <div class="">
                                        <select name="start_date" ng-model="PostJobData.start_date" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
                                          <option value="">Date</option>
                                          <?php
                                          for($i=1; $i<=31; $i++)
                                          { ?>
                                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <label class="error" ng-bind="Error.start_date"></label>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                      <div class="">
                                        <select name="start_year" ng-model="PostJobData.start_year" class="form-control" data-bvalidator="required" data-bvalidator-msg="Required">
                                          <option value="">Year</option>
                                          <?php
                                          for($j=date('Y'); $j<=date('Y')+2; $j++)
                                          { ?>
                                          <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <label class="error" ng-bind="Error.start_year"></label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group clearfix">
                                <div class="col-md-4 col-sm-4 label-block compensation-padding">
                                  <label class="label">Compensation<font> :</font> </label>
                                </div>
                                <div class="col-md-7 col-sm-8 signup-full-column">
                                  <select name="compensation" ng-model="PostJobData.compensation" class="form-control"  ng-if="compensation">
                                    <option value="">Select Compensation</option>
                                    <option ng-repeat ="(key,value) in compensation" ng-if="key" value="{{key}}">{{value}}</option>
                                  </select>
                                  <span class ="optional-tag">(optional)</span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group clearfix">
                                <div class="col-md-4 col-sm-4 label-block title">
                                  <label class="required label title" >Call Responsibilities<font> :</font></label>
                                </div>
                                <div class="col-md-7 col-sm-8 signup-full-column">
                                  
                                  <div class="radio radio-info radio-inline">
                                    <input type="radio" name="call_responsibility" id="responsibilities_free" value="Yes" ng-model="PostJobData.call_responsibility" >
                                    <label class="radio-inline" for="responsibilities_free" >Yes </label>
                                  </div>
                                  <div class="radio radio-info radio-inline">
                                    <input type="radio" name="call_responsibility" id="responsibilities_none" ng-model="PostJobData.call_responsibility" value="No" data-bvalidator="required" data-bvalidator-msg="Required.">
                                    <label class="radio-inline" for="responsibilities_none" >No</label>
                                  </div>
                                  <label class="error" ng-bind="Error.call_responsibility"></label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                    <div class="sectionDivide panel" id="panel5">
                      <div class="section-heading" role="tab" id="heading4">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="4" aria-expanded="false" aria-controls="collapse4"><h3><span class="number">5</span>Facility Location</h3>
                      </a>
                    </div>
                    
                    <div id="collapse4" ng-class="{'panel-collapse collapse in' : (ErrorCheck == '1'),'panel-collapse collapse in' : (ErrorCheck == '0')}" role="tabpanel" aria-labelledby="heading4" class="panel-collapse">
                      <section>
                        <div id="search_loader_small" class="search_loader_small" style="width: 100%;height: 100%;display: none;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;opacity: 0.5;">
                          <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label"> Facility Name<font> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <input type="text" name="facility_name"  ng-model="PostJobData.facility_name"  class="form-control" data-bvalidator="required,maxlen[50]"  >
                                <label class="error" ng-bind="Error.facility_name"></label>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label"> Province<font> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <select name="facility_state" ng-model="PostJobData.facility_state" class="form-control"  ng-if="canada_province" data-bvalidator="required"  ng-change="get_city(PostJobData.facility_state)">
                                  <option value="">Select Province</option>
                                  <option ng-repeat ="canada_pro in canada_province" ng-if="canada_pro.id" value="{{canada_pro.province_name}}">{{canada_pro.province_name}}</option>
                                </select>
                                <label class="error" ng-bind="Error.facility_state"></label>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label"> City<font> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <select ui-select2 name="facility_city" ng-model="PostJobData.facility_city" class="form-control"   data-bvalidator="required" ng-change="get_zipcode(PostJobData.facility_city);">
                                  <option value="">Select City</option>
                                  <option ng-repeat ="city_name in CityProvince" ng-if="city_name.id" value="{{city_name.city_name}}">{{city_name.city_name}}</option>
                                </select>
                                <label class="error" ng-bind="Error.facility_city"></label>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label">Street number and name(relevant for the map)<font class="street-number-colon"> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <input name ="facility_address" ng-model="PostJobData.facility_address" class="form-control" data-bvalidator="required"  Placeholder="">
                                <label class="error" ng-bind="Error.facility_address"></label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label">Suite or Office number<font> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <input name ="facility_suite_office" ng-model="PostJobData.facility_suite_office" class="form-control"   Placeholder="">
                                <span class ="optional-tag">(optional)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label">Postal Code<font> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-6 signup-full-column">
                                <!--  <select name="facility_postal_code" ng-model="PostJobData.facility_postal_code" class="form-control"   data-bvalidator="required" data-bvalidator-msg="Postal code Required" >
                                  <option value="">Select Postal code</option>
                                  <option ng-repeat ="postal_code in PostalCode" ng-if="postal_code.id" value="{{postal_code.postal_code}}">{{postal_code.postal_code}}</option>
                                </select> -->
                                <!--
                                <label class="error" ng-bind="Error.facility_postal_code"></label>-->
                                <tags-input  replace-spaces-with-dashes="false" name="facility_postal_code" ng-model="zip_code_data" on-tag-added="tagAdded(zip_code_data)" style="height:35px;"  display-property="postal_code" >
                                <auto-complete source="loadTags(PostJobData.facility_city,$query)" max-results-to-show="1000" select-first-match="false" debounce-delay="0" min-length="1"
                                ></auto-complete>
                                </tags-input>
                                <label style="color:gray;">Postal code should be A1B 2C3</label>
                                <label class="error" ng-if="UpdatePostalCodeErr">{{ UpdatePostalCodeErr}}</label>
                                <label class="error" ng-if="UpdatePostalValidErr">{{ UpdatePostalValidErr}}</label>
                                
                              </div>
                              
                            </div>
                          </div>
                        </div>
                        
                      </section>
                    </div>
                  </div>
                  <div class="sectionDivide panel" id="panel6">
                    <div class="section-heading" role="tab" id="heading5">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="5" aria-expanded="false" aria-controls="collapse5"><h3><span class="number">6</span>Public Transport</h3>
                    </a>
                  </div>
                  <div id="collapse5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading5">
                    <section>
                      <div class="row">
                        <p class="text-center info-text">If available, provide your facility’s closest public transit information.
                        </p>
                        <br>
                        <div class="col-md-12" ng-if="transport_type">
                          <div class="form-group clearfix">
                            <!-- <div class="col-md-4 col-sm-4 label-block">
                              <label class="label"> Transport <font> :</font> </label>
                            </div> -->
                            <div class="col-md-10 col-md-offset-1 col-sm-8 col-xs-12  transportBlock" >
                              <div>
                                <div class="transportBlock-panel clearfix">
                                  <div class="checkBlock">
                                    <label for="transport_metro_subway">
                                      <div class="checkbox-info">
                                        <img src="<?php echo FRONTEND_THEME_URL ?>images/metro.svg"><span>Metro/Subway</span>
                                        <input type="checkbox"  name="transport[]"  id="transport_metro_subway" ng-model="PostJobData.transport_metro_subway" value="{{PostJobData.transport_metro_subway}}">
                                        <span></span>
                                      </div>
                                    </label>
                                  </div>
                                  <div class="form-group" ng-show="PostJobData.transport_metro_subway" ng-if="PostJobData.transport_metro_subway != '0'">
                                    <input type ="text" name="transport_m_s_value" ng-model="PostJobData.transport_m_s_value"  class="form-control"  placeholder="Metro/Subway description">
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="transportBlock-panel clearfix">
                                  <div class="checkBlock">
                                    <label for="transport_bus">
                                      <div class="checkbox-info">
                                        <img src="<?php echo FRONTEND_THEME_URL ?>images/bus.svg"><span>Bus</span>
                                        <input type="checkbox" name="transport[]"  id="transport_bus" value="{{PostJobData.transport_bus}}" ng-model="PostJobData.transport_bus">
                                        <span></span>
                                      </div>
                                    </label>
                                  </div>
                                  <div class="form-group" ng-show="PostJobData.transport_bus" ng-if="PostJobData.transport_bus != '0'">
                                    <input type ="text"  name="transport_bus_value"  ng-model="PostJobData.transport_bus_value"  class="form-control" placeholder="Bus description">
                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="checkBlock">
                                <label for="transport_train">
                                  <div class="checkbox-info">
                                    <img src="<?php echo FRONTEND_THEME_URL ?>images/train.svg"><span>Train</span>
                                    <input type="checkbox" name="transport[]"  id="transport_train" value="{{PostJobData.transport_train}}" ng-model="PostJobData.transport_train"><span></span>
                                  </div>
                                </label>
                              </div>
                              <div class="form-group" ng-show="PostJobData.transport_train" ng-if="PostJobData.transport_train != '0'">
                                <input type ="text" name="transport_train_value"  ng-model="PostJobData.transport_train_value"  class="form-control" placeholder="Train Description">
                              </div>
                              <div class="clearfix"></div>
                              <div class="checkBlock">
                                <label for="transport_other">
                                  <div class="checkbox-info">
                                    <img src="<?php echo FRONTEND_THEME_URL ?>images/transport-other.svg"><span>Other</span>
                                    <input type="checkbox" name="transport[]"  id="transport_other" ng-model="PostJobData.transport_other"  value="{{PostJobData.transport_other}}" placeholder="Other description">
                                    <span></span>
                                  </div>
                                </label>
                              </div>
                              
                              
                              <div class="form-group" ng-show="PostJobData.transport_other" ng-if="PostJobData.transport_other != '0'">
                                <input type ="text" name="transport_other_value"  ng-model="PostJobData.transport_other_value"  class="form-control" >
                              </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group clearfix">
                                  <div class="col-md-4 col-sm-4 label-block">
                                    <label class="required label title" > Parking<font> :</font> </label>
                                  </div>
                                  <div class="col-md-6 col-sm-8 signup-full-column">
                                    <!-- <input type="text" name="paking"  ng-model="PostJobData.paking"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Paking is Required" > -->
                                    
                                    
                                    
                                    
                                    <div class="radio radio-info radio-inline">
                                      <input type="radio" name="parking" id="parking_free" value="Free"  ng-model="PostJobData.parking">
                                      <label class="radio-inline" for="parking_free" >Free </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                      <input type="radio" name="parking" id="parking_paid" value="Paid" ng-model="PostJobData.parking" >
                                      <label class="radio-inline" for="parking_paid">Paid</label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                      <input type="radio" name="parking" id="parking_none" value="None" ng-model="PostJobData.parking" data-bvalidator="required" data-bvalidator-msg="Parking Required.">
                                      <label class="radio-inline" for="parking_none" >None </label>
                                    </div>
                                    
                                    <label class="error" ng-bind="Error.parking"></label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <!--  <label class="error" ng-bind="Error.transport_array"></label> -->
                        </div>
                      </section>
                    </div>
                  </div>
                  <div class="sectionDivide panel" id="panel7">
                    <div class="section-heading" role="tab" id="heading7">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="7" aria-expanded="false" aria-controls="collapse7"><h3><span class="number">7</span> Facility Hours</h3>
                    </a>
                  </div>
                  <div id="collapse7" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading7">
                    <section>
                      <div class="row">
                        <div class="col-md-12" ng-if="PostJobData.facility_hours">
                          
                          <div class="col-md-10 col-sm-10 signup-full-column center-block" style="float:none;">
                            <div class="row dobBlock" ng-repeat="(hours_key,hours_value) in PostJobData.facility_hours" >
                              <div>
                                <div class="col-md-6 col-sm-4 col-xs-4 form-group date" >
                                  <div ng-dropdown-multiselect  options="master_days_name" selected-model="PostJobData.facility_hours[hours_key].master_days_name"   ></div>
                                  
                                </div>
                                
                                <div class="col-md-3 col-sm-4 col-xs-4 form-group month">
                                  <select name="start_time" class="form-control" ng-model="PostJobData.facility_hours[hours_key].start_time" data-bvalidator="required" >
                                    <option value="">Start Time</option>
                                    <option ng-repeat = "val in time_array" value="{{val}}">{{val}}</option>
                                  </select>
                                </div>
                                
                                <div class="col-md-3 col-sm-4 col-xs-4 form-group year">
                                  <select name="end_time" class="form-control" ng-model="PostJobData.facility_hours[hours_key].end_time" data-bvalidator="required">
                                    <option value="">End Time</option>
                                    <option ng-repeat = "val in time_array_eve"  value="{{val}}">{{val}}</option>
                                  </select>
                                  
                                </div>
                                <div class="remove-btn-block" ng-if="$index != '0'">
                                  <a ng-click="delete($index)" class="btn btn-danger btn-xs" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Remove"><i class="fa fa-close"></i></a>
                                </div>
                              </div>
                            </div>
                            <label class="error" ng-if="FacilityHoursErr">{{FacilityHoursErr}}</label>
                            <div class="addMoreWarp">
                              <br>
                              <a href="javascript:void(0);" class="btn btn-success btn-xs" ng-click="addMoreFacilityHours()">
                              <i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
                              
                            </div>
                          </div>
                          
                          
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="sectionDivide panel" id="panel8">
                  <div class="section-heading" role="tab" id="heading6">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="8" aria-expanded="false" aria-controls="collapse8"><h3><span class="number">8</span>Facility Details</h3>
                  </a>
                </div>
                <div id="collapse6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading6">
                  <section>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group clearfix">
                          <div class="col-md-4 col-sm-4 label-block">
                            <label class="label"> Facility Type <font> :</font> </label>
                          </div>
                          <div class="col-md-7 col-sm-8 signup-full-column">
                            <select name="facility_type" ng-model="PostJobData.facility_type" class="form-control"  ng-if="facility_type" data-bvalidator="required" data-bvalidator-msg="Required">
                              <option value="">Select Facility Type</option>
                              <option ng-repeat ="(key,value) in facility_type"  ng-if="key" value="{{key}}">{{value}}</option>
                            </select>
                            <label class="error" ng-bind="Error.facility_type"></label>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group clearfix">
                          <div class="col-md-4 col-sm-4 label-block">
                            <label class="label">EMR<font> :</font> </label>
                          </div>
                          <div class="col-md-7 col-sm-8 signup-full-column">
                            <input type="text" name="emr"  ng-model="PostJobData.emr"  class="form-control" >
                            <span class ="optional-tag">(optional)</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group clearfix">
                          <div class="col-md-4 col-sm-4 label-block">
                            <label class="label">Number of full-time doctors<font> :</font> </label>
                          </div>
                          <div class="col-md-2 col-sm-8 signup-full-column">
                            <input type="text" only-num name="no_of_fulltime"  ng-model="PostJobData.no_of_fulltime"  class="form-control" step="1" data-bvalidator="required" data-bvalidator-msg="Required" min="0" maxlength="6">
                            <label class="error" ng-bind="Error.no_of_fulltime"></label>
                          </div>
                          <div class="clearfix visible-sm"></div>
                          <div class="col-md-3 col-sm-4  no-padding label-block remove-nopadding-right remove-nopadding-left">
                            <label class="label">Part-time doctors<font> :</font> </label>
                          </div>
                          <div class="col-md-2 col-sm-8 signup-full-column">
                            <input type="text"  only-num step="1" name="part_time_doctor" min="0" ng-model="PostJobData.part_time_doctor" maxlength="6"  class="form-control" data-bvalidator="required" data-bvalidator-msg="Required" >
                            <label class="error" ng-bind="Error.part_time_doctor"></label>
                          </div>
                          
                          
                          
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group clearfix">
                          <div class="col-md-4 col-sm-4 label-block">
                            <label class="label">Number of exam rooms<font> :</font> </label>
                          </div>
                          <div class="col-md-2 col-sm-8 signup-full-column">
                            <input type="text" only-num step="1"  name="exam_room"  ng-model="PostJobData.exam_room" maxlength="6" class="form-control" data-bvalidator="required" min="0" data-bvalidator-msg="Required" >
                            <label class="error" ng-bind="Error.exam_room"></label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--  <div class="row">
                      <div class="col-md-12" ng-if="nursing">
                        <div class="form-group clearfix">
                          <div class="col-md-4 col-sm-4 label-block">
                            <label class="label"> Nursing <font> :</font> </label>
                          </div>
                          <div class="col-md-7 col-sm-8 signup-full-column">
                            <div class="row checkbox-group">
                              
                              <div class="col-md-6"  ng-repeat ="nurs in nursing" ng-if="nurs.id" >
                                <div class="checkbox checkbox-info" ng-if="($index + 1) != nursing.length">
                                  
                                  <input type="checkbox"  id="{{'nursing_'+nurs.id}}" name="nursing_array[]"  ng-model="PostJobData.nursing_array[nurs.id]" value="{{nurs.id}}" ng-if="($index + 1) != nursing.length" ng-click="uncheckAll_nursing($index)">
                                  <label for="{{'nursing_'+nurs.id}}">{{nurs.name}}</label>
                                  
                                </div>
                                
                                <div class="checkbox checkbox-info" ng-if="($index + 1) == nursing.length">
                                  
                                  <input type="checkbox"  id="{{'nursing_'+nurs.id}}" name="nursing_array[]"  ng-model="PostJobData.nursing_array[nurs.id]" value="{{nurs.id}}" ng-if="($index + 1) == nursing.length" data-bvalidator="required" data-bvalidator-msg="Required." ng-click="uncheckAll_nursing($index)">
                                  <label for="{{'nursing_'+nurs.id}}">{{nurs.name}}</label>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </div> -->
                  </section>
                </div>
              </div>
              
              
              <!--    <div class="sectionDivide panel" id="panel9">
                <div class="section-heading" role="tab" id="heading9">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="9" aria-expanded="false" aria-controls="collapse9"><h3><span class="number">9</span>Allied Health Professionals</h3>
                </a>
              </div>
              <div id="collapse9" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading9">
                <section>
                  <div class="row">
                    <div class="col-md-12" ng-if="health_professionals">
                      <div class="form-group clearfix">
                        <div class="col-md-4 col-sm-4 label-block">
                          <label class="label"> Allied Health Professionals: </label>
                        </div>
                        <div class="col-md-7 col-sm-8 signup-full-column">
                          <div class="row checkbox-group">
                            <div class="col-md-6" ng-repeat ="health_p in health_professionals" ng-if="health_p.id">
                              <div class="checkbox checkbox-info" ng-if="($index + 1) != health_professionals.length">
                                <input type="checkbox" name="health_array[]" id="{{'health_'+health_p.id}}" ng-model="PostJobData.health_array[health_p.id]" value="{{health_p.id}}" ng-if="($index + 1) != health_professionals.length"  ng-click="uncheckAll_health_pro($index)"> <label for="{{'health_'+health_p.id}}" >{{health_p.name}}</label>
                                
                              </div>
                              <div class="checkbox checkbox-info" ng-if="($index + 1) == health_professionals.length">
                                <input type="checkbox" name="health_array[]" id="{{'health_'+health_p.id}}" ng-model="PostJobData.health_array[health_p.id]" value="{{health_p.id}}" ng-if="($index + 1) == health_professionals.length" data-bvalidator="required" data-bvalidator-msg="Required." ng-click="uncheckAll_health_pro($index)"> <label for="{{'health_'+health_p.id}}" >{{health_p.name}}</label>
                                
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                    -->
                    <div class="sectionDivide panel" id="panel9">
                      <div class="section-heading" role="tab" id="heading10">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="9" aria-expanded="false" aria-controls="collapse9"><h3><span class="number">9</span>Listing Description</h3>
                      </a>
                    </div>
                    <div id="collapse9" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading10">
                      <section>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label">Listing Title<font> :</font> </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <input type="text" maxlength="30" ng-trim="false" name="listing_title" ng-model="PostJobData.listing_title" class="form-control" data-bvalidator="required" data-bvalidator-msg="Title is required" placeholder="Ex. Busy Clinic Downtown (as example)">
                                <span>{{30 - PostJobData.listing_title.length}} Remaining</span>
                                <label class="error" ng-bind="Error.listing_title" ></label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group clearfix">
                              <div class="col-md-4 col-sm-4 label-block">
                                <label class="label"> Listing Description<font> :</font>
                                </label>
                              </div>
                              <div class="col-md-7 col-sm-8 signup-full-column">
                                <textarea  maxlength="5000" ng-trim="false" name="listing_description" ng-model="PostJobData.listing_description" class="form-control" data-bvalidator="required" data-bvalidator-msg="Description is required" rows="7" placeholder="Briefly describe your facility, what you are looking for and the benefits of working here; Radiology available, Specialties available etc. Please NO phone numbers, e-mail addresses or hyperlinks">
                                </textarea>
                                
                                <span ng-init="maxLimitOfListingDescription=5000">{{maxLimitOfListingDescription - PostJobData.listing_description.length}} remaining</span>
                                <label class="error" ng-bind="Error.listing_description"></label>
                              </div>
                            </div>
                          </div>
                          
                        </section>
                      </div>
                    </div>
                    
                    <div class="sectionDivide panel" id="panel10">
                      <div class="section-heading" role="tab" id="heading12">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" class="collapsed" data-target="10" aria-expanded="false" aria-controls="collapse10"><h3><span class="number">10</span>Upload</h3>
                      </a>
                    </div>
                    <div id="collapse10" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading12">
                      <section>
                        <div class="form-section-block">
                          
                          <div class="form-section-block">
                            <h3 class="heading text-center">Upload photos</h3>
                            <div class="uploadPhotoBlock">
                              <div class="" >
                                <div class="col-md-11 picUploadContent" >
                                  <div class="my-drop-zone" nv-file-drop="" uploader="uploader" >
                                    <!-- <h3>Select files</h3> -->
                                    <span class="icon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                                    <span class="text">Click to add file or Drag here</span>
                                    <input type="file" nv-file-select="" uploader="uploader" multiple="" />
                                    <div class="leftSide">
                                      <ul>
                                        <li>Please share <font>5</font> relevant pictures of the your medical facility </li>
                                        <li><span>1: Reception,</span><span> 2: Waiting Room,</span><span> 3: Exam Room</span></li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <div class="text-center photo-link">
                                  See how to take professional looking photos yourself &nbsp;&nbsp;
                                  <div class="text-center">
                                    <a href="{{site_url}}photographer-photo" target="_blank" class="btn btn-theme">Click Here</a>
                                  </div>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              
                              <div class="row" >
                                <div class="col-md-12">                                  
                                  <ul class="uploadPhotoList">
                                    <li ng-repeat="itemImage in imagesArray " class=" itemImage">
                                      <div class="itemImage-warp" >
                                        <img src="<?php echo base_url('assets/uploads/jobs/thumbs'); ?>/{{itemImage.url}}<?php echo "?timestamp=".time(); ?>" width="120" height="65" alt="" class="img-responsive">
                                      </div>
                                      <div class="progress" style="margin-top: 10px;">
                                        <div class="progress-bar" role="progressbar" style="width:100%;"></div>
                                      </div>
                                      <div>
                                        <button ng-click="imageremovefrompreset($index)" title="" class="btn btn-danger btn-xs pull-right"><i class="fa fa-close"></i></button>
                                        <button type="button" class="btn btn-warning btn-xs btn-rotate" ng-click="imagerotate($index,'img',itemImage.url)">
                                          <i class="fa fa-repeat"></i>
                                        </button>
                                        <span class="btn btn-success btn-xs pull-left">
                                          <i class="glyphicon glyphicon-ok"></i> Uploded
                                        </span>
                                      </div>
                                    </li>
                                    <li ng-repeat="item in uploader.queue" class="no-padding-right" >
                                      <div class="itemImage-warp" >
                                      <div class="canvas-warp">
                                        <div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, width: 120 }"></div>
                                      </div>
                                      </div>
                                      <div class="progress" style="margin-top: 10px;">
                                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                      </div>
                                      <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                                      <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                                      
                                      <button type="button" class="btn btn-danger btn-xs pull-right" ng-click="item.remove()">
                                      <i class="fa fa-close"></i>
                                      </button>
                                      <button ng-show="item.isSuccess" type="button" class="btn btn-warning btn-xs btn-rotate" ng-click="imagerotate($index,'canvas',item.file.name)">
                                        <i class="fa fa-repeat"></i>
                                      </button>
                                      <span ng-show="item.isSuccess" class="btn btn-success btn-xs pull-left">
                                      <i class="glyphicon glyphicon-ok"></i> Uploded</span>
                                      
                                    </li>
                                  </ul>
                                  <div class="clearfix"></div>
                                  <div class="rotate-text text-center">You can rotate uploaded image by clicking on rotation button once you update job.</div>
                                </div>
                                <div class="clearfix"></div>
                                <br>
                              </div>
                              <!-- Upload demo area  -->
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                  <div class="alert alert-danger error" style="margin-top: 20px;" ng-if="UpdateMassageErr" ng-hide="UpdateAlertErrorMessage">
                    {{UpdateMassageErr}}
                  </div>
                  
                  <div class="clearfix"></div>
                  <br><br>
                  <div class="text-center" >
                    <button type="submit" name="" value="Register as a physician" class="btn-blue" data-bvalidator="required">
                    Update Job
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <!--  <div class="col-sm-10 col-md-10 center-block upgrade-message " ng-if="PostJobData.job_verify == '0' && PostJobData.status != '4'">
      <div class="table-cell"> Do you want to increase your chances of finding a doctor?<br>Please click here</div>
      <div class="table-cell">
        <a  ng-if="PostJobData.job_verify == '0'" ng-href="{{site_url}}access/{{PostJobData.encrypt_id}}" class="btn btn-warning btn-sm"  data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Upgrade" >Upgrade</a>
      </div>
    </div> -->
    
    <div class="clearfix"></div>
  </div>
  
  
</div>
</div>
</div>
</div>
</div>
</div>

<style type="text/css">
  .itemImage-warp{height: 80px;overflow: hidden;}
  .itemImage-warp img{max-width: 100%;max-height: 100%;margin: 0 auto;}
</style>
<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
  })
</script>
<?php
$this->load->view('include/common_modal_msg');
$this->load->view('include/footer_menu');
?>