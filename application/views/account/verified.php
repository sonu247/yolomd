<?php 
  $this->load->view('include/header_menu');
?>
 <div id="search_loader" class="search_loader" style="width: 100%;height: 80%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left:0;text-aling:center;">
                <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
            </div>
<div class="paypalWarp">

<div class="container">
  <div class="paypal-container">
    <div class="col-md-10 col-md-offset-1">
      <div class="paypalheading">
       <h3 class="text-center">YoloMD Verified Facility:</h3>
       <p class="text-center">These facilities pay us to send a professional photographer to take high quality photos of their facility</p>
        <div class="icon-block">
          <span><i class="fa fa-usd" aria-hidden="true"></i></span>
        </div>
      </div>
     <div class="paypalContent">

     <div class="listContent">
     <ul>
      <li>Receive a <span class="highlight">50% discount</span> for YoIoMD Verified Status when you purchase Priority Placement and YoIoMD Verification photographs together</li>
      <li>
      Purchase professional photography and doctors will be able to see and search for "YoIoMD Verified" facilities on the search results page
      </li>
      <li>See Here! Expanding thumbnail with screen shot of enlarged pins and regional statistics page</li>
      <li>Let us send you one of our professional photographers for a <span class="highlight">one-time fee of $599 </span> to have your facility YoIoMD Verified!</li>
    </ul>
    </div>

    <div class="priceCheck verifiedCheck clearfix ">
      <ul>
        <li class="verified-facility">
          <div class="checkbox checkbox-info">
          <input type="checkbox" name="term" id="verified">
          <label for="verified" class="text-capitalize">&nbsp;&nbsp;<b>Yes, I want YoloMD Verified Facility</b></label>
        </div>
        </li>       
      </ul>
    </div>

    <hr>

    <div class="clearfix">
      <div class="pull-right text-right">
        <h3>Total : $1000.00</h3>
        <a href="#" class="btn btn-blue">PostJob for Free</a>   
        <a href="#" class="btn btn-blue">Pay Now</a>
      </div>
    </div>

    </div>

    </div>

    <div class="clearfix"></div>
    <br>

  </div>
</div>

</div>

<script type="text/javascript">
setTimeout(function() {

  var $tdArr = $('.listContent ul li');
  function bold(i){
      if(i == $tdArr.length){
          return;
      }
      $tdArr.eq(i).addClass('inner')   
      setTimeout(function() { bold(i+1) },1200);
  }
  bold(0);

}, 500);

</script>



<?php 
  $this->load->view('include/footer_menu');
?>