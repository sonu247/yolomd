<div ng-controller="SidebarMenuDashboard">
	<div class="dashboard-Left-toggle">
		<a href="#" ng-click="dashboardLeftToggle()"><i class="{{dashboardFaClass}}" aria-hidden="true"></i> Dashboard Menu</a>
	</div>
	<div class="dashboard-Left {{ dashboardToggleClass }}">
	 	<div class="account-logo text-center">
		 	<span class="image-block"><img src="<?php echo FRONTEND_THEME_URL ?>/images/user-vector.svg" class="img-responsive"></span>
		 	<div class="user-name text-center"><?php if($medicaluser_info=get_medicaluser_info()) echo $medicaluser_info->user_first_name.' '.$medicaluser_info->user_last_name ; ?></div>
	 	</div>
	 	<div class="menu">
		 	<ul>
				<li ><a ng-class="{'active' : (state_name == 'MedicalDashboard') , '' : (state_name != 'MedicalDashboard')}" href="<?php echo base_url('medical-dashboard');?>" ng-click="dashboardLeftToggle()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-dashboard.svg"> My Dashboard</a></li>

				<li ><a ng-class="{'active' : (state_name == 'MedicalMessage') , '' : (state_name != '')}" href="<?php echo base_url('medical-message');?>" ng-click="dashboardLeftToggle()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/messages.svg" class="fa"> My Messages <span  ng-controller="MessageCount" ><span  ng-if="msg_count != '0'" class="badge" ng-bind-html="msg_count" ng-class="{'plus_tag' : (msg_count >  '100')}"></span></span></a></li>

				<li ><a ng-class="{'active' : ((state_name == 'JobListing') || (state_name == 'PostJobUpdate'))}" href="<?php echo base_url('job-listing');?>" ng-click="dashboardLeftToggle()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.svg"> My Job Listings</a></li>
				<!-- 	<li ><a ng-class="{'active' : ((state_name == 'StatisticsJob') || (state_name == 'StatisticsJob'))}" href="<?php //echo base_url('statistics-jobs');?>" ng-click="dashboardLeftToggle()"><img src="<?php //echo FRONTEND_THEME_URL ?>/images/icons/line-chart.svg"> Job Statistics </a></li> -->
				
				<!--<li ><a ng-class="{'active' : ((state_name == 'Receipts') || (state_name == 'ReceiptsDownload'))}" href="<?php //echo base_url('receipts');?>" ng-click="dashboardLeftToggle()"><img src="<?php //echo FRONTEND_THEME_URL ?>/images/receipt.svg" class="fa">Receipts</a></li>-->

				<li ><a ng-class="{'active' : (state_name == 'MedicalProfile') , '' : (state_name != 'MedicalProfile')}" href="<?php echo base_url('medical-profile');?>" ng-click="dashboardLeftToggle()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/profile.svg" class="fa">My Profile</a></li>

				<li ><a ng-class="{'active' : (state_name == 'ChangePassword') , '' : (state_name != 'ChangePassword')}" href="<?php echo base_url('medical-change-password');?>" ng-click="dashboardLeftToggle()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/change-password.svg" class="fa">Change Password</a></li>

				<li><a href="" ng-controller="logoutCtrl" ng-click="logout()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/logout.svg" class="fa">Log Out </a></li>
		 	</ul>
	 	</div>
	</div>
</div>