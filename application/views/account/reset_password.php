<div class="container two-col-holder no-padding">
	<!--Login container start here-->
	<div class="container login-container-warp"><br><br>
	<div class="col-md-12 col-lg-12">
	<div class="login-page-warp">
	 <div class="col-md-7 col-lg-7 login-left-warp">
	 	<h3>Almost There!</h3>
	 	<p>Create a new account or sign in to your existing account, to join our Prep Insider Rewards Program.</p>
	 	<h3>Prep Insider Benefits Include:</h3>
	 	<ul>
	 		<li>Special Promotions</li>
	 		<li>Exclusive Discounts</li>
	 		<li>Insider Info</li>
	 	</ul>
	 	<b>Get started on being rewarded for future purchases!</b>
	 </div>
	 <div class="col-md-5 col-lg-5" >
	 	<div class="login-holder">
	 	<div class="panel panel-default">

		  <div class="panel-heading text-center"><h4><b>Reset Password</b></h4></div>
		  <div class="panel-body">
		  <!-- <div class="col-md-6 login-logo">
		  	<img src="images/corporate.png" width="60%" class="img-center">
		  </div> -->
		   <div class="col-md-12">
		   <?php echo msg_alert_front(); ?>
		   	<form class="form-signin" method="post" role="form" action="<?php echo current_url(); ?>" id="form_valid">
		   			
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
				<input type="password" name="password" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Password" aria-describedby="basic-addon1"   data-bvalidator-msg="" data-bvalidator="required,maxlength[14],minlength[6]" autofocus>
			</div>
				  <?php echo form_error('password') ?>
		    <div class="input-group">
			  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span> <input type="password" name="confpassword" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Confirm Password" aria-describedby="basic-addon1"   data-bvalidator-msg="" data-bvalidator="required,maxlength[14],minlength[6]" autofocus>
			</div>	  
			<?php echo form_error('confpassword') ?>
            <button class="btn btn-primary pull-left load_loader " type="submit">Submit</button>&nbsp;&nbsp;
		  <label></label>
	      </form>
		   </div>		   
		  </div>
		</div>      		
	 	</div>
	 </div>
	<div class="clearfix"></div>
	
	 <div class="create-store create-store-login">
	   <form action="<?php echo base_url().'store/signup' ?>" method="get" >
             <div class="row">                 
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 no-padding create-block">
                  <h4>Create your store today!</h4>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 create-block">
                <div class="btn-group" role="group" style="width:100%;">
                    <!-- <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" name="store_category" >
                      Select Your Category
                      <span class="caret"></span>
                    </button> -->

                 <!--    <ul class="dropdown-menu" role="menu"> -->
                 <select name="" class="form-control">
                  <option value="sports-teams-leagues">Sports, Teams, Leagues</option>
                    <option value="churches-ministries-camps">Churches, Ministries, Camps</option>
                    <option value="foundations-non-profits">Foundations, Non-Profits</option>
                    <option value="schools-colleges">Schools, Colleges</option>
                    <option value="companies-corporations">Companies, Corporations</option>
                    <option value="clubs-groups">Clubs, Groups</option>
                    <option value="law-enforcement-fire-stations">Law Enforcement, Fire Stations</option>
                    <option value="medical-and-health-care">Medical and Health Care</option>
                    <option value="other">Other</option>
                                          <!-- </ul> -->
                    </select>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 create-block">
                 <input type="text" name="store_name" class="form-control" placeholder="Your Store Name">
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 create-block">
                 <button class="btn  btn-try" type="submit">TRY IT NOW <i class="fa fa-caret-right"></i></button>
                </div>
             </div>
             </form>
          </div>
     </div>
	 <br>
	 </div><br>
    </div>
	<!--login container end-->
</div>
