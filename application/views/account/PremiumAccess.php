<?php 
  $this->load->view('include/header_menu');
?>
<div class="content-container two-column-warp ng-cloak">
<div class="formPage-section">

  <div id="search_loader" class="search_loader" style="width: 100%;height: 100%;display: block;background-color: rgba(255, 255, 255, 0.94);position: absolute;z-index: 100;left: 0;text-aling:center;">
    <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif">
   </div>

  <div class="col-md-7 col-sm-7 full-height left-side post-job-warp" style="{{style_container}}" >
  <?php echo msg_alert_front(); ?>
  <div class="themeform" >
      
      <div ng-switch on="premiumjob"   class="formsection">   
      <section ng-switch-when="1" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
      <div class="col-md-12">
      <div class="premium-price clearfix col-md-11 center-block">
      <h3 class="heading text-center">YoloMD Premium Access</h3>
      <div ng-if="city_subscription">
        <h4><i class="fa fa-usd" aria-hidden="true"></i> &nbsp;Paid Services <small>(cites registered under premium access)</small></h4>
        
        <ul class="paid-city">
         <li ng-repeat="city_content in city_subscription" >
            <div>{{city_content.city_name}}</div>
         </li>
        </ul>
       </div>
        <div class="clearfix"></div>
        <br><br>
        <h4><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i> &nbsp;Select the region where you would like to Priority Place your Job</h4>
        <ul>
          <li ng-repeat="city_content in CityData" ng-if="CityData">
            <div class="checkbox checkbox-info">
             <input type="checkbox" name="placement_array[]" id="{{city_content.id}}" ng-model="PostData.placement_array[city_content.id]" value="{{city_content.id}}" ng-click="nextStep()">
             <label for="{{city_content.id}}">{{city_content.city_name}} <span class="price"><font>+</font>{{city_content.price | currency}}</span></label>
            </div>
            </li>
          </ul>
         <div class="clearfix"></div>
      <br/>

      <p style="font-size:16px;">Type and Select your city</p>
       <div > 
        <tags-input  ng-model="PostData.selectedTags"  ng-keydown="nextStep();" ng-click="nextStep();"  add-from-autocomplete-only="true" display-property="city_name"  placeholder="Type and Select your city">
          <auto-complete source="loadTags(jobId,$query)" ng-click="nextStep();" max-results-to-show="20" select-first-match="false" debounce-delay="500" min-length="1" load-on-focus="false"
                     load-on-empty="true"
                         ></auto-complete>
        </tags-input>
       </div> 
      <!-- <div ng-controller="MainCtrl"    >
        <tags-input ng-model="tags" display-property="city_name">
          <auto-complete source="loadTags(jobId,query)" 
                         match-class="{even: $index % 2 == 0, odd: $index % 2 != 0}"></auto-complete>
        </tags-input>
      </div> -->

     <!--  <select ng-change="nextStep()" style="width: 100%;" ui-select2="select2Options" multiple ng-model="PostData.selectedTags">
          <option ng-repeat="tag in tags" value="{{tag.id}}">{{tag.city_name}}</option>
      </select> -->
      <br />



        <div class="stepToggle-btn pull-right">
          <span class="">
           <button class="btn up" data-toggle="modal" data-target="#paymentoverviewModal" ng-click="getpaydata()" ng-if="gross_total > 0  && yolomdservicetaken==1">Next  <i class="fa fa-angle-right" aria-hidden="true"></i></button>

           <button ng-if="yolomdservicetaken == 0" ng-click="nextStepPremium()" class="btn down" >Next <i class="fa fa-angle-right" aria-hidden="true"></i>
           </button>
            <a ng-if="gross_total < 0.1  && yolomdservicetaken==1" href="job-preview/{{encrypt_id}}" class="btn up" ng-if="gross_total == ''">Next <i class="fa fa-angle-right" aria-hidden="true"></i></a>
        </span>
        </div>
        </div>
      </div>
      </section>
     
 
    
      <section ng-switch-when="2" class="form-section {{animationClass}} wow" ng-class="{{animationClass}}">
      <div class="premium-price clearfix col-md-10 col-md-offset-1">
      <div class="listContent">
       <h3 class="heading text-center"> YoloMD Professional Photography Services</h3>
      </div>
      <div class="verifiedCheck clearfix premium-price" >
        <ul>
          <li class="verified-facility" style="width:100%;">
            <div class="checkbox checkbox-info">
            <input type="checkbox" name="term" id="verified"  name="yolomd_verify" id="verified" ng-model="PostData.yolomd_verify" ng-click="nextStep()">
            <label for="verified" class="text-capitalize" ng-if="PhotographyFee.option_value != '' ">&nbsp;&nbsp;<font style="color: #ff7902;">+</font><b style="color: #ff7902;">{{PhotographyFee.option_value | currency}}</b> Add YoloMD Verified Facility</label>
          </div>
          </li>       
         </ul>
        </div>
      <div class="clearfix"></div>    
        <div class="stepToggle-btn pull-right">
          <button class="btn up"  ng-click="previousStepPremium()">
          <i class="fa fa-angle-left" aria-hidden="true"></i> Back
          </button>
           <button class="btn up" data-toggle="modal" data-target="#paymentoverviewModal" ng-click="getpaydata()" ng-if="gross_total > 0">Next <i class="fa fa-angle-right" aria-hidden="true"></i></button>
         <button ng-click="check_post_status_redirect()"  class="btn up" ng-if="gross_total == ''">Next <i class="fa fa-angle-right" aria-hidden="true"></i></button>
        
         
      </div>
      </div>
      </section>
    </div>
  <!--END sECTION-->
  </div>
  </div>
  <div class="process-bar col-md-7 col-sm-7 col-xs-12" >
  

    <div class="stepredirect clearfix pull-right" style="margin-bottom:0px;">
        
          <div id="search_loader_small" class="search_loader_small" style="">
          <img src="<?php echo FRONTEND_THEME_URL ?>images/loading-new.gif" style="width:40px;">
         </div>
        
          <h3 ng-if="gross_total > 0" class="grossTotal pull-left">
         {{gross_total | currency}}</h3>
       </div>
   </div>

  <div class="col-md-5 col-sm-5 register-right-section page-container">
      <div ng-if="premiumjob=='1'">
       <div class="col-md-10 col-md-offset-1">
        <div class="listContent" ng-if="PriorityPlacement.option_value != ''" ng-bind-html="PriorityPlacement.option_value">
        </div>
       </div>
      </div>
      <div ng-if="premiumjob=='2'">
       <div class="col-md-10 col-md-offset-1">
        <div class="listContent">
         <div ng-if="VerifiedFacility.option_value != '' " ng-bind-html="VerifiedFacility.option_value">
         </div>
        </div>
       </div>
      </div>
   </div>
  <div class="clearfix"></div>
 </div>
</div>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>
<script type="text/javascript">
 wow = new WOW(
    {
      boxClass:     'wow',    
      animateClass: 'animated', 
      offset:       200,         
      mobile:       true,       
      live:         true        
    }
  )
  wow.init();
  //event.stopPropagation();
</script>
<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  id="paymentoverviewModal">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
       <div class="themeform">
        <div class="col-md-12 ">
        <div class="placement-cartHeading">
          <h3 class="">{{listing_title}}</h3>
        </div>
        <div class="placement-cart">
            <div  ng-if="placement_city" class="placmentCartPrice">
            <div class="placmentCartCity">
              <div>
                <table class="table table-striped cityTable" >
                  <tr ng-repeat="city_content in placement_city">
                    <td class="text-right"><label>{{city_content.city_name}} :</label></td>    
                    <td class="text-right"><span>
                       + {{city_content.city_placement_charge | currency}}
                    </span></td>
                  </tr>
                </table>
                </div>
            </div>

            <div ng-if="yolomd_verify_fee" class="verifyFee">
               <table class="table table-striped">
                <tr>
                  <td class="text-right">
                   <label>Yolomd Verify Fee : </label>
                  </td>
                  <td> + 
                      {{yolomd_verify_fee | currency}}
                  </td>
                </tr>
               </table>
               </div>

               <div ng-if="gross_total" class="discount">
               <table class="table table-striped" >
                <tr >
                  <td class="text-right">
                    <label>Amount To Pay : </label>
                  </td>
                  <td>
                    {{gross_total | currency}}
                  </td>
                </tr>
               </table>
               </div>
             </div>
            </div>
        <div class="paybuttonBlock text-right">
        <div class="block pull-left">
          <img src="<?php echo FRONTEND_THEME_URL ?>images/paypal-logo.svg" class="img-responsive">
        </div>
        <div class="block pull-right">
          <form class="paypal" action="{{actionUrl}}" method="post" >
          <!-- <input type="hidden" name="cmd" value="_xclick" />
          <input type="hidden" name="no_note" value="1" />
          <input type="hidden" name="lc" value="UK" />
          <input type="hidden" name="currency_code" value="USD" />
          <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
          <input name="shipping" type="hidden" value="0.00"> -->
          <input type="submit" name="submit" value="Confirm Payment "  class="btn btn-pay" />
        </form>
        </div>
        <div class="clearfix"></div>
      </div>
         <button class="button"   ng-click="close_preview(encrypt_id)" style="margin-top:20px;">
         Skip <i class="fa fa-angle-right" aria-hidden="true"></i>
         </button>

        <div class="clearfix"></div>
        </div>
        </div>
        <div class="clearfix"></div>
         <div class="block pull-right">
        
         

         
         </div>
       </div>
      </div>
    </div>
  </div>
</div>


<style type="text/css" media="screen">
body{overflow:hidden; }
  .select2 > .select2-choice.ui-select-match {
            height: 29px;
        }
        .selectize-control > .selectize-dropdown {
            top: 36px;
        }
        .select-box {
          background: #fff;
          position: relative;
          z-index: 1;
        }
        .alert-info.positioned {
          margin-top: 1em;
          position: relative;
          z-index: 10000; /* The select2 dropdown has a z-index of 9999 */
        }
.search_loader_small{
    width: 205px;
    height: 55px;
    display: none;
    position: absolute;
    z-index: 100;
    right: 0px;
    text-align: center;
    top: 1px;
    background: rgb(238,238,238);
    background: -moz-linear-gradient(top, rgba(238,238,238,1) 0%, rgba(204,204,204,1) 100%);
    background: -webkit-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%);
    background: linear-gradient(to bottom, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 );
}
</style>








