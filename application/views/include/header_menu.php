<header id="mobilelook" ng-class="{'fixed-header fix-head home_class':(headerClass == 'Home'),'fixed-header inner-header fix-head':(headerClass != 'Home'),'inner-header fix-head job-page-head':(headerClass == 'jobs' || headerClass == 'JobDetail' || headerClass == 'JobPreview')}"  class="<?php echo medicaluser_logged_in()==1?'medical-header logged-in-class':(physician_logged_in()==1?'physician-header logged-in-class':'normal-header') ?>">
<!-- 'inner-header fix-head':(headerClass == 'FAQ' || headerClass == 'ContactUs' || headerClass == 'PrivatePolicy' || headerClass == 'TermsofUse') -->
  <div class="logo pull-left">
    <a href="<?php echo base_url();?>" ><img src="<?php echo FRONTEND_THEME_URL ?>images/YoloMD_logo-2.svg" class="img-responsive"></a>
  </div>
  <button class="btn btn-theme header-toggle-btn">
  <i class="fa fa-bars" aria-hidden="true"></i>
  </button>
  <?php if(medicaluser_logged_in()==1){
    $count_job = '';
    $id = '';
    $id = medicaluser_id();
    $count_job = all_row_count('post_jobs',array('user_id'=> $id,'status !=' =>4)); ?>
    <div class="navSide-bar">
      <span class="close-btn"></span>
      <div class="navigation pull-left">
        <ul>
          <li><a href="jobs/" class="" target="_self" ng-class="{'active':(headerClass == 'jobs') || (headerClass == 'detail')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/search-glass.svg"></i> Search Jobs</a></li>
          <li><a href="faq" class="" ng-class="{'active':(headerClass == 'FAQ')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/faq.svg"></i> FAQ</a></li>
          <li><a href="contact-us" class="" ng-class="{'active':(headerClass == 'ContactUs')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/cell-phone.svg"></i> Contact Us</a></li>
        </ul>
      </div>
      <div class="login-nav pull-right" >
        <ul class="">

          <li class="dropdown right_side_menu">
            <div class="login-icon-button">
              <a href="#" class="dropdown-toggle icon user-login-icon " data-toggle="dropdown" >
              Hello <?php if($medicaluser_info=get_medicaluser_info()){
              echo $medicaluser_info->user_first_name; } ?> <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu" data-dropdown-in="fadeInUp" data-dropdown-out="fadeOutDown">
                <li ng-class="{'active' : (state_name == 'MedicalDashboard') , '' : (state_name != 'MedicalDashboard')}"><a href="<?php echo base_url('medical-dashboard');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-dashboard.svg" class="fa"> My Dashboard</a></li>
                <li ng-class="{'active' : (state_name == '') , '' : (state_name != '')}"><a href="<?php echo base_url('medical-message');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/messages.svg" class="fa"> My Messages <!-- <span  ng-controller="MessageCount"><span  ng-if="msg_count != '0'" class="badge1" ng-class="{'plus_tag' : (msg_count >  '100')}" ng-bind-html="msg_count" ></span></span> --></a></li>
                <li ng-class="{'active' : ((state_name == 'JobListing') || (state_name == 'PostJobUpdate'))}"><a href="<?php echo base_url('job-listing');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-job-listing.svg" class="fa"> My Job Listings</a></li>
                <li ng-class="{'active' : (state_name == 'MedicalProfile') , '' : (state_name != 'MedicalProfile')}"><a href="<?php echo base_url('medical-profile');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/profile.svg" class="fa">My Profile</a></li>
                <li ng-class="{'active' : (state_name == 'ChangePassword') , '' : (state_name != 'ChangePassword')}"><a href="<?php echo base_url('medical-change-password');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/change-password.svg" class="fa">Change Password</a></li>
                <li><a href="" ng-controller="logoutCtrl" ng-click="logout()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/logout.svg" class="fa">Log Out </a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
        <?php
          if(!empty($count_job) &&  $count_job > 0)
          {?>
         <!--  <li class="right_side_menu"> -->
            <a href="<?php echo base_url('copy'); ?>" class="btn-red post-icon icon">Post A Job</a><!-- </li> -->
          <?php }
          else {?>
          <!-- <li class="right_side_menu"> -->
            <a href="<?php echo base_url('post-job'); ?>" class="btn-red post-icon icon">Post A Job</a>
          <!-- </li> -->
        <?php } ?>
   
  <?php } 
  else if(physician_logged_in()==1){ ?>
    <div class="navSide-bar">
      <span class="close-btn"></span>
      <div class="navigation pull-left">
        <ul>
          <li><a href="jobs/" class="" target="_self" ng-class="{'active':(headerClass == 'jobs') || (headerClass == 'detail')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/search-glass.svg"></i> Search Jobs</a></li>
          <li><a href="faq" class="" ng-class="{'active':(headerClass == 'FAQ')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/faq.svg"></i> FAQ</a></li>
          <li><a href="contact-us" class="" ng-class="{'active':(headerClass == 'ContactUs')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/cell-phone.svg"></i> Contact Us</a></li>
        </ul>
      </div>
      <div class="login-nav pull-right" >
        <ul class="">
          <li class="dropdown">
            <div class="login-icon-button">
              <a href="#" class="dropdown-toggle login-icon icon user-login-icon" data-toggle="dropdown" >Hello <?php if($physicain_info=physicain_name()){ echo $physicain_info; } ?> <span class="caret"></span></a>
              
              <ul class="dropdown-menu " role="menu" data-dropdown-in="fadeInUp" data-dropdown-out="fadeOutDown">
                <li><a ng-class="{'active' : (state_name == 'PhysicianMessage') , '' : (state_name != '')}" href="<?php echo base_url('physician-message');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/large/messages.svg" class="fa" > My Messages </a></li>
                <li><a ng-class="{'active' : (state_name == 'PhysicianFavorite') , '' : (state_name != '')}" href="<?php echo base_url('physician-favorite');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/my-favorite.svg" class="fa"> My Favourites</a></li>
                <li><a ng-class="{'active' : (state_name == 'PhysicianSaveSearch') , '' : (state_name != '')}" href="<?php echo base_url('save-search');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/saved-search.svg" class="fa"> Saved Searches</a></li>
                <li><a ng-class="{'active' : (state_name == 'PhysicianProfile') , '' : (state_name != '')}" href="<?php echo base_url('physician-profile');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/profile.svg" class="fa"> Profile</a></li>
                <li><a ng-class="{'active' : (state_name == 'PhysicianChangePassword') , '' : (state_name != '')}" href="<?php echo base_url('physician-change-password');?>"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/change-password.svg" class="fa">Change Password</a></li>
                <li><a ng-class="{'active' : (state_name == 'MedicalMessage') , '' : (state_name != '')}" href="" ng-controller="logoutCtrl" ng-click="logout()"><img src="<?php echo FRONTEND_THEME_URL ?>/images/icons/logout.svg" class="fa">Log Out </a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  <?php }
  else{ ?>
    <div class="navSide-bar">
      <span class="close-btn"></span>
      <div class="navigation pull-left">
        <ul>
          <li><a href="jobs/" class="" target="_self" ng-class="{'active':(headerClass == 'jobs') || (headerClass == 'detail')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/search-glass.svg"></i> Search Jobs</a></li>
          <li><a href="faq" class="" ng-class="{'active':(headerClass == 'FAQ')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/faq.svg"></i> FAQ</a></li>
          <li><a href="contact-us" class="" ng-class="{'active':(headerClass == 'ContactUs')}"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/cell-phone.svg"></i> Contact Us</a></li>
        </ul>
      </div>
      <div class="login-nav pull-right" >
        <ul class="">
          <li><a href="#" class="login-icon icon logout-hide"  data-toggle="modal" data-target="#signupModal" ng-click="resetVarible()"><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/login-lock.svg"></i> Login</a></li>
          <li><a class="signup-icon icon" ng-class="{'active':(headerClass == 'Signup' || headerClass == 'PhysicianSignup' || headerClass == 'MedicalFacilitySignup')}" href="<?php echo base_url('signup');?>" ><i class="media-view"><img src="<?php echo FRONTEND_THEME_URL ?>/images/sign-up-user-icon.svg"></i> Signup</a></li>
        </ul>

      </div>
    </div>
      <a class="btn-red post-icon icon" data-toggle="modal" data-target="#postJObMsg" ng-controller="userLogin" style="cursor:pointer;">Post A Job</a>
  <?php } ?>
  <div ng-controller="GoTop">
    <div class="top-move-btn" ng-click="go_top()" data-toggle="tooltip" tooltip-placement="top" uib-tooltip="Back to top">
      <button class="btn btn-theme" >
      <i class="fa fa-angle-up" aria-hidden="true"></i>
      </button>
    </div>
  </div>
</header>
<script>
  $('.header-toggle-btn').click(function() {
    $('#mobilelook').addClass('view');
  });
  $('.close-btn').click(function() {
    $('#mobilelook').removeClass('view');
  });
</script>
<?php $this->load->view('auth/login'); ?>
<script type="text/javascript">
  jQuery(window).on("resize", function() {
    var winWidthnew = $(window).width();
    if(winWidthnew > 800){
      $('.login-nav ul li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
      });
    }
  }).resize();
</script>

<!-- Message Modal -->
<div class="modal fade custom-modal"  ng-controller="userLogin" id="postJObMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog login-dialog" role="document">
    <div class="modal-content" style="min-height:30px!important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        
      </div>
      <div class="modal-body">
        
        <div class="login-error-box">
          <h2 class="text-center"><span class="icon"><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i></span> Please login as a Medical Facility to post a job.</h2>
        </div>
        <div  class="text-center">
          <a href="#" class="btn btn-signup "  data-toggle="modal" data-target="#signupModal" ng-click="resetVaribleJob(1)">Login</a>
          <!--  <a ng-click="closepopup1()" href="<?php //echo base_url('signup');?>" class="btn btn-signup ">Sign Up</a> -->
        </div>
        <div class="social-login">
          <h4 class="signup-line">Don’t have an account?</h4>
          <h4 class="text-center">
          Please click here to <a ng-click="closepopup1()" href="<?php echo base_url('medical-facility-signup'); ?>" class="">Sign Up</a>
          </h4>
          <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        
      </div>
    </div>
  </div>
</div>
