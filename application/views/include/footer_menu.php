<!-- <div class="map-search">
  <div class="container">
    <div class="text">
      <h1>find job by your area ,by your specialty ... <a href="#">Find It Now >></a></h1>
    </div>
  </div>
  <div class="overlay"></div>
</div> -->
<footer class="footer" >
  <div class="container">
      <div class="footer-ribbon">
        <span><a ng-href="{{site_url}}contact-us">Get in Touch</a></span>
      </div>
    <div class="col-xs-12 col-sm-4 col-md-4">
      <?php
      $get_result = get_particular_option();
      ?>
      <?php if(!empty($get_result)){
      if(!empty($get_result[24]->option_value)) {
      ?>
      <!-- <h3><?php echo $get_result[24]->option_value; ?></h3> -->
      <?php } } ?>
      <?php if(!empty($get_result)){
      if(!empty($get_result[25]->option_value)) {
      ?>
      <p class="footer-about-text"><?php if(!empty($get_result[25]->option_value)) echo $get_result[25]->option_value; ?> &nbsp;<a ng-href="{{site_url}}aboutus"  class="read-more">Read More...</a></p>
      <?php } } ?>

        <?php  if(!empty($get_result)){ ?>
        <div class="">
          <div class="social-warp">
            <ul>
              <?php if(!empty($get_result[2]->option_value)) { ?>
              <li><a href="<?php if(!empty($get_result[2]->option_value)) echo $get_result[2]->option_value; ?>" class="facebook" title="facebook"></a></li>
              <?php } ?>
              <?php if(!empty($get_result[10]->option_value)) { ?>
              <li><a href="<?php if(!empty($get_result[10]->option_value)) echo $get_result[10]->option_value; ?>" class="snap-chat" title="snapchat"></a></li>
              <?php } ?>
              <?php if(!empty($get_result[9]->option_value)) { ?>
              <li><a href="<?php if(!empty($get_result[9]->option_value)) echo $get_result[9]->option_value; ?>" class="insta" title="instagram"></a></li>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
        </div>        
      </div>
      
        <?php $get_menu10 = get_particular_menu(10);
        if(!empty($get_menu10))
        {
        ?>
        <div class="col-xs-6 col-sm-2 col-md-2 col-md-offset-1 footer-links">
          
          <h3><?php if(!empty($get_menu10->name)) echo $get_menu10->name; ?></h3>
          <ul>
            <?php $get_submenu10 = get_submenu(10);
            if(!empty($get_submenu10))
            {
            foreach ($get_submenu10 as $get_submenu10)
            {
            if(!empty($get_submenu10->name))
            {
            if($get_submenu10->name == 'Login' || $get_submenu10->name == 'login')
            {
            if(physician_logged_in())
            {
            ?>
            <li><a href="physician-profile">Account</a></li>
            <?php }elseif(medicaluser_logged_in()){ ?>
            <li><a href="medical-profile">Account</a></li>
            <?php }else{ ?>
            <li><a data-toggle="modal" data-target="#signupModal" href="<?php echo site_url();if(!empty($get_submenu10->link)) echo $get_submenu10->link; ?>"><?php if(!empty($get_submenu10->name)) echo $get_submenu10->name; ?></a></li>
            
            <?php
            }
            }elseif($get_submenu10->name == 'Sign Up'){
            if(physician_logged_in() || medicaluser_logged_in()){
            ?>
            
            <?php
            }else{?>
            <li><a href="<?php echo site_url();if(!empty($get_submenu10->link)) echo $get_submenu10->link; ?>"><?php if(!empty($get_submenu10->name)) echo $get_submenu10->name; ?></a></li>
            <?php }
            }
            elseif($get_submenu10->name == 'Post a Job'){
              if (physician_logged_in()==1) {

              }
              elseif(medicaluser_logged_in()==1){
                $id = medicaluser_id();
                $count_job = all_row_count('post_jobs',array('user_id'=> $id,'status !=' =>4));
                if(!empty($count_job) &&  $count_job > 0)
                {?>
                  <li><a href="<?php echo base_url('copy'); ?>">Post A Job</a></li>
                <?php }
                elseif(empty($count_job)){?>
                  <li><a href="<?php echo base_url('post-job'); ?>">Post A Job</a></li>
                <?php }
                else{ ?>
                  <li><a ng-controller="userLogin" data-toggle="modal" data-target="#postJObMsg" style="cursor:pointer;">Post A Job</a></li>
                <?php }
              }
              else{ ?>
                <li><a data-toggle="modal" data-target="#postJObMsg" ng-controller="userLogin" style="cursor:pointer;">Post A Job</a></li>
                <!-- <li><a data-toggle="modal"  ng-controller="userLogin" data-target="#postJObMsg" style="cursor:pointer;">Post A Job</a></li> -->
              <?php }
            }

            else{
            ?>
            <li><a href="<?php echo site_url();if(!empty($get_submenu10->link)) echo $get_submenu10->link; ?>"><?php if(!empty($get_submenu10->name)) echo $get_submenu10->name; ?></a></li>
            <?php
            }
            }
            }
            }
            ?>
            
          </ul>
        </div>
        <?php } ?>
        <?php $get_menu11 = get_particular_menu(11);
        if(!empty($get_menu11))
        {
        ?>
        <div class="col-xs-6 col-sm-2 col-md-2 footer-links"  ng-controller="GetSearch">
          
          <h3><?php if(!empty($get_menu11->name)) echo $get_menu11->name; ?></h3>
          <ul>
            <?php $get_submenu11 = get_submenu(11);
            if(!empty($get_submenu11))
            {
            foreach ($get_submenu11 as $get_submenu11)
            {
            if(!empty($get_submenu11->name))
            {
            ?>
            <li data-ng-click="get_id_save_search('<?php echo $get_submenu11->name; ?>','')" ><a >
            <?php if(!empty($get_submenu11->name)) echo $get_submenu11->name; ?></a></li>
            <?php
            }
            }
            }
            ?>
          </ul>
        </div>
        <?php } ?>
      
     <div class="col-xs-12 col-sm-4 col-md-3 no-padding-right">
      <h3>Contact Us</h3>
      <?php if(!empty($get_result)){ ?>
      <address>
        <?php if(!empty($get_result[20]->option_value)) { ?>
        <div><span>Phone Number : </span><?php  echo $get_result[20]->option_value; ?></div>
        <?php } ?>
        <?php if(!empty($get_result[26]->option_value)) { ?>
        <div><span>Alternative  Number : </span><?php if(!empty($get_result[9]->option_value)) echo $get_result[26]->option_value; ?></div>
        <?php } ?>
        <!--  <div><span>CA: </span>+1 999-000-8765</div> -->
        
      </address>
      <?php } ?>
      <div class="clearfix"></div>
    <!--     </div>
    <div class="col-md-5 text-center"> -->
      <?php  if(!empty($get_result)){
      if(!empty($get_result[18]->option_value)) {
      ?>
      <div class="address-line">
        <b><img src="<?php echo FRONTEND_THEME_URL ?>images/map-marker.svg"> Address :</b> <?php if(!empty($get_result[18]->option_value)) echo $get_result[18]->option_value; ?>
      </div>
      <?php } } ?>
      <?php  if(!empty($get_result)){
      if(!empty($get_result[16]->option_value)) {
      ?>
      <div class="email">
      <b><a href="#"><img src="<?php echo FRONTEND_THEME_URL ?>images/envlope-25X25.svg"> Email : </b><?php if(!empty($get_result[16]->option_value)) echo $get_result[16]->option_value; ?></a></div>
      <?php } } ?>
      <!-- <h3 >Social</h3> -->
<!--       <?php  //if(!empty($get_result)){ ?>
    <div class="">
      <div class="social-warp">
        <ul>
          <?php //if(!empty($get_result[2]->option_value)) { ?>
          <li><a href="<?php //if(!empty($get_result[2]->option_value)) echo $get_result[2]->option_value; ?>" class="facebook" title="facebook"></a></li>
          <?php //} ?>
          <?php //if(!empty($get_result[10]->option_value)) { ?>
          <li><a href="<?php //if(!empty($get_result[10]->option_value)) echo $get_result[10]->option_value; ?>" class="snap-chat" title="snapchat"></a></li>
          <?php //} ?>
          <?php //if(!empty($get_result[9]->option_value)) { ?>
          <li><a href="<?php //if(!empty($get_result[9]->option_value)) echo $get_result[9]->option_value; ?>" class="insta" title="instagram"></a></li>
          <?php //} ?>
        </ul>
      </div>
      <?php //} ?>
    </div> -->
    <div class="clearfix"></div>
    
  </div>
  <div class="clearfix"></div>
  <div class="container">
    <div class="copy-right"><p>&copy; Copyright <?php echo date('Y')?>. All Rights Reserved.</p></div>
  </div>
  <div class="container">
  <div class="design-by text-center"><p>Designed & Developed By <a href="http://www.chapter247.com/" target="_blank"><img src="./assets/front/images/chapter_logo.png"></a></p></div>
  </div>
  </div>
</footer>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/wow.min.js"></script>