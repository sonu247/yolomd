<div class="modal fade custom-modal" id="common_modal_msg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog login-dialog" role="document" >
    <div class="modal-content" style="min-height:30px!important;">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
     
      <div class="modal-body" >

       <div class="themeform">
     
        <section ng-class="{{class_msg}}">
         <div class="info-modal-block">
         <div class="info-icon">
          <span>
           <strong ng-bind-html="icon"></strong> 
          </span>
         </div>
         <h4 class="text-center">
          <span ng-bind-html="msg_info"></span>
         </h4>
         <div class="clearfix"></div>
         </div>
        </section>
       </div>
      </div>
    </div>
  </div>
</div>