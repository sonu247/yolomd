<?php 
$this->load->view('include/header_menu');
?>
<div ng-cloak>

<!-- Add the slick-theme.css if you want default styling -->
<div ng-if="banner_section">
<div class="home-slider" >
<div class="slide" style="background:url(<?php echo base_url('assets/uploads/banner');?>/{{banner_section.cms_image}});">
  <div class="slide-text">
     <h1 ng-if="banner_section.cms_title" ng-bind-html="banner_section.cms_title"></h1>
    <h4  ng-if="banner_section.cms_sub_title" ng-bind-html="banner_section.cms_sub_title"></h4>
    <div  ng-if="banner_section.label" class="slide-button"><a href="{{banner_section.cms_button_link}}" class="btn-blue">{{banner_section.label}}</a></div>
  </div>
</div>
</div>
</div>
<!--==========HOME PAGE SLIDER END===========-->

<!--==========HOME PAGE SIGN UP SECTION===========-->
<div class="home-signup-warp">
<div class="container home-signup">
 <div class="col-md-6 col-sm-6">
  <div class="section left clearfix" >
    <div class="pull-left icon-block">
      <span><img src="<?php echo FRONTEND_THEME_URL ?>images/test/physicianjob_icon.svg"></span>
    </div>
    <div class="pull-left content-block">
    <h2 class="heading" ng-if="physician_medical_section.title1">
    <span ng-bind-html="physician_medical_section.title1"></span>
    </h2>
    <div class="wow fadeInLeft" data-wow-offset="10">
    <p ng-if="physician_medical_section.sub_title1" ng-bind-html="physician_medical_section.sub_title1"></p>
    <br>
    <div ng-if="physician_medical_section.label1"><a href="<?php echo site_url()?>{{physician_medical_section.link1}}" class="button">{{physician_medical_section.label1}}</a> </div>
    </div>
    </div>
    
  </div>
 </div>
 <div class="col-md-6 col-sm-6">
  <div class="section right clearfix">
    <div class="pull-left icon-block">
      <span><img src="<?php echo FRONTEND_THEME_URL ?>images/test/facility-managers.svg"></span>
    </div>
    <div class="pull-left content-block">
    <h2 class="heading" ng-if="physician_medical_section.title2" ng-bind-html="physician_medical_section.title2"></h2>
    <div class="wow fadeInRight" data-wow-offset="10">
      <p ng-if="physician_medical_section.sub_title2" ng-bind-html="physician_medical_section.sub_title2" ></p>
      <br>
      <div ng-if="physician_medical_section.label2" ><a href="<?php echo site_url()?>{{physician_medical_section.link2}}" class="button">{{physician_medical_section.label2}}</a> </div>
    </div>
    </div>

  </div>

<div class="clearfix"></div>
</div>
</div>
</div>
<!--==========HOME PAGE SIGN UP SECTION END===========-->

<!--==========HOME PAGE SEARCH CITY SECTION===========-->
<div class="search-city wow bounceInUp clearfix">
<div class="container "   ng-controller="GetSearch" >
<h3 class="text-center heading">Search Jobs by City</h3>
<P class="text-center heading-text">Click below to quick search all job opportunities in some of Canada’s major cities
</P>
  <div class="search-city-slider col-md-11">
<!-- 
  <slick on-init="slickOnInit()" class="slider single-item" data="this" current-index="index" responsive="breakpoints" slides-to-show=1 slides-to-scroll=1>
    <div ng-repeat="i in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]">
        <h3>{{ i }}</h3>
        <a data-ng-if="!refreshing" ng-click="clickTheThing(i)" style="color:blue;">Click Here</a>
    </div>
    <p>Current index: {{ index }}</p>
</slick> -->

    <slick ng-if="city_section.length" on-init="slickOnInit()" class="slider single-item" current-index="index" responsive="breakpoints" dots=false infinite=true speed=300 slides-to-show={{slidesOnPage}} touch-move=false slides-to-scroll=1 init-onload=true data="city_section">
    <div  ng-repeat="city_s in city_section" >
      <div class="city-img-tile"  ng-click="get_id_save_search(city_s.search_city_name,'')">
      <img ng-if="city_s.search_city_image" src="<?php echo UPLOAD_PATH ?>city/{{city_s.search_city_image}}" class="img-responsive">
      <div class="caption" ng-if="city_s.search_city_name"><a href="#" class="city-name"><h3 class="city-name">{{city_s.search_city_name}}</h3></a></div>
      </div>
    </div>
  </slick>
  </div>
  <div class="clearfix"></div>
  
</div>
</div>
<!--==========HOME PAGE SEARCH CITY SECTION END===========-->

<!--==========HOME PAGE POST JOB SECTION ===========-->
<div class="postjob-warp wow bounceInUp" data-wow-offset="10">
<div class="container">
  <div class="row">
  <h3 class="text-center heading">OUR SERVICES & SPECIAL FIELDS</h3>
    <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
      <div class="job-column text-center">
        <div class="img-section"><img src="<?php echo FRONTEND_THEME_URL ?>images/free.svg"></div>
        <div class="text">
          <h4 ng-if="section_4.title1">{{section_4.title1}}</h4>
          <p ng-if="section_4.sub_title1">{{section_4.sub_title1}}</p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
      <div class="job-column text-center">
        <div class="img-section"><img src="<?php echo FRONTEND_THEME_URL ?>images/posting.svg"></div>
        <div class="text">
          <h4 ng-if="section_4.title2">{{section_4.title2}}</h4>
          <p ng-if="section_4.sub_title2">{{section_4.sub_title2}}</p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.7s">
      <div class="job-column text-center">
        <div class="img-section"><img src="<?php echo FRONTEND_THEME_URL ?>images/responsive.svg"></div>
        <div class="text">
          <h4 ng-if="section_4.title3">{{section_4.title3}}</h4>
          <p ng-if="section_4.sub_title3">{{section_4.sub_title3}}</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--==========HOME PAGE POST JOB SECTION END===========-->

<?php 
$this->load->view('include/footer_menu');
?>
<script type="text/javascript" src="<?php echo FRONTEND_THEME_URL ?>js/vendor/theme.js"></script>
</div>

