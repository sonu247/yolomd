<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Getoption{
	  public function __construct($params=''){
		$options=FALSE;
		$CI =& get_instance();
		 $CI->load->model('common_model'); 

		if($option=$CI->common_model->get_result('options')){
			$this->options=$option;
		}
	}

	function get_option_value($key_slug=FALSE){
		if($key_slug){			
			foreach ($this->options as $key => $value) {				
				if($key_slug==$value->option_name){
					return $value->option_value;
				}
			}
		}else{
			return FALSE;
		}
	}

}
?>