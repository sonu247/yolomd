<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*	clear cache
*/
if ( ! function_exists('clear_cache')) {
	function clear_cache(){
		$CI =& get_instance();
		$CI->output->set_header('Expires: Wed, 11 Jan 1984 05:00:00 GMT' );
		$CI->output->set_header('Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . 'GMT');
		$CI->output->set_header("Cache-Control: no-cache, no-store, must-revalidate");
		$CI->output->set_header("Pragma: no-cache");			
	}
}
/**
*	check superadmin logged in
*/
if ( ! function_exists('superadmin_logged_in')) {
	function superadmin_logged_in(){
		$CI =& get_instance();
		$superadmin_info = $CI->session->userdata('superadmin_info');
		if($superadmin_info['logged_in']===TRUE && $superadmin_info['user_role'] == 0 )
			return TRUE;
		else
			return FALSE;
	}
}
/*  
 facility list
*/

if ( ! function_exists('get_medicalfasility_list')) {
    function get_medicalfasility_list(){
    	  $CI =& get_instance();
	       $CI->db->where('status','1');
		   $query = $CI->db->get('medical_specialties_annex2');
	       $result =  $query->result();
	    return $result;      
    }
}
/**
*	get superadmin id
*/
if ( ! function_exists('superadmin_id')) {
	function superadmin_id(){
		$CI =& get_instance();
		$superadmin_info = $CI->session->userdata('superadmin_info');		
			return $superadmin_info['id'];		
	}
}
/**
*	get photographer id
*/
if ( ! function_exists('photographer_id')) {
	function photographer_id(){
		$CI =& get_instance();
		$photographer_info = $CI->session->userdata('photographer_info');		
			return $photographer_info['id'];		
	}
}
if ( ! function_exists('photographer_name')) { 
	function photographer_name(){
		$CI =& get_instance();
		$photographer_info = $CI->session->userdata('photographer_info');
		if($photographer_info['logged_in']===TRUE )
		 	return $photographer_info['first_name']." ".$photographer_info['last_name'];
		else
			return FALSE;
	}					
}
if ( ! function_exists('photographer_logged_in')) {
    function photographer_logged_in(){
        $CI =& get_instance();
        $photographer_login_info = $CI->session->userdata('photographer_info');
        if($photographer_login_info['logged_in']===TRUE && $photographer_login_info['user_role'] == 3 )
            return TRUE;
        else
            return FALSE;
    }
}
/**
*	superadmin login information
*/
if ( ! function_exists('superadmin_name')) { 
	function superadmin_name(){
		$CI =& get_instance();
		$superadmin_info = $CI->session->userdata('superadmin_info');
		if($superadmin_info['logged_in']===TRUE )
		 	return $superadmin_info['first_name']." ".$superadmin_info['last_name'];
		else
			return FALSE;
	}					
}

if ( ! function_exists('physician_logged_in')) {
    function physician_logged_in(){
        $CI =& get_instance();
        $physician_login_info = $CI->session->userdata('physician_login_info');
        if($physician_login_info['logged_in']===TRUE && $physician_login_info['user_role'] == 2 )
            return TRUE;
        else
            return FALSE;
    }
}

if ( ! function_exists('medicaluser_logged_in')) {
    function medicaluser_logged_in(){
        $CI =& get_instance();
        $medicaluser_logged_info = $CI->session->userdata('medicaluser_logged_in');
        if($medicaluser_logged_info['logged_in']===TRUE && $medicaluser_logged_info['user_role'] == 1 )
            return TRUE;
        else
            return FALSE;
    }
}
 
if ( ! function_exists('physicain_id')) {
    function physicain_id(){
        $CI =& get_instance();
        $physician_login_info = $CI->session->userdata('physician_login_info');        
        return $physician_login_info['id'];        
    }
}

if ( ! function_exists('medicaluser_id')) {
    function medicaluser_id(){
        $CI =& get_instance();
        $medicaluser_logged_info = $CI->session->userdata('medicaluser_logged_in');        
        return $medicaluser_logged_info['id'];        
    }
}


if ( ! function_exists('get_medicaluser_info')) {
    function get_medicaluser_info(){
        $CI =& get_instance();
        $medical_user_info = $CI->common_model->get_row('users',array('user_id'=>medicaluser_id()));        
        return $medical_user_info;        
    }
}
if ( ! function_exists('get_user_info')) {
    function get_user_info($id){
        $CI =& get_instance();
        $user_info = $CI->common_model->get_row('users',array('user_id'=>$id));        
        return $user_info;        
    }
}


/**
*    admin login information
*/
if ( ! function_exists('physicain_name')) {
    function physicain_name(){
        $CI =& get_instance();
        $physician_login_info = $CI->session->userdata('physician_login_info');
        if($physician_login_info['logged_in']===TRUE ){
        	if($physician_admin = $CI->common_model->get_row('users',array('user_id'=>physicain_id()), array('user_first_name'))){
             return $physician_admin->user_first_name;
        	}else{
        		return FALSE;
        	}
         }
        else
            return FALSE;
    }                    
}

if ( ! function_exists('get_physicain_info')) {
    function get_physicain_info(){
        $CI =& get_instance();
        $physician_login_info = $CI->common_model->get_row('users',array('user_id'=>physicain_id()));        
        return $physician_login_info;        
    }
}

if ( ! function_exists('get_physicain_detail_info')) {
    function get_physicain_detail_info(){
        $CI =& get_instance();
        $physician_login_info = $CI->common_model->get_row('physicians',array('user_id'=>physicain_id()));        
        return $physician_login_info;        
    }
}

if ( ! function_exists('backend_pagination')) {
	function backend_pagination(){
		$data = array();		
		$data['full_tag_open'] = '<ul class="pagination">';		
		$data['full_tag_close'] = '</ul>';
		$data['first_tag_open'] = '<li>';
		$data['first_tag_close'] = '</li>';
		$data['num_tag_open'] = '<li>';
		$data['num_tag_close'] = '</li>';
		$data['last_tag_open'] = '<li>';
		$data['last_tag_close'] = '</li>';
		$data['next_tag_open'] = '<li>';
		$data['next_tag_close'] = '</li>';
		$data['prev_tag_open'] = '<li>';
		$data['prev_tag_close'] = '</li>';
		$data['cur_tag_open'] = '<li class="active"><a href="#">';
		$data['cur_tag_close'] = '</a></li>';
		return $data;
	}					
}
/**
*	frontend pagination
*/
if ( ! function_exists('frontend_pagination')) {
	function frontend_pagination(){
		$data = array();
		$data['full_tag_open'] = '<ul class="pagination">';		
		$data['full_tag_close'] = '</ul>';
		$data['first_tag_open'] = '<li>';
		$data['first_tag_close'] = '</li>';
		$data['num_tag_open'] = '<li>';
		$data['num_tag_close'] = '</li>';
		$data['last_tag_open'] = '<li>';		
		$data['last_tag_close'] = '</li>';
		$data['next_tag_open'] = '<li>';
		$data['next_tag_close'] = '</li>';
		$data['prev_tag_open'] = '<li>';
		$data['prev_tag_close'] = '</li>';
		$data['cur_tag_open'] = '<li class="active"><a href="#">';
		$data['cur_tag_close'] = '</a></li>';
		$data['next_link'] = 'Next';
		$data['prev_link'] = 'Previous';
		return $data;
	}					
}

/**
*	thisis  back end helper 
*/
if ( ! function_exists('msg_alert')) {
	function msg_alert(){
	$CI =& get_instance(); ?>
<?php if($CI->session->flashdata('msg_success')): ?>	
	<div class="alert alert-success">
		 <button type="button" class="close" data-dismiss="alert">&times;</button> 
	    <strong>Success :</strong> <br>  <?php echo $CI->session->flashdata('msg_success'); ?>
	</div>
 <?php endif; ?>
<?php if($CI->session->flashdata('msg_info')): ?>	
	<div class="alert alert-info">
		 <button type="button" class="close" data-dismiss="alert">&times;</button> 
	    <strong>Info :</strong> <br> <?php echo $CI->session->flashdata('msg_info'); ?>
	</div>
<?php endif; ?>
<?php if($CI->session->flashdata('msg_warning')): ?>	
	<div class="alert alert-warning">
		 <button type="button" class="close" data-dismiss="alert">&times;</button> 
	     <strong>Warning :</strong> <br> <?php echo $CI->session->flashdata('msg_warning'); ?>
	</div>
<?php endif; ?>
<?php if($CI->session->flashdata('msg_error')): ?>	
	<div class="alert alert-danger">
		 <button type="button" class="close" data-dismiss="alert">&times;</button> 
	     <strong>Error :</strong> <br>  <?php echo $CI->session->flashdata('msg_error'); ?>
	</div>
<?php endif; ?>
	<?php }					
}
/**
*	thisis  back end helper 
*/
if ( ! function_exists('msg_alert_front')) {
	function msg_alert_front(){
	$CI =& get_instance(); ?>
	<?php if($CI->session->flashdata('theme_danger')): ?>	
	<div class="alert theme-alert-danger">
		 <button type="button" class="close" data-dismiss="alert">&times;</button>
	     <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('theme_danger'); ?>
	</div>
 <?php endif; ?>
 <?php if($CI->session->flashdata('theme_success')): ?>	
	<div class="alert theme-success">
		 <button type="button" class="close" data-dismiss="alert">&times;</button>
	     <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('theme_success'); ?>
	</div>
 <?php endif; ?>

<?php if($CI->session->flashdata('msg_success')): ?>	
	<div class="alert alert-success">
		 <button type="button" class="close" data-dismiss="alert">&times;</button>
	     <!-- <strong>Success :</strong> <br> --> <?php echo $CI->session->flashdata('msg_success'); ?>
	</div>
 <?php endif; ?>
<?php if($CI->session->flashdata('msg_info')): ?>	
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert">&times;</button> 
	    <!-- <strong>Info :</strong> <br> --> <?php echo $CI->session->flashdata('msg_info'); ?>
	</div>
<?php endif; ?>
<?php if($CI->session->flashdata('msg_warning')): ?>	
	<div class="alert alert-warning">
		<!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
	   <!--  <strong>Warning :</strong> <br> --> <?php echo $CI->session->flashdata('msg_warning'); ?>
	</div>
<?php endif; ?>
<?php if($CI->session->flashdata('msg_error')): ?>	
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert">&times;</button> 
	    <!-- <strong>Error :</strong> <br> --> <?php echo $CI->session->flashdata('msg_error'); ?>
	</div>
<?php endif; ?>
	<?php }					
}
/**
*	Menu Information
*/
if ( ! function_exists('upload_file')) {
	function upload_file($param = null){
		$CI =& get_instance();		
		
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|xls|xlsx|csv|jpeg|pdf|doc|docx';
		$config['max_size']	= 1024*90;
		$config['image_resize']= FALSE;
		$config['resize_width']= 126;
		$config['resize_height']= 126;
		
		if ($param){
            $config = $param + $config;
        }
		$CI->load->library('upload', $config);
		if(!empty( $config['file_name']))
			$file_Status = $CI->upload->do_upload($config['file_name']);
		else
			$file_Status = $CI->upload->do_upload();
		if (!$file_Status){
			return array('STATUS'=>FALSE,'FILE_ERROR' => $CI->upload->display_errors());			
		}else{
			$uplaod_data=$CI->upload->data();
	
			$upload_file = explode('.', $uplaod_data['file_name']);
			
			if($config['image_resize'] && in_array($upload_file[1], array('gif','jpeg','jpg','png','bmp','jpe'))){
				$param2=array(
					'source_image' 	=>	$config['source_image'].$uplaod_data['file_name'],
					'new_image' 	=>	$config['new_image'].$uplaod_data['file_name'],
					'create_thumb' 	=>	FALSE,
					'maintain_ratio'=>	FALSE,
					'width' 		=>	$config['resize_width'],
					'height' 		=>	$config['resize_height'],
					);
			
				image_resize($param2);
			}	
			return array('STATUS'=>TRUE,'UPLOAD_DATA' =>$uplaod_data );
		}
	}
}
/**
*	image resize
*/
if ( ! function_exists('image_resize')) {
	function image_resize($param = null){
		$CI =& get_instance();
		$config['image_library'] = 'gd2';
		$config['source_image']	= './assets/uploads/';
		$config['new_image']	= './assets/uploads/';		
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['width']	 = 150;
		$config['height']	= 150;
		
		 if ($param) {
            $config = $param + $config;
        }
		$CI->load->library('image_lib', $config); 
		if ( ! $CI->image_lib->resize())
		{
		   //return array('STATUS'=>TRUE,'MESSAGE'=>$CI->image_lib->display_errors()); 
			die($CI->image_lib->display_errors());
		}else{
			 return array('STATUS'=>TRUE,'MESSAGE'=>'Image resized.'); 
		}
	}
}

/**
*	Menu Information
*/
if ( ! function_exists('get_nav_menu')) {
	function get_nav_menu($slug='',$is_location=FALSE){
		$CI =& get_instance();
		//$CI->load->model('user_model');		
		if($menu =$CI->common_model->get_nav_menu($slug,$is_location))
			return $menu;
		else
			return FALSE;
	}					
}
/**
*	Get YouTube video ID from URL
*/
if ( ! function_exists('get_youtube_id_from_url')) {
	function get_youtube_thumbnail($youtube_url='',$alt=TRUE){
			$youtubeId = preg_replace('/^[^v]+v.(.{11}).*/', '$1', $youtube_url); 
		
		if($alt) $alt='alt="AA'.$youtubeId.'"'; else $alt='';
		return'<img style="border-radius: 0px !important; transition: none 0s ease 0s;" class="timeline-img pull-left imgsize" src="http://img.youtube.com/vi/'.$youtubeId.'/default.jpg" '.$alt.'>';
				
	}					
}
//for option
if ( ! function_exists('get_option_value')) {
	function get_option_value($key=FALSE){	
		$CI =& get_instance();		
		if($option = $CI->getoption->get_option_value($key))		
			return $option;
		else
			return FALSE;	
	}
}
if ( ! function_exists('file_download')) {
	function file_download($title=FALSE,$data=FALSE){
		$data=str_replace('./', '', $data);		
		$CI =& get_instance();		
		$CI->load->helper('download');
		if(!empty($title) && !empty($data)):
			$title=url_title($title, '-', TRUE);
			if($file = file_get_contents($data)){ 		
			$extend=end(explode('.',$data));			 
			$file_name = $title.'.'.$extend;			
			force_download($file_name, $file);
		}else{
			return FALSE;
		}
		endif;	
	}
}
if ( ! function_exists('get_post')) {
	function get_post($slug='',$is_slug=FALSE){
		$CI =& get_instance();	
		if(!empty($slug))				
			return $CI->common_model->get_post($slug,$is_slug);
		else
			return FALSE;
	}					
}


/**
*	thumbnail image
*/
if ( ! function_exists('create_thumbnail')) {
	function create_thumbnail($config_img='',$img_fix='') {
		$CI =& get_instance();
		$config_image['image_library'] = 'gd2';
		$config_image['source_image'] = $config_img['source_path'].$config_img['file_name'];	
		//$config_image['create_thumb'] = TRUE;
		$config_image['new_image'] = $config_img['destination_path'].$config_img['file_name'];
		$config_image['height']=$config_img['height'];
		$config_image['width']=$config_img['width'];
		if($img_fix){
		$config_image['maintain_ratio'] = FALSE;
		}
		else{
			$config_image['maintain_ratio'] = TRUE;
			list($width, $height, $type, $attr) = getimagesize($config_img['source_path'].$config_img['file_name']);

	        if ($width < $height) {
	        	$cal=$width/$height;
	        	$config_image['width']=$config_img['width']*$cal;
	        }
			if ($height < $width)
			{
				$cal=$height/$width;
		    	$config_image['height']=$config_img['height']*$cal;
			}
		}
		
		$CI->load->library('image_lib');
		$CI->image_lib->initialize($config_image);
		
		if(!$CI->image_lib->resize()) 
			return array('status'=>FALSE,'error_msg'=>$CI->image_lib->display_errors());
		else
			return array('status'=>TRUE,'file_name'=>$config_img['file_name']);
	}
}
/*
/**
*	get_social_url
*/
if ( ! function_exists('get_option_url')) {
	function get_option_url($option_name){	
		$CI =& get_instance();		
		 if($query = $CI->common_model->get_row('options',array('option_name'=>$option_name)))
		 	return $query->option_value;
		 else
		 	return false;
	}
}
if(!function_exists('currently_status'))
{
	function currently_status($status=''){
		$currently_status = array(
									'1' => 'Medical Student',
									'2' => 'Resident',
									'3' => 'Physician'

								  );
		return $currently_status;

	}
}

if(!function_exists('get_particular_option'))
{
	function get_particular_option($key = '')
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_result('options',array('status'=>1)))
	  {
		  	foreach($result as $key => $get_opt){
	        $finalArray[$get_opt->id]=$get_opt;
	   		}
	   		 return $finalArray;
	  }else{
	  	return false;		
	  }
	 
	}
}
if(!function_exists('get_particular_menu'))
{
	function get_particular_menu($menu_id = '')
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_row('cms_menu',array('status'=>1,'parent_id'=>0,'id'=>$menu_id),array(),array('order_by','asc')))
	  return $result;
		else
	  return false;		

	}
}
if(!function_exists('get_submenu'))
{
	function get_submenu($menu_id = '')
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_result('cms_menu',array('status'=>1,'parent_id'=>$menu_id),array(),array('order_by','asc')))
	  return $result;
		else
	  return false;		

	}
}	
	if(!function_exists('transport_type'))
{
	function transport_type(){
		$transport_type = array(
									'1' => 'Metro/Subway',
									'2' => 'Bus',
									'3' => 'Other'

								  );
		return $transport_type;

	}
}
if(!function_exists('facility_type'))
{
	function facility_type(){
		$facility_type = array(
									'1' => 'Hospital',
									'2' => 'Government Clinic',
									'3' => 'Private Family Practice',
									'4' => 'Private Clinic',
									'5' => 'Rehabilitation Centre',
									'6' => 'Retirement Home',
									'7' => 'Specialist’s Clinic',
									

								  );
		return $facility_type;

	}
}
if(!function_exists('hours'))
{
	function hours(){
		$hours = array(
						'1' => 'Full-Time',
						'2' => 'Part-Time',
						'3' => 'Flexible',
					 );
		return $hours;

	}
}

if(!function_exists('compensation'))
{
	function compensation(){
		$compensation = array(
						'1' => 'Fee-for-service',
						'2' => 'Salary',
						'3' => 'Other',
					 );
		return $compensation;

	}
}
if(!function_exists('health_professionals'))
{
	function health_professionals()
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_result('allied_health_professionals',array('status'=>1),array(),array('order_by','asc')))
	  return $result;
		else
	  return false;		

	}
}
if(!function_exists('nursing'))
{
	function nursing()
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_result('nursing',array('status'=>1),array(),array('order_by','asc')))
	  return $result;
		else
	  return false;		

	}
}


if(!function_exists('verified_facility'))
{
	function verified_facility()
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_row('options',array('status'=>1,'option_name'=>'VERIFIED_FACILITY'),array(),array('order','asc')))
	  return $result;
		else
	  return false;		

	}
}
if(!function_exists('paypal_priority'))
{
	function paypal_priority()
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_row('options',array('status'=>1,'option_name'=>'PAYPAL_PRIORITY_PLACEMENT'),array(),array('order','asc')))
	  return $result;
		else
	  return false;		

	}
}
if(!function_exists('month_name'))
{
	function month_name()
	{
	  $month_name = array(
						'1' => 'Jan',
						'2' => 'Feb',
						'3' => 'Mar',
						'4' => 'Apr',
						'5' => 'May',
						'6' => 'Jun',
						'7' => 'July',
						'8' => 'Aug',
						'9' => 'Sept',
						'10' => 'Oct',
						'11' => 'Nov',
						'12' => 'Dec',
					 );
		return $month_name;

	}
}
if(!function_exists('days_name'))
{
	function days_name()
	{
		 $days_name = array(
		array('id' => 1, 'label'=>'Mon'),
		array('id' => 2, 'label'=>'Tues'),
		array('id' => 3, 'label'=>'Wed'),
		array('id' => 4, 'label'=>'Thur'),
		array('id' => 5, 'label'=>'Fri'),
		array('id' => 6, 'label'=>'Sat'),
		array('id' => 7, 'label'=>'Sun'),
		);
		return $days_name;

	}
}
if ( ! function_exists('get_medical_specialty')) {
    function get_medical_specialty($id=''){
        $CI =& get_instance();
        if($result = $CI->common_model->get_row('medical_specialties_annex2',array('id'=>$id)))
        	return $result->name;
        else
     	 return false; 
    }
}
if ( ! function_exists('get_medical_specialty_logo')) {
    function get_medical_specialty_logo($id=''){
        $CI =& get_instance();
        if($result = $CI->common_model->get_row('medical_specialties_annex2',array('id'=>$id)))
        	return $result->logo;
        else
     	 return false; 
    }
}
if ( ! function_exists('get_health_professional')) {
    function get_health_professional($id=''){
        $CI =& get_instance();
        if($result = $CI->common_model->get_row('allied_health_professionals',array('id'=>$id)))
       	 return $result->name;
        else
     	 return false; 
    }
}
if(!function_exists('get_facility_type'))
{
    function get_facility_type($id=''){
        $facility_type = array(
                                    '1' => 'Hospital',
                                    '2' => 'Government Clinic',
                                    '3' => 'Private Family Practice',
                                    '4' => 'Private Clinic',
                                    '5' => 'Rehabilitation Centre',
                                    '6' => 'Retirement Home',
                                    '7' => 'Specialist’s Clinic',
                                    '8' => 'Parking',
                                  );
        if(array_key_exists($id, $facility_type)){
              return $facility_type[$id];
        }
        else
        {
          return false;
        }

    }
}
if ( ! function_exists('get_province_name')) {
    function get_province_name($id=''){
        $CI =& get_instance();
        if($id)
      	  return $id;
        else
     	  return false;  
    }
}

if ( ! function_exists('time_array')) {
    function time_array($id=''){
         $time = '00:00'; // start
         $time_array=array();
	     for ($i = 0; $i <= 47; $i++)
	     {
	         $prev = date('g:ia',strtotime($time)); // format the start time
	         $next = strtotime('+30mins',strtotime($time)); // add 30 mins
	         $time = date('g:ia',$next); // format the next time
	          $time_array[]=$prev;
	     }
	     return  $time_array;
    }
}
if ( ! function_exists('time_array_eve')) {
    function time_array_eve($id=''){
         $time = '00:00'; // start
         $time_array=array();
	     for ($i = 0; $i <= 47; $i++)
	     {
	         $prev = date('g:ia',strtotime($time)); // format the start time
	         $next = strtotime('-30mins',strtotime($time)); // add 30 mins
	         $time = date('g:ia',$next); // format the next time
	          $time_array[]=$time;
	     }
	    // krsort($time_array);
	     return  $time_array;
    }
}
if ( ! function_exists('get_nursing')) {
    function get_nursing($id=''){
        $CI =& get_instance();
        if($result = $CI->common_model->get_row('nursing',array('id'=>$id)))
      	  return $result->name;
        else
     	  return false;  
    }
}
if(!function_exists('get_days_name'))
{
	function get_days_name($id='')
	{
	  $days_name = array(
						
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday',
						'7' => 'Sunday',
					 );
		if(array_key_exists($id, $days_name)){
              return $days_name[$id];
        }
        else
        {
          return false;
        }

	}
}
if(!function_exists('get_hours'))
{
	function get_hours($id){
		$hours = array(
						'1' => 'Full-Time',
						'2' => 'Part-Time',
						'3' => 'Flexible',
					 );
		if(array_key_exists($id, $hours)){
              return $hours[$id];
        }
        else
        {
          return false;
        }

	}
}
if(!function_exists('get_compensation'))
{
	function get_compensation($id){
		$compensation = array(
						'1' => 'Fee-for-service',
						'2' => 'Salary',
						'3' => 'Other',
					 );
		if(array_key_exists($id, $compensation)){
              return $compensation[$id];
        }
        else
        {
          return false;
        }

	}
}

if(!function_exists('get_payment_status'))
{
	function get_payment_status($unique_id = '')
	{
	  $CI =& get_instance();		
	  if($result = $CI->common_model->get_row('payment_city',array('unique_id'=>$unique_id)))
	  return $result->payment_status;
		else
	  return 2;		

	}
}	
if ( ! function_exists('get_assignee_detail')) {
    function get_assignee_detail($job_id){
        $CI =& get_instance();
        $assignee_info = $CI->common_model->get_row('assign_user',array('job_id'=>$job_id)); 
        if(!empty($assignee_info))   
        return $assignee_info->assign_name;   
        else
        return false;     
    }
}
if ( ! function_exists('get_job_name')) {
    function get_job_name($job_id){
        $CI =& get_instance();
        $job_info = $CI->common_model->get_row('post_jobs',array('id'=>$job_id)); 
        if(!empty($job_info))   
        return ucfirst($job_info->listing_title);   
        else
        return false;     
    }
}

if ( ! function_exists('check_marked_complete')) {
    function check_marked_complete($job_id){
        $CI =& get_instance();
        $marked_count = $CI->common_model->count_rows('assign_user', array('job_id'=>$job_id,'marked_complete'=>1),'',''); 
        if(!empty($marked_count))   
        return true;   
        else
        return false;     
    }
}
if(!function_exists('get_job_status'))
{
	function get_job_status($id){
		$job = array(
						'0' => 'Pending',
						'1' => 'Active',
						'2' => 'Deactive',
						'3' => 'Banned',
						'4' => 'Draft'
					 );
		if(array_key_exists($id, $job)){
              return $job[$id];
        }
        else
        {
          return false;
        }

	}
}
if(!function_exists('get_user_status'))
{
	function get_user_status($id){
		$user_status = array(
						'1' => 'Active',
						'2' => 'Deactive',
						'3' => 'Banned',
					 );
		if(array_key_exists($id, $user_status)){
              return $user_status[$id];
        }
        else
        {
          return false;
        }

	}
}

if(!function_exists('all_row_count'))
{
	function all_row_count($table='',$condition=''){
		$CI =& get_instance();
        $count = $CI->common_model->count_rows($table,$condition); 
        if(!empty($count))   
        return $count;   
        else
        return 0;     
	}
}
if(!function_exists('getLocalIp'))
{
	function getLocalIp(){
			$ipaddress = '';
	     if (getenv('HTTP_CLIENT_IP'))
	         $ipaddress = getenv('HTTP_CLIENT_IP');
	     else if(getenv('HTTP_X_FORWARDED_FOR'))
	         $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	     else if(getenv('HTTP_X_FORWARDED'))
	         $ipaddress = getenv('HTTP_X_FORWARDED');
	     else if(getenv('HTTP_FORWARDED_FOR'))
	         $ipaddress = getenv('HTTP_FORWARDED_FOR');
	     else if(getenv('HTTP_FORWARDED'))
	         $ipaddress = getenv('HTTP_FORWARDED');
	     else if(getenv('REMOTE_ADDR'))
	         $ipaddress = getenv('REMOTE_ADDR');
	     else
	         $ipaddress = 'UNKNOWN';

	     return $ipaddress;

		return $ipaddress ; 
	}
}
if ( ! function_exists('get_job_detail')) {
    function get_job_detail($job_id){
        $CI =& get_instance();
        $job_info = $CI->common_model->get_row('post_jobs',array('id'=>$job_id)); 
        if(!empty($job_info))   
        return $job_info;   
        else
        return false;     
    }
}
if ( ! function_exists('get_postalcode_id')) {
    function get_postalcode_id($postal_code){
        $CI =& get_instance();
        $postal_info = $CI->common_model->get_row('location_complete_data',array('postal_code'=>$postal_code)); 
        $res = array();
        if(!empty($postal_info))
        {
        	$res = array('id'=>$postal_info->id,'postal_code'=>$postal_info->postal_code);
        	 return $res;  
        }else{
        	return false;   
        }   
        
    }
}
if ( ! function_exists('get_msg_detail')) {
    function get_msg_detail($msg_id){
        $CI =& get_instance();
        $msg_info = $CI->common_model->get_row('jobs_messages',array('msg_id'=>$msg_id)); 
        if(!empty($msg_info))   
        return $msg_info;   
        else
        return false;     
    }
}
if ( ! function_exists('generate_captcha')) {
    function generate_captcha($image="cap_code"){
	    $CI =& get_instance();
	    $ranStr = md5(microtime());
        $ranStr = substr($ranStr, 0, 6);
        $font = './assets/front/Route159-LightItalic.otf';
        $CI->session->set_userdata($image,$ranStr);
        $newImage = imagecreatefromjpeg('assets/uploads/cap_bg.jpg');
        $txtColor = imagecolorallocate($newImage, 3, 63, 88);
        imagettftext($newImage, 22, 0, 14, 35, $txtColor, $font, $ranStr);
        ob_start(); 
	        imagejpeg($newImage); 
	        $contents = ob_get_contents();
	    ob_end_clean();

	    $dataUri = "data:image/jpeg;base64," . base64_encode($contents);
    	return $dataUri;
  	}
}
if ( ! function_exists('rotate_image')) {
    function rotate_image($filename){
    	$explode = explode(".", $filename);
		$extension = $explode[count($explode)-1];
		$img = "assets/uploads/jobs/".$filename;
		$thumb_img = "assets/uploads/jobs/thumbs/".$filename;
    	if ($extension=='jpg' || $extension=='jpeg' || $extension=='JPG'  || $extension=='JPEG') {
			// Load Main Image
			$source = imagecreatefromjpeg($img);
			$source = imagerotate($source, -90, 0);
			imagealphablending( $source, false ); 
	    	imagesavealpha( $source, true );
			imagejpeg($source,$img);
			imagedestroy($source);
			// Load Thumb Image
			$source2 = imagecreatefromjpeg($thumb_img);
			$source2 = imagerotate($source2, -90, 0);
			imagealphablending( $source2, false ); 
	    	imagesavealpha( $source2, true );
			imagejpeg($source2,$thumb_img);
			imagedestroy($source2);
    	}
    	else if ($extension=='png' || $extension=='PNG') {
			// Load Main Image
			$source = imagecreatefrompng($img);
			$source = imagerotate($source, -90, 0);
			imagealphablending( $source, false ); 
	    	imagesavealpha( $source, true );
			imagepng($source,$img);
			imagedestroy($source);
			// Load Thumb Image
			$source2 = imagecreatefrompng($thumb_img);
			$source2 = imagerotate($source2, -90, 0);
			imagealphablending( $source2, false ); 
	    	imagesavealpha( $source2, true );
			imagepng($source2,$thumb_img);
			imagedestroy($source2);
    	}
		die;
  	}
}