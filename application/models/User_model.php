<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return $this->db->insert_id();
		else
			return FALSE;		
	}
	public function get_result($table_name='', $id_array='',$columns=array()){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;		
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	public function get_row($table_name='', $id_array='',$columns=array()){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}
	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		return $this->db->update($table_name, $data);		
	}
	public function delete($table_name='', $id_array=''){		
	 return $this->db->delete($table_name, $id_array);
	}
	public function login($email,$password,$user_type=FALSE,$admin='',$role_check='',$check_social_login='')
	{
		$email		=trim($email);
		$password	=trim($password);
		$data_array	= array('user_email'=>$email,'user_status'=>1);
		if(!empty($role_check) && $role_check == 3)
		{
			$data_array['user_role'] = 3;
		}else{
			$data_array['user_role !='] = 3;
		}
		$query_get	= $this->db->get_where('users',$data_array);
		$count		= $query_get->num_rows();
		$res		= $query_get->row_array();
		$salt		= $res['salt'];
		//echo $this->db->last_query();
		//exit;
		if($count==1)
		 {
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_email', $email);	
			if(!empty($admin) && $admin == 1)
			{
				$this->db->where('password',$password);
			}elseif(!empty($check_social_login) && $check_social_login == 1)
			{
				$this->db->where('password',$res['password']);
			}else{
				$this->db->where('password', sha1($salt.sha1($salt.sha1($password))));
			} 
			
			if(!empty($role_check) && $role_check == 3)
			{
				$this->db->where('user_role',3);
			}else{
				$this->db->where('user_role !=',3);
			}
			$query = $this->db->get();
			//echo $this->db->last_query();
			//exit;
			$sql= $query;
			$check_count = $sql->num_rows();
			$result = $sql->row();
			if($check_count == 1)
			{
				$user_data = array(
					'id'			=> $sql->row()->user_id,
					'user_role'		=> $sql->row()->user_role,
					'first_name'	=> $sql->row()->user_first_name,
					'last_name'		=> $sql->row()->user_last_name,
					'email'			=> $sql->row()->user_email,
					'logged_in'		=> TRUE
					);

				if($user_type=='superadmin' && ($sql->row()->user_role==0 || $sql->row()->user_role == NULL || empty($sql->row()->user_role)))
				{
					$this->session->set_userdata('superadmin_info',$user_data);
					$this->update('users',array(
							'last_login_ip'			=> $this->input->ip_address(),
							'last_login_datetime'	=> date('Y-m-d h:i:s')
						),array(
							'user_id'=>$sql->row()->user_id)
						);
					return TRUE;
				}
				elseif($user_type=='user' && ($sql->row()->user_role==1 || $sql->row()->user_role == 2))
				{
					if($sql->row()->user_role==1){
						$this->session->set_userdata('medicaluser_logged_in',$user_data);
						$this->session->unset_userdata('physician_login_info');
					}elseif($sql->row()->user_role==2){
						$this->session->set_userdata('physician_login_info',$user_data);
						$this->session->unset_userdata('medicaluser_logged_in');
					}
					if( $sql->row()->user_role==1 || $sql->row()->user_role == 2){
						$this->update('users',array('last_login_ip' => $this->input->ip_address(),
							'last_login_datetime' => date('Y-m-d h:i:s')),array('user_id'=>$sql->row()->user_id));
						return TRUE;
					}else{
						$this->session->set_flashdata('msg_error', 'Something get wrong try again....!');
						return FALSE;
					}
				}elseif($user_type=='photographer' && ($sql->row()->user_role==3 )){
					$this->session->set_userdata('photographer_info',$user_data);
					$this->update('users',array(
							'last_login_ip'			=> $this->input->ip_address(),
							'last_login_datetime'	=> date('Y-m-d h:i:s')
						),array(
							'user_id'=>$sql->row()->user_id)
						);
					//echo $this->db->last_query();
					//exit;
					return TRUE;
				}
				else{
					$this->session->set_flashdata('msg_error', 'Something get wrong try again....!');
					return FALSE;
				}
			}
			else
			{
				$this->session->set_flashdata('msg_error', 'Incorrect Email or Password.');
				return FALSE;
			}
		}
		else
		{
			$this->session->set_flashdata('msg_error', 'Incorrect Email.');
			return FALSE;
		}
	}
	public function user_result($offset='', $per_page=''){
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('physicians', 'physicians.user_id = users.user_id');
		$this->db->where('users.user_role', 2);	
        if(!empty($_GET)){
        	
			 if(!isset($_GET['UserId']) || !isset($_GET['UserName']) || !isset($_GET['Email']) || !isset($_GET['order']) || !isset($_GET['status'])){
			 		redirect('backend/listusers/physician');
			 }
			
			if(!empty($_GET['UserId']))
			{		              
			     $this->db->where('users.user_id',trim($this->input->get('UserId')));	
		      
			}
            if(!empty($_GET['Email']))
			{		              
			     $this->db->like('users.user_email',trim($this->input->get('Email')));	
		      
			}
   			if(!empty($_GET['UserName']))
			{		
			     
				$search_term = trim($_GET['UserName']);
				$j = 1;
				$keywords = explode(" ", preg_replace("/\s+/", " ", $search_term));
				$get_val = count($keywords);
				$wherelike = '';
				foreach($keywords as $keyword){
				if($j == 1)
				  {
				  	$wherelike .= '(';
				  }
				 $wherelike .= '`users.user_first_name` LIKE "%'.trim($keyword).'%"  or `users.user_last_name` LIKE "%'.trim($keyword).'%"';
				  if($j< $get_val)
				  {
				  	$wherelike .= ' or ';
				  }elseif($j == $get_val)
				  {
				  	 $wherelike .= ')' ;
				  }
				  $j++;
				}
				$like_condition = $wherelike;
			 	$this->db->where($like_condition);


			
			}

			
		    if(!empty($_GET['status']) && $_GET['status'] !="All")
			{
			 
			 if($_GET['status'] ==1)
			 {
			 	$status_val = 1;
			 }
			 if($_GET['status'] ==2)
			 {
			 	$status_val = 2;
			 }
			 if($_GET['status'] ==3)
			 {
			 	$status_val = 3;
			 }
			 $this->db->where('users.user_status', $status_val);	
			
		   }
			
			if($_GET['order']){	
			 		
				$column_name = 'physician_id';	
			  	$order =$this->input->get('order'); 
			  	 $this->db->order_by($column_name,$order);
			}
		}
		else{
			 $this->db->order_by('physician_id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	

			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function physician_detail($id='')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->join('physicians', 'physicians.user_id = users.user_id');
		$this->db->where('users.user_role', 2);	
		$this->db->where('users.user_id', $id);	
		$query = $this->db->get();
		if($query->num_rows()>0)
				return $query->row();
			else
				return FALSE;
	}
	public function medical_result($offset='', $per_page=''){
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_role', 1);	
        if(!empty($_GET)){
        	
			 if(!isset($_GET['UserId']) || !isset($_GET['UserName']) || !isset($_GET['Email']) || !isset($_GET['order']) || !isset($_GET['status'])){
			 		redirect('backend/listusers/medical');
			 }
			
			if(!empty($_GET['UserId']))
			{		              
			     $this->db->where('user_id',trim($this->input->get('UserId')));	
		      
			}
            if(!empty($_GET['Email']))
			{		              
			     $this->db->like('user_email',trim($this->input->get('Email')));	
		      
			}
   			if(!empty($_GET['UserName']))
			{		
			     
				$search_term = trim($_GET['UserName']);
				$j = 1;
				$keywords = explode(" ", preg_replace("/\s+/", " ", $search_term));
				$get_val = count($keywords);
				$wherelike = '';
				foreach($keywords as $keyword){
				if($j == 1)
				  {
				  	$wherelike .= '(';
				  }
				 $wherelike .= '`user_first_name` LIKE "%'.trim($keyword).'%"  or `user_last_name` LIKE "%'.trim($keyword).'%"';
				  if($j< $get_val)
				  {
				  	$wherelike .= ' or ';
				  }elseif($j == $get_val)
				  {
				  	 $wherelike .= ')' ;
				  }
				  $j++;
				}
				$like_condition = $wherelike;
			 	$this->db->where($like_condition);


			
			}

			
		    if(!empty($_GET['status']) && $_GET['status'] !="All")
			{
			 
			 if($_GET['status'] ==1)
			 {
			 	$status_val = 1;
			 }
			 if($_GET['status'] ==2)
			 {
			 	$status_val = 2;
			 }
			 if($_GET['status'] ==3)
			 {
			 	$status_val = 3;
			 }
			 $this->db->where('user_status', $status_val);	
			
		   }
			
			if($_GET['order']){	
			 	
				$column_name = 'user_id';	
			  	$order =$this->input->get('order'); 
			  	$this->db->order_by($column_name,$order);
			}
		}
		else{
			 $this->db->order_by('user_id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	

			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function postjob_result($offset='', $per_page=''){
		
		$this->db->select('post_jobs.*,users.user_name_title,users.user_first_name,users.user_last_name');
		$this->db->from('post_jobs');
		$this->db->join('users','post_jobs.user_id=users.user_id');	
        if(!empty($_GET)){

			 if(!isset($_GET['Id']) || !isset($_GET['userid']) || !isset($_GET['specialty']) || !isset($_GET['order']) || !isset($_GET['FacilityName'])||!isset($_GET['status'])){
			 		redirect('backend/jobs/all_jobs');
			 }
			
			if(!empty($_GET['Id']))
			{		              
			     $this->db->where('post_jobs.id',trim($this->input->get('Id')));	
		      
			}
             if(!empty($_GET['userid']))
			{		              
			     $this->db->where('post_jobs.user_id',trim($this->input->get('userid')));	
		      
			}
			
   			if(!empty($_GET['specialty']) && $_GET['specialty'] !="All")
			{		
			    
				 $this->db->where('post_jobs.medical_specialty_name',trim($this->input->get('specialty')));	
			
			}
			if(!empty($_GET['title']))
			{		
			     
				 $this->db->like('post_jobs.listing_title',trim($this->input->get('title')));	
			
			}
			if(!empty($_GET['FacilityName']))
			{		
			     
				 $this->db->like('post_jobs.facility_name',trim($this->input->get('FacilityName')));	
			
			}
			

			
		    if(!empty($_GET['status']) && $_GET['status'] !="All")
			{
			 
			 if($_GET['status'] ==1)
			 {
			 	$status_val = 1;
			 }
			 if($_GET['status'] ==2)
			 {
			 	$status_val = 2;
			 }
			 if($_GET['status'] ==3)
			 {
			 	$status_val = 3;
			 }
			 if($_GET['status'] ==4)
			 {
			 	$status_val = 4;
			 }
			 if($_GET['status'] ==5)
			 {
			 	$status_val = 0;
			 }
			 $this->db->where('post_jobs.status', $status_val);	
			
		   }			
		
			if($_GET['order']){	
			 	
				$column_name = 'post_jobs.id';	
			  	$order =$this->input->get('order'); 
			  	$this->db->order_by($column_name,$order);
			}
		}

		else{
			 $this->db->order_by('post_jobs.id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	
			//echo $this->db->last_query();
			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
    public function all_receipts_information_for_payment_receipts($offset='', $per_page='')
    {
        $this->db->select('*');
        $this->db->from('post_jobs');
        $this->db->join('payment', 'payment.job_id = post_jobs.id');
        if(!empty($_GET)){
            
             if(!isset($_GET['transaction_id']) || !isset($_GET['job_id'])|| !isset($_GET['user_id']) || !isset($_GET['order'])){
                    redirect('backend/payment_receipts/payment_list');
             }
            
            if(!empty($_GET['transaction_id']))
            {                     
                 $this->db->where('payment.txn_id',trim($this->input->get('transaction_id'))); 
              
            }
             if(!empty($_GET['user_id']))
            {                     
                 $this->db->where('payment.user_id',trim($this->input->get('user_id'))); 
              
            }
            if(!empty($_GET['job_id']))
            {                     
                 $this->db->where('payment.job_id',trim($this->input->get('job_id')));  
              
            }                  
            
            if($_GET['order']){ 
                
                $column_name = 'payment_id';    
                $order =$this->input->get('order'); 
                $this->db->order_by($column_name,$order);
            }
        }
        else{
             $this->db->order_by('payment_id','DESC');
        }

        if($offset>=0 && $per_page>0)
        {
            $this->db->limit($per_page,$offset);
            $query = $this->db->get();  
            
            if($query->num_rows()>0)
                return $query->result_array();
            else
                return FALSE;
        }
        else
        {
            return $this->db->count_all_results();
        }
    }
	public function all_receipts_information($offset='', $per_page='')
	{
		$this->db->select('*');
		$this->db->from('post_jobs');
		$this->db->join('payment', 'payment.job_id = post_jobs.id');
		if(!empty($_GET)){
        	
			 if(!isset($_GET['UserId']) || !isset($_GET['JobTitle']) || !isset($_GET['YolomdVerify']) || !isset($_GET['order']) || !isset($_GET['status'])){
			 		redirect('backend/listusers/payment');
			 }
			
			if(!empty($_GET['UserId']))
			{		              
			     $this->db->where('payment.user_id',trim($this->input->get('UserId')));	
		      
			}
            if(!empty($_GET['JobTitle']))
			{		              
			     $this->db->like('payment.listing_title',trim($this->input->get('JobTitle')));	
		      
			}
   			if(!empty($_GET['YolomdVerify']))
			{		
			     
				 $this->db->like('payment.listing_title',trim($this->input->get('YolomdVerify')));	


			
			}

			
		    if(!empty($_GET['status']) && $_GET['status'] !="All")
			{
			 
			 if($_GET['status'] ==1)
			 {
			 	$status_val = 1;
			 }
			 if($_GET['status'] ==2)
			 {
			 	$status_val = 2;
			 }
			
			 $this->db->where('status', $status_val);	
			
		   }
			
			if($_GET['order']){	
			 	
				$column_name = 'payment_id';	
			  	$order =$this->input->get('order'); 
			  	$this->db->order_by($column_name,$order);
			}
		}
		else{
			 $this->db->order_by('payment_id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	

			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function receipts_information_job($user_id)
	{
		$this->db->select('payment.*');
		$this->db->from('post_jobs');
		$this->db->where('payment.user_id',$user_id);
		$this->db->join('payment', 'payment.job_id = post_jobs.id');
		$this->db->order_by('payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->row_array();
			else
				return FALSE;
	}
	public function photographer_result($offset='', $per_page=''){
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_role', 3);	
        if(!empty($_GET)){
        	
			 if(!isset($_GET['UserId']) || !isset($_GET['UserName']) || !isset($_GET['Email']) || !isset($_GET['order']) || !isset($_GET['status'])){
			 		
			 		redirect('backend/photographer');
			 }
			
			if(!empty($_GET['UserId']))
			{		              
			     $this->db->where('user_id',trim($this->input->get('UserId')));	
		      
			}
            if(!empty($_GET['Email']))
			{		              
			     $this->db->like('user_email',trim($this->input->get('Email')));	
		      
			}
   			if(!empty($_GET['UserName']))
			{		
			     
				$search_term = trim($_GET['UserName']);
				$j = 1;
				$keywords = explode(" ", preg_replace("/\s+/", " ", $search_term));
				$get_val = count($keywords);
				$wherelike = '';
				foreach($keywords as $keyword){
				if($j == 1)
				  {
				  	$wherelike .= '(';
				  }
				 $wherelike .= '`user_first_name` LIKE "%'.trim($keyword).'%"  or `user_last_name` LIKE "%'.trim($keyword).'%"';
				  if($j< $get_val)
				  {
				  	$wherelike .= ' or ';
				  }elseif($j == $get_val)
				  {
				  	 $wherelike .= ')' ;
				  }
				  $j++;
				}
				$like_condition = $wherelike;
			 	$this->db->where($like_condition);


			
			}

			
		    if(!empty($_GET['status']) && $_GET['status'] !="All")
			{
			 
			 if($_GET['status'] ==1)
			 {
			 	$status_val = 1;
			 }
			 if($_GET['status'] ==2)
			 {
			 	$status_val = 2;
			 }
			 if($_GET['status'] ==3)
			 {
			 	$status_val = 3;
			 }
			 $this->db->where('user_status', $status_val);	
			
		   }
			
			if($_GET['order']){	
			 	
				$column_name = 'user_id';	
			  	$order =$this->input->get('order'); 
			  	$this->db->order_by($column_name,$order);
			}
		}
		else{
			 $this->db->order_by('user_id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	

			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function verified_jobs_result($offset='', $per_page=''){
		
		$this->db->select('post_jobs.*,users.user_name_title,users.user_first_name,users.user_last_name');
		$this->db->from('post_jobs');
		$this->db->join('users','post_jobs.user_id=users.user_id');	
		$this->db->where('post_jobs.job_verify',1);
        if(!empty($_GET)){

			 if(!isset($_GET['Id']) || !isset($_GET['user_name']) || !isset($_GET['specialty']) || !isset($_GET['order'])){
			 		redirect('backend/photographer/verified_jobs');
			 }
			
			if(!empty($_GET['Id']))
			{		              
			     $this->db->where('post_jobs.id',trim($this->input->get('Id')));	
		      
			}
             
			if(!empty($_GET['user_name']))
			{		
			     
				$search_term = trim($_GET['user_name']);
				$j = 1;
				$keywords = explode(" ", preg_replace("/\s+/", " ", $search_term));
				$get_val = count($keywords);
				$wherelike = '';
				foreach($keywords as $keyword){
				if($j == 1)
				  {
				  	$wherelike .= '(';
				  }
				 $wherelike .= '`users.user_first_name` LIKE "%'.trim($keyword).'%"  or `users.user_last_name` LIKE "%'.trim($keyword).'%"';
				  if($j< $get_val)
				  {
				  	$wherelike .= ' or ';
				  }elseif($j == $get_val)
				  {
				  	 $wherelike .= ')' ;
				  }
				  $j++;
				}
				$like_condition = $wherelike;
			 	$this->db->where($like_condition);


			
			}

   			if(!empty($_GET['specialty']) && $_GET['specialty'] !="All")
			{		
			    
				 $this->db->where('post_jobs.medical_specialty_name',trim($this->input->get('specialty')));	
			
			}
			if(!empty($_GET['title']))
			{		
			     
				 $this->db->like('post_jobs.listing_title',trim($this->input->get('title')));	
			
			}
			
			if($_GET['order']){	
			 	
				$column_name = 'post_jobs.id';	
			  	$order =$this->input->get('order'); 
			  	$this->db->order_by($column_name,$order);
			}
		}

		else{
			 $this->db->order_by('post_jobs.id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	
			//echo $this->db->last_query();
			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	
	public function photographer_jobs_result($offset='', $per_page=''){
		$this->db->select('post_jobs.*');
		$this->db->from('assign_user');
		$this->db->join('post_jobs', 'post_jobs.id = assign_user.job_id');
		$this->db->where('assign_user.assign_name', photographer_id());	
        if(!empty($_GET)){

			 if(!isset($_GET['title']) || !isset($_GET['specialty']) || !isset($_GET['order'])){
			 		redirect('backend/photographer_jobs');
			 }
			
   			if(!empty($_GET['specialty']) && $_GET['specialty'] !="All")
			{		
			    
				 $this->db->like('post_jobs.medical_specialty_name',trim($this->input->get('specialty')));	
			
			}
			if(!empty($_GET['title']))
			{		
			     
				 $this->db->like('post_jobs.listing_title',trim($this->input->get('title')));	
			
			}
			
			if($_GET['order']){	
			 	
				$column_name = 'assign_user.assign_id';	
			  	$order =$this->input->get('order'); 
			  	$this->db->order_by($column_name,$order);
			}
		}

		else{
			 $this->db->order_by('assign_user.assign_id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();	
			//echo $this->db->last_query();
			if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function get_messages_detail_admin($msg_id=''){
		$where="(parent_id=".$msg_id." OR msg_id=".$msg_id.")";
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('msg_id','ASC');
		$query=$this->db->get('jobs_messages');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	
}
//user_model end