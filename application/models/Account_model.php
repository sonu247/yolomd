<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_model extends CI_Model {	

public function login($data='',$user_type=FALSE){	
	
	$query = $this->db->get_where('users',$data);
	if($query->num_rows()==1){
		
		$set_status=1;
		$user_data = array(
		'id' 			=> $query->row()->id,
		'user_role' 	=> $query->row()->user_role,
		'first_name' 	=> $query->row()->first_name,
		'last_name'		=> $query->row()->last_name,
		'email'			=> $query->row()->email,
		'last_ip' 		=> $query->row()->last_ip,
		'last_login' 	=> $query->row()->last_login,
		'set_status'	=> $set_status,
		'logged_in' 	=> TRUE);
		if($user_type=='superadmin')
			$this->session->set_userdata('superadmin_info',$user_data);
		elseif($user_type=='store')				
			$this->session->set_userdata('physician_login_info',$user_data);
		else
			$this->session->set_userdata('user_info',$user_data);

		$store_admin_profile_data = array(
		'set_status'	=> $set_status);
		$this->session->set_userdata('store_admin_profile_info',$store_admin_profile_data);

		$this->update('users',array('last_ip' => $this->input->ip_address(),'last_login' => date('Y-m-d h:i:s')),array('id'=>$query->row()->id));		
		return TRUE;
	}else{
		$this->session->set_flashdata('theme_danger', 'Incorrect Email or Password.');
		return FALSE;
	}
}	

public function insert($table_name='',  $data=''){
	$query=$this->db->insert($table_name, $data);
	if($query)
		return $this->db->insert_id();
	else
		return FALSE;		
}
public function get_result($table_name='', $id_array='',$columns=array(),$order_by=array(),$limit=''){
	if(!empty($columns)):
		$all_columns = implode(",", $columns);
		$this->db->select($all_columns);
	endif;
	if(!empty($order_by)):			
		$this->db->order_by($order_by[0], $order_by[1]);
	endif; 
	if(!empty($id_array)):		
		foreach ($id_array as $key => $value){
			$this->db->where($key, $value);
		}
	endif;	
	if(!empty($limit)):	
		$this->db->limit($limit);
	endif;	
	$query=$this->db->get($table_name);
	if($query->num_rows()>0)
		return $query->result();
	else
		return FALSE;
}
public function get_row($table_name='', $id_array='',$columns=array(),$order_by=array()){
	
	if(!empty($columns)):
		$all_columns = implode(",", $columns);
		$this->db->select($all_columns);
	endif; 
	if(!empty($id_array)):		
		foreach ($id_array as $key => $value){
			$this->db->where($key, $value);
		}
	endif;
	if(!empty($order_by)):			
		$this->db->order_by($order_by[0], $order_by[1]);
	endif; 
	$query=$this->db->get($table_name);
	if($query->num_rows()>0)
		return $query->row();
	else
		return FALSE;
}
public function update($table_name='', $data='', $id_array=''){
	if(!empty($id_array)):
		foreach ($id_array as $key => $value){
			$this->db->where($key, $value);
		}
	endif;
	return $this->db->update($table_name, $data);		
}

public function delete($table_name='', $id_array=''){		
 return $this->db->delete($table_name, $id_array);
}

public function password_check($data='',$user_id=''){  
	$query = $this->db->get_where('users',$data,array('id'=>$user_id));
		if($query->num_rows()>0)
		return TRUE;
	else{
		//$this->form_validation->set_message('password_check', 'The %s field can not match');
		return FALSE;
	}
}
public function statistics($table_name,$status='',$store_type=''){  
        $query1=$this->db->query('SELECT count(*) as total_rows FROM '.$table_name.' WHERE store_status='.$status.' AND user_id='.store_admin_id()); 
        return $query1->row();
    }
 
 public function statistics_store_total($table_name,$status='',$store_type=''){  
        $query1=$this->db->query('SELECT count(*) as total_rows FROM '.$table_name.' WHERE user_id='.store_admin_id()); 
        return $query1->row();
    }
}