<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model {	
	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return $this->db->insert_id();
		else
			return FALSE;		
	}
	public function get_result($table_name='', $id_array='',$columns=array(),$order_by=array(),$limit=''){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		if(!empty($order_by)):			
			$this->db->order_by($order_by[0], $order_by[1]);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;	
		if(!empty($limit)):	
			$this->db->limit($limit);
		endif;	
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
		public function get_row($table_name='', $id_array='',$columns=array(),$order_by=array()){
		
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($order_by)):			
			$this->db->order_by($order_by[0], $order_by[1]);
		endif; 
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}
	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		return $this->db->update($table_name, $data);		
	}
	public function delete($table_name='', $id_array=''){		
	 return $this->db->delete($table_name, $id_array);
	}
	
	public function password_check($data=''){  
		$query = $this->db->get_where('users',$data);
 		if($query->num_rows()>0)
			return TRUE;
		else{
			//$this->form_validation->set_message('password_check', 'The %s field can not match');
			return FALSE;
		}
	}
	
	public function get_nav_menu($slug='',$is_location=FALSE){		
		$this->db->select('menus.id,menus.menu_label,menus.menu_link');
		$this->db->order_by('menus.menu_order','asc');
		$this->db->group_by('menus.menu_order');
		$this->db->from('menus');
		$this->db->join('menu_categories','menu_categories.id=menus.menu_category_id','right');
		if($is_location)
			$this->db->where('menus.menu_location',$slug);
		else
			$this->db->where('menu_categories.menu_category_slug',$slug);
		
		$query = $this->db->get();
		if($query->num_rows()>0){
			//return $query->result();
			$main_menu=array();
			foreach ($query->result() as $row):
				$main_menu[]= (object) array(
					'id' =>$row->id,
					'menu_label' =>$row->menu_label,
					'menu_link' =>$row->menu_link,
					'child_menu' => $this->get_result('menus',array('menu_parent_id'=>$row->id),array('id','menu_label','menu_link'),array('menu_order','asc')),
					);
			endforeach;
			//print_r($main_menu);
			//die;
			return $main_menu;
		}
		else
			return FALSE;
	}
	public function page($slug=''){
		if(!empty($slug)){
			$query = $this->db->get_where('pages',array('page_slug'=>$slug, 'status'=>'1'));
			if($query->num_rows()>0)
				return $query->row();
			else
				return FALSE;
		}else{
			return FALSE;
		}
	}	
	
	public function get_post($slug,$is_slug){
		if($is_slug)
			$where=array('id'=>$slug,'post_type'=>'post');
		else
			$where=array('post_slug'=>$slug,'post_type'=>'post');
	    return	$this->get_row('posts',$where,array('post_title','post_content'));	    
	}

	public function banner_result(){
		
		$this->db->select('*');
		$this->db->from('cms_home_page');
		$this->db->order_by('cms_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
		
	}
	public function city_result($offset='', $per_page=''){
		
		$this->db->select('*');
		$this->db->from('search_job_city');
		$this->db->order_by('search_city_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
		{
				return $query->result_array();
		}
		else
		{
				return FALSE;
		}
	}
	public function get_random_banner(){
		
		$this->db->order_by('id','RANDOM');
		$this->db->where('status',1);
		$query=$this->db->get('cms_home_page');
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}
	public function login($email,$password,$user_type=FALSE){
		$data  = array('user_email'=>$email);
		$query_get = $this->db->get_where('users',$data);
		$count = $query_get->num_rows();
		$res = $query_get->row_array();
		$salt = $res['salt'];
		if($count==1){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_email',$email);
		$this->db->where('password',sha1($salt.sha1($salt.sha1($password))));
		$sql = $this->db->get();	
		$check_count = $sql->num_rows();
		$result = $sql->row();
		if($check_count == 1)
		{
		$user_data = array('id' => $sql->row()->user_id,
		'user_role' => $sql->row()->user_role,
		'first_name' => $sql->row()->user_first_name,
		'last_name' => $sql->row()->user_last_name,
		'logged_in' => TRUE);
	
		if($user_role ==1){
		$this->session->set_userdata('physician_info',$user_data);
		}
		elseif($user_role ==2){
		$this->session->set_userdata('medical_facility_info',$user_data);
		$this->session->set_flashdata('msg_info', 'Welcome '. $sql->row()->user_name.', to YoloMD');
		}
		return TRUE;
		}else{
		$this->session->set_flashdata('msg_error', 'Incorrect Password.');
		return FALSE;
		}
		}else{
		$this->session->set_flashdata('msg_error', 'Incorrect Email.');
		return FALSE;
		}
	}

	public function alljobs($lat,$lon){

		$this->db->select('id,listing_title,job_verify,facility_state,latitude,longitude,facility_city,facility_postal_code,
		
		( 6371 * acos( cos(radians('.$lat.')) * cos( radians(`latitude`)) * 
		  	cos( radians( `longitude` ) - radians('.$lon.')) + sin( radians('.$lat.')) * sin( radians( `latitude` )) 
		    )) AS distance');
		$this->db->having('distance <', 50);
		/*$this->db->having('distance <='. 10);*/
		//$this->db->select('id,listing_title,job_verify,facility_state,latitude,longitude,facility_city,facility_postal_code');
		$this->db->where('status',1);
		$this->db->order_by('id','DESC');
		$query=$this->db->get('post_jobs');
		//echo $this->db->last_query();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function receipts_information()
	{
		$this->db->select('*');
		$this->db->from('post_jobs');
		$this->db->where('payment.user_id',medicaluser_id());
		$this->db->join('payment', 'payment.job_id = post_jobs.id');
		$this->db->order_by('payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
	}
	public function receipts_user_id($id='')
	{
		$this->db->select('*');
		$this->db->from('post_jobs');
		$this->db->where('payment.user_id',medicaluser_id());
		$this->db->where('payment.unique_id',$id);
		$this->db->join('payment', 'payment.job_id = post_jobs.id');
		//$this->db->join('payment_city', 'payment_city.unique_id = payment.unique_id');
		$this->db->order_by('payment.payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->row_array();
			else
				return FALSE;
	}
	function city_payment($id='')
	{
		$this->db->select('*');
		$this->db->from('payment');
		$this->db->where('payment.unique_id',$id);
		$this->db->join('payment_city', 'payment_city.unique_id = payment.unique_id');
		 $this->db->group_by('payment_city.city_id'); 
		$this->db->order_by('payment_city.payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result_array();
			else
				return FALSE;
	}
	public function get_result_highlight($city_subscription_id=''){
		$this->db->select('id,city_name,price');
		$this->db->from('location_data');
		$this->db->where('searching',1);
		$this->db->where('status',1);
		if($city_subscription_id){
			$wherecon="id NOT IN(".$city_subscription_id.")"; 
			$this->db->where($wherecon);
		}
		$this->db->order_by('city_name','ASC');
		$query=$this->db->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_result_city($city_subscription_id='',$search=''){
		$this->db->select('id,city_name,price');
		$this->db->from('location_data');
		$this->db->where('searching',0);
		$this->db->where('status',1);
		if($city_subscription_id){
			$wherecon="id NOT IN(".$city_subscription_id.")"; 
			$this->db->where($wherecon);
		}
		$this->db->like('city_name', $search, 'after');
		$this->db->order_by('city_name','ASC');
		$this->db->limit(20);
		$query=$this->db->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function city_subscription($id='')
	{
		$this->db->select('city_id,city_name');
		$this->db->from('payment_city');
		$this->db->where('job_id',$id);
		$where="(valid_date >= '".date('Y-m-d')."' or valid_date='0000-00-00')";
		$this->db->where($where);
		$this->db->where('user_id',medicaluser_id());
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
	}

	public function receipts_payment($id='')
	{
		$this->db->select('*');
		$this->db->from('post_jobs');
		$this->db->where('payment.payment_id',$id);
		$this->db->join('payment', 'payment.job_id = post_jobs.id');
		//$this->db->join('payment_city', 'payment_city.unique_id = payment.unique_id');
		$this->db->order_by('payment.payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->row_array();
			else
				return FALSE;
	}

	public function get_count($table_name='', $id_array='',$columns=array(),$order_by=array(),$limit=''){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		if(!empty($order_by)):			
			$this->db->order_by($order_by[0], $order_by[1]);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;	
		if(!empty($limit)):	
			$this->db->limit($limit);
		endif;	
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->num_rows();
		else
			return FALSE;
	}
	
	public function get_province($table_name='', $id_array='',$columns=array(),$order_by=array(),$group_by='',$limit=''){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		$this->db->group_by($group_by); 
		if(!empty($order_by)):			
			$this->db->order_by($order_by[0], $order_by[1]);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;	
		if(!empty($limit)):	
			$this->db->limit($limit);
		endif;	
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_city($table_name='', $id_array='',$columns=array(),$order_by=array(),$group_by='',$limit=''){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		$this->db->group_by($group_by); 
		if(!empty($order_by)):			
			$this->db->order_by($order_by[0], $order_by[1]);
		endif; 
		if(!empty($id_array)):		
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;	
		if(!empty($limit)):	
			$this->db->limit($limit);
		endif;	
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function favourite_job_res($user_id)
	{
		$this->db->select('favourite_job.*,post_jobs.listing_title,post_jobs.uploadImageArray,post_jobs.medical_specialty_name');
		$this->db->from('favourite_job');
		$this->db->where('favourite_job.user_id',$user_id);

		$this->db->join('post_jobs', 'post_jobs.id = favourite_job.job_id');
		$this->db->where('post_jobs.status',1);
		$this->db->order_by('favourite_job.favourite_job_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
	}

	public function get_messages($coulombname='',$user_id=''){
		
		$this->db->select('jobs_messages.*,post_jobs.medical_specialty_name,post_jobs.listing_title,post_jobs.facility_city');
		$this->db->from('jobs_messages');
		$this->db->where('jobs_messages.'.$coulombname,$user_id);
		$this->db->where('jobs_messages.parent_id',0);
		if(medicaluser_logged_in()){
	        $this->db->where('jobs_messages.medical_delete',0);
	    }elseif(physician_logged_in()){
	        $this->db->where('jobs_messages.physician_delete',0);
	    }
		$this->db->join('post_jobs', 'jobs_messages.job_id = post_jobs.id');
		$this->db->order_by('jobs_messages.msg_id','DESC');
		$query=$this->db->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_messages_detail($msg_id='',$user_id=''){
		$where="(sender_id=".$user_id." OR receiver_id=".$user_id.") AND (parent_id=".$msg_id." OR msg_id=".$msg_id.")";
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('msg_id','ASC');
		$query=$this->db->get('jobs_messages');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_unreadmessages($msg_id='',$receiver_id=''){
		$where="(receiver_id=".$receiver_id.") AND (parent_id=".$msg_id." OR msg_id=".$msg_id.")";
		$this->db->select('msg_id');
		$this->db->where('read_status',1);
		$this->db->where($where);
		if(medicaluser_logged_in()){
	        $this->db->where('medical_delete',0);
	    }elseif(physician_logged_in()){
	        $this->db->where('physician_delete',0);
	    }
		$this->db->order_by('msg_id','DESC');
		$query=$this->db->get('jobs_messages');
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	function count_rows($tablename='',$where='',$fields='',$orwhere='')
    {
        $this->db->select($fields);
        if(!empty($where))
        {
        	 $this->db->where($where); 
        }
        if(!empty($orwhere)){
            $this->db->or_where($orwhere); 
        } 
        $query = $this->db->get($tablename);
        return $query->num_rows();
    }

    public function active_payment_city($city_name)
	{
		$this->db->select('pj.id as job_id');
		$this->db->from('post_jobs as pj');
		//$this->db->join('payment_city as pc', 'pc.job_id = pj.id', 'left');
		// if($city_name){
		// 	$this->db->like('pc.city_name',$city_name);
		// }
		$this->db->where('pj.status',1);
		//$this->db->where('pc.payment_status',1);
		//$where="pc.valid_date >= '".date('Y-m-d')."'";
		//$this->db->group_by('pc.job_id'); 
		$this->db->where($where);
		$this->db->order_by('pj.id', 'DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	public function get_jobs_id($jobs_id_city, $canada_province, $medical_specialty, $Position, $city_postal_code,$lat='',$lon='')
	{
		if($Position && $Position!='Any'){
			$this->db->where('post_jobs.position',trim($Position));
		}
		if($city_postal_code){
			//$where="(post_jobs.facility_city LIKE '%".trim($city_postal_code)."%' OR facility_postal_code LIKE '%".trim($city_postal_code)."%')";
			//$this->db->where($where);
		}
		if($medical_specialty && $medical_specialty!='All'){
			$this->db->where('post_jobs.medical_specialty_name', trim($medical_specialty));
		}
		if($canada_province && $canada_province!='All'){
			$this->db->where('facility_state', trim($canada_province));
		}
		if(!empty($lat) && !empty($lon))
		{
			$this->db->having('distance <', 650);
		}
		if(!empty($lat) && !empty($lon))
		{
			$this->db->select('id as job_id,( 6371 * acos( cos(radians('.$lat.')) * cos( radians(`latitude`)) * 
			  	cos( radians( `longitude` ) - radians('.$lon.')) + sin( radians('.$lat.')) * sin( radians( `latitude` )) 
			    )) AS distance');
		}else{
			$this->db->select('id as job_id');
		}	
		
		$this->db->where('status',1);
		$this->db->from('post_jobs');
		// if($jobs_id_city){
		// 	$wherecon="id NOT IN(".$jobs_id_city.")"; 
		// 	$this->db->where($wherecon);
		// }
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();	
		//echo $this->db->last_query();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}

	function jobs($jobs_id,$per_page,$offset=''){
		//public $_ci_cached_vars;
		//$this->db->_protect_identifiers = FALSE; 
		
		$this->db->select('post_jobs.id,post_jobs.facility_postal_code,post_jobs.listing_title,post_jobs.job_verify,post_jobs.uploadImageArray,post_jobs.facility_state,post_jobs.latitude,post_jobs.longitude,post_jobs.facility_city');
			
		$this->db->where('post_jobs.status',1);
		$this->db->order_by('post_jobs.id','desc');
		if($jobs_id){
			$wherecon="id IN(".$jobs_id.")"; 
			$this->db->where($wherecon);
			
		}
		
		$this->db->limit($per_page,$offset);
		/*SELECT ps.id, pc.payment_id FROM `post_jobs` as ps left join payment_city as pc on ps.id=pc.job_id where pc.valid_date >='2016-09-03' group by ps.id order by pc.payment_id desc*/
		
		
		$query=$this->db->get('post_jobs');
		//print_r($this->db->last_query());die;
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	function check_payment_status($id='')
	{
		$this->db->select('*');
		$this->db->from('payment');
		$this->db->where('payment.job_id',$id);
		$this->db->join('payment_city', 'payment_city.unique_id = payment.unique_id');
		$where="payment_city.valid_date >= '".date('Y-m-d')."'";
		$this->db->where($where);
		$this->db->order_by('payment_city.payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->num_rows();
			else
				return FALSE;
	}
	public function statistics_jobs()
	{
		$this->db->select('post_jobs.*');
		$this->db->from('post_jobs');
		$this->db->where('payment.user_id',medicaluser_id());
		$this->db->join('payment', 'payment.job_id = post_jobs.id');
		$this->db->join('payment_city','payment_city.unique_id = payment.unique_id');
		$where="payment_city.valid_date >= '".date('Y-m-d')."'";
		$this->db->where($where);
		$this->db->group_by('payment.job_id'); 
		$this->db->order_by('payment.payment_id','DESC');
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
	}
	public function check_message_exists($msg_id)
	{
		$this->db->select('*');
		$this->db->where('msg_id',$msg_id); 
        $where = "(physician_delete = '1' or medical_delete = '1')";
        $this->db->where($where); 
        $query = $this->db->get('jobs_messages');
        return $query->num_rows();
	}
	public function update_msg($table_name='', $data='', $msg_id=''){
		$where="msg_id  = $msg_id or parent_id = $msg_id";
	    $this->db->where($where);
		return $this->db->update($table_name, $data);		
	}
	public function get_result_zipcode($city_name='',$search='',$stateval=''){
		$this->db->select('id,postal_code');
		$this->db->from('location_complete_data');
		//if(!empty($stateval))
			//$this->db->where('province_name', $stateval);
		$this->db->where('city_name', $city_name);
		$this->db->like('postal_code', $search, 'after');
		$this->db->order_by('city_name','ASC');
		$query=$this->db->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	public function all_verified_jobs()
	{
		$this->db->select('*');
		$this->db->from('post_jobs');
		$this->db->join('assign_user', 'assign_user.job_id = post_jobs.id');
		$this->db->where('post_jobs.user_id',medicaluser_id());
		$this->db->where('post_jobs.job_verify',1);
		$this->db->where('assign_user.marked_complete',1);
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
	}
	public function get_group_by_city($where='')
	{
		$this->db->select('*');
		$this->db->from('location_data');
		$this->db->where($where);
		$this->db->group_by('searching'); 
		$query = $this->db->get();	
		if($query->num_rows()>0)
				return $query->row();
			else
				return FALSE;
	}
	
	
}
