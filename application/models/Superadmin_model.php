<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Superadmin_model extends CI_Model {
	public function insert($table_name='',  $data=''){
		$query=$this->db->insert($table_name, $data);
		if($query)
			return  $this->db->insert_id();
		else
			return FALSE;
	}
	public function get_result($table_name='', $id_array='',$columns=array(),$order_by=array(),$limit=''){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		if(!empty($order_by)):
			$this->db->order_by($order_by[0], $order_by[1]);
		endif;
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($limit)):
			$this->db->limit($limit);
		endif;
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->result();
		else
			return FALSE;
	}
	public function get_row($table_name='', $id_array='',$columns=array(),$order_by=array()){
		if(!empty($columns)):
			$all_columns = implode(",", $columns);
			$this->db->select($all_columns);
		endif;
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		if(!empty($order_by)):			
			$this->db->order_by($order_by[0], $order_by[1]);
		endif; 
		$query=$this->db->get($table_name);
		if($query->num_rows()>0)
			return $query->row();
		else
			return FALSE;
	}
	public function update($table_name='', $data='', $id_array=''){
		if(!empty($id_array)):
			foreach ($id_array as $key => $value){
				$this->db->where($key, $value);
			}
		endif;
		return $this->db->update($table_name, $data);
	}
	public function delete($table_name='', $id_array=''){
		return $this->db->delete($table_name, $id_array);
	}
	public function get_result_with_pagination($offset='', $per_page='',$tablename){
		$this->db->from($tablename);
		if($offset>=0 && $per_page>0){
			$this->db->limit($per_page,$offset);
			$this->db->order_by('id','desc');
			$query = $this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			return $this->db->count_all_results();
		}
	}
	public function get_result_with_pagination_msg($offset='', $per_page='',$tablename){
		$this->db->where('parent_id',0);
		$this->db->from($tablename);
		if($offset>=0 && $per_page>0){
			$this->db->limit($per_page,$offset);
			$this->db->order_by('id','desc');
			$query = $this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}else{
			return $this->db->count_all_results();
		}
	}
	

	 public function email_templates_model($offset='', $per_page=''){
		$this->db->from('email_templates');
		if ($this->input->get('title')) 
		{
			$this->db->like('template_name',$this->input->get('title'));
		}
		/*
		if ($this->input->get('status')=='1' || $this->input->get('status')=='0') 
		{
			$this->db->like('status',$this->input->get('status'));
		}
		if ($this->input->get('order')) 
		{
			$this->db->order_by('id',$this->input->get('order'));
		}
		else
		{*/
			$this->db->order_by('id','ASC');
		//}
		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$this->db->order_by('id','asc');
			$query = $this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}

	public function faq_model($offset='', $per_page=''){
		$this->db->from('faq');
		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$this->db->order_by('order_by','asc');
			$query = $this->db->get();
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function contactus_model($offset='', $per_page=''){
		$this->db->from('contact_us');
		
        if(!empty($_GET)){
			 if(!isset($_GET['name']) || !isset($_GET['subject'])  || !isset($_GET['order'])){
			 		redirect('backend/contactus');
			 }
			
			if(!empty($_GET['name']))
			{		              
			     $this->db->like('name',trim($this->input->get('name')));	
		      
			}
            
            if(!empty($_GET['subject']))
			{		
			     $this->db->like('subject',trim($this->input->get('subject',TRUE)));
			
			}

			if($_GET['order']){			
				$column_name = 'id';	
			  	$order =$this->input->get('order'); 
			  	 $this->db->order_by($column_name,$order);
			}
		}
		else{
			 $this->db->order_by('id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();			
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	public function get_all_message_flagged($offset='', $per_page=''){
		$this->db->from('jobs_messages');
		$this->db->where('flag',1); 
        if(!empty($_GET)){
			if(!isset($_GET['MsgId']) || !isset($_GET['JobId'])  || !isset($_GET['order'])){
			 		redirect('backend/messages');
			}
			
			if(!empty($_GET['MsgId']))
			{		              
			     $this->db->where('msg_id',trim($this->input->get('MsgId')));	
		      
			}
            
            if(!empty($_GET['JobId']))
			{		
			     $this->db->where('job_id',trim($this->input->get('JobId')));
			
			}
			if(!empty($_GET['status']) && $_GET['status'] != 'All'){			
				if($_GET['status'] == 2)
			  	  $this->db->where('admin_marked',0);
			  	else
			  		$this->db->where('admin_marked',$this->input->get('status'));
			}
			
			if($_GET['order']){			
				$column_name = 'msg_id';	
			  	$order =$this->input->get('order'); 
			  	 $this->db->order_by($column_name,$order);
			}
		}
		else{
			 $this->db->order_by('msg_id','DESC');
		}

		if($offset>=0 && $per_page>0)
		{
			$this->db->limit($per_page,$offset);
			$query = $this->db->get();			
			if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
		}
		else
		{
			return $this->db->count_all_results();
		}
	}
	function get_result_msg($id=''){
		$this->db->select('*');
		$this->db->from('contact_us');
		$this->db->join('contact_us_reply', 'contact_us_reply.contact_parent_id	 = contact_us.id');
		$this->db->where('contact_us_reply.contact_parent_id', $id);	
		$query = $this->db->get();
		if($query->num_rows()>0)
				return $query->result();
			else
				return FALSE;
	}
	
}