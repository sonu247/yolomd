'use strict';
angular
    .module('yolomd')
    // Angular File Upload module does not include this directive
    // Only for example
    /**
    * The ng-thumb directive
    * @author: nerv
    * @version: 0.1.2, 2014-01-09
    */
    .directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                 if(item.size >  10485760){
                    alert('Max file size is 10MB');
                     return false;
                 }
                 else
                 {
                    return angular.isObject(item) && item instanceof $window.File;
                 }
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                    EXIF.getData(this, function() {
                        if (this.exifdata && this.exifdata.Orientation) {
                            var Orientation = this.exifdata.Orientation;
                            var deg = 0;
                            if(Orientation==6){
                                deg = 90;     
                            }
                            else if(Orientation==3){
                                deg = 180;     
                            }
                            else if(Orientation==8){
                                deg = 270;     
                            }
                            canvas.css('transform', "rotate("+deg+"deg)");
                        }
                    });
                }
            }
        };
    }]);