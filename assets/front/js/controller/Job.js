angular.module('yolomd', ['ngSanitize'])
    .controller('jobs', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$location', '$window', '$timeout', '$stateParams', 'localStorageService', '$window', '$interval', '$timeout', function($scope, $rootScope, $http, $state, commonService, $location, $window, $timeout, $stateParams, localStorageService, $window, $interval, $timeout) {
        $scope.go_top = function() {
            window.scrollTo(0, 0);
        };
        
        //myMap();
        $scope.filter_toggle = '';
        $scope.toggle_text = 'Show Filters';
        $scope.toggle_fa = 'fa fa-angle-down';
        $scope.toggle_filters = function() {
            if ($scope.filter_toggle=='') {
                $scope.toggle_text = 'Hide Filters';
                $scope.filter_toggle = 'filter_toggle';
                $scope.toggle_fa = 'fa fa-angle-up';
            }
            else{
                $scope.toggle_text = 'Show Filters';
                $scope.filter_toggle = '';
                $scope.toggle_fa = 'fa fa-angle-down';
            }
        };
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.showGlobalLoader = false;
        $scope.marked_favourite = '';
        var job_parameter = {};
        var jobId = $stateParams.jobId;
        var Storagesearchdata = localStorageService.get('localStoragesearchdata');
        var isReset = localStorageService.get('isReset');
        $scope.showDetails = false;
        $scope.offset = 0;
        $rootScope.headerClass = $state.current.name;
        $scope.is_load_more = true;
        $scope.req_in_process = false;
        $scope.search_data = {};
        $scope.search_data.canada_province = 'All';
        $scope.search_data.medical_specialty = '';
        $scope.search_data.Position = 'Any';
        $scope.search_data.sorting = '1';
        $scope.search_data.citypostalcode = '';
        $scope.check_user_login_value = '';
        $scope.favorite_error = '';
        $scope.UpdateMassage = '';
        $scope.row_favourite = '';
        $scope.UpdatePostAlertMessage = '';
        $scope.saved_search_error = '';
        $scope.contact_error = '';
        $scope.checkdisable = true;
        $scope.map_listing = 'View Map';
        $scope.job_count = 0;
        //toggle map
        $scope.toggleclassfunctioning = '';
        $scope.localdatasave = {};
        var serch_set = false;

        var localBackupdata = localStorageService.get('localStoragebackdata');

        var localBackupCome = localStorageService.get('localStoragebackcome');
        if (localBackupdata) {
            $scope.search_data.canada_province = localBackupdata.canada_province;
            $scope.search_data.medical_specialty = localBackupdata.medical_specialty;
            $scope.search_data.Position = localBackupdata.Position;
            $scope.search_data.citypostalcode = localBackupdata.citypostalcode;
            localStorage.clear('localStoragebackdata');
            localStorage.clear('localStoragebackcome');
        }
        if ($( window ).width()<=800) {
            setTimeout(function(){ angular.element("#map_canvas").parent("div").css('display', 'none'); }, 3000);
        }
        $scope.toggeleclass = function(reset) {
            if (reset) {
                angular.element("#map_canvas").parent("div").css('display', 'none');
                $scope.toggleclassfunctioning = '';
                $scope.map_listing = 'View Map';
            }
            else{
                if ($scope.toggleclassfunctioning == 'mobile_preview_map') {
                    angular.element("#map_canvas").parent("div").css('display', 'none');
                    $scope.toggleclassfunctioning = '';
                    $scope.map_listing = 'View Map';

                } else {
                    angular.element("#map_canvas").parent("div").css('display', 'block');
                    $scope.toggleclassfunctioning = 'mobile_preview_map';
                    $scope.map_listing = 'View Listing';
                }
            }
        }
        if (Storagesearchdata) {
            $scope.search_data.canada_province = Storagesearchdata.provice_id;
            $scope.search_data.medical_specialty = Storagesearchdata.medical_specialties_id;
            $scope.search_data.Position = Storagesearchdata.position;
            if (Storagesearchdata.city_postal_code == 0) {
                $scope.search_data.citypostalcode = '';
            } else {
                $scope.search_data.citypostalcode = Storagesearchdata.city_postal_code;
            }
            if (Storagesearchdata.medical_specialties_id == 0 || Storagesearchdata.medical_specialties_id == 'All') {
                $scope.search_data.medical_specialty = '';
            } else {
                $scope.search_data.medical_specialty = Storagesearchdata.medical_specialties_id;
            }
            serch_set = true;
        }
        $scope.searchResultCity = function() {
            $scope.offset = 0;
            $scope.checkdisable = false;
            $scope.req_in_process = false;
            show_hide_loader('search_loader', false);
            // $timeout(function() {
                get_job_data("");

            // }, 2000);
            $scope.checkdisable = true;
        }
        $scope.searchResult = function(opt) {
            show_hide_loader('search_loader', false);
            $scope.offset = 0;
            $scope.req_in_process = false;
            // $timeout(function() {
                get_job_data(opt);
            // }, 2000);
        }

        function get_job_data(opt) {
            var url = site_url + 'webservices/jobs';
            $scope.showDetails = false;
            job_parameter.offset = 0;
            job_parameter.canada_province = $scope.search_data.canada_province;
            job_parameter.medical_specialty = $scope.search_data.medical_specialty;
            job_parameter.Position = $scope.search_data.Position;
            job_parameter.sorting = $scope.search_data.sorting;
            job_parameter.opt = opt;

            if ($scope.search_data.citypostalcode == 0) {
                job_parameter.citypostalcode = '';
            } else {
                job_parameter.citypostalcode = $scope.search_data.citypostalcode;
            }

            if ($scope.req_in_process) {
                return false;
            }
            $scope.req_in_process = true;
            show_hide_loader('search_loader', true);
            commonService.apiPost(url, job_parameter)
                .then(function(response) {
                        $scope.jobs = '';
                        show_hide_loader('search_loader', false);
                        $scope.check_user_login_value = response.Data.check_user_login_value;
                        var healthCenter = {};
                        var i = 0;
                        var latlng = {};
                        var firstLat = '';
                        var firstLng = '';
                        $scope.req_in_process = false;
                        $scope.jobs = response.Data.jobs_data;
                        $scope.job_count = response.Data.job_count;
                        $scope.offset = response.Data.offset;
                        $scope.is_load_more = response.Data.is_load_more;
                        if (Storagesearchdata) {
                            localStorage.clear('localStoragesearchdata');
                        }
                        angular.forEach($scope.jobs, function(value, key) {
                            if (value.latitude != '' || value.longitude) {
                                latlng = [value.listing_title, value.latitude, value.longitude, value.job_id, value.icon];
                                healthCenter[i] = latlng;
                                i++;
                            }
                        });
                        var hovericon = 'markerIconhover.svg';
                        if (healthCenter) {
                            var zoom_level = 10;
                            if (job_parameter.canada_province != 'All' && $scope.search_data.citypostalcode) {
                                zoom_level = 7;
                            } else if (job_parameter.canada_province != 'All') {
                                zoom_level = 5;
                            }
                            if (response.Data.latf && response.Data.lngf && response.Data.varible_to_zoom) {
                                firstLat = response.Data.latf;
                                firstLng = response.Data.lngf;
                                zoom_level = response.Data.varible_to_zoom;
                            }
                            initialize(healthCenter, firstLat, firstLng, hovericon, zoom_level);

                        }
                    },
                    function(err) {
                        show_hide_loader('search_loader', false);
                        if ($scope.offset == 0) {
                            var healthCenter = {};
                            var latlng = {};
                            var firstLat = '';
                            var firstLng = '';
                            $scope.jobs = '';
                            $scope.job_count = 0;
                            var hovericon = 'markerIconhover.svg';
                            initialize(healthCenter, firstLat, firstLng, hovericon, 10);
                        }
                    });
        }
        var defaultMsg = '';
        $scope.contact = {};
        var content_url = site_url + 'webservices/searching_content';
        commonService.apiGet(content_url).then(function(response) {
            $scope.contact.default_msg = response.Data.default_msg;
            defaultMsg = $scope.contact.default_msg;

            $scope.medical_specialty = response.Data.medical_specialty;
            $scope.canada_province = response.Data.canada_province;
            if (response.Data.canada_city)
                $scope.citypostalcode = response.Data.canada_city;
            if (isReset) {
                localStorage.clear('isReset');
            } else if (!localBackupdata && !serch_set) {
                if (response.Data.selectState) {
                    $scope.search_data.canada_province = response.Data.selectState;
                }
                if (response.Data.selectSpecialty) {
                    $scope.search_data.medical_specialty = response.Data.selectSpecialty;
                }
            }
        });
        $scope.contact.job_id = '';
        $scope.contact.datainsert = 0;
        $scope.resetVarible = function() {
            $scope.saved_search_error = '';
            $scope.contact_error = '';
            $scope.favorite_error = '';
        }
        $scope.setjobID = function(id, check_login, check_status, listing_title) {
            $scope.listing_name = '';
            $scope.listing_name = listing_title;
            if (check_login == 2) {
                show_hide_loader('message_loader', true);
                $scope.contact.default_msg = defaultMsg;
                $scope.contact.contact_subject = '';
                $scope.contact.msgSend = 0;
                $scope.contact.msg_send_id = 0;
                $scope.contact.job_id = id;
                $scope.contact.check_status = check_status;
                var url = site_url + 'webservices/check_msg_send';
                commonService.apiPost(url, $scope.contact).then(function(response) {
                        show_hide_loader('message_loader', false);
                        $scope.contact.msg_send_id = response.Data;
                        $scope.contact.msgSend = 1;
                    },
                    function(err) {
                        show_hide_loader('message_loader', false);
                    });
            }
            if (check_login == 3) {
                $scope.favorite_error = '';
                $scope.saved_search_error = '';
                $scope.contact_error = 'Please login as a Doctor user to use contact facility.';
            }
        }
        $scope.error_msg = '';
        $scope.submit_message = function() {
            show_hide_loader('message_loader', true);
            var url = site_url + 'webservices/save_message';
            commonService.apiPost(url, $scope.contact).then(function(response) {
                    show_hide_loader('message_loader', false);
                    $scope.contact.msgSend = 2;
                    $scope.error_msg = '';
                    common_modal_close('contactmessage');
                },
                function(err) {
                    $scope.error_msg = 'Please fill required fileds of contact facility.'
                    show_hide_loader('message_loader', false);
                });
        }

        $scope.load_more = function() {
            if ($scope.is_load_more && $scope.offset) {
                get_job_data("");
            }
        }

        $scope.SaveSearch = function(check_login) {
            if (check_login == 2) {
                if ($scope.search_data.canada_province != 'All' || $scope.search_data.medical_specialty != 'All' || $scope.search_data.Position != 'All' || $scope.search_data.citypostalcode != '') {
                    var saved_url = site_url + 'webservices/saved_search';
                    commonService.apiPost(saved_url, $scope.search_data)
                        .then(function(response) {
                            if (response.Data.saved_msg == 1) {
                                $scope.msg_info = 'You have already saved these job search parameters. <br/><br/><a class="button" target="_blank" href="save-search">VIEW SAVED SEARCHES</a>';
                                $scope.icon = '<i class="fa fa-files-o"></i>';
                                $scope.class_msg = 'error_msg';
                            } else {
                                $scope.msg_info = 'Search parameters saved<br/> <br/><a class="button" target="_blank" href="save-search">View save search</a>';
                                $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                                $scope.class_msg = 'success_msg';
                            }

                            $scope.PopUp();
                        });
                } else {
                    $scope.msg_info = 'Kindly select search parameter to save in your account.';
                    $scope.icon = '<i class="fa fa-warning"></i>';
                    $scope.class_msg = 'error_msg';
                    $scope.PopUp();
                }
            }
            if (check_login == 3) {
                $scope.favorite_error = '';
                $scope.contact_error = '';
                $scope.saved_search_error = 'Please login as a Doctor user to use save search facility.';
            }
        }
        $scope.favorite_mark = function(id, check_login, index, checkpage) {
            if (check_login == 2) {
                var mark_url = site_url + 'webservices/mark_favourite';
                commonService.apiPost(mark_url, {
                    'job_id': id
                })
                .then(function(response) {
                    $scope.msg_info = 'Job saved to favourites list';
                    $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                    $scope.class_msg = 'success_msg';
                    $scope.PopUp();
                    if (checkpage == 'joblist') {
                        $scope.jobs[index].row_favourite = 1;
                    }
                    if (checkpage == 'detail') {
                        $scope.marked_favourite = 1;
                    }
                });
            }
            if (check_login == 3) {
                $scope.saved_search_error = '';
                $scope.contact_error = '';
                $scope.favorite_error = 'Please login as a Doctor user to use favourite mark facility.';
            }
        }
        $scope.unfavorite_mark = function(id, check_login, index, checkpage) {
            if (check_login == 2) {
                var mark_url = site_url + 'webservices/mark_unfavourite';
                commonService.apiPost(mark_url, {
                    'favourite_id': id
                })
                .then(function(response) {
                    $scope.msg_info = 'Job removed from favourites list.';
                    $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                    $scope.class_msg = 'success_msg';
                    $scope.PopUp();
                    //window.location = site_url + 'jobs/';
                    if (checkpage == 'joblist') {
                        $scope.jobs[index].row_favourite = '';
                    }
                    if (checkpage == 'detail') {
                        $scope.marked_favourite = '';
                    }
                });
            }
        }
        $scope.PopUp = function() {
            common_modal('common_modal_msg');
        };
        $scope.check_user_login_val = '';

        $rootScope.ismarkerClicked = '';
        $rootScope.$watch("ismarkerClicked", function(newvalue, oldvalue) {
            if (newvalue != '') {
                var value = Base64.encode(newvalue);
                $location.path('jobdetail/' + value);
            }
        })
        $window.getJobDetails = $scope.jobDetails;
        $scope.cleardata = function() {
            localStorage.clear('localStoragebackcome');
            localStorage.clear('localStoragesearchdata');
            localStorage.clear('localStoragebackdata');

            localStorageService.set('isReset', 1);

            window.location = site_url + 'jobs/';
        }
        $scope.saveLocaldata = function(encrpt_id) {
            $scope.localdatasave.canada_province = $scope.search_data.canada_province;
            $scope.localdatasave.medical_specialty = $scope.search_data.medical_specialty;
            $scope.localdatasave.Position = $scope.search_data.Position;
            $scope.localdatasave.citypostalcode = $scope.search_data.citypostalcode;
            localStorageService.set('localStoragebackdata', $scope.localdatasave);
            window.location = site_url + 'jobdetail/' + encrpt_id;
        }
        $timeout(function() {
            get_job_data("");
        }, 1000);
        $scope.reinitializeMap = function() {
            initialize(healthCenter, firstLat, firstLng, hovericon, zoom_level);
            //initialize(healthCenter, $scope.jobDetail.latitude, $scope.jobDetail.longitude, 'markerIcon.svg', 14);
        }


    }]).controller('JobDetail', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', 'localStorageService', '$window', '$timeout', '$compile', 'filteredListService', '$location', function($scope, $rootScope, $http, $state, commonService, $stateParams, localStorageService, $window, $timeout, $compile, filteredListService, $location) {
        //alert("test");
        $rootScope.showGlobalLoader = false;
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.showGlobalLoader = false;
        $scope.marked_favourite = '';
        var job_parameter = {};
        var jobId = $stateParams.jobId;
        if (jobId == '' || jobId == 'NULL') {
            window.location = site_url + 'jobs/';
        }
        $rootScope.headerClass = $state.current.name;
        $scope.check_user_login_value = '';
        $scope.favorite_error = '';
        $scope.UpdateMassage = '';
        $scope.row_favourite = '';
        $scope.UpdatePostAlertMessage = '';
        $scope.saved_search_error = '';
        $scope.contact_error = '';
        $scope.checkdisable = true;
        $scope.map_listing = 'View Map';
        var defaultMsg = '';
        $scope.contact = {};
        $scope.contact.datainsert = 0;
        if ($( window ).width()<=800) {
            setTimeout(function(){ angular.element("#map_canvas").parent("div").css('display', 'none'); }, 3000);
        }
        $scope.toggeleclass = function(reset) {
            if (reset) {
                angular.element("#map_canvas").parent("div").css('display', 'none');
                $scope.toggleclassfunctioning = '';
                $scope.map_listing = 'View Map';
            }
            else{
                if ($scope.toggleclassfunctioning == 'mobile_preview_map') {
                    angular.element("#map_canvas").parent("div").css('display', 'none');
                    $scope.toggleclassfunctioning = '';
                    $scope.map_listing = 'View Map';

                } else {
                    angular.element("#map_canvas").parent("div").css('display', 'block');
                    $scope.toggleclassfunctioning = 'mobile_preview_map';
                    $scope.map_listing = 'View Listing';
                }
            }

        }

        show_hide_loader('search_loader_detail', true);
        $scope.showDetails = true;
        $scope.jobs_image = '';
        var url = site_url + 'webservices/job_detail/' + jobId;
        commonService.apiPost(url)
            .then(function(response) {
                show_hide_loader('search_loader', false);
                show_hide_loader('search_loader_detail', false);
                //console.log(response.Data);
                $scope.check_user_login_val = response.Data.check_user_login_val;
                $scope.marked_favourite = response.Data.marked_favourite;
                $scope.jobDetail = response.Data.jobs_data;
                $scope.jobs_image = response.Data.jobs_image;
                $scope.MedicalSpecialtyValue = response.Data.medical_spec_name;
                $scope.FacilityType = response.Data.facility_type;
                $scope.HealthProfessional = response.Data.health_professionals;
                $scope.ProvinceName = response.Data.province_name;
                $scope.NursingArray = response.Data.nursing_array;
                $scope.FacilityDaysNameNew = response.Data.facility_days_name;
                $scope.HoursValue = response.Data.jobhours;
                $scope.StartDateValue = response.Data.start_date;
                $scope.CompensationValue = response.Data.compensation;
                $scope.MedicalSpecLogo = response.Data.medical_spec_logo;
                $scope.check_user_login_value = response.Data.check_user_login_val;
                defaultMsg = response.Data.physicain_detail_info.physician_default_msg;
                var dateOrders = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                var FacilityDaysNameAll = [];
                angular.forEach(dateOrders, function(value1, key1) {
                    angular.forEach(response.Data.facility_days_name, function(value, key) {
                        if (value == value1) {
                            FacilityDaysNameAll.push(value);
                        }
                    });
                });

                $scope.FacilityDaysName = FacilityDaysNameAll;

                $scope.FacilityHoursAll = response.Data.facility_hours_all;
                var healthCenter = {};
                var i = 0;
                angular.forEach(response.Data.allJobs, function(value, key) {
                    if (value.latitude != '' || value.longitude) {
                        latlng = [value.listing_title, value.latitude, value.longitude, value.job_id, value.icon];
                        healthCenter[i] = latlng;
                        i++;
                    }
                });

                initialize(healthCenter, $scope.jobDetail.latitude, $scope.jobDetail.longitude, 'markerIcon.svg', 14);
                $rootScope.meta_id = jobId;
            }, function(err) {
                show_hide_loader('search_loader_detail', false);
                show_hide_loader('search_loader', false);
                $scope.showDetails = false;
                $scope.msg_info = 'Currently your are not able to view this job.';
                $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                $scope.class_msg = 'success_msg';
                $scope.PopUp();
                window.location = site_url + 'jobs/';
            });

        $rootScope.ismarkerClicked = '';
        $rootScope.$watch("ismarkerClicked", function(newvalue, oldvalue) {
            if (newvalue != '') {
                var value = Base64.encode(newvalue);
                $location.path('jobdetail/' + value);
                //$scope.jobDetails(newvalue);			
            }
        })
        $scope.checkback = function() {

            localStorageService.set('localStoragebackcome', 1);
            window.location = site_url + 'jobs/';

        }
        $scope.googleplusbtn = function(ID) {
            var url = site_url + 'jobs/' + ID;
            sharelink = "https://plus.google.com/share?url=" + url;
            newwindow = window.open(sharelink, 'name', 'height=400,width=600');
            if (window.focus) {
                newwindow.focus()
            }
            return false;
        }
        $scope.linkedinbtn = function(id, title, description) {
            var url = site_url + 'jobs/' + id;
            sharelink = "https://www.linkedin.com/shareArticle?mini=true&url=" + url + "&title=" + title + "&summary=" + description;
            newwindow = window.open(sharelink, 'name', 'height=400,width=600');
            if (window.focus) {
                newwindow.focus()
            }
            return false;
        }
        $scope.facebookbtn = function(id, img, title, description) {
            var url = site_url + 'jobs/' + id;
            var image = site_url + 'assets/uploads/jobs/' + img;
            var cart_redirect = site_url + 'cart/redirect_facebook';
            sharelink = "http://www.facebook.com/dialog/feed?app_id="+FB_SHARE_KEY+"&link=" + url + "&picture=" + image + "&name=" + title + "&description=" + description + "&redirect_uri=" + cart_redirect;
            newwindow = window.open(sharelink, 'name', 'height=400,width=600');
            if (window.focus) {
                newwindow.focus()
            }
            return false;
        }
        $scope.favorite_mark = function(id, check_login, index, checkpage) {
            if (check_login == 2) {

                var mark_url = site_url + 'webservices/mark_favourite';
                commonService.apiPost(mark_url, {
                        'job_id': id
                    })
                    .then(function(response) {
                        $scope.msg_info = 'Job saved to favourites list';
                        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                        $scope.class_msg = 'success_msg';
                        $scope.PopUp();
                        if (checkpage == 'joblist') {
                            $scope.jobs[index].row_favourite = 1;
                        }
                        if (checkpage == 'detail') {
                            $scope.marked_favourite = 1;
                        }
                    });

            }

            if (check_login == 3) {
                $scope.saved_search_error = '';
                $scope.contact_error = '';
                $scope.favorite_error = 'Please login as a Doctor user to use favourite mark facility.';
            }

        }
        $scope.unfavorite_mark = function(id, check_login, index, checkpage) {

            if (check_login == 2) {

                var mark_url = site_url + 'webservices/mark_unfavourite';
                commonService.apiPost(mark_url, {
                        'favourite_id': id
                    })
                    .then(function(response) {
                        $scope.msg_info = 'Job removed from favourites list.';
                        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                        $scope.class_msg = 'success_msg';
                        $scope.PopUp();
                        //window.location = site_url + 'jobs/';
                        if (checkpage == 'joblist') {
                            $scope.jobs[index].row_favourite = '';
                        }
                        if (checkpage == 'detail') {
                            $scope.marked_favourite = '';
                        }
                    });

            }
        }
        $scope.PopUp = function() {
            common_modal('common_modal_msg');
        };
        $scope.error_msg = '';
        $scope.submit_message = function() {

            show_hide_loader('message_loader', true);
            var url = site_url + 'webservices/save_message';
            commonService.apiPost(url, $scope.contact).then(function(response) {
                    show_hide_loader('message_loader', false);
                    $scope.contact.msgSend = 2;
                    $scope.error_msg = '';
                    common_modal_close('contactmessage');
                },
                function(err) {
                    $scope.error_msg = 'Please fill required fileds of contact facility.'
                    show_hide_loader('message_loader', false);
                });
        }
        $scope.resetVarible = function() {
            $scope.saved_search_error = '';
            $scope.contact_error = '';
            $scope.favorite_error = '';
        }
        $scope.setjobID = function(id, check_login, check_status, listing_title) {
            $scope.listing_name = '';
            $scope.listing_name = listing_title;
            if (check_login == 2) {
                show_hide_loader('message_loader', true);
                $scope.contact.default_msg = defaultMsg;
                $scope.contact.contact_subject = '';
                $scope.contact.msgSend = 0;
                $scope.contact.msg_send_id = 0;
                $scope.contact.job_id = id;
                $scope.contact.check_status = check_status;
                var url = site_url + 'webservices/check_msg_send';
                commonService.apiPost(url, $scope.contact).then(function(response) {
                        show_hide_loader('message_loader', false);
                        $scope.contact.msg_send_id = response.Data;
                        $scope.contact.msgSend = 1;
                    },
                    function(err) {
                        show_hide_loader('message_loader', false);
                    });
            }
            if (check_login == 3) {
                $scope.favorite_error = '';
                $scope.saved_search_error = '';
                $scope.contact_error = 'Please login as a Doctor user to use contact facility.';
            }
        }
    }]);