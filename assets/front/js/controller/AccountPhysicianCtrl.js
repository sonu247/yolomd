angular.module('yolomd').controller('PhysicianProfile', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams',
    function($scope, $rootScope, $http, $state, commonService, $stateParams) {
        commonService.CheckLogin('check_physicain_login'); //Check Physician login
        $rootScope.showGlobalLoader = false;
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;

        var update_profile_url = site_url + 'webservices/get_physician_profile_detail';
        $scope.Message = '';
        $scope.Error = {};
        $scope.PhysicianData = {};
        commonService.apiGet(update_profile_url).then(function(res) {
             show_hide_loader('search_loader', false);
            $scope.Message = res.Data.Message;
            $scope.PhysicianData = res.Data.user_detail;

        });
        var url = site_url + 'webservices/physician_signup_content';
        commonService.apiGet(url)
        .then(function(response) {
             show_hide_loader('search_loader', false);
            $scope.currently_status     = response.Data.currently_status;
            $scope.medical_school       = response.Data.medical_school;
            $scope.medical_specialty    = response.Data.medical_specialty;
            $scope.canada_province      = response.Data.canada_province;
            $scope.physician_content    = response.Data.physician_content;
        });
        $scope.PhysicianProfileUpdate = function() {
            $scope.UpdateMassage = '';
            var physician_profile_url = site_url + 'webservices/physician_update';
            var post_data = $scope.PhysicianData;
            $scope.Message = '';
            $scope.Error = {};
             show_hide_loader('search_loader', true);
            commonService.apiPost(physician_profile_url, post_data).then(function(response) {
                $scope.Error = '';
                 show_hide_loader('search_loader', false);
                $scope.PopUp();
            }, function error(err) {
                 show_hide_loader('search_loader', false);
                $scope.Message = err.Message;
                $scope.Error = err.Error;
            });
        }
        $scope.PopUp = function() {
            $scope.msg_info = 'Profile updated successfully.';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            common_modal('common_modal_msg');
        }
    }
]).controller('PhysicianChangePassword', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$timeout', '$stateParams',
    function($scope, $rootScope, $http, $state, commonService, $timeout, $stateParams) {
        commonService.CheckLogin('check_physicain_login'); //Check Physician login
        $rootScope.showGlobalLoader = false;
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
        $scope.ChangePassword = {};
        $scope.Message = '';
        $scope.Error = {};
        $scope.UpdateAlertMessage = true;
        show_hide_loader('search_loader', false);
        $scope.SubmitChangePassword = function() {
             show_hide_loader('search_loader', true);
            $scope.UpdateMassage = '';
            var change_password_url = site_url + 'webservices/physician_change_password';
            var post_data = $scope.ChangePassword;
            commonService.apiPost(change_password_url, post_data).then(function(res) {
                $scope.Message = res.Message;
                $scope.Error = '';
                $scope.PopUp();
                 show_hide_loader('search_loader', false);
            }, function error(err) {
                $scope.Message = err.Message;
                $scope.Error = err.Error;
                 show_hide_loader('search_loader', false);
            });
        }
        $scope.PopUp = function() {
            $scope.msg_info = 'Your password has been changed successfully.';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            common_modal('common_modal_msg');
        }
    }
]).controller('SaveSearch', ['$scope', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$timeout', 'localStorageService', '$stateParams',
    function($scope, $rootScope, $http, $state, commonService, filteredListService, $timeout, localStorageService, $stateParams) {
        commonService.CheckLogin('check_physicain_login'); //Check Physician login
        $rootScope.showGlobalLoader = false;
        $rootScope.headerClass = $state.current.name;
        $rootScope.pageTitle = '';
        $rootScope.pageTitle = $stateParams.pageTitle;
        $scope.state_name = $rootScope.headerClass;
        $scope.last_index = '';
        $scope.order = '1';
        $scope.reverse = false;
        $pass_parameter = false;
        $scope.saved_search_count = '';
        $scope.count = 0;
        var url = site_url + 'webservices/saved_search';
        commonService.apiGet(url).then(function(response) {
            //alert(JSON.stringify(response.Data.saved_search));
             show_hide_loader('search_loader', false);
            $scope.saved_search = response.Data.saved_search;
            $scope.saved_search_count = $scope.saved_search.length;
            if ($scope.saved_search_count>0) {
                window_play();
            }
            $scope.pageSize = 10;
            $scope.filteredList = $scope.saved_search;
            $scope.currentPage = 0;
            $scope.pagination();
        });
        $scope.pagination = function() {
            $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
            $scope.last_index = $scope.ItemsByPage.length - 1;
        };
        $scope.setPage = function() {
            $scope.currentPage = this.n;
            $scope.active = 1;
        };
        $scope.firstPage = function() {
            $scope.currentPage = 0;
            $scope.active = 1;
        };
        $scope.lastPage = function() {
            $scope.currentPage = $scope.ItemsByPage.length - 1;
            $scope.active = 1;
        };
        $scope.range = function(input, total) {
            var ret = [];
            if (!total) {
                total = input;
                input = 0;
            }
            for (var i = input; i < total; i++) {
                if (i != 0 && i != total - 1) {
                    ret.push(i);
                }
            }
            return ret;
        };
        $scope.delete_savesearch = function(id, index, pageno) {
            if (confirm('Are you sure you want to delete this job search?')) {
                var url_delete = site_url + 'webservices/delete_savesearch';
                 show_hide_loader('search_loader', true);
                commonService.apiPost(url_delete, {
                    'search_id': id
                }).then(function(response) {
                     show_hide_loader('search_loader', false);
                    $scope.PopUp();
                    $scope.saved_search.splice(index, 1);
                    $scope.ItemsByPage[pageno].splice(index, 1);
                });
            }
        };
        $scope.PopUp = function() {
            $scope.msg_info = 'Saved job search parameter removed successfully.';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            common_modal('common_modal_msg');
        };
    }
]).controller('Favorite', ['$scope', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$timeout', '$stateParams',
    function($scope, $rootScope, $http, $state, commonService, filteredListService, $timeout, $stateParams) {
        commonService.CheckLogin('check_physicain_login'); //Check Physician login
        $rootScope.showGlobalLoader = false;
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
        $scope.last_index = '';
        $scope.order = '1';
        $scope.reverse = false;
        $pass_parameter = false;
        $scope.favourite_jobs_count = '';
        $scope.count = 0;
        var url = site_url + 'webservices/favourite_job';
        commonService.apiGet(url).then(function(response) {
            //alert(JSON.stringify(response.Data.favourite_jobs));
            show_hide_loader('search_loader', false);
            $scope.favourite_jobs = response.Data.favourite_jobs;
            $scope.favourite_jobs_count = $scope.favourite_jobs.length;
            if ($scope.favourite_jobs_count>0) {
                window_play();
            }
            $scope.pageSize = 5;
            $scope.filteredList = $scope.favourite_jobs;
            $scope.currentPage = 0;
            $scope.pagination();
        });
        $scope.pagination = function() {
            $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
            $scope.last_index = $scope.ItemsByPage.length - 1;
        };
        $scope.setPage = function() {
            $scope.currentPage = this.n;
            $scope.active = 1;
        };
        $scope.firstPage = function() {
            $scope.currentPage = 0;
            $scope.active = 1;
        };
        $scope.lastPage = function() {
            $scope.currentPage = $scope.ItemsByPage.length - 1;
            $scope.active = 1;
        };
        $scope.range = function(input, total) {
            var ret = [];
            if (!total) {
                total = input;
                input = 0;
            }
            for (var i = input; i < total; i++) {
                if (i != 0 && i != total - 1) {
                    ret.push(i);
                }
            }
            return ret;
        };
        $scope.unfavorite_mark = function(id, index, pageno) {
            if (confirm('Are you sure you want to remove this job from favourite list ?')) {
                 show_hide_loader('search_loader', true);
                var mark_url = site_url + 'webservices/mark_unfavourite';
                commonService.apiPost(mark_url, {
                    'favourite_id': id
                }).then(function(response) {
                     show_hide_loader('search_loader', false);
                    $scope.PopUp();
                    $scope.favourite_jobs.splice(index, 1);
                    $scope.ItemsByPage[pageno].splice(index, 1);
                });
            }
        }
        $scope.PopUp = function() {
            $scope.msg_info = 'Favourites list updated successfully.';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            common_modal('common_modal_msg');
        };
    }
]).controller('PhysicianMessage', ['$scope', '$filter', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$timeout', '$window', '$stateParams','$interval',
    function($scope, $filter, $rootScope, $http, $state, commonService, filteredListService, $timeout, $window, $stateParams,$interval) {
        commonService.CheckLogin('check_physicain_login'); //Check Physician login
        show_hide_loader('search_loader', true);
        show_hide_loader('search_loader_step', false);
        $scope.last_index = '';
        $scope.Header = ['', '', ''];
        $scope.order = '1';
        $scope.reverse = false;
        $pass_parameter = false;
        $scope.post_message_count = '';
        $scope.change_status = '';
        $scope.count = 0;
        $scope.UpdateMassage = '';
        iconName = 'glyphicon glyphicon-chevron-up';
        $scope.Header[0] = iconName;
        $scope.CheckPostValue = '';
        $rootScope.showGlobalLoader = false;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
        $scope.replymsg = {};        
        $scope.replymsg.msgId = '';
        $scope.messages ='';
        $scope.sorting = '2';
        $scope.showmessagedetail = function(msgId) {
            //$scope.check_msg_status();
             show_hide_loader('search_loader', true);
            $scope.replymsg.reply = '';
            $scope.replymsg.msgId = msgId;
            $scope.messages = '';
            $scope.msg_subject = '';
           
            var url = site_url + 'webservices/msg_detail';
            commonService.apiPost(url, {
                'msgId': msgId
            }).then(function(response) {
                show_hide_loader('search_loader', false);
              
                $scope.messages = response.Data.messages_data;
                $scope.msg_subject = response.Data.msg_subject;
                $scope.medical_specialty = response.Data.medical_specialty;
                $scope.listing_title = response.Data.listing_title;
                $scope.facility_city = response.Data.facility_city;
                
            });
        }
        $scope.sorting_msg = function(sorting_value){

             $scope.loadmessage('',sorting_value);
             //$scope.check_msg_status();
        }
        $scope.msg_sent = '';
        $scope.reportSent = false;
        $scope.insertmsg = function() {
            $scope.replymsg.msgId =  $scope.replymsg.msgId;
            show_hide_loader('search_loader', true);

            var url = site_url + 'webservices/insert_msg_reply';
            commonService.apiPost(url, $scope.replymsg).then(function(response) {
                $scope.showmessagedetail($scope.replymsg.msgId);
                 show_hide_loader('search_loader', false);
                $scope.msg_sent = '<h4>Message sent</h4>';
                $scope.reportSent = true;
                $timeout(function() {
                  $scope.reportSent = false;
                }, 2000);
            });
        }
        $scope.loadmessage = function(msg_id,sorting_value) {
            if(sorting_value == '' || sorting_value == undefined)
            {
                sorting_value = $scope.sorting;
            }
            show_hide_loader('search_loader', true);
            var url = site_url + 'webservices/physician_msg/'+sorting_value;
            commonService.apiGet(url).then(function(response) {
                show_hide_loader('search_loader', false);
                $scope.messages = response.Data.messages_data;
                $scope.post_message_count = $scope.messages.length;
                if ($scope.post_message_count>0) {
                    window_play();
                }
                $scope.pageSize = 7;
                $scope.filteredList = $scope.messages;
                $scope.currentPage = 0;
                $scope.pagination();
                
            });
        }
        $scope.loadmessage();    
        $scope.pagination = function() {
            $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
            $scope.last_index = $scope.ItemsByPage.length - 1;
        };
        $scope.setPage = function() {
            $scope.currentPage = this.n;
            $scope.active = 1;
        };
        $scope.firstPage = function() {
            $scope.currentPage = 0;
            $scope.active = 1;
        };
        $scope.lastPage = function() {
            $scope.currentPage = $scope.ItemsByPage.length - 1;
            $scope.active = 1;
        };
        $scope.range = function(input, total) {
            var ret = [];
            if (!total) {
                total = input;
                input = 0;
            }
            for (var i = input; i < total; i++) {
                if (i != 0 && i != total - 1) {
                    ret.push(i);
                }
            }
            return ret;
        };
        $scope.delete_msg = function(id, index, pageno) {
            if (confirm('Are you sure you want to delete this message?')) {
                 show_hide_loader('search_loader', true);
                var url_delete = site_url + 'webservices/delete_msg_physician';
                commonService.apiPost(url_delete, {
                    'msg_id': id
                }).then(function(response) {
                     show_hide_loader('search_loader', false);
                    $scope.msg_info = 'Message has been deleted successfully.';
                    $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                    $scope.class_msg = 'success_msg';
                    $scope.PopUp();
                    $scope.messages.splice(index, 1);
                    $scope.ItemsByPage[pageno].splice(index, 1);
                });
            }
        };
        $scope.PopUp = function(){
            common_modal('common_modal_msg');
        };
        $scope.count_unreadmsg = '';
        
        // $scope.check_msg_status = function (){
        // var count_unreadmsg_url  = site_url + 'webservices/check_read_unread_status/'+$scope.sorting;
        //         commonService.apiGet(count_unreadmsg_url)
        //         .then(function(response) {
        //          $scope.status_msg = response.Data.status_msg;
        //         // console.log(response.Data.status_msg[0]['count_unreadmsg']);

        //     });
        // };
        //$scope.check_msg_status();
        // $scope.StartTimerMsgStatus = function () {

        //     //Set the Timer start message.
        //     $scope.Message = "Timer started. ";
        //     //Initialize the Timer to run every 1000 milliseconds i.e. one second.
        //     $scope.Timer = $interval(function () {
        //         //Display the current time.
        //      // $scope.check_msg_status();
        //     }, 30000);
        // };
        //$scope.StartTimerMsgStatus();
       

    }
]);