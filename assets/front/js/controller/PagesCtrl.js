angular
.module('yolomd', ['ngSanitize'])
.controller('ContactUs',['$scope', '$rootScope','$http','$state','$location','$window','$stateParams','commonService', function($scope, $rootScope, $http,$state,$location,$window,$stateParams,commonService)
{
  	$window.scrollTo(0, 0);	
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.showGlobalLoader	= false;
	$rootScope.headerClass = $state.current.name;
	$scope.Contactus_detail = {};
	show_hide_loader('search_loader', false);
	$scope.SubmitContactUS =function(){
	var ContactusData = $scope.Contactus_detail;
    var contact_us = site_url + 'webservices/contact_us';
    show_hide_loader('search_loader', true);
    $scope.Message = '';
    commonService.apiPost(contact_us, ContactusData).then(function(res) {
    	$scope.Error = {};
    	$scope.Contactus_detail = {};
    	show_hide_loader('search_loader', false);
        $scope.PopUp();
    }, function(err) {
        $scope.Message = err.Message;
        $scope.Error = err.Error;
        show_hide_loader('search_loader', false);
    });
	};
	$scope.PopUp = function() {
        $scope.msg_info = 'Message sent';
        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
        $scope.class_msg = 'success_msg';
        common_modal('common_modal_msg');
    }
    $scope.ReloadCaptcha = function()
	{		
		var url = site_url + 'webservices/reload_captcha';
		var post_data = {'captcha_for':'cap_code'};
		show_hide_loader('search_loader', true);
		commonService.apiPost(url,post_data).then(function(res)
		{
			show_hide_loader('search_loader', false);
			angular.element('.captchaImg').attr('src',res.Data);
		},
		function(err){
			show_hide_loader('search_loader', false);
			console.log(err);
		});		
	}
}])
.controller('FAQ',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{	
	$window.scrollTo(0, 0);	
	$scope.FAQData				= {};
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.showGlobalLoader	= false;
	$rootScope.headerClass		= $state.current.name;
	var content_url				= site_url + 'webservices/faq_content';
	commonService.apiGet(content_url)
    .then(function(response) {
		$scope.FAQData		= response.Data.faq;
    });
}])
.controller('Aboutus',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{		
	$window.scrollTo(0, 0);
	$rootScope.pageTitle	= $stateParams.pageTitle;
	$rootScope.showGlobalLoader				= false;
	$rootScope.headerClass	= $state.current.name;
	var content_url			= site_url + 'webservices/aboutus';
	commonService.apiGet(content_url)
    .then(function(response) {
		$scope.CONTENT		= response.Data.content_aboutus;
    });
 }])
.controller('HowWork',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{		
	$window.scrollTo(0, 0);
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.showGlobalLoader	= false;
	$rootScope.headerClass		= $state.current.name;
	var content_url				= site_url + 'webservices/howwork';
	commonService.apiGet(content_url)
    .then(function(response) {
		$scope.CONTENT		= response.Data.content_howwork;
    });
}])
.controller('TermCondition',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{	
	$window.scrollTo(0, 0);	
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.showGlobalLoader	= false;
	$rootScope.headerClass		= $state.current.name;
	var content_url				= site_url + 'webservices/termcondition';
	commonService.apiGet(content_url)
    .then(function(response) {
		$scope.CONTENT		= response.Data.content_termcondition;
    });
}])
.controller('PrivatePolicy',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{	
	$window.scrollTo(0, 0);	
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.showGlobalLoader	= false;
	$rootScope.headerClass		= $state.current.name;
	var content_url				= site_url + 'webservices/privatepolicy';
	commonService.apiGet(content_url)
    .then(function(response) {
		$scope.CONTENT		= response.Data.content_privatepolicy;
    });
}])
.controller('PhotographerPhoto',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{		
	$window.scrollTo(0, 0);
	$scope.FAQData				= {};
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.showGlobalLoader	= false;
	$rootScope.headerClass		= $state.current.name;
	var content_url				= site_url + 'webservices/photographypage';
	commonService.apiGet(content_url)
    .then(function(response) {
    
		$scope.CONTENT		= response.Data.content_photographypage;
    });
}])
.controller('ResetPassword', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', '$timeout',function($scope, $rootScope, $http, $state, commonService, $stateParams, $timeout) 
{
	$window.scrollTo(0, 0);
   	commonService.CheckLoginredirection();
    $rootScope.showGlobalLoader = false;
    $rootScope.pageTitle = $stateParams.pageTitle;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.ChangePassword = {};
    $scope.Message = '';
    $scope.Error = {};
    $scope.UpdateAlertMessage = true;
    var activationKey = $stateParams.activationKey;
    if (activationKey == '' || activationKey == 'NULL') {
        window.location = site_url + 'signup';
    }
    show_hide_loader('search_loader', false);
    $scope.SubmitChangePassword = function() {
        $scope.UpdateMassage = '';
        var change_password_url = site_url + 'webservices/reset_password';
        $scope.ChangePassword.activation_key = activationKey;
        var changepassword_data = $scope.ChangePassword;
        show_hide_loader('search_loader', true);
        commonService.apiPost(change_password_url, changepassword_data).then(function(res) {
            $scope.Message = '';
            show_hide_loader('search_loader', false);
            $scope.PopUp();
        }, function error(err) {
            $scope.Message = err.Message;
            show_hide_loader('search_loader', false);
            $scope.Error = err.Error;
        });
    }
    $scope.PopUp = function() {
        $scope.msg_info = 'Your password changed successfully. Now you can use password to login.';
        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
        $scope.class_msg = 'success_msg';
        common_modal('common_modal_msg');
    }
}]);