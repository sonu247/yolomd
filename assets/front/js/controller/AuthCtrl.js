angular
.module('yolomd')
.controller('signup',['$scope', '$rootScope','$http','$state','commonService','$stateParams','$stateParams','$stateParams','$window', function($scope, $rootScope, $http,$state,commonService,$stateParams,$window)
{
	
	commonService.CheckLoginredirection(); //Check user login if login then redirect

	$rootScope.showGlobalLoader	= false;
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.headerClass		= $state.current.name;
	$rootScope.pageTitle		= $stateParams.pageTitle;
	
	var url = site_url + 'webservices/physician_medical_facility_content';
	commonService.apiGet(url)
    .then(function(response) {
		$scope.physician_medical_section	= response.Data.physician_medical_section;
		$scope.physician_content			= response.Data.physician_content;
		$scope.medical_facility				= response.Data.medical_facility;
    });
   
}])
.controller('MedicalFacilitySignup',['$scope', '$rootScope','$http','commonService','$state','$stateParams','$stateParams','$window', function($scope, $rootScope, $http,commonService, $state,$stateParams,$window)
{
	commonService.CheckLoginredirection(); //Check user login if login then redirect
	$rootScope.showGlobalLoader	= false;
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.headerClass		= $state.current.name;
	$scope.Signup				= {};
	$scope.Message				= '';
	$scope.Error				= {};
	$scope.MedicalData			= {};
	$scope.medicalStep			= 1;
	$scope.MedicalData.user_name_title = 'Mr.';
	var url						= site_url + 'webservices/physician_medical_facility_content';
	commonService.apiGet(url)
    .then(function(response) {
        $scope.medical_facility = response.Data.medical_facility;
		$scope.animationClass	='fadeInUp';
		//$($window).scrollTo(0, 0);
    });
    show_hide_loader('search_loader', false);
	$scope.SubmitMedicalSignup = function()
	{		
		var url = site_url + 'webservices/medical_facility_signup';
		var post_data = $scope.MedicalData;
		
		$scope.Message = '';
		show_hide_loader('search_loader', true);
		commonService.apiPost(url,post_data).then(function(res)
		{
			show_hide_loader('search_loader', false);
			$scope.Message		= res.Message;
			$scope.MedicalData	= {};
			$scope.medicalStep	= 2;
		},
		function(err){
			show_hide_loader('search_loader', false);
			$scope.Message	= err.Message;
			$scope.Error	= err.Error;
		});		
	}
    $scope.ReloadCaptcha = function()
	{		
		var url = site_url + 'webservices/reload_captcha';
		var post_data = {'captcha_for':'medical'};
		show_hide_loader('search_loader', true);
		commonService.apiPost(url,post_data).then(function(res)
		{
			show_hide_loader('search_loader', false);
			angular.element('.captchaImg').attr('src',res.Data);
		},
		function(err){
			show_hide_loader('search_loader', false);
			console.log(err);
		});		
	}
}])
.controller('PhysicianSignup',['$scope', '$rootScope','$http','commonService','$state','$stateParams','$window', function($scope, $rootScope, $http,commonService, $state,$stateParams,$window)
{
	commonService.CheckLoginredirection(); 
	//Check user login if login then redirect 
	$scope.Signup				={};
	$scope.Message				='';
	$scope.PhysicianData		= {};
	$rootScope.showGlobalLoader	= false;
	$rootScope.pageTitle		= $stateParams.pageTitle;
	$rootScope.headerClass		= $state.current.name;
	$scope.PhysicianData.physician_dob_year = '1980';
	$scope.PhysicianData.traning_year = '2000';
	$scope.PhysicianData.user_name_title = 'Mr.';
	$scope.PhysicianData.physician_gender = 'Male';
	$scope.PhysicianData.physician_country = 'Canada';
	var url						= site_url + 'webservices/physician_signup_content';
	commonService.apiGet(url)
    .then(function(response) {
		$scope.currently_status		= response.Data.currently_status;
		$scope.medical_school		= response.Data.medical_school;
		$scope.medical_specialty	= response.Data.medical_specialty;
		$scope.canada_province		= response.Data.canada_province;
		$scope.physician_content	= response.Data.physician_content;
    });
		$scope.animationClass	='fadeInUp';
		$window.scrollTo(0, 0);
		$scope.progressBar		= 0;
		$scope.physicianStep	= 1;
		var multipicationbar	= 25;
    $scope.previousStep =function(){
    	$window.scrollTo(0, 0);
		$scope.physicianStep	= $scope.physicianStep-1;
		$scope.progressBar		=($scope.physicianStep -1 )*  multipicationbar;
		$scope.animationClass	='fadeInUp';
		$window.scrollTo(0, 0);
    }
    show_hide_loader('search_loader', false);
    $scope.nextStep =function(){
		$scope.animationClass	='fadeInUp';
		$window.scrollTo(0, 0);
	    if($scope.physicianStep == 4){
			var url				= site_url + 'webservices/physician_signup';
			var PhysicianData	= $scope.PhysicianData;
			$scope.Message		= '';
			show_hide_loader('search_loader', true);
		   	commonService.apiPost(url,PhysicianData).then(function(res)
		   	{
				show_hide_loader('search_loader', false);
				$scope.progressBar		=$scope.physicianStep * multipicationbar;
				$scope.physicianStep	= $scope.physicianStep+1;
			},
			function(err){
				show_hide_loader('search_loader', false);
				$scope.Message			= err.Message;
				$scope.Error			= err.Error;
				if (err.Data.step!=4) {
					$scope.physicianStep	= 1;
				}
			});		
	    }
      	else{
			$scope.progressBar		=$scope.physicianStep * multipicationbar;
			$scope.physicianStep	= $scope.physicianStep+1;
	 	}
    }
    $scope.ReloadCaptcha = function()
	{		
		var url = site_url + 'webservices/reload_captcha';
		var post_data = {'captcha_for':'physician'};
		show_hide_loader('search_loader', true);
		commonService.apiPost(url,post_data).then(function(res)
		{
			show_hide_loader('search_loader', false);
			angular.element('.captchaImg').attr('src',res.Data);
		},
		function(err){
			show_hide_loader('search_loader', false);
			console.log(err);
		});		
	}
}]);


