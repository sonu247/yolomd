angular
.module('yolomd')
.controller('SidebarMenuDashboard', ['$scope', '$filter', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$timeout', '$window', '$stateParams','$interval',
    function($scope, $filter, $rootScope, $http, $state, commonService, filteredListService, $timeout, $window, $stateParams,$interval) {
        $rootScope.dashboardFaClass = 'fa fa-bars';
        $rootScope.dashboardToggleClass = '';
        $rootScope.dashboardLeftToggle = function() {
            if ($rootScope.dashboardToggleClass=='') {
                $rootScope.dashboardFaClass = 'fa fa-times';
                $rootScope.dashboardToggleClass = 'dashboard-Left-toggle-show';
            }
            else{
                $rootScope.dashboardFaClass = 'fa fa-bars';
                $rootScope.dashboardToggleClass = '';
            }
        }
    }
])
.controller('userLogin',['$scope', '$rootScope','$http','commonService','$state','$location','$stateParams','$window', function($scope, $rootScope, $http,commonService,$state,$location,$window)
{
	$scope.Message            = '';
	$scope.ErrorMessage       = '';
	$scope.Message_new        = '';
	$scope.resetVarible = function(get_forget){
		$scope.saved_search_error = '';
		$scope.contact_error      = '';
		$scope.favorite_error     = '';
		$scope.social_login_error = '';
		$scope.contact_facility_error    = '';
		if(get_forget)
		{
			$scope.closepopupForget();
		}
		
	}
	$scope.resetVaribleJob = function(get_forget){
		$scope.saved_search_error = '';
		$scope.contact_error      = '';
		$scope.favorite_error     = '';
		$scope.social_login_error = '';
		$scope.contact_facility_error    = '';
		if(get_forget)
		{
			$scope.closepopupPostJob();
		}
		
	}
	$scope.socal_login_er = function(){
		$scope.social_login_error = '';
	}
	$scope.closepopup = function(){
		$scope.social_login_error = '';
		$("#signupModal").modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}
	$scope.closepopup1 = function(){
		$scope.social_login_error = '';
		$("#postJObMsg").modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}
	$scope.closepopupForget = function(){
		$scope.social_login_error = '';
		$("#forgetpasswordModal").modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}
	$scope.closepopupPostJob = function(){
		$scope.social_login_error = '';
		$("#postJObMsg").modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}
	$scope.login = function(){
		$scope.social_login_error = '';
		var url				= site_url + 'webservices/user_login';
		var user_login		= $scope.user_login;
		commonService.apiPost(url,user_login).then(function(res)
		{
			window.location = site_url + res.Data;
		},
		function(err){
			console.log(err);
			$scope.Message			= err.Message;
			$scope.ErrorMessage = err.Data.error_message;
			$scope.Error			= err.Error;

			}
		);
 	}

 	show_hide_loader('search_loader_forget', false);
 	$scope.forgot_password_data = true;
 	$scope.Message_new	= '';
 	$scope.forgetPassword = function(){
 		$scope.social_login_error = '';
		var url				= site_url + 'webservices/forgot_password';
		var user_forget		= $scope.user_forget;
		show_hide_loader('search_loader_forget', true);
		commonService.apiPost(url,user_forget).then(function(res)
		{
			show_hide_loader('search_loader_forget', false);
			$scope.Error = '';
			$scope.forgot_password_data = false;
			$scope.Message_new	= '<h3>Instructions have been sent.</h3><p>Password reset instructions have been sent to <strong>'+$scope.user_forget.email+'</strong>. Don’t forget to check your spam and junk mail filters if they don’t turn up.</p>';
			$scope.user_forget = '';
		},
		function(err){

			console.log(err);
			$scope.Message_new			= err.Message;
			$scope.ErrorMessage 	= err.Data.error_message;
			$scope.Error			= err.Error;
			$scope.forgot_password_data = true;
			show_hide_loader('search_loader_forget', false);
			}
		);
 	}


	// $scope.tokenUrl = function() {
	// 	alert('kkkk');
	//     return window.location.origin + '~examin8/CI/yoloMD/token';
	// };
	// $scope.applicationId = function() {
	// 	alert('sadad');
	//     return 'gkkeeejopjnpoohocihi';
	// };
 	
}])
.controller('logoutCtrl',['$scope', '$rootScope','$http','commonService','$state','$location', function($scope, $rootScope, $http,commonService, $state,$location)
{
	
	$scope.Message		= '';
	$scope.logout = function(){
		var url				= site_url + 'webservices/logout';
		commonService.apiPost(url).then(function(res)
		{
			
			window.location = site_url;
		},
		function(err){
		
			console.log(err.Error);
			$scope.Message			= err.Message;
			$scope.Error			= err.Error;
			}
		);
	 }
}])

.controller('error_404',['$scope', '$rootScope', '$http','commonService','$state', function($scope, $rootScope,$http,commonService,$state)
   {

   }])
.controller('GetSearch',['$scope', '$rootScope','$http','$state','commonService','localStorageService','$window', function($scope, $rootScope, $http,$state,commonService,localStorageService,$window)
{
	
	$scope.get_id_save_search = function(cityname,searchId){
	
	    $scope.localstoragearray   							= {};
	    $scope.localstoragearray.provice_id					= 'All';
		$scope.localstoragearray.medical_specialties_id		= 'All';
		$scope.localstoragearray.position					= 'Any';
		$scope.localstoragearray.city_postal_code			= '';
	  	if(searchId && searchId != 'All')
	    {
	        
	        var saved_search_value  = site_url + 'webservices/get_saved_search_value';
	        commonService.apiPost(saved_search_value,{'save_search_id':searchId})
	        .then(function(response) {
	            $scope.localstoragearray = response.Data.saved_search_data;
	            localStorageService.set('localStoragesearchdata',$scope.localstoragearray); 
	            window.location = site_url + 'jobs/';
	            //$window.open(site_url + 'jobs/', '_blank');
	            
	        });
	    }else if(cityname){
	    	//alert('dsadsadad');
	        $scope.localstoragearray.city_postal_code = cityname;
	        localStorageService.set('localStoragesearchdata',$scope.localstoragearray); 
	        //$window.open(site_url + 'jobs/');
	        window.location = site_url + 'jobs/';
	    }
	}  
}])
.controller('MessageCount',['$scope', '$rootScope','$http','$state','commonService','localStorageService','$window','$interval', function($scope, $rootScope, $http,$state,commonService,localStorageService,$window,$interval)
{
	
	$scope.msg_count = '';
	$scope.get_count_msg = function (){
	    var message_count_value  = site_url + 'webservices/count_message';
			commonService.apiPost(message_count_value)
 			.then(function(response) {
   			 $scope.msg_count = response.Data.msg_count;
   			 if($scope.msg_count > 99)
   			 {
   			 	$scope.msg_count = '99<font>+</font>';
   			 }
 		});
	};
	$scope.get_count_msg();
    $scope.StartTimer = function (seconds) {
        $scope.Message = "Timer started. ";
        $scope.Timer = $interval(function () {
          $scope.get_count_msg();
        }, seconds*1000);
    };
    $scope.StartTimer(60);
    
}])
.controller('GoTop',['$scope', '$rootScope','$http','$state','commonService','localStorageService','$window','$interval','$anchorScroll', function($scope, $rootScope, $http,$state,commonService,localStorageService,$window,$interval,$anchorScroll)
{
	$rootScope.go_top = function (){
		window.scrollTo(0,0);
	};
	
}]);
