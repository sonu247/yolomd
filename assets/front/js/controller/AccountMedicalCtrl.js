angular.module('yolomd', ['ngSanitize']).controller('MedicalDashboard', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams',
    function($scope, $rootScope, $http, $state, commonService, $stateParams) {
        commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
        $rootScope.showGlobalLoader = false;
        show_hide_loader('search_loader', false);
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
    }
]).controller('MedicalProfile', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', '$timeout',
    function($scope, $rootScope, $http, $state, commonService, $timeout, $stateParams) {
        commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
        $rootScope.showGlobalLoader = false;
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
        var update_profile_url = site_url + 'webservices/get_medical_profile_detail';
        $scope.Message = '';
        $scope.Error = {};
        $scope.MedicalDetailData = {};
        commonService.apiGet(update_profile_url).then(function(res) {
            show_hide_loader('search_loader', false);
            $scope.Message = res.Data.Message;
            $scope.MedicalDetailData = res.Data.user_detail;
            $scope.Error = '';
        });
        $scope.MedicalProfileUpdate = function() {
            $scope.UpdateMassage = '';
              show_hide_loader('search_loader', true);
            var medical_profile_url = site_url + 'webservices/medical_profile_update';
            var post_data = $scope.MedicalDetailData;
            $scope.Message = '';
            $scope.Error = {};
            commonService.apiPost(medical_profile_url, post_data).then(function(response) {
                 $scope.Error = '';
                   show_hide_loader('search_loader', false);
                $scope.PopUp();
            }, function error(err) {
                $scope.Message = err.Message;
                  show_hide_loader('search_loader', false);
                $scope.Error = err.Error;
            });
        }
        $scope.PopUp = function() {
            $scope.msg_info = 'Profile updated successfully.';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            common_modal('common_modal_msg');
        }
    }
]).controller('ChangePassword', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', '$timeout',
    function($scope, $rootScope, $http, $state, commonService, $stateParams, $timeout) {
        commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
        $rootScope.showGlobalLoader = false;
        $rootScope.pageTitle = $stateParams.pageTitle;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
        $scope.ChangePassword = {};
        $scope.Message = '';
        $scope.Error = {};
        $scope.UpdateAlertMessage = true;
        show_hide_loader('search_loader', false);
        $scope.SubmitChangePassword = function() {
            $scope.UpdateMassage = '';
              show_hide_loader('search_loader', true);
            var change_password_url = site_url + 'webservices/medical_change_password';
            var post_data = $scope.ChangePassword;
            commonService.apiPost(change_password_url, post_data).then(function(res) {
                $scope.Message = res.Message;
                  show_hide_loader('search_loader', false);
                $scope.Error = '';
                $scope.PopUp();
            }, function error(err) {
                  show_hide_loader('search_loader', false);
                $scope.Message = err.Message;
                $scope.Error = err.Error;
            });
        }
        $scope.PopUp = function() {
            $scope.msg_info = 'Your password has been changed successfully.';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            common_modal('common_modal_msg');
        }
    }
]).controller('MedicalMessage', ['$scope', '$filter', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$timeout', '$window', '$stateParams','$interval',
    function($scope, $filter, $rootScope, $http, $state, commonService, filteredListService, $timeout, $window, $stateParams,$interval) {
        commonService.CheckLogin('check_Medical_user_login'); //Check Physician login
        show_hide_loader('search_loader', true);
        show_hide_loader('search_loader_step', false);
        $scope.last_index = '';
        $scope.Header = ['', '', ''];
        $scope.order = '1';
        $scope.reverse = false;
        $pass_parameter = false;
        $scope.post_message_count = '';
        $scope.change_status = '';
        $scope.count = 0;
        $scope.UpdateMassage = '';
        iconName = 'glyphicon glyphicon-chevron-up';
        $scope.Header[0] = iconName;
        $scope.CheckPostValue = '';
        $rootScope.showGlobalLoader = false;
        $rootScope.headerClass = $state.current.name;
        $scope.state_name = $rootScope.headerClass;
        $scope.replymsg = {};        
        $scope.replymsg.msgId = '';
        $scope.messages = '';
        $scope.sorting = '2';
        $scope.showmessagedetail = function(msgId,index,pageno) {
            //$scope.check_msg_status();
            $scope.ItemsByPage[pageno][index].count_unreadmsg="";
            $scope.replymsg.reply = '';
            $scope.replymsg.msgId = msgId;
            show_hide_loader('search_loader', true);
            $scope.messages = '';
            $scope.msg_subject = '';
            var url = site_url + 'webservices/msg_detail_medical';
            commonService.apiPost(url, {
                'msgId': msgId
            }).then(function(response) {
                show_hide_loader('search_loader', false);
                $scope.messages = response.Data.messages_data;
                $scope.msg_subject = response.Data.msg_subject;
                $scope.medical_specialty = response.Data.medical_specialty;
                $scope.listing_title = response.Data.listing_title;
                $scope.facility_city = response.Data.facility_city;
                $scope.username = response.Data.user_name;
            });
        }
        $scope.sorting_msg = function(sorting_value){
            $scope.loadmessage('',sorting_value);
            //$scope.check_msg_status();
        }
        $scope.msg_sent = '';
        $scope.reportSent = false;
        $scope.insertmsg = function() {
            $scope.replymsg.msgId =  $scope.replymsg.msgId;
            show_hide_loader('search_loader', true);
            var url = site_url + 'webservices/insert_msg_reply_medical';
            commonService.apiPost(url, $scope.replymsg).then(function(response) {
                $scope.showmessagedetail($scope.replymsg.msgId);
                $scope.msg_sent = '<h4>Message sent.</h4>';
                show_hide_loader('search_loader', false);
                $scope.reportSent = true;
                $timeout(function() {
                  $scope.reportSent = false;
                }, 2000);
            });
        }
        $scope.markflag = function(msg_id) {
            var url = site_url + 'webservices/markflag';
            show_hide_loader('search_loader_step', true);
            commonService.apiPost(url, {'msg_id': msg_id }).then(function(response) {
                $scope.msg_info = 'Message flagged for administrative review.';
                $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                $scope.class_msg = 'success_msg';
                common_modal('common_modal_msg');
                show_hide_loader('search_loader_step', false);
                $scope.loadmessage('',sorting_value);
            });
        }
        $scope.loadmessage = function(msg_id,sorting_value) {
            if(sorting_value == '' || sorting_value == undefined)
            {
                sorting_value = $scope.sorting;
            }
            show_hide_loader('search_loader', true);
            var url = site_url + 'webservices/medical_msg/'+sorting_value;
            commonService.apiGet(url).then(function(response) {
                show_hide_loader('search_loader', false);
                $scope.messages = response.Data.messages_data;
                $scope.post_message_count = $scope.messages.length;
                $scope.pageSize = 5;
                $scope.filteredList = $scope.messages;
                $scope.currentPage = 0;
                if ($scope.post_message_count>0) {
                    window_play();
                }
                $scope.pagination();
                //$scope.sorting_msg($scope.sorting);
            });
        }
        $scope.loadmessage();
        $scope.pagination = function() {
            $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
            $scope.last_index = $scope.ItemsByPage.length - 1;
        };
        $scope.setPage = function() {
            $scope.currentPage = this.n;
            $scope.active = 1;
        };
        $scope.firstPage = function() {
            $scope.currentPage = 0;
            $scope.active = 1;
        };
        $scope.lastPage = function() {
            $scope.currentPage = $scope.ItemsByPage.length - 1;
            $scope.active = 1;
        };
        $scope.range = function(input, total) {
            var ret = [];
            if (!total) {
                total = input;
                input = 0;
            }
            for (var i = input; i < total; i++) {
                if (i != 0 && i != total - 1) {
                    ret.push(i);
                }
            }
            return ret;
        };
        $scope.delete_msg = function(id, index, pageno) {
            if (confirm('Are you sure you want to delete this message?')) {
                var url_delete = site_url + 'webservices/delete_msg_medical';
                  show_hide_loader('search_loader', true);
                commonService.apiPost(url_delete, {
                    'msg_id': id
                }).then(function(response) {
                    $scope.msg_info = 'Message has been deleted successfully.';
                    $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                    $scope.class_msg = 'success_msg';
                      show_hide_loader('search_loader', false);
                    $scope.PopUp();
                    $scope.messages.splice(index, 1);
                    $scope.ItemsByPage[pageno].splice(index, 1);
                });
            }
        };
        $scope.PopUp = function(){
            common_modal('common_modal_msg');
        };

        $scope.count_unreadmsg = '';
        
        // $scope.check_msg_status = function (){
        // var count_unreadmsg_url  = site_url + 'webservices/check_read_unread_status/'+$scope.sorting;
        //         commonService.apiGet(count_unreadmsg_url)
        //         .then(function(response) {
        //          $scope.status_msg = response.Data.status_msg;
        //         // console.log(response.Data.status_msg[0]['count_unreadmsg']);

        //     });
        // };
        //$scope.check_msg_status();
        // $scope.StartTimerMsgStatus = function () {

        //     //Set the Timer start message.
        //     $scope.Message = "Timer started. ";
        //     //Initialize the Timer to run every 1000 milliseconds i.e. one second.
        //     $scope.Timer = $interval(function () {
        //         //Display the current time.
        //       //$scope.check_msg_status();
        //     }, 30000);
        // };
        //$scope.StartTimerMsgStatus();

    }
]);