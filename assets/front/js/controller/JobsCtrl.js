angular.module('yolomd', ['ngSanitize']).controller('PostJob', ['$scope', '$rootScope', '$http', '$state', 'commonService', 'FileUploader', '$location', '$window', 'localStorageService', '$stateParams', 'ProvinceService', function($scope, $rootScope, $http, $state, commonService, FileUploader, $location, $window, localStorageService, $stateParams, ProvinceService) {
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
    $scope.CityProvince = '';
    $scope.count = 0;
    $scope.PostJobData = {};
    //$scope.PostJobData.nursing_array = {};
    //$scope.PostJobData.health_array = {};
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.PostJobData.facility_hours = [];
    $scope.FacilityDaysName = '';
    $scope.FacilityHoursAll = '';
    $scope.PreviewShow = '';
    $scope.PostJobData.draft = '';
    $scope.HoursValue = '';
    $scope.CompensationValue = '';
    $scope.StartDateValue = '';
    $scope.PostJobData.start_date = '';
    $scope.PostJobData.start_month = '';
    $scope.PostJobData.start_year = '';
    $scope.PostJobData.call_responsibility = 'No';
    $scope.PostJobData.position = 'Permanent';
    $scope.PostJobData.parking = 'None';
    $scope.MassageErr ='';
    $scope.UpdatePostalCodeErr = "";
    $scope.zip_code_data = [];
    //$scope.check_loader = '';
    show_hide_loader('search_loader_step', false);
    $scope.cloneData=function(getclonejobid){
    var job_content_url = site_url + 'webservices/clone';
    commonService.apiPost(job_content_url,{'job_id':getclonejobid})
    .then(function(response) {
        $scope.PostJobData = response.Data.post_data_clone; 
        //alert(response.Data.post_data_clone);
        $scope.PostJobData.listing_title = '';
        $scope.PostJobData.listing_description = '';
        if ($scope.PostJobData) {
                if ($scope.PostJobData.transport_metro_subway == 1) {
                    $scope.PostJobData.transport_metro_subway = true;
                }
                if ($scope.PostJobData.transport_bus == 1) {
                    $scope.PostJobData.transport_bus = true;
                }
                if ($scope.PostJobData.transport_other == 1) {
                    $scope.PostJobData.transport_other = true;
                }
                if ($scope.PostJobData.transport_train == 1) {
                    $scope.PostJobData.transport_train = true;
                }
               // $scope.imagesArray = response.Data.post_data_update.imagesarray;
               // console.log($scope.imagesArray);
            }
            if ($scope.PostJobData.facility_state) {
                $scope.get_city($scope.PostJobData.facility_state, 1);
            }
            if ($scope.PostJobData.facility_city) {
                //$scope.get_zipcode($scope.PostJobData.facility_city);
                 $scope.loadTags($scope.PostJobData.facility_city,$scope.PostJobData.facility_postal_code,1);
            }
           
            localStorage.clear('localStorageClone');
        });
    };
    var getclonejobid = localStorageService.get('localStorageClone');
    //alert(getclonejobid);
    if(getclonejobid)
    {
        $scope.cloneData(getclonejobid);
    }
    var content_url = site_url + 'webservices/physician_signup_content';
    var StartDate = '';
    var PreviewRedirection = '';
    $rootScope.pageTitle = $stateParams.pageTitle;
    commonService.apiGet(content_url).then(function(response) {
        $scope.currently_status = response.Data.currently_status;
        $scope.medical_school = response.Data.medical_school;
        $scope.medical_specialty = response.Data.medical_specialty;
        $scope.canada_province = response.Data.canada_province;
        $scope.physician_content = response.Data.physician_content;
    });

    $scope.get_city = function(selectionType,calling) {
         $scope.CityProvince = '';
         $scope.PostalCode = '';
        if (selectionType !== undefined || selectionType !== null) {
            show_hide_loader('search_loader_small', true);
            ProvinceService.getCity(selectionType).then(function(response) {
                show_hide_loader('search_loader_small', false);
                $scope.CityProvince = response.Data.result;
                if (calling != 1) {
                    $scope.PostJobData.facility_city = '';
                    $scope.PostalCode = '';
                    $scope.zip_code_data = [];
                }
             }, function(err) {
                show_hide_loader('search_loader_small', false);
           
            });
        }
    }
    
    $scope.get_zipcode = function(selectionType) {
        $scope.zip_code_data = [];
        // $scope.PostalCode = '';
        // if (selectionType !== undefined || selectionType !== null) {
        //     show_hide_loader('search_loader_small', true);
        //     ProvinceService.getZipcode(selectionType).then(function(response) {
        //         show_hide_loader('search_loader_small', false);
        //         $scope.PostalCode = response.Data.result;
        //     });
        // }
    }
   $scope.tagAdded = function(tag) {
        if($scope.zip_code_data.length < 2)
        {
            return true;
        }else{
            $scope.zip_code_data.splice($scope.zip_code_data.length - 1, 1);
            return $scope.zip_code_data;
        }
        
    };
    var tags_city = [];
    $scope.loadTags = function(cityId,query,check_load) {
        //alert(query);
        tags_city = [];
        
        if(check_load == 1)
        {
             $scope.zip_code_data  = [query];
             //alert(JSON.stringify($scope.zip_code_data));
        }else{
            var city_get = site_url + 'webservices/get_zip_code';
            return commonService.apiPost(city_get, {'city':cityId,'search':query}).then(function(response) {
                tags_city = response.Data.result;
                console.log(tags_city);
                return  tags_city;
            });
        }
        
        //return  tags_city;
    };
    //toggle map
    $scope.toggleclassfunctioning           = '';
    $scope.toggeleclass = function()
    {   
        $window.scrollTo(0, 0);
        if($scope.toggleclassfunctioning == 'mobile_preview_map'){
            $scope.toggleclassfunctioning = '';
        }
        else{
            $scope.toggleclassfunctioning = 'mobile_preview_map';
        }
    }

    var job_content_url = site_url + 'webservices/post_job_content';
    commonService.apiGet(job_content_url).then(function(response) {
        $scope.time_array = response.Data.time_array;
        $scope.time_array_eve = response.Data.time_array_eve;
        $scope.transport_type = response.Data.transport_type;
        $scope.facility_type = response.Data.facility_type;
        $scope.hours = response.Data.hours;
        $scope.compensation = response.Data.compensation;
        //$scope.health_professionals = response.Data.health_professionals;
        //$scope.nursing = response.Data.nursing;
        $scope.verified_facility = response.Data.verified_facility;
        $scope.paypal_priority = response.Data.paypal_priority;
        $scope.user_information = response.Data.user_information;
        $scope.PostJobData.contact_first_name = response.Data.user_information.user_first_name;
        $scope.PostJobData.contact_last_name = response.Data.user_information.user_last_name;
        $scope.PostJobData.contact_email_address = response.Data.user_information.user_email;
        $scope.PostJobData.contact_name_title = response.Data.user_information.user_name_title;
        $scope.master_days_name = response.Data.days_name;
        $scope.month_name = response.Data.month_name;
        if($scope.PostJobData.facility_hours.length< 1)
        {
             $scope.addMoreFacilityHours();
        }
       
    });
    $scope.postJob = 1;
    $scope.progressBar = 0;
    $scope.multipicationbar = 10;
    $scope.animationClass   ='fadeInUp';
    $window.scrollTo(0, 0);
    $scope.yourEvents = {
        onInitDone: function(item) {
            console.log(item);
        },
        onItemDeselect: function(item) {
            console.log(item);
        }
    };
    
    $scope.previousStep = function() {
        $window.scrollTo(0, 0);
        $scope.style_container = '';
        $scope.postJob = $scope.postJob - 1;
        $scope.progressBar = Math.ceil(($scope.postJob - 1) * $scope.multipicationbar);
        $scope.animationClass   ='fadeInUp';
        $window.scrollTo(0, 0);
        PreviewRedirection = 0;
    }
    //show_hide_loader('search_loader_step', false);
    $scope.SaveAsDraft = function() {
       
        if (!$scope.PostJobData.medical_specialty_name) {
            alert("Please select Medical Specialties.")
            return false;
        }
        if($scope.zip_code_data != '')
        {
            $scope.PostJobData.facility_postal_code = $scope.zip_code_data[0]['postal_code'];
        }
        show_hide_loader('search_loader_step', true);
        $scope.PostJobData.draft = 1;
        var PostData = $scope.PostJobData;
        var url = site_url + 'webservices/post_job';
        $scope.Message = '';
        commonService.apiPost(url, PostData).then(function(res) {
            show_hide_loader('search_loader_step', false);
            //alert('success');
            localStorageService.set('localStorageCheckRedirectDraft', 1);
            window.location = site_url + 'job-listing';
        }, function(err) {
            //console.log(err);
            show_hide_loader('search_loader_step', false);
            $scope.Message = err.Message;
            $scope.Error = err.Error;
            $scope.postJob = 1;
            $scope.progressBar = ($scope.postJob - 1) * $scope.multipicationbar;
            $scope.animationClass   ='fadeInUp';
            $window.scrollTo(0, 0);
        });
    }
    $scope.nextStep = function() {
        $window.scrollTo(0, 0);
        if (PreviewRedirection == 1 && $scope.postJob < 11) {
            $scope.postJob = 11;
            PreviewRedirection = 0;
        }
         //show_hide_loader('search_loader_step', false);
        if ($scope.postJob == 10) {
             show_hide_loader('search_loader_step', true);
           
            var uploadImageArray = [];
            $scope.style_container = 'padding:0px';
            $scope.PreviewShow = 1;
            angular.forEach(uploader.queue, function(value, key) {
                var imageName = value.file.name;
                uploadImageArray.push({
                    imageName:imageName
                });
            });
            $scope.PostJobData.uploadImageArray = uploadImageArray;
            //console.log(uploadImageArray);
            $scope.PreviewValue = {};
            
           /* var address = $scope.PostJobData.facility_address + "," + $scope.PostJobData.facility_city + "," + $scope.PostJobData.facility_state + "," + $scope.PostJobData.facility_postal_code + ", canada";;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $scope.PostJobData.latitude = results[0].geometry.location.lat();
                    $scope.PostJobData.longitude = results[0].geometry.location.lng();
                    initMap($scope.PostJobData.latitude, $scope.PostJobData.longitude);
                }
            });*/
            // if(check_loader == 1)
            // {
            //     show_hide_loader('search_loader_step', true);
            // }
           
            if($scope.zip_code_data != '')
            {
                $scope.PostJobData.facility_postal_code = $scope.zip_code_data[0]['postal_code'];
            }
            $scope.PostJobData.draft = '';
            $scope.FacilityHoursErr = '';
             //alert('hiiii');
            var PostData = $scope.PostJobData;
            var url = site_url + 'webservices/post_job';
            $scope.Message = '';
            commonService.apiPost(url, PostData).then(function(res) {
                //$scope.check_loader = '';
                 show_hide_loader('search_loader_step', false);
                 //alert('iiii');
                localStorageService.set('localStorageCheckRedirect', 1);
                //window.location = site_url + 'access/' + res.Data.last_id_key;
                //window.location = site_url + 'job-listing';
                window.location = site_url + 'job-preview/'+res.Data.last_id_key; 

            }, function(err) {
                //$scope.check_loader = '';
                show_hide_loader('search_loader_step', false);
                $scope.Message = err.Message;
                $scope.Error = err.Error;
                $scope.postJob = 1;
                $scope.progressBar = ($scope.postJob - 1) * $scope.multipicationbar;
                $scope.animationClass   ='fadeInUp';
                $window.scrollTo(0, 0);
            });
        } else {
            if ($scope.postJob == 5) {
                $scope.UpdatePostalCodeErr = "";
                $scope.UpdatePostalValidErr = '';
               //alert('ppp');
                if($scope.zip_code_data != '')
                {
                    $scope.PostJobData.facility_postal_code = $scope.zip_code_data[0]['postal_code'];
                    if( $scope.PostJobData.facility_postal_code)
                    {
                        var check_code = site_url + 'webservices/valid_postal_code';
                        commonService.apiPost(check_code, {
                            'city': $scope.PostJobData.facility_city,
                            'province': $scope.PostJobData.facility_state,
                            'postal_code': $scope.PostJobData.facility_postal_code
                        }).then(function(response) {
                            $scope.UpdatePostalCodeErr = '';
                            var latlon_url = site_url + 'webservices/get_latlog';
                            commonService.apiPost(latlon_url, {
                                'city': $scope.PostJobData.facility_city,
                                'state': $scope.PostJobData.facility_state,
                                'postal_code': $scope.PostJobData.facility_postal_code
                            }).then(function(response) {
                                //alert(JSON.stringify(response));
                                $scope.UpdatePostalValidErr = '';
                                $scope.PostJobData.longitude = response.Data.get_latlong.longitude;
                                $scope.PostJobData.latitude = response.Data.get_latlong.latitude;
                                $scope.postJob = $scope.postJob + 1;
                                $scope.progressBar = Math.ceil(($scope.postJob - 1) * $scope.multipicationbar);
                                $scope.animationClass   ='fadeInUp';
                                $window.scrollTo(0, 0);
                            },
                            function error(err) {
                               $scope.UpdatePostalCodeErr = '';
                              $scope.postJob = $scope.postJob;
                              $scope.animationClass ='fadeInUp';
                              $window.scrollTo(0, 0);
                            });
                          }, function error(err) {
                          
                          $scope.UpdatePostalCodeErr = '';
                          $scope.UpdatePostalValidErr = 'Valid postal code is required according to the province & city.';
                          $scope.postJob = $scope.postJob;
                          $scope.animationClass ='fadeInUp';
                          $window.scrollTo(0, 0);
                          
                        });   
                    }          
                }else{
                    $scope.UpdatePostalValidErr = '';
                    $scope.UpdatePostalCodeErr = "Postal Code is required";
                    $scope.postJob = $scope.postJob;
                    $scope.animationClass   ='fadeInUp';
                    $window.scrollTo(0, 0);
                }    
            } else {
               
                if($scope.postJob == 4) {
                    var curretdate = new Date().toISOString().slice(0,10);
                    var check_date = $scope.PostJobData.start_year+'/'+$scope.PostJobData.start_month+'/'+$scope.PostJobData.start_date;
                    var currentdateparse = Date.parse(curretdate);
                    var checkdateparse = Date.parse(check_date);
                   
                    if(currentdateparse <=  checkdateparse)
                    {
                        $scope.MassageErr = '';
                        $scope.postJob = $scope.postJob + 1;
                        $scope.progressBar = Math.ceil(($scope.postJob - 1) * $scope.multipicationbar);
                        $scope.animationClass   ='fadeInUp';
                        $window.scrollTo(0, 0);
                    }else{
                        $scope.MassageErr = "Start date should be greater than today's date.";
                        $scope.postJob = $scope.postJob;
                        $scope.animationClass   ='fadeInUp';
                        $window.scrollTo(0, 0);
                    }
            
                }else{
                     $scope.FacilityHoursErr = '';
                     if($scope.postJob == 7) {
                        values = $scope.PostJobData.facility_hours;
                        $check_facility_error = '';
                        angular.forEach(values, function(value, key){
                            if($scope.PostJobData.facility_hours[key]['master_days_name'] == '')
                            {
                                
                                $check_facility_error = 1;
                            }
                        });
                        if($check_facility_error != '')
                        {
                             $scope.FacilityHoursErr = "Select days are required";
                        }else{
                            $scope.postJob = $scope.postJob + 1;
                            $scope.progressBar = Math.ceil(($scope.postJob - 1) * $scope.multipicationbar);
                            $scope.animationClass  ='fadeInUp';
                            $window.scrollTo(0, 0);
                        }

                    }else{
                        $scope.postJob = $scope.postJob + 1;
                        $scope.progressBar = Math.ceil(($scope.postJob - 1) * $scope.multipicationbar);
                        $scope.animationClass   ='fadeInUp';
                        $window.scrollTo(0, 0);
                    }    
                }  

            }
           

        }
        
    }
  
    $scope.addMoreFacilityHours = function() {
        var temp = {
            'start_time': '',
            'end_time': ''
        };
        $scope.PostJobData.facility_hours.push(temp);
        console.log($scope.PostJobData.facility_hours);
    }
    $scope.delete = function(index1) {
        $scope.PostJobData.facility_hours.splice(index1, 1);
    }
    // $scope.uncheckAll_nursing = function(index) {
    //     if (index == 7) {
    //         if ($scope.PostJobData.nursing_array[8] == false) {
    //             $scope.PostJobData.nursing_array = [];
    //         } else {
    //             $scope.PostJobData.nursing_array = [];
    //             $scope.PostJobData.nursing_array[8] = true;
    //         }
    //     } else {
    //         $scope.PostJobData.nursing_array[8] = false;
    //     }
    // }
    // $scope.uncheckAll_health_pro = function(index) {
    //     if (index == 10) {
    //         if ($scope.PostJobData.health_array[11] == false) {
    //             $scope.PostJobData.health_array = [];
    //         } else {
    //             $scope.PostJobData.health_array = [];
    //             $scope.PostJobData.health_array[11] = true;
    //         }
    //     } else {
    //         $scope.PostJobData.health_array[11] = false;
    //     }
    // }
    var uploader = $scope.uploader = new FileUploader({
        url: site_url + 'cart/upload'
    });
    // FILTERS
    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            //alert(JSON.stringify(item));
            if (item.size > 10485760) {
                alert('Max file size is 10MB.');
                return false;
            } else {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|'.indexOf(type) !== -1;
            }
        }
    });
    $scope.imagerotate = function(index,type,image) {
        var deg = 0;
        var ele = angular.element('.uploadPhotoList').find(type).eq(index);
        var trans = ele.css('transform');
        if(trans=='none' || trans=='matrix(1, 0, 0, 1, 0, 0)'){
            deg = 90;     
        }
        else if(trans=='matrix(6.12323e-17, 1, -1, 6.12323e-17, 0, 0)' || trans=='matrix(0.00000000000000006123233995736766, 1, -1, 0.00000000000000006123233995736766, 0, 0)'){
            deg = 180;     
        }
        else if(trans=='matrix(-1, 1.22465e-16, -1.22465e-16, -1, 0, 0)' || trans=='matrix(-1, 0.00000000000000012246467991473532, -0.00000000000000012246467991473532, -1, 0, 0)'){
            deg = 270;     
        }
        else{
            deg = 0;    
        }
        ele.css('transform', "rotate("+deg+"deg)");
        var rotate_url = site_url + 'webservices/rotate_image';
        commonService.apiPost(rotate_url, {
            'img_url': image
        }).then(function(response) {
            //console.log(response);
        }, function error(err) {
            //console.log(err);          
        }); 
    }
    //console.log(uploader);
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        //console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        //console.info('onAfterAddingFile', fileItem);
        if (uploader.queue.length > 5) {
            fileItem.remove();
        }
        fileItem.upload();
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        //console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        //console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        //console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        //console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        //console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        //console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        //console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        //console.info('onCompleteAll');
    };
}]).controller('JobListing', ['$scope', '$filter', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$timeout', '$window', '$stateParams','localStorageService', function($scope, $filter, $rootScope, $http, $state, commonService, filteredListService, $timeout, $window, $stateParams,localStorageService) {
    $rootScope.showGlobalLoader = false;
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
    $scope.last_index = '';
    $scope.Header = ['', '', ''];
    $scope.order = '1';
    $scope.reverse = false;
    $pass_parameter = false;
    $scope.post_jobs_count = '';
    $scope.change_status = '';
    $scope.count = 0;
    $scope.UpdateMassage = '';
    iconName = 'glyphicon glyphicon-chevron-up';
    $scope.Header[0] = iconName;
    $scope.CheckPostValue = '';
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    show_hide_loader('search_loader', true);
    var url = site_url + 'webservices/job_listing';
    commonService.apiGet(url).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.post_jobs = response.Data.post_jobs;
        $scope.post_jobs_count = $scope.post_jobs.length;
        if ($scope.post_jobs_count>0) {
            window_play();
        }
        $scope.pageSize = 5;
        $scope.filteredList = $scope.post_jobs;
        $scope.currentPage = 0;
        $scope.pagination();
        //$scope.sort('id');
        $scope.CheckPostValue = response.Data.check_post_value;
        //alert($scope.CheckPostValue);
        
        Storageplacementdatadarft = localStorageService.get('localStorageCheckRedirectDraft');
        if(Storageplacementdatadarft)
        {
            $scope.msg_info = 'Job saved as draft.<br/><p style="text-decoration: underline;">You can complete this job posting later.</p>';
            $scope.icon = '<i class="fa fa-thumbs-up"></i>';
            $scope.class_msg = 'success_msg';
            $scope.PopUp();
            localStorage.clear();
        }    
    });
    
    $scope.pagination = function() {
        $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
        $scope.last_index = $scope.ItemsByPage.length - 1;
    };
    $scope.setPage = function() {
        $scope.currentPage = this.n;
        $scope.active = 1;
    };
    $scope.firstPage = function() {
        $scope.currentPage = 0;
        $scope.active = 1;
    };
    $scope.lastPage = function() {
        $scope.currentPage = $scope.ItemsByPage.length - 1;
        $scope.active = 1;
    };
    $scope.range = function(input, total) {
        var ret = [];
        if (!total) {
            total = input;
            input = 0;
        }
        for (var i = input; i < total; i++) {
            if (i != 0 && i != total - 1) {
                ret.push(i);
            }
        }
        return ret;
    };
    $scope.sort_order = function() {
        if ($scope.order == 1) {
            $pass_parameter = false;
        } else {
            $pass_parameter = true;
        }
        $scope.reverse = false;
        $pass_parameter = !$pass_parameter;
        $scope.columnToOrder1 = 'id';
        $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder1, $pass_parameter);
        $scope.Header[0] = iconName;
        $scope.pagination();
    }
    $scope.sort = function(sortBy) {
        $scope.count++;
        if ($scope.count == 1) {
            $scope.reverse = true;
        }
        $scope.columnToOrder = sortBy;
        $scope.reverse = !$scope.reverse;
        $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse);
        if ($scope.reverse) {
            iconName = 'glyphicon glyphicon-chevron-up';
            $scope.order = '1';
        } else {
            iconName = 'glyphicon glyphicon-chevron-down';
            $scope.order = '2';
        }
        if (sortBy === 'id') {
            $scope.Header[0] = iconName;
        } else {
            $scope.Header[2] = iconName;
        }
        $scope.pagination();
    };
    $scope.changestatus = function(updateStatus, id,pageno,index) {
        if (confirm('Are you sure to change status?')) {
            show_hide_loader('search_loader', true);
            var url_changestatus = site_url + 'webservices/changestatus_joblisting';
            commonService.apiPost(url_changestatus, {
                'status_value': updateStatus,
                'post_id': id
            }).then(function(response) {
                show_hide_loader('search_loader', false);
                $scope.msg_info = 'Job status has been changed successfully.';
                $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                $scope.class_msg = 'success_msg';
                $scope.PopUp();
                if (updateStatus == 1) {
                    $scope.ItemsByPage[pageno][index].status = 'Active';
                } 
                else if (updateStatus == 2) {
                    $scope.ItemsByPage[pageno][index].status = 'Deactive';
                }
                /*if (this.change_status == 1) {
                    return this.post_detail.status = 'Active';
                } else
                if (this.change_status == 2) {
                    return this.post_detail.status = 'Deactive';
                }*/
            });
        } else {
            if (this.change_status == 1) {
                return this.change_status = '2';
            } else
            if (this.change_status == 2) {
                return this.change_status = '1';
            }
        }
    };
    $scope.delete_post_job = function(id, index, pageno) {
        if (confirm('Are you sure you want to delete this job?')) {
             show_hide_loader('search_loader', true);
            var url_delete = site_url + 'webservices/delete_joblisting';
            commonService.apiPost(url_delete, {
                'post_id': id
            }).then(function(response) {
                 show_hide_loader('search_loader', false);
                $scope.msg_info = 'Job has been deleted successfully.';
                $scope.icon = '<i class="fa fa-thumbs-up"></i>';
                $scope.class_msg = 'success_msg';
                $scope.PopUp();
                $scope.post_jobs.splice(index, 1);
                $scope.ItemsByPage[pageno].splice(index, 1);
                $scope.post_jobs_count = $scope.post_jobs.length;
                $scope.pagination();
            });
        }
    };
    // $scope.create_clone = function(jobId){
    //     var job_content_url = site_url + 'webservices/clone';
    //     commonService.apiPost(job_content_url,{'job_id':jobId})
    //     .then(function(response) {
    //         window.location = site_url + 'post-job-update/'+response.Data.last_id;
    //     }); 
    // };    
    
    $scope.get_listname = function(name,id,job_list_id){
        $scope.name_listing = name;
        $scope.job_list_id = job_list_id;
        open_modal(id);
    };   
    $scope.PopUp = function(){
        common_modal('common_modal_msg');
    };
}]).controller('PremiumAccess', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', 'localStorageService', '$window', '$timeout', '$compile', function($scope, $rootScope, $http, $state, commonService, $stateParams, localStorageService, $window, $timeout, $compile) {
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    var jobId = $stateParams.jobId;
    $scope.PostData = {};
    $scope.CheckPostValue = '';
    if (jobId == '' || jobId == 'NULL') {
        window.location = site_url + 'job-listing';
    }
    //toggle map
    $scope.toggleclassfunctioning           = '';
    $scope.toggeleclass = function()
    {   
        if($scope.toggleclassfunctioning == 'mobile_preview_map'){
            $scope.toggleclassfunctioning = '';
        }
        else{
            $scope.toggleclassfunctioning = 'mobile_preview_map';
        }
    }
    //$scope.PostData.selectedTags = [];
    
    $scope.jobId = jobId;
    $scope.premiumjob = 1;
    $scope.nextStepPremium = function() {
        $scope.premiumjob = $scope.premiumjob + 1;
    }
    $scope.previousStepPremium = function() {
        $scope.premiumjob = $scope.premiumjob - 1;
    }
    
   
    $scope.PostData.selectedTags = '';
    show_hide_loader('search_loader', true);
    var priority_placement_url = site_url + 'webservices/priority_placement_content/' + jobId;
    commonService.apiGet(priority_placement_url).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.listing_title = response.Data.listing_title;
        $scope.job_verify = response.Data.job_verify;
        $scope.discount_amount = response.Data.discount_amount;
        $scope.CityData = response.Data.city_data;
        $scope.OthercityData = response.Data.othercity_data;
        $scope.yolomd_verify_fee = response.Data.yolomd_verify_fee;
        $scope.PriorityPlacement = response.Data.priority_placement;
        $scope.VerifiedFacility = response.Data.verified_facility;
        $scope.PhotographyFee = response.Data.photograph_fee;
        $scope.encrypt_id = response.Data.encrypt_id;
        $scope.CheckPostValue = response.Data.check_post_value;
        $scope.city_subscription = response.Data.city_subscription;
        $scope.yolomdservicetaken = response.Data.job_verify;
        // alert($scope.CheckPostValue);
        //$scope.PopUp();
        $scope.noResultsTag = null;
       // $scope.tags = response.Data.othercity_data;
        $scope.select2Options = {
            formatNoMatches: function(term) {
                console.log("Term: " + term);
                var message = '<a ng-click="addTag()">Add tag:"' + term + '"</a>';
                if (!$scope.$$phase) {
                    $scope.$apply(function() {
                        $scope.noResultsTag = term;
                    });
                }
                return message;
            }
        };
        
        $scope.$watch('noResultsTag', function(newVal, oldVal) {
            if (newVal && newVal !== oldVal) {
                $timeout(function() {
                    var noResultsLink = $('.select2-no-results');
                    console.log(noResultsLink.contents());
                    $compile(noResultsLink.contents())($scope);
                });
            }
        }, true);
    }, function(err) {
        window.location = site_url + 'job-listing';
    });
    $scope.gross_total = 0;
    $scope.PostData.placement_array = {};
    $scope.PostData.yolomd_verify = '';
    show_hide_loader('search_loader_small', false);
   
    $scope.nextStep = function() {
        localStorage.clear();
        localStorageService.set('localStorageplacementdata', '');
        PostData = $scope.PostData;
        show_hide_loader('search_loader_small', true);
        var calculatePlacements = site_url + 'webservices/calculate_placements/' + jobId;
        commonService.apiPost(calculatePlacements, PostData).then(function(response) {
            show_hide_loader('search_loader_small', false);
            localStorageService.set('localStorageplacementdata', response.Data);
            $scope.gross_total = response.Data.gross_total;
        });
    };
    // $scope.PopUp = function(){
    //     $scope.msg_info = 'Job information saved successfully. Admin will review soon once, the job get activated you will get notification email';
    //     $scope.icon = '<i class="fa fa-thumbs-up"></i>';
    //     $scope.class_msg = 'success_msg';
    //     common_modal('common_modal_msg');
    // };
    $scope.getpaydata = function() {
        Storageplacementdata = localStorageService.get('localStorageplacementdata');
        //alert(JSON.stringify(Storageplacementdata));
        $scope.placement_city = Storageplacementdata.placement_city;
        $scope.yolomd_verify_fee = Storageplacementdata.yolomd_verify_fee;
        $scope.discount = Storageplacementdata.discount;
        $scope.gross_total = Storageplacementdata.gross_total;
        $scope.jobId = Storageplacementdata.job_id;
        //$scope.actionUrl			= site_url+'cart/paypal_procces/'+Storageplacementdata.job_id;
        //alert($scope.actionUrl);
        $scope.actionUrl = site_url + 'cart/paymentByPaypal/' + Storageplacementdata.job_id;
    };
   
   
    var tags_city = [];
    $scope.loadTags = function(jobId,query) {

        tags_city = [];
        var city_get = site_url + 'webservices/get_city_subscribtion';
        return commonService.apiPost(city_get, {'job_id':jobId,'search':query}).then(function(response) {
            tags_city = response.Data.othercity_data;
            return  tags_city;
        });
        //return  tags_city;
    };
    $scope.check_post_status_redirect = function(){
        localStorageService.set('localStorageCheckRedirect', 1);
        window.location = site_url + 'job-preview/'+$scope.encrypt_id; 
    }
    $scope.close_preview = function(encrypt_id){
        if(confirm('Do you want to post free listing?'))
        {
            $("#paymentoverviewModal").modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            localStorageService.set('localStorageCheckRedirect', 1);
            window.location = site_url + 'job-preview/'+encrypt_id;
        }else{
            return false;
        }    
    }
}]).controller('PostJobUpdate', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', '$timeout', 'FileUploader', '$anchorScroll', '$timeout', '$window', 'ProvinceService','localStorageService', function($scope, $rootScope, $http, $state, commonService, $stateParams, $timeout, FileUploader, $anchorScroll, $timeout, $window, ProvinceService,localStorageService) {
    $scope.PopUp = function(){
        $scope.msg_info = 'Job posting updated successfully.';
        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
        $scope.class_msg = 'success_msg';
        common_modal('common_modal_msg');
    };
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user 
    var jobId = $stateParams.jobId;
    if (jobId == '' || jobId == 'NULL') {
        window.location = site_url + 'job-listing';
    }
    if (window.location.hash) {
        var hash_key = $window.location.hash.substring(1);
        //$timeout(function() {
            $anchorScroll('panel' + hash_key);
        //}, 1000);
    };
    if (localStorageService && localStorageService.get('localStoragePopUp')) {
        $scope.PopUp();
        localStorageService.set('localStoragePopUp',0);
    }
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.PostJobData = {};
    $scope.post_update_data = '';

    $scope.post_update = '';
    $scope.Message = '';
    $scope.ErrorCheck = 0;
    $scope.PostJobData.facility_postal_code = '';
    $scope.UpdatePostalCode = '';
    $scope.FacilityHoursErr = '';
    show_hide_loader('search_loader', true);
    $check_facility_error = '';
    $scope.zip_code_data = [];
    $scope.PostJobData.facility_hours = [];

    var content_url = site_url + 'webservices/physician_signup_content';
    commonService.apiGet(content_url).then(function(response) {
        $scope.currently_status = response.Data.currently_status;
        $scope.medical_school = response.Data.medical_school;
        $scope.medical_specialty = response.Data.medical_specialty;
        $scope.canada_province = response.Data.canada_province;
        $scope.physician_content = response.Data.physician_content;
    });
    var job_content_url = site_url + 'webservices/post_job_content';
    commonService.apiGet(job_content_url).then(function(response) {
        $scope.time_array = response.Data.time_array;
        $scope.time_array_eve = response.Data.time_array_eve;
        $scope.transport_type = response.Data.transport_type;
        $scope.facility_type = response.Data.facility_type;
        $scope.hours = response.Data.hours;
        $scope.compensation = response.Data.compensation;
        $scope.verified_facility = response.Data.verified_facility;
        $scope.paypal_priority = response.Data.paypal_priority;
        $scope.days_name = response.Data.days_name;
        $scope.month_name = response.Data.month_name;
        $scope.master_days_name = response.Data.days_name;
    });
    $scope.PostJobData.transport_bus = '';
    $scope.PostJobData.transport_metro_subway = '';
    $scope.PostJobData.transport_other = '';
    $scope.PostJobData.transport_train = '';
    var post_data_url = site_url + 'webservices/post_data_value';
    commonService.apiPost(post_data_url, {
        'Id': $stateParams.jobId
    }).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.PostJobData = response.Data.post_data_update;
        if ($scope.PostJobData) {
            if ($scope.PostJobData.transport_metro_subway == 1) {
                $scope.PostJobData.transport_metro_subway = true;
            }
            if ($scope.PostJobData.transport_bus == 1) {
                $scope.PostJobData.transport_bus = true;
            }
            if ($scope.PostJobData.transport_other == 1) {
                $scope.PostJobData.transport_other = true;
            }
            if ($scope.PostJobData.transport_train == 1) {
                $scope.PostJobData.transport_train = true;
            }
            $scope.imagesArray = response.Data.post_data_update.imagesarray;
        }
        if ($scope.PostJobData.facility_state) {
            $scope.get_city($scope.PostJobData.facility_state, 1);
        }
        if ($scope.PostJobData.facility_city) {
            $scope.loadTags($scope.PostJobData.facility_city,$scope.PostJobData.facility_postal_code,1);
        }
    });
    $scope.get_city = function(selectionType, calling) {
        show_hide_loader('search_loader_small', true);

        ProvinceService.getCity(selectionType).then(function(response) {
            show_hide_loader('search_loader_small', false);
            $scope.CityProvince = response.Data.result;
 
            if (calling != 1) {
                $scope.PostJobData.facility_city = '';
                $scope.PostalCode = '';
                $scope.zip_code_data = [];
            }
        }, function(err) {
            show_hide_loader('search_loader_small', false);
           
        });
        
    }
    $scope.get_zipcode = function(selectionType) {
        $scope.zip_code_data = [];
    }
   
    $scope.tagAdded = function(tag) {
        if($scope.zip_code_data.length < 2)
        {
            return true;
        }else{
            $scope.zip_code_data.splice($scope.zip_code_data.length - 1, 1);
            return $scope.zip_code_data;
        }
        
    };
    var tags_city = [];
    $scope.loadTags = function(cityId,query,check_load,stateval) {
        //alert(stateval);
        tags_city = [];
        
        if(check_load == 1)
        {
             $scope.zip_code_data  = [query];
             //alert(JSON.stringify($scope.zip_code_data));
        }else{
            var city_get = site_url + 'webservices/get_zip_code';
            return commonService.apiPost(city_get, {'city':cityId,'search':query,'stateval':stateval}).then(function(response) {
                tags_city = response.Data.result;
                return  tags_city;
            });
        }
    };
    var uploader = $scope.uploader = new FileUploader({
        url: site_url + 'cart/upload'
    });
    // FILTERS
    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item , options) {
            if (item.size > 10485760) {
                alert('Max file size is 10MB.');
                return false;
            } else {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|'.indexOf(type) !== -1;
            }
        }
    });
    uploader.onAfterAddingFile = function(fileItem) {
        new_length = 5 - ($scope.imagesArray.length);
        if (uploader.queue.length > new_length) {
            fileItem.remove();
        }
        fileItem.upload();
    };
    $scope.imagerotate = function(index,type,image) {
        var deg = 0;
        var ele = angular.element('.uploadPhotoList').find(type).eq(index);
        var trans = ele.css('transform');
        if(trans=='none' || trans=='matrix(1, 0, 0, 1, 0, 0)'){
            deg = 90;     
        }
        else if(trans=='matrix(6.12323e-17, 1, -1, 6.12323e-17, 0, 0)' || trans=='matrix(0.00000000000000006123233995736766, 1, -1, 0.00000000000000006123233995736766, 0, 0)'){
            deg = 180;     
        }
        else if(trans=='matrix(-1, 1.22465e-16, -1.22465e-16, -1, 0, 0)' || trans=='matrix(-1, 0.00000000000000012246467991473532, -0.00000000000000012246467991473532, -1, 0, 0)'){
            deg = 270;     
        }
        else{
            deg = 0;    
        }
        ele.css('transform', "rotate("+deg+"deg)");
        var rotate_url = site_url + 'webservices/rotate_image';
        commonService.apiPost(rotate_url, {
            'img_url': image
        }).then(function(response) {
            console.log(response);
        }, function error(err) {
            console.log(err);          
        }); 
    }
    $scope.imageremovefrompreset = function(index) {
        $scope.imagesArray.splice(index, 1);
    }
    $scope.addMoreFacilityHours = function() {
        var temp = {
            'start_time': '',
            'end_time': ''
        };
        $scope.PostJobData.facility_hours.push(temp);
        console.log($scope.PostJobData.facility_hours);
    }
    $scope.delete = function(index1) {
        $scope.PostJobData.facility_hours.splice(index1, 1);
    }
    $scope.formatDate = function(date) {
        if (date != '0000-00-00 00:00:00') {
            var array = date.split(' ');
            var d = Date.parse(array[0]);
            return d;
        }
    };
    $scope.PostJobUpdate = function() {
        //alert(JSON.stringify($scope.PostJobData.facility_hours.length));
        
        values = $scope.PostJobData.facility_hours;
        $check_facility_error = '';
        angular.forEach(values, function(value, key){
            if($scope.PostJobData.facility_hours[key]['master_days_name'] == '')
            {
                
                $check_facility_error = 1;
            }
        });
        //alert(JSON.stringify($scope.PostJobData.facility_hours.master_days_name.length));
        $scope.check_val_code = '';
        var curretdate = new Date().toISOString().slice(0,10);
        var check_date = $scope.PostJobData.start_year+'/'+$scope.PostJobData.start_month+'/'+$scope.PostJobData.start_date;
            var currentdateparse = Date.parse(curretdate);
            var checkdateparse = Date.parse(check_date);
            if($scope.zip_code_data != '')
            {
                $scope.get_check_result_postal_code = '';
                $scope.UpdatePostalValidErr = '';
                $scope.PostJobData.facility_postal_code = $scope.zip_code_data[0]['postal_code'];
            }
            $scope.UpdatePostalValidErr = '';
            if(currentdateparse <=  checkdateparse && $scope.zip_code_data != '' && $check_facility_error == '' )
            {
                var check_code = site_url + 'webservices/valid_postal_code';
                commonService.apiPost(check_code, {
                    'city': $scope.PostJobData.facility_city,
                    'province': $scope.PostJobData.facility_state,
                    'postal_code': $scope.PostJobData.facility_postal_code
                }).then(function(response) {
                    $scope.UpdateMassageErr = '';
                    $scope.UpdatePostalCodeErr = '';
                    $scope.FacilityHoursErr = '';
                    $scope.UpdatePostalValidErr = '';
                    //show_hide_loader('search_loader', true);
                    var uploadImageArray = [];
                    angular.forEach($scope.imagesArray, function(value, key) {
                        var imageName = value.url;
                        uploadImageArray.push({
                            imageName :imageName
                        });
                    });
                    angular.forEach(uploader.queue, function(value, key) {
                        var imageName = value.file.name;
                        uploadImageArray.push({
                            imageName :imageName
                        });
                    });
                    $scope.PostJobData.uploadImageArray = uploadImageArray;
                    
                    var latlon_url = site_url + 'webservices/get_latlog';
                    commonService.apiPost(latlon_url, {
                        'city': $scope.PostJobData.facility_city,
                        'state': $scope.PostJobData.facility_state,
                        'postal_code': $scope.PostJobData.facility_postal_code
                    }).then(function(response) {
                        $scope.PostJobData.longitude = response.Data.get_latlong.longitude;
                        $scope.PostJobData.latitude = response.Data.get_latlong.latitude;
                        var post_update_url = site_url + 'webservices/post_update';
                        var post_update = $scope.PostJobData;
                        commonService.apiPost(post_update_url, $scope.PostJobData).then(function(response) {
                            //show_hide_loader('search_loader', false);
                            
                            localStorageService.set('localStoragePopUp',1);
                            window.location.reload();
                            // if (response.Data.encrypt_id) {
                            //     window.location = site_url + 'access/' + response.Data.encrypt_id;
                            // }
                        });
                    }, function error(err) {
                        //show_hide_loader('search_loader', false);
                        $scope.ErrorCheck = 1;
                    }); 
                 $scope.UpdatePostalValidErr = '';
                  show_hide_loader('search_loader', false);
                }, function error(err) {
                  $scope.check_val_code = 0;
                  $scope.UpdateMassageErr = '';
                  $scope.UpdatePostalCodeErr = '';
                  $scope.FacilityHoursErr = '';
                  show_hide_loader('search_loader', false);
                  $scope.UpdatePostalValidErr = 'Valid postal code is required according to the province & city.';
                  
                });     
            }else{
               
                if(checkdateparse <  currentdateparse)
                {
                     
                     $scope.UpdateMassageErr = "Start date should be greater than today's date.";
                }
                if($scope.zip_code_data == '')
                {
                     
                      $scope.UpdatePostalCodeErr = "Postal Code is required";
                }
                if($check_facility_error != '')
                {
                     $scope.FacilityHoursErr = "Select days are required";
                }
               
            }    
    }
    $scope.check_valid_postal_code = function(){
        var check_code = site_url + 'webservices/valid_postal_code';
          commonService.apiPost(check_code, {
                    'city': $scope.PostJobData.facility_city,
                    'province': $scope.PostJobData.facility_state,
                    'postal_code': $scope.PostJobData.facility_postal_code
                }).then(function(response) {
                 $scope.check_val_code = 1;
                 $scope.get_check_result_postal_code = 1;
                 $scope.UpdatePostalValidErr = '';
                }, function error(err) {
                  $scope.check_val_code = 0;
                  $scope.UpdatePostalValidErr = 'Valid postal code is required according to the province & city.';
                  
                }); 
    }
}]).controller('Receipts', ['$scope', '$rootScope', '$http', '$state', 'commonService', 'filteredListService', '$filter', '$timeout', '$stateParams', function($scope, $rootScope, $http, $state, commonService, filteredListService, $filter, $timeout, $stateParams) {
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user
    $scope.receipts_count = '';
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.ReceiptsInfo = 0;
    $scope.reverse = false;
    var receipts_info = site_url + 'webservices/receipts';
    show_hide_loader('search_loader', true);
    commonService.apiGet(receipts_info).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.ReceiptsInfo = response.Data.receipts_information;
        $scope.receipts_count = $scope.ReceiptsInfo.length;
        $scope.pageSize = 7;
        $scope.filteredList = $scope.ReceiptsInfo;
        $scope.currentPage = 0;
        $scope.pagination();
        
    },
	function(err){
			show_hide_loader('search_loader', false);
		});



    $scope.pagination = function() {
        $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
        $scope.last_index = $scope.ItemsByPage.length - 1;
    };
    $scope.setPage = function() {
        $scope.currentPage = this.n;
        $scope.active = 1;
    };
    $scope.firstPage = function() {
        $scope.currentPage = 0;
        $scope.active = 1;
    };
    $scope.lastPage = function() {
        $scope.currentPage = $scope.ItemsByPage.length - 1;
        $scope.active = 1;
    };
    $scope.range = function(input, total) {
        var ret = [];
        if (!total) {
            total = input;
            input = 0;
        }
        for (var i = input; i < total; i++) {
            if (i != 0 && i != total - 1) {
                ret.push(i);
            }
        }
        return ret;
    };
    $scope.addition = function(firstvalue, secondvalue) {
        var sub = firstvalue - secondvalue;
        return sub.toFixed(2);
    };
    $scope.formatDate = function(date) {
        var array = date.split(' ');
        var d = Date.parse(array[0]);
        return d;
    };
    $scope.encodebase64 = function(job_id){
        var encodedString = Base64.encode(job_id);
        return encodedString; // Outputs: "SGVsbG8gV29ybGQh"
    }
    
}]).controller('Photographer', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', function($scope, $rootScope, $http, $state, commonService, $stateParams) {
    //commonService.CheckLogin('check_Medical_user_login');//Check Medical user
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
}]).controller('ReceiptsDownload', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', function($scope, $rootScope, $http, $state, commonService, $stateParams) {
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.message_error = '';
    var paymentId = $stateParams.paymentId;
    // alert(jobId);
    if (paymentId == '' || paymentId == 'NULL') {
        window.location = site_url + 'receipts';
    }
    var post_update_url = site_url + 'webservices/receipts_id';
    var payment_id = paymentId;
    commonService.apiPost(post_update_url, {
        'payment_id': payment_id
    }).then(function(response) {
        $scope.message_error = '';
        $scope.JobDetail = response.Data.jobs_detail;
        $scope.CityDetail = response.Data.city_detail;
    }, function(err) {
       $scope.message_error = 'Payment receipt information not found.';
           
    });
    $scope.formatDate = function(date) {
        if (date != '0000-00-00') {
            var check_date = new Date().toISOString().slice(0,10);;
            var t = Date.parse(check_date);
            var d = Date.parse(date);
            if(t <=  d)
            {
                return d;
            }else{
                
                var get_res = '(Expired)';
                return get_res;
            }
            
        }
    };

}]).controller('JobPreview', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams','localStorageService', function($scope, $rootScope, $http, $state, commonService, $stateParams,localStorageService) {
    commonService.CheckLogin('check_Medical_user_login');
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    var jobId = $stateParams.jobId;
    if (jobId == '' || jobId == 'NULL') {
        window.location = site_url + 'job-listing';
    }
    $scope.map_listing = 'View Map';

    $rootScope.toggleclassfunctioning_head = '';
    if ($( window ).width()<=800) {
        setTimeout(function(){ angular.element("#map").parent("div").css('display', 'none'); }, 3000);
    }
    $scope.toggeleclass = function() {
        if ($scope.toggleclassfunctioning == 'mobile_preview_map') {
            $rootScope.toggleclassfunctioning_head = '';
            angular.element("#map").parent("div").css('display', 'none');
            $scope.toggleclassfunctioning = '';
            $scope.map_listing = 'View Map';

        } else {
            $rootScope.toggleclassfunctioning_head = ' job-page-head';
            angular.element("#map").parent("div").css('display', 'block');
            $scope.toggleclassfunctioning = 'mobile_preview_map';
            $scope.map_listing = 'View Listing';
        }

    }
    show_hide_loader('search_loader', true);
    var url = site_url + 'webservices/job_preview/' + jobId;
    commonService.apiPost(url).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.encrypt_id            = jobId;
        $scope.jobDetail             = response.Data.jobs_data;
        $scope.jobs_image            = response.Data.jobs_image;
        $scope.MedicalSpecialtyValue = response.Data.medical_spec_name;
        $scope.FacilityType          = response.Data.facility_type;
       // $scope.HealthProfessional    = response.Data.health_professionals;
        $scope.ProvinceName          = response.Data.province_name;
       // $scope.NursingArray          = response.Data.nursing_array;
        $scope.FacilityDaysNameNew   = response.Data.facility_days_name;
        $scope.HoursValue            = response.Data.jobhours;
        $scope.StartDateValue        = response.Data.start_date;
        $scope.CompensationValue     = response.Data.compensation;
        $scope.ReceiptUniqueId       = response.Data.receipt_unique_id;
        $scope.JobStatus             = response.Data.status;
        $scope.MedicalSpecLogo       = response.Data.medical_spec_logo;
       // alert($scope.ReceiptUniqueId);
        var dateOrders = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        var FacilityDaysNameAll = [];
        angular.forEach(dateOrders, function(value1, key1) {
            angular.forEach(response.Data.facility_days_name, function(value, key) {
                if (value == value1) {
                    FacilityDaysNameAll.push(value);
                }
            });
        });
        $scope.FacilityDaysName = FacilityDaysNameAll;
        $scope.FacilityHoursAll = response.Data.facility_hours_all;
       
        initMap($scope.jobDetail.latitude, $scope.jobDetail.longitude);
        if (response.Data.payment_message) {
            $scope.PopUp();
        }
        Storageplacementcheckdata = '';
        Storageplacementcheckdata = localStorageService.get('localStorageCheckRedirect');
        if(Storageplacementcheckdata == 1 && $scope.JobStatus == 0) {
            localStorage.clear('localStorageCheckRedirect');
            $scope.PopUp_pending();
        }
    });

    $scope.PopUp = function(){
        var msg_section_one = 'Your Job has been posted successfully.';
        var msg_section_two = '';
        if($scope.JobStatus == 0)
        {
            msg_section_two = '<br/><p>Thank you for posting a job!<br/>Our team will review your ad and <br/>activate it when approved.<br/><br/>For any further questions, please contact us.</p>';
        }
        if($scope.JobStatus == 1)
        {
            msg_section_two = '<br/> Currently your job status is Active.<br/><br/> If you have any question you can contact us';
        }
        //var msg_val = '<br/><br/><a  class="button" target="_blank" href="receipts-download/'+ $scope.ReceiptUniqueId +'">View  receipt</a>';
        $scope.msg_info = msg_section_one + msg_section_two;
        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
        $scope.class_msg = 'success_msg';
        common_modal_premium('common_modal_msg');
    };
    $scope.PopUp_pending = function(){
        var msg_section_two = '';
        if($scope.JobStatus == 0)
        {
            msg_section_two = '<br/><p>Thank you for posting a job!<br/>Our team will review your ad and<br/>activate it when approved.<br/>For any further questions, please contact us.</p>';
        }
        
        $scope.msg_info = msg_section_two;
        $scope.icon = '<i class="fa fa-thumbs-up"></i>';
        $scope.class_msg = 'success_msg';
        common_modal_premium('common_modal_msg');
    };
}]).controller('StatisticsJob', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', 'filteredListService', function($scope, $rootScope, $http, $state, commonService, $stateParams, filteredListService) {
    //commonService.CheckLogin('check_Medical_user_login');//Check Medical user
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    var url = site_url + 'webservices/statistics_job';
    commonService.apiGet(url).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.statistics_jobs = response.Data.statistics_job;
        $scope.statistics_jobs_count = $scope.statistics_jobs.length;
        $scope.pageSize = 7;
        $scope.filteredList = $scope.statistics_jobs;
        $scope.currentPage = 0;
        $scope.pagination();
        //$scope.sort('id');
        
    });
    /*chart start*/
    $scope.graph = function(msg_count,job_count,statics_ratio,remaining_ratio,listing_title) {
       
        var new_data =  [];
        if(remaining_ratio == 0 && statics_ratio == 100)
        {
             new_data =  [];
        }else{
             new_data = [remaining_ratio];
        }
        $scope.single_title = listing_title;
        $scope.myJson = '';
        $scope.myJson = {
         type : "bar",
         title:{
           backgroundColor : "transparent",
           fontColor :"black",
           text : "Users visited & contacted for this job"
         },
         backgroundColor : "white",
         series : [
           {
             values : [msg_count,job_count],
             backgroundColor : "#4DC0CF"
           },
           ],
          "scale-x": {
               "labels":["No. of doctors <br/> contacts  for this <br/>job","No. of users  <br/> visited to the job"] /* Scale Labels */
             },
       };

        $scope.myJson1 = '';
        $scope.myJson1 = {
            type: "ring", 
            title: {
                textAlign: 'center',
                text: "Conversation rate",
            },
            plot:{
              detach:false,
              slice: 100 ,//to make a donut
              decimals:2
            },
            legend: {
            layout: "x5",
            position: "1%",
            borderColor: "transparent",
            marker: {
                borderRadius: 10,
                borderColor: "transparent"
            }
        },
        tooltip: {
            text: "%v %t"
        },
            series : [
                {
                    values : [statics_ratio],
                    text : "% Conversation rate"

                },
                {
                     values : new_data,
                    text : "% Contacted rate"

                }
            ]
        };
    };  
       /*chart end*/
    $scope.pagination = function() {
        $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
        $scope.last_index = $scope.ItemsByPage.length - 1;
    };
    $scope.setPage = function() {
        $scope.currentPage = this.n;
        $scope.active = 1;
    };
    $scope.firstPage = function() {
        $scope.currentPage = 0;
        $scope.active = 1;
    };
    $scope.lastPage = function() {
        $scope.currentPage = $scope.ItemsByPage.length - 1;
        $scope.active = 1;
    };
    $scope.range = function(input, total) {
        var ret = [];
        if (!total) {
            total = input;
            input = 0;
        }
        for (var i = input; i < total; i++) {
            if (i != 0 && i != total - 1) {
                ret.push(i);
            }
        }
        return ret;
    };
   
}])
.controller('CityCtrl', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', '$timeout', 'FileUploader', '$anchorScroll', '$timeout', '$window', 'ProvinceService', function($scope, $rootScope, $http, $state, commonService, $stateParams, $timeout, FileUploader, $anchorScroll, $timeout, $window, ProvinceService) {
    var tags_city = [];
    $scope.loadTags = function(jobId,query) {
        var city_get = site_url + 'webservices/get_city_subscribtion';
        commonService.apiPost(city_get, {'job_id':jobId,'search':query}).then(function(response) {
            tags_city = response.Data.othercity_data;
            return  tags_city;
        });
        return  tags_city;
    };
    // $scope.loadTags = function(query) {
       
    //     $scope.tags = [
    //       { "text": "Tag" },
    //       { "text": "Tag2" },
    //       { "text": "Tag3" },
    //       { "text": "Tag4" },
    //       { "text": "Tag5" },
    //       { "text": "Tag6" },
    //       { "text": "Tag7" },
    //       { "text": "Tag8" },
    //       { "text": "Tag9" },
    //       { "text": "Tag10" }
    //     ];
        
    // return $scope.tags;
    // };

}]).controller('CreateClone', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', 'localStorageService', '$window', '$timeout', '$compile', function($scope, $rootScope, $http, $state, commonService, $stateParams, localStorageService, $window, $timeout, $compile) {
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.skipStep = function() {
       window.location = site_url + 'post-job';
    }
    
     $scope.jobs_list = '';
    /*clone*/
   
    var url = site_url + 'webservices/clone_job_listing';
    commonService.apiGet(url).then(function(response) {
        $scope.jobs_list = response.Data.post_jobs;
        if(response.Data.post_jobs == ''){ window.location = site_url + 'post-job'; } 

    });
    $scope.jobs_list_name = '';
    localStorageService.set('localStorageClone', '');
    show_hide_loader('search_loader', false);
    $scope.create_clone = function(){
        localStorageService.set('localStorageClone', $scope.jobs_list_name);
        show_hide_loader('search_loader', true);
        window.location = site_url + 'post-job';
    }; 
     //$scope.create_clone();
    /*end clone*/
   
}]).controller('DownloadImage', ['$scope', '$rootScope', '$http', '$state', 'commonService', '$stateParams', 'localStorageService', '$window', '$timeout', '$compile','filteredListService', function($scope, $rootScope, $http, $state, commonService, $stateParams, localStorageService, $window, $timeout, $compile,filteredListService) {
    commonService.CheckLogin('check_Medical_user_login'); //Check Medical user login
    $rootScope.showGlobalLoader = false;
    $rootScope.headerClass = $state.current.name;
    $scope.state_name = $rootScope.headerClass;
    $scope.verified_jobs_count = '';
    $scope.reverse = false;
    $scope.verified_jobs = [];
    var url = site_url + 'webservices/checked_verified_jobs';
    commonService.apiGet(url).then(function(response) {
        show_hide_loader('search_loader', false);
        $scope.verified_jobs = response.Data.verified_jobs;
        $scope.verified_jobs_count = $scope.verified_jobs.length;
        $scope.pageSize = 5;
        $scope.filteredList = $scope.verified_jobs;
        $scope.currentPage = 0;
        $scope.pagination();
        //$scope.sort('id');
        
    });
    $scope.pagination = function() {
        $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
        $scope.last_index = $scope.ItemsByPage.length - 1;
    };
    $scope.setPage = function() {
        $scope.currentPage = this.n;
        $scope.active = 1;
    };
    $scope.firstPage = function() {
        $scope.currentPage = 0;
        $scope.active = 1;
    };
    $scope.lastPage = function() {
        $scope.currentPage = $scope.ItemsByPage.length - 1;
        $scope.active = 1;
    };
    $scope.range = function(input, total) {
        var ret = [];
        if (!total) {
            total = input;
            input = 0;
        }
        for (var i = input; i < total; i++) {
            if (i != 0 && i != total - 1) {
                ret.push(i);
            }
        }
        return ret;
    };
    $scope.image_name_title = '';
    $scope.title = '';
    $scope.showImage =function(image_name,title_name){
        $scope.image_name_title = '';
        $scope.title = '';
        $scope.image_name_title = image_name;
        $scope.title = title_name;
    }
}]);