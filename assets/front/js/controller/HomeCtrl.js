angular
.module('yolomd')
.controller('HomeCtrl',['$scope', '$rootScope', '$http','commonService','$state','$stateParams','$window', function($scope, $rootScope,$http,commonService,$state,$stateParams,$window)
   {
       $scope.slidesOnPage=3;
       $width=$window.innerWidth;
       if($width <= 480){
          $scope.slidesOnPage=1;
       }
       else if($width >= 480 && $width <= 800 ){
          $scope.slidesOnPage=2;
       }
       $rootScope.headerClass = $state.current.name;
           /* commonService.getLangauge('signup_facility').then(function(res){
       });*/
       $rootScope.pageTitle = $stateParams.pageTitle;
       $rootScope.showGlobalLoader = false;
       $scope.slickOnInit = function(){
          $scope.refreshing=true;
          $scope.$apply();
          $scope.refreshing=false;
          $scope.$apply();
       };
        $scope.check_social_login_val = '';   
        $scope.social_login_error = '';     
       var url = site_url + 'webservices/home_content';
       commonService.apiGet(url)
       .then(function(response) {
            $scope.banner_section = response.Data.banner_section;
            $scope.physician_medical_section = response.Data.physician_medical_section;
            $scope.city_section = response.Data.city_section;
            $scope.section_4 = response.Data.section_4;
            $scope.check_social_login_val = response.Data.check_social_login_val;
            $scope.social_login_error = response.Data.social_login_error;
            if($scope.check_social_login_val)
            {
              //alert($scope.social_login_error);
              $('#signupModal').modal('show');
            } 

       });
       setupInit();
       $scope.src_image = '';
       function setupInit() {
  
          var setupGet = new XMLHttpRequest();
          setupGet.onreadystatechange=function() {
             //alert(JSON.stringify(setupGet.readyState));
            if (setupGet.readyState==4 && setupGet.status==200) {
              //alert('fsdfsfs');
              var url_data = site_url;
              //var url_data = site_url+"sample/setup.php";
              $.get(url_data, function(data){
                
                  var setupData = JSON.parse(JSON.stringify(data || null ));
                  //alert(JSON.stringify(setupData));
                  //alert(setupData.stat);
                if (setupData.stat == 'ok') {
                  var iframeSrc = '[appDomain]/openid/embed?token_url=[baseUrl]token%2F&flags=stay_in_window,hide_sign_in_with';
                  window.jnBaseUrl = ''+window.location;
                  var encBaseUrl = encodeURIComponent(window.location);
                  window.jnAppId = setupData.settings.app_id;
                  iframeSrc = iframeSrc.replace('[appDomain]', setupData.settings.application_domain);
                  iframeSrc = iframeSrc.replace('[baseUrl]', encBaseUrl);
                  $scope.src_image = iframeSrc;
                  //alert($scope.src_image);
                }
              });
            }
          }
          setupGet.open('GET','setup.php',true);
          setupGet.send();
        }
       //alert(test);
    }
]);