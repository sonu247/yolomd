var markers = new Array();
var map ;
function initialize_detail(locations,firstLat,firstLng,hovericon) {
   var locations = locations; 
    map = new google.maps.Map(document.getElementById('map_canvas'), {
      scrollwheel           : false,
      zoom                  : 14,
      center                : new google.maps.LatLng( parseFloat(firstLat),  parseFloat(firstLng)),
      disableDefaultUI      : false,
      streetViewControl     : false,
      mapTypeControlOptions : { mapTypeIds: [] },
      mapTypeId             : google.maps.MapTypeId.ROADMAP
    });
    map.setOptions({ minZoom: 3, maxZoom: 20 });
    var marker, i;
    // var markers = new Array();
    var styles = [
      {
        featureType: "poi",
        elementType: "business",
        stylers: [
          { visibility: "off" }
        ]
        },
        {
        featureType: 'water',
        elementType: 'all',
        stylers: [
          { hue: '#46bcec' },
          { saturation: 45 },
          { lightness: -11 },
          { visibility: 'on' }
        ]
      },{
        featureType: 'road.highway',
        elementType: 'all',
        stylers: [
          { hue: '#E0FFFF' },
          { saturation: 100 },
          { lightness: 83 },
          { visibility: 'on' }
        ]
      },
      {
      featureType: 'poi.park',
      elementType: 'all',
      stylers: [
        { hue: '#CFECEC' },
        { saturation: -42 },
        { lightness: -17 },
        { visibility: 'on' }
      ]
      },{
      featureType: 'administrative.province',
      elementType: 'labels',
      stylers: [
        { hue: '#555555' },
        { saturation: 0 },
        { lightness: -35 },
        { visibility: 'simplified' }
      ]
    },
    {
      featureType: 'administrative.province',
      elementType: 'labels',
      stylers: [
        { hue: '#555555' },
        { saturation: 0 },
        { lightness: -35 },
        { visibility: 'off' }
      ]
    },
    {
    featureType: 'landscape.man_made',
    elementType: 'geometry',
    stylers: [
      { hue: '#D1D0CE' },
      { saturation: -88 },
      { lightness: -9 },
      { visibility: 'on' }
    ]
    }
    ];
    map.setOptions({styles: styles});
    Object.size = function(obj) {
     var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
     };
var markerclusterer = null;
var mc = new MarkerClusterer(map);
for (i = 0; i < Object.size(locations); i++) { 
      marker = new google.maps.Marker({
        position     : new google.maps.LatLng(locations[i][1], locations[i][2]),
        icon         : site_url+'assets/front/images/'+locations[i][4],
        map          : map,
        jobID        : locations[i][3],
        draggable    : false,
        originalicon : site_url+'/assets/front/images/'+locations[i][4],
        hovericon    : site_url+'assets/front/images/'+hovericon,
        zIndex       : Math.round((locations[i][1], locations[i][2])*-100000)<<5
      });
      google.maps.event.addListener(marker, 'click', function() {
        //alert(this.jobID);

        var appElement=document.querySelector('[ng-app=yolomd]');
        var $rootscope = angular.element(appElement).scope();
        console.log($rootscope);
        $rootscope.ismarkerClicked=this.jobID;
        $rootscope.$apply();

          // window.location.href = this.jobID;
      });
      google.maps.event.addListener(marker, "mouseover", function() {
           this.setIcon(this.hovericon);
      });
      google.maps.event.addListener(marker, "mouseout", function() { 
        this.setIcon(this.originalicon); 
      });
      markers.push(marker);
      mc.addMarker(marker);
      /* map.setZoom(14);
       map.setCenter(marker.getPosition());*/
      //var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'images/m1.png'});
      /*google.maps.event.addListener(mc, 'clusterclick', function(cluster) {
        map.setZoom(9);
        map.setCenter(marker.getPosition());
      });*/
    function AutoCenter() {
      var bounds = new google.maps.LatLngBounds();
      $.each(markers, function (index, marker) {
        bounds.extend(marker.position);
      });
      map.fitBounds(bounds);
    }
    //AutoCenter();
   }
  }

/*function changemarkericonjs(markercounting,icon)
 { 
  
   markers[markercounting].setIcon(site_url+"assets/front/images/"+icon);
  }

 function reversemarkericonjs(markercounting,icon)
 { 
  
   markers[markercounting].setIcon(site_url+"assets/front/images/"+icon);
  }
*/