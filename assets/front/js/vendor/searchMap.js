var markers = new Array();
var map;

function initialize(locations, firstLat, firstLng, hovericon, zoomLevel) {
    if (firstLat != '' && firstLng != '') {
        // alert(JSON.stringify(zoomLevel));
        var locations = locations;
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            scrollwheel: false,
            zoom: zoomLevel,
            center: new google.maps.LatLng(parseFloat(firstLat), parseFloat(firstLng)),
            disableDefaultUI: false,
            streetViewControl: false,
            mapTypeControlOptions: {
                mapTypeIds: []
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        if (zoomLevel > 13) {
            map.setOptions({
                minZoom: 4,
                maxZoom: 18
            });
        } else {
            map.setOptions({
                minZoom: 4,
                maxZoom: 15
            });
        }
        var marker, i;
        var styles = [{
                featureType: "poi",
                elementType: "business",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                featureType: "water",
                elementType: "all",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#73b6e6"
                    }
                ]
            },
            {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [{
                        "visibility": "on"
                    },
                    {
                        "color": "#f8f8f8"
                    }
                ]
            },
            {
                featureType: "road",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                featureType: "road.highway",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                featureType: "road",
                elementType: "geometry",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#ffffff"
                    }
                ]
            },
            {
                featureType: "road.arterial",
                elementType: "labels",
                stylers: [{
                    visibility: "on"
                }]
            },
            {
                featureType: "road.arterial",
                elementType: "labels.text",
                stylers: [{
                    visibility: "on"
                }]
            },
            {
                featureType: "road.local",
                elementType: "labels.text",
                stylers: [{
                    visibility: "on"
                }]
            },
            {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#cddaa2"
                    }
                ]
            },
            {
                featureType: "landscape",
                elementType: "geometry.fill",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#ede7e1"
                    }
                ]
            },
            {
                featureType: "landscape",
                elementType: "all",
                stylers: [{
                    "color": "#ede7e1"
                }]
            },
        ];
        map.setOptions({
            styles: styles
        });
        Object.size = function(obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        var latlngbounds = new google.maps.LatLngBounds();


        var markerclusterer = null;
        var mc = new MarkerClusterer(map);
        for (i = 0; i < Object.size(locations); i++) {
            //alert(site_url+'assets/front/images/'+locations[i][4]);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                icon: site_url + 'assets/front/images/' + locations[i][4],
                map: map,
                jobID: locations[i][3],
                draggable: false,
                originalicon: site_url + '/assets/front/images/' + locations[i][4],
                hovericon: site_url + 'assets/front/images/' + hovericon,
                zIndex: Math.round((locations[i][1], locations[i][2]) * -100000) << 5
            });
            google.maps.event.addListener(marker, 'click', function() {
                //alert(this.jobID);

                var appElement = document.querySelector('[ng-app=yolomd]');
                var $rootscope = angular.element(appElement).scope();
                console.log($rootscope);
                $rootscope.ismarkerClicked = this.jobID;
                $rootscope.$apply();

                // window.location.href = this.jobID;
            });
            latlngbounds.extend(marker.position);

            // google.maps.event.addListener(marker, "mouseover", function() {
            //      this.setIcon(this.hovericon);
            // });
            // google.maps.event.addListener(marker, "mouseout", function() { 
            //   this.setIcon(this.originalicon); 
            // });
            markers.push(marker);
            mc.addMarker(marker);
            //var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'images/m1.png'});
            /*google.maps.event.addListener(mc, 'clusterclick', function(cluster) {
              map.setZoom(9);
              map.setCenter(marker.getPosition());
            });*/
            //  function AutoCenter() {
            //    var bounds = new google.maps.LatLngBounds();
            //    $.each(markers, function (index, marker) {
            //      bounds.extend(marker.position);
            //    });
            //    map.fitBounds(bounds);
            //  }
            // if(zoomLevel==10){
            //   AutoCenter();
            //  }
            //bounds.extend(marker.position);
            google.maps.event.trigger(map, "resize");
        }
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();

        //Center map and adjust Zoom based on the position of all markers.
        if (zoomLevel <= 13) {
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);
        }
    } else {

        var mapCanvas = document.getElementById("map_canvas");
        var mapOptions = {
            scrollwheel: false,
            disableDefaultUI: false,
            streetViewControl: false,
            mapTypeControlOptions: {
                mapTypeIds: []
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: new google.maps.LatLng(49.246292, -123.116226),
            zoom: 4
        }

        var map = new google.maps.Map(mapCanvas, mapOptions);
        map.setOptions({
            minZoom: 4,
            maxZoom: 20
        });

        var marker, i;
        var styles = [{
                featureType: "poi",
                elementType: "business",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                featureType: "water",
                elementType: "all",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#73b6e6"
                    }
                ]
            },
            {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [{
                        "visibility": "on"
                    },
                    {
                        "color": "#f8f8f8"
                    }
                ]
            },
            {
                featureType: "road",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                featureType: "road.highway",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            },
            {
                featureType: "road",
                elementType: "geometry",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#ffffff"
                    }
                ]
            },
            {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#cddaa2"
                    }
                ]
            },
            {
                featureType: "landscape",
                elementType: "geometry.fill",
                stylers: [{
                        visibility: "on"
                    },
                    {
                        color: "#ede7e1"
                    }
                ]
            },
            {
                featureType: "landscape",
                elementType: "all",
                stylers: [{
                    "color": "#ede7e1"
                }]
            },
        ];
        map.setOptions({
            styles: styles
        });

    }
    // resizeofmap(); 
}
function initialize_new(locations, firstLat, firstLng, hovericon, zoomLevel) {
    //alert('dsa');
    //var map;
    google.maps.event.trigger(map, 'resize');
    //map = new google.maps.Map(document.getElementById("map_canvas")); 
    //google.maps.event.trigger(map, 'resize');
}
// function changemarkericonjs(markercounting,icon)
//  {   
//    markers[markercounting].setIcon(site_url+"assets/front/images/"+icon);
//   }

//  function reversemarkericonjs(markercounting,icon)
//  {   
//    markers[markercounting].setIcon(site_url+"assets/front/images/"+icon);
//   }

function Zoom(markerNum) {
    map.panTo(markers[markerNum].getPosition());
    map.setZoom(9);
}
/*function resizeofmap(){
    var mapCanvas = document.getElementById("map_canvas");
    var mapOptions = {
      scrollwheel           : false,
      disableDefaultUI      : false,
      streetViewControl     : false,
      mapTypeControlOptions : { mapTypeIds: [] },
      mapTypeId             : google.maps.MapTypeId.ROADMAP,
      center: new google.maps.LatLng(49.246292, -123.116226),
      zoom: 3
    }

    var map = new google.maps.Map(mapCanvas, mapOptions);
    map.setOptions({ minZoom: 4, maxZoom: 20 });
}*/
// function myMap() {

//   var mapCanvas = document.getElementById("map");
//   var mapOptions = {
//     center: new google.maps.LatLng(51.5, -0.2),
//     zoom: 10
//   }
//   var map = new google.maps.Map(mapCanvas, mapOptions);
// }

function get_meta_tag() {

    $("head").append('<meta property="og:title" content="blubb1">');
    $("meta[property='og\\:title']").attr("content", 'dsfsfsffdf');
    $("head").append('<meta property="og:description" content="blubb2">');
    $("head").append('<meta property="og:url" content="blubb3">');
    var url = "http://205.134.251.196/~examin8/CI/yoloMD/jobs/28";
    window.location.href = "https://plus.google.com/share?url=" + url;

}