jQuery(document).ready(function($) {

    // on scroll header  resize
    $(window).scroll(function() {    
      var scroll          = $(window).scrollTop();
      var headeHeight     = $('.fixed-header').height();

      if (scroll >= 200) {

        $(".fixed-header").addClass("custom-head");
        
      }
      else{
         $(".fixed-header.home_class").removeClass("custom-head");
        
      }
    });

    //============Home effect==============//


});

$('.city-slider').slick({
  dots: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  speed: 600,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
});

jQuery(document).ready(function($) {

  var elementoffset = $('.home-signup-warp').offset().top;
  var countScrol=1;
  $(window).scroll(function(){ 
      var scrollTop = $(window).scrollTop();
      if(scrollTop  >= elementoffset-450 && countScrol ===1){
       countScrol =2;
         wow = new WOW(
            {
              boxClass:     'wow',    
              animateClass: 'animated', 
              offset:       200,         
              mobile:       true,       
              live:         true        
            }
          )
          wow.init();
          event.stopPropagation();
      }
  });
});