var map;
function initMap(lat, lng) 
  {
  	lat = parseFloat(lat);
  	lng = parseFloat(lng);
    map = new google.maps.Map(document.getElementById('map'), {
      'scrollwheel'      : false,
      'center'           : {'lat': parseFloat(lat), 'lng': parseFloat(lng)},
      'zoom'             : 15,
      'disableDefaultUI' : false,
      'streetViewControl': false,
      'mapTypeId'        : google.maps.MapTypeId.ROADMAP,
      'mapTypeControlOptions' : { mapTypeIds: [] },
     /* 'mapTypeControl'   : false*/
    });
    var myCenter=new google.maps.LatLng(lat, lng);
    var marker=new google.maps.Marker({
		    position:myCenter,
		    icon:site_url+'assets/front/images/map/markerIcon.svg',
	  });
    marker.setMap(map);

      map.setOptions({ minZoom: 3, maxZoom: 20 });
      var marker, i;
      var styles = [
        {
          featureType: "poi",
          elementType: "business",
          stylers: [
            { visibility: "off" }
          ]
        },
        {
          featureType: "water",
          elementType: "all",
          stylers: [
              {
                  visibility: "on"
              },
              {
                  color: "#73b6e6"
              }
          ]
        },
        {
            featureType: "road.highway",
            elementType: "geometry.fill",
            stylers: [
                {
                    "visibility": "on"
                },
                {
                    "color": "#f8f8f8"
                }
            ]
        },
        {
            featureType: "road",
            elementType: "labels",
            stylers: [
                {
                    visibility: "off"
                }
            ]
        },
        {
            featureType: "road.highway",
            elementType: "labels",
            stylers: [
                {
                    visibility: "off"
                }
            ]
        },
        {
            featureType: "road",
            elementType: "geometry",
            stylers: [
                {
                    visibility: "on"
                },
                {
                    color: "#ffffff"
                }
            ]
        },
        {
            featureType: "road.arterial",
            elementType: "labels",
            stylers: [
                {
                    visibility: "on"
                }
            ]
        },
        {
            featureType: "road.local",
            elementType: "labels.text",
            stylers: [
                {
                    visibility: "on"
                }
            ]
        },
        {
            featureType: "road.arterial",
            elementType: "labels.text",
            stylers: [
                {
                    visibility: "on"
                }
            ]
        },
        {
          featureType: "poi.park",
          elementType: "geometry",
          stylers: [
              {
                  visibility: "on"
              },
              {
                  color: "#cddaa2"
              }
            ]
        },
       {
          featureType: "landscape",
          elementType: "geometry.fill",
          stylers: [
              {
                  visibility: "on"
              },
              {
                  color: "#ede7e1"
              }
          ]
       },
       {
          featureType: "landscape",
          elementType: "all",
          stylers: [
              {
                  "color": "#ede7e1"
              }
          ]
      },
      ];
      map.setOptions({styles: styles});
  }

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      var place = results[i];
      console.log(" Places ", place);
      createMarker(results[i]);
    }
  }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: map,
      position: place.geometry.location
    });

    marker.setMap(map);
/*
    google.maps.event.addListener(marker, 'click', function() {
      infowindow.setContent(place.name);
      infowindow.open(map, this);
    });*/
  }