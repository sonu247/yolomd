app = angular.module('yolomd');
app.service('commonService', function($q, $http, $rootScope) {
    this.apiPost = function(url, postData) {
        var deferred = $q.defer();
        $http.post(url, postData).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
    this.apiGet = function(url) {
        var deferred = $q.defer();
        $http.get(url).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
    this.getLangauge = function(key) {
        var deferred = $q.defer();
        url = site_url + 'home/lang_data/' + key;
        console.log(url);
        $http.get(url).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
    this.CheckLogin = function(check_user_login, PostJob) {
        var url = site_url + 'webservices/' + check_user_login;
        $http.get(url).success(function(res) {
            return true;
        }).error(function(err) {
            if (PostJob) {
                window.location = site_url + 'signup';
            } else {
                window.location = site_url;
            }
        });
    }
    this.CheckLoginredirection = function() {
        var url = site_url + 'webservices/CheckLoginredirection';
        $http.get(url).success(function(res) {
            window.location = site_url + res.Data;
        });
    }
});
app.directive('onlyNumSpecial', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^-0-9\-]/g, '');
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
})
app.directive('onlyNum', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.service('filteredListService', function() {
    this.paged = function(valLists, pageSize) {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };
});
app.filter('plusless', function() {
    return function(input) {
        if (input) {
            return input.split('+').join(',');
        }
    }
});
app.filter('capitalize', function() {
    return function(input, all) {
        var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
        return (!!input) ? input.replace(reg, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});
app.factory('ProvinceService', ['$http', '$q', function($http, $q) {
    var factory = {
        getCity: function(selectionType) {
            var deferred = $q.defer();
            var city_url = site_url + 'webservices/get_city';
            $http.post(city_url, {
                'provice': selectionType
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        },
        getZipcode: function(selectionType) {
            var deferred = $q.defer();
            var city_url = site_url + 'webservices/get_zip_code';
            $http.post(city_url, {
                'city': selectionType
            }).success(function(data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }
    }
    return factory;
}]);
app.directive('scroll', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            scope.$watchCollection(attr.scroll, function(newVal) {
                $timeout(function() {
                    element[0].scrollTop = element[0].scrollHeight;
                });
            });
        }
    }
});
app.directive('myDirective', ['$window', function ($window) {

     return {
        link: link,
        restrict: 'E',
        template: '<div>window size: {{width}}px</div>'
     };

     function link(scope, element, attrs){

       scope.width = $window.innerWidth;

       angular.element($window).bind('resize', function(){

         scope.width = $window.innerWidth;

         // manuall $digest required as resize event
         // is outside of angular
         scope.$digest();
       });

     }

 }]);