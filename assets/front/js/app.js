app = angular.module('yolomd', ['ui.router', 'oc.lazyLoad', 'ngSanitize', 'slick', 'ui.bootstrap', 'ngMask', 'angularFileUpload', 'LocalStorageModule', 'infinite-scroll', 'ui.select2', 'zingchart-angularjs', 'ngTagsInput']);
app.directive("whenScrolled", function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            raw = elem[0];
            if (raw.nodeName.toLowerCase() === 'input') {
                elem.bind("keyup", function() {
                    if (scope.req.searchSendText.length >= 3) {
                        scope.loading = true;
                        scope.req.sendrequest = [];
                        scope.$apply(attrs.whenScrolled);
                    } else if (!scope.req.searchSendText) {
                        scope.$apply(attrs.whenScrolled);
                    }
                });
            } else {
                elem.bind("scroll", function() {
                    if (raw.scrollTop + raw.offsetHeight + 5 >= raw.scrollHeight) {
                        scope.loading = true;
                        scope.$apply(attrs.whenScrolled);
                    }
                });
            }
        }
    }
});
app.config(['$stateProvider', '$locationProvider', '$httpProvider', '$urlRouterProvider', '$ocLazyLoadProvider', 'localStorageServiceProvider', function($stateProvider, $locationProvider, $httpProvider, $urlRouterProvider, $ocLazyLoadProvider, localStorageServiceProvider) {
    $ocLazyLoadProvider.config({
        debug: false
    });
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('Home', {
        url: '/',
        templateUrl: site_url + 'template/home/index',
        data: {
            pageTitle: 'Home',
            routename: 'Home'
        },
        controller: 'HomeCtrl',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/HomeCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('Signup', {
        url: '/signup',
        templateUrl: site_url + 'template/auth/signup',
        data: {
            pageTitle: 'Signup',
            routename: 'Home'
        },
        controller: 'signup',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AuthCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('MedicalFacilitySignup', {
        url: '/medical-facility-signup',
        templateUrl: site_url + 'template/auth/signup_medical_facility',
        data: {
            pageTitle: 'Medical Facility Signup',
            routename: 'Home'
        },
        controller: 'MedicalFacilitySignup',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AuthCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhysicianSignup', {
        url: '/physician-signup',
        templateUrl: site_url + 'template/auth/signup_physician',
        data: {
            pageTitle: 'Physician Signup',
            routename: 'Home'
        },
        controller: 'PhysicianSignup',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AuthCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('MedicalDashboard', {
        url: '/medical-dashboard',
        templateUrl: site_url + 'template/account/MedicalDashboard',
        data: {
            pageTitle: 'Medical Dashboard',
            routename: 'Home'
        },
        controller: 'MedicalDashboard',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountMedicalCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('MedicalProfile', {
        url: '/medical-profile',
        templateUrl: site_url + 'template/account/MedicalProfile',
        data: {
            pageTitle: 'Medical Profile',
            routename: 'Home'
        },
        controller: 'MedicalProfile',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountMedicalCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('ChangePassword', {
        url: '/medical-change-password',
        templateUrl: site_url + 'template/account/MedicalChangePassword',
        data: {
            pageTitle: 'Change Password',
            routename: 'Home'
        },
        controller: 'ChangePassword',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountMedicalCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PostJob', {
        url: '/post-job',
        templateUrl: site_url + 'template/account/PostJob',
        data: {
            pageTitle: 'Post Job',
            routename: 'Home'
        },
        controller: 'PostJob',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js', 'assets/front/file_upload/directives.js'],
                    serie: true
                });
            }]
        }
    }).state('MedicalMessage', {
        url: '/medical-message',
        templateUrl: site_url + 'template/account/MedicalMessage',
        data: {
            pageTitle: 'Medical Message',
            routename: 'Home'
        },
        controller: 'MedicalMessage',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountMedicalCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('JobListing', {
        url: '/job-listing',
        templateUrl: site_url + 'template/account/MediacalJobListing',
        data: {
            pageTitle: 'Job Listing',
            routename: 'Home'
        },
        controller: 'JobListing',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PriorityPlacement', {
        url: '/placement/:jobId',
        templateUrl: site_url + 'template/account/PriorityPlacement',
        data: {
            pageTitle: 'Priority Placement',
            routename: 'Home'
        },
        controller: 'PriorityPlacement',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhysicianChangePassword', {
        url: '/physician-change-password',
        templateUrl: site_url + 'template/account/PhysicianChangePassword',
        data: {
            pageTitle: 'Change Password',
            routename: 'Home'
        },
        controller: 'PhysicianChangePassword',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountPhysicianCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('jobs', {
        url: '/jobs/:jobId',
        templateUrl: site_url + 'template/job/Joblist',
        data: {
            pageTitle: 'Jobs',
            routename: 'Home'
        },
        controller: 'jobs',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/Job.js'],
                    serie: true
                });
            }]
        }
    }).state('error_404', {
        url: '/page',
        templateUrl: site_url + 'template/errors/error_404',
        data: {
            pageTitle: '404',
            routename: 'Home'
        },
        controller: 'error_404',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/CommonCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PostJobUpdate', {
        url: '/post-job-update/:jobId',
        templateUrl: site_url + 'template/account/PostJobUpdate',
        data: {
            pageTitle: 'Post Job Update',
            routename: 'Home'
        },
        controller: 'PostJobUpdate',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js', 'assets/front/file_upload/directives.js'],
                    serie: true
                });
            }]
        }
    }).state('ContactUs', {
        url: '/contact-us',
        templateUrl: site_url + 'template/pages/ContactUs',
        data: {
            pageTitle: 'Contact Us',
            routename: 'Home'
        },
        controller: 'ContactUs',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('FAQ', {
        url: '/faq',
        templateUrl: site_url + 'template/pages/FAQ',
        Data: {
            pageTitle: 'FAQ',
            routename: 'Home'
        },
        param: {
            pageTitle: 'FAQ',
            routename: 'Home'
        },
        controller: 'FAQ',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('Receipts', {
        url: '/receipts',
        templateUrl: site_url + 'template/account/Receipts',
        data: {
            pageTitle: 'Payment receipts',
            routename: 'Home'
        },
        controller: 'Receipts',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('Photographer', {
        url: '/photographer',
        templateUrl: site_url + 'template/account/Photographer',
        data: {
            pageTitle: 'Photographer',
            routename: 'Home'
        },
        controller: 'Photographer',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhysicianProfile', {
        url: '/physician-profile',
        templateUrl: site_url + 'template/account/PhysicianProfile',
        data: {
            pageTitle: 'Physician Profile',
            routename: 'Home'
        },
        controller: 'PhysicianProfile',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountPhysicianCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhysicianSaveSearch', {
        url: '/save-search',
        templateUrl: site_url + 'template/account/PhysicianSaveSearch',
        data: {
            pageTitle: 'Save Search',
            routename: 'Home'
        },
        controller: 'SaveSearch',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountPhysicianCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhysicianFavorite', {
        url: '/physician-favorite',
        templateUrl: site_url + 'template/account/PhysicianFavorite',
        data: {
            pageTitle: 'Favorite',
            routename: 'Home'
        },
        controller: 'Favorite',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountPhysicianCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhysicianMessage', {
        url: '/physician-message',
        templateUrl: site_url + 'template/account/PhysicianMessage',
        data: {
            pageTitle: 'Physician Message',
            routename: 'Home'
        },
        controller: 'PhysicianMessage',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/AccountPhysicianCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('Aboutus', {
        url: '/aboutus',
        templateUrl: site_url + 'template/pages/Aboutus',
        data: {
            pageTitle: 'About-us',
            routename: 'Home'
        },
        controller: 'Aboutus',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('TermsofUse', {
        url: '/terms-of-use',
        templateUrl: site_url + 'template/pages/HowWork',
        data: {
            pageTitle: 'Terms of Use',
            routename: 'Home'
        },
        controller: 'HowWork',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PrivatePolicy', {
        url: '/privacy-policy',
        templateUrl: site_url + 'template/pages/PrivatePolicy',
        data: {
            pageTitle: 'Privacy Policy',
            routename: 'Home'
        },
        controller: 'PrivatePolicy',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PhotographerPhoto', {
        url: '/photographer-photo',
        templateUrl: site_url + 'template/pages/PhotographyPhoto',
        data: {
            pageTitle: 'Photographer Photo',
            routename: 'Home'
        },
        controller: 'PhotographerPhoto',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('PremiumAccess', {
        url: '/access/:jobId',
        templateUrl: site_url + 'template/account/PremiumAccess',
        data: {
            pageTitle: 'Premium Access',
            routename: 'Home'
        },
        controller: 'PremiumAccess',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('JobPreview', {
        url: '/job-preview/:jobId',
        templateUrl: site_url + 'template/account/JobPreview',
        data: {
            pageTitle: 'Job Preview',
            routename: 'Home'
        },
        controller: 'JobPreview',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('Clone', {
        url: '/clone/:jobId',
        templateUrl: '',
        data: {
            pageTitle: 'Job clone',
            routename: 'Home'
        },
        controller: 'Clone',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/Job.js'],
                    serie: true
                });
            }]
        }
    }).state('ReceiptsDownload', {
        url: '/receipts-download/:paymentId',
        templateUrl: site_url + 'template/account/ReceiptsDownload',
        data: {
            pageTitle: 'Receipts Download',
            routename: 'Home'
        },
        controller: 'ReceiptsDownload',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('StatisticsJob', {
        url: '/statistics-jobs',
        templateUrl: site_url + 'template/account/StatisticsJob',
        data: {
            pageTitle: 'Statistics Job',
            routename: 'Home'
        },
        controller: 'StatisticsJob',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('ResetPassword', {
        url: '/reset-password/:activationKey',
        templateUrl: site_url + 'template/pages/ResetPassword',
        data: {
            pageTitle: 'Reset Password',
            routename: 'Home'
        },
        controller: 'ResetPassword',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/PagesCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('CreateClone', {
        url: '/copy',
        templateUrl: site_url + 'template/account/CreateClone',
        data: {
            pageTitle: 'Create Clone',
            routename: 'Home'
        },
        controller: 'CreateClone',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('DownloadImage', {
        url: '/download-image',
        templateUrl: site_url + 'template/account/DownloadImage',
        data: {
            pageTitle: 'Download Image',
            routename: 'Home'
        },
        controller: 'DownloadImage',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/JobsCtrl.js'],
                    serie: true
                });
            }]
        }
    }).state('JobDetail', {
        url: '/jobdetail/:jobId',
        templateUrl: site_url + 'template/job/detail',
        data: {
            pageTitle: 'Job Detail',
            routename: 'Home'
        },
        controller: 'JobDetail',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    insertBefore: '#ng_load_plugins_before',
                    files: ['assets/front/js/controller/Job.js'],
                    serie: true
                });
            }]
        }
    })
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $locationProvider.html5Mode(true);
}]);
app.run(['$rootScope', '$location', '$state', function($rootScope, $location, $state, auth) {
    $rootScope.$state = $state;
    $rootScope.previousLocation = '/';
    $rootScope.currentLocation = '/';
    $rootScope.pageTitle = 'YoloMD';
    $rootScope.showGlobalLoader = false;
    $rootScope.$on("$locationChangeStart", function(e, currentLocation, previousLocation) {
        $rootScope.previousLocation = $rootScope.currentLocation;
        $rootScope.currentLocation = $location.path();
    });
    $rootScope.$on("event:auth-loginCancelled", function(data) {
        $location.path($rootScope.previousLocation);
    });
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        /*if (toState.resolve != '' && (toState.name == 'JobListing' || toState.name == 'jobs')) {
            $rootScope.showGlobalLoader = true;
        } else {*/
            show_hide_loader('search_loader', true);
        // }
    });
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            show_hide_loader('search_loader', false);
        }
        if (toState.controller !='HomeCtrl') 
            $rootScope.bodyClass = 'body-wrapper';
        else
            $rootScope.bodyClass = '';
    });
}]);

function show_hide_loader(id, status) {
    if (status) {
        $('#' + id).show();
    } else {
        $('#' + id).hide();
        $('#' + id).css('display', 'none!important');
    }
}

function open_modal(modalId) {
    $('#' + modalId).modal('show');
}
var setTime;

function common_modal(id) {
    myStopFunction();
    $('#' + id).modal('show');
    setTime = setTimeout(function() {
        $("#" + id).modal('hide');
    }, 8000);
}

function myStopFunction() {
    clearTimeout(setTime);
}

function common_modal_close(id) {
    setTimeout(function() {
        $("#" + id).modal('hide');
    }, 7000);
}

function common_modal_premium(id) {
    myStopFunction();
    $('#' + id).modal('show');
    setTime = setTimeout(function() {
        $("#" + id).modal('hide');
    }, 25000);
}