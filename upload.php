<?php

if ( !empty( $_FILES ) ) {

   	$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $name = $_FILES[ 'file' ]['name' ];
    $img_destination_url = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'assets/uploads/jobs' . DIRECTORY_SEPARATOR . $name;
    $imgThumb_destination_url = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'assets/uploads/jobs/thumbs' . DIRECTORY_SEPARATOR . $name;
    function compress_image($source_url, $destination_url , $quality ,$maxWidth ,$maxHeight ) {
		$info = list($width, $height, $type, $attr) = getimagesize($source_url);

		if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
		elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
	
		$origWidth = $width;
		$origHeight = $height;

		if ($maxWidth == 0)
	    {
	        $maxWidth  = $height;
	    }

	    if ($maxHeight == 0)
	    {
	        $maxHeight = $width;
	    }
	    if($origWidth <=0)
	    {
	    	$origWidth = 1;
	    }
	    if($origHeight <=0)
	    {
	    	$origHeight = 1;
	    }
	    // Calculate ratio of desired maximum sizes and original sizes.
	    $widthRatio = $maxWidth / $origWidth;
	    $heightRatio = $maxHeight / $origHeight;

	    // Ratio used for calculating new image dimensions.
	    $ratio = min($widthRatio, $heightRatio);

	    // Calculate new image dimensions.
	    $newWidth  = (int)$origWidth  * $ratio;
	    $newHeight = (int)$origHeight * $ratio;
	    // Calculate new image dimensions.
	    $newThumbWidth  = (int)$newWidth/5;
	    $newThumbHeight = (int)$newHeight/5;

	    // Create final image with new dimensions.
	    $newImage = imagecreatetruecolor($newWidth, $newHeight);

	    imagealphablending( $newImage, false ); 
	    imagesavealpha( $newImage, true );

	    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
	    
	    if ($info['mime'] == 'image/jpeg') imagejpeg($newImage, $destination_url, $quality);
		elseif ($info['mime'] == 'image/png') imagepng($newImage, $destination_url);
		return $destination_url;
	}
 
	//usage
	$compressed = compress_image($tempPath, $img_destination_url, 90 ,900 ,600);
	$thumb_compressed = compress_image($tempPath, $imgThumb_destination_url, 50 ,600 ,450);
    // $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'assets/uploads/jobs' . DIRECTORY_SEPARATOR . $compressed;

    // move_uploaded_file( $tempPath, $uploadPath );

    $answer = array( 'answer' => 'File transfer completed' );
    $json = json_encode( $answer );

    echo $json;

} else {

    echo 'No files';

}

?>
